SUMMARY = "nrfutil"
DESCRIPTION = " nrfutil ble firmware update"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"
LICENSE = "CLOSED"

PREFERRED_VERSION_pip = "18.0"
DEPENDS += "python-pip"

PACKAGE_ARCH = "all"

SRC_URI = "file://*"

do_install () {
    install -d ${D}${datadir}/nrfutil
    install -m 0644 -D -t ${D}${datadir}/nrfutil/ ${WORKDIR}/*.gz
    install -m 0644 -D -t ${D}${datadir}/nrfutil/ ${WORKDIR}/*.whl
    install -m 0644 -D -t ${D}${datadir}/nrfutil/ ${WORKDIR}/requirements.txt
}

FILES_${PN} += "${datadir}/nrfutil"
