# The source code is a part of JN-AN-1222.zip souce
# link "http://www.nxp.com/documents/application_note/JN-AN-1222.zip"

DESCRIPTION = " JNFlasher utility applications "
SECTION = "app"
LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

PR = "r0"


SRC_URI = "file://*"

S = "${WORKDIR}"

do_install () {
    install -d ${D}${bindir}
    install -m 0755 ${S}/jnFlasher/jennicProgrammer/iot_jp  ${D}${bindir}/iot_jp
    install -m 0755 ${S}/jnFlasher/jennicDetector/iot_jd  ${D}${bindir}/iot_jd
    install -m 0755 ${S}/jnFlasher/scripts/JennicModuleProgram.sh  ${D}${bindir}/JennicModuleProgram.sh
    install -d ${D}/usr/share/iot/
    install -m 0755 ${S}/jnFlasher/jennicProgrammer/JN5169_module_firmware_1_4_1.bin  ${D}/usr/share/iot/
}

INSANE_SKIP_${PN} += "already-stripped"
FILES_${PN} += " \
    /usr/share/iot/JN5169_module_firmware_1_4_1.bin \
"
