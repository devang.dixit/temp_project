DESCRIPTION = "Cab-Gateway Image"

# Automatically locate license files in "/usr/share/common-licenses".
COPY_LIC_MANIFEST = "1"
COPY_LIC_DIRS = "1"

require recipes-core/images/core-image-base.bb
require recipes-extended/images/swupdate-image.bb
require recipes-support/u-boot-imx-fw-utils/u-boot-imx-fw-utils.bb

export IMAGE_BASENAME = "cabgateway"
#IMAGE_FSTYPES = "tar tar.bz2 ext4 sdcard.bz2 sdcard"
IMAGE_FSTYPES = "tar tar.bz2 "

IMAGE_INSTALL += " \
    ${CORE_IMAGE_BASE_INSTALL} \
    u-boot-imx-fw-utils \
"

#IMAGE_INSTALL += " \
#    ${CORE_IMAGE_BASE_INSTALL} \
#"

inherit extrausers
EXTRA_USERS_PARAMS = "usermod -P cg123 cabgateway;"

CORE_IMAGE_EXTRA_INSTALL += " i2c-tools "
CORE_IMAGE_EXTRA_INSTALL += " ppp "
CORE_IMAGE_EXTRA_INSTALL += " kernel-modules "
CORE_IMAGE_EXTRA_INSTALL += " eg91LTESupport "
CORE_IMAGE_EXTRA_INSTALL += " cabgatewayapp "
CORE_IMAGE_EXTRA_INSTALL += " cabgatewayutils "
CORE_IMAGE_EXTRA_INSTALL += " zip "
CORE_IMAGE_EXTRA_INSTALL += " python python-pip "
CORE_IMAGE_EXTRA_INSTALL += " pip-python-magic "
CORE_IMAGE_EXTRA_INSTALL += " canutils libsocketcan "
CORE_IMAGE_EXTRA_INSTALL += " python-nrfutil "
CORE_IMAGE_EXTRA_INSTALL += " jnflash "
CORE_IMAGE_EXTRA_INSTALL += " openssl "
CORE_IMAGE_EXTRA_INSTALL += " openssh "
CORE_IMAGE_EXTRA_INSTALL += " busybox-syslog "
CORE_IMAGE_EXTRA_INSTALL += " libiio "
