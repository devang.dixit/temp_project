#
# This file was derived from the 'Hello World!' example recipe in the
# Yocto Project Development Manual.
#

DESCRIPTION = " BLE and Zigbee Application "
SECTION = "examples"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"
PR= "r0"

SRC_URI = " file://cabgatewayutils/ "

S = "${WORKDIR}/cabgatewayutils/"

TARGET_CC_ARCH += "${LDFLAGS}" 

PACKAGES = "${PN} ${PN}-dev "

inherit systemd

SYSTEMD_SERVICE_${PN} = "remoteDiag.service"

do_install() {
    echo "Cabgateway utilities installing"
    install -d -m 777 ${D}/cabApp/
    install -d -m 777 ${D}/cabApp/ble/
    install -d -m 777 ${D}/cabApp/can/
    install -d -m 777 ${D}/cabApp/rs232/
    install -d -m 777 ${D}/cabApp/zigbee/
    install -d -m 777 ${D}/cabApp/ebyte/
    install -v ${S}/softwareVersion.txt ${D}/cabApp/.
    install -v ${S}/flashOTA.sh ${D}/cabApp/.
#    install -v ${S}/accelActivity.sh ${D}/cabApp/.
    install -v ${S}/ble/* ${D}/cabApp/ble/.
    install -v ${S}/can/* ${D}/cabApp/can/.
    install -v ${S}/rs232/* ${D}/cabApp/rs232/.
    install -v ${S}/zigbee/* ${D}/cabApp/zigbee/.
    install -v ${S}/ebyte/* ${D}/cabApp/ebyte/.
    install -d ${D}/etc/init.d/
    install -m 0755 ${S}/cabAutoBoot.sh ${D}/etc/init.d/
    install -m 0755 ${S}/cabStartApps.sh ${D}/cabApp/
    install -m 0755 ${S}/packSyslog.sh ${D}/cabApp/
    install -d ${D}${systemd_unitdir}/system
    install -m 0644 ${S}/remoteDiag.service ${D}${systemd_unitdir}/system/
    install -m 0777 ${S}/testjigApp ${D}/cabApp/
    install -m 0777 ${S}/cc1310.out ${D}/cabApp/

    install -d ${D}/etc/rc2.d/
    ln -sf ../init.d/cabAutoBoot.sh  ${D}/etc/rc2.d/S99cabAutoBoot.sh
    install -d ${D}/etc/rc3.d/
    ln -sf ../init.d/cabAutoBoot.sh  ${D}/etc/rc3.d/S99cabAutoBoot.sh
    install -d ${D}/etc/rc4.d/
    ln -sf ../init.d/cabAutoBoot.sh  ${D}/etc/rc4.d/S99cabAutoBoot.sh
    install -d ${D}/etc/rc5.d/
    ln -sf ../init.d/cabAutoBoot.sh  ${D}/etc/rc5.d/S99cabAutoBoot.sh
    install -d ${D}/etc/rc6.d/
    ln -sf ../init.d/cabAutoBoot.sh  ${D}/etc/rc6.d/S99cabAutoBoot.sh
    install -d ${D}/etc/ssl/
    install -v ${S}/client.pem ${D}/etc/ssl/.
}

#FILES_${PN} = " \
#    /home/root \
#    ${libdir}/lib*${SOLIBSDEV} \
#    ${sysconfdir}/udev/rules.d \
#    ${datadir}/hsdk \
#
#    "

FILES_${PN} += "/cabApp/*"
FILES_${PN} += "/etc/init.d/*"
FILES_${PN} += "/etc/rc2.d/*"
FILES_${PN} += "/etc/rc3.d/*"
FILES_${PN} += "/etc/rc4.d/*"
FILES_${PN} += "/etc/rc5.d/*"
FILES_${PN} += "/etc/rc6.d/*"

# Added to avoid QA issue: No GNU_HASH in the elf binary
INSANE_SKIP_${PN} = "ldflags dev-deps"
INSANE_SKIP_${PN}-dev = "ldflags"


INSANE_SKIP_${PN} = "installed-vs-shipped "

COMPATIBLE_MACHINE = "(mx6|mx6ul|mx7|mx6ull)"

ALLOW_EMPTY_${PN} = "1"
