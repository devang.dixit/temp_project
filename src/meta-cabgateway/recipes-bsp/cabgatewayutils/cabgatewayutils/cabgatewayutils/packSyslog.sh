#!/bin/sh -e
### BEGIN INIT INFO
# Provides:          CAB Gateway
# Short-Description: CAB Gateway Application collect and package syslogs from RAM.
### END INIT INFO

FILE_PATH_SYSLOG_RAM="/var/syslog/messages"
SYSLOG_FILE_NAME="log"
LOG_ZIP_FILE_NAME="CG_$(date +%s)_log.zip"
SYSLOG_ZIP_STATUS_FILE="/home/root/syslog-zip-status-file.txt"
DIR_PATH_SYSLOG="/media/userdata/sysLog"
INIT_TIME=0

MERGE_COMMAND="DIR_PATH_SYSLOG=\"/media/userdata/sysLog\" SYSLOG_FILE_NAME=\"log\" cat % >> $DIR_PATH_SYSLOG/$SYSLOG_FILE_NAME; rm %"

if [ $1 == $INIT_TIME ]; then
	# To be called after syslog module init.
	echo "INIT_TIME"
	files=()
	# List all the files older than today start of the day
	for f in $FILE_PATH_SYSLOG_RAM*
	do
		if [[ -e $f && $(date -r $f +%s) -le $(date -d 00:00:00 +%s) ]]; then
			files+="$f "
		fi
	done
	# As per older to newer timestamp, get the file from RAM, merge into log file on data partition and zip it.
	if (( ${#files[@]} )); then
		ls -tr $files | xargs -r -I % -t sh -c "$MERGE_COMMAND"
		cd $DIR_PATH_SYSLOG
		FILE_NAME="${SYSLOG_FILE_NAME}_$(date -d -24:00:00 +%m_%d)"
		mv $SYSLOG_FILE_NAME $FILE_NAME
		zip -m $LOG_ZIP_FILE_NAME $FILE_NAME 2>/dev/null 1>/dev/null
		cd -
		if [ $? == 0 ]; then
			echo 1 > $SYSLOG_ZIP_STATUS_FILE
			echo "Zipped succesfully"
		else
			echo $? > $SYSLOG_ZIP_STATUS_FILE
			echo "Zipping failed"
		fi
	else
		echo "No older syslog file found"
		# Now only today's log file are available in RAM. Merge the available log files as well to reduce the RAM load.
		ls -tr $FILE_PATH_SYSLOG_RAM* | xargs -r -I % -t sh -c "$MERGE_COMMAND"
		echo $? > $SYSLOG_ZIP_STATUS_FILE
	fi
	sync
else
	# To be called at the end of the day
	echo "DAY_CHANGE"
	# As per older to newer timestamp, get the file from RAM, merge into log file on data partition and zip it.
	ls -tr $FILE_PATH_SYSLOG_RAM* | xargs -r -I % -t sh -c "$MERGE_COMMAND"
	cd $DIR_PATH_SYSLOG
	if [ -e $SYSLOG_FILE_NAME ]; then
		FILE_NAME="${SYSLOG_FILE_NAME}_$(date +%m_%d)"
		mv $SYSLOG_FILE_NAME $FILE_NAME
		zip -m $LOG_ZIP_FILE_NAME $FILE_NAME 2>/dev/null 1>/dev/null
		echo "Zipped succesfully"
	fi
	cd -
	echo $? > $SYSLOG_ZIP_STATUS_FILE
fi

sync