#!/bin/sh

RST_PIN=$1
CURR_DIR=$('pwd')

cd /sys/class/gpio
if [ ! -e gpio$RST_PIN ];then
	echo $RST_PIN > export
	echo "out" > gpio$RST_PIN/direction
fi
echo 0 > /sys/class/gpio/gpio$RST_PIN/value
sleep 1
echo 1 > /sys/class/gpio/gpio$RST_PIN/value
sleep 1
echo 0 > /sys/class/gpio/gpio$RST_PIN/value
sleep 1
echo 1 > /sys/class/gpio/gpio$RST_PIN/value

# command to flash zigbee coordinator firmware
#./JennicModuleProgram.sh JN5169_module_firmware_1_5_0.bin --erasepdm

#start zigbee coordinator
cd /cabApp/zigbee
killall zigbee_control
./zigbee_control /dev/ttymxc3 > "/cabApp/zigbee/zigbeeLog.txt" &
cd -

# press reset s/w 7 times on end node (with 1s interval) to start discovery.
# it will take approx. 30s to join and start sending data
