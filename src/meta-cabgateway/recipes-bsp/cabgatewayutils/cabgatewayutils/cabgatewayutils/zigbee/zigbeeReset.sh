echo "Resetting ZigBee..."
echo 38 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio38/direction
sleep 1
echo 0 > /sys/class/gpio/gpio38/value
sleep 1
echo 1 > /sys/class/gpio/gpio38/value
sleep 1
echo 0 > /sys/class/gpio/gpio38/value
sleep 1
echo 1 > /sys/class/gpio/gpio38/value
sleep 1
echo "ZigBee Reset Done."
