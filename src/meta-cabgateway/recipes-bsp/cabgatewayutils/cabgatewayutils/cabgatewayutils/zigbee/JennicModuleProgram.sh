#!/bin/sh

# Copyright (c) 2018 NXP
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# o Redistributions of source code must retain the above copyright notice, this list
#   of conditions and the following disclaimer.
#
# o Redistributions in binary form must reproduce the above copyright notice, this
#   list of conditions and the following disclaimer in the documentation and/or
#   other materials provided with the distribution.
#
# o Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from this
#   software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

# ----------------------------------------------------------------------
# Script that Flash the firmware on JN51xx module connected with Modular Gateway
# ----------------------------------------------------------------------


if [ $# != 1 ] && [ $# != 2 ]
then
    echo "Error: Invalid arguments"
    echo "    To flash the firmware without erasing PDM"
    echo "        ${0} <Firmware_File>"
    echo "    To flash the firmware erasing PDM"
    echo "        ${0} <Firmware_File> --erasepdm"
    exit 1
fi

if [ ! -f "$1" ]
then
    echo "Error: Update File $1 does not exist"
    exit 1
fi

# Put JN51xx MISO pin to low
MISO_PIN=36
if [ ! -f "/sys/class/gpio/gpio$MISO_PIN/direction" ]
then 
	echo $MISO_PIN > /sys/class/gpio/export
fi
echo out > /sys/class/gpio/gpio$MISO_PIN/direction
echo 0 > /sys/class/gpio/gpio$MISO_PIN/value

# Now reset JN51xx to put in Programming mode
RESET_PIN_MK=38
if [ ! -f "/sys/class/gpio/gpio$RESET_PIN_MK/direction" ]
then 
	echo $RESET_PIN_MK > /sys/class/gpio/export
fi

echo out > /sys/class/gpio/gpio$RESET_PIN_MK/direction

echo 0 > /sys/class/gpio/gpio$RESET_PIN_MK/value
sleep 1
echo 1 > /sys/class/gpio/gpio$RESET_PIN_MK/value
sleep 1

RESET_PIN_MZ=36
if [ ! -f "/sys/class/gpio/gpio$RESET_PIN_MZ/direction" ]
then 
	echo $RESET_PIN_MZ > /sys/class/gpio/export
fi

echo out > /sys/class/gpio/gpio$RESET_PIN_MZ/direction

echo 0 > /sys/class/gpio/gpio$RESET_PIN_MZ/value
sleep 1
echo 1 > /sys/class/gpio/gpio$RESET_PIN_MZ/value
sleep 1

# Now Flash the firmware
# Call the flasher program
if [ $1 ]
then
  echo Flash $1
  if [ "$2" == "--erasepdm" ]
  then
    iot_jp -s /dev/ttymxc3 -f $1 -v -V 2 --erasepdm
  else
    iot_jp -s /dev/ttymxc3 -f $1 -v -V 2
  fi
  result=$?
fi
sleep 1

# Put JN51xx MISO pin to High
echo 1 > /sys/class/gpio/gpio$MISO_PIN/value
sleep 1
# Now reset JN51xx to put in running mode
echo 0 > /sys/class/gpio/gpio$RESET_PIN_MK/value
sleep 1
echo 1 > /sys/class/gpio/gpio$RESET_PIN_MK/value
sleep 1
echo 0 > /sys/class/gpio/gpio$RESET_PIN_MZ/value
sleep 1
echo 1 > /sys/class/gpio/gpio$RESET_PIN_MZ/value
sleep 1


exit 0


