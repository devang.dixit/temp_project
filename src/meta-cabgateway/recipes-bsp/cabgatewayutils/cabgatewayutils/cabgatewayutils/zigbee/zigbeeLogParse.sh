#!/bin/sh

ZIGBEE_LOG_FILE="/cabApp/zigbee/zigbeeLog.txt"
ZIGBEE_CNT_THRESHOLD=2
ZIGBEE_RSSI_THRESHOLD=32
ZIGBEE_ADDRESS=$1
ZIGBEE_TMP_FILE="/cabApp/zigbee/tmp"

killall zigbee_control

function revCab() {
        declare copy=${1:-$(</dev/stdin)};
        len=${#copy}
        i=$len
        while [ $i -ge 0 ] ; 
        do 
                rev="$rev${copy:$i:1}";
                i=`expr $i - 1`
        done

        echo $rev
}



if [ $# -ne 1 ]; then 
        echo "Usage: $0 <EUI number>"
        exit 1
fi

rssiSum=0
count=0

cat $ZIGBEE_LOG_FILE > $ZIGBEE_TMP_FILE

for line in `cat $ZIGBEE_TMP_FILE`; do
        eui=`echo $line | cut -d"," -f1`
        echo $eui
        ZIGBEE_ADDRESS=`echo "$ZIGBEE_ADDRESS" | awk '{print toupper($0)}'`
        eui=`echo "$eui" |  awk '{print toupper($0)}'`
        if [ "$eui" == "$ZIGBEE_ADDRESS" ]; then
                rssiSum=`echo $line | cut -d "," -f2`
                echo $rssiSum
                count=`echo $line | cut -d "," -f3`
                echo $count
        fi
done

rm $ZIGBEE_TMP_FILE

if [ $count -ne 0 ]; then
        rssiavg=`expr $rssiSum / $count`
else 
        rssiavg=0
fi
if [ $rssiavg -lt $ZIGBEE_RSSI_THRESHOLD -o $count -lt $ZIGBEE_CNT_THRESHOLD ];
then
        if [ $rssiavg -lt $ZIGBEE_RSSI_THRESHOLD ]; then
                echo "Zigbee test failed as avg RSSI "$rssiavg" is less than " $ZIGBEE_RSSI_THRESHOLD
        fi
        if [ $count -lt $ZIGBEE_CNT_THRESHOLD ]; then
                echo "Zigbee test failed as count "$count" is less than "$ZIGBEE_CNT_THRESHOLD
        fi
        exit 1;
else
        echo "RSSI CNT: "$count
        echo "RSSI SUM: "$rssiSum
        echo "RSSI AVG: "$rssiavg
        echo "Pass"
        exit 0;
fi
