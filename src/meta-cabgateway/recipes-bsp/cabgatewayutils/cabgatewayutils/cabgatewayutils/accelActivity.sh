#!/bin/sh
INACT_THS=$1
INACT_DUR=$2
ACCEL_ODR=$3
if [ $# -lt 3 ]; then
	echo "Usage $0 <inactivity threshold> <inactivity duration> <accelerometer ODR>"
	exit 1
fi
echo -e "\n *******************************************************" 
echo -e "\n *********** STARTING CAB ACC SERVICES ***************" 
echo -e "\n *******************************************************" 
accelService -d lis2hh12_accel  -r 3 -b 2 -t lis2hh12_accel-trigger -T $INACT_THS -D $INACT_DUR -O $ACCEL_ODR 
echo -e "\n *******************************************************" 
