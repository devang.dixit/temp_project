#!/bin/sh

SUCCESS=0
FAILURE=1

BLE_LOG_FILE=/blelog.txt
BLE_REFERENCE_STRING="Device programmed"

BLE_RST_PIN=32
BLE_PROG_PIN=39

SOFTDEVICE_NAME=softdevice.zip
APPLICATION_NAME=ble_app.zip

WORK_DIR=$(pwd)

if [ -e /usr/bin/nrfutil ];then

	if [ "$1" = "MOUNT_PARTITION" ];then
		echo "Flashing BLE from Mount Partition"
		BLE_PATH=/cabApp/ble
	elif [ "$1" = "UPGRADE_PARTITION" ];then
		echo "Flashing BLE from Upgrade Partition"
		BLE_PATH=/tmp/ble/cabApp/ble
	else
		echo "Invalid Argument"
		echo "BLE OTA FAIL"
		exit 1;
	fi

	isSoftdevicePresent() {
		if [ -e $BLE_PATH/$SOFTDEVICE_NAME ];then
			echo "BLE Softdevice found"
			return $SUCCESS
		else
			return $FAILURE
		fi
	}

	isApplicationPresent() {
		if [ -e $BLE_PATH/$APPLICATION_NAME ];then
			echo "BLE Application found"
			return $SUCCESS
		else
			return $FAILURE
		fi
	}

	enableBLEGpio() {
		cd /sys/class/gpio
		if [ ! -e gpio$BLE_RST_PIN ];then
			echo $BLE_RST_PIN > export
			echo "out" > gpio$BLE_RST_PIN/direction
		fi

		if [ ! -e gpio$BLE_PROG_PIN ];then
			echo $BLE_PROG_PIN > export
			echo "out" > gpio$BLE_PROG_PIN/direction
		fi

		cd $WORK_DIR

		echo 0 > /sys/class/gpio/gpio$BLE_PROG_PIN/value
		echo 0 > /sys/class/gpio/gpio$BLE_RST_PIN/value
		echo 1 > /sys/class/gpio/gpio$BLE_RST_PIN/value
		echo 1 > /sys/class/gpio/gpio$BLE_PROG_PIN/value
	}

	flashSoftdevice() {
		BLE_OUTPUT_STRING="undefined"
		nrfutil dfu serial -pkg $BLE_PATH/$SOFTDEVICE_NAME -p /dev/ttymxc4 -fc 0 -b 115200 > $BLE_LOG_FILE
		BLE_OUTPUT_STRING=`awk "/$BLE_REFERENCE_STRING/{print}" $BLE_LOG_FILE`
		if [ -n "$BLE_OUTPUT_STRING" ];then
			echo "BLE Softdevice Flashed"
			return $SUCCESS
		else
			return $FAILURE
		fi
	}

	flashApplication() {
		BLE_OUTPUT_STRING="undefined"
		nrfutil dfu serial -pkg $BLE_PATH/$APPLICATION_NAME -p /dev/ttymxc4 -fc 0 -b 115200 > $BLE_LOG_FILE
		BLE_OUTPUT_STRING=`awk "/$BLE_REFERENCE_STRING/{print}" $BLE_LOG_FILE`
		if [ -n "$BLE_OUTPUT_STRING" ];then
			echo "BLE Application Flashed"
			return $SUCCESS
		else
			return $FAILURE
		fi
	}

	# Check whether softdevice is present or not
	isSoftdevicePresent
	if [ $? -ne $SUCCESS ]; then
		echo "BLE Softdevice Not found"
		exit 1
	fi

	# Check whether application is present or not
#	isApplicationPresent
#	if [ $? -ne $SUCCESS ]; then
#		echo "BLE Application Not found"
#		exit 1
#	fi

	# Enable gpio for programming mode
	enableBLEGpio
	if [ $? -ne $SUCCESS ]; then
		exit 1
	fi

	# Flash Softdevice
	flashSoftdevice
	if [ $? -ne $SUCCESS ]; then
		echo "Failed to Flash BLE Softdevice"
		exit 1
	fi

	# Flash Application
#	flashApplication
#	if [ $? -ne $SUCCESS ]; then
#		echo "Failed to Flash BLE Application"
#		exit 1
#	fi

	echo "BLE SWUpdate Successfull"
	exit 0
else
	echo "nrfutil not present in rootfs"
	if [ "$1" = "MOUNT_PARTITION" ];then
		exit 0
	else
		echo "BLE OTA FAIL"
		exit 1
	fi
fi
