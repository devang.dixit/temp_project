#!/bin/sh

BLE_RST_PIN=32

WORK_DIR=$(pwd)

cd /sys/class/gpio
if [ ! -e gpio$BLE_RST_PIN ];then
    echo $BLE_RST_PIN > export
    echo "out" > gpio$BLE_RST_PIN/direction
fi
cd $WORK_DIR
echo 0 > /sys/class/gpio/gpio$BLE_RST_PIN/value
sleep 1
echo 1 > /sys/class/gpio/gpio$BLE_RST_PIN/value

echo "BLE module reset. Waiting for resume..."
