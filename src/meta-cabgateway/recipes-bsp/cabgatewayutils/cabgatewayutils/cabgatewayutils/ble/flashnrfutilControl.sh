#!/bin/sh

`which nrfutil`
if [ $? -eq 0 ];
then
	exit 0
else
	cd /cabApp/tools/nrfutil_arm
	sh flashnrfutil.sh
fi
