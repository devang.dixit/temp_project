#!/bin/sh

# SUCCESS/FAIL macros
SUCCESS=1
FAILURE=0

#Status of different components updated in OTA
zb_status=0
ble_status=0
can_status=0
rf_status=0
ebyte_status=0
full_update=0

#OTA diagnostics file
#OTA_DIAG_FILE="/media/userdata/dataStorage/diag.csv"
OTA_DIAG_FILE="/dev/null"
#OTA status file populated with different component's OTA status information
OTA_LOG_FILE=ota.txt

#OTA verification reference string 
FULL_OTA_VERIFY_REFERENCE_STRING="successfully checked "
INCR_OTA_VERIFY_REFERENCE_STRING="Incremental OTA verified succeffully"

#OTA perform verification string 
FULL_OTA_PERFORM_REFERENCE_STRING1="Software updated successfully"
FULL_OTA_PERFORM_REFERENCE_STRING2="Please reboot the device to start the new software"
FULL_OTA_PERFORM_REFERENCE_STRING3="SWUPDATE successful ! "

INCR_OTA_PERFORM_REFERENCE_STRING1="Incremental OTA performed successfully"
INCR_OTA_PERFORM_REFERENCE_STRING2="CGUPDATE successful !"

OTA_BLE_PERFORM_REF_STRING="Updating BLE module firmware"
OTA_ZB_PERFORM_REF_STRING="Updating ZigBee module firmware"
OTA_CAN_PERFORM_REF_STRING="Updating CAN firmware"
OTA_RF_PERFORM_REF_STRING="Updating RF module firmware"
OTA_EBYTE_PERFORM_REF_STRING="Updating EBYTE module firmware"

#OTA environment update success verification string
OTA_ENV_UPDATE_REF_STRING="OTA environment updated successfully"

# FULL OTA & Incremental OTA  file extension
FULL_OTA_FILE_NAME_EXTENSION="swu"
INCR_OTA_FILE_NAME_EXTENSION="cgu"
DETECTED_OTA_TYPE="undefined"

# Global variables
OTA_VERIFY_OUTPUT_STRING="undefined"
OTA_PERFORM_OUTPUT_STRING1="undefined"
OTA_PERFORM_OUTPUT_STRING2="undefined"
OTA_PERFORM_OUTPUT_STRING3="undefined"


verifyFullOTAImage() {
	retValue=$FAILURE
	swupdate -c -i $1 -v -k /etc/keys/public.pem > $OTA_LOG_FILE
	cat $OTA_LOG_FILE >> $OTA_DIAG_FILE

	OTA_VERIFY_OUTPUT_STRING=`awk "/$FULL_OTA_VERIFY_REFERENCE_STRING/{print}" $OTA_LOG_FILE`

	if [ -n "$OTA_VERIFY_OUTPUT_STRING" ]
	then
		retValue=$SUCCESS
		echo "FULL OTA Image Verify Status : PASS"
		echo "FULL OTA Image Verify Status : PASS" >> $OTA_DIAG_FILE
	else
		retValue=$FAILURE
		echo "FULL OTA Image Verify Status : FAIL"
		echo "FULL OTA Image Verify Status : FAIL" >> $OTA_DIAG_FILE
	fi
	rm $OTA_LOG_FILE
	return $retValue
}

verifyIncrOTAImage() {
	retValue=$FAILURE
	cgupdate -c /etc/keys/public.pem $1 > $OTA_LOG_FILE
	cat $OTA_LOG_FILE >> $OTA_DIAG_FILE

	OTA_VERIFY_OUTPUT_STRING=`awk "/$INCR_OTA_VERIFY_REFERENCE_STRING/{print}" $OTA_LOG_FILE`

	if [ -n "$OTA_VERIFY_OUTPUT_STRING" ]
	then
		retValue=$SUCCESS
		echo "Incremental OTA Image Verify Status : PASS"
		echo "Incremental OTA Image Verify Status : PASS" >> $OTA_DIAG_FILE
	else
		retValue=$FAILURE
		echo "Incremental OTA Image Verify Status : FAIL"
		echo "Incremental OTA Image Verify Status : FAIL" >> $OTA_DIAG_FILE
	fi
	rm $OTA_LOG_FILE
	return $retValue
}

verifyOTAImage() {
	ota_file=$1
	DETECTED_OTA_TYPE="${ota_file##*.}"
	if [ "$DETECTED_OTA_TYPE" = "$FULL_OTA_FILE_NAME_EXTENSION" ]; then
  		echo "OTA file type: Full OTA"	
  		echo "OTA file type: Full OTA" >> $OTA_DIAG_FILE	
		full_update=1
		verifyFullOTAImage $1
		return $?
	elif [ "$DETECTED_OTA_TYPE" = "$INCR_OTA_FILE_NAME_EXTENSION" ]; then
  		echo "OTA file type: Incremental OTA"	
  		echo "OTA file type: Incremental OTA" >> $OTA_DIAG_FILE
		full_update=0
		verifyIncrOTAImage $1
		return $?
	else
  		echo "OTA file type(${DETECTED_OTA_TYPE}) not supported"	
  		echo "OTA file type(${DETECTED_OTA_TYPE}) not supported" >> $OTA_DIAG_FILE
		return $FAILURE
	fi
}

performFullOTA() {
	retValue=$FAILURE
	swupdate -i $1 -v -k /etc/keys/public.pem > $OTA_LOG_FILE
	cat $OTA_LOG_FILE >> $OTA_DIAG_FILE

	OTA_PERFORM_OUTPUT_STRING1=`awk "/$FULL_OTA_PERFORM_REFERENCE_STRING1/{print}" $OTA_LOG_FILE`
	OTA_PERFORM_OUTPUT_STRING2=`awk "/$FULL_OTA_PERFORM_REFERENCE_STRING2/{print}" $OTA_LOG_FILE`
	OTA_PERFORM_OUTPUT_STRING3=`awk "/$FULL_OTA_PERFORM_REFERENCE_STRING3/{print}" $OTA_LOG_FILE`

	if [ -n "$OTA_PERFORM_OUTPUT_STRING1" ] && [ -n "$OTA_PERFORM_OUTPUT_STRING2" ] &&
	   [ -n "$OTA_PERFORM_OUTPUT_STRING3" ]	
	then
		retValue=$SUCCESS
		echo "FULL OTA Perform Status : PASS"
		echo "FULL OTA Perform Status : PASS" >> $OTA_DIAG_FILE
	else
		retValue=$FAILURE
		echo "FULL OTA Perform Status : FAIL"
		echo "FULL OTA Perform Status : FAIL" >> $OTA_DIAG_FILE
	fi
	#Get BLE update status
	OTA_PERFORM_OUTPUT_STRING3=`awk "/$OTA_BLE_PERFORM_REF_STRING/{print}" $OTA_LOG_FILE`
	if [ -n "$OTA_PERFORM_OUTPUT_STRING3" ]
	then
		ble_status=1
	fi
	#Get ZigBee update status
	OTA_PERFORM_OUTPUT_STRING3=`awk "/$OTA_ZB_PERFORM_REF_STRING/{print}" $OTA_LOG_FILE`
	if [ -n "$OTA_PERFORM_OUTPUT_STRING3" ]
	then
		zb_status=1
	fi
	#Get CAN update status
	OTA_PERFORM_OUTPUT_STRING3=`awk "/$OTA_CAN_PERFORM_REF_STRING/{print}" $OTA_LOG_FILE`
	if [ -n "$OTA_PERFORM_OUTPUT_STRING3" ]
	then
		can_status=1
	fi
	#Get RF update status
	OTA_PERFORM_OUTPUT_STRING3=`awk "/$OTA_RF_PERFORM_REF_STRING/{print}" $OTA_LOG_FILE`
	if [ -n "$OTA_PERFORM_OUTPUT_STRING3" ]
	then
		rf_status=1
	fi
	#Get EBYTE update status
	OTA_PERFORM_OUTPUT_STRING3=`awk "/$OTA_EBYTE_PERFORM_REF_STRING/{print}" $OTA_LOG_FILE`
	if [ -n "$OTA_PERFORM_OUTPUT_STRING3" ]
	then
		ebyte_status=1
	fi
	#rm $OTA_LOG_FILE
	return $retValue
}

performIncrOTA() {
	retValue=$FAILURE
	cgupdate -p > $OTA_LOG_FILE
	cat $OTA_LOG_FILE >> $OTA_DIAG_FILE

	OTA_PERFORM_OUTPUT_STRING1=`awk "/$INCR_OTA_PERFORM_REFERENCE_STRING1/{print}" $OTA_LOG_FILE`

	OTA_PERFORM_OUTPUT_STRING2=`awk "/$INCR_OTA_PERFORM_REFERENCE_STRING2/{print}" $OTA_LOG_FILE`


	if [ -n "$OTA_PERFORM_OUTPUT_STRING1" ] && [ -n "$OTA_PERFORM_OUTPUT_STRING2" ] &&
	   [ -n "$OTA_PERFORM_OUTPUT_STRING3" ]	
	then
		retValue=$SUCCESS
		echo "Incremental OTA Perform Status : PASS"
		echo "Incremental OTA Perform Status : PASS" >> $OTA_DIAG_FILE
	else
		retValue=$FAILURE
		echo "Incremental OTA Perform Status : FAIL"
		echo "Incremental OTA Perform Status : FAIL" >> $OTA_DIAG_FILE
	fi
	#rm $OTA_LOG_FILE
	return $retValue
}
#Perform OTA image
performOTA() {
	if [ "$DETECTED_OTA_TYPE" = "$FULL_OTA_FILE_NAME_EXTENSION" ]; then
  		echo "Performing FULL OTA upgrade ..."	
  		echo "Performing FULL OTA upgrade ..." >> $OTA_DIAG_FILE	
		performFullOTA $1
		return $?
	elif [ "$DETECTED_OTA_TYPE" = "$INCR_OTA_FILE_NAME_EXTENSION" ]; then
  		echo "Performing Incremental OTA upgrade ..."	
  		echo "Performing Incremental OTA upgrade ..." >> $OTA_DIAG_FILE
		performIncrOTA $1
		return $?
	else
  		echo "Can not perform OTA. File type(${DETECTED_OTA_TYPE}) not supported"	
  		echo "Can not perform OTA. File type(${DETECTED_OTA_TYPE}) not supported" >> $OTA_DIAG_FILE
		return $FAILURE
	fi
}

#Update OTA environment
updateEnv(){
	# No need to update environment for incremental update. Alrady taken care in cgupdate
	if [ $6 -eq 0 ]; then
		return $SUCCESS
	fi 
	retValue=$FAILURE
        cgupdate -se $1 $2 $3 $4 $5 $6 > $OTA_LOG_FILE
	cat $OTA_LOG_FILE >> $OTA_DIAG_FILE

        OTA_PERFORM_OUTPUT_STRING1=`awk "/$OTA_ENV_UPDATE_REF_STRING/{print}" $OTA_LOG_FILE`

        if [ -n "$OTA_PERFORM_OUTPUT_STRING1" ] 
        then
                retValue=$SUCCESS
                echo "OTA environment updated successfully"
                echo "OTA environment updated successfully" >> $OTA_DIAG_FILE
        else
                retValue=$FAILURE
                echo "Error updating OTA environment"
                echo "Error updating OTA environment" >> $OTA_DIAG_FILE
        fi
	rm $OTA_LOG_FILE
	return $retValue
}

#Check for OTA image
echo "Verfying OTA image..."
echo "Verfying OTA image..." >> $OTA_DIAG_FILE

verifyOTAImage $1
if [ $? -ne $SUCCESS ]; then
	echo "Updateding env ${zb_status} ${ble_status} ${can_status} ${rf_status} ${ebyte_status} ${full_update}" >> $OTA_DIAG_FILE
	updateEnv ${zb_status} ${ble_status} ${can_status} ${rf_status} ${ebyte_status} ${full_update}
	exit 1
fi

echo "Performing OTA..."
echo "Performing OTA..." >> $OTA_DIAG_FILE

performOTA $1
if [ $? -ne $SUCCESS ]; then
	echo "updateEnv ${zb_status} ${ble_status} ${can_status} ${rf_status} ${ebyte_status} ${full_update}" >> $OTA_DIAG_FILE
	updateEnv ${zb_status} ${ble_status} ${can_status} ${rf_status} ${ebyte_status} ${full_update}
	exit 1
fi

echo "OTA Done!!!"
echo "OTA Done!!!" >> $OTA_DIAG_FILE

updateEnv ${zb_status} ${ble_status} ${can_status} ${rf_status} ${ebyte_status} ${full_update}
echo "updateEnv ${zb_status} ${ble_status} ${can_status} ${rf_status} ${ebyte_status} ${full_update}" >> $OTA_DIAG_FILE
exit 0
