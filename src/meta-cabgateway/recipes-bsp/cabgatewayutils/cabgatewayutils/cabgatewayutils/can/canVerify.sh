###### CAN test verify script #########
#!/bin/sh

count=`grep "11 12 13 14 15 16 17 18" -rn /cabApp/can/canLog | wc -l`
if [ $count -eq 4 ];
then
	echo -e "CAN test PASS\n\n"
	exit 0;
else
	echo -e "CAN test FAIL\n\n"
	exit 1;
fi
