#### CAN  other board script #########
#!/bin/sh

modprobe can
modprobe can-dev
modprobe can-raw
modprobe flexcan

ip link set can0 up type can bitrate 125000
ifconfig

canecho can0 &
