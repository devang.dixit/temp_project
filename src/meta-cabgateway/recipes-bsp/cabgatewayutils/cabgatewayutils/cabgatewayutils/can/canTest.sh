###### CAN main board script #########
#!/bin/sh

modprobe can
modprobe can-dev
modprobe can-raw
modprobe flexcan

ip link set can0 up type can bitrate 125000
ifconfig
rm /cabApp/can/canLog
candump can0 >> /cabApp/can/canLog &
sleep 10
echo -e "\n\n====== Can Test =====\n\n"
cansend can0 --loop 1 -i 500 0x11 0x12 0x13 0x14 0x15 0x16 0x17 0x18
sleep 2
cansend can0 --loop 1 -i 500 0x11 0x12 0x13 0x14 0x15 0x16 0x17 0x18
sleep 2
echo -e "\n\n====== Can Test Done =====\n\n"
