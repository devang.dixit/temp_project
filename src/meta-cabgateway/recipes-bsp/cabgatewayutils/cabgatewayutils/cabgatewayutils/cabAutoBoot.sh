#!/bin/sh -e
### BEGIN INIT INFO
# Provides:          CAB Gateway
# Short-Description: CAB Gateway Application Auto Start on Boot-up .
### END INIT INFO

serviceFile="/lib/systemd/system/remoteDiag.service"
if [ -f $serviceFile ]; then

	activeStatus=`systemctl is-active --quiet remoteDiag.service`
	enabledStatus=`systemctl is-enabled --quiet remoteDiag.service`

	if [ $enabledStatus="enabled" -a $activeStatus="active" ]; then
		echo "Service is loaded and active"
		exit 0
	fi
fi

case "$1" in
start)
        echo -e "\n *******************************************************" >> /dev/console
        echo -e "\n *********** Verifying firmware recovery ***************" >> /dev/console
        echo -e "\n *******************************************************" >> /dev/console
        retValue=$FAILURE
	OTA_LOG_FILE="recoveryStatus.txt"
	INCR_OTA_RECOVERY_REF_STRING="Incremental OTA recovery succeed"
        cgupdate -r > $OTA_LOG_FILE
        OTA_PERFORM_OUTPUT_STRING1=`awk "/$INCR_OTA_RECOVERY_REF_STRING/{print}" $OTA_LOG_FILE`
        rm $OTA_LOG_FILE
        if [ -n "$OTA_PERFORM_OUTPUT_STRING1" ]
        then
                echo "Firmware recovery verification succeed !" >> /dev/console
        else
                echo "Firmware recovery verification failed ! Rebooting cabgateway ..." >> /dev/console
		reboot
		sleep 60
        fi
        echo -e "\n *******************************************************" >> /dev/console
        echo -e "\n *********** STARTING CAB SERVICES ***************" >> /dev/console
        echo -e "\n *******************************************************" >> /dev/console

        #/cabApp/bbtemp.sh &
        echo 1000000 > /proc/sys/fs/mqueue/queues_max
        ulimit -q 1000000000
        ulimit -n 1000000
        ulimit -a
        /cabApp/remoteDiagApp 1>/dev/console 2>/dev/console

        echo -e "\n *******************************************************"
        ;;

stop)
        echo -e "\n *******************************************************"
        echo -e "\n *********** STOP CAB SERVICES *******************"
        echo -e "\n *******************************************************"

        ;;

force-reload|restart)
                    $0 stop
                    sleep 5
                    $0 start
        ;;

*)
        echo -e "\nUsage: /etc/init.d/cabAutoBoot.sh {start|stop}\n"
        exit 1
        ;;
esac

exit 0

