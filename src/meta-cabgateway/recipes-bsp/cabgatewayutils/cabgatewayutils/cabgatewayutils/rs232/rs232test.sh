#!/bin/sh

refLine="=== Cabgateway RS232 test PASS ====="

stty -F /dev/ttymxc1 9600 clocal cread cs8 -cstopb -parenb -echo

echo -e $refLine > /dev/ttymxc1

while read -r line < /dev/ttymxc1; do
  if [ "$line" == "$refLine" ];
  then
      echo -e "RS232 Test pass"
      exit 0;
  else
      echo -e "RS232 Test Fail"
      exit 1;
  fi  
done
