#!/bin/sh

EBYTE_BOOT_PIN=7
EBYTE_RST_PIN=34
EBYTE_PWR_PIN=46

if [ -e /cabApp/ebyte/cc2538-bsl.py ];then
	if [ -e /cabApp/ebyte/cabGateway.bin ];then
		# Export the GPIO7 for Boot pin
		if [ ! -e /sys/class/gpio/gpio$EBYTE_BOOT_PIN ];then
			echo $EBYTE_BOOT_PIN > /sys/class/gpio/export
			echo "out" > /sys/class/gpio/gpio$EBYTE_BOOT_PIN/direction
		fi

		if [ ! -e /sys/class/gpio/gpio$EBYTE_RST_PIN ];then
			echo $EBYTE_RST_PIN > /sys/class/gpio/export
			echo "out" > /sys/class/gpio/gpio$EBYTE_RST_PIN/direction
		fi

		if [ ! -e /sys/class/gpio/gpio$EBYTE_PWR_PIN ];then
			echo $EBYTE_PWR_PIN > /sys/class/gpio/export
			echo "out" > /sys/class/gpio/gpio$EBYTE_PWR_PIN/direction
		fi

		# Power on the Ebyte module
		echo 1 > /sys/class/gpio/gpio$EBYTE_PWR_PIN/value

		# Set 0 to instruct CC1310 to get into serial bootloader mode
		echo 0 > /sys/class/gpio/gpio$EBYTE_BOOT_PIN/value

		# Reset the Ebyte module
		echo 0 > /sys/class/gpio/gpio$EBYTE_RST_PIN/value
		echo 1 > /sys/class/gpio/gpio$EBYTE_RST_PIN/value

		# Call the python utility to write the Ebyte FW
		python /cabApp/ebyte/cc2538-bsl.py -p /dev/ttymxc2 -v -e -w /cabApp/ebyte/cabGateway.bin

		# Save the last return value
		returnval=$?

		# Set 1 on boot pin
		echo 1 > /sys/class/gpio/gpio$EBYTE_BOOT_PIN/value

		# unexport the boot pin gpio
		echo $EBYTE_BOOT_PIN > /sys/class/gpio/unexport

		# Reset the Ebyte module
		echo 0 > /sys/class/gpio/gpio$EBYTE_RST_PIN/value
		echo 1 > /sys/class/gpio/gpio$EBYTE_RST_PIN/value

		# unexport the power pin gpio
		echo $EBYTE_PWR_PIN > /sys/class/gpio/unexport

		if [[ "$returnval" == "0" ]]
		then
			echo "Success: Ebyte FW flashed"
			exit 0
		else
			echo "Failure: Ebyte flahser python utility failed"
			echo "Ebyte OTA FAIL"
			exit 1
		fi
	else
		echo "Ebyte FW binary not available not present in rootfs"
		echo "Ebyte OTA FAIL"
		exit 1
	fi
else
	echo "Ebyte flasher python utility not present in rootfs"
	echo "Ebyte OTA FAIL"
	exit 1
fi