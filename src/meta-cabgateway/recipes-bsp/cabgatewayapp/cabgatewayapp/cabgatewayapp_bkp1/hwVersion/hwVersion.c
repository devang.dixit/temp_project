#include "hwVersion.h"


static const HWVersionLookUp_t HWVersionTable[] = {
    /*Ver*/    /*Ver str*/    /*T Shutof*/ /*CC OTA*/
    /*A*/{      "0.0.0.0",      false,      false},        /* Proto batch */
    /*B*/{      "0.0.0.1",      false,      false},        /* Proto batch */
    /*C*/{      "0.0.1.0",      true,       true },
    /*D*/{      "0.0.1.1",      true,       false},
    /*E*/{      "0.1.0.0",      true,       true },        /* Rev1.5 OTA + Thermal Shutoff + CC1310 OTA */
    /*F*/{      "0.1.0.1",      true,       false},        /* Thermal Shutoff */
    /*G*/{      "0.1.1.0",      true,       true },
    /*H*/{      "0.1.1.1",      true,       false},
    /*I*/{      "1.0.0.0",      true,       false},        /* eSIM + Thermal Shutoff */
    /*J*/{      "1.0.0.1",      true,       false},        /* eSIM + Thermal Shutoff */
    /*K*/{      "1.0.1.0",      true,       true },
    /*L*/{      "1.0.1.1",      true,       false},
    /*M*/{      "1.1.0.0",      true,       false},        /* (1000 Production rework) CC1310 + Thermal Shutoff + Without CC1310 OTA support */
    /*N*/{      "1.1.0.1",      true,       false},
    /*O*/{      "1.1.1.0",      true,       true },
    /*P*/{      "1.1.1.1",      true,       false}
};


returnCode_e getHardwareVersion(char *hwVersion) 
{
	FUNC_ENTRY 
	uint8_t tmp[4] = {0};
	returnCode_e retCode = GEN_SUCCESS;

	if(!hwVersion)
	{
		CAB_DBG(CAB_GW_ERR , "Invalid argument");
		return GEN_NULL_POINTING_ERROR;
	}
	retCode = gpioInit(HW_VER_GPIO_0, IN);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "gpioInit() fail with error %d", retCode);
		return retCode;
	}

	retCode = gpioRead(HW_VER_GPIO_0, &tmp[0]);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "gpioRead() fail with error %d", retCode);
		return retCode;
	}

	retCode = gpioDeInit(HW_VER_GPIO_0);
	if(retCode != GEN_SUCCESS) 
	{
		CAB_DBG(CAB_GW_ERR, "gpioDeInit() fail with error %d", retCode);
		return retCode;
	}

	retCode = gpioInit(HW_VER_GPIO_1, IN); 
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "gpioInit() fail with error %d", retCode);
		return retCode;
	} 

	retCode = gpioRead(HW_VER_GPIO_1, &tmp[1]);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "gpioRead() fail with error %d", retCode);
		return retCode;
	}

	retCode = gpioDeInit(HW_VER_GPIO_1);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "gpioDeInit() fail with error %d", retCode);
		return retCode;
	}

	retCode = gpioInit(HW_VER_GPIO_2, IN);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "gpioInit() fail with error %d", retCode);
		return retCode;
	}

	retCode = gpioRead(HW_VER_GPIO_2, &tmp[2]);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "gpioRead() fail with error %d", retCode);
		return retCode;
	}

	retCode = gpioDeInit(HW_VER_GPIO_2); 
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "gpioDeInit() fail with error %d", retCode);
		return retCode;
	}

	retCode = gpioInit(HW_VER_GPIO_3, IN);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "gpioInit() fail with error %d", retCode);
		return retCode;
	}

	retCode = gpioRead(HW_VER_GPIO_3, &tmp[3]);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "gpioRead() fail with error %d", retCode);
		return retCode;
	}

	retCode = gpioDeInit(HW_VER_GPIO_3);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "gpioDeInit() fail with error %d", retCode);
		return retCode;
	}

	sprintf(hwVersion, "%u.%u.%u.%u", tmp[0], tmp[1], tmp[2], tmp[3]); 

	FUNC_EXIT                       
	return retCode;
}

/******************************************************************
 *@brief  (It is used get the sensor vendor type)
 *
 *@param[IN] None
 *@param[OUT] sensorVendorType_e* (Indicates pointer to sensorVendorType_e)
 *
 *@return returnCode_e (It returns the return code GEN_SUCCESS or error code)
 *********************************************************************/
returnCode_e getSensorVendorType(sensorVendorType_e *sensorMake)
{
	FUNC_ENTRY
	uint8_t val = 0;
	returnCode_e retCode = GEN_SUCCESS;

	retCode = gpioInit(HW_VER_GPIO_3, IN);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "gpioInit() fail with error %d", retCode);
		return retCode;
	}

	retCode = gpioRead(HW_VER_GPIO_3, &val);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "gpioRead() fail with error %d", retCode);
		return retCode;
	}

	retCode = gpioDeInit(HW_VER_GPIO_3);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "gpioDeInit() fail with error %d", retCode);
		return retCode;
	}
	*sensorMake = (sensorVendorType_e)val;

	FUNC_EXIT
	return retCode;
}


/******************************************************************
 *@brief  (It is used check that thermal shutoff capacbility is availalbe for the HW version or not)
 *
 *@param[IN] None
 *@param[OUT] bool* (Indicates pointer to bool for availibility)
 *
 *@return returnCode_e (It returns the return code GEN_SUCCESS or error code)
 *********************************************************************/
returnCode_e isThermalShutOffAvailable(bool *val)
{
    char hardwareVersion[MAX_LEN_HARDWARE_VERSION];
    returnCode_e retCode = GEN_SUCCESS;
    bool retVal = false;

    retCode = getHardwareVersion(hardwareVersion);                               
    if (retCode != GEN_SUCCESS) {                                                
        CAB_DBG(CAB_GW_ERR, "get hardware version API failed %d", retCode);
        return GEN_API_FAIL;                                                 
    }

    for (uint8_t i=0; i < sizeof(HWVersionTable)/sizeof(HWVersionTable[0]); i++) {
        if (strncmp(hardwareVersion, HWVersionTable[i].hardwareVersion, sizeof(hardwareVersion)) == 0) {
            retVal = HWVersionTable[i].isThermalShutOffAvail;
            break;
        }
    }

    *(val) = retVal;

    return retCode;
}


/******************************************************************
 *@brief  (It is used check that CC1310 OTA capacbility is availalbe for the HW version or not)
 *
 *@param[IN] None
 *@param[OUT] bool* (Indicates pointer to bool for availibility)
 *
 *@return returnCode_e (It returns the return code GEN_SUCCESS or error code)
 *********************************************************************/
returnCode_e isCC1310OTAAvailable(bool *val)
{
    char hardwareVersion[MAX_LEN_HARDWARE_VERSION];
    returnCode_e retCode = GEN_SUCCESS;
    bool retVal = false;

    retCode = getHardwareVersion(hardwareVersion);                               
    if (retCode != GEN_SUCCESS) {                                                
        CAB_DBG(CAB_GW_ERR, "get hardware version API failed %d", retCode);
        return GEN_API_FAIL;                                                 
    }

    for (uint8_t i=0; i < sizeof(HWVersionTable)/sizeof(HWVersionTable[0]); i++) {
        if (strncmp(hardwareVersion, HWVersionTable[i].hardwareVersion, sizeof(hardwareVersion)) == 0) {
            retVal = HWVersionTable[i].isCC_OTA_Avail;
            break;
        }
    }

    *(val) = retVal;

    return retCode;
}