/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file  diagnostic.h
 * @brief (Header file for diagnostic module)
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Sep/13/2018, VT , First Draft
 **************************************************************/

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include "commonData.h"

/****************************************
 ************* DEFINES ******************
 ****************************************/
#define MAX_GW_ID_LEN           (64)        /**< Gateway ID length. */
#define MAX_HW_VER_LEN          (8)         /**< Hardware version length. */
#define MAX_SW_VER_LEN          (10)         /**< Software version length. */
#define MAX_MODEM_IMEI_LEN      (15)        /**< Modem IMEI length. */
#define MAX_SIM_ICCID_LEN       (64)        /**< SIM ICCID length. */

/****************************************
 ************* ENUMS ********************
 ****************************************/
/**
 * @enum sysDiagInfoParam
 *
 *          Enumeration for diffrent type of parameter which is used to diagnostic gateway
 *  information.
 */
typedef enum sysDiagInfoParam {
    SYS_GATEWAY_UNIQUE_ID = 0,      /**< Unique gateway ID. */
    SYS_HARDWARE_VERSION,           /**< Hardware version. */
    SYS_SOFTWARE_VERSION,           /**< Software version. */
    SYS_MODEM_IMEI,                 /**< Modem IMEI number. */
    SYS_SIM_ICCID,                  /**< SIM ICCID. */
    SYS_CLOUD_URI,                  /**< cloud uri. */
    SYS_MAX_DIAG_INFO_PARAM,        /**< No of system diagnostic information parameter. */
} sysDiagInfoParam_e;

/****************************************
 ************ STRUCTURES ****************
 ****************************************/
/**
 * @struct sysDiagInfo
 * @brief  gateway diagnostic information structure
 *
 *      This strucure to be used for diagnostic gateway information in mobile over BLE.
 **/
typedef struct sysDiagInfo {
    char gatewayId[MAX_GW_ID_LEN];           /**< Gateway ID information. */
    char hwVersion[MAX_HW_VER_LEN];          /**< Hardware version information. */
    char swVersion[MAX_SW_VER_LEN];          /**< Software version information. */
    char modemIMEI[MAX_MODEM_IMEI_LEN + 1];      /**< Modem IMEI information. */
    char simICCID[MAX_SIM_ICCID_LEN];        /**< SIM ICCID information. */
    char cloudUrl[MAX_LEN_CLOUD_URI];        /**< Cloud url information. */
} sysDiagInfo_t;

/****************************************
 ******** GLOBAL VARIABLES **************
 ****************************************/

/****************************************
 ******** EXTERN VARIABLES **************
 ****************************************/

/****************************************
 ************* CALLBACKS ****************
 ****************************************/
/** Callback to inform M&T module to update diagnostic information */
typedef void (*notifyDiagGetEvent)(void);

/****************************************
 ********* FUNCTION PROTOTYPES **********
 ****************************************/
/******************************************************************
 *@brief (This API is used to initialize diagnostic module)
 *
 *@param[IN] notifyDiagGetEvent (Callback to notify M&T module to update diagnostic information.)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e initDiagModule(notifyDiagGetEvent);

/******************************************************************
 *@brief (This API is used to deinitialize diagnostic module)
 *
 *@param[IN] void (No arguments passed)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e deInitDiagModule(void);

/******************************************************************
 *@brief (This API is used to set GPS diagnostic data)
 *
 *@param[IN] gpsDiagInfo_t * (pointer to structure of gps diagnostic information)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e diagSetGpsData(gpsDiagInfo_t *);

/******************************************************************
 *@brief (This API is used to set LTE diagnostic data)
 *
 *@param[IN] lteDiagInfo_t * (pointer to structure of lte diagnostic information)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e diagSetLteData(lteDiagInfo_t *);

/******************************************************************
 *@brief (This API is used to set TPMS diagnostic data)
 *
 *@param[IN] tpmsDiagDataInfo_t * (pointer to structure of TPMS diagnostic information)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e diagSetTpmsData(tpmsDiagDataInfo_t *);

/******************************************************************
 *@brief (This API is used to set gateway diagnostic information)
 *
 *@param[IN] sysDiagInfoParam_e (Type of gateway diagnostic information)
 *@param[IN] void * (gateway diagnostic information data)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e diagSetGatewayInfo(sysDiagInfoParam_e , void *);

/******************************************************************
 *@brief (This API is used to get diagnostic information)
 *
 *@param[IN] commandCode_e (Type of diagnostic information)
 *@param[OUT] void * (Pointer to diagnostic information structure)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e getDiagInfo(commandCode_e , void *);
