/***************************************************************
 * Copyright(c) <2018>, Volansys
 * Description
 * @file : monitorAndTelemetry.c
 * @brief : This file provides the core buisness logic for
 *			Cab gateway
 * @Author     -
 * History
 *
 * Aug/29/2018, VT , First draft
 **************************************************************/

#ifndef __monitorAndTelemetry_h__
#define __monitorAndTelemetry_h__

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include <fcntl.h>
#include <mqueue.h>
#include <pthread.h>
#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include "error.h"
#include "watchdog.h"

#ifdef CABAPPA7
#include "hwVersion.h"
#endif

/****************************************
 ************ DEFINES ******************
 ****************************************/
//#define SIMULATE_TIMER

/* Timer module macros */
#ifndef SIMULATE_TIMER
#define ONE_MINUTE 		    	(60)    /* One minute seconds macro */
#define ONE_HOUR 		    	(3600)  /* One hour seconds macro */
#define TIMER_RESOLUTION_SEC        	(60)  /* Minimum resolution time for timer. */
#else
#define ONE_MINUTE 		    	(20)    /* One minute seconds macro */
#define ONE_HOUR 		    	(3600)  /* One hour seconds macro */
#define TIMER_RESOLUTION_SEC        	(20)  /* Minimum resolution time for timer. */
#endif


	      /*     *****  NOTE *******   */
	/* For accurate timing, please take care that 
           timer values is devisible resolution time.
           Timer count should be integer.*/

#ifndef SIMULATE_TIMER
#define XMISSION_INT_MOVING_TIM_COUNT   (3)
#define XMISSION_INT_PARKED_TIM_COUNT	(1)
#define SIM_DET_CHK_TIME_INTERVAL       (1 * ONE_HOUR)
#define SIM_DET_CHK_COUNT		(SIM_DET_CHK_TIME_INTERVAL/TIMER_RESOLUTION_SEC)
#define TIRE_LEAK_MEAS_TIME_INTERVAL    (1 * ONE_MINUTE)
#define TIRE_LEAK_MEAS_TIME_COUNT	(TIRE_LEAK_MEAS_TIME_INTERVAL/TIMER_RESOLUTION_SEC)
#define WATCHDOG_TIMER_INTERVAL		(47 * ONE_MINUTE)
#define WATCHDOG_TIMER_COUNT		(WATCHDOG_TIMER_INTERVAL/TIMER_RESOLUTION_SEC)
#define XMISSION_MOVING_TIMER_SEC 	(5 * ONE_MINUTE)  /* transmission timer in moving state */
#define XMISSION_MOVING_TIMER_COUNT  	(XMISSION_MOVING_TIMER_SEC/TIMER_RESOLUTION_SEC)	
#define XMISSION_PARKED_TIMER_SEC 	(48 * ONE_HOUR)  /* transmission timer in parked state */
#define XMISSION_PARKED_TIMER_COUNT  	(XMISSION_PARKED_TIMER_SEC/TIMER_RESOLUTION_SEC)	
#define XMISSION_ALERT_TIMER_SEC 	(5 * ONE_MINUTE)  /* transmission timer in alert state */
#define XMISSION_ALERT_TIMER_COUNT  	(XMISSION_ALERT_TIMER_SEC/TIMER_RESOLUTION_SEC)	
#define INITIAL_OTA_TIMER_SEC 		(1 * ONE_HOUR)  /* initial ota timer when device is on vehical power */
#define INITIAL_OTA_TIMER_COUNT  	(INITIAL_OTA_TIMER_SEC/TIMER_RESOLUTION_SEC)	
#define INCREMENTAL_OTA_TIMER_SEC 	(10 * ONE_MINUTE)  /* initial ota timer when device is on vehical power */
#define INCREMENTAL_OTA_TIMER_COUNT  	(INCREMENTAL_OTA_TIMER_SEC/TIMER_RESOLUTION_SEC)	
#define BLE_OTA_TOUT_TIMER_SEC 		(30 * ONE_MINUTE)   /* OTA timer reload value when BLE is connected  */
#define BLE_OTA_TOUT_TIMER_COUNT  	(BLE_OTA_TOUT_TIMER_SEC/TIMER_RESOLUTION_SEC)	
#define OTA_TIMER_SEC 		    	(1 * ONE_HOUR)  /* regular ota timer */
#define OTA_TIMER_COUNT  	        (OTA_TIMER_SEC/TIMER_RESOLUTION_SEC)	
#define BATT_CHECK_TIMER_SEC    	(15 * ONE_MINUTE)  /* battery check timer value */
#define BATT_CHECK_TIMER_COUNT	        (BATT_CHECK_TIMER_SEC/TIMER_RESOLUTION_SEC)	
#define LTE_CHECK_TIMER_SEC 		(15 * ONE_MINUTE)  /* LTE check time timer */
#define LTE_CHECK_TIMER_COUNT  	        (LTE_CHECK_TIMER_SEC/TIMER_RESOLUTION_SEC)	
#define DIAG_CHECK_TIMER_SEC 		(15 * ONE_MINUTE)  /* diag check timer */
#define DIAG_CHECK_TIMER_COUNT  	(DIAG_CHECK_TIMER_SEC/TIMER_RESOLUTION_SEC)	
#define ZIP_PREPARE_TIMER_SEC 		(15 * ONE_MINUTE)  /* zip prepare timer */
#define ZIP_PREPARE_TIMER_COUNT  	(ZIP_PREPARE_TIMER_SEC/TIMER_RESOLUTION_SEC)	
#else
#define XMISSION_INT_MOVING_TIM_COUNT   (3)
#define XMISSION_INT_PARKED_TIM_COUNT	(1)
#define TIRE_LEAK_MEAS_TIME_INTERVAL    (1 * ONE_MINUTE)
#define TIRE_LEAK_MEAS_TIME_COUNT	(TIRE_LEAK_MEAS_TIME_INTERVAL/TIMER_RESOLUTION_SEC)
#define XMISSION_MOVING_TIMER_SEC 	(3 * ONE_MINUTE)  /* transmission timer in moving state */
#define XMISSION_MOVING_TIMER_COUNT  	(XMISSION_MOVING_TIMER_SEC/TIMER_RESOLUTION_SEC)	
#define XMISSION_PARKED_TIMER_SEC 	(15 * ONE_MINUTE)  /* transmission timer in parked state */
#define XMISSION_PARKED_TIMER_COUNT  	(XMISSION_PARKED_TIMER_SEC/TIMER_RESOLUTION_SEC)	
#define XMISSION_ALERT_TIMER_SEC 	(15 * ONE_MINUTE)  /* transmission timer in alert state */
#define XMISSION_ALERT_TIMER_COUNT  	(XMISSION_ALERT_TIMER_SEC/TIMER_RESOLUTION_SEC)	
#define INITIAL_OTA_TIMER_SEC 		(2 * ONE_MINUTE)  /* initial ota timer when device is on vehical power */
#define INITIAL_OTA_TIMER_COUNT  	(INITIAL_OTA_TIMER_SEC/TIMER_RESOLUTION_SEC)	
#define BLE_OTA_TOUT_TIMER_SEC 		(3 * ONE_MINUTE)  /* OTA timer reload value when BLE is connected  */
#define BLE_OTA_TOUT_TIMER_COUNT  	(BLE_OTA_TOUT_TIMER_SEC/TIMER_RESOLUTION_SEC)	
#define OTA_TIMER_SEC 		    	(5 * ONE_MINUTE)  /* regular ota timer */
#define OTA_TIMER_COUNT  	        (OTA_TIMER_SEC/TIMER_RESOLUTION_SEC)	
#define BATT_CHECK_TIMER_SEC    	(2 * ONE_MINUTE)  /* battery check timer value */
#define BATT_CHECK_TIMER_COUNT	        (BATT_CHECK_TIMER_SEC/TIMER_RESOLUTION_SEC)	
#define LTE_CHECK_TIMER_SEC 		(2 * ONE_MINUTE)  /* LTE check time timer */
#define LTE_CHECK_TIMER_COUNT  	        (LTE_CHECK_TIMER_SEC/TIMER_RESOLUTION_SEC)	
#define DIAG_CHECK_TIMER_SEC 		(4 * ONE_MINUTE)  /* diag check timer */
#define DIAG_CHECK_TIMER_COUNT  	(DIAG_CHECK_TIMER_SEC/TIMER_RESOLUTION_SEC)	
#define ZIP_PREPARE_TIMER_SEC 		(5 * ONE_MINUTE)  /* zip prepare timer */
#define ZIP_PREPARE_TIMER_COUNT  	(ZIP_PREPARE_TIMER_SEC/TIMER_RESOLUTION_SEC)	
#endif

#define PUSH_BTN_GPIO (64)

#define SELF_DIAG_TEST_COUNT (9)

#define CC1310_RSSI_THRESHOLD                   (-80)
#define XBR_RSSI_THRESHOLD                      (20)


#define EVENT_QUEUE_NAME  "/mnt_queueEvent"
#define TPMS_QUEUE_NAME  "/mnt_queueTPMS"
#define CONFIG_QUEUE_NAME "/mnt_queueConfig"

/* System wide events can increase for future data */
#define EVENT_MSGQ_MAX_SIZE	(200)
/* Keeping more size to avoid event missing during 
   application of initial state processing. */
#define CONFIG_MSGQ_MAX_SIZE	(200)
/* Keeping buffer more to avoid any data missing due to 
   future updates on alert processing*/
#define TPMS_MSGQ_MAX_SIZE	(500)

/* event manager message queue timeout */
#define MQ_SEND_EVMGR_TOUT_SEC		(3 * ONE_MINUTE)
/* tpms thread message queue timeout */
#define MQ_SEND_TPMS_TOUT_SEC		(1 * ONE_MINUTE)
/* cfg manager message queue timeout */
#define MQ_SEND_CFGMGR_TOUT_SEC		(2 * ONE_MINUTE)


#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))
#define MAX_SDC_ZIP			(1500)
/* 5 PSI leak threshold */
#define TIRE_LEAK_THRESHOLD_DIFF        (10.0) 
#define TIRE_LEAK_MEAS_TOTAL_TIME       (15 * ONE_MINUTE)
#define TIRE_LEAK_MAVG_WINDOW_SIZE      (TIRE_LEAK_MEAS_TOTAL_TIME/TIRE_LEAK_MEAS_TIME_INTERVAL)
/* Keep Alive check timer*/
#define SUB1_KEEPALIVE_CHECK_COUNTER	(1)
#define BLE_KEEPALIVE_CHECK_COUNTER	(1)
#define BLE_KEEPALIVE_FAIL_COUNTER	(3)
/****************************************
 ************ ENUMS ******************
 ****************************************/
typedef enum {
	ZIP_DATA = 0,
	XMIT_DATA = 1,
	MAX_XMIT_EVENT
} xMitEventDataEnum_e;

typedef enum {
	OTA_THREAD_RUNNING = 0,
	OTA_NOT_RUNNING,
	MAX_OTA_STATES
} otaMgrState_e;

typedef enum {
	BLE_CONNECTED_MNT_FLAG = 0,
	BLE_DISCONNECTED_MNT_FLAG,
	MAX_BLE_STATES
} bleState_e;

typedef enum {
	OTA_NO_PROGRESS = 0,
	OTA_CHECK_DONE,
	OTA_DOWNLOAD_DONE,
	MAX_OTA_PROGRESS_STATES
} otaProgressState_e;

/****************************************
 ************ FUNCTIONS ******************
 ****************************************/

/*********************************************************************
 *@brief          : Init Monitoring and Telemetry module
 *
 *@return         : CG_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e monTelemetryInit();

/*********************************************************************
 *@brief          : DeInit Monitoring and Telemetry module
 *
 *@return         : CG_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e monTelemetryDeInit();

/*********************************************************************
 *@brief          : remoteDiagapp response message handler callback
 *
 *@return         : None
 *********************************************************************/
void rDiagModuleResHandlerCallback(void * );

#endif // __monitorAndTelemetry_h__


