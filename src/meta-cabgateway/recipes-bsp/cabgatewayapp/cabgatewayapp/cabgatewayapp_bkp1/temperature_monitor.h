 #include <stdio.h>
 #include <stdlib.h>
 #include <pthread.h>
 #include <unistd.h>
 #include <semaphore.h>

#define MPU_TEMPERATURE                                 "/sys/class/thermal/thermal_zone0/temp"                   
#define MPU_THRESHOLD                                   "/cabApp/threshold.txt"                                   
#define CFG_TEMP_SHUTOFF_FILE                           "/media/userdata/thermalShutOFF/thermalShutoffconfig.json"
#define THERMAL_DATA_RECORD_FILE                        "/media/userdata/thermalShutOFF/thermalDataRecord.txt"    
#define TEMP_SHUTOFF_DIR                                "/media/userdata/thermalShutOFF"                          
#define TOTAL_RANGE                                     4                                                         
#define NORMAL_TEMP_FOR_CLOUD                           66                                                        
#define CMD_BUF_SIZE					5

typedef struct temp_shutoff_record_data {
	int alarm_count;                 
	int detected_Range;              
	int last_sleep_time;             
	bool OTP_Bit;                    
} temp_shutoff_record_data_t;            

typedef struct configTemperatureData {   
	int range;                       
	int lower_accel_limit;           
	int upper_accel_limit;           
	int normal_sleep_time;           
	int abv_thr_sleep_time;          
	int alarm_count;                 
} configTemperatureData_t;               

