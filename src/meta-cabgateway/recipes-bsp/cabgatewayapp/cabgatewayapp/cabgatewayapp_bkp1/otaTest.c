#include <stdint.h>
#include <openssl/md5.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <dirent.h>
#include "otaModule.h"
#include "cloud.h"

/****************************************
 ******** STATIC VARIABLES **************
 ****************************************/

static pthread_mutex_t g_otaInfoFileLock = PTHREAD_MUTEX_INITIALIZER;

static returnCode_e readOTASummary(otaDetails_t *otaDetails)
{
	FUNC_ENTRY

	int summaryFile;
	uint32_t ret;

	/*Input argument validation*/
	if(otaDetails == NULL) {
		CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
		FUNC_EXIT
		return GEN_NULL_POINTING_ERROR;
	}

	//opening event summary file for read
	pthread_mutex_lock(&g_otaInfoFileLock);
	summaryFile = open(OTA_FILE_PATH, O_RDONLY, 0644);
	if(summaryFile == -1) {
		CAB_DBG(CAB_GW_ERR, "%s file open error.", OTA_FILE_PATH);
		pthread_mutex_unlock(&g_otaInfoFileLock);
		FUNC_EXIT
		return FILE_OPEN_FAIL;
	}

	CAB_DBG(CAB_GW_DEBUG, "reading summary file");
	//reading event summary structure data from file
	ret = read(summaryFile, otaDetails, sizeof(otaDetails_t));
	if(ret == -1) {
		CAB_DBG(CAB_GW_ERR, "%s file read error.", OTA_FILE_PATH);
		close(summaryFile);
		pthread_mutex_unlock(&g_otaInfoFileLock);
		FUNC_EXIT
		return FILE_READ_ERROR;
	}
	//close event summary file
	close(summaryFile);
	pthread_mutex_unlock(&g_otaInfoFileLock);

	FUNC_EXIT
	return GEN_SUCCESS;
}

static returnCode_e writeOTASummary(otaDetails_t *otaDetails)
{
	FUNC_ENTRY

		int outFile;                               //file descriptor
	uint32_t ret;

	/*Input argument validation*/
	if(otaDetails == NULL) {
		CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
		FUNC_EXIT
		return GEN_NULL_POINTING_ERROR;
	}

	//opening event summary file for write
	pthread_mutex_lock(&g_otaInfoFileLock);
	outFile = open(OTA_FILE_PATH, O_WRONLY | O_CREAT, 0644);
	if(outFile == -1) {
		CAB_DBG(CAB_GW_ERR, "%s file open error.", OTA_FILE_PATH);
		pthread_mutex_unlock(&g_otaInfoFileLock);
		FUNC_EXIT
		return FILE_OPEN_FAIL;
	}

	CAB_DBG(CAB_GW_DEBUG, "writing summary file");
	//writing event summery structure data to file
	ret = write(outFile, otaDetails, sizeof(otaDetails_t));
	if(ret == -1) {
		CAB_DBG(CAB_GW_ERR, "%s file read error.", OTA_FILE_PATH);
		close(outFile);
		pthread_mutex_unlock(&g_otaInfoFileLock);
		FUNC_EXIT
		return FILE_WRITE_ERROR;
	}

	fsync(outFile);
	//close event summary file
	close(outFile);
	pthread_mutex_unlock(&g_otaInfoFileLock);

	FUNC_EXIT
	return GEN_SUCCESS;
}

void generateOTASummary(void) {
	otaDetails_t otaDetails;
	otaDetails.state = MAX_OTA_STATE;

	printf("Enter Current s/w version\r\n");
	scanf("%s", otaDetails.otaInfo.version);

	printf("Enter updated s/w version\r\n");
	scanf("%s", otaDetails.otaInfo.updateVersion);

	printf("Enter OTA file name\r\n");
	scanf("%s", otaDetails.otaInfo.filename);

	printf("Enter md5sum\r\n");
	scanf("%s", otaDetails.otaInfo.md5sum);

//	strcpy(otaDetails.otaInfo.otaUrl,"www.google.com");
	strcpy(otaDetails.otaInfo.storagePath,"/cabApp/ota/");

	writeOTASummary(&otaDetails);
	
}

main() {
	otaDetails_t otaDetails;
	generateOTASummary();
	readOTASummary(&otaDetails);
	
	CAB_DBG(CAB_GW_DEBUG, "Current SW Version : %s", otaDetails.otaInfo.version);
	CAB_DBG(CAB_GW_DEBUG, "Updated Version : %s", otaDetails.otaInfo.updateVersion);
	CAB_DBG(CAB_GW_DEBUG, "OTA File Path : %s", otaDetails.otaInfo.storagePath);
	CAB_DBG(CAB_GW_DEBUG, "OTA File Name : %s", otaDetails.otaInfo.filename);
	CAB_DBG(CAB_GW_DEBUG, "OTA File MD5sum : %s", otaDetails.otaInfo.md5sum);
}
