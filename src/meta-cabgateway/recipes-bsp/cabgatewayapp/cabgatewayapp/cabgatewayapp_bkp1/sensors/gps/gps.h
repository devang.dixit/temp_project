/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : gps.h
 * @brief : Header file for GPS
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Aug/28/2018, VR , Stub APIs addition
 * Aug/27/2018, VT , First Draft
 **************************************************************/

#ifndef __GPS_H
#define __GPS_H
#include "commonData.h"

extern time_t start_time, curr_time;
extern uint8_t StartTimer;

/** @brief GPS Init Device.
 *
 *      Init GPS device APS
 *
 *  @param[in] config Input config for GPS device.
 *  @return Error Code
 */
returnCode_e initGPS(void *config);

/** @brief GPS Deinit device.
 *  Device API to deinit GPS device
 *
 *  @return Error Code.
 */
returnCode_e deinitGPS();

/** @brief GPS Get data
 *
 *  Device API to get GPS data
 *
 *  @param[out] gpsData  GPS data
 *  @return Error Code
 */
returnCode_e getGPSData(gpsData_t *gpsData);

/** @brief Reset GPS
 *
 *  Device API to reset GPS
 *
 *  @return Error Code
 */
returnCode_e resetGPS();

/** @brief Enable GPSOneXTRA feature
 *
 *  Device API to enable GPSOneXTRA feature.
 *
 *  @return Error Code
 */
returnCode_e enableGPSOneXtra();

/** @brief Check if XDATA is vaild
 *
 *  Device API to check if XDATA is valid.
 *
 *  @return Error Code
 */
returnCode_e CheckXdataisValid(uint8_t *isValid);
#endif
