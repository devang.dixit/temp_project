/************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : userInterface.c
 * @brief : (User interface module is responsible to handle local user indicators
 *           (i.e LED, Buzzer, Button etc.) and contains the API definations which is offred by
 *           user interface module.)
 *
 * @Author     - VT
 *************************************************************************************************
 * History
 *
 * Aug/06/2018, VT , First Draft
 *************************************************************************************************/

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include <pthread.h>
#include "userInterface.h"
#include "debug.h"
#include "sysTimer.h"
#include "cJSON.h"

/****************************************
 ******** STATIC VARIABLES **************
 ****************************************/
/* This variable is used to store LED pattern database locally */

static ledPattern_t normal = {NORMAL_TYPE_COUNT, 1000, 500, 0};

static ledPattern_t alert = {ALERT_TYPE_COUNT, 1000, 800, 0};

static ledPattern_t error_pattern = {ERROR_TYPE_COUNT, 1000, 1000, 0};

static ledPattern_t selfDiag = {0, 1000, 1000, 0};

static ledPattern_t ota = {OTA_PROCESSING_TYPE_COUT, 1000, 500, 500};

static ledPattern_t disable_led = {0, 0, 0, 0};

static ledPattern_t g_cabGwLedPatterns[MAX_SYS_STATUS_TYPE];

static ledPattern_t inUseLedPattern[MAX_LED_ID];

static uint8_t patternIsChanged;
static uint32_t totalTimeOverFlow;

static buttonPressedEventNotifyCb g_notifFunPtr;

static TIMER_HANDLE totalTimeHandle  = INVALID_TIMER_HANDLE;

static pthread_t UIindicationThreadID_t;

static pthread_mutex_t UIStateLock;

static moduleStatus_e UImoduleStatus = NOT_INITIALIZED;

// Declaration of thread condition variable
pthread_cond_t cond1 = PTHREAD_COND_INITIALIZER;

// declaring mutex
pthread_mutex_t cond1Lock = PTHREAD_MUTEX_INITIALIZER;

/****************************************
 ********* FUNCTION DEFINATION **********
 ****************************************/

static uint32_t getTotalTimeOverflowbit()
{
	uint32_t bitStatus;

	pthread_mutex_lock(&UIStateLock);
	bitStatus = totalTimeOverFlow;
	pthread_mutex_unlock(&UIStateLock);
	return bitStatus;
}

static void setTotalTimeOverflowbit(uint32_t value)
{
	pthread_mutex_lock(&UIStateLock);
	totalTimeOverFlow = value;
	pthread_mutex_unlock(&UIStateLock);
}

static uint8_t getPatternChangedStatus()
{
	uint32_t changeStatus;

	pthread_mutex_lock(&UIStateLock);
	changeStatus = patternIsChanged;
	pthread_mutex_unlock(&UIStateLock);
	return changeStatus;
}

static void setPatternChangedStatus(uint8_t value)
{
	pthread_mutex_lock(&UIStateLock);
	patternIsChanged = value;
	pthread_mutex_unlock(&UIStateLock);
}

//*****************************************************************************
//  deInitGPIO()
//  Param:
//      IN :    NONE
//      OUT:    NONE
//  Returns:
//              None
//  Description:
//      de-initialize the UI gpios.
//
//  [Pre-condition:]
//      None
//
//  [Constraints:]
//      None
//
//*****************************************************************************
static returnCode_e deInitGPIO()
{
    returnCode_e retCode = GEN_SUCCESS;

    if (gpioDeInit(RED_LED) != GEN_SUCCESS || gpioDeInit(BLUE_LED)!= GEN_SUCCESS)
    {
        CAB_DBG(CAB_GW_WARNING, "De-initialization user interface module failed as GPIO is not Initialized.");
        retCode = GEN_API_FAIL;
    }

    return retCode;
}

//*****************************************************************************
//  initGPIO()
//  Param:
//      IN :    NONE
//      OUT:    NONE
//  Returns:
//              None
//  Description:
//      Initialize the UI gpios. If alread initialized by other process, re-initialize.
//
//  [Pre-condition:]
//      None
//
//  [Constraints:]
//      None
//
//*****************************************************************************
static returnCode_e initGPIO()
{
    returnCode_e retCode = GEN_SUCCESS;

    if (gpioInit(RED_LED, OUT) != GEN_SUCCESS || gpioInit(BLUE_LED, OUT) != GEN_SUCCESS)
    {
        CAB_DBG(CAB_GW_WARNING, "GPIOs are already Initialized by another process, re-initilizing.");

        retCode = deInitGPIO();

        if (retCode == GEN_SUCCESS)
        {
            if (gpioInit(RED_LED, OUT) != GEN_SUCCESS || gpioInit(BLUE_LED, OUT) != GEN_SUCCESS)
            {
                retCode = GEN_API_FAIL;
            }
        }
    }

    return retCode;
}

//*****************************************************************************
//  printLEDPattern()
//  Param:
//      IN :    bool    //blue
//              bool    //red
//              uint16_t * // array of the duty cycle value for each led
//      OUT:    NONE
//  Returns:
//              None
//  Description:
//      None
//
//  [Pre-condition:]
//      None
//
//  [Constraints:]
//      None
//
//*****************************************************************************
static void printLEDPattern(bool blue, bool red, uint16_t *dutyCycle)
{
    cJSON *root;
    cJSON *fmt;
    char* string = NULL;
    char blinkStr[15];

    root = cJSON_CreateObject();
    cJSON_AddItemToObject(root, "LED_indication_change", fmt = cJSON_CreateObject());
    cJSON_AddStringToObject(fmt, "GREEN", "ON");
    snprintf(blinkStr, sizeof(blinkStr), "BLINK%u", dutyCycle[BLUE]);
    cJSON_AddStringToObject(fmt, "BLUE", (blue == true) ? ((dutyCycle[BLUE] != 0) ? blinkStr : "ON") : "OFF");
    snprintf(blinkStr, sizeof(blinkStr), "BLINK%u", dutyCycle[RED]);
    cJSON_AddStringToObject(fmt, "RED", (red == true) ? ((dutyCycle[RED] != 0) ? blinkStr : "ON") : "OFF");

    string  = cJSON_PrintUnformatted(root);
    if (string == NULL)
    {
        CAB_DBG(CAB_GW_ERR, "Failed to print LED pattern.");
    }
    else
    {
        CAB_DBG(CAB_GW_INFO, "Change in LED pattern : %s", string);
    }

    cJSON_Delete(root);
    free(string);
}

void totalTimeOverflowCB(uint32_t data)
{
	pthread_mutex_lock(&UIStateLock);
	totalTimeOverFlow = 1;
	pthread_mutex_unlock(&UIStateLock);
}

//*****************************************************************************
//  UIindication()
//  Param:
//      IN :    void *    //blue
//      OUT:    NONE
//  Returns:
//              None
//  Description:
//      This is a thread function to handle the LED indication, pattern changes
//      and pattern change notification.
//
//  [Pre-condition:]
//      The LED GPIOs are initialized.
//
//  [Constraints:]
//      Untill first pattern to be indicated is not provided, the thread won't 
//      run.
//
//*****************************************************************************
static void* UIindication(void *arg)
{
    ledPattern_t LedPattern[MAX_LED_ID]; // For saving the current configuration
    bool isLedOn[MAX_LED_ID] = {true, true}; // Indicates whether LED will be always off
    bool isFlashOn[MAX_LED_ID] = {false, false}; // Indicates whether LED flashing is on
    bool patternToBeHold[MAX_LED_ID] = {false, false}; // Indicates whether LED pattern needs to be hold until changed
    uint8_t changeStatus = 0;
    uint16_t dutyCycle[MAX_LED_ID] = {0, 0};
    bool lastWrite = false;
    int i;
    TIMER_INFO_t totalIndicationTimerInfo;
    TIMER_STATUS_e timerRetSTat = TIMER_SUCCESS;

    /* Wait until the indication pattern is notified. */
    pthread_cond_wait(&cond1, &cond1Lock);

    while(UImoduleStatus < DEINITIALIZATION_REQUESTED)
    {
        /* First check timer if overflow, if it is then disable both the LED and wait till next change  */
        if (getTotalTimeOverflowbit() == 1)
        {
            if (gpioWrite(BLUE_LED, 0) != GEN_SUCCESS || gpioWrite(RED_LED, 0) != GEN_SUCCESS)
            {
                CAB_DBG(CAB_GW_ERR, "GPIO write failed user interface module.");
                break;
            }

            setTotalTimeOverflowbit(0);
            deleteTimer(&totalTimeHandle);
            pthread_cond_wait(&cond1, &cond1Lock);
        }

        if (getPatternChangedStatus() == 1)
        {
            LedPattern[BLUE] = inUseLedPattern[BLUE];
            LedPattern[RED] = inUseLedPattern[RED];
            /* We got the modified patterns, now reset the flag */
            setPatternChangedStatus(0);
            /* Update it for local use */
            changeStatus = 1;
            if (totalTimeHandle != INVALID_TIMER_HANDLE)
                deleteTimer(&totalTimeHandle);
        }

        if (changeStatus)
        {
            /* Reset the last configuration. */
            lastWrite = false;
            isFlashOn[BLUE] = false;
            isFlashOn[RED] = false;
            isLedOn[BLUE] = true;
            isLedOn[RED] = true;
            patternToBeHold[BLUE] = false;
            patternToBeHold[RED] = false;
            dutyCycle[BLUE] = 0;
            dutyCycle[RED] = 0;

            for (i=RED;i<MAX_LED_ID;i++)
            {
                /* Check if we need to flash it */
                if (LedPattern[i].onTime != 0 && LedPattern[i].offTime != 0)
                {
                    isFlashOn[i] = true;
                    /* Check if we need to hold the pattern */
                    if (LedPattern[i].totalCount == 0)
                    {
                        patternToBeHold[i] = true;
                    }
                    else if (LedPattern[i].totalTime != 0)
                    {
                        totalIndicationTimerInfo.count = LedPattern[i].totalTime;
                        totalIndicationTimerInfo.funcPtr = totalTimeOverflowCB;
                        totalIndicationTimerInfo.data = 0xA5; // Dummy data, not used anywhere

                        pthread_mutex_lock(&UIStateLock);
                        timerRetSTat = startSystemTimer(totalIndicationTimerInfo, &totalTimeHandle);
                        pthread_mutex_unlock(&UIStateLock);
                        if (timerRetSTat != TIMER_SUCCESS)
                        {
                            CAB_DBG(CAB_GW_ERR, "UI indication timer failed with timer module code : %d", timerRetSTat);
                            break;
                        }
                    }
                    else
                    {
                        CAB_DBG(CAB_GW_ERR, "totalTime must be specified, if totalCount is not 0.");
                        break;
                    }
                }
                /* Check if it is always off */
                else if (LedPattern[i].onTime == 0)
                {
                    isLedOn[i] = false;
                }
            }
            changeStatus = 0;

            /* Print the LED indication change on console and syslog */

            /* If onTime and offTime are non zero then LED will be blinking, so get the 
               duty cycle to print it. */
            if (LedPattern[BLUE].onTime != 0 && LedPattern[BLUE].offTime != 0)
            {
                dutyCycle[BLUE] = LedPattern[BLUE].onTime;
            }

            if (LedPattern[RED].onTime != 0 && LedPattern[RED].offTime != 0)
            {
                dutyCycle[RED] = LedPattern[RED].onTime;
            }

            printLEDPattern(isLedOn[BLUE], isLedOn[RED], dutyCycle);
        }

        if (isFlashOn[RED] && isFlashOn[BLUE])
        {
            lastWrite = !lastWrite;
            if (gpioWrite(BLUE_LED, lastWrite) != GEN_SUCCESS || gpioWrite(RED_LED, lastWrite) != GEN_SUCCESS)
            {
                CAB_DBG(CAB_GW_ERR, "GPIO write failed user interface module.");
                break;
            }
            usleep(LedPattern[BLUE].onTime * 1000);
            lastWrite = !lastWrite;
            if (gpioWrite(BLUE_LED, lastWrite) != GEN_SUCCESS || gpioWrite(RED_LED, lastWrite) != GEN_SUCCESS)
            {
                CAB_DBG(CAB_GW_ERR, "GPIO write failed user interface module.");
                break;
            }
            usleep(LedPattern[BLUE].offTime * 1000);
        }
        else if (isFlashOn[RED])
        {
            lastWrite = !lastWrite;
            if (gpioWrite(BLUE_LED, isLedOn[BLUE]) != GEN_SUCCESS || gpioWrite(RED_LED, lastWrite) != GEN_SUCCESS)
            {
                CAB_DBG(CAB_GW_ERR, "GPIO write failed user interface module.");
                break;
            }
            usleep(LedPattern[BLUE].onTime * 1000);
            lastWrite = !lastWrite;
            if (gpioWrite(BLUE_LED, isLedOn[BLUE]) != GEN_SUCCESS || gpioWrite(RED_LED, lastWrite) != GEN_SUCCESS)
            {
                CAB_DBG(CAB_GW_ERR, "GPIO write failed user interface module.");
                break;
            }
            usleep(LedPattern[BLUE].offTime * 1000);
        }
        else if (isFlashOn[BLUE])
        {
            lastWrite = !lastWrite;
            if (gpioWrite(RED_LED, isLedOn[RED]) != GEN_SUCCESS || gpioWrite(BLUE_LED, lastWrite) != GEN_SUCCESS)
            {
                CAB_DBG(CAB_GW_ERR, "GPIO write failed user interface module.");
                break;
            }
            usleep(LedPattern[BLUE].onTime * 1000);
            lastWrite = !lastWrite;
            if (gpioWrite(RED_LED, isLedOn[RED]) != GEN_SUCCESS || gpioWrite(BLUE_LED, lastWrite) != GEN_SUCCESS)
            {
                CAB_DBG(CAB_GW_ERR, "GPIO write failed user interface module.");
                break;
            }
            usleep(LedPattern[BLUE].offTime * 1000);
        }
        else
        {
        /* For now, if no LED is flashing they both hold the pattern */
            if (gpioWrite(RED_LED, isLedOn[RED]) != GEN_SUCCESS || gpioWrite(BLUE_LED, isLedOn[BLUE]) != GEN_SUCCESS)
            {
                CAB_DBG(CAB_GW_ERR, "GPIO write failed user interface module.");
                break;
            }
            /* If no LED is flashing, then assign their respective value and wait for the change in pattern */
            pthread_cond_wait(&cond1, &cond1Lock);
            continue;
        }
    }

    CAB_DBG(CAB_GW_DEBUG, "User interface indication thread exited.");
    pthread_detach(pthread_self());
    pthread_exit(0);
}

returnCode_e indicateSysStatus(sysStatusType_e type)
{
    FUNC_ENTRY

    /**< Take decision based on system status type */
    switch(type) {
        case NORMAL :
        	pthread_mutex_lock(&UIStateLock);
        	inUseLedPattern[BLUE] = g_cabGwLedPatterns[NORMAL];
        	inUseLedPattern[RED] = disable_led;
        	patternIsChanged = 1;
        	pthread_mutex_unlock(&UIStateLock);
            break;
        case ALERT :
            pthread_mutex_lock(&UIStateLock);
            inUseLedPattern[BLUE] = disable_led;
            inUseLedPattern[RED] = disable_led;
            patternIsChanged = 1;
            pthread_mutex_unlock(&UIStateLock);
            break;
        case ERROR :
            pthread_mutex_lock(&UIStateLock);
            inUseLedPattern[BLUE] = disable_led;
            inUseLedPattern[RED] = g_cabGwLedPatterns[ERROR];
            patternIsChanged = 1;
            pthread_mutex_unlock(&UIStateLock);
            break;
        case LOW_BATTERY :
            break;
        case LOW_TEMP :
            break;
        case HIGH_TEMP :
            break;
        case GATEWAY_PAIRING :
            break;
        case GATEWAY_PAIRED :
            break;
        case LTE_CONNECTING :
            break;
        case LTE_CONNECTED :
            break;
        case NO_LTE_CONNECTIVITY :
            break;
        case TPMS_PAIRING :
            break;
        case TPMS_PAIRED :
            break;
        case SELF_DIAG_PATTERN :
            pthread_mutex_lock(&UIStateLock);
            inUseLedPattern[BLUE] = g_cabGwLedPatterns[SELF_DIAG_PATTERN];
            inUseLedPattern[RED] = g_cabGwLedPatterns[SELF_DIAG_PATTERN];
            patternIsChanged = 1;
            pthread_mutex_unlock(&UIStateLock);
            break;
        case OTA_PROCESS_PATTERN :
        	pthread_mutex_lock(&UIStateLock);
        	inUseLedPattern[BLUE] = g_cabGwLedPatterns[OTA_PROCESS_PATTERN];
        	inUseLedPattern[RED] = disable_led;
        	patternIsChanged = 1;
        	pthread_mutex_unlock(&UIStateLock);
        	break;
        default :
            CAB_DBG(CAB_GW_ERR, "Invalid option");
            return GEN_INVALID_TYPE;
    }
    /* Pattern is changed now, signal the thread if it is waiting after totalTime overflow */
    pthread_cond_signal(&cond1);
    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e setLedPattern(sysStatusType_e type, ledPattern_t pattern)
{
    FUNC_ENTRY
    /* To update pattern in local LED pattern variable as it is used further by user */
    memcpy(&g_cabGwLedPatterns[type], &pattern, sizeof(g_cabGwLedPatterns[type]));
    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e getLedPattern(sysStatusType_e type, ledPattern_t * pattern)
{
    FUNC_ENTRY
    /* To return pattern type from local LED pattern variable */
    memcpy(pattern, &g_cabGwLedPatterns[type], sizeof(g_cabGwLedPatterns[type]));
    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e initUIModule(uiConfig_t * data)
{
    FUNC_ENTRY
    returnCode_e retCode;

    /* Module is already initialized for this process, return success. */
    if (UImoduleStatus == INITIALIZED)
    {
        return GEN_SUCCESS;
    }

    retCode = initGPIO();
    if (retCode != GEN_SUCCESS)
    {
        CAB_DBG(CAB_GW_ERR, "Initializing user interface module failed");
        return retCode;
    }

    pthread_mutex_init(&UIStateLock, NULL);

    pthread_mutex_lock(&UIStateLock);
    g_cabGwLedPatterns[NORMAL] = normal;
    g_cabGwLedPatterns[ALERT] = alert;
    g_cabGwLedPatterns[ERROR] = error_pattern;
    g_cabGwLedPatterns[SELF_DIAG_PATTERN] = selfDiag;
    g_cabGwLedPatterns[OTA_PROCESS_PATTERN] = ota;
    UImoduleStatus = INITIALIZED;
    pthread_mutex_unlock(&UIStateLock);

    pthread_create(&UIindicationThreadID_t, NULL, &UIindication, NULL);
    CAB_DBG(CAB_GW_INFO, "Initialized user interface module");

#if 0
    uint8_t loopCnt;
    //Copies led patterns array of stuctures to static local array
    for(loopCnt = NORMAL; loopCnt < 3; loopCnt++) {     //TODO : condition value will be changed
        memcpy(&g_cabGwLedPatterns[loopCnt], data->cabGwLedPatterns[loopCnt], \
               sizeof(g_cabGwLedPatterns[loopCnt]));
    }
    //register call back to get status of gpio button
    g_notifFunPtr = data->buttonPressedEventNotifyCb;
    CAB_DBG(CAB_GW_INFO, "Intialized user interface module");
    (*g_notifFunPtr)(5);            //To test the callback and it will be removed
#endif
    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e deInitUIModule(void)
{
    FUNC_ENTRY
    returnCode_e retCode;

    /* Flag the thread to break */
    pthread_mutex_lock(&UIStateLock);
    UImoduleStatus = DEINITIALIZATION_REQUESTED;
    pthread_mutex_unlock(&UIStateLock);

    pthread_cond_signal(&cond1);
    pthread_join(UIindicationThreadID_t, NULL);

    pthread_mutex_destroy(&UIStateLock);

    if (totalTimeHandle != INVALID_TIMER_HANDLE)
        deleteTimer(&totalTimeHandle);

    retCode = deInitGPIO();

    if (retCode != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "De-Initializing user interface module failed");
        return retCode;
    }

    memset(&g_cabGwLedPatterns, 0, sizeof(g_cabGwLedPatterns)); //Reset the array of led patterns
    memset(&inUseLedPattern, 0, sizeof(ledPattern_t));
    g_notifFunPtr = NULL;       //callback Points to NULL
    UImoduleStatus = DEINITIALIZED;
    CAB_DBG(CAB_GW_INFO, "De-Initialized user interface module");

    FUNC_EXIT
    return GEN_SUCCESS;
}
