/*************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : userInterface.h
 * @brief (This file contains the user interface information like includes, macros, enums,
 *         structures, global/extern variables, callbacks, and API prototypes.)
 *
 * @Author     - VT
 ************************************************************************************************
 * History
 *
 * Aug/06/2018, VT , First Draft
 ************************************************************************************************/

#ifndef _USER_INTERFACE_H
#define _USER_INTERFACE_H

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include "ledConfig.h"
#include "error.h"
#include "gpio.h"

/****************************************
 ************* DEFINES ******************
 ****************************************/
#define RED_LED     116         /* GPIO04_IO20 */
#define BLUE_LED    115         /* GPIO04_IO19 */

/****************************************
 ************* TYPEDEFS *****************
 ****************************************/
typedef uint32_t ledType;

/****************************************
 ************* ENUMS ********************
 ****************************************/
typedef enum moduleStatus {
	NOT_INITIALIZED=0,
	INITIALIZED,
	DEINITIALIZATION_REQUESTED,
	DEINITIALIZED
} moduleStatus_e;

/****************************************
 ************ STRUCTURES ****************
 ****************************************/
/* This stucture contains the information of LED patterns and callback function */
typedef struct uiConfig {
    ledPattern_t * cabGwLedPatterns[MAX_SYS_STATUS_TYPE];
    void (*buttonPressedEventNotifyCb)(uint8_t gpio);
} uiConfig_t;

/****************************************
 ******** GLOBAL VARIABLES **************
 ****************************************/

/****************************************
 ******** EXTERN VARIABLES **************
 ****************************************/

/****************************************
 ************* CALLBACKS ****************
 ****************************************/
/* to handle callback to get status of button  */
typedef void (*buttonPressedEventNotifyCb)(uint8_t gpio);

/****************************************
 ********* FUNCTION PROTOTYPES **********
 ****************************************/
/******************************************************************
 *@brief (This API is used to initialize the user interface module to show system status to user)
 *
 *@param[IN]  uiConfig_t * (Pointer to user interface configuration structure)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e initUIModule(uiConfig_t *);

/******************************************************************
 *@brief (This API is used to De-initialize the user interface module)
 *
 *@param[IN] None
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e deInitUIModule(void);

/******************************************************************
 *@brief (This API is used to indicate system status to User via different peripherals like
 *        LED and buzzer)
 *
 *@param[IN] sysStatusType_e (Indicates type of system status)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 **********************************************************************************************/
returnCode_e indicateSysStatus(sysStatusType_e);

/******************************************************************
 *@brief (This API is used to set the led pattern)
 *
 *@param[IN] sysStatusType_e (Indicates type of system status)
 *@param[IN] ledPattern_t (Indicates pattern structure)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 **********************************************************************************************/
returnCode_e setLedPattern(sysStatusType_e , ledPattern_t);

/******************************************************************
 *@brief (This API is used to get the led pattern)
 *
 *@param[IN] sysStatusType_e (Indicates type of system status)
 *@param[OUT] ledPattern_t* (Indicates pointer to pattern structure)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 **********************************************************************************************/
returnCode_e getLedPattern(sysStatusType_e , ledPattern_t *);

#endif /* _USER_INTERFACE_H */
