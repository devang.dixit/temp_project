/*******************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description: 
 * @file cc1310.h
 * @brief  
 * 
 * @Author   - Volansys
 *
 *******************************************************************************
 *
 * @History
 *
 * Aug/13/2018 VT	Created
 *
 ******************************************************************************/
#ifndef CC1310_H
#define CC1310_H

/**************************
 * 		INCLUDES
 *************************/
#include<stdint.h>
#include<stdbool.h>
#include <termios.h>
#include "sub1.h"
#include "watchdog.h"
#include "syscall.h"

/**************************
 * 		MACROS
 *************************/

#define UART_PORT_AVETECH			"/dev/ttymxc2"
#define UART_BAUDRATE_AVETECH			B115200

#define MAX_RETRY	3

#define DEBUG_FS_FILE       "/sys/kernel/debug/signalconfpid"
#define SIG_UART   51

/*	Offset for tpms data received from CC1310 in payload	*/
#define	SENSORID_OFFSET	0		/*	4bytes	*/
#define	PRESSURE_OFFSET	4		/*	4bytes	*/
#define	TEMPERATURE_OFFSET	8	/*	4bytes	*/
#define	BATTERY_OFFSET	12		/*	4bytes	*/
#define	RSSI_OFFSET	16	        /*	1 byte	*/
#define STATUS_OFFSET   17              /*      1byte   */

/**************************
 *		Enumerations
 *************************/
#if 0
typedef enum {
	NORMAL_POWER_MODE = 0 ,
	REMOVE_SENSOR ,
    NOTIFY_RF ,
	STOP_RF ,
    START_RF ,
    ADD_SENSOR ,
	SEND_SENSOR_DATA ,
	LOW_POWER_MODE ,
	MAX_COMMANDS
} UART_COMMAND_e;
#endif

/************************
 * Function Prototypes
 ************************/
/** Initialize CC1310 sub1 module.
 *
 * This api is used to initialize CC1310 based supported vendor sub1 module.
 * Device manager will send sensor vendor type and also a callback
 * for tpms module to send data. This function will register this 
 * callback and initialize respective tpms module based on vendor 
 * type. Also, it will pass on the respective module api details to 
 * device manager.
 *
 * @param[in]   sub1Info    callback & vendor Type as input.
 *
 * @return int8_t   error code.
 */
int8_t initCC1310(const rfInfo_t sub1Info);

/** DeInitialize CC1310 sub1 module.
 *
 * This api is used to deinitialize CC1310 sub1 module.
 *
 * @return  error code.
 */
int8_t deInitCC1310(void); 

/** DeInitialize sub1 module.
 *
 * This api is used to deinitialize sub1 module.
 *
 * @return  error code.
 */
int8_t addTpmsSensor(void *sensorID);

/** DeInitialize sub1 module.
 *
 * This api is used to deinitialize sub1 module.
 *
 * @return  error code.
 */
int8_t removeTpmsSensor(void *sensorID);

/*
 * Callback function to fetch data from relevant RF received module
*/
void CC1310DataFetchCB(int sigNum, siginfo_t * sigInfo, void * unused);

#endif	/* end CC1310_H */
