/*******************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 * 
 * Description:
 * @file sub1.h
 * @brief
 *    
 * This functional block is an interface between device manager and low level 
 * TPMS module. It is a medium providing communication api's and/or callback 
 * between device manager and low level TPMS module. This module will be aware 
 * of low level TPMS module api's that would be required by above module like 
 * device manager to communicate.
 * 
 * @Author       - Volansys
 *
 *******************************************************************************
 *
 * History
 *                      
 * @date Aug/28/2018, VT, Created
 *
 ******************************************************************************/

#ifndef SUB1_H
#define SUB1_H

/*******************
 * Includes
 *******************/
#include "tpmsConfig.h"
#include <stdbool.h>
#include <signal.h>
#include "error.h"

/*************************
 * Defines
 *************************/
#define SUB1_POWER_GPIO	46
#define SUB1_RESET_GPIO	34

/***********************
 * Typedefs
 ***********************/
typedef returnCode_e (*callback) (void *);

/***********************
 * Structures
 ***********************/
/*! \struct rfInfo_t sub1.h "sub1.h"
	\brief Consists of sub1 device communication information.	*/
typedef struct {
    callback addSensor;				/**< Callback to add tpms sensor.	*/
    callback removeSensor;			/**< Callback to remove tpms sensor.	*/
    callback sendTpmsDataToDM;		/**< Callback for sending tpms data to device manager.	*/
    sensorVendorType_e vendorType;	/**< Sensor vendor Type configured by device manager.	*/
} rfInfo_t;


/***********************
 * Global Variables
 ***********************/
callback sendDataToDM;

/************************
 * Function Prototypes
 ************************/
/** Initialize sub1 module.
 *
 * This api is used to initialize sub1 module by device manager.
 * Device manager will send sensor vendor type and also a callback
 * for tpms module to send data. This function will register this 
 * callback and initialize respective tpms module based on vendor 
 * type. Also, it will pass on the respective module api details to 
 * device manager.
 *
 * @param[in/out]	tpmsInfo	callback & vendor Type as input,
 * 								vendor specific tpms api information as output.
 *
 * @return int8_t	error code.
 */ 
int8_t initializeSub1(rfInfo_t *tpmsInfo);

/** DeInitialize sub1 module.
 *
 * This api is used to deinitialize sub1 module.
 *
 * @return 	error code.
 */ 
int8_t deInitializeSub1(void);
returnCode_e selfTestSub1(void);

void Sub1DataFetchCB(int sigNum, siginfo_t * sigInfo, void * unused);
#endif  /* '#endif' of SUB1_H */
