/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file  batModule.c
 * @brief (Source file for battery module.)
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Sep/24/2018, VT , First Draft
***************************************************************/

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include "batModule.h"
#include "gpio.h"
#include "debug.h"

/****************************************
 ******** STATIC VARIABLES **************
 ****************************************/
//static sem_t g_semLock;                                     /**< Initialize semaphore variable */
static pthread_mutex_t g_lock = PTHREAD_MUTEX_INITIALIZER;  /**< Initialize mutex variable */
static notifyPowerSwitchEvent g_notifFunPtr;                /**< Register calllback */
static uint16_t g_batVoltage;                               /**< Store battery voltage */
static pthread_t g_threadId;                                /**< Thread id */
static uint8_t g_lowBatteryThreshold;                       /**< Low battery threshold */
static float g_lowBatteryVolt;                       /**< Low battery threshold */
static uint8_t g_batteryStatus;                             /**< Battery status */

/****************************************
 ********* FUNCTION DEFINITION **********
 ****************************************/


static float getThrVolt(uint8_t thrPercentage)
{
	if (thrPercentage <= 17) {
		return LOW_BAT_11_PERC_THRESHOLD_VOLT;
	} else if (thrPercentage <= 26) {
		return LOW_BAT_23_PERC_THRESHOLD_VOLT;
	} else if (thrPercentage <= 38) {
		return LOW_BAT_29_PERC_THRESHOLD_VOLT;
	} else if (thrPercentage <= 53) {
		return LOW_BAT_48_PERC_THRESHOLD_VOLT;
	} else if (thrPercentage <= 60) {
		return LOW_BAT_57_PERC_THRESHOLD_VOLT;
	} else if (thrPercentage <= 66) {
		return LOW_BAT_62_PERC_THRESHOLD_VOLT;
	} else if (thrPercentage <= 73) {
		return LOW_BAT_70_PERC_THRESHOLD_VOLT;
	} else if (thrPercentage <= 80) {
		return LOW_BAT_76_PERC_THRESHOLD_VOLT;
	} else if (thrPercentage <= 86) {
		return LOW_BAT_84_PERC_THRESHOLD_VOLT;
	} else {
		return LOW_BAT_88_PERC_THRESHOLD_VOLT;
	}
}

/********************************************************************
 *@brief (It is used to count battery percentage.)
 *
 *@param[IN] uint16_t (Battery voltage ADC reading.)
 *@param[OUT] None
 *
 *@return uint8_t (Returns Battery percentage value.)
 *********************************************************************/
static uint8_t countBatteryPercentage(uint16_t value)
{
    FUNC_ENTRY

    uint16_t batVoltageVal;
    float analogBatVol;
    uint8_t batVolInPercentage;

    return 90;
    batVoltageVal = value;
    analogBatVol = (((float)batVoltageVal * ADC_REF_VOLTAGE) / ((float)ADC_RESOLUTION));

    batVolInPercentage = (uint8_t)(100 - ((HIGH_BAT_THRESHOLD - analogBatVol) / \
                                          ((HIGH_BAT_THRESHOLD - g_lowBatteryVolt) / (HIGH_BAT_PERCENTAGE - g_lowBatteryThreshold))));

    CAB_DBG(CAB_GW_DEBUG, "Bat percentage : %d", batVolInPercentage);

    FUNC_EXIT
    return batVolInPercentage;
}

/********************************************************************
 *@brief (It is used to get battery voltage.)
 *
 *@param[IN] None
 *@param[OUT] uint16_t * (pointer to store battery volatage.)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e readBatteryLevel(uint16_t * level)
{
    FUNC_ENTRY

    int filePtr;
    int ret;
    char batLevel[5];

    memset(batLevel, 0 , sizeof(batLevel));
    usleep(BAT_READ_INIT_DELAY*1000);
    filePtr = open(BAT_READ_PATH, O_RDONLY);
    if(filePtr == -1) {
        CAB_DBG(CAB_GW_ERR, "Opening the file failed.");
        close(filePtr);
        return FILE_OPEN_FAIL;
    }
    ret = read(filePtr, (void*)&batLevel, sizeof(batLevel));
    if(ret == -1) {
        CAB_DBG(CAB_GW_ERR, "Reading the %s file failed.", BAT_READ_PATH);
        close(filePtr);
        return FILE_READ_ERROR;
    }
    *level = atoi(batLevel);
    CAB_DBG(CAB_GW_DEBUG, "ADC value read %d", *level);

    close(filePtr);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/********************************************************************
 *@brief (It is used to handle thread callback.)
 *
 *@param[IN] void * (indicates sleep interval value)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static void* threadHandler(void* arg)
{
    FUNC_ENTRY

    uint8_t loopCnt, ret, batteryPercentage;
    uint16_t batVol[BAT_DATA_BUF_SIZE];
    uint32_t sum = 0;

    while(1) {
        //lock mutex
        pthread_mutex_lock(&g_lock);

        ret = gpioWrite(BAT_CHECK_EN, 1);
        if(ret != GEN_SUCCESS) {
            CAB_DBG(CAB_GW_WARNING, "Error writing value for BAT_CHECK_EN");
        }

        /**< To read average value for battery voltage */
        for(loopCnt = 0; loopCnt < BAT_DATA_BUF_SIZE; loopCnt ++) {
            ret = readBatteryLevel((void*)&batVol[loopCnt]);
            sum += batVol[loopCnt];
            usleep(BAT_MONTOR_TIME_INTR * 1000);
        }

        ret = gpioWrite(BAT_CHECK_EN, 0);
        if(ret != GEN_SUCCESS) {
            CAB_DBG(CAB_GW_WARNING, "Error writing value for BAT_CHECK_EN");
        }

        g_batVoltage = sum / BAT_DATA_BUF_SIZE;
        sum = 0;
        batteryPercentage = countBatteryPercentage(g_batVoltage);

        if(batteryPercentage <= g_lowBatteryThreshold) {
            g_batteryStatus = LOW_BAT;
        } else {
            g_batteryStatus = NORMAL_BAT;
        }

        //unlock mutex
        pthread_mutex_unlock(&g_lock);

        if(g_batteryStatus == LOW_BAT) {
			powerSwitchEvent_e battStat = LOW_BAT;
            (*g_notifFunPtr)((void *)&battStat);
        }

        /**< To lock semaphore variable */
        //sem_wait(&g_semLock);
    }
    FUNC_EXIT
}

returnCode_e initBatModule(notifyPowerSwitchEvent funPtr, uint8_t lowBatThreshold)
{
    FUNC_ENTRY

    CAB_DBG(CAB_GW_INFO, "Initializing battery module");

    //Initializind mutext lock
    pthread_mutex_init(&g_lock, NULL);

    //lock mutex
    pthread_mutex_lock(&g_lock);
#if 0
    //Initialize semaphore
    ret = sem_init(&g_semLock, 0, 1);
    if(ret != 0) {
        CAB_DBG(CAB_GW_ERR, "Couldn't Initialize the semaphore");
        //unlock mutex
        pthread_mutex_unlock(&g_lock);
        return MODULE_INIT_FAIL;
    }
    /**< To lock semaphore variable */
    sem_wait(&g_semLock);
#endif
    if(funPtr == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error");
        //unlock mutex lock
        pthread_mutex_unlock(&g_lock);
        return GEN_NULL_POINTING_ERROR;
    }
    g_notifFunPtr = funPtr;
    g_lowBatteryThreshold = lowBatThreshold;
    g_lowBatteryVolt = getThrVolt(g_lowBatteryThreshold);
    CAB_DBG(CAB_GW_DEBUG, "Battery Threshold Voltage updated %d, %f", g_lowBatteryThreshold, g_lowBatteryVolt);

    gpioInit(BAT_SWITCH_OVER, IN);
    gpioInit(CHG_STAT1, IN);
    gpioInit(CHG_STAT2, IN);
    gpioInit(BAT_CHECK_EN, OUT);
#if 0
    ret = pthread_create(&g_threadId, NULL, threadHandler, NULL);
    if(ret != 0) {
        CAB_DBG(CAB_GW_ERR, "Error while creating thread : %d", (int)g_threadId);
        //unlock mutex
        pthread_mutex_unlock(&g_lock);
        return MODULE_INIT_FAIL;
    }
#endif
    //unlock mutex
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e deInitBatModule(void)
{
    FUNC_ENTRY

    /*Any ongoing file operation will not allow to take this lock untill
      it completes. Once completed, no other thread will spawn to use event
     module as all other modules must have been deinit before */
    pthread_mutex_lock(&g_lock);

    CAB_DBG(CAB_GW_INFO, "Deinitializing battery module");

    gpioDeInit(BAT_SWITCH_OVER);
    gpioDeInit(CHG_STAT1);
    gpioDeInit(CHG_STAT2);
    gpioDeInit(BAT_CHECK_EN);

    /**< To cancel current running thread */
    //ret = pthread_cancel(g_threadId);
    //if(ret != 0) {
    //    CAB_DBG(CAB_GW_ERR, "Error while canceling thread : %d", (int)g_threadId);
    //    //unlock mutex
    //    pthread_mutex_unlock(&g_lock);
    //    return MODULE_DEINIT_FAIL;
    //}

    //sem_post(&g_semLock);
    //sem_destroy(&g_semLock);

    pthread_mutex_unlock(&g_lock);

    //destroying lock
    pthread_mutex_destroy(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e getPowerSource(powerDevice_e * powerDevice)
{
    FUNC_ENTRY

    char powerSource;
    uint8_t status, readValue;
    int file;

    if(powerDevice == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error");
        return GEN_NULL_POINTING_ERROR;
    }

    *(powerDevice_e*)powerDevice = VEHICLE_POWER;
    return GEN_SUCCESS;
    //lock mutex
    pthread_mutex_lock(&g_lock);

    file = open(BAT_SWITCH_PATH, O_RDONLY);
    if(file == -1) {
        CAB_DBG(CAB_GW_ERR, "Failed to open %s file", BAT_SWITCH_PATH);
        //unlock mutex
        pthread_mutex_unlock(&g_lock);
        return FILE_OPEN_FAIL;
    }
    status = read(file, &powerSource, sizeof(char));
    if(status == -1) {
        CAB_DBG(CAB_GW_ERR, "Error reading value for battery switch over event file");
        close(file);
        //unlock mutex
        pthread_mutex_unlock(&g_lock);
        return FILE_READ_ERROR;
    }
    close(file);

    //readValue = (powerSource == '1') ? 1 : 0;

    if ( gpioRead(BAT_SWITCH_OVER, &readValue) != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "Error reading value for battery switch over gpio file");
        pthread_mutex_unlock(&g_lock);
	return FILE_READ_ERROR;
    }
    if(readValue == 1) {
        *(powerDevice_e*)powerDevice = BACKUP_BATTERY;
    } else if(readValue == 0) {
        *(powerDevice_e*)powerDevice = VEHICLE_POWER;
    }

    //unlock mutex
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}
#if 0
returnCode_e notifyLowBatCond(void)
{
    FUNC_ENTRY

    //lock mutex
    pthread_mutex_lock(&g_lock);

    /**< To unlock the semaphore variable */
    sem_post(&g_semLock);

    //unlock mutex
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}
#endif
returnCode_e switchingSignalHandler(void)
{
    FUNC_ENTRY

    char powerSource;
    uint8_t status, readValue;
    int file;
    returnCode_e retCode = GEN_SUCCESS;

    return GEN_SUCCESS;
    //lock mutex
    pthread_mutex_lock(&g_lock);

    file = open(BAT_SWITCH_PATH, O_RDONLY, 0644);
    if(file == -1) {
        CAB_DBG(CAB_GW_ERR, "Failed to open %s file", BAT_SWITCH_PATH);
        //unlock mutex
        pthread_mutex_unlock(&g_lock);
        return FILE_OPEN_FAIL;
    }
    status = read(file, &powerSource, sizeof(char));
    if(status == -1) {
        CAB_DBG(CAB_GW_ERR, "Error reading value for battery switch over event file");
        close(file);
        //unlock mutex
        pthread_mutex_unlock(&g_lock);
        return FILE_READ_ERROR;
    }
    close(file);

    retCode = gpioRead(BAT_SWITCH_OVER, &readValue);
    if (retCode != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "Error reading value for battery switch over gpio file");
        pthread_mutex_unlock(&g_lock);
	return retCode;
    }

	powerSwitchEvent_e battStat;

    if(readValue == 0) {
		battStat = BAT_TO_VEH;
		(*g_notifFunPtr)((void *)&battStat);
    } else if(readValue == 1) {
		battStat = VEH_TO_BAT;
		(*g_notifFunPtr)((void *)&battStat);
    }

    //unlock mutex
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return status;
}

returnCode_e getChargingStat(chargingStatus_e * chargingStat)
{
    FUNC_ENTRY

    uint32_t stat1, stat2;
    uint8_t status;

    if(chargingStat == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error");
        return GEN_NULL_POINTING_ERROR;
    }

    *(chargingStatus_e*)chargingStat = FAST_CHARGE;
    return GEN_SUCCESS;
    //lock mutex
    pthread_mutex_lock(&g_lock);

    status = gpioRead(CHG_STAT1, (void*)&stat1);
    if(status != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "Error reading value for CHG_STAT1");
        //unlock mutex
        pthread_mutex_unlock(&g_lock);
        return FILE_READ_ERROR;
    }
    status = gpioRead(CHG_STAT2, (void*)&stat2);
    if(status != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "Error reading value for CHG_STAT2");
        //unlock mutex
        pthread_mutex_unlock(&g_lock);
        return FILE_READ_ERROR;
    }

    if(stat1 == 1 && stat2 == 1) {
        *(chargingStatus_e*)chargingStat = PRE_CHARGE;
    } else if(stat1 == 1 && stat2 == 0) {
        *(chargingStatus_e*)chargingStat = FAST_CHARGE;
    } else if(stat1 == 0 && stat2 == 1) {
        *(chargingStatus_e*)chargingStat = CHARGE_DONE;
    } else if(stat1 == 0 && stat2 == 0) {
        *(chargingStatus_e*)chargingStat = SLEEP_MODE;
    }

    //unlock mutex
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e getBatteryHealth(powerSwitchEvent_e * status)
{
    FUNC_ENTRY

    int ret;
    uint8_t loopCnt, batteryPercentage;
    uint16_t batVol[BAT_DATA_BUF_SIZE];
    uint32_t sum = 0;

    if(status == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error");
        return GEN_NULL_POINTING_ERROR;
    }

    *(powerSwitchEvent_e*)status = NORMAL_BAT;
    return GEN_SUCCESS;
    //lock mutex
    pthread_mutex_lock(&g_lock);

    ret = gpioWrite(BAT_CHECK_EN, 1);
    if(ret != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "Error writing value for BAT_CHECK_EN");
        //unlock mutex lock
        pthread_mutex_unlock(&g_lock);
        return FILE_WRITE_ERROR;
    }

    /**< To read average value for battery voltage */
    for(loopCnt = 0; loopCnt < BAT_DATA_BUF_SIZE; loopCnt ++) {
        ret = readBatteryLevel(&batVol[loopCnt]);
        sum += batVol[loopCnt];
        usleep(BAT_READ_INIT_DELAY * 1000);
    }

    ret = gpioWrite(BAT_CHECK_EN, 0);
    if(ret != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "Error writing value for BAT_CHECK_EN");
        //unlock mutex lock
        pthread_mutex_unlock(&g_lock);
        return FILE_WRITE_ERROR;
    }
    g_batVoltage = sum / BAT_DATA_BUF_SIZE;
    sum = 0;
    batteryPercentage = countBatteryPercentage(g_batVoltage);

    if(batteryPercentage <= g_lowBatteryThreshold) {
        g_batteryStatus = LOW_BAT;
    } else {
        g_batteryStatus = NORMAL_BAT;
    }
    *(powerSwitchEvent_e*)status = g_batteryStatus;

    //unlock mutex lock
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}


returnCode_e setBatThreshold(uint32_t batThreshold) 
{
    FUNC_ENTRY
    return GEN_SUCCESS;
    pthread_mutex_lock(&g_lock);
    g_lowBatteryThreshold = batThreshold;
    g_lowBatteryVolt = getThrVolt(g_lowBatteryThreshold);
    CAB_DBG(CAB_GW_DEBUG, "Battery Threshold Voltage updated %d, %f", g_lowBatteryThreshold, g_lowBatteryVolt);
    pthread_mutex_unlock(&g_lock);
    FUNC_EXIT
    return GEN_SUCCESS;
}
