#ifndef __LteModule_h__
#define __LteModule_h__

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>   /* File Control Definitions */
#include <termios.h> /* POSIX Terminal Control Definitions */
#include <unistd.h>  /* UNIX Standard Definitions */
#include <errno.h>   /* ERROR Number Definitions */
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <netdb.h>
#include "error.h"
#include "commonData.h"
#include <mqueue.h>
#include <sys/stat.h>
#include "watchdog.h"

/****************************************
 ************* MACROS ****************
 ****************************************/
#define CONN_CHECK_INTERVAL 	 	(10)
#define CONN_CHECK_MAX_TRY	 	(12)
#define MAX_BUFF_SIZE 512


#define LTE_CMD_QUEUE_NAME "/lte_cmd_queue"
#define LTE_STATE_QUEUE_NAME "/lte_state_queue"
#define LTE_STATE_QUEUE_NAME_FOR_REMOTE_DIAG_APP "/lte_state_queue_for_remote_diag_app"
#define NO_OPER_INFO_STR 	"No Operator Info"
#define NO_SIM_PRESENT_STR 	"SIM Not Present"

/* Timer module macros */
#define ONE_MINUTE                      (60)    /* One minute seconds macro */

#define MQ_SEND_LTE_CMD_TIMEOUT      	(5 * ONE_MINUTE)
#define MQ_SEND_LTE_STATUS_TIMEOUT      (5 * ONE_MINUTE)

/****************************************
 ************* ENUMS ****************
 ****************************************/
typedef enum {
	CONNECTED = 0,
	DISCONNECTED = 1,
	MAX_LTE_CB_TYPE
} lteCBDataType_e;

typedef enum {
	CONNECTED_LTE_STATE = 0,
	DISCONNECTED_LTE_STATE = 1,
	WATCHDOG_LTE_STATE = 2,
	MAX_LTE_STATE
} lteStatus_e;

typedef enum {
        CONNECT = 0,
        DISCONNECT = 1,
        DEINIT = 2,
	WATCHDOG_LTE_CMD = 3,
        MAX_CMD_TYPE
} lteCmd_e;

typedef struct {
	lteCBDataType_e eventType;
} lteCBData_t;

typedef struct lteCmd_t {
        lteCmd_e cmd;
} lteCmd_t;

typedef struct lteState {
        lteStatus_e state;
} lteState_t;

typedef struct lteSMS {
	long msg_type;
	char msg_buff[MAX_BUFF_SIZE];
} lteSms_t;

/****************************************
 ************* FUNCTIONS ****************
 ****************************************/

typedef void (*lteCallback)(lteCBData_t *data);
/*********************************************************************
 *@brief          : Init LTE module
 *
 *@param[IN]      : None
 *@param[OUT]     : None
 *
 *@return         : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e initLTE(lteCallback cbFunc);

/*********************************************************************
 *@brief          : deInit LTE module
 *
 *@param[IN]      : None
 *@param[OUT]     : None
 *
 *@return         : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e deInitLTE();

/*********************************************************************
 *@brief          : Establish LTE connectivity
 *
 *@param[IN]      : None
 *@param[OUT]     : None
 *
 *@return 	      : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e connectLTE();

/*********************************************************************
 *@brief          : Disconnect LTE connectivity
 *
 *@param[IN]      : None
 *@param[OUT]     : None
 *
 *@return 	      : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e disconnectLTE();

/*********************************************************************
 *@brief          : Provide LTE operator name
 *
 *@param[IN]      : None
 *@param[OUT]     : Cellular Operator Name
 *
 *@return 	      : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
 returnCode_e getLTEOperatorInfo(char *opInfo);

/*********************************************************************
 *@brief          : Provide current signal strength of LTE connection
 *
 *@param[IN]      : None
 *@param[OUT]     : RSSI - Signal strength
 *					Bit Error Rate - Quality
 *@return 	      : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e getLTESigQuality(int *rssi, int *bErrorRate);

/*********************************************************************
 *@brief          : Provide LTE diagnostic data
 *
 *@param[IN]      : None
 *@param[OUT]     : struct lteDiagInfo_t
 *@return 	      : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e getLTEDiagInfo(lteDiagInfo_t *diagInfo);

/*********************************************************************
 *@brief          : Provide LTE IMEI number
 *
 *@param[IN]      : None
 *@param[OUT]     : IMEI number
 *@return 	      : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
 returnCode_e getLteIMEINum(uint8_t *imeiNum);

/*********************************************************************
 *@brief          : Provide LTE ICCID
 *
 *@param[IN]      : None
 *@param[OUT]     : ICCID number
 *@return 	      : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
 returnCode_e getLteICCIDNum(char *iccId);

/*********************************************************************
 *@brief          : Check SIM detection
 *
 *@param[IN]      : None
 *@param[OUT]     : Is SIM inserted or not
 *@return 	      : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
 returnCode_e checkSIMDetection(uint8_t *present);

/*********************************************************************
 *@brief          : Provide network syncronized time in local timezone
 *
 *@param[IN]      : None
 *@param[OUT]     : local time
 *@return 	      : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
 returnCode_e getLteNetTime(time_t *currTime);




#endif // __LteModule_h__
