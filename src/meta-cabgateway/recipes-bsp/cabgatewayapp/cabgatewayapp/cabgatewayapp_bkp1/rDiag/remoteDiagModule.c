#include <string.h>
#include <error.h>
#include <stdint.h>
#include "circularBuffer.h"
#include "remoteDiagModule.h"
#include "error.h"
#include "debug.h"
#include "syscall.h"
#include <errno.h>

static mqd_t mqRDiagReqQueue = -1;
static mqd_t mqRDiagResQueue = -1;
static pthread_t rDiagModResHandlerThreadID_t;


static CAB_CIRCULAR_BUFFER_t *g_rdiagBufHandle = NULL;
static rDiagModuleResHandlerCB g_notifFunPtr;


/*NOTE: Don't use cab logs in this file.*/
static returnCode_e rDiagReqQueueSetup()
{
	returnCode_e retCode = GEN_SUCCESS;
	struct mq_attr attr;
	FUNC_ENTRY

	/* initialize the queue attributes */
	attr.mq_flags = 0;
	attr.mq_maxmsg = RDIAG_MAX_MSG;
	attr.mq_msgsize = sizeof(remoteDiagWrapper_t);
	attr.mq_curmsgs = 0;

	/* Cleanup the msg queue if exist */
	//mq_unlink(RDIAG_REQ_QUEUE);

	/* create the message queue */
	mqRDiagReqQueue = mq_open(RDIAG_REQ_QUEUE, O_CREAT | O_WRONLY, 0666, &attr);
	if(mqRDiagReqQueue < 0)
	{
		CAB_DBG(CAB_GW_ERR, "mq_open for Remote Diag Request queue fail with error %s", strerror(errno));
		FUNC_EXIT
		return MQ_OPEN_ERROR;
	}

	FUNC_EXIT
	return retCode;
}

/*NOTE: Don't use cab logs in this file.*/
static returnCode_e rDiagResQueueSetup()
{
	returnCode_e retCode = GEN_SUCCESS;
	struct mq_attr attr;
	FUNC_ENTRY

	/* initialize the queue attributes */
	attr.mq_flags = 0;
	attr.mq_maxmsg = RDIAG_MAX_MSG;
	attr.mq_msgsize = sizeof(remoteDiagWrapper_t);
	attr.mq_curmsgs = 0;

	/* Cleanup the msg queue if exist */
	//mq_unlink(RDIAG_RES_QUEUE);

	/* create the message queue */
	mqRDiagResQueue = mq_open(RDIAG_RES_QUEUE, O_CREAT | O_RDONLY, 0444, &attr);
	if(mqRDiagResQueue < 0)
	{
		CAB_DBG(CAB_GW_ERR, "mq_open for Remote Diag Response queue fail with error %s", strerror(errno));
		FUNC_EXIT
		return MQ_OPEN_ERROR;
	}

	FUNC_EXIT
	return retCode;
}

/*NOTE: Don't use cab logs in this file.*/
static returnCode_e setupMsgQueue()
{
	returnCode_e retCode = GEN_SUCCESS;
	FUNC_ENTRY

	retCode = rDiagReqQueueSetup();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "rDiagReqQueueSetup() fail with error %d", retCode);
		FUNC_EXIT
		return retCode;
	}
	retCode = rDiagResQueueSetup();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "rDiagResQueueSetup() fail with error %d", retCode);
		FUNC_EXIT
		return retCode;
	}
	FUNC_EXIT
	return retCode;
}

/*NOTE: Don't use cab logs in this file.*/
static void *rDiagModuleResHandler(void *arg)
{
	ssize_t bytes_read;
	remoteDiagWrapper_t msg;

	FUNC_ENTRY

	CAB_DBG(CAB_GW_DEBUG, "Remote Diag Response handler thread id : %ld", syscall(SYS_gettid));

	while(1)
	{
		memset(&msg, '\0', sizeof(remoteDiagWrapper_t));
		pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
		bytes_read = mq_receive(mqRDiagResQueue, (char *)&(msg), sizeof(remoteDiagWrapper_t), NULL);
		pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
		if(bytes_read >= 0)
		{
			/* Handle it in the MNT because we do not want to read/write any configuration and/or
			 *  status parameters when cabapp is in dinit state. */
			(*g_notifFunPtr)((void *)&msg);
		}
	}
	FUNC_EXIT
}


void sendMsgToMsgQ(void *messageTobeSent)
{
        FUNC_ENTRY

	char errorMsg[100] = "";
        struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0};
        clock_gettime(CLOCK_REALTIME, &abs_timeout);
        abs_timeout.tv_sec += MQ_SEND_TOUT_SEC;

        if(!messageTobeSent)
        {
                CAB_DBG(CAB_GW_ERR, "Invalid argument");
                return;
        }

        if (mq_timedsend(mqRDiagReqQueue, (const char *)messageTobeSent, sizeof(remoteDiagWrapper_t), 0, &abs_timeout) < 0) {
		perror("MQ send fail rDiag");
		sprintf(errorMsg, "mq_timedsend for %s failed with error %s", RDIAG_REQ_QUEUE, strerror(errno));
	//	rDiagSendMessage(MESSAGE_QUEUE_OVERFLOW, GW_WARNING, errorMsg);
		CAB_DBG(CAB_GW_ERR, "message queue send failed");
	}

        FUNC_EXIT
}

/*NOTE: Don't use cab logs in this file.*/
returnCode_e rDiagInit()
{
	FUNC_ENTRY
	if (cabCBInit(&g_rdiagBufHandle, RDIAG_BUF_MAX_SIZE, sizeof(remoteDiagInfoStore_t)) != 0) {
		printf("[%s]:[%d] Remote diag init failed\r\n", basename(__FILE__), __LINE__);
		FUNC_EXIT
		return MODULE_INIT_FAIL;
	}
	FUNC_EXIT
	return GEN_SUCCESS;
}
returnCode_e rDiagModInit(rDiagModuleResHandlerCB funPtr)
{
	FUNC_ENTRY

	if (funPtr == NULL) {
		CAB_DBG(CAB_GW_ERR, "Invalid arg");
		return MODULE_INIT_FAIL;
	}

	if (setupMsgQueue() != GEN_SUCCESS) {
		printf("message queue setup failed\r\n");
		return MODULE_INIT_FAIL;
	}

	g_notifFunPtr = funPtr;

	return GEN_SUCCESS;
	FUNC_EXIT
}

returnCode_e rdiagDeinit()
{
	FUNC_ENTRY
	returnCode_e retCode;

	retCode = cabCBDeinit(&g_rdiagBufHandle);

	if(retCode != GEN_SUCCESS)
	{
		printf("cabCBDeinit() fail with error %d\r\n", retCode);
	}

	FUNC_EXIT
	return retCode;
}

returnCode_e rDiagModDeinit()
{
	FUNC_ENTRY
	returnCode_e retCode = GEN_SUCCESS;

	rDiagStopResponseQueue();

	/* queue cleanup */
	/* Don't unlink the queue as remoteDiagApp is reading/writing to this queue. */
	mq_close(mqRDiagReqQueue);
	// mq_unlink(RDIAG_REQ_QUEUE);
	mq_close(mqRDiagResQueue);
	// mq_unlink(RDIAG_RES_QUEUE);

	FUNC_EXIT
	return retCode;
}

#if defined(CABAPPA7)
/*NOTE: Don't use cab logs in this file.*/
returnCode_e rDiagAddMessage(time_t logTime,  debugLevel_e msgLevel, char *message)
{
	FUNC_ENTRY
	returnCode_e retCode = GEN_SUCCESS;
	remoteDiagInfoStore_t messageToBeAdded;
	remoteDiagInfoStore_t *tempRDiagInfo=NULL;

	if ((message == NULL) || (msgLevel > MAX_GW_DBG) || (msgLevel < GW_ERR)) {
		return GEN_INVALID_ARG;
	}

	if(strlen(message) < MAX_LEN_REMOTE_DIAG_MSG) {
		strcpy(messageToBeAdded.msg, message);
	} else {
		return GEN_INVALID_ARG;
	}

	messageToBeAdded.level = msgLevel;
	memcpy(&(messageToBeAdded.time), &logTime, sizeof(time_t));

	if(cabCBGetBufs(g_rdiagBufHandle, (char *)&tempRDiagInfo, 1) != 0) {
		return GEN_API_FAIL;
	}

	memcpy((char *)tempRDiagInfo, (char*)&messageToBeAdded, sizeof(remoteDiagInfoStore_t));
	FUNC_EXIT
	return retCode;
}
#endif

void sendGatewayIdToRdiag(initConfig_t *initConfig)
{

	FUNC_ENTRY

	remoteDiagWrapper_t msgTobeSent;
	memset(&msgTobeSent, '\0', sizeof(remoteDiagWrapper_t));

	msgTobeSent.type = GW_ID_RES;
	memcpy(&(msgTobeSent.sendInitConfig), initConfig, sizeof(initConfig));

	sendMsgToMsgQ((void *)&msgTobeSent);
	FUNC_EXIT
}

returnCode_e rDiagStartResponseQueue()
{
	FUNC_ENTRY
	returnCode_e retCode = GEN_SUCCESS;

	retCode = pthread_create(&rDiagModResHandlerThreadID_t, NULL, &rDiagModuleResHandler, NULL);
	if (retCode != GEN_SUCCESS)    
	{
		CAB_DBG(CAB_GW_ERR, "thread creation failed for remote diag response handler with code %d", retCode);
	}

	return retCode;
	FUNC_EXIT
}

void rDiagStopResponseQueue()
{
	FUNC_ENTRY
	if (rDiagModResHandlerThreadID_t != NULL)
	{
		pthread_cancel(rDiagModResHandlerThreadID_t);
	}
	FUNC_EXIT
}
void rDiagSendLTEDebug(lteDiagInfo_t *lteDiagInfo)
{

	FUNC_ENTRY

	remoteDiagWrapper_t messageTobeSent;
	messageTobeSent.type = LTE_DIAG;

	memcpy(&(messageTobeSent.sendLTEDebug), lteDiagInfo, sizeof(lteDiagInfo_t));

	sendMsgToMsgQ((void *)&messageTobeSent);
	FUNC_EXIT
}

void rDiagSendInitConf(uint16_t maxZipCount, char *cloudURI, uint8_t *gatewayId, char *softwareVersion)
{

	FUNC_ENTRY

	remoteDiagWrapper_t messageTobeSent;
	messageTobeSent.type = INIT_CONFIG;
	
	messageTobeSent.sendInitConfig.maxZipCount = maxZipCount;
	memcpy(messageTobeSent.sendInitConfig.cloudURI, cloudURI, sizeof(messageTobeSent.sendInitConfig.cloudURI));
	memcpy(messageTobeSent.sendInitConfig.gatewayId, gatewayId, sizeof(messageTobeSent.sendInitConfig.gatewayId));
	memcpy(messageTobeSent.sendInitConfig.softwareVersion, softwareVersion, sizeof(messageTobeSent.sendInitConfig.softwareVersion));

	sendMsgToMsgQ((void *)&messageTobeSent);
	FUNC_EXIT
}

void rDiagSendFactoryResetCmd(void)
{
	FUNC_ENTRY

	remoteDiagWrapper_t messageTobeSent;
	memset(&messageTobeSent, '\0', sizeof(remoteDiagWrapper_t));

	messageTobeSent.type = FACTORY_RESET_FROM_CABAPP;

	sendMsgToMsgQ((void *)&messageTobeSent);
	FUNC_EXIT
}

# if defined(CABAPPA7)
/*NOTE: Don't use cab logs in this file.*/
void rDiagSendMessage(remoteDiagError_e errorType, debugLevel_e msgLevel, char* errorExtraMessage) 
{
	time_t errorTime;
	FUNC_ENTRY

	time(&errorTime);
	remoteDiagWrapper_t messageTobeSent;
	messageTobeSent.type = RDIAG_MSG;
	if ((errorExtraMessage == NULL) || (msgLevel > MAX_GW_DBG) || (msgLevel < GW_ERR)) {
		printf("Invalid argument\r\n");
		return GEN_INVALID_ARG;
	}

	messageTobeSent.sendRDiagMsg.level = msgLevel;
	messageTobeSent.sendRDiagMsg.errorType = errorType;
	memcpy(&(messageTobeSent.sendRDiagMsg.time), &errorTime, sizeof(time_t));
	strcpy(&(messageTobeSent.sendRDiagMsg.msg), errorExtraMessage);
	if (cabCBGetCount(g_rdiagBufHandle, &messageTobeSent.sendRDiagMsg.noOfRDiagLogs) != 0) {
		printf("Get count failed for circular buffer\r\n");
		return GEN_API_FAIL;
	}

	if(cabCBFetchData(g_rdiagBufHandle, &(messageTobeSent.sendRDiagMsg.rDiagLogs), messageTobeSent.sendRDiagMsg.noOfRDiagLogs, 0) != 0) {
		printf("Fetch data failed\r\n");
		return GEN_API_FAIL;
	}

	// printf("\nrdiag send message : %s\r\n",messageTobeSent.sendRDiagMsg.msg);

	sendMsgToMsgQ((void *)&messageTobeSent);
	FUNC_EXIT

}
#else
void rDiagSendMessage(remoteDiagError_e errorType, debugLevel_e msgLevel, char* errorExtraMessage)
{
	FUNC_ENTRY
	/* Not going to send message to remoteDiag app if it is not from cabApp */
	FUNC_EXIT
}
#endif

