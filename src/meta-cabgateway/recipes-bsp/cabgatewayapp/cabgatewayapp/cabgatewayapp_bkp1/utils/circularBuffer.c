/*******************************************************************************
 * Copyright(c) <2016>, Volansys Technologies
 *
 * Description: 
 * @file circularBuffer.c
 * @brief This file contain API for circular buffer module
 * 
 * @Author   - Volansys
 *
 *******************************************************************************
 *
 * @History
 *
*
 * Nov/22/2016, Vivek Rajpara   Updated code for overlapping logic
 * Nov/21/2016, Vivek Rajpara   Added code for circular buffer
 *
 ******************************************************************************/
#include "stdio.h"
#include "stdint.h"
#include "circularBuffer.h"
#include "debug.h"

/******************************************************************************
 * @brief Circular buffer initialization
 * 		This function is used to initialize circular buffer.
 * @param[in] cirBuffHandle Pointer to handle to be initialized.
 * @param[in] noOfBufs    Number of buffers to be used in circular buffer.
 * @param[in] sizeOfBuf   Sizo of one buffer.
 * @return 	Error Code
 ******************************************************************************/
int cabCBInit(CAB_CIRCULAR_BUFFER_t **cirBuffHandle, uint32_t noOfBufs, size_t sizeOfBuf)
{
    FUNC_ENTRY
    if (cirBuffHandle == NULL) {
	printf("Invalid Argument\r\n");
	return 1;
    }

    if ((*cirBuffHandle) != NULL) {
	printf("Invalid argument : Circular buffer handle should be NULL\r\n");
	return 1;
    }

    if ( noOfBufs == 0 || sizeOfBuf == 0 ) {
	printf("Invalid Argument\r\n");
	return 1;
    }

    /* Allocate memory for circular buffer handle */
    (*cirBuffHandle) = (CAB_CIRCULAR_BUFFER_t *) malloc (sizeof(CAB_CIRCULAR_BUFFER_t));
    /* Check for available memory */
    if ( (*cirBuffHandle) == NULL ) {
	printf("Not enough memory\r\n");
	return 1;
    }

    /* Allocate buffer as per buffer size and counts */
    (*cirBuffHandle)->bufferStart = calloc(noOfBufs , sizeOfBuf);
    /* check available memory for buffer or not */
    if ( (*cirBuffHandle)->bufferStart == NULL ) {
	printf("Not enough memory\r\n");
	return 1;
    }
       
    /* Initialize parameters of the handle */
    (*cirBuffHandle)->bufferEnd = (char *)(*cirBuffHandle)->bufferStart + noOfBufs * sizeOfBuf;
    (*cirBuffHandle)->noOfBufs = noOfBufs;
    (*cirBuffHandle)->sizeOfBuf = sizeOfBuf;
    (*cirBuffHandle)->currBufCounts = 0;
    (*cirBuffHandle)->head = (*cirBuffHandle)->bufferStart;
    (*cirBuffHandle)->tail = (*cirBuffHandle)->bufferStart;
    (*cirBuffHandle)->readCBMutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;
    (*cirBuffHandle)->writeCBMutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;
    (*cirBuffHandle)->cirBufCountMutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;
    FUNC_EXIT
    return 0;
}

/******************************************************************************
 * @brief Circular buffer initialization
 * 		This function is used to deinitialize circular buffer.
 * @param[in] cirBuffHandle Pointer to handle to be deinitialized.
 * @return    Error Code
 ******************************************************************************/
int cabCBDeinit(CAB_CIRCULAR_BUFFER_t ** cirBuffHandle)
{
    FUNC_ENTRY
    if (cirBuffHandle == NULL || (*cirBuffHandle) == NULL) {
	printf("Invalid Argument\r\n");
	return 1;
    }
    
    /* Free Memory allocated for circular buffer */
    free((*cirBuffHandle)->bufferStart);
    free((*cirBuffHandle));
    FUNC_EXIT
    return 0;
}

/******************************************************************************
 * @brief Get buffers from circular buffer
 * 		This function is used to get buffers from circular buffer to 
           Push data into memory of the circular buffer.
 * @param[in] cirBuffHandle Pointer to handle of the circular buffer.
 * @param[in] item  Array of buffer items
 * @param[in] numberOfItems Number of items to be pushed.
 * @return Error Code
 ******************************************************************************/
int cabCBGetBufs(CAB_CIRCULAR_BUFFER_t * cirBuffHandle, void *item[], int numberOfItems)
{
    int i = 0;

    FUNC_ENTRY 
    if (cirBuffHandle == NULL || numberOfItems == 0 || item == NULL) {
	printf("Invalid Argument\r\n");
	return 1;
    }

    pthread_mutex_lock(&(cirBuffHandle->writeCBMutex));
    for ( i=0; i<numberOfItems; i++ ) {

        pthread_mutex_lock(&(cirBuffHandle->cirBufCountMutex));
        if (cirBuffHandle->currBufCounts == cirBuffHandle->noOfBufs) {
            cirBuffHandle->currBufCounts--;
            cirBuffHandle->tail = (char*)cirBuffHandle->tail + cirBuffHandle->sizeOfBuf;
            if(cirBuffHandle->tail == cirBuffHandle->bufferEnd) {
                cirBuffHandle->tail = cirBuffHandle->bufferStart;
	    }
        }
        pthread_mutex_unlock(&(cirBuffHandle->cirBufCountMutex));

        item[i] = cirBuffHandle->head;
        cirBuffHandle->head = (char*)cirBuffHandle->head + cirBuffHandle->sizeOfBuf;

    	if(cirBuffHandle->head == cirBuffHandle->bufferEnd) {
        	cirBuffHandle->head = cirBuffHandle->bufferStart;
        }

        pthread_mutex_lock(&(cirBuffHandle->cirBufCountMutex));
    	cirBuffHandle->currBufCounts++;
        pthread_mutex_unlock(&(cirBuffHandle->cirBufCountMutex));
    }
    pthread_mutex_unlock(&(cirBuffHandle->writeCBMutex));
    FUNC_EXIT
    return 0;
}



/******************************************************************************
 * @brief Fetch Data from circular buffer
 * 		This function is used to pop data from circular buffer.
 * @param[in] cirBuffHandle Pointer to handle of the circular buffer.
 * @param[out] item  Array of buffer items
 * @param[in] numberOfItems Number of items to be pushed.
 * @param[in] overlapValue This flag takes care of overlapping and gives updates 
                           tail by only positions given in ovelapValue regardless
                           of the number of items to be fetched.
 * @return Error Code
 ******************************************************************************/
int cabCBFetchData(CAB_CIRCULAR_BUFFER_t *cirBuffHandle, void *item, int numberOfItems, uint8_t overlapValue)
{
    int i=0;
    void * tempOverLapTail = NULL;

    FUNC_ENTRY 

    if (cirBuffHandle == NULL || numberOfItems == 0 || item == NULL || overlapValue > numberOfItems ) {
	printf("Invalid Argument\r\n");
	return 1;
    }

    if (overlapValue == numberOfItems) {
	overlapValue = 0;
    }

    pthread_mutex_lock(&(cirBuffHandle->readCBMutex));

    for ( i=0; i<numberOfItems; i++ ) {
        pthread_mutex_lock(&(cirBuffHandle->cirBufCountMutex));
        if(cirBuffHandle->currBufCounts == 0){
            printf("Buffer is empty\r\n");
            pthread_mutex_unlock(&(cirBuffHandle->cirBufCountMutex));
            pthread_mutex_unlock(&(cirBuffHandle->readCBMutex));
            return 1;
        }
        pthread_mutex_unlock(&(cirBuffHandle->cirBufCountMutex));

        memcpy(item+i*cirBuffHandle->sizeOfBuf, cirBuffHandle->tail, cirBuffHandle->sizeOfBuf);
        cirBuffHandle->tail = (char*)cirBuffHandle->tail + cirBuffHandle->sizeOfBuf;

        if(cirBuffHandle->tail == cirBuffHandle->bufferEnd) {
            cirBuffHandle->tail = cirBuffHandle->bufferStart;
	}

        if ( overlapValue != 0 && i < overlapValue ) {
		tempOverLapTail = cirBuffHandle->tail;
        }
        if (overlapValue == 0) {
            pthread_mutex_lock(&(cirBuffHandle->cirBufCountMutex));
            cirBuffHandle->currBufCounts--;
            pthread_mutex_unlock(&(cirBuffHandle->cirBufCountMutex));
        }
    }

    if (overlapValue != 0) {
        pthread_mutex_lock(&(cirBuffHandle->cirBufCountMutex));
	cirBuffHandle->currBufCounts -= overlapValue;
        pthread_mutex_unlock(&(cirBuffHandle->cirBufCountMutex));
        cirBuffHandle->tail = tempOverLapTail;
    }

    pthread_mutex_unlock(&(cirBuffHandle->readCBMutex));

    FUNC_EXIT

    return 0;
}


/******************************************************************************
 * @brief Get buffer counter
 *              This function is used to get current buffer counter
 * @param[in] cirBuffHandle Pointer to handle of the circular buffer.
 * @param[out] counter      Pointer to fill up the current circular buffer counter
 * @return Error Code
 ******************************************************************************/
int cabCBGetCount ( CAB_CIRCULAR_BUFFER_t *cirBuffHandle, uint32_t * counter )
{
	FUNC_ENTRY
	if (cirBuffHandle == NULL || counter == NULL) {
		printf("Invalid Argument\r\n");
		return 1;
	}
        pthread_mutex_lock(&(cirBuffHandle->cirBufCountMutex));
	*counter = cirBuffHandle->currBufCounts;
        pthread_mutex_unlock(&(cirBuffHandle->cirBufCountMutex));
	FUNC_EXIT
	return 0;
}
