/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file sysTimer.c
 * @brief System timer code 
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Sep/18/2018  VT Added first draft
***************************************************************/


//*** Place all include files here ***
#include <stdio.h>
#include <signal.h>
#include <malloc.h>
#include <pthread.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <strings.h>
#include <sys/time.h>

#include "sysTimer.h"
#include "debug.h"

//******** Extern Variables **********


//****** Defines and Data Types ******
typedef struct {
    TIMER_INFO_t            timerInfo;      // timer information structure
    uint32_t                currCount;      // current count of timer
} SYS_TIMER_INFO_t;

struct TIMER_LIST_NODE {
    struct TIMER_LIST_NODE  * next;         // next node pointer
    struct TIMER_LIST_NODE  * prev;         // previous node pointer
    SYS_TIMER_INFO_t        sysTimer;       // system timer structure
};

#define MAX_ONE_MIN_FUNC    (10)

//******** Function Prototypes *******



//******** Global Variables **********



//******** Static Variables **********
static uint32_t             sysTick;            // keeps count of system ticks @ 100 mSec
static pthread_rwlock_t     sysTickMutex;

static SYS_TIMER_LIST_t *   sysTimerListHead;   // start node address of timer list
static SYS_TIMER_LIST_t *   sysTimerListTail;   // end node address of timer list

static pthread_mutex_t      resourceMutex = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;

static uint8_t                oneMinFunCnt;
static ONE_MIN_NOTIFY_t     oneMinNotify[MAX_ONE_MIN_FUNC];

static SYS_TIMER_LIST_t *   nextNodePtr;            // next node in list after current node
static bool                 sysTmrCallBackActiveF;  // indicates if call back process is active

/****************************************************************
               Timer handle only for linux systems - start
******************************************************************/
timer_t timerid;
void timerHandler(int sig)
{
    /* Note: calling printf() from a signal handler is not safe
       (and should not be done in production programs), since
       printf() is not async-signal-safe; see signal-safety(7).
       Nevertheless, we use printf() here as a simple way of
       showing that the handler was called. */
    //printf("\r\n-S-\r\n");
    runSysTimer();
}

TIMER_STATUS_e linuxSysTimerInit(uint32_t resSeconds)
{
    TIMER_STATUS_e retStatus = TIMER_SUCCESS;
    struct sigevent sev;
    struct itimerspec its;
    long long freq_nanosecs;

    /* Create the timer */
    sev.sigev_notify = SIGEV_SIGNAL;
    sev.sigev_signo = TIMER_SIG;
    sev.sigev_value.sival_ptr = &timerid;
    if (timer_create(CLOCKID, &sev, &timerid) == -1) {
	return TIMER_SYS_API_FAIL;
    }

    //printf("timer ID is 0x%lx\n", (long) timerid);

    /* Start the timer */
    freq_nanosecs = resSeconds;
    its.it_value.tv_sec = freq_nanosecs;
    its.it_value.tv_nsec = 0;
    its.it_interval.tv_sec = its.it_value.tv_sec;
    its.it_interval.tv_nsec = its.it_value.tv_nsec;

    if (timer_settime(timerid, 0, &its, NULL) == -1) {
	return TIMER_SYS_API_FAIL;
    }
    return retStatus;
}

/****************************************************************
               Timer handle only for linux systems - end
******************************************************************/




//*****************************************************************************
//  initSysTimer()
//  Param:
//      IN :    NONE
//      OUT:    NONE
//  Returns:
//              NONE
//  Description:
//      This function initializes all system timer related parameters.
//      It needs to be called during start-up of the program.
//
//  [Pre-condition:]
//      NONE
//  [Constraints:]
//      NONE
//
//*****************************************************************************
TIMER_STATUS_e initSysTimer(uint32_t resSeconds)
{
    uint8_t   cnt;
    TIMER_STATUS_e retStatus = TIMER_SUCCESS;
    FUNC_ENTRY

    // reset system tick counter
    sysTick = 0;
    pthread_rwlock_init(&sysTickMutex, NULL);

    // reset all link list node pointers
    sysTimerListHead = NULL;
    sysTimerListTail = NULL;
    nextNodePtr = NULL;

    sysTmrCallBackActiveF = false;

    oneMinFunCnt = 0;

    for(cnt = 0; cnt < MAX_ONE_MIN_FUNC; cnt++) {
        oneMinNotify[cnt].funcPtr = NULL;
        oneMinNotify[cnt].userData = 0;
    }

    retStatus = linuxSysTimerInit(resSeconds);
    FUNC_EXIT
    return retStatus;
}

//*****************************************************************************
//  runSysTimer()
//  Param:
//      IN :    NONE
//      OUT:    NONE
//  Returns:
//              NONE
//  Description:
//      This function traverses through timer lists and updates counts of the
//      active timers. If any of the timers from the list expires, it calls the
//      callback function registered with that timer. This function is called
//      upon the receipt of SIG_ALRM at interval of 100mSec.
//
//  [Pre-condition:]
//      initSysTimer() : System timer must have been initialized.
//  [Constraints:]
//      This function needs to run in super-loop. so accuracy varies with the
//      execution time of the loop.
//
//*****************************************************************************
void runSysTimer(void)
{
    FUNC_ENTRY

    SYS_TIMER_LIST_t * currNodePtr;
    //SYS_TIMER_LIST_t * tempNodePtr;

    // set node pointer to beginning of list and set active call-back flag
    currNodePtr = sysTimerListHead;

    // lock the node, in order to prevent multiple simultaneous access to timer list.
    pthread_mutex_lock(&resourceMutex);

    sysTmrCallBackActiveF = true;

    // traverse through the list until end of the list is reached
    while(currNodePtr != NULL) {
        // Store next node pointer first since it can be deleted
        // by callback function of current node.
        nextNodePtr = currNodePtr->next;

    	// increment current count of timer, if it is greater than or equal to user count,
    	// reset current count, invoke the call-back function and pass the data to it
    	(currNodePtr->sysTimer.currCount)++;
	//printf("TR : %d %d\r\n", currNodePtr->sysTimer.currCount, currNodePtr->sysTimer.timerInfo.count);
        if(currNodePtr->sysTimer.currCount
                >= currNodePtr->sysTimer.timerInfo.count) {
            currNodePtr->sysTimer.currCount = 0;
            (*(currNodePtr->sysTimer.timerInfo.funcPtr))
            (currNodePtr->sysTimer.timerInfo.data);
        }
        // point address to next node
        currNodePtr = nextNodePtr;
    }

    sysTmrCallBackActiveF = false;

    // unlock the resource
    pthread_mutex_unlock(&resourceMutex);

    //pthread_rwlock_wrlock(&sysTickMutex);

    //sysTick++;

    //if((sysTick % 10) == 0) {
    //    pthread_rwlock_unlock(&sysTickMutex);

    //    getLocalTimeInBrokenTm(&localTime);

    //    if(prevMin != localTime.tm_min) {
    //        prevMin = localTime.tm_min;

    //        for(cnt = 0; cnt < MAX_ONE_MIN_FUNC; cnt++) {
    //            if(oneMinNotify[cnt].funcPtr != NULL) {
    //                oneMinNotify[cnt].funcPtr(oneMinNotify[cnt].userData);
    //            }
    //        }
    //    }
    //} else {
    //    pthread_rwlock_unlock(&sysTickMutex);
    //}
    FUNC_EXIT
}

//*****************************************************************************
//  startSystemTimer()
//  Param:
//      IN :    TIMER_INFO_t            // structure containing new timer info
//      OUT:    TIMER_HANDLE_t *        // Allocated handle to be used to reload/delete timer.
// INVALID_TMR_HANDLE in case of failure
//  Returns:
//              TIMER_SUCCESS                 // on successful creation of timer
//  Description:
//      This function starts a new timer with specified timer count and
//      register callback function by adding a node in timer list. On expiry
//      of the timer, user supplied callback function will be invoked along
//      with data supplied.
//      Timer handle returned to caller is nothing but address of timer node
//      in the list.
//
//  [Pre-condition:]
//      initSysTimer()  : System timer must have been initialized prior
//                        to starting any timer.
//  [Constraints:]
//      NONE
//
//*****************************************************************************
TIMER_STATUS_e startSystemTimer(TIMER_INFO_t timerInfo, TIMER_HANDLE * handle)
{
    FUNC_ENTRY
    TIMER_STATUS_e retVAl = TIMER_SUCCESS;
    // declare timer list pointers
    SYS_TIMER_LIST_t    * newNode;
    SYS_TIMER_LIST_t    **head;
    SYS_TIMER_LIST_t    **tail;

    // Make sure pointer where we return handle is not NULL
    if(handle != INVALID_TIMER_HANDLE) {
        // get memory from the pool for new timer to be created, store its address in new node pointer
        newNode = (SYS_TIMER_LIST_t *)malloc(sizeof(SYS_TIMER_LIST_t));

        // if no memory is available, NULL the timer handle and return fail status
        if(newNode != NULL) {
            // if memory is available, load user data to timer buffer
            newNode->sysTimer.currCount = 0;
            newNode->sysTimer.timerInfo.count = timerInfo.count;
            newNode->sysTimer.timerInfo.data = timerInfo.data;
            newNode->sysTimer.timerInfo.funcPtr = timerInfo.funcPtr;

            // initialize timer list pointers
            head = &sysTimerListHead;
            tail = &sysTimerListTail;

            // lock the resource to avoid simultaneous access
            pthread_mutex_lock(&resourceMutex);

            // if this is a first entry in list, insert it at beginning
            // otherwise insert it at the end of the list
            if(*head == INVALID_TIMER_HANDLE) {
                (*head) = newNode;
            } else {
                (*tail)->next = newNode;
            }
            newNode->next = NULL;
            newNode->prev = (*tail);
            (*tail) = newNode;

            // unlock the resource
            pthread_mutex_unlock(&resourceMutex);

            // store new node as timer handle
            *handle = newNode;
            retVAl = TIMER_SUCCESS;
        } else {
            *handle = INVALID_TIMER_HANDLE;
            retVAl = TIMER_MALLOC_FAIL;
            //printf("Memory Allocation Fails");
        }
    } else {
	retVAl = TIMER_INVLAID_ARG;
    }
    FUNC_EXIT
    return retVAl;
}

//*****************************************************************************
//  deleteTimer()
//  Param:
//      IN :    TIMER_HANDLE *          // timer handle
//      OUT:    NONE
//  Returns:
//              NONE
//  Description:
//      This function marks the timer to be deleted from the list.
//
//  [Pre-condition:]
//      NONE
//  [Constraints:]
//      NONE
//
//*****************************************************************************
void deleteTimer(TIMER_HANDLE * handlePtr)
{
    FUNC_ENTRY
    SYS_TIMER_LIST_t * tempNodePtr;

    // check if the handle pointer is invalid or not
    if(handlePtr != INVALID_TIMER_HANDLE) {
        // check if the timer handle is invalid or not
        if(*handlePtr != INVALID_TIMER_HANDLE) {
            // lock the timer list prior to search operation
            pthread_mutex_lock(&resourceMutex);

            // Start searching specified handle from head
            tempNodePtr = sysTimerListHead;

            // Traverse through the entire list
            while(tempNodePtr != NULL) {
                // if specified timer found then mark the delete flag,
                // else point to next timer
                if(tempNodePtr == *handlePtr) {
                    // if head is deleted then update head
                    if(tempNodePtr == sysTimerListHead) {
                        sysTimerListHead = tempNodePtr->next;

                        if(sysTimerListHead != NULL) {
                            sysTimerListHead->prev = NULL;
                        }
                    }
                    // if tail is deleted then update tail
                    else if(tempNodePtr == sysTimerListTail) {
                        sysTimerListTail = tempNodePtr->prev;
                        sysTimerListTail->next = NULL;
                    } else { // deleting node other than head & tail
                        if(tempNodePtr->prev != NULL) {
                            tempNodePtr->prev->next = tempNodePtr->next;
                        }
                        if(tempNodePtr->next != NULL) {
                            tempNodePtr->next->prev = tempNodePtr->prev;
                        }
                    }

                    // While timer task processes current node, it backs up for next node
                    // that is to be processed after the current node has been processed.
                    // So, if we needs to delete next node of current node then we need to
                    // update next node.
                    if((tempNodePtr == nextNodePtr) && (sysTmrCallBackActiveF == true)) {
                        nextNodePtr = tempNodePtr -> next;
                    }

                    free(tempNodePtr);
                    tempNodePtr = NULL;
                    *handlePtr = INVALID_TIMER_HANDLE;
                    break;
                } else {
                    // fetch next timer
                    tempNodePtr = tempNodePtr->next;
                }
            }

            // unlock timer list at end of search
            pthread_mutex_unlock(&resourceMutex);
        }
    }
    FUNC_EXIT
}

//*****************************************************************************
//  reloadTimer()
//  Param:
//      IN :    TIMER_HANDLE        // Timer Handle
//              uint32_t              // Reload Count
//      OUT:    NONE
//  Returns:
//              TIMER_SUCCESS             // on successful reload of count
//  Description:
//      This function reloads timer count, specified by handle.
//
//  [Pre-condition:]
//      NONE
//  [Constraints:]
//      NONE
//
//*****************************************************************************
returnCode_e reloadTimer(TIMER_HANDLE handle, uint32_t count)
{
    FUNC_ENTRY
    returnCode_e status = TIMER_SUCCESS;
    // declare node pointer
    SYS_TIMER_LIST_t    * tempNodePtr;

    // check handle is not invalid, if it is then return with fail status
    if(handle != INVALID_TIMER_HANDLE) {
        pthread_mutex_lock(&resourceMutex);
        // traverse through the timer list from the beginning,
	if (sysTimerListHead == NULL) {
	    status = TIMER_LIST_EMPTY;
	} else {
            tempNodePtr = sysTimerListHead;
       	    while(tempNodePtr != NULL) {
       	        // find out the node specified by handle
       	        if(tempNodePtr == handle) {
       	            // load reload count to timer; reset current count to zero and return success
       	            handle->sysTimer.timerInfo.count = count;
       	            handle->sysTimer.currCount = 0;
       	            status = TIMER_SUCCESS;
       	            break;
       	        } else {
       	            // fetch next node
       	            tempNodePtr = tempNodePtr->next;
       	        }
       	    }
	}
        pthread_mutex_unlock(&resourceMutex);
    } else {
	status = TIMER_INVLAID_ARG;
    }

    FUNC_EXIT
    return status;
}

//*****************************************************************************
//  GetElapsedTime()
//  Param:
//      IN :    TIMER_HANDLE        // Timer Handle
//      OUT:    uint32_t              // timer count
//  Returns:
//              TIMER_SUCCESS             // on successful reload of count
//  Description:
//      Get Elapsed timer since started timer of that particluar handle.
//
//  [Pre-condition:]
//      NONE
//  [Constraints:]
//      NONE
//
//*****************************************************************************
TIMER_STATUS_e GetElapsedTime(TIMER_HANDLE handle, uint32_t * timeCnt)
{
    FUNC_ENTRY
    TIMER_STATUS_e status = TIMER_SUCCESS;
    // declare node pointer
    SYS_TIMER_LIST_t * tempNodePtr;

    // check handle is not invalid, if it is then return with fail status
    if(handle != INVALID_TIMER_HANDLE) {
        pthread_mutex_lock(&resourceMutex);
        // traverse through the timer list from the beginning,
        tempNodePtr = sysTimerListHead;

        while(tempNodePtr != NULL) {
            // find out the node specified by handle
            if(tempNodePtr == handle) {
                // load reload count to timer; reset current count to zero and return success
                *timeCnt = handle->sysTimer.currCount;
                status = TIMER_SUCCESS;
                break;
            } else {
                // fetch next node
                tempNodePtr = tempNodePtr->next;
            }
        }
        pthread_mutex_unlock(&resourceMutex);
    } else {
	status = TIMER_INVLAID_ARG;
    }
    FUNC_EXIT
    return status;
}

//*****************************************************************************
//  getRemainingTime()
//  Param:
//      IN :    TIMER_HANDLE        // Timer Handle
//      OUT:    uint32_t              // timer count
//  Returns:
//              TIMER_SUCCESS             // on successful reload of count
//  Description:
//      Get Remaining timer since started timer of that particluar handle.
//
//  [Pre-condition:]
//      NONE
//  [Constraints:]
//      NONE
//
//*****************************************************************************
TIMER_STATUS_e getRemainingTime(TIMER_HANDLE handle, uint32_t * timeCnt)
{
    FUNC_ENTRY
    TIMER_STATUS_e status = TIMER_SUCCESS;
    // declare node pointer
    SYS_TIMER_LIST_t * tempNodePtr;

    // check handle is not invalid, if it is then return with fail status
    if(handle != INVALID_TIMER_HANDLE) {
        pthread_mutex_lock(&resourceMutex);
        // traverse through the timer list from the beginning,
        tempNodePtr = sysTimerListHead;

        while(tempNodePtr != NULL) {
            // find out the node specified by handle
            if(tempNodePtr == handle) {
                if(handle->sysTimer.timerInfo.count >= handle->sysTimer.currCount) {
                    // load reload count to timer; reset current count to zero and return success
                    *timeCnt = (handle->sysTimer.timerInfo.count - handle->sysTimer.currCount);
                    status = TIMER_SUCCESS;
                } else {
                    *timeCnt = 0;
                }
                break;
            } else {
                // fetch next node
                tempNodePtr = tempNodePtr->next;
            }
        }
        pthread_mutex_unlock(&resourceMutex);
    } else {
    	// if handle is invalid return with fail status
	status = TIMER_INVLAID_ARG;
    }
    FUNC_EXIT
    return status;
}

//*****************************************************************************
//  getSysTick()
//  Param:
//      IN :    NONE
//      OUT:    NONE
//  Returns:
//              uint32_t      // current system timer ticks
//  Description:
//      This function returns current value of system ticks.
//
//  [Pre-condition:]
//      runSysTimer() : System timer must be in running state.
//  [Constraints:]
//      NONE
//
//*****************************************************************************
uint32_t getSysTick(void)
{
    FUNC_ENTRY
    uint32_t  temp;

    pthread_rwlock_rdlock(&sysTickMutex);
    temp = sysTick;
    pthread_rwlock_unlock(&sysTickMutex);

    FUNC_EXIT
    // return the count of system timer ticks
    return(temp);
}

//*****************************************************************************
//  elapsedTick()
//  Param:
//      IN :    uint32_t      // reference tick
//      OUT:    NONE
//  Returns:
//              uint32_t      // count of ticks since marked tick
//  Description:
//      This function returns the count of system ticks since the mark tick.
//
//  [Pre-condition:]
//      runSysTimer() : System timer must be in running state.
//  [Constraints:]
//      NONE
//
//*****************************************************************************
uint32_t elapsedTick(uint32_t markTick)
{
    FUNC_ENTRY
    uint32_t  temp;

    pthread_rwlock_rdlock(&sysTickMutex);

    // return the difference between current system tick and marked tick
    if(sysTick < markTick) {
        temp = (sysTick + (0xFFFFFFFF - markTick));
    } else {
        temp = (sysTick - markTick);
    }

    pthread_rwlock_unlock(&sysTickMutex);

    FUNC_EXIT
    return(temp);
}

//*****************************************************************************
//  registerOnMinFun()
//  Param:
//      IN :    ONE_MIN_NOTIFY_t *
//      OUT:    NONE
//  Returns:
//              bool
//  Description:
//      This function registers one min function which is call @ every actual
//      minute modified.
//
//  [Pre-condition:]
//      runSysTimer() : System timer must be in running state.
//  [Constraints:]
//      NONE
//
//*****************************************************************************
TIMER_STATUS_e registerOnMinFun(ONE_MIN_NOTIFY_t * oneMinFun)
{
    FUNC_ENTRY
    TIMER_STATUS_e retVal = TIMER_SUCCESS;

    if(oneMinFunCnt < MAX_ONE_MIN_FUNC) {
        oneMinNotify[oneMinFunCnt].funcPtr = oneMinFun->funcPtr;
        oneMinNotify[oneMinFunCnt].userData = oneMinFun->userData;
        oneMinFunCnt++;
    } else {
	retVal = TIMER_RESOURCE_LIMIT_ERR;
        //printf("One min timer count reach max");
    }
    FUNC_EXIT
    return retVal;
}

//***************************************************************************
//
//
//
//
//
//***************************************************************************
void getLocalTimeInBrokenTm(struct tm *localTime)
{
    FUNC_ENTRY

    struct tm *timeinfo;
    time_t rawtime;

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    localTime->tm_min = timeinfo->tm_min;

    FUNC_EXIT
}
