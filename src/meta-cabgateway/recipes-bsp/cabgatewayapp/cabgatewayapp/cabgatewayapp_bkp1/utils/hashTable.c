/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : hashTable.c
 * @brief : Hash Table Library implementation
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Aug/30/2018, VT , DBG Prints for error.
 * Aug/22/2018, VT , Modular approach and mutex protection.
 * Aug/14/2018, VT , Added initial version of the Hash
 **************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include "hashTable.h"
#include "debug.h"

static hashItem_t dummyHashItem = {.key = DUMMY_DATA, .data = NULL};

/* Hash code logic */
static uint32_t hashCode(uint32_t key, uint16_t hashSize)
{
    FUNC_ENTRY
    return (key % hashSize);
    FUNC_EXIT
}

/* initialize hash table by allocating resources and filling with default data. */
HASH_STATUS initHashTable(hashHandle_t **hashHandle, uint16_t hashSize, size_t hashDataSize)
{
    HASH_STATUS retCode = HASH_SUCCESS;
    uint16_t hashArrInd = 0;
    FUNC_ENTRY
    /* Validate input arguments */
    if(hashHandle == NULL || hashSize == 0 || hashDataSize == 0) {
        retCode = HASH_INVLID_ARG;
        CAB_DBG(CAB_GW_ERR, "Hash invalid args in func : %s", __FUNCTION__);
        goto initHashExit;
    }

    /* Allocate handle */
    *hashHandle = (hashHandle_t *) malloc(sizeof(hashHandle_t));
    if((*hashHandle) == NULL) {
        retCode = HASH_MALLOC_ERROR;
        CAB_DBG(CAB_GW_ERR, "Hash malloc error in func : %s", __FUNCTION__);
        goto initHashExit;
    }
    (*hashHandle)->hashMutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;

    pthread_mutex_lock(&((*hashHandle)->hashMutex));
    /* Init hash resources */
    (*hashHandle)->hashArray = (hashItem_t **)malloc(hashSize * sizeof(hashItem_t*));
    if((*hashHandle)->hashArray == NULL) {
        retCode = HASH_MALLOC_ERROR;
        CAB_DBG(CAB_GW_ERR, "Hash malloc error in func : %s", __FUNCTION__);
        goto initHashExit;
    }
    for(hashArrInd = 0; hashArrInd < hashSize; hashArrInd++) {
        (*hashHandle)->hashArray[hashArrInd] = NULL;
    }

    (*hashHandle)->totalHashSize = hashSize;
    (*hashHandle)->currHashSize = 0;
    (*hashHandle)->hashDataSize = hashDataSize;
    (*hashHandle)->isInit = 1;
    pthread_mutex_unlock(&((*hashHandle)->hashMutex));
initHashExit:
    FUNC_EXIT
    return retCode;
}

/* Deinit Hash Table and free all resources */
HASH_STATUS deInitHashTable(hashHandle_t *hashHandle)
{
    HASH_STATUS retCode = HASH_SUCCESS;
    uint16_t hashArrInd = 0;
    FUNC_ENTRY
    /* Validate input arguments */
    if(hashHandle == NULL) {
        retCode = HASH_INVLID_ARG;
        CAB_DBG(CAB_GW_ERR, "Hash invalid arg in func : %s", __FUNCTION__);
        goto deInitHashExit;
    }
    if(hashHandle->isInit != 1) {
        retCode = HASH_NOT_INIT;
        CAB_DBG(CAB_GW_ERR, "Hash not inited in func : %s", __FUNCTION__);
        goto deInitHashExit;
    }

    /* Free all allocated Hash Table resources. */
    pthread_mutex_lock(&(hashHandle->hashMutex));
    for(hashArrInd = 0; hashArrInd < hashHandle->totalHashSize; hashArrInd++) {
        if((hashHandle->hashArray[hashArrInd] != NULL) && (hashHandle->hashArray[hashArrInd]->key != DUMMY_DATA)) {
            free(hashHandle->hashArray[hashArrInd]->data);
            free(hashHandle->hashArray[hashArrInd]);
            hashHandle->hashArray[hashArrInd] = NULL;
        }
    }
    hashHandle->isInit = 0;
    hashHandle->currHashSize = 0;
    hashHandle->totalHashSize = 0;
    free(hashHandle->hashArray);
    pthread_mutex_unlock(&(hashHandle->hashMutex));
    pthread_mutex_destroy(&(hashHandle->hashMutex));
    free(hashHandle);
deInitHashExit:
    FUNC_EXIT
    return retCode;

}



/* Search in HASH table */
HASH_STATUS hashSearch(hashHandle_t *hashHandle, uint32_t key, void *outData)
{
    HASH_STATUS retCode = HASH_SUCCESS;
    uint32_t hashIndex = 0;
    uint16_t iterationCount = 0;

    FUNC_ENTRY

    /* Validate input arguments */
    if(hashHandle == NULL || outData == NULL || key == DUMMY_DATA) {
        retCode = HASH_INVLID_ARG;
        CAB_DBG(CAB_GW_ERR, "Hash invalid arg in func : %s", __FUNCTION__);
        return retCode;
    }
    if(hashHandle->isInit != 1) {
        retCode = HASH_NOT_INIT;
        CAB_DBG(CAB_GW_ERR, "Hash not inited in func : %s", __FUNCTION__);
        return retCode;
    }

    /* get the hash */
    hashIndex = hashCode(key, hashHandle->totalHashSize);

    pthread_mutex_lock(&(hashHandle->hashMutex));
    /* move in array until an empty */
    while(hashHandle->hashArray[hashIndex] != NULL) {

        if(hashHandle->hashArray[hashIndex]->key == key) {
            memcpy(outData, (hashHandle->hashArray[hashIndex])->data, hashHandle->hashDataSize);
            retCode = HASH_SUCCESS;
            goto hashSearchExit;
        }

        /* go to next cell */
        ++hashIndex;

        /* wrap around the table */
        hashIndex %= (hashHandle->totalHashSize);
        iterationCount++;
        if(hashHandle->totalHashSize == iterationCount) {
            retCode = HASH_KEY_NOT_FOUND;
            CAB_DBG(CAB_GW_ERR, "Hash Key not found in func : %s", __FUNCTION__);
            goto hashSearchExit;
        }

    }

    retCode = HASH_KEY_NOT_FOUND;
hashSearchExit:
    pthread_mutex_unlock(&(hashHandle->hashMutex));
    FUNC_EXIT
    return retCode;
}

/* Insert element in Hash Table */
HASH_STATUS hashInsert(hashHandle_t * hashHandle, uint32_t key, void *data)
{
    HASH_STATUS retCode = HASH_SUCCESS;
    uint16_t iterationCount = 0;
    uint32_t hashIndex = 0;

    FUNC_ENTRY
 
    if(hashHandle == NULL || data == NULL || key == DUMMY_DATA) {
        retCode = HASH_INVLID_ARG;
        CAB_DBG(CAB_GW_ERR, "Hash invalid arguments: %s", __FUNCTION__);
        return retCode;
    }

    if(hashHandle->isInit != 1) {
        retCode = HASH_NOT_INIT;
        CAB_DBG(CAB_GW_ERR, "Hash not init in func: %s", __FUNCTION__);
        return retCode;
    }


    hashItem_t *item = (hashItem_t*) malloc(sizeof(hashItem_t));
    if(item == NULL) {
        retCode = HASH_MALLOC_ERROR;
        CAB_DBG(CAB_GW_ERR, "Hash Malloc error in: %s", __FUNCTION__);
        return retCode;
    }
    memset(item, 0, sizeof(hashItem_t));
    item->data = (void *)malloc(hashHandle->hashDataSize);

    if(item->data == NULL) {
        retCode = HASH_MALLOC_ERROR;
        CAB_DBG(CAB_GW_ERR, "Hash Malloc error in: %s", __FUNCTION__);
	free(item);
	item = NULL;
        return retCode;
    }
    memset(item->data, 0, hashHandle->hashDataSize);

    memcpy(item->data, data, hashHandle->hashDataSize);
    item->key = key;

    /* get the hash */
    hashIndex = hashCode(key, hashHandle->totalHashSize);

    pthread_mutex_lock(&hashHandle->hashMutex);
    /* move in array until an empty or deleted cell */
    while( (hashHandle->hashArray[hashIndex] != NULL)  && (hashHandle->hashArray[hashIndex]->key != DUMMY_DATA) ) {
        /* go to next cell */
        ++hashIndex;

        /* wrap around the table */
        hashIndex %= (hashHandle->totalHashSize);

        iterationCount++;
        if(hashHandle->totalHashSize == iterationCount) {
            free(item->data);
            free(item);
            retCode = HASH_TABLE_FULL;
            goto hashInsertExit;
        }
    }

    CAB_DBG(CAB_GW_DEBUG, "Hash Added :%d, %x", hashIndex, item->key);
    hashHandle->hashArray[hashIndex] = item;
    hashHandle->currHashSize++;
hashInsertExit:
    pthread_mutex_unlock(&hashHandle->hashMutex);
    FUNC_EXIT
    return retCode;
}

/* Flush all elements from hash table*/

HASH_STATUS hashFlush(hashHandle_t *hashHandle)
{
    HASH_STATUS retCode = HASH_SUCCESS;
    uint16_t hashArrInd;
 
    FUNC_ENTRY

    if(hashHandle == NULL) {
        retCode = HASH_INVLID_ARG;
        CAB_DBG(CAB_GW_ERR, "Invalid Arg in func : %s", __FUNCTION__);
        return retCode;
    }
    if(hashHandle->isInit != 1) {
        retCode = HASH_NOT_INIT;
        CAB_DBG(CAB_GW_ERR, "Hash not init in func : %s", __FUNCTION__);
        return retCode;
    }
    pthread_mutex_lock(&(hashHandle->hashMutex));
    for(hashArrInd = 0; hashArrInd < hashHandle->totalHashSize; hashArrInd++) {
        if((hashHandle->hashArray[hashArrInd] != NULL) && (hashHandle->hashArray[hashArrInd]->key != DUMMY_DATA)) {
	    free(hashHandle->hashArray[hashArrInd]->data);
            free(hashHandle->hashArray[hashArrInd]);
            hashHandle->hashArray[hashArrInd] = NULL;
            hashHandle->currHashSize--;
        }
    }
    pthread_mutex_unlock(&(hashHandle->hashMutex));

    FUNC_EXIT
    return retCode;
}

/* Remove elelment from hash table */
HASH_STATUS hashDelete(hashHandle_t *hashHandle, uint32_t key)
{
    HASH_STATUS retCode = HASH_SUCCESS;
    uint32_t hashIndex = 0;
    uint16_t iterationCount = 0;

    FUNC_ENTRY

    if(hashHandle == NULL || key == DUMMY_DATA) {
        retCode = HASH_INVLID_ARG;
        CAB_DBG(CAB_GW_ERR, "Invalid Arg in func : %s", __FUNCTION__);
        return retCode;
    }
    if(hashHandle->isInit != 1) {
        retCode = HASH_NOT_INIT;
        CAB_DBG(CAB_GW_ERR, "Hash not init in func : %s", __FUNCTION__);
        return retCode;
    }

    /* get the hash */
    hashIndex = hashCode(key, hashHandle->totalHashSize);

    pthread_mutex_lock(&(hashHandle->hashMutex));
    /* move in array until an empty */
    while(hashHandle->hashArray[hashIndex] != NULL) {
        if((hashHandle->hashArray[hashIndex])->key == key) {
    	    CAB_DBG(CAB_GW_DEBUG, "Hash Removed :%d, %x", hashIndex, key);
            free(hashHandle->hashArray[hashIndex]->data);
            free(hashHandle->hashArray[hashIndex]);
            hashHandle->hashArray[hashIndex] = &dummyHashItem;
            hashHandle->currHashSize--;
            retCode = HASH_SUCCESS;
            goto hashDeleteExit;
        }

        //go to next cell
        ++hashIndex;

        //wrap around the table
        hashIndex %= (hashHandle->totalHashSize);
        iterationCount++;
        if(hashHandle->totalHashSize == iterationCount) {
            retCode = HASH_KEY_NOT_FOUND;
            CAB_DBG(CAB_GW_DEBUG, "Hash Key Not found in func : %s", __FUNCTION__);
            goto hashDeleteExit;
        }

    }

hashDeleteExit:
    pthread_mutex_unlock(&(hashHandle->hashMutex));

    FUNC_EXIT
    return retCode;
}


/* Get current filled up size of the hash table */
HASH_STATUS getSizeOfHashTable(hashHandle_t *hashHandle, uint16_t * currSize)
{
    HASH_STATUS retCode = HASH_SUCCESS;
    FUNC_ENTRY
    /* Validate input arguments */
    if(hashHandle == NULL) {
        retCode = HASH_INVLID_ARG;
        CAB_DBG(CAB_GW_ERR, "Hash invalid arg in func : %s", __FUNCTION__);
        return retCode;
    }
    if(hashHandle->isInit != 1) {
        retCode = HASH_NOT_INIT;
        CAB_DBG(CAB_GW_ERR, "Hash not inited in func : %s", __FUNCTION__);
        return retCode;
    }

    /* Get size of the hash Table */
    pthread_mutex_lock(&(hashHandle->hashMutex));
    *currSize = hashHandle->currHashSize;
    pthread_mutex_unlock(&(hashHandle->hashMutex));
    FUNC_EXIT
    return retCode;

}

/* API to update data of searhcedIndex data */
HASH_STATUS hashUpdateData(hashHandle_t *hashHandle, uint32_t key, void * inData)
{
    HASH_STATUS retCode = HASH_SUCCESS;
    uint32_t hashIndex = 0;
    uint16_t iterationCount = 0;

    FUNC_ENTRY

    /* Validate input arguments */
    if(hashHandle == NULL || inData == NULL || key == DUMMY_DATA) {
        retCode = HASH_INVLID_ARG;
        CAB_DBG(CAB_GW_ERR, "Hash invalid arg in func : %s", __FUNCTION__);
        return retCode;
    }
    if(hashHandle->isInit != 1) {
        retCode = HASH_NOT_INIT;
        CAB_DBG(CAB_GW_ERR, "Hash not inited in func : %s", __FUNCTION__);
        return retCode;
    }

    /* get the hash */
    hashIndex = hashCode(key, hashHandle->totalHashSize);

    pthread_mutex_lock(&(hashHandle->hashMutex));
    /* move in array until an empty */
    while(hashHandle->hashArray[hashIndex] != NULL) {

        if(hashHandle->hashArray[hashIndex]->key == key) {
            memcpy((hashHandle->hashArray[hashIndex])->data, inData, hashHandle->hashDataSize);
            retCode = HASH_SUCCESS;
            goto hashUpdateExit;
        }

        /* go to next cell */
        ++hashIndex;

        /* wrap around the table */
        hashIndex %= (hashHandle->totalHashSize);
        iterationCount++;
        if(hashHandle->totalHashSize == iterationCount) {
            retCode = HASH_KEY_NOT_FOUND;
            CAB_DBG(CAB_GW_ERR, "Hash Key not found in func : %s", __FUNCTION__);
            goto hashUpdateExit;
        }

    }

    retCode = HASH_KEY_NOT_FOUND;
hashUpdateExit:
    pthread_mutex_unlock(&(hashHandle->hashMutex));
    FUNC_EXIT
    return retCode;
}

/* Current data fetch API
   This API fetches all data from currently present nodes. */
HASH_STATUS fetchCurrentDataHashTable(hashHandle_t *hashHandle, void *data)
{
    HASH_STATUS retCode = HASH_SUCCESS;
    uint16_t hashArrInd = 0;
    uint16_t outputInd = 0;
    FUNC_ENTRY
    /* Validate input arguments */
    if(hashHandle == NULL) {
        retCode = HASH_INVLID_ARG;
        CAB_DBG(CAB_GW_ERR, "Hash invalid arg in func : %s", __FUNCTION__);
        return retCode;
    }
    if(hashHandle->isInit != 1) {
        retCode = HASH_NOT_INIT;
        CAB_DBG(CAB_GW_ERR, "Hash not inited in func : %s", __FUNCTION__);
        return retCode;
    }

    /* Feel all data in the hash table */
    pthread_mutex_lock(&(hashHandle->hashMutex));
    for(hashArrInd = 0; hashArrInd < hashHandle->totalHashSize; hashArrInd++) {
        if(hashHandle->hashArray[hashArrInd] != NULL) {
	    if (hashHandle->hashArray[hashArrInd]->data != NULL) {
		memcpy( (data + (outputInd * hashHandle->hashDataSize)), (hashHandle->hashArray[hashArrInd])->data, hashHandle->hashDataSize );
		outputInd++;
	    } 
        }
    }
    pthread_mutex_unlock(&(hashHandle->hashMutex));
    FUNC_EXIT
    return retCode;

}

/* TODO: Debug utility for hash debug to be removed afterwards. */
void display(hashHandle_t * hashHandle)
{
    int i = 0;
    FUNC_ENTRY
    for(i = 0; i < hashHandle->totalHashSize; i++) {

        if(hashHandle->hashArray[i] != NULL)
	    if (hashHandle->hashArray[i]->data != NULL) { 
	            printf(" (%x,%x)", hashHandle->hashArray[i]->key, *((uint32_t*)hashHandle->hashArray[i]->data));
	    } else {
		    printf(" (%x,Empty)",  hashHandle->hashArray[i]->key);
	    }
        else
            printf(" ~~ ");
    }

    printf("\n");
    FUNC_EXIT
}

