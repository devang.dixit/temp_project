/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file signalRegister.c
 * @brief Signal registration code
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Sep/18/2018  VT Added first draft
***************************************************************/

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include "debug.h"
#include "signalRegister.h"
#include "pthread.h"
#include "sysTimer.h"
#include "batModule.h"
#include "syscall.h"
#include "sub1.h"
#ifdef ACCEL_SUPPORT
#include "accel.h"
#endif


static sigset_t mask;
static uint8_t sigregd = 0;

pthread_t singalHandlerThreadId = (pthread_t)NULL;

// TODO: Send signal to signalRegister thread for watchdog
#if 0
static pthread_mutex_t signalHandlerAliveLock;
static bool g_signalHandlerAliveStatus = true;

static bool getSignalHandlerAliveStatus(void)
{
        bool status;
        pthread_mutex_lock(&signalHandlerAliveLock);
        status = g_signalHandlerAliveStatus;
        pthread_mutex_unlock(&signalHandlerAliveLock);
        return status;
}

static void setSignalHandlerAliveStatus(bool value)
{
        pthread_mutex_lock(&signalHandlerAliveLock);
        g_signalHandlerAliveStatus = value;
        pthread_mutex_unlock(&signalHandlerAliveLock);
}

static bool signalHandlerCallback(void)
{
        bool status = false;
        CAB_DBG(CAB_GW_DEBUG, "In func : %s\n", __func__);
        if(getSignalHandlerAliveStatus() == true)
        {
                status = true;
        }
        
        setSignalHandlerAliveStatus(false);

        return status;
}
#endif

void *signalHandlerThread(void * unused) 
{
    int signo = 0;
	int configfd;
	char buf[10];
#if 0
	watchDogTimer_t newWatchdogNode;
	
	newWatchdogNode.threadId = syscall(SYS_gettid);
	newWatchdogNode.CallbackFn = signalHandlerCallback;
	
	threadRegister(&newWatchdogNode);
#endif
	configfd = open(DEBUG_FS_FILE, O_WRONLY);
	if(configfd < 0) {
		CAB_DBG (CAB_GW_ERR, "Error opening file to write PID. \
				 	 	 	 	 	 Signal Handler failed so shutting down.\r\n");
	}

	sprintf(buf, "%i", syscall(SYS_gettid));

	if (write(configfd, buf, strlen(buf) + 1) < 0) {
		CAB_DBG (CAB_GW_ERR, "Error writing PID to sysfs file. \
									 Signal Handler failed so shutting down.\r\n");
		close(configfd);
	}

	fsync(configfd);
	close(configfd);

    while (1) {
        // sigwait() wait for USR1 only (as set in the signal mask)
    	sigwait (&mask, &signo);
	if ( signo == TIMER_SIG) 
	{
		timerHandler(signo);
	} 	
	else if (signo == BATT_SIG) 
	{
		switchingSignalHandler();
	} 
	else if (signo == SUB1_SIG) 
	{
		Sub1DataFetchCB(signo, NULL, NULL);
	} 
#ifdef ACCEL_SUPPORT
	else if (signo == SIG_ACC_INT1) 
	{
		accelInterrupt1Handle(signo);
	} 
	else if (signo == SIG_ACC_INT2) 
	{
		accelInterrupt2Handle(signo);
	}
#endif
    }
    return 0;
}

returnCode_e signalRegister(void)
{
	FUNC_ENTRY
	int r;

	CAB_DBG(CAB_GW_INFO, "Registering external interrupts");

	// create signal handler thread
	sigemptyset (&mask);
	sigaddset (&mask, TIMER_SIG);
	sigaddset (&mask, BATT_SIG);
	sigaddset (&mask, SUB1_SIG);
#ifdef ACCEL_SUPPORT
	sigaddset (&mask, SIG_ACC_INT1);
	sigaddset (&mask, SIG_ACC_INT2);
#endif

	// USR1 is now blocked for all threads in the process
	pthread_sigmask (SIG_BLOCK, &mask, 0);

	r = pthread_create (&singalHandlerThreadId, 0, signalHandlerThread, 0);
	if (r != 0) {
		return MODULE_INIT_FAIL;
	}
	
	sigregd = 1;

	FUNC_EXIT
	return GEN_SUCCESS;
}

returnCode_e signalDeRegister(void)
{
	FUNC_ENTRY
	CAB_DBG(CAB_GW_INFO,"Deregistering all external interrupts");
	//int fd;
	//char cmd[128];

	//fd = open(DISABLE_KEY_FILE, O_WRONLY);
	//if(fd < 0) {
        //	CAB_DBG (CAB_GW_ERR, "Error opening file to disable gpio keys\r\n");
	//        return FILE_OPEN_FAIL;
	//}

    	//snprintf(cmd, sizeof cmd, "%d,%d,%d,%d,%d,%d,%d", VOICE_JOURNAL_KEY_CODE, BLE_SYNC_KEY_CODE,
	//											SPI_IPC_KEY_CODE, UART_IPC_KEY_CODE,
        //                            			POWER_OFF_KEY_CODE,USB_CHARGING_DETECT_KEY_CODE,
	//											USB_CHARGING_STATUS_KEY_CODE);


	//if (write(fd, cmd, strlen(cmd) + 1) < 0) {
	//        CAB_DBG (CAB_GW_ERR, "Error writing PID for disabling gpio keys \r\n");
	//    	close(fd);
	//        return FILE_WRITE_ERROR;
	//}

	//fsync(fd);
	//close(fd);

	if( sigregd == 1 ) {
		pthread_cancel(singalHandlerThreadId);
		pthread_join(singalHandlerThreadId, NULL);
	}
	FUNC_EXIT
	return GEN_SUCCESS;
}
