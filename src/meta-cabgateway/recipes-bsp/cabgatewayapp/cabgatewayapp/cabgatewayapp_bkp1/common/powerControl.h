/***************************************************************
 * Copyright(c) <2020>, Volansys Technologies
 *
 * Description:
 * @file : powerControl.h
 * @brief : This file is used for power related functions.
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Jan/18/2020, VT , First Draft
 **************************************************************/

#ifndef _POWER_CONTROL_H_
#define _POWER_CONTROL_H_

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include "debug.h"
#include "userInterface.h"
#include "eg91ATCmd.h"
#include "sysLogStorage.h"

/****************************************
 ********* FUNCTION PROTOTYPES **********
 ****************************************/

/******************************************************************
 *@brief (This API is used to reboot the device after performing 
 *        neccesarry actions.)
 *
 *@param[IN] None
 *@param[OUT] None
 *
 *@return None
 *********************************************************************/
void rebootSystem( void );

#endif      //ifndef _POWER_CONTROL_H_
