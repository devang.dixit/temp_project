/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file commonData.h
 * @brief (Header file for common module)
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * July/24/2018, VT , First Draft
 **************************************************************/

#ifndef _COMMON_DATA_H_
#define _COMMON_DATA_H_

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include <stdint.h>
#include <errno.h>
#include "configuration.h"

/****************************************
 ************* DEFINES ******************
 ****************************************/
#define MAX_LEN_OPERATOR_NAME               (30)                //Max len of operator name
#define MAX_SELF_DIAG_TPMS_ID               (4)			//Max number of TPMS sensor ID sent from mobile app
#define TWENTY_FOUR_HOUR                    (24 * 3600)
#define SOFT_PWR_OFF_FILE "/cabApp/PwrOffTime"
/****************************************
 ************* TYPEDEFS *****************
 ****************************************/
typedef tpmsPairedInfo_t tpmsPairedDiagInfo_t;
typedef attachedHaloSNInfo_t attachedHaloSNDiagInfo_t;

/****************************************
 ************* ENUMS ********************
 ****************************************/
/* This enum defines system states */
typedef enum systemStates
{
	INIT_STATE,
	CONFIG_STATE,
	MOVING_STATE,
	PARKED_STATE,
	OTA_STATE,
	DEINIT_STATE,
	SELF_DIAG_STATE,
	MAX_SYS_STATES
} sysStates_e;

/* This enum defines system events */
typedef enum systemEvents
{
	INIT_DONE,
	CONFIG_SUCCESS,
	TPMS_ALERT,
	OTA_CHECK,
	OTA_PROCESS,
	LTE_CHECK,
	BATT_CHECK,
	BATT_TO_VEH,
	VEH_TO_BATT,
	DIAG_CHECK,
	LOW_BATT,
	XMIT_CHECK,
	LEAK_DET_CHECK,
	SIM_DET_CHECK,
	SELF_DIAG_EVENT,
	WATCHDOG_TIMER_EVENT,
	MAX_SYS_EVENTS
} sysEvent_e;

/*This enum is defines the different type of alerts*/
typedef enum alert {                    //Used to indicate ALERT
    NO = 0,
    YES,
    MAX_ALERT_TYPE
} alert_e;

/*This enum is defines the different type of devices*/
typedef enum sysDataType {              //Indicates device type
    TPMS = 0,                           //TPMS sensor
    BATTERY,                            //Backup Battery in Gateway
    CONFIG_PARAM,                       //Configuration parameter
    DIAGNOSTIC_DATA,                    //Diagnostic data
    SYS_OTHER_DATA,                     //System other data
    MAX_SYS_DATA_TYPE                   //Indicates maximum quantity of system data type
} sysDataType_e;

/*This enum is defines the different type of device at which the gateway is used to get power*/
typedef enum powerDevice {              //Indicates power device types
    VEHICLE_POWER = 0,                  //getting power from vehicle battery
    BACKUP_BATTERY,                     //getting power from backup battery
    MAX_POWER_DEVICE                    //Indicates maximum quantity of power devices
} powerDevice_e;

/**
 * @enum powerSwitchEvent
 *
 *  Enumeration for indicating power switch event.
 */
typedef enum powerSwitchEvent {
    BAT_TO_VEH = 0,     /**< Battery to vehicle event */
    VEH_TO_BAT,         /**< Vehicle to battery event */
    LOW_BAT,            /**< Low battery event */
    NORMAL_BAT,         /**< Normal battery status */
    MAX_EVENT_TYPE      /**< Max number of power switching event */
} powerSwitchEvent_e;



typedef enum {
    CFG_GW_SYSTEM_CMD = 0x0001,
    CFG_GW_OPT_CMD,                             //0x0002
    CFG_GW_TPMS_VENDOR_CMD,                     //0x0003
    CFG_GW_TPMS_PAIR_CMD,                       //0x0004
    CFG_GW_TPMS_UNPAIR_CMD,                     //0x0005
    CFG_GW_CLOUD_CMD,                           //0x0006
    CFG_GW_TARGET_PRESSURE_CMD,                 //0x0007
    CFG_GW_BATTERY_CMD,                         //0x0008
    CFG_BLE_PASSKEY_CMD,                        //0x0009
    CFG_NEW_LINE_INVALID_CMD,                   //0x000A
    CFG_MOBILE_UTC_TIME_CMD,                    //0x000B
    CFG_FILE_INIT_CMD,                          //0x000C
    CFG_FILE_DATA_CMD,                          //0x000D
    CFG_MAX_CMD                                 //0x000E
} bleConfigCmd_e;

typedef enum {
    GET_CFG_GW_SYSTEM_CMD = CFG_MAX_CMD + 1,    //0x000F
    GET_CFG_GW_OPT_CMD,                         //0x0010
    GET_CFG_GW_TPMS_VENDOR_CMD,                 //0x0011
    GET_CFG_GW_TPMS_PAIR_CMD,                   //0x0012
    GET_CFG_GW_CLOUD_CMD,                       //0x0013
    GET_CFG_GW_TARGET_PRESSURE_CMD,             //0x0014
    GET_CFG_GW_BATTERY_CMD,                     //0x0015
    GET_CFG_MAX_CMD                             //0x0016
} bleGetConfigCmd_e;

typedef enum {
    DIAG_GW_INFO_CMD = GET_CFG_MAX_CMD + 1,     //0x0017
    DIAG_GW_PAIRED_SENSOR_CMD,                  //0x0018
    DIAG_GW_TPMS_DATA_CMD,                      //0x0019
    DIAG_GW_TPMS_BY_ID_CMD,                     //0x001A
    DIAG_GW_GPS_INFO_CMD,                       //0x001B
    DIAG_GW_CELLULAR_INFO_CMD,                  //0x001C
    DIAG_GW_BATTERY_EV_LOG_CMD,                 //0x001D
    DIAG_MAX_CMD                                //0x001E
} bleDiagCmd_e;

typedef enum {
    SYS_MOBILE_RESP_CMD = DIAG_MAX_CMD + 1,     //0x001F
    SYS_BLE_PASSKEY_CMD,                        //0x0020
    SYS_BLE_MAC_CMD,                            //0x0021
    SYS_NOTIFY_BLE_CONNECTED,                   //0x0022
    SYS_NOTIFY_BLE_DISCONNECTD,                 //0x0023
    SYS_NOTIFY_MOBILE_DATA_SUCCESS,             //0x0024
    SYS_NOTIFY_MOBILE_DATA_FAIL,                //0x0025
    SYS_NOTIFY_GW_APP_START,                    //0x0026
    SYS_SET_LTE_IMEI_CMD,                       //0x0027
    SYS_GET_LTE_IMEI_CMD,                       //0x0028
    SYS_SET_SIM_ICCID_CMD,                      //0x0029
    SYS_SET_HARDWARE_VERSION,                   //0x002A
    SYS_GET_SYSTEM_ID,                          //0x002B
    SYS_RTC_FLAG,                               //0x002C
    SYS_BLE_LOW_POWER_CMD,                      //0x002D
    SYS_NOTIFY_BLE_RESET_CMD,                   //0x002E
    SYS_ADVERTISEMENT_STARTED,                  //0x002F
    SYS_DIAG_TIME_CMD,                          //0x0030
    SELF_DIAG_GW_CMD = 0x0031,                  //0x0031
    SYS_GW_RESET_CMD = 0x0033,                  //0x0033
    INSTALLATION_STATUS_CMD = 0x0034,           //0x0034
    CFG_GW_ATTACH_HALO_SN_CMD,                  //0x0035
    CFG_GW_DETACH_HALO_SN_CMD,                  //0x0036
    GET_CFG_GW_ALL_HALO_SN_CMD,                 //0x0037
    CFG_GW_UPDATE_TYRE_DETAILS_CMD,             //0x0038
    GET_CFG_GW_TYRE_DETAILS_CMD,                //0x0039
    CFG_GW_FLUSH_TYRE_DETAILS_CMD,              //0x003A
    CFG_GW_RECORD_TREAD_DETAILS_CMD,            //0x003B
    GET_CFG_GW_TREAD_HISTORY_CMD,               //0x003C
    SYS_MTU_UPDATED,                            //0x003D
    SYS_MAX_CMD = 0x00FF                        //0x00FF
} bleInternalCmd_e;

typedef enum {
    SELF_DIAG_GW_REQ_CMD = SYS_MAX_CMD + 1,     //0x0100
    SELF_DIAG_GW_RESP_CMD,                      //0x0101
    CONIF_FILE_UPLOAD_STATUS_CMD,               //0x0102
    CONIF_FILE_CREATION_STATUS_CMD,             //0x0103
    RDIAG_CLOUD_CONNECTION_STATUS_CMD,          //0x0104
    MNT_CLOUD_CONNECTION_STATUS_CMD,            //0x0105
    INTERNET_CONNECTIVITY_TIMESTAMP,            //0x0106
    /* This command is used for remote config change */
    CHANGE_FLEET_ID_CMD,                        //0x0107
    SELF_DIAG_MAX_CMD
} bleSelfDiagCmd_e;

typedef enum {
    SYS_BLE_WATCHDOG_DATA_CMD = 100,			//0x0064
    WATCHDOG_MAX_CMD
} watchDogCmd_e;

/****************************************
 ************ STRUCTURES ****************
 ****************************************/
typedef struct cgEventData_t {
	sysEvent_e evType;
	int32_t data;
} cgEventData_t;

/*This structure is used for self diagnostics status update */
typedef struct selfDiagStatus {
    bool isTestCompleted;
    bool ble_test_status;
    bool lte_sim_test_status;
    bool lte_rssi_test_status;
    bool lte_cloud_test_status;
    bool gps_test_status;
    bool accelerometer_test_status;
    bool rtc_test_status;
    bool led_btn_test_status;
    bool rf_test_status;
} selfDiagStatus_t;

/*This structure is used to define GPS data*/
typedef struct gpsData {                //GPS data stucture
    double latitude;                    //Latitude
    double longitude;                   //Longitude
    double altitude;                    //Altitude
    double speed;                       //speed
} gpsData_t;

/* This structure is used to define GPS Diagnostic data */
typedef struct gpsDiagInfo {            //GPS diagnostic Info stucture
    time_t timestamp;                   //GPS data timestamp
    gpsData_t gpsData;                  //Latitude and longitude
    uint8_t noOfSatInView;              //No of satellite views
    uint16_t fixQuality;                //Fix quality
} gpsDiagInfo_t;

/*This structure is used to define LTE Diagnostic data*/
typedef struct lteDiagInfo {                            //LTE diagnostic info stucture
    time_t timestamp;                                   //LTE data timestamp
    char operatorName[MAX_LEN_OPERATOR_NAME];           //Network Operator name
    int8_t signalQuality;                               //Signal quality
    int8_t signalStrength;                              //Signal strength
} lteDiagInfo_t;

/*This structure is used to store RTC coin cell and powerswtich time*/
typedef struct rtcPwrAccDiagInfo {
    bool coinCellStatusBool;
    time_t powerSWTime;
    bool isInternetConnected;
    int16_t accelTemperature;
    int mpuTemperature;
    uint8_t PwrOffFlg;
    time_t  PwrOffTmStamp;
} rtcPwrAccDiagInfo_t;

/*This structure is used to define Diagnostic data*/
typedef struct diagnosticData {             //Diagnostic data
    gpsDiagInfo_t gpsDiagData;              //GPS diagnostic data
    lteDiagInfo_t lteDiagData;             //LTE diagnostic data
    rtcPwrAccDiagInfo_t rtcPwrAccDiagData; // RTC, Power & Acc diag data
} diagnosticData_t;

/*This structure is used to define Bateery related data*/
typedef struct batteryData {            //Battery data stucture
    powerDevice_e device;               //Power device type
    alert_e alert;                      //Alert type
} batteryData_t;

/*This structure is used to define back battery event data */
typedef struct {
    char powerSource[20];
    char coinCellStatus[15];
    char RTCStatus[20];
    time_t startTime;
} batteryDiagInfo_t;

/* This stucture is used to define system data for diffrent devices */
typedef struct sysOtherData {
    uint32_t eventID;           //Unique ID for each event(battery/config/diag) generated in system
    sysDataType_e type;         //Type of system data
    time_t time;                //Event record time in epoch format
    union {
        batteryData_t batteryData;           //Battery data
        diagnosticData_t diagData;          //Diagnostic data
        /*Removed accelerometer & Cab temperature data as it is not required now */
    };
} sysOtherData_t;

/*This structure is used to define TPMS data for Sub GHz module*/
typedef struct tpmsData {
    uint32_t sensorID;                  //Sensor ID
    float pressure;                     //Tyre pressure
    float temperature;                  //Tyre temperature
    float battery;                      //TPMS battery voltage
    int8_t rssi;                        //RSSI value
    int8_t status;                      //Status value(2-Slow leak, 4-Fast leak, 6-Temp high/low)
} tpmsData_t;

/*This structure is used to define TPMS data for DM module*/
typedef struct tpmsDMData {
    time_t time;                        //Event record time in epoch format
    gpsData_t gpsData;                  //Vehicle position based on GPS data
    wheelInfo_t wheelInfo;              //Wheel information
    tpmsData_t tpmsData;                //TPMS sensor data stucture from sub GHz module
} tpmsDMData_t;

/* This structure is used to define TPMS data for Diag module */
typedef struct tpmsDiagDataInfo {
    int pairedNumber;                   //No of Paired TPMS
    tpmsDMData_t data[MAX_TPMS];
} tpmsDiagDataInfo_t;

/*This structure is used to define TPMS data for M&T module*/
typedef struct tpmsEvent {
    uint32_t eventID;                   //Unique ID for each TPMS data/alert event
    alert_e alert;                      //Alert type
    tpmsDMData_t tpmsDMData;            //TPMS sensor data stucture from DM module
} tpmsEvent_t;

/*This structure is used to define Changed configuration */
typedef struct changeConfigParam {
    uint32_t eventID;                   //Unique ID for each TPMS data/alert event
    configParam_e type;                 //Type of configuration parameter which is changed
    time_t time;                        //Event record time in epoch format
    union {
        unsigned char vehicleNum[MAX_LEN_VEH_NO];           //Vehicle number
        unsigned char customerID[MAX_LEN_CUSTOMER_ID];      //customer id
        vehicleType_e vehicleType;                          //Type of vehicle
        unsigned char fleetName[MAX_LEN_FLEET_NAME];        //Fleet name
        unsigned char fleetID[MAX_LEN_FLEET_ID];            //Fleet id
        unsigned char vehicleMake[MAX_LEN_VEHICLE_MAKE];    //vehicle manufacturer
        uint16_t vehicleYear;                               //vehicle manufacturing year
        unsigned char tyreMake[MAX_LEN_TYRE_MAKE];  //up to 6 axle.i.e.[1: "Michelin", 2: "Appolo"]
        unsigned char tyreSize[MAX_LEN_TYRE_SIZE];  //up to 6 axle.i.e.[1: "22", 2: "22", 3: "22"]
        sensorVendorType_e sensorType;              //Sensor vendor type
        tpmsConfig_t tpmsSensorID;                  //TPMS sensor ID and wheel information
        //int tpmsThreshold[MAX_AXLE_NO];           //Threshold value for tyre pressure
        targetPressureInfo_t targetPressInfo;        //Tyre pressure target threshold value and number of axle
        uint8_t lowBatteryThreshold;                //Threshold value to identify low battery
        char cloudURI[MAX_LEN_CLOUD_URI];           //Cloud URI
        haloSNInfo_t haloSNData;                    //HALO serial number
        tyreInfo_t tireDetails;                     //Tire details
    };
} changeConfigParam_t;

/* This structure is used to define data sent by mobile app for self diag test */
typedef struct selfDiagReq {
    uint32_t sensorID[MAX_SELF_DIAG_TPMS_ID];
} selfDiagReq_t;
/****************************************
 ******** GLOBAL VARIABLES **************
 ****************************************/

/****************************************
 ******** EXTERN VARIABLES **************
 ****************************************/

/****************************************
 ************* CALLBACKS ****************
 ****************************************/

/****************************************
 ********* FUNCTION PROTOTYPES **********
 ****************************************/

#endif      /*_COMMON_DATA_H_*/
