/*******************************************************************************
 * Copyright(c) <2016>, Volansys Technologies
 *
 * Description:
 * @file uart.c
 * @brief  Communication between M3 and A7.
 *
 * @Author   - Volansys
 *
 *******************************************************************************
 *
 * @History
 *
 * Aug/08/2018 VT, Created
 ******************************************************************************/

#include <termios.h>
#include <stdlib.h>
#include <errno.h>

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "uart.h"
#include "error.h"
#include "debug.h"

/** Verify Header of frame
 *
 * This function will compare 1st byte of
 * header with the command supported in
 * UART_COMMAND_VALUE_e.
 *
 * @param[in]   header		pointer to data bytes
 * @return		GEN_SUCCESS		valid command
 *				FAILURE		invalid command
 */
static int8_t validateCommand(const uint16_t command)
{
	//FUNC_ENTRY
	switch(command) {
		case NORMAL_POWER_MODE :
		case REMOVE_SENSOR :
		case NOTIFY_RF :
		case STOP_RF :
		case START_RF :
		case ADD_SENSOR :
		case SEND_SENSOR_DATA :
		case LOW_POWER_MODE :
			return GEN_SUCCESS;
			break;
		default:
			return FAILURE;
			break;
	}
	//FUNC_EXIT
}

/** Calculate CRC
 *
 * This snippet will calculate crc.
 *
 * @param[in]   data  			pointer to data bytes
 * @param[in]	length			length of data bytes
 * @return      calculated_crc	two byte crc value
 */
static uint16_t CalculateCRC(const uint8_t *const payload, const uint16_t payloadLength)
{
	//FUNC_ENTRY
	uint16_t index = 0;
	uint16_t calculatedCRC = 0;

	for (index = 0; index < payloadLength; index++) {
		calculatedCRC ^= payload[index];
	}

	return calculatedCRC;
	//FUNC_EXIT
}

/** Verify actual CRC with Calculated CRC
 *
 * This function will compare crc value taken as third argument
 * with calculating crc of data taken as second argument.
 *
 * @param[in]   data  			pointer to data bytes of received frame
 * @param[in]	length			length of data bytes
 * @param[in]	actual_crc		actual crc received from frame
 * @return      GEN_SUCCESS 		CRC matched
 *              FAILURE 		CRC not matched
 */
static int8_t validateCRC(const uint8_t *const payload, const uint16_t payloadLength, 
							uint16_t actualCRC)
{
	//FUNC_ENTRY
	uint16_t calculatedCRC = 0;

	calculatedCRC = CalculateCRC(payload, payloadLength);
	if(calculatedCRC == actualCRC)
		return GEN_SUCCESS;

	return FAILURE;
	//FUNC_EXIT
}

int8_t uartRead(const int uartfd, UART_FRAME_t *readFrame)
{
	//FUNC_ENTRY
	uint8_t uartFrame[FRAME_SIZE] = {0};
	uint16_t readBytes = 0;
	uint16_t totalReadBytes = 0;
	UART_STATE_e state = UART_SOF_STATE;	
	uint16_t readLength = UART_SOF_BYTES + UART_HEADER_BYTES;

	if(readFrame == NULL) {
		return FAILURE;
	}
	while(1) {
		readBytes = read(uartfd, uartFrame + totalReadBytes, readLength);
		if(readBytes <= 0) {
			CAB_DBG(CAB_GW_DEBUG, "UART Timeout!");
			return FAILURE;
		}
		totalReadBytes = totalReadBytes + readBytes;
		if(readBytes != readLength) {
			readLength = readLength - readBytes;
		}
		else {
			switch(state) {
				case UART_SOF_STATE :
					memcpy(&(readFrame->startofFrame), uartFrame + UART_SOF_STATE, UART_SOF_BYTES);
					if(readFrame->startofFrame != START_OF_FRAME) {
						return FAILURE;
					}
				case UART_COMMAND_STATE :
					memcpy(&(readFrame->command), uartFrame + UART_COMMAND_STATE, UART_COMMAND_BYTES);
					if(validateCommand(readFrame->command) != GEN_SUCCESS) {
						return FAILURE;
					}
				case UART_PAYLOAD_LENGTH_STATE :
					memcpy(&(readFrame->payloadLength), uartFrame + UART_PAYLOAD_LENGTH_STATE, 
							UART_PAYLOAD_LENGTH_BYTES);
					if(readFrame->payloadLength == 0) {
						return GEN_SUCCESS;
					}
					else {
						readLength = readFrame->payloadLength;
						state = UART_PAYLOAD_STATE;
					}
					break;
				case UART_PAYLOAD_STATE :
					memcpy(readFrame->payload, uartFrame + UART_PAYLOAD_STATE, 
							readFrame->payloadLength - UART_CRC_BYTES);
				case UART_CRC_STATE :
					memcpy(&readFrame->crc, uartFrame + UART_PAYLOAD_STATE + 
							readFrame->payloadLength - UART_CRC_BYTES, UART_CRC_BYTES);
					if(validateCRC(readFrame->payload, readFrame->payloadLength - UART_CRC_BYTES, readFrame->crc) 
									== FAILURE)	{
						return FAILURE;
					}
					else {
						return GEN_SUCCESS;
					}
					break;
				default :
					break;
			}
		}
	}
	//FUNC_EXIT
}

int8_t uartWrite(const int uartfd, const uint16_t command, const uint8_t *const payload, 
						const uint16_t payloadLength)
{
	//FUNC_ENTRY
	uint8_t sendFrame[FRAME_SIZE] = {0};
	uint16_t sendFrameLength = UART_SOF_BYTES + UART_HEADER_BYTES;
	uint16_t frameCRC = 0;
	uint16_t frameSOF = START_OF_FRAME;
	uint16_t framePayloadLength = payloadLength;
	int8_t retVal = 0;

	if((payload == NULL) && (framePayloadLength != 0)) {
		return FAILURE;
	}

	memcpy(sendFrame + UART_SOF_STATE, &frameSOF, UART_SOF_BYTES);
	memcpy(sendFrame + UART_COMMAND_STATE, &command, UART_COMMAND_BYTES);

	if(framePayloadLength) {
		sendFrameLength = sendFrameLength + framePayloadLength + UART_CRC_BYTES;

		memcpy(sendFrame + UART_PAYLOAD_STATE, payload, framePayloadLength);

		frameCRC = CalculateCRC(payload, framePayloadLength);
		memcpy(sendFrame + UART_PAYLOAD_STATE + framePayloadLength, &frameCRC, UART_CRC_BYTES);

		framePayloadLength += UART_CRC_BYTES;
		memcpy(sendFrame + UART_PAYLOAD_LENGTH_STATE, &framePayloadLength, UART_PAYLOAD_LENGTH_BYTES);
	}
	else {
		memcpy(sendFrame + UART_PAYLOAD_LENGTH_STATE, &framePayloadLength, UART_PAYLOAD_LENGTH_BYTES);
	}

	retVal = write(uartfd, sendFrame, sendFrameLength);
	if(retVal < sendFrameLength) {
		return FAILURE;
	}
	return retVal;
	//FUNC_EXIT
}

int uartInit(int *uartfd, tcflag_t baudrate, const char *uartPort)
{
	//FUNC_ENTRY
    struct termios my_termios, new_termios;

    *uartfd = open(uartPort, O_RDWR | O_NOCTTY | O_SYNC);
    if (*uartfd < 0)
    {
        //CABGW_DBG (CABGW_DBG_LVL_1,"Open function fail ");
        return -1;
    }

	tcgetattr(*uartfd, &my_termios);
    my_termios.c_iflag &= ~(IGNBRK | BRKINT | ICRNL | INLCR | PARMRK | INPCK | ISTRIP | IXON);
    my_termios.c_oflag &= ~(OCRNL | ONLCR | ONLRET | ONOCR | OFILL | OLCUC | OPOST);
    my_termios.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG | ECHOK | ECHOCTL | ECHOPRT);
    my_termios.c_cflag &= ~(CSIZE | PARENB | CSTOPB);
    my_termios.c_cflag &= ~(CRTSCTS | IGNCR | ICRNL | IXON | IXOFF | IXANY);
    my_termios.c_cflag |= CS8 | CLOCAL | CREAD;
    my_termios.c_cflag &= ~CBAUD;
    my_termios.c_cflag |= baudrate;
    my_termios.c_cc[VMIN] = 0;
    my_termios.c_cc[VTIME] = 30;
    tcsetattr(*uartfd, TCSANOW, &my_termios);
    tcgetattr(*uartfd, &new_termios);

    close(*uartfd);

	*uartfd = open(uartPort, O_RDWR | O_NOCTTY | O_SYNC);
	if (*uartfd < 0)
	{
        perror(uartPort);
        return FAILURE;
	}

	//FUNC_EXIT
	return GEN_SUCCESS;
}

void uartDeInit(int uartfd)
{
	//FUNC_ENTRY
	close(uartfd);
	//FUNC_EXIT
}
