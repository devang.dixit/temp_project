/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : gpio.c
 * @brief : This file contains all APIs defination and static APIs defination to handle GPIO.
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Aug/24/2018, VT , First Draft
***************************************************************/

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include "gpio.h"
#include "debug.h"

/****************************************
 ******** STATIC VARIABLES **************
 ****************************************/
static const char * gpioDir[] = {"in", "out"};

/****************************************
 ********* FUNCTION DEFINATION **********
 ****************************************/
/******************************************************************
 *@brief (To export the given gpio in particular direction.)
 *
 *@param[IN] gpioPinNum (Indicates GPIO pin number)
 *@param[IN] gpioPinDir (Indicates GPIO pin direction)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e gpioExport(gpioPinNum pinNumber, gpioDirection_e direction)
{
    //FUNC_ENTRY

    int gpioNode;
    int status, ret = GEN_SUCCESS;
    char gpioNumber[4] = {'\0'};
    char buf[40] = {'\0'};

    /*  open export path    */
    gpioNode = open(GPIO_EXPORT_PATH, O_WRONLY);
    if(gpioNode == -1) {
        CAB_DBG(CAB_GW_ERR, "Error opening %s for gpio %d", GPIO_EXPORT_PATH, pinNumber);
        ret = FILE_OPEN_FAIL;
        goto closeFile;
    }
    sprintf(gpioNumber, "%d", pinNumber);
    status = write(gpioNode, (void *)gpioNumber, strlen(gpioNumber) + 1);
    if(status == -1) {
        CAB_DBG(CAB_GW_ERR, "Error exporting for gpio %s", gpioNumber);
        ret = FILE_WRITE_ERROR;
        goto closeFile;
    }
    fsync(gpioNode);
    close(gpioNode);

    sprintf(buf, GPIO_DIRECTION_PATH, pinNumber);
    /*  open direction path */
    gpioNode = open(buf, O_WRONLY);
    if(gpioNode == -1) {
        CAB_DBG(CAB_GW_ERR, " Error opening %s", buf);
        ret = FILE_OPEN_FAIL;
        goto closeFile;
    }
    status = write(gpioNode, (void *)gpioDir[direction], strlen(gpioDir[direction]) + 1);
    if(status == -1) {
        CAB_DBG(CAB_GW_ERR, "Error setting direction %s for %s", gpioDir[direction], buf);
        ret = FILE_WRITE_ERROR;
        goto closeFile;
    }
closeFile:
    fsync(gpioNode);
    close(gpioNode);

    //FUNC_EXIT
    return ret;
}

/******************************************************************
 *@brief (To unexport the given exporeted gpio.)
 *
 *@param[IN] gpioPinNum (Indicates GPIO pin number)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e gpioUnExport(gpioPinNum pinNumber)
{
    //FUNC_ENTRY

    int gpioNode;
    int status, ret = GEN_SUCCESS;
    char gpioNumber[4] = {'\0'};

    /*  open export path    */
    gpioNode = open(GPIO_UNEXPORT_PATH, O_WRONLY);
    if(gpioNode == -1) {
        CAB_DBG(CAB_GW_ERR, "Error opening %s for gpio %d", GPIO_EXPORT_PATH, pinNumber);
        ret = FILE_OPEN_FAIL;
        goto closeFile;
    }
    sprintf(gpioNumber, "%d", pinNumber);
    status = write(gpioNode, (void *)gpioNumber, strlen(gpioNumber) + 1);
    if(status == -1) {
        CAB_DBG(CAB_GW_ERR, "Error unexporting for gpio %s", gpioNumber);
        ret = FILE_WRITE_ERROR;
        goto closeFile;
    }
closeFile:
    fsync(gpioNode);
    close(gpioNode);

    //FUNC_EXIT
    return ret;
}

returnCode_e gpioRead(gpioPinNum pinNumber, uint8_t* value)
{
    //FUNC_ENTRY

    int gpioNode;
    int status, ret = GEN_SUCCESS;
    char buf[40] = {'\0'};
    char readValue;

    sprintf(buf, GPIO_VALUE_PATH, pinNumber);

    /*  open value path */
    gpioNode = open(buf, O_RDONLY);
    if(gpioNode == -1) {
        CAB_DBG(CAB_GW_ERR, " Error opening %s", buf);
        ret = FILE_OPEN_FAIL;
        goto closeFile;
    }
    status = read(gpioNode, &readValue, sizeof(char));
    if(status == -1) {
        CAB_DBG(CAB_GW_ERR, "Error reading value for %s", buf);
        ret = FILE_READ_ERROR;
        goto closeFile;
    }
    *value = (readValue == '1') ? 1 : 0;
closeFile:
    close(gpioNode);

    //FUNC_EXIT
    return ret;
}

returnCode_e gpioWrite(gpioPinNum pinNumber, bool value)
{
    //FUNC_ENTRY

    char data;
    int status, ret = GEN_SUCCESS;
    char buf[40] = {'\0'};
    int gpioNode;

    sprintf(buf, GPIO_VALUE_PATH, pinNumber);

    /*  open value path */
    gpioNode = open(buf, O_WRONLY);
    if(gpioNode == -1) {
        CAB_DBG(CAB_GW_ERR, "Error opening %s for gpio %d", buf, pinNumber);
        ret = FILE_OPEN_FAIL;
        goto closeFile;
    }
    data = value ? '1' : '0';
    status = write(gpioNode, (void *)&data, sizeof(data));
    if(status == -1) {
        perror("write");
        CAB_DBG(CAB_GW_ERR, "Error writing value %c for %s", data, buf);
        ret = FILE_WRITE_ERROR;
        goto closeFile;
    }
closeFile:
    fsync(gpioNode);
    close(gpioNode);

    //FUNC_EXIT
    return ret;
}

returnCode_e gpioInit(gpioPinNum pinNum, gpioDirection_e pinDir)
{
    //FUNC_ENTRY

    int ret = GEN_SUCCESS;
    ret = gpioExport(pinNum, pinDir);
    if(ret != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "Error exporting for gpio %d", pinNum);
    }

    //FUNC_EXIT
    return ret;
}

returnCode_e gpioDeInit(gpioPinNum pinNum)
{
    //FUNC_ENTRY

    int ret = GEN_SUCCESS;
    ret = gpioUnExport(pinNum);
    if(ret != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "Error Unexporting for gpio %d", pinNum);
    }

    //FUNC_EXIT
    return ret;
}
