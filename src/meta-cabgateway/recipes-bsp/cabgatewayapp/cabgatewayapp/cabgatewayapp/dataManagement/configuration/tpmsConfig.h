/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file tpmsConfig.h
 * @brief (Header file for TPMS configuration)
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * July/18/2018, VT , First Draft
***************************************************************/

#ifndef _TPMS_CONFIG_H_
#define _TPMS_CONFIG_H_

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include <stdint.h>

/****************************************
 ************* DEFINES ******************
 ****************************************/
#define MAX_AXLE_NO                     (10)                //Max no of axle
#define MAX_TPMS                        (40)                //Max no of TPMS
#define MAX_HALO_SN                     (MAX_AXLE_NO * 2)   //Max no of HALO SN can be attached
#define MAX_TYRE_NO                     ((MAX_AXLE_NO * 4) - 2) //Max no of Tyres for the available axles
#define MAX_LEN_SENSOR_PART_NO          (5)                 /**< Sensor part number string max len*/
#define MAX_LEN_SENSOR_MODEL            (6)                 /**< Sensor model string max len*/

/****************************************
 ************* ENUMS ********************
 ****************************************/
/*This enum is defines the different type of sensor types*/
typedef enum sensorVendorType {     //Sensor vendor types
    VENDOR_AVE_TECH = 0,
    VENDOR_PRESSURE_PRO,
    MAX_VENDORS
} sensorVendorType_e;

/*This enum is defines the different attachment type*/
typedef enum attachedType {              //Indicates attachment type (i.e. Tyre or Halo)
    TYRE = 0,
    HALO,
    MAX_ATTACH_TYPE
} attachedType_e;

/*This enum is defines the different type of sides of wheel*/
typedef enum vehicleSide {            //Indicates Vehicle side (i.e. left or right)
    LEFT = 0,
    RIGHT,
    MAX_WHEEL_SIDE
} vehicleSide_e;

/*This enum is defines the different type of tyre position*/
typedef enum tyrePos {            //Indicates tyre position (i.e. Inner or Outer)
    INNER = 0,
    OUTER,
    MAX_TYRE_POS
} tyrePos_e;

/*This enum is defines the status of Halo*/
typedef enum haloEnable {            //Indicates Halo status (i.e. Enable or disable)
    ENABLE = 0,
    DISABLE,
    MAX_HALO_STATUS
} haloEnable_e;

/****************************************
 ************ STRUCTURES ****************
 ****************************************/
/*Wheel_id format :- 16-bits (9-bits   x    x     x     xxxx)
                             (Reserved T/H  I/O   L/R   Axle_no)
                                                        (F2B)       //F2B -> Front to Back
*/

/*This structure is used to define wheel data for configuration*/
typedef struct wheelInfo {
    haloEnable_e isHaloEnable;          //Halo status (i.e. Enable or disable)
    attachedType_e attachedType;        //Attachment type (i.e. Tyre or Halo)
    tyrePos_e tyrePos;                  //Wheel position (i.e. Inner or Outer)
    vehicleSide_e vehicleSide;          //Vehicle side (i.e. left or right)
    uint8_t axleNum;                    //Axle number
} wheelInfo_t;

/*This structure is used to define TPMS data for configuration*/
typedef struct tpmsConfig {
    uint32_t sensorID;              //sensor id
    char sensorPN[MAX_LEN_SENSOR_PART_NO];  //sensor part number  
    char sensorModel[MAX_LEN_SENSOR_MODEL]; //sensor model  
    uint16_t wheelID;               //wheel id
    wheelInfo_t wheelInfo;          //wheel info (i.e. wheelSide, attachedType, tyrePos, axleNum)
} tpmsConfig_t;

/*This structure is used to define TPMS target pressure information*/
typedef struct targetPressureInfo {
    uint8_t numberOfAxle;               //Total number axle
    int tpmsThreshold[MAX_AXLE_NO];     //Threshold value for tyre pressure
} targetPressureInfo_t;

/****************************************
 ******** GLOBAL VARIABLES **************
 ****************************************/

/****************************************
 ******** EXTERN VARIABLES **************
 ****************************************/

/****************************************
 ************* CALLBACKS ****************
 ****************************************/

/****************************************
 ********* FUNCTION PROTOTYPES **********
 ****************************************/

#endif      /*_TPMS_CONFIG_H_*/
