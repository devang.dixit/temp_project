/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file configuration.h
 * @brief (Header file for configuration module)
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * July/18/2018, VT , First Draft
***************************************************************/

#ifndef _CONFIGURATION_H_
#define _CONFIGURATION_H_

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include "ledConfig.h"
#include "tpmsConfig.h"
#include "error.h"

/****************************************
 ************* DEFINES ******************
 ****************************************/
#define DEFAULT_VEH_NUM                         ""
#define DEFAULT_CUSTOMER_ID                     ""
#define DEFAULT_VEH_TYPE                        (MAX_VEHICLE_TYPE)
#define DEFAULT_FLEET_NAME                      ""
#define DEFAULT_FLEET_ID                        ""
#define DEFAULT_VEHICLE_MAKE                    ""
#define DEFAULT_VEHICLE_YEAR                    (0)
#define DEFAULT_TYRE_MAKE                       ""
#define DEFAULT_TYRE_SIZE                       ""
#define DEFAULT_HALO_SN                         ""
#define DEFAULT_SENSOR_TYPE                     (VENDOR_AVE_TECH)
#define DEFAULT_TPMS_THRESHOLD                  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}      // in psi
#define DEFAULT_LOW_BAT_THRESHOLD               30                                  // in %
#define DEFAULT_CLOUD_URI                       "https://api.aperiatechnologies.com"
#define DEFAULT_LTE_IMEI                        ""
#define DEFAULT_LTE_ICCID                       ""
#define DEFAULT_GATEWAY_ID                      ""
#define DEFAULT_BLE_MAC                         ""
#define DEFAULT_BLE_PASSKEY                     ""
#define DEFAULT_HARDWARE_VERSION                ""
#define DEFAULT_SOFTWARE_VERSION                ""
#define DEFAULT_LED_PATTERN                     (NORMAL)
#define DEFAULT_RTC_STATUS                      0
#define DEFAULT_MANDATORY_STATUS                false
#define DEFAULT_CONFIG_FILE_UPLOAD_STATUS       false
#define DEFAULT_CONFIG_FILE_UPDATE_STATUS       false
#define DEFAULT_CONFIG_FILE_NAME                ""
#define DEFAULT_CLOUD_CONNECTION_STATUS	        false
#define DEFAULT_INTERNT_CONNECTION_TIMESTAMP    0
#define DEFAULT_TYRE_SERIAL_NO                  ""
#define DEFAULT_TYRE_TIN                        ""
#define DEFAULT_TYRE_MODEL                      ""
#define DEFAULT_TYRE_WIDTH                      ""
#define DEFAULT_RATIO                           ""
#define DEFAULT_DIAMETER                        ""
#define DEFAULT_LOAD_RATING                     ""
#define DEFAULT_TREAD_DEPTH                     ""
#define DEFAULT_VEHICLE_MILEAGE                 0
#define DEFAULT_RECORD_TIME                     0
#define DEFAULT_TYRE_DETAILS_STATUS             false
#define DEFAULT_SENSOR_PART_NO                  "0000"
#define DEFAULT_SENSOR_MODEL                    "00000"

#define MAX_LEN_VEH_NO                  (50)                /**< Max len of vehicle no */
#define MAX_LEN_CUSTOMER_ID             (50)                /**< Max len of vehicle id */
#define MAX_LEN_VEH_TYPE                (50)                /**< Max len of vehicle type */
#define MAX_LEN_FLEET_NAME              (50)                /**< Max len of fleet name */
#define MAX_LEN_FLEET_ID                (50)                /**< Max len of fleet id */
#define MAX_LEN_VEHICLE_MAKE            (30)                /**< Max name of vehicle make */
#define MAX_LEN_TYRE_SIZE               (100)               /**< Max len of tyre size */
#define MAX_LEN_TYRE_MAKE               (200)               /**< Max len of tyre make */
#define MAX_LEN_HALO_SN                 (10)                /**< Max len of HALO SN*/
#define MAX_LEN_BLE_MAC                 (6)                 /**< Max len of ble mac address */
#define MAX_LEN_BLE_PASSKEY             (6)                 /**< Max len of ble passkey */
#define MAX_LEN_LTE_IMEI                (16)                /**< Max len of LTE IMEI number */
#define MAX_LEN_GATEWAY_ID              (MAX_LEN_BLE_MAC + MAX_LEN_LTE_IMEI) /**< Max len of gateway id */
#define MAX_LEN_HARDWARE_VERSION        (8)                 /**< Max len of Hardware version. */
#define MAX_LEN_SOFTWARE_VERSION        (10)                /**< Max len of Software version. */
#define MAX_LEN_SIM_ICCID               (64)                /**< Max len of SIM ICCID. */
#define MAX_LEN_OTA_VERSION             (25)                /**< Max len of OTA vesion. */
#define MAX_LEN_FILENAME                (256)               /**< Max len of filename. */
#define MAX_LEN_PATH                    (256)               /**< Max len of path. */
#define MAX_LEN_CLOUD_URI               (2048)              /**< Max len of cloud URI */
#define MAX_LEN_MD5SUM                  (32)                /**< Max len of MD5SUM. */
#define RTC_NW_SYNC_BIT                 (1)                 /**< RTC sync with network time flag */
#define RTC_MOBILE_SYNC_BIT             (0)                 /**< RTC sync with mobile time flag */
#define LOW_BATT_THRESHOLD_LOWER_LIMIT  (1)                 /**< battery threshold low limit for validation*/
#define LOW_BATT_THRESHOLD_UPPER_LIMIT  (90)                /**< battery threshold high limit for validation*/
#define MAX_RESET_CMD_LEN               (10)                /**< Reset command max len */
#define MAX_LEN_TREAD_DEPTH             (6)                 /**< Tread depth data max len*/
#define MAX_TREAD_DEPTH_RECORD          (5)                 /**< Tread depth data max records*/
#define MAX_LEN_TYRE_SERIAL_NO          (31)                /**< Tyre Serial number data max len*/
#define MAX_LEN_TYRE_TIN                (5)                 /**< Tyre TIN data max len*/
#define MAX_LEN_TYRE_MAKE_STR           (26)                /**< Tyre Make string max len*/
#define MAX_LEN_TYRE_MODEL              (26)                /**< Tyre Model string max len*/
#define MAX_LEN_TYRE_WIDTH              (11)                /**< Tyre Width string max len*/
#define MAX_LEN_RATIO                   (11)                /**< Tyre Ratio string max len*/
#define MAX_LEN_DIAMETER                (11)                /**< Tyre Diameter string max len*/
#define MAX_LEN_LOAD_RATING             (11)                /**< Tyre load rating string max len*/

// MAX_FILENAME_LEN needs to be in sync with MAX_CFG_FILENAME_LEN from configData.h
#define MAX_FILENAME_LEN                300
// sizeof(cabGatewayConfig_t) + size of json config file when GW is not configured = 12.5 KB + 23.3 KB = ~40 KB
#define MAX_JSON_STRING_LEN             40 * 1024
#define USERDATA_FOLDER                 "/media/userdata/"
#define CFG_STORAGE_FOLDER              "/media/userdata/configStorage"
#define DATE_STORAGE_FOLDER             "/media/userdata/dataStorage"
#define DATA_CFG_FOLDER                 "/media/userdata/dataConfig"
#define SYSLOG_FOLDER                   "/media/userdata/sysLog"
#define FILE_PATH_CONFIG_PARAM          "/media/userdata/configStorage/configParam.txt"
#define FILE_PATH_CONFIG_PARAM_OBSOLETE "/media/userdata/configStorage/configParam_obsolete.txt"
#define NEW_FILE_PATH_CONFIG_PARAM      "/media/userdata/configStorage/configParam.cfg"
#define FILE_PATH_STATUS_PARAM          "/media/userdata/configStorage/configStatus.txt"
#define FILE_PATH_SOFTWARE_VERSION      "/cabApp/softwareVersion.txt"
#define DIR_PATH_CONFIG_STORAGE         "/media/userdata/configStorage"

#define NEW_VEHICLE_TYPE

/****************************************
 ************* TYPEDEF ******************
 ****************************************/
typedef uint32_t commandCode_e;

/****************************************
 ************* ENUMS ********************
 ****************************************/
/*This enum is defines the different type of vehicles*/
typedef enum vehicleType {          //Type of vehicle
    TENDEM_DUAL_TRACTOR = 0,
    TENDEM_WIDE_TRACTOR,
    SINGLE_DUAL_TRACTOR,
#ifdef NEW_VEHICLE_TYPE
    SINGLE_DUAL_DELIVERY_VAN,
    SINGLE_SINGLE_DELIVERY_VAN,
    SINGLE_DUAL_STRAIGHT_TRUCK,
#endif
    DUMMY_VEHICLE,
    MAX_VEHICLE_TYPE
} vehicleType_e;

/*  This enum defines the different type of configuration parameters.
    It is used to identify parameter type at set/get_configuration_param call
    to access all parameters of configuration module
*/
typedef enum configParam {
    ALL_DATA = 0,
    VEHICLE_NUMBER,             //1
    CUSTOMER_ID,                //2
    VEHICLE_TYPE,               //3
    FLEET_NAME,                 //4
    FLEET_ID,                   //5
    VEHICLE_MAKE,               //6
    VEHICLE_YEAR,               //7
    TYRE_MAKE,                  //8
    TYRE_SIZE,                  //9
    SENSOR_TYPE,                //10
    ADD_SENSOR_ID,              //11
    REMOVE_SENSOR_ID,           //12
    GET_SENSOR_ID,              //13
    GET_ALL_SENSOR_ID,          //14
    TPMS_THRESHOLD,             //15
    LOW_BATTERY_THRESHOLD,      //16
    CLOUD_URI,                  //17
    ATTACH_HALO_SN,             //18
    DETACH_HALO_SN,             //19
    UPDATE_TYRE_DETAILS,        //20
    FLUSH_TIRE_DETAILS,         //21
    UPDATE_TREAD_DEPTH_VALUE,   //22
    BLE_CONNECTED,              //23
    BLE_DISCONNECTED,           //24
    BLE_PASSKEY,                //25
    BLE_MAC,                    //26
    TIME_STAMP,                 //27
    LTE_IMEI,                   //28
    GATEWAY_ID,                 //29
    SOFTWARE_VERSION,           //30
    HARDWARE_VERSION,           //31
    SIM_ICCID,                  //32
    GATEWAY_CONFIGURED,         //33
    GATEWAY_NOT_CONFIGURED,     //34
    MANDATORY_FLAG,             //35
    RTC_FLAG,                   //36
    LED_PATTERN,                //37
    DIAG_TIME_CMD,              //38
    MANDATORY_FIELDS,           //39
    SELF_DIAG_REQ,              //40
    WATCHDOG_TIMER_CONFIG,      //41
    FACTORY_RESET,              //42
    BLE_ADVERTISEMENT_STARTED,  //43
    MAX_CONFIGURABLE_PARAM,
} configParam_e;

/* This enum defines the type of the configuration to be migrated to JSON */
typedef enum configInitType {
    DEFAULT_CONFIG = 0,
    EXISTING_CONFIG = 1   
} configInitType_e;

/****************************************
 ************ STRUCTURES ****************
 ****************************************/
/*This structure is used to define the no of max tyres, wheelID, and sensorID for vehicle*/
typedef struct vehicleDef {
    uint8_t maxTyre;                    //no of maximum tyres in vehicle
    uint8_t maxAxle;
    tpmsConfig_t tpmsConfig[MAX_TPMS];  //Array of 40 TPMS sensor configuration structures
} vehicleDef_t;

/* This structure is used to define the attaching/attached HALO SN data */
typedef struct haloSNInfo {
    char serialNo[MAX_LEN_HALO_SN];             // HALO serial number
    vehicleSide_e vehicleSide;                  // Vehicle side (i.e. left or right)
    uint8_t axleNum;                            // Axle number
} haloSNInfo_t;

/* This structure is used to define the tread depth history data */
typedef struct treadDepthInfo {
    char treadDepthValue[MAX_LEN_TREAD_DEPTH];  //Tread Depth value for the specific tyre
    uint32_t vehicleMileage;                    //Mileage value at the time of recording
    time_t recordTime;                          //Time of recording the Tread depth value
} treadDepthInfo_t;

/* This structure is used to define the tyre info */
typedef struct tyreInfo {
    uint8_t axleNum;                            // Axle number
    vehicleSide_e vehicleSide;                  // Vehicle side (i.e. left or right)
    tyrePos_e tyrePos;                          // Wheel position (i.e. Inner or Outer)
    bool isTyreDetailConfigured;                // Flag that indicate tyre details are configured or not
    char tyreSerialNo[MAX_LEN_TYRE_SERIAL_NO];  // Tyre serial number
    char tyreTIN[MAX_LEN_TYRE_TIN];             // Tyre TIN
    char tyreMake[MAX_LEN_TYRE_MAKE_STR];       // Tyre Make string
    char tyreModel[MAX_LEN_TYRE_MODEL];         // Tyre Model string
    char tyreWidth[MAX_LEN_TYRE_WIDTH];         // Tyre Width
    char ratio[MAX_LEN_RATIO];
    char diameter[MAX_LEN_DIAMETER];
    char loadRating[MAX_LEN_LOAD_RATING];
    treadDepthInfo_t treadDepthHistory[MAX_TREAD_DEPTH_RECORD];
} tyreInfo_t;

/*This structure is used to define the all the configurations of cab gateway*/
typedef struct cabGatewayConfig {
    char vehicleVin[MAX_LEN_VEH_NO];                //Vehicle number
    char customerID[MAX_LEN_CUSTOMER_ID];           //Customer id
    vehicleType_e vehicleType;                      //Type of vehicle
    char fleetName[MAX_LEN_FLEET_NAME];             //Fleet name
    char fleetID[MAX_LEN_FLEET_ID];                 //Fleet id
    char vehicleMake[MAX_LEN_VEHICLE_MAKE];         //vehicle manufacturer
    uint16_t vehicleYear;                           //vehicle manufacturing year
    char tyreMake[MAX_LEN_TYRE_MAKE];               //up to 6 axle.i.e.[1: "Michelin", 2: "Michelin"]
    char tyreSize[MAX_LEN_TYRE_SIZE];               //up to 6 axle.i.e.[1: "22", 2: "22", 3: "22"]
    vehicleDef_t vehicleDef_s;                      //Vehicle configuration with no. of tyre and TPMS
    sensorVendorType_e sensorType;                  //Sensor vendor type
    //int tpmsThreshold[MAX_AXLE_NO];               //Threshold value for tyre pressure
    targetPressureInfo_t targetPressInfo;           //Tyre pressure target threshold value and number of axle
    uint8_t lowBatteryThreshold;                    //Threshold value to identify low battery
    char cloudURI[MAX_LEN_CLOUD_URI];               //Cloud URI
    uint8_t imeiNumber[MAX_LEN_LTE_IMEI];           //LTE IMIE number
    char blePassKey[MAX_LEN_BLE_PASSKEY];           //Ble passkey
    uint8_t bleMac[MAX_LEN_BLE_MAC];                //Ble MAC adderess
    uint8_t gatewayId[MAX_LEN_GATEWAY_ID];          //Gateway id
    char hardwareVersion[MAX_LEN_HARDWARE_VERSION]; //Hardware version
    char softwareVersion[MAX_LEN_SOFTWARE_VERSION]; //software version
    char simIccid[MAX_LEN_SIM_ICCID];               //sim iccid
    uint8_t isRtcSync;   	                        //Rtc configured flag
    bool isMandatoryInfoAvailable;                  //Mandatory information available
    ledPattern_t ledPattern_s;                      //Led patterns
    time_t diagLTETime;                             //LTE time stamp
    haloSNInfo_t haloSNConfig[MAX_HALO_SN];         //HALO SN configuration
    tyreInfo_t tyreConfig[MAX_TYRE_NO];             //Tyre configuration for individual tyre details
} cabGatewayConfig_t;

/* This structure is used to define all the status parameters for used by installation status command*/
typedef struct cabGatewayStatus {
	bool configFileUploadStatus;
	bool mandatoryConfigFileUpdated;
	char latestMandatoryConfigFileName[MAX_FILENAME_LEN];
	bool rDiagCloudConnectionStatus;
	bool mntCloudConnectionStatus;
	time_t internetConnectivityTimestamp;
} cabGatewayStatus_t;

/*This structure is used to define the mandatory gateway configuration*/
typedef struct gatewayInfo {
    char customerID[MAX_LEN_CUSTOMER_ID];      //Customer id
    char fleetID[MAX_LEN_FLEET_ID];            //Fleet id
    char vehicleType[MAX_LEN_VEH_TYPE];        //Type of vehicle
    char vehicleVin[MAX_LEN_VEH_NO];           //Vehicle number
} gatewayInfo_t;

/*This structure is used to define the optional gateway configuration*/
typedef struct vehicleOptInfo {
    char fleetName[MAX_LEN_FLEET_NAME];         //Fleet name
    char vehicleMake[MAX_LEN_VEHICLE_MAKE];     //vehicle manufacturer
    uint16_t vehicleYear;                       //vehicle manufacturing year
    char tyreMake[MAX_LEN_TYRE_MAKE];           //up to 6 axle.i.e.[1: "Michelin", 2: "Michelin"]
    char tyreSize[MAX_LEN_TYRE_SIZE];           //up to 6 axle.i.e.[1: "22", 2: "22", 3: "22"]
} vehicleOptInfo_t;

/*This structure is used to define the TPMS pairing data*/
typedef struct tpmsInfo {
    uint32_t sensorID;                              //Sensor id
    char sensorPN[MAX_LEN_SENSOR_PART_NO];          //Sensor part number
    char sensorModel[MAX_LEN_SENSOR_MODEL];         //Sensor Model type
    haloEnable_e isHaloEnable;                      //Halo status (i.e. Enable or disable)
    attachedType_e attachedType;                    //Attachment type (i.e. Tyre or Halo)
    tyrePos_e tyrePos;                              //Wheel position (i.e. Inner or Outer)
    vehicleSide_e vehicleSide;                      //Vehicle side (i.e. left or right)
    uint8_t axleNum;                                //Axle number
} tpmsInfo_t;

/* This structure is used to define paired TPMS list for Configuration module */
typedef struct tpmsPairedInfo {
    int pairedNumber;                   //No of Paired TPMS
    tpmsInfo_t tpmsInfo[MAX_TPMS];      //TPMS data
} tpmsPairedInfo_t;

/* This structure is used to define attached HALO SN list for Configuration module */
typedef struct attachedHaloSNInfo {
    unsigned int numberOfAttachedHaloSN;        // Number of attached HALO SNs
    haloSNInfo_t haloSNInfo[MAX_HALO_SN];       // HALO SN data
} attachedHaloSNInfo_t;

typedef struct tyrePosInfo {
    uint8_t axleNum;                            // Axle number
    vehicleSide_e vehicleSide;                  // Vehicle side (i.e. left or right)
    tyrePos_e tyrePos;                          //Wheel position (i.e. Inner or Outer)
} tyrePosInfo_t;

/* This structure is used to define configured Tread depth records for Configuration module */
typedef struct configuredTreadRecordInfo {
    unsigned int numberOfRecords;               // Number of tread depth records
    treadDepthInfo_t treadDepthInfo[MAX_TREAD_DEPTH_RECORD]; //sorted tread depth history
    tyrePosInfo_t tyrePosInfo;                  //Tire Position
} configuredTreadRecordInfo_t;

typedef struct treadRecordInfo {
    tyrePosInfo_t tyrePosInfo;                  //Tire Position
    treadDepthInfo_t treadDepthInfo;            //Tread depth and vehicle mileage value
} treadRecordInfo_t;

/****************************************
 ******** EXTERN VARIABLES **************
 ****************************************/

/****************************************
 ************* CALLBACKS ****************
 ****************************************/
/*It is used to register callback to notify the updated configuration*/
typedef void (*updatedConfigNotifCB)(configParam_e, void *);

/****************************************
 ******** GLOBAL VARIABLES **************
 ****************************************/

/****************************************
 ********* FUNCTION PROTOTYPES **********
 ****************************************/
/****************************************************************************************
 *@brief       (It is used to initialize the configuration module)
 *
 *@param[IN] updatedConfigNotifCB (Indicates pointer to function which will be called)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 ****************************************************************************************/
returnCode_e initConfigModule(updatedConfigNotifCB);

/******************************************************************
 *@brief    (It is used to Deinitialize the configuration module)
 *
 *@param[IN] void   (No arguments passed)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e deInitConfigModule(void);

/******************************************************************
 *@brief  (It is used to set parameters of configuration module)
 *
 *@param[IN] commandCode_e (Indicates type of configuration parameter)
 *@param[IN] void * (Indicates pointer to configuration parameter which is comes from BLE module)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e setConfigModuleParam(commandCode_e type, void *);

/******************************************************************
 *@brief  (It is used to get parameters of configuration module)
 *
 *@param[IN] configParam_e (Indicates type of required parameter)
 *@param[OUT] void * (Indicates pointer to parameter which is required by Monitoring & Tel. Module)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e  getConfigModuleParam(configParam_e type , void *);

/******************************************************************
 *@brief  (It is used to get parameters of configuration module for BLE module)
 *
 *@param[IN] commandCode_e (Indicates type of required parameter)
 *@param[OUT] void * (Indicates pointer to parameter which is required by BLE Module)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e  getConfigForBle(commandCode_e type , void *);

/******************************************************************
 *@brief  (It is used to provide LED pattern database configuration.)
 *
 *@param[IN] None
 *@param[OUT] void * (Indicates pointer to LED patterns configuration)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e getLedPatterns(void *);

/******************************************************************
 *@brief  (This API is used to get paired TPMS information)
 *
 *@param[IN] None
 *@param[OUT] tpmsPairedInfo_t * (Pointer to structure of paired TPMS information)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e  getPairedTpmsList(tpmsPairedInfo_t *);

/******************************************************************
 *@brief  (It is used to get the tyre details for given tyre position)
 *
 *@param[IN] None
 *@param[OUT] tyreInfo_t * (Pointer to structure of tyre detail)
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e getTyreDetail(tyreInfo_t *);

/******************************************************************
 *@brief  (It is used to get the tread depth history for given tyre position)
 *
 *@param[IN] None
 *@param[OUT] configuredTreadRecordInfo_t * (Pointer to structure of tread depth history)
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e getTreadDepthHistory(configuredTreadRecordInfo_t *);

/******************************************************************
 *@brief  (This API is used to get attached HALO SN information)
 *
 *@param[IN] None
 *@param[OUT] attachedHaloSNInfo_t * (Pointer to structure of attached HALO SN information)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e  getAttachedHaloSNList(attachedHaloSNInfo_t *);

/******************************************************************
 *@brief  (This API is used to indicate application to start initial config)
 *
 *@param[IN] None
 *param[OUT] None
 *
 *********************************************************************/
void startDeviceConfiguration();

returnCode_e setVendorTypeTemp(sensorVendorType_e sensor);
returnCode_e getVendorTypeTemp(sensorVendorType_e *sensor);

#endif      /*_CONFIGURATION_H_*/
