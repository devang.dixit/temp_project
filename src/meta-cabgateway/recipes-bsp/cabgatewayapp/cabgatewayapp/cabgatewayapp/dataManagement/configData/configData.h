/************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file dataStorage.h
 * @brief (Header file for Data storage module.)
 *
 * @Author     - VT
 *************************************************************************************************
 * History
 *
 * July/24/2018, VT , First Draft
 *************************************************************************************************/

#ifndef _CFG_DATA_H_
#define _CFG_DATA_H_

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <fcntl.h>
#include <time.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include "commonData.h"
#include "error.h"
#include "debug.h"
#include "diag.h"

/****************************************
 ************* DEFINES ******************
 ****************************************/
/* File paths for each data file */
#define CFG_VEHICLE_INFO_FILE_PATH      "/media/userdata/dataConfig/vehicleInfo.csv"
#define CFG_FILE_PATH_EVENT_SUMM        	"/media/userdata/dataConfig/eventSummary.txt"

/* Directory path to locate all data file */
#define DIR_PATH_CFG_DATA       "/media/userdata/dataConfig"

/* Zip file name format */
#define CFG_ZIP_FILE_NAME               "R%02d_%02d_%02d_%02d__CG_%ld.zip"

/*Max directory name length*/
#define MAX_CFG_DIRNAME_LEN			(200)
#define MAX_CFG_FILENAME_LEN			(300)

#define MAX_LEN_COMMAND_IN_CFG_FILE		(300)

/****************************************
 ************ STRUCTURES ****************
 ****************************************/
/* This stucture is used to define summary data for different sensors */
typedef struct cfgSummary {
    uint32_t vehicleInfoCnt;
    uint16_t zipFileCnt;
} cfgSummary_t;

/****************************************
 ******** GLOBAL VARIABLES **************
 ****************************************/
cfgSummary_t g_cfgEventSumm;      //event cfgSummary variable which contains information for all sensors

/****************************************
 ********* FUNCTION PROTOTYPES **********
 ****************************************/
/******************************************************************
 *@brief  (This API is used to initialize data storage module)
 *
 *@param[IN] uint16_t (Indicates maximum number of storage package file.)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e initCfgStorageModule(uint16_t noOfPackage, char *swVersion, char *inputDirName);


/******************************************************************
 *@brief  (This API is used to Deinitialize data storage module)
 *
 *@param[IN] void  (No arguments passed)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e deInitCfgStorageModule(void);

/******************************************************************
 *@brief  (This API is used to set the event data to specific storage file as per device type)
 *
 *@param[IN] void * (Indicates pointer to structure of device data)
 *@param[IN] sysDataType_e  (Indicates device type)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e eventCfgSet(void * , sysDataType_e);

/******************************************************************
 *@brief  (This API is used to create a file package to send data on cloud)
 *
 *@param[IN] time_t (Indicates epoch time to give unique name to zip file)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e createCfgFilePackage(time_t packageTime);

/******************************************************************
 *@brief  (This API is used to get the path of package file to access that file)
 *
 *@param[IN] None
 *@param[OUT] void * (Indicates pointer to get path of package file)
 *@param[OUT] void * (Indicates pointer to flag which indicates that the package file is latest mandatory configuration)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e getCfgPackageFile(void *, void *);

/******************************************************************
 *@brief  (This API is used to delete all package files from dataStorage directory)
 *
 *@param[IN] None
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e deleteCfgPackageFile(void *);


/******************************************************************
 *@brief  (This API is used to set Changed configuration event data in configuration event file)
 *
 *@param[IN] void * (Indicates pointer to structure of changed configuration data)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e configEventSetForCfgFile(void * data);
#endif      /*_DATA_STORAGE_H_*/
