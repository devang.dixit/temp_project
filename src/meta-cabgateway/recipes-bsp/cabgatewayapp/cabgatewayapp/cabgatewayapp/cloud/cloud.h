/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : cloud.h
 * @brief : Header file for cloud.h
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Sep/17/2018, VT , First Draft
 ***************************************************************/
#ifndef __CLOUD_H__
#define __CLOUD_H__

#include "configuration.h"


#define SW_VERSION_STRING_FORMAT	"R%02d_%02d_%02d_%02d"
#define SHA256_KEYGEN_MAX_TRY		3

/**
 * @enum restApiType
 *
 * Enumeration for cloud action.
 **/
typedef enum restApiType_e {
    ZIP_POST_DATA,          /**< send zip data */
    JSON_POST_DATA,         /**< send json data */
    OTA_GET_DATA,           /**< get ota json data */
    OTA_GET_DOWNLOAD_DATA,      /**< download ota data */
    REGISTER_POST_DATA,           /**< get register token */
    NOTIFY_GW_VERSION
} restApiType_e;

/**
 * @enum callBackNotificationType
 *
 * Enumeration for callback action.
 **/
typedef enum callBackNotificationType_e {
	CLOUD_OTA_DATA = 0,
	CLOUD_CONNECTION_STATUS,
    CONFIG_CHANGE_REQUEST,
	MAX_CLOUD_NOTIFICATION_TYPE
} callBackNotificationType_e;

/**
 * @struct cloudInitInfo
 * @brief Init cloud information
 *
 * This structure have information related cloud and
 * system info which required for cloud
 **/
typedef struct cloudInitInfo {
    char url[MAX_LEN_CLOUD_URI];
    char gatewayID[MAX_LEN_GATEWAY_ID];
} cloudInitInfo_t;


/**
 * @struct file storage information
 * @brief file name and file location related information
 *
 **/
typedef struct fileStorageInfo {
    char filename[MAX_LEN_FILENAME];
    char storagePath[MAX_LEN_PATH];
} fileStorageInfo_t;
/**
 * @struct otaInfo
 * @brief ota information structure
 *
 * This structure have information related ota
 **/
typedef struct otaInfo {
    char version[MAX_LEN_OTA_VERSION];
    char updateVersion[MAX_LEN_OTA_VERSION];
    char md5sum[MAX_LEN_MD5SUM + 1];
    char otaUrl[MAX_LEN_CLOUD_URI];
    char filename[MAX_LEN_FILENAME];
    char storagePath[MAX_LEN_PATH];
} otaInfo_t;

/**
 * @struct configInfo
 * @brief config change information structure
 *
 * This structure have information related config change
 **/
typedef struct configChangeInfo {
    bool setPointChangeAvailable;
    int tpmsThreshold[2];  // For steer and drive
    bool fleetIDChangeAvailable;
    char fleetID[MAX_LEN_FLEET_ID];
} configChangeInfo_t;

typedef struct callBackNotification {
    callBackNotificationType_e CBNotifType;
    otaInfo_t otaInfo;
    configChangeInfo_t configChangeInfo;
    bool cloudConnectionStatus;
} callBackNotification_t;

/****************************************
 ************* CALLBACKS ****************
 ****************************************/
/*It is used to register callback to notify the cloud transaction status*/
typedef void (*cloudConStatusNotifCB)(void *);

/** @brief Initialize of cloud resource
 *
 * Initalization of libcurl and connection related parameter
 *
 * @return status code.
 */
returnCode_e cloudInit(cloudInitInfo_t *cloudInitInfo, char *swVersion, cloudConStatusNotifCB);

/** @brief DeInitialize of cloud resource
 *
 * DeInitialize of libcurl and release connection related parameter
 *
 * @return status code.
 */
returnCode_e cloudDeinit(void);

/** @brief cloud communication using rest api
 *
 * @param[in] report data type
 * @param[in] report data
 *
 * @return status code.
 */
returnCode_e connectToCloud(restApiType_e restApiType, void *dataInfo);

/** @brief Update cloud uri
 *
 * @param[in] url URL for cloud
 *
 * @return status code.
 */
returnCode_e updateCloudUrl(const char *url);

returnCode_e getAPiToken();

returnCode_e validateGWID(char *gatewayID);
#endif /* __CLOUD_H__ */
