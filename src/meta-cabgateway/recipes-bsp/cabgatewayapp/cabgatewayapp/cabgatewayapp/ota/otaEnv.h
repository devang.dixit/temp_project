#ifndef __OTA_ENV_H__
#define __OTA_ENV_H__

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>   /* File Control Definitions */
#include <termios.h> /* POSIX Terminal Control Definitions */
#include <unistd.h>  /* UNIX Standard Definitions */
#include <errno.h>   /* ERROR Number Definitions */
#include <signal.h>
#include <sys/types.h>
#include "error.h"
#include "debug.h"
#include <unistd.h>
#include <pthread.h>
#include <netdb.h>

/****************************************
 ************ DEFINES ******************
 ****************************************/
#define OTA_ENV_DIR_PATH "/cabApp/ota"
#define OTA_ENV_FILE_PATH "/cabApp/ota/otaEnv.txt"
#define OTA_ENV_FILE_NAME "otaEnv.txt"
#define BLE_RETRY_COUNT		3
#define RECOVERY_COUNT_LIMIMT	6
/****************************************
 ************ structures ******************
 ****************************************/
typedef struct otaEnv {
	uint8_t update_available;
	uint8_t backup_available;
	uint8_t zb_updated;
	uint8_t ble_updated;
	uint8_t can_updated;
	uint8_t rf_updated;
	uint8_t cabApp_updated;
	uint8_t parentApp_updated;
	uint8_t boot_count;
	uint8_t full_update; /*1=full update 0=incremental update*/
	/* Added in 3.12.0.0 */
	uint8_t remoteDiagApp_updated;
	uint8_t lteApp_updated;
	uint8_t ebyte_updated;
} otaEnv_t;

typedef enum otaEnvInfo{
	OTA_ZIGBEE = 0,
	OTA_BLE,
	OTA_CAN,
	OTA_RF,
	OTA_CABAPP,
	OTA_PARENTAPP,
	OTA_BOOT_COUNT,
	OTA_FULL_UPDATE,
	OTA_UPDATE_AVAILABLE,
	OTA_BACKUP_AVAILABLE,
	OTA_EBYTE,
	OTA_REMOTEDIAGAPP,
	OTA_LTEAPP,
	MAX_OTA_ENV_INFO
} otaEnvInfo_e;

typedef enum otaType{
	OTA_TYPE_INCR_UPDATE = 0,
	OTA_TYPE_FULL_UPDATE = 1
} otaType_e;
/****************************************
 ************* FUNCTIONS ****************
 ****************************************/

/*********************************************************************
 *@brief          : Read OTA Environment variables
 *
 *@param[IN]      : None
 *@param[OUT]     : otaEnd
 *
 *@return         : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e readOTAEnv(otaEnv_t *otaEnv);

/*********************************************************************
 *@brief          : Write OTA Environment variables
 *
 *@param[IN]      : None
 *@param[OUT]     : None
 *
 *@return         : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e writeOTAEnv(otaEnv_t *otaEnv);

/*********************************************************************
 *@brief          : Increment boot count and compare it with target limit
 *
 *@param[IN]      : None
 *@param[OUT]     : if limit crossed or not
 *
 *@return         : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e incrementBootCount(uint8_t *limitCrossed);

/*********************************************************************
 *@brief          : Read boot count
 *
 *@param[IN]      : None
 *@param[OUT]     : boot count
 *
 *@return         : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e getBootCount(uint8_t *bootcount);

/*********************************************************************
 *@brief          : Reset boot count
 *
 *@param[IN]      : None
 *@param[OUT]     : boot count
 *
 *@return         : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e resetBootCount(void);
#endif // __OTA_ENV_H__
