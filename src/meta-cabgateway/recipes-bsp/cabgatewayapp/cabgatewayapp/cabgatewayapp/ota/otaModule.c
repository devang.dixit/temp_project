#include <stdint.h>
#include <string.h>
#include <openssl/md5.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <dirent.h>
#include "otaModule.h"
#include "otaEnv.h"
#include "cloud.h"
#include "diag.h"
#include "hwVersion.h"

/****************************************
 ******** STATIC VARIABLES **************
 ****************************************/
static pthread_mutex_t g_otaInfoFileLock = PTHREAD_MUTEX_INITIALIZER;

static void replaceSpacesWithEscapeSpace(char* source, uint8_t *isSpace, char *dest)
{
  char *destPtr=dest;
  char tempChar;
  *isSpace = 0;
  while(*source != 0)
  {
    *destPtr = *source++;
    if(*destPtr == ' ' || *destPtr == '!' || *destPtr == '@' ||
       *destPtr == '#' || *destPtr == '$' || *destPtr == '%' ||
       *destPtr == '^' || *destPtr == '&' || *destPtr == '*' ||  
       *destPtr == '(' || *destPtr == ')' || *destPtr == '-' || 
       *destPtr == '\'' || *destPtr == '\"' || *destPtr == '?' 
       ) {
      tempChar = *destPtr;
      *destPtr++ = '\\';
      *destPtr = tempChar;
      *isSpace = 1;
    }
    destPtr++;
  }
  *destPtr = 0;
}

static void replaceSpacesWithUnderScore(char* source)
{
  char* i = source;
  char* j = source;
  while(*j != 0)
  {
    *i = *j++;
    if(*i == ' ' || *i == '!' || *i == '@' ||
       *i == '#' || *i == '$' || *i == '%' ||
       *i == '^' || *i == '&' || *i == '*' ||  
       *i == '(' || *i == ')' || *i == '-' || 
       *i == '\'' || *i == '\"' || *i == '?' 
       ) {
      *i = '_';
    }

    i++;
  }
  *i = 0;
}


static returnCode_e readOTASummary(otaDetails_t *otaDetails)
{
	FUNC_ENTRY

	int summaryFile;
	uint32_t ret;

	/*Input argument validation*/
	if(otaDetails == NULL) {
		CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
		return GEN_NULL_POINTING_ERROR;
	}

	//opening event summary file for read
	pthread_mutex_lock(&g_otaInfoFileLock);
	summaryFile = open(OTA_FILE_PATH, O_RDONLY, 0644);
	if(summaryFile == -1) {
		CAB_DBG(CAB_GW_ERR, "%s file opening error.", OTA_FILE_PATH);
		pthread_mutex_unlock(&g_otaInfoFileLock);
		return FILE_OPEN_FAIL;
	}

	CAB_DBG(CAB_GW_DEBUG, "reading summary file");
	//reading event summary structure data from file
	ret = read(summaryFile, otaDetails, sizeof(otaDetails_t));
	if(ret == -1) {
		CAB_DBG(CAB_GW_ERR, "%s file reading error.", OTA_FILE_PATH);
		close(summaryFile);
		pthread_mutex_unlock(&g_otaInfoFileLock);
		return FILE_READ_ERROR;
	}
	//close event summary file
	close(summaryFile);
	pthread_mutex_unlock(&g_otaInfoFileLock);

	FUNC_EXIT
	return GEN_SUCCESS;
}

static returnCode_e writeOTASummary(otaDetails_t *otaDetails)
{
	FUNC_ENTRY

	int outFile;                               //file descriptor
	uint32_t ret;

	/*Input argument validation*/
	if(otaDetails == NULL) {
		CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
		return GEN_NULL_POINTING_ERROR;
	}

	//opening event summary file for write
	pthread_mutex_lock(&g_otaInfoFileLock);
	outFile = open(OTA_FILE_PATH, O_WRONLY | O_CREAT, 0644);
	if(outFile == -1) {
		CAB_DBG(CAB_GW_ERR, "%s file opening error.", OTA_FILE_PATH);
		pthread_mutex_unlock(&g_otaInfoFileLock);
		return FILE_OPEN_FAIL;
	}

	CAB_DBG(CAB_GW_DEBUG, "writing summary file");
	//writing event summery structure data to file
	ret = write(outFile, otaDetails, sizeof(otaDetails_t));
	if(ret == -1) {
		CAB_DBG(CAB_GW_ERR, "%s file writing error.", OTA_FILE_PATH);
		close(outFile);
		pthread_mutex_unlock(&g_otaInfoFileLock);
		return FILE_WRITE_ERROR;
	}

	fsync(outFile);
	//close event summary file
	close(outFile);
	pthread_mutex_unlock(&g_otaInfoFileLock);

	FUNC_EXIT
	return GEN_SUCCESS;
}

static returnCode_e verifyMd5sum(char *fileName, char *refMd5sum, uint8_t *isMd5sumVerfied)
{
	FUNC_ENTRY

	returnCode_e retCode = GEN_SUCCESS;

	char calMd5sum[MD5_DIGEST_LENGTH];
	char convertedMd5sum[MAX_LEN_MD5SUM + 1];
	int index, stringIndex;
	MD5_CTX mdContext;
	int bytes;
	unsigned char data[1024];


	if(!fileName || !refMd5sum || !isMd5sumVerfied) {
		CAB_DBG(CAB_GW_ERR, "Invalid arg");
		DIAG_DUMP( "Invalid arg\n");
		return GEN_NULL_POINTING_ERROR;
	}

	FILE *inFile = fopen (fileName, "rb");
	if (inFile == NULL) {
		CAB_DBG(CAB_GW_ERR, "%s can't be opened.", fileName);
		DIAG_DUMP( "%s can't be opened.\n", fileName);
		return FILE_OPEN_FAIL;
	}

	MD5_Init(&mdContext);
	while ((bytes = fread (data, 1, 1024, inFile)) != 0) {
		MD5_Update (&mdContext, data, bytes);
	}

	MD5_Final (calMd5sum, &mdContext);

	for(index = 0; index < MD5_DIGEST_LENGTH; index++) {
		CAB_DBG(CAB_GW_TRACE, "%02x", calMd5sum[index]);
	}

	fclose (inFile);

	for(index = 0, stringIndex = 0; index < MD5_DIGEST_LENGTH; index++) {
		sprintf(convertedMd5sum + stringIndex, "%02x", calMd5sum[index]);
		stringIndex = stringIndex + 2;
	}

	for(index = 0; index < MAX_LEN_MD5SUM; index++) {
		CAB_DBG(CAB_GW_TRACE, "%02x", convertedMd5sum[index]);
	}
	CAB_DBG(CAB_GW_INFO,"\r\n");

	for(index = 0; index < MAX_LEN_MD5SUM; index++) {
		CAB_DBG(CAB_GW_TRACE, "%02x", refMd5sum[index]);
	}

	CAB_DBG(CAB_GW_INFO, "===============================================\r\n");
	CAB_DBG(CAB_GW_INFO, "Calculated MD5Sum : %s\r\n", convertedMd5sum);
	CAB_DBG(CAB_GW_INFO, "Reference  MD5Sum : %s\r\n", refMd5sum);
	CAB_DBG(CAB_GW_INFO, "===============================================\r\n");
	DIAG_DUMP("Calculated MD5Sum : %s\r\n", convertedMd5sum);
	DIAG_DUMP("Reference  MD5Sum : %s\r\n", refMd5sum);

	if(!strcmp(refMd5sum, convertedMd5sum)) {
		CAB_DBG(CAB_GW_DEBUG, "MD5 verified");
		*isMd5sumVerfied = 1;
	}
	else {

	}
	FUNC_EXIT
	return retCode;
}


static returnCode_e otaDirectoryCleanup(void)
{
	FUNC_ENTRY

	returnCode_e retCode = GEN_SUCCESS;
	otaDetails_t otaDetails = {0};
	char file[MAX_LEN_FILENAME];
	struct dirent *entry;
	int status;
	DIR *otaDir;

	memset(&otaDetails, 0, sizeof(otaDetails_t));

	/*	if file exists, read the ota information	*/
	if(access(OTA_FILE_PATH, F_OK) == 0) {
		readOTASummary(&otaDetails);
	}

	otaDir = opendir(OTA_DIR_PATH);
	if(otaDir == NULL) {
		CAB_DBG(CAB_GW_ERR, "%s directory opening error.", OTA_DIR_PATH);
		retCode = OTA_CLEANUP_FAIL;
		goto failure;
	}

	CAB_DBG(CAB_GW_TRACE, "Cleaning up %s", OTA_DIR_PATH);
	while((entry = readdir(otaDir)) != NULL) {
		if( (strcmp(entry->d_name, ".") == 0) || (strcmp(entry->d_name, "..") == 0) || 
		    (strcmp(entry->d_name, OTA_FILE_NAME) == 0)  || (strcmp(entry->d_name, otaDetails.otaInfo.filename) == 0) ||
		    (strcmp(entry->d_name, OTA_ENV_FILE_NAME) == 0)
		) {
			continue;
		}
		sprintf(file, "%s/%s", OTA_DIR_PATH, entry->d_name);
		status = remove(file);
		if(status != 0) {
			CAB_DBG(CAB_GW_ERR, "Error in deleting the package file");
			retCode = OTA_CLEANUP_FAIL;
			goto failure;
		}
	}

failure:
	FUNC_EXIT
	return retCode;
}

int8_t compareVersion(char *updatedVersion, char *currentVersion)
{
	uint32_t updatedMajor, currentMajor;
	uint32_t updatedMinor, currentMinor;
	uint32_t updatedRC, currentRC;
	uint32_t updatedInternal, currentInternal;

	if(strcmp(updatedVersion, currentVersion) != 0) {

		sscanf(updatedVersion, "%u.%u.%u.%u", &updatedMajor, &updatedMinor, &updatedRC, &updatedInternal);
		sscanf(currentVersion, "%u.%u.%u.%u", &currentMajor, &currentMinor, &currentRC, &currentInternal);

		if( (updatedMajor > currentMajor) || 
			((updatedMajor >= currentMajor) && (updatedMinor > currentMinor)) ||
			((updatedMajor >= currentMajor) && (updatedMinor >= currentMinor) && (updatedRC > currentRC)) || 
			((updatedMajor >= currentMajor) && (updatedMinor >= currentMinor) 
				&& (updatedRC >= currentRC) && (updatedInternal > currentInternal)) ) {
			CAB_DBG(CAB_GW_INFO, "Software version %s is greater than %s", updatedVersion, currentVersion);
			DIAG_DUMP("Software version %s is greater than %s", updatedVersion, currentVersion);
			return 1;
		}
		else {
			CAB_DBG(CAB_GW_INFO, "Software version %s is less than %s", updatedVersion, currentVersion);
			DIAG_DUMP("Software version %s is less than %s", updatedVersion, currentVersion);
			return -1;
		}
	}
	else {
		CAB_DBG(CAB_GW_INFO, "Software version %s is equal to %s", updatedVersion, currentVersion);
		DIAG_DUMP("Software version %s is equal to %s", updatedVersion, currentVersion);
		return 0;
	}
}

returnCode_e checkPreviousOTAType(uint8_t *full_update)
{
	FUNC_ENTRY
	returnCode_e retCode = GEN_SUCCESS;
	otaEnv_t otaEnv;

	*full_update = 0;
	retCode = readOTAEnv(&otaEnv);
	if(retCode != GEN_SUCCESS){
		CAB_DBG(CAB_GW_ERR,"Error extracting previous OTA details");
		DIAG_DUMP("Error extracting previous OTA details");
		FUNC_EXIT
		return retCode;
	}
	else{
		*full_update = otaEnv.full_update;
	}
	FUNC_EXIT
	return retCode;
}
returnCode_e otaInit(void)
{
	FUNC_ENTRY
	struct stat st = {0};
	otaDetails_t storedOTADetails;
	returnCode_e retCode = GEN_SUCCESS;
	otaEnv_t otaEnv;

	CAB_DBG(CAB_GW_INFO, "Initializing OTA module");

	/*	To check for the ota directory and if it not exist then create it	*/
	if(stat(OTA_DIR_PATH, &st) == -1) {
		if((mkdir(OTA_DIR_PATH, 0755)) == -1) {
			CAB_DBG(CAB_GW_ERR, "OTA directory not created");
			return DIR_CREATE_FAIL;
		}
	}
	/*	if file exists, read the ota information	*/
	if(access(OTA_FILE_PATH, F_OK) == 0) {
		CAB_DBG(CAB_GW_DEBUG, "file : %s exists", OTA_FILE_PATH);
		retCode = readOTASummary(&storedOTADetails);
		/* this case will come if rebooted at upgrade progess stage, so
			roll back ble image for safer side.	*/ 
		if( (retCode == GEN_SUCCESS) && (storedOTADetails.state == UPGRADE_PROGRESS) ) {
			CAB_DBG(CAB_GW_INFO, "Seems like OTA was failed in previous boot");
			DIAG_DUMP("Seems like OTA was failed in previous boot\r\n");
			CAB_DBG(CAB_GW_INFO, "Extracting information about components updated in last OTA");
			DIAG_DUMP("Extracting information about components updated in last OTA\r\n");
                        retCode = readOTAEnv(&otaEnv);
                        if(retCode != GEN_SUCCESS){
                                remove(OTA_FILE_PATH);
                                otaDirectoryCleanup();
                                FUNC_EXIT
                                return OTA_UPGRADE_FAIL;
                        }
			CAB_DBG(CAB_GW_INFO, "Reverting back to previous working image ...");
			DIAG_DUMP("Reverting back to previous working image ...\r\n");
			retCode = rollBackFirmwareComponents(&otaEnv);
			if(GEN_SUCCESS == retCode) {
				CAB_DBG(CAB_GW_INFO, "All components are rolled back successfully");
				DIAG_DUMP("All components are rolled back successfully\r\n");
				storedOTADetails.state = DOWNLOAD_COMPLETE;
				retCode = writeOTASummary(&storedOTADetails);
				/* if unable to write new ota image to file then remove the file stored */
				if(retCode != GEN_SUCCESS) {
					remove(OTA_FILE_PATH);
				}
			}
			retCode = OTA_RESTART_APP;
		}
	}
	FUNC_EXIT
	return retCode;
}

returnCode_e otaDeInit()
{
	FUNC_ENTRY
	returnCode_e retCode = GEN_SUCCESS;

	CAB_DBG(CAB_GW_INFO, "Deinitializing OTA Module");

	pthread_mutex_destroy(&g_otaInfoFileLock);

	FUNC_EXIT
	return retCode;
}

returnCode_e checkOTA(char *currentSwVersion, otaDetails_t *otaDetails, uint8_t *isOtaAvailable)
{
	FUNC_ENTRY
	returnCode_e retCode = GEN_SUCCESS;

	if(!currentSwVersion || !isOtaAvailable || !otaDetails) {
		CAB_DBG(CAB_GW_ERR, "Invalid arg");
		DIAG_DUMP("Invalid arg\n");
		return GEN_NULL_POINTING_ERROR;
	}

	memset(otaDetails, 0, sizeof(otaDetails_t));
	memset(isOtaAvailable, 0, sizeof(uint8_t));

	retCode = otaDirectoryCleanup();
	if(retCode != GEN_SUCCESS) {
		return retCode;
	}

	strcpy(otaDetails->otaInfo.storagePath, OTA_DIR_PATH);
	memcpy(otaDetails->otaInfo.version, currentSwVersion, strlen(currentSwVersion));

	retCode = connectToCloud(OTA_GET_DATA, &otaDetails->otaInfo);
	if(retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "connectToCloud() fail with error: %d", retCode);
		DIAG_DUMP("connectToCloud() fail with error: %d\r\n", retCode);
		if(retCode == OTA_UNAVAILABLE){
			CAB_DBG(CAB_GW_ERR,"No more OTA image available on cloud to perform !");
			DIAG_DUMP("No more OTA image available on cloud to perform !");
	                *isOtaAvailable = 0;
        	        /*Set full_update=0 in env file as no more OTA image available on cloud
                	This helps to skip restarting seperate timer for incremental OTA check*/
	                setOtaUpdateType(OTA_TYPE_FULL_UPDATE);
			return GEN_SUCCESS;
		}
		CAB_DBG(CAB_GW_INFO,"Check if previously downloaded OTA image waiting to be processed ..?");
		DIAG_DUMP("Check if previously downloaded OTA image waiting to be processed ..?");
		retCode = readOTASummary(otaDetails);
		if(retCode != GEN_SUCCESS) {
			CAB_DBG(CAB_GW_INFO,"No OTA found in waiting to get processed !");
			DIAG_DUMP("No OTA found in waiting to get processed !");
			return CHECK_OTA_FAIL;
		}
	}

	CAB_DBG(CAB_GW_INFO, "Checking OTA...");
	CAB_DBG(CAB_GW_INFO, "Current SW Version : %s", otaDetails->otaInfo.version);
	CAB_DBG(CAB_GW_INFO, "OTA SW Version : %s", otaDetails->otaInfo.updateVersion);
	CAB_DBG(CAB_GW_INFO, "OTA File Path : %s", otaDetails->otaInfo.storagePath);
	CAB_DBG(CAB_GW_INFO, "OTA File Name : %s", otaDetails->otaInfo.filename);
	CAB_DBG(CAB_GW_INFO, "OTA URL : %s", otaDetails->otaInfo.otaUrl);
	CAB_DBG(CAB_GW_INFO, "OTA File MD5sum : %s", otaDetails->otaInfo.md5sum);
	DIAG_DUMP("Checking OTA...\r\nCurrent SW Version : %s \r\nOTA SW Version : %s \r\n OTA File Path : %s \r\n	OTA File Name : %s \r\n	OTA URL : %s \r\n OTA File MD5sum : %s\r\n" \
	, otaDetails->otaInfo.version , otaDetails->otaInfo.updateVersion, otaDetails->otaInfo.storagePath \
	, otaDetails->otaInfo.filename, otaDetails->otaInfo.otaUrl, otaDetails->otaInfo.md5sum);

	if(compareVersion(otaDetails->otaInfo.updateVersion, currentSwVersion) > 0) {
		CAB_DBG(CAB_GW_INFO, "OTA available");
		DIAG_DUMP("OTA available\r\n");
		*isOtaAvailable = 1;
		otaDetails->state = MAX_OTA_STATE;
	}
	else {
		CAB_DBG(CAB_GW_INFO, "OTA not available. Updating env info ..");
		DIAG_DUMP("OTA not available. Updating env info ..");
		*isOtaAvailable = 0;
		/*Set full_update=0 in env file as no more OTA image available on cloud
		This helps to skip restarting seperate timer for incremental OTA check*/
		setOtaUpdateType(OTA_TYPE_FULL_UPDATE);	
	}

	FUNC_EXIT
	return retCode;
}

returnCode_e downloadOTA(otaDetails_t *latestOTADetails, uint8_t *isDownloadCompleted)
{
	FUNC_ENTRY

	returnCode_e retCode = GEN_SUCCESS;
	uint8_t isMd5sumVerfied = 0;
	char fileName[MAX_LEN_PATH+MAX_LEN_FILENAME];
	otaDetails_t storedOTADetails;
	bool downloadStart = true;
	bool storedOTAvailable = false;

	memset(&storedOTADetails, 0, sizeof(otaDetails_t));
	memset(fileName, 0, sizeof(fileName));
	memset(isDownloadCompleted, 0, sizeof(uint8_t));

	if(!isDownloadCompleted || !latestOTADetails) {
		CAB_DBG(CAB_GW_ERR, "Invalid arg\n");
		DIAG_DUMP( "Invalid arg\n");
		return GEN_NULL_POINTING_ERROR;
	}

	/*	if file exists, read the ota information	*/
	if(access(OTA_FILE_PATH, F_OK) == 0) {
		CAB_DBG(CAB_GW_INFO, "Reading previously downloaded ota information file before downloading new ota file");
		DIAG_DUMP("Reading previously downloaded ota information file before downloading new ota file\r\n");
		/* variable to know whether any ota image is stored or not	*/
		storedOTAvailable = true;
		retCode = readOTASummary(&storedOTADetails);
		if(retCode == GEN_SUCCESS) {
			/*	Assuming downgrading the version will not be there, just compared both versions	*/
			if(compareVersion(latestOTADetails->otaInfo.updateVersion, storedOTADetails.otaInfo.updateVersion) <=  0) {
				/* no need to download if both are equal as it would be already downloded and stored in flash 
				, but still check md5sum for stored OTA file */
				snprintf(fileName, sizeof fileName, "%s/%s", storedOTADetails.otaInfo.storagePath, storedOTADetails.otaInfo.filename);
				retCode = verifyMd5sum(fileName, storedOTADetails.otaInfo.md5sum, &isMd5sumVerfied);
				if(retCode == GEN_SUCCESS) {
					if(isMd5sumVerfied) {
						CAB_DBG(CAB_GW_INFO, "MD5 verified. No need to download ota file");
						DIAG_DUMP("MD5 verified. No need to download ota file\r\n");
						downloadStart = false;
					}
				}
				memset(fileName, 0, sizeof(fileName));
			}
		}
	}

	if(downloadStart == true) {
		CAB_DBG(CAB_GW_INFO, "Downloading OTA...");
		DIAG_DUMP( "Downloading OTA...");
		retCode = connectToCloud(OTA_GET_DOWNLOAD_DATA, &latestOTADetails->otaInfo);
		if(retCode != GEN_SUCCESS) {
			CAB_DBG(CAB_GW_ERR, "connectToCloud() fail with error: %d", retCode);
			DIAG_DUMP("connectToCloud() fail with error: %d\r\n", retCode);
			/* if new ota image download fails and we have already old ota image stored */
			goto checkOtaSummary;
		}
		snprintf(fileName, sizeof fileName, "%s/%s", latestOTADetails->otaInfo.storagePath, latestOTADetails->otaInfo.filename);

		CAB_DBG(CAB_GW_INFO, "OTA Image Downloaded is %s", fileName);
		DIAG_DUMP("OTA Image Downloaded is %s\r\n", fileName);
		retCode = verifyMd5sum(fileName, latestOTADetails->otaInfo.md5sum, &isMd5sumVerfied);
		if(retCode != GEN_SUCCESS) {
			CAB_DBG(CAB_GW_ERR, "verifyMd5sum() fail with error: %d", retCode);
			DIAG_DUMP("verifyMd5sum() fail with error: %d\r\n", retCode);
			goto checkOtaSummary;
		}

		if(!isMd5sumVerfied) {
			CAB_DBG(CAB_GW_ERR, "md5sum for %s/%s is not verified", latestOTADetails->otaInfo.storagePath, \
					latestOTADetails->otaInfo.filename);
			DIAG_DUMP("md5sum for %s/%s is not verified  \n", latestOTADetails->otaInfo.storagePath, \
					latestOTADetails->otaInfo.filename);
			retCode = CHECKSUM_FAIL;
			goto checkOtaSummary;
		}

		CAB_DBG(CAB_GW_INFO, "Md5sum verified OK");
		DIAG_DUMP("Md5sum verified OK\r\n");
		latestOTADetails->state = DOWNLOAD_COMPLETE;

		retCode = writeOTASummary(latestOTADetails);
		/* if unable to write new ota image to file then remove the file stored */
		if(retCode != GEN_SUCCESS) {
			remove(OTA_FILE_PATH);
			otaDirectoryCleanup();
		}
		else {
			*isDownloadCompleted = 1;
		}
	}
checkOtaSummary:
	if(storedOTAvailable == true) {
		*isDownloadCompleted = 1;
		retCode = GEN_SUCCESS;
	}
	FUNC_EXIT
	return retCode;
}

returnCode_e flash_ebyte_fw(void)
{
	FUNC_ENTRY
    returnCode_e retCode;
    uint8_t isEbyteFlashed = 1;     /* 1 indicates error */
    uint8_t retryCount = 0;
    bool flag = false;

    retCode = isCC1310OTAAvailable(&flag);
    if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Function isCC1310OTAAvailable() failed with code %d", retCode);
	}

	if (!flag) {
		CAB_DBG(CAB_GW_INFO, "CC1310 OTA capability is not available in this HW version. Skipping the CC1310 FW update");
		FUNC_EXIT
		return GEN_SUCCESS;
	}

    while ((isEbyteFlashed != 0) && (retryCount < BLE_RETRY_COUNT)) {
        CAB_DBG(CAB_GW_INFO, "Recovering Ebyte FW [retrycount: %d]\r\n", retryCount);
        isEbyteFlashed = WEXITSTATUS(system("/cabApp/ebyte/flashEbyteFW.sh"));
        retryCount++;
    }

    if(0 == isEbyteFlashed)
        retCode = GEN_SUCCESS;
    else
        retCode = OTA_UPGRADE_FAIL;

    FUNC_EXIT
    return retCode;
}

returnCode_e flash_ble_fw(void)
{
	FUNC_ENTRY
	returnCode_e retCode;
	uint8_t isBleFlashed = 1;	/* initialized by 1 indicates error */
	uint8_t retryCount = 0;

	while( (isBleFlashed != 0) && (retryCount < BLE_RETRY_COUNT) ) {
		CAB_DBG(CAB_GW_INFO, "Recovering BLE softdevice [retrycount: %d]", retryCount);
		DIAG_DUMP("Recovering BLE softdevice [retrycount: %d]\r\n", retryCount);
		isBleFlashed = WEXITSTATUS(system("/cabApp/ble/flashble-sd.sh MOUNT_PARTITION"));
		retryCount++;
	}

	if(isBleFlashed == 0) {
		isBleFlashed = 1;	/* initialized by default value	*/
		retryCount = 0;		/* initialized by default value */
		while( (isBleFlashed != 0) && (retryCount < BLE_RETRY_COUNT) ) {
			CAB_DBG(CAB_GW_INFO, "Recovering BLE applicataion [retrycount: %d]", retryCount);
			DIAG_DUMP("Recovering BLE applicataion [retrycount: %d]\r\n", retryCount);
			isBleFlashed = WEXITSTATUS(system("/cabApp/ble/flashble-app.sh MOUNT_PARTITION"));
			retryCount++;
		}
	}
	if(0 == isBleFlashed)
		retCode = GEN_SUCCESS;
	else
		retCode = OTA_UPGRADE_FAIL;
	FUNC_EXIT
	return retCode;
}
static returnCode_e rollback_zb_fw(void)
{
	FUNC_ENTRY
	return GEN_SUCCESS;
	FUNC_EXIT
}
static returnCode_e rollback_can_fw(void)
{
	FUNC_ENTRY
	return GEN_SUCCESS;
	FUNC_EXIT
}
static returnCode_e rollback_rf_fw(void)
{
	FUNC_ENTRY
	return GEN_SUCCESS;
	FUNC_EXIT
}

returnCode_e  rollBackFirmwareComponents(otaEnv_t *otaEnv)
{
	FUNC_ENTRY
	if(otaEnv->ble_updated){
		//revert back BLE firmware
		if(GEN_SUCCESS != flash_ble_fw()){
			CAB_DBG(CAB_GW_ERR,"Error roll back BLE firmware image");
			DIAG_DUMP("Error roll back BLE firmware image\r\n");
			FUNC_EXIT
			return OTA_RESTART_APP;
		}
	}
	if(otaEnv->zb_updated){
		//revert back ZigBee firmware
		if(GEN_SUCCESS != rollback_zb_fw()){
			CAB_DBG(CAB_GW_ERR,"Error roll back ZigBee firmware image");
			DIAG_DUMP("Error roll back ZigBee firmware image\r\n");
			FUNC_EXIT
			return OTA_RESTART_APP;
		}
	}
	if(otaEnv->can_updated){
		//revert back CAN firmware
		if(GEN_SUCCESS != rollback_can_fw()){
			CAB_DBG(CAB_GW_ERR,"Error roll back CAN firmware image");
			DIAG_DUMP("Error roll back CAN firmware image\r\n");
			FUNC_EXIT
			return OTA_RESTART_APP;
		}
	}
	if(otaEnv->rf_updated){
		//revert back RF firmware
		if(GEN_SUCCESS != rollback_rf_fw()){
			CAB_DBG(CAB_GW_ERR,"Error roll back RF firmware image");
			DIAG_DUMP("Error roll back RF firmware image\r\n");
			FUNC_EXIT
			return OTA_RESTART_APP;
		}
	}
	if(otaEnv->ebyte_updated){
		//revert back Ebyte firmware
		if(GEN_SUCCESS != flash_ebyte_fw()){
			CAB_DBG(CAB_GW_ERR,"Error roll back Ebyte firmware image");
			DIAG_DUMP("Error roll back Ebyte firmware image\r\n");
			FUNC_EXIT
			return OTA_RESTART_APP;
		}
	}
	FUNC_EXIT
	return GEN_SUCCESS;
}

returnCode_e processOTA(uint8_t *isOTADone)
{
	FUNC_ENTRY
	returnCode_e retCode = GEN_SUCCESS;
	otaDetails_t storedOTADetails;
	char escapeFilename[MAX_LEN_FILENAME];
	char otaCommand[MAX_LEN_PATH+MAX_LEN_FILENAME];
	uint8_t isSpacePresent = 0;
	otaEnv_t otaEnv;

	memset(isOTADone, 0, sizeof(uint8_t));
	memset(&storedOTADetails, 0, sizeof(otaDetails_t));

	/*	if file exists, read the ota information	*/
	if(access(OTA_FILE_PATH, F_OK) == 0) {

		retCode = readOTASummary(&storedOTADetails);
		if(retCode == GEN_SUCCESS) {
			storedOTADetails.state = UPGRADE_PROGRESS;

			retCode = writeOTASummary(&storedOTADetails);
			/* if unable to write new ota image to file then remove the file stored */
			if(retCode != GEN_SUCCESS) {
				remove(OTA_FILE_PATH);
				otaDirectoryCleanup();
				return OTA_UPGRADE_FAIL;
			}
			memset(otaCommand, 0, sizeof(otaCommand));
			isSpacePresent = 0;
			replaceSpacesWithEscapeSpace(storedOTADetails.otaInfo.filename, &isSpacePresent, escapeFilename);
			if (isSpacePresent == 1) {
				replaceSpacesWithUnderScore(storedOTADetails.otaInfo.filename);
				snprintf(otaCommand, sizeof(otaCommand), "mv %s/%s %s/%s", \
					storedOTADetails.otaInfo.storagePath, escapeFilename, storedOTADetails.otaInfo.storagePath, storedOTADetails.otaInfo.filename);
				system(otaCommand);
				retCode = writeOTASummary(&storedOTADetails);
				/* if unable to write new ota image to file then remove the file stored */
				if(retCode != GEN_SUCCESS) {
					remove(OTA_FILE_PATH);
					otaDirectoryCleanup();
					return OTA_UPGRADE_FAIL;
				}
			}

			memset(otaCommand, 0, sizeof(otaCommand));
			snprintf(otaCommand, sizeof(otaCommand), "/cabApp/flashOTA.sh %s/%s ", \
				storedOTADetails.otaInfo.storagePath, storedOTADetails.otaInfo.filename);
			
	        	signal(SIGCHLD,SIG_DFL); 
			CAB_DBG(CAB_GW_INFO, "Running OTA script %s\n", otaCommand);
			DIAG_DUMP("Running OTA script %s", otaCommand);
			*isOTADone = WEXITSTATUS(system(otaCommand));
			retCode = readOTAEnv(&otaEnv);
			if(retCode != GEN_SUCCESS){
				remove(OTA_FILE_PATH);
                                otaDirectoryCleanup();
				FUNC_EXIT
                                return OTA_UPGRADE_FAIL;
			}
			/*isOTADone = 0 meaning OTA is successful*/
			if(*isOTADone == 0) {
				//Cleanup OTA dir & remove OTA file for incremental update
				if(!otaEnv.full_update){
					remove(OTA_FILE_PATH);
                                        otaDirectoryCleanup();
				}
				/* Reset ble for 120 seconds */
				CAB_DBG(CAB_GW_INFO, "Resetting BLE module...");
				DIAG_DUMP("Resetting BLE module... \r\n");
				system("/cabApp/ble/resetble.sh");
				sleep(120);

				CAB_DBG(CAB_GW_INFO, "Merging the SYSLOG from RAM");
				if (getAndMergeLogsFromRAM() == GEN_SUCCESS) {
					CAB_DBG(CAB_GW_INFO, "Merged the SYSLOG successfully");
				}
				else {
					CAB_DBG(CAB_GW_ERR, "Merging the SYSLOG failed");
				}
#ifdef SOFT_POWER_OFF_EN
                                FILE *fp;
                                time_t tm;
				fp = fopen(SOFT_PWR_OFF_FILE, "w");
				if(fp == NULL) {
					CAB_DBG(CAB_GW_WARNING,"Unable To Open soft power off file");
				}
				time(&tm);
				fprintf(fp, "%d", tm);
				fclose(fp);
#endif
				CAB_DBG(CAB_GW_INFO, "Rebooting...");
				DIAG_DUMP("Rebooting... \r\n");
				system("reboot");
				sleep(60);
			}

			CAB_DBG(CAB_GW_INFO, "OTA processing failed! Reverting back to previous image..");
			DIAG_DUMP("OTA processing failed! Reverting back to previous image..\r\n");
			/* If OTA fail then roll back updated components & remove the stored ota summary file */	
			retCode = rollBackFirmwareComponents(&otaEnv);
			if(GEN_SUCCESS == retCode) {
				CAB_DBG(CAB_GW_INFO, "All components are rolled back successfully");
				DIAG_DUMP("All components are rolled back successfully\r\n");
				remove(OTA_FILE_PATH);
				otaDirectoryCleanup();
				/*Set update_available=0 to avoid repeated roll_back in autoBootScript*/
				setOtaEnvUpdateAvail(0);	
				/*Set full_update=1 to avoid repeated check for incremental OTA too quickly */
				setOtaUpdateType(OTA_TYPE_FULL_UPDATE);	
				retCode = OTA_UPGRADE_FAIL;
			}
			retCode = OTA_RESTART_APP;
		}
	}

	FUNC_EXIT
	return retCode;
}

returnCode_e notifyToCloud(char *currentSwVersion, otaDetails_t *otaDetails)
{
	FUNC_ENTRY
	returnCode_e retCode = GEN_SUCCESS;

	if(!currentSwVersion || !otaDetails) {
		CAB_DBG(CAB_GW_ERR, "Invalid arg");
		DIAG_DUMP( "Invalid arg\n");
		return GEN_NULL_POINTING_ERROR;
	}

	memset(otaDetails, 0, sizeof(otaDetails_t));

	memcpy(otaDetails->otaInfo.version, currentSwVersion, strlen(currentSwVersion));

	retCode = connectToCloud(NOTIFY_GW_VERSION, &otaDetails->otaInfo);
	return retCode;
}
