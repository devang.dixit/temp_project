#include "watchdog.h"

static watchDogTimer_t *watchDogTimerHead = NULL;
static pthread_mutex_t gWatchDogTimerMutex = PTHREAD_MUTEX_INITIALIZER;

int threadRegister(watchDogTimer_t *tempNode)
{
        pthread_mutex_lock(&gWatchDogTimerMutex);

        watchDogTimer_t *temp = watchDogTimerHead;

        if (watchDogTimerHead == NULL)
        {
                watchDogTimerHead = malloc(sizeof(watchDogTimer_t));
                if (watchDogTimerHead == NULL)
                {
                        CAB_DBG(CAB_GW_ERR, "Watch-dog Timer head memory allocation fail.");
			pthread_mutex_unlock(&gWatchDogTimerMutex);
                        return GEN_MALLOC_ERROR;
                }
                memset(watchDogTimerHead, '\0', sizeof(watchDogTimer_t));
                watchDogTimerHead->threadId = tempNode->threadId;
		watchDogTimerHead->CallbackFn = tempNode->CallbackFn;
                watchDogTimerHead->next = NULL;
        }
        else
        {
                while(temp->next != NULL)
                {
                        temp = temp->next;
                }

                temp->next = malloc(sizeof(watchDogTimer_t));
                if (temp->next == NULL)
                {
                        CAB_DBG(CAB_GW_ERR, "Watch-dog Timer node memory allocation fail.");
                        pthread_mutex_unlock(&gWatchDogTimerMutex);
                        return GEN_MALLOC_ERROR;
                }
		memset(temp->next, 0, sizeof(watchDogTimer_t));
                temp->next->threadId = tempNode->threadId;
		temp->next->CallbackFn = tempNode->CallbackFn;
                temp->next->next = NULL;
        }

        pthread_mutex_unlock(&gWatchDogTimerMutex);

        CAB_DBG(CAB_GW_DEBUG, "threadRegister : %ld", tempNode->threadId);
        return GEN_SUCCESS;
}

int threadDeregister(pthread_t thId)
{
        pthread_mutex_lock(&gWatchDogTimerMutex);

        watchDogTimer_t *presentNode = watchDogTimerHead;
        watchDogTimer_t *pastNode = watchDogTimerHead;

        if (watchDogTimerHead == NULL)
        {
                CAB_DBG(CAB_GW_ERR, "Watch-dog Timer deregister error.");
                pthread_mutex_unlock(&gWatchDogTimerMutex);
                return GEN_NULL_POINTING_ERROR;
        }

        if (watchDogTimerHead->threadId == thId)
        {
                watchDogTimerHead = watchDogTimerHead->next;
                free(presentNode);
                presentNode = NULL;
        }
        else
        {
                while((presentNode != NULL) && (presentNode->threadId != thId))
                {
                        pastNode = presentNode;
                        presentNode = presentNode->next;
                }

		if (presentNode == NULL)
                {
                        CAB_DBG(CAB_GW_ERR, "Node not found.");
			pthread_mutex_unlock(&gWatchDogTimerMutex);
                        return GEN_NULL_POINTING_ERROR;
                }
                else if (presentNode->threadId == thId)
                {
                        pastNode->next = presentNode->next;
                        free(presentNode);
			presentNode = NULL;
                }
        }
        pthread_mutex_unlock(&gWatchDogTimerMutex);
        CAB_DBG(CAB_GW_DEBUG, "threadDeregister: %ld", thId);
        return GEN_SUCCESS;
}

watchDogTimer_t * getWatchDogTimerHead(void)
{
	watchDogTimer_t *watchDogTimerHeadLocal;
	pthread_mutex_lock(&gWatchDogTimerMutex);
	watchDogTimerHeadLocal =  watchDogTimerHead;
	pthread_mutex_unlock(&gWatchDogTimerMutex);
	return watchDogTimerHeadLocal;
}
