/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file ZIP incremental package update utility
 * @brief (This file contains the main application
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Jan/09/2019, VT , First Draft
***************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include <stdbool.h>
#include <limits.h>  // for INT_MAX
#include <stdlib.h>  // for strtol
#include "error.h"
#include "debug.h"
#include "gpio.h"
#include "batModule.h"
#include "commonData.h"
#include "configuration.h"
#include "cJSON.h"
#include "otaEnv.h"
#include "hwVersion.h"

#define CHECKLIST_MAX_LEN	(1024)
#define INCR_OTA_DIR_PATH	"/incr_ota_package/"
#define TMP_PATH		"/tmp/log"
#define CHECKLIST_FILE_NAME	"sw-description"
#define SW_LIST_SIG_NAME	"sw-description.sig"
#define BLE_APP_NAME		"ble_app.zip"
#define BLE_SD_NAME		"softdevice.zip"
#define CAB_APP_NAME		"cabAppA7"
#define PARENT_APP_NAME		"cabAppLauncher"
#define ZBEE_FILE_NAME		"JN5169_module_firmware_1_5_0.bin"
#define CAN_FW_NAME		"canfw"
#define RF_FW_NAME		"cc1310.out"
#define EBYTE_FW_NAME   "cabGateway.bin"
#define REMOTEDIAG_APP_NAME "remoteDiagApp"
#define LTE_APP_NAME        "lteApp"



//static pthread_mutex_t g_checklist_lock = PTHREAD_MUTEX_INITIALIZER;
static bool capabilityFlag = false;

typedef struct otaChecklist{
    /* @TODO: Reomve zigbee update support from update. */
    uint8_t zb_update_req;
    char zb_sha[65];
    uint8_t rf_update_req;
    char rf_sha[65];
    uint8_t ble_update_req;
    char bleapp_sha[65];
    char blesd_sha[65];
    uint8_t can_update_req;
    char can_sha[65];
    uint8_t cabApp_update_req;
    char cabApp_sha[65];
    uint8_t parentApp_update_req;
    char parent_sha[65];
    uint8_t remoteDiagApp_update_req;
    char remoteDiagApp_sha[65];
    uint8_t lteApp_update_req;
    char lteApp_sha[65];
    uint8_t ebyte_update_req;
    char ebyte_sha[65];
}otaChecklist_t;

void printZipUpdateUsage(void)
{
	CAB_DBG(CAB_GW_INFO,"\nzipupdate OPTION [KEY_FILE] [OTA FILE NAME] [zb][ble][can][rf][full_update]\n");
	CAB_DBG(CAB_GW_INFO,"\n  -c : Check ZIP bundle authenticity\n");
	CAB_DBG(CAB_GW_INFO,"\n  -p : Perform incremental OTA \n");
	CAB_DBG(CAB_GW_INFO,"\n  -r : Recovery check\n");
	CAB_DBG(CAB_GW_INFO,"\n  -se: set environment for expernal peripheral components\n");
	CAB_DBG(CAB_GW_INFO,"\n  KEY_FILE: Public key file\n");
}

/*
Checllist format:
{"zb":"1","ble:1","can":"1","cabapp":"1","parentapp":"1"}
*/
static readOTAChecklist(char* filename, otaChecklist_t *checklist)
{
    FUNC_ENTRY
    FILE * filePtr;
    int16_t index=0;
    uint8_t status = GEN_SUCCESS;
    char fileContent[CHECKLIST_MAX_LEN] = "";
    cJSON *value;
    cJSON *root = NULL;
    uint8_t bleApp_avail = 0;
    uint8_t bleSd_avail = 0;


    /* Read checklist file */
    filePtr = fopen(filename, "r");
    if(filePtr == NULL) {
        CAB_DBG(CAB_GW_ERR, "%s file open failed.", filename);
        return FILE_OPEN_FAIL;
    }

    while (status = fgets( ((char *)&fileContent+strlen(fileContent) ), CHECKLIST_MAX_LEN, filePtr) != NULL ) {
    }
    
    for (index = 0; index < CHECKLIST_MAX_LEN; index++) {
        if (fileContent[index] == '}'){
            fileContent[index + 1] = '\0';
        }
    }
    fclose(filePtr);

    CAB_DBG(CAB_GW_INFO,"Checklist file content:[%s]",&fileContent[0]);

    /*Default checklist for all components = 0*/
    checklist->zb_update_req = 0;
    checklist->ble_update_req = 0;
    checklist->can_update_req = 0;
    checklist->rf_update_req = 0;
    checklist->cabApp_update_req = 0;
    checklist->parentApp_update_req = 0;
    checklist->remoteDiagApp_update_req = 0;
    checklist->lteApp_update_req = 0;
    checklist->ebyte_update_req = 0;


    root = cJSON_Parse(fileContent);
    if (!root) {
        CAB_DBG(CAB_GW_ERR, "Can't Parse cJSON");
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(root, "zb");
    if (value && value->valuestring) {
        checklist->zb_update_req = 1;
        strcpy(checklist->zb_sha, value->valuestring);
    }

    value = cJSON_GetObjectItem(root, "bleapp");
    if (value && value->valuestring) {
        bleApp_avail = 1;
        strcpy(checklist->bleapp_sha, value->valuestring);
    }

    value = cJSON_GetObjectItem(root, "blesd");
    if (value && value->valuestring) {
        bleSd_avail = 1;
        strcpy(checklist->blesd_sha, value->valuestring);
    }

    if (bleSd_avail == 1 && bleApp_avail == 1) {
        checklist->ble_update_req = 1;
    } else if (!(bleSd_avail == 0 && bleApp_avail == 0)) {
        /*Return if either app fw or sd fw for ble is not present*/
        CAB_DBG(CAB_GW_ERR, "BLE APP and SW FW should be present together");
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(root, "can");
    if (value && value->valuestring) {
        checklist->can_update_req = 1;
        strcpy(checklist->can_sha, value->valuestring);
    }
   
    value = cJSON_GetObjectItem(root, "rf");
    if (value && value->valuestring) {
        checklist->rf_update_req = 1;
        strcpy(checklist->rf_sha, value->valuestring);
    }

    value = cJSON_GetObjectItem(root, "ebyte");
    if (value && value->valuestring) {
        checklist->ebyte_update_req = 1;
        strcpy(checklist->ebyte_sha, value->valuestring);
    }

    value = cJSON_GetObjectItem(root, "cabapp");
    if (value && value->valuestring) {
        checklist->cabApp_update_req = 1;
        strcpy(checklist->cabApp_sha, value->valuestring);
    }

    value = cJSON_GetObjectItem(root, "parentapp");
    if (value && value->valuestring) {
        checklist->parentApp_update_req = 1;
        strcpy(checklist->parent_sha, value->valuestring);
    }

    value = cJSON_GetObjectItem(root, "remotediagapp");
    if (value && value->valuestring) {
        checklist->remoteDiagApp_update_req = 1;
        strcpy(checklist->remoteDiagApp_sha, value->valuestring);
    }

    value = cJSON_GetObjectItem(root, "lteapp");
    if (value && value->valuestring) {
        checklist->lteApp_update_req = 1;
        strcpy(checklist->lteApp_sha, value->valuestring);
    }
    cJSON_Delete(root);

    CAB_DBG(CAB_GW_INFO,"***** Checklist Info *****");
    CAB_DBG(CAB_GW_INFO,"*****zb_update_req = %d",checklist->zb_update_req);
    CAB_DBG(CAB_GW_INFO,"*****zb_sha = %s",checklist->zb_sha);
    CAB_DBG(CAB_GW_INFO,"*****ble_update_req = %d",checklist->ble_update_req);
    CAB_DBG(CAB_GW_INFO,"*****bleapp_sha = %s",checklist->bleapp_sha);
    CAB_DBG(CAB_GW_INFO,"*****blesd_sha = %s",checklist->blesd_sha);
    CAB_DBG(CAB_GW_INFO,"*****can_update_req = %d",checklist->can_update_req);
    CAB_DBG(CAB_GW_INFO,"*****can_sha = %s",checklist->can_sha);
    CAB_DBG(CAB_GW_INFO,"*****rf_update_req = %d",checklist->rf_update_req);
    CAB_DBG(CAB_GW_INFO,"*****rf_sha = %s",checklist->rf_sha);
    CAB_DBG(CAB_GW_INFO,"*****ebyte_update_req = %d",checklist->ebyte_update_req);
    CAB_DBG(CAB_GW_INFO,"*****ebyte_sha = %s",checklist->ebyte_sha);
    CAB_DBG(CAB_GW_INFO,"*****cabApp_update_req = %d",checklist->cabApp_update_req);
    CAB_DBG(CAB_GW_INFO,"*****cabApp_sha = %s",checklist->cabApp_sha);
    CAB_DBG(CAB_GW_INFO,"*****parentApp_update_req = %d",checklist->parentApp_update_req);
    CAB_DBG(CAB_GW_INFO,"*****parentapp_sha = %s",checklist->parent_sha);
    CAB_DBG(CAB_GW_INFO,"*****remoteDiagApp_update_req = %d",checklist->remoteDiagApp_update_req);
    CAB_DBG(CAB_GW_INFO,"*****remoteDiagApp_sha = %s",checklist->remoteDiagApp_sha);
    CAB_DBG(CAB_GW_INFO,"*****lteApp_update_req = %d",checklist->lteApp_update_req);
    CAB_DBG(CAB_GW_INFO,"*****lteApp_sha = %s",checklist->lteApp_sha);


    FUNC_EXIT
    return GEN_SUCCESS;
}


/**
	* @brief This function verifies cmd output is valid or not.
 */
static bool execCmdCheckOutputPattern(char *cmd, int noOfExpectedLines, char *expectedPattern)
{
   FUNC_ENTRY
   bool status;
   int successCount = 0;
   char cmdLocal [255] = "";
   FILE *fp =NULL;

   system("rm -rf "TMP_PATH);
   memset(cmdLocal, 0, sizeof(cmdLocal));
   sprintf(cmdLocal, "%s 1>>"TMP_PATH" 2>&1", cmd);
   CAB_DBG(CAB_GW_INFO,"1 cmd --%s--\r\n", cmdLocal);
   system(cmdLocal);
   memset(cmdLocal, 0, sizeof(cmdLocal));
   sprintf(cmdLocal, "cat "TMP_PATH" | wc -l");
   CAB_DBG(CAB_GW_INFO,"2 cmd --%s--\r\n", cmdLocal);
   fp = popen(cmdLocal, "r");
   successCount = 0;
   fscanf(fp, "%d", &successCount);
   CAB_DBG(CAB_GW_INFO,"--%d--\r\n", successCount);
   pclose(fp);

   if (successCount == noOfExpectedLines) {
           status = true;
   } else {
           status = false;
   }

   if (status == true)  {
   	memset(cmdLocal, 0, sizeof(cmdLocal));
   	sprintf(cmdLocal, "cat "TMP_PATH" | grep -c \"%s\"", expectedPattern);
   	CAB_DBG(CAB_GW_INFO,"3 cmd --%s--\r\n", cmdLocal);
   	fp = popen(cmdLocal, "r");
   	successCount = 0;
   	fscanf(fp, "%d", &successCount);
   	CAB_DBG(CAB_GW_INFO,"--%d--\r\n", successCount);
   	pclose(fp);
   	if ( successCount >= 1 ) {
   		status = true;
   	} else {
   		status = false;
   	}
   }

 
   FUNC_EXIT
   return status;
}

returnCode_e checkZIPAuthenticity(char* key, char* otaFilePath)
{
    FUNC_ENTRY
    char cmd[200];
    otaEnv_t otaEnv;
    returnCode_e retCode = GEN_SUCCESS;
    otaChecklist_t otaCl;

    CAB_DBG(CAB_GW_INFO,"\nKey file name: %s\n",key);

    if (key == NULL || otaFilePath == NULL) {
	CAB_DBG(CAB_GW_ERR, "OTA check auth. failed as invalid args.");
	return GEN_INVALID_ARG;
    }
	
    if (access(otaFilePath, F_OK) != 0) {
	CAB_DBG(CAB_GW_ERR, "OTA check auth. failed as OTA file is not accessible");
	return GEN_INVALID_ARG;
    }
    /*Reset OTA env status as new firmware update request has been received*/
    /*Read Environment*/
    retCode = readOTAEnv(&otaEnv);
    if(retCode != GEN_SUCCESS){
	CAB_DBG(CAB_GW_ERR,"\nError reading OTA environment(%d)\n",retCode);
	return DATA_INVALID;
    }
    /*Reinitialize OTA environment so if verification failed, host app can skip
      recovery process*/
    otaEnv.zb_updated = 0;
    otaEnv.ble_updated = 0;
    otaEnv.can_updated = 0;
    otaEnv.rf_updated = 0;
    otaEnv.cabApp_updated = 0;
    otaEnv.parentApp_updated = 0;
    otaEnv.update_available = 0;
    otaEnv.full_update = 0;
    otaEnv.boot_count = 0;
    otaEnv.remoteDiagApp_updated = 0;
    otaEnv.lteApp_updated = 0;
    otaEnv.ebyte_updated = 0;

    /*Read Environment*/
    retCode = writeOTAEnv(&otaEnv);
    if(retCode != GEN_SUCCESS){
	CAB_DBG(CAB_GW_ERR,"\nError updating OTA environment(%d)\n",retCode);
	return DATA_INVALID;
    }
    /*Create a directory to exract OTA package & cleaar pervious data*/
    memset(cmd, 0, sizeof(cmd));
    sprintf(cmd, "mkdir -p %s", INCR_OTA_DIR_PATH);
    system(cmd);

    memset(cmd, 0, sizeof(cmd));
    sprintf(cmd, "rm -rf %s/*", INCR_OTA_DIR_PATH);
    system(cmd);

    CAB_DBG(CAB_GW_INFO,"\nKey file: %s\n",key);
    
    /* Extract OTA package in OTA_PACKAGE directory */
    memset(cmd, 0, sizeof(cmd));
    sprintf(cmd, "cd %s && cpio -i < %s ", INCR_OTA_DIR_PATH, otaFilePath);
    if (execCmdCheckOutputPattern(cmd, 1, "blocks") != true) {
    	CAB_DBG(CAB_GW_ERR, "OTA Package extraction failed");
    	FUNC_EXIT
    	return DATA_INVALID;
    }
    
    /* Verify signature of check list */
    memset(cmd, 0, sizeof(cmd));
    sprintf(cmd, "openssl dgst -sha256 -verify %s -signature %s/%s %s/%s", key, INCR_OTA_DIR_PATH, SW_LIST_SIG_NAME, INCR_OTA_DIR_PATH, CHECKLIST_FILE_NAME);
    if (execCmdCheckOutputPattern(cmd, 1, "Verified OK") != true) {
    	CAB_DBG(CAB_GW_ERR, "OTA Package signature authentication failed");
    	FUNC_EXIT
    	return DATA_INVALID;
    }
    
    /* Read check list file */
    memset(cmd, 0, sizeof(cmd));
    sprintf(cmd, "%s/%s", INCR_OTA_DIR_PATH, CHECKLIST_FILE_NAME);
    retCode = readOTAChecklist(cmd, &otaCl);
    if (retCode != GEN_SUCCESS) {
    	CAB_DBG(CAB_GW_ERR, "Read OTA checklist failed or invalid checklist");
    	return DATA_INVALID;	
    }
    
    if(otaCl.zb_update_req == 1) {
    	/*zigbee files SHA check*/
    	memset(cmd, 0, sizeof(cmd));
    	sprintf(cmd, "sha256sum %s/%s ", INCR_OTA_DIR_PATH, ZBEE_FILE_NAME);
    	if (execCmdCheckOutputPattern(cmd, 1, otaCl.zb_sha) != true) {
    		CAB_DBG(CAB_GW_ERR, "ZBEE FW SHA verification failed");
    		FUNC_EXIT
    		return DATA_INVALID;
    	}
    }
    
    if(otaCl.ble_update_req == 1) {
    	/* Verify SHA of BLE APP FW */
    	memset(cmd, 0, sizeof(cmd));
    	sprintf(cmd, "sha256sum %s/%s ", INCR_OTA_DIR_PATH, BLE_APP_NAME);
    	if (execCmdCheckOutputPattern(cmd, 1, otaCl.bleapp_sha) != true) {
    		CAB_DBG(CAB_GW_ERR, "BLE APP SHA verification failed");
    		FUNC_EXIT
    		return DATA_INVALID;
    	}
    
    	/* Verify SHA of BLE SD FW */
    	memset(cmd, 0, sizeof(cmd));
    	sprintf(cmd, "sha256sum %s/%s ", INCR_OTA_DIR_PATH, BLE_SD_NAME);
    	if (execCmdCheckOutputPattern(cmd, 1, otaCl.blesd_sha) != true) {
    		CAB_DBG(CAB_GW_ERR, "BLE SD SHA verification failed");
    		FUNC_EXIT
    		return DATA_INVALID;
    	}
    }
    
    if(otaCl.can_update_req == 1) {
    	/*CAN files SHA check*/
    	memset(cmd, 0, sizeof(cmd));
    	sprintf(cmd, "sha256sum %s/%s ", INCR_OTA_DIR_PATH, CAN_FW_NAME);
    	if (execCmdCheckOutputPattern(cmd, 1, otaCl.can_sha) != true) {
    		CAB_DBG(CAB_GW_ERR, "CAN FW SHA verification failed");
    		FUNC_EXIT
    		return DATA_INVALID;
    	}
    }
    
    if(otaCl.rf_update_req == 1) {
    	/*RF FW SHA check*/
    	memset(cmd, 0, sizeof(cmd));
    	sprintf(cmd, "sha256sum %s/%s ", INCR_OTA_DIR_PATH, RF_FW_NAME);
    	if (execCmdCheckOutputPattern(cmd, 1, otaCl.rf_sha) != true) {
    		CAB_DBG(CAB_GW_ERR, "RF FW SHA verification failed");
    		FUNC_EXIT
    		return DATA_INVALID;
    	}
    }

    if(otaCl.ebyte_update_req == 1) {
        /*EBYTE FW file SHA check*/
        memset(cmd, 0, sizeof(cmd));
        sprintf(cmd, "sha256sum %s/%s ", INCR_OTA_DIR_PATH, EBYTE_FW_NAME);
        if (execCmdCheckOutputPattern(cmd, 1, otaCl.ebyte_sha) != true) {
            CAB_DBG(CAB_GW_ERR, "EBYTE FW SHA verification failed");
            FUNC_EXIT
            return DATA_INVALID;
        }
    }
    
    if(otaCl.cabApp_update_req == 1) {
    	/* Verify SHA of cabAppA7 */
    	memset(cmd, 0, sizeof(cmd));
    	sprintf(cmd, "sha256sum %s/%s ", INCR_OTA_DIR_PATH, CAB_APP_NAME);
    	if (execCmdCheckOutputPattern(cmd, 1, otaCl.cabApp_sha) != true) {
    		CAB_DBG(CAB_GW_ERR, "CAB APP SHA verification failed");
    		FUNC_EXIT
    		return DATA_INVALID;
    	}
    }
    
    if(otaCl.parentApp_update_req == 1) {
    	/* Verify SHA of ParentApp */
    	memset(cmd, 0, sizeof(cmd));
    	sprintf(cmd, "sha256sum %s/%s ", INCR_OTA_DIR_PATH, PARENT_APP_NAME);
    	if (execCmdCheckOutputPattern(cmd, 1, otaCl.parent_sha) != true) {
    		CAB_DBG(CAB_GW_ERR, "PARENT APP SHA verification failed");
    		FUNC_EXIT
    		return DATA_INVALID;
    	}
    }

    if(otaCl.remoteDiagApp_update_req == 1) {
        /* Verify SHA of RemoteDiagApp */
        memset(cmd, 0, sizeof(cmd));
        sprintf(cmd, "sha256sum %s/%s ", INCR_OTA_DIR_PATH, REMOTEDIAG_APP_NAME);
        if (execCmdCheckOutputPattern(cmd, 1, otaCl.remoteDiagApp_sha) != true) {
            CAB_DBG(CAB_GW_ERR, "REMOTE DIAG APP SHA verification failed");
            FUNC_EXIT
            return DATA_INVALID;
        }
    }

    if(otaCl.lteApp_update_req == 1) {
        /* Verify SHA of RemoteDiagApp */
        memset(cmd, 0, sizeof(cmd));
        sprintf(cmd, "sha256sum %s/%s ", INCR_OTA_DIR_PATH, LTE_APP_NAME);
        if (execCmdCheckOutputPattern(cmd, 1, otaCl.lteApp_sha) != true) {
            CAB_DBG(CAB_GW_ERR, "LTE APP SHA verification failed");
            FUNC_EXIT
            return DATA_INVALID;
        }
    }

    /**** Do not change below status message text ****/
    CAB_DBG(CAB_GW_INFO,"\nIncremental OTA verified succeffully\n");
	FUNC_EXIT
	return GEN_SUCCESS;


}

static returnCode_e updateEbtyeFirmware()
{
    FUNC_ENTRY
    returnCode_e retCode;
    uint8_t isEbyteFlashed = 1;     /* 1 indicates error */
    uint8_t retryCount = 0;
    bool flag = false;

    if (!capabilityFlag) {
        CAB_DBG(CAB_GW_INFO, "CC1310 OTA capability is not available in this HW version. Skipping the CC1310 FW update");
        FUNC_EXIT
        return GEN_SUCCESS;
    }

    while ((isEbyteFlashed != 0) && (retryCount < BLE_RETRY_COUNT)) {
        CAB_DBG(CAB_GW_INFO, "Recovering Ebyte FW [retrycount: %d]\r\n", retryCount);
        isEbyteFlashed = WEXITSTATUS(system("/cabApp/ebyte/flashEbyteFW.sh"));
        retryCount++;
    }

    if(0 == isEbyteFlashed)
        retCode = GEN_SUCCESS;
    else
        retCode = OTA_UPGRADE_FAIL;

    FUNC_EXIT
    return retCode;
}

static returnCode_e updateBleFirmware()
{
        FUNC_ENTRY
        returnCode_e retCode;
        uint8_t isBleFlashed = 1;       /* initialized by 1 indicates error */
        uint8_t retryCount = 0;

        while( (isBleFlashed != 0) && (retryCount < BLE_RETRY_COUNT) ) {
                CAB_DBG(CAB_GW_INFO, "Recovering BLE softdevice [retrycount: %d]\r\n", retryCount);
                isBleFlashed = WEXITSTATUS(system("/cabApp/ble/flashble-sd.sh MOUNT_PARTITION"));
                retryCount++;
        }

        if(isBleFlashed == 0) {
                isBleFlashed = 1;       /* initialized by default value */
                retryCount = 0;         /* initialized by default value */
                while( (isBleFlashed != 0) && (retryCount < BLE_RETRY_COUNT) ) {
                        CAB_DBG(CAB_GW_INFO, "Recovering BLE applicataion [retrycount: %d]\r\n", retryCount);
                        isBleFlashed = WEXITSTATUS(system("/cabApp/ble/flashble-app.sh MOUNT_PARTITION"));
                        retryCount++;
                }
        }
        if(0 == isBleFlashed)
                retCode = GEN_SUCCESS;
        else
                retCode = OTA_UPGRADE_FAIL;
        FUNC_EXIT
        return retCode;
}

void prepareBackup(void){
	FUNC_ENTRY
	system("mkdir -p /cabApp_bkup"); 
	system("rm -rf /cabApp_bkup/*");
	system("sync");
	system("mkdir -p /cabApp_bkup/zigbee/");
	system("cp /cabApp/zigbee/JN5169_module_firmware_1_5_0.bin /cabApp_bkup/zigbee/; sync"); 
	system("mkdir -p /cabApp_bkup/ble/");
	system("cp /cabApp/ble/ble_app.zip /cabApp_bkup/ble/; sync"); 
    CAB_DBG(CAB_GW_INFO,"\nBLE App backup successful\n");
	system("cp /cabApp/ble/softdevice.zip /cabApp_bkup/ble/; sync");
    CAB_DBG(CAB_GW_INFO,"\nBLE soft device backup successful\n");

    if (capabilityFlag) {
        system("mkdir -p /cabApp_bkup/ebyte/");
        system("cp /cabApp/ebyte/cabGateway.bin /cabApp_bkup/ebyte/; sync");
        CAB_DBG(CAB_GW_INFO,"\nEBYTE FW backup successful\n");
    }

	system("mkdir -p /cabApp_bkup/can/");
	system("cp -r /cabApp/can/* /cabApp_bkup/can/; sync"); 
	system("cp /cabApp/cabAppA7 /cabApp_bkup/; sync"); 
    CAB_DBG(CAB_GW_INFO,"\ncabAppA7 backup successful\n");
	system("cp /cabApp/cabAppLauncher /cabApp_bkup/; sync"); 
    CAB_DBG(CAB_GW_INFO,"\ncabAppLauncher backup successful\n");
    system("cp /cabApp/remoteDiagApp /cabApp_bkup/; sync"); 
    CAB_DBG(CAB_GW_INFO,"\nremoteDiagApp backup successful\n");
    system("cp /cabApp/lteApp /cabApp_bkup/; sync"); 
    CAB_DBG(CAB_GW_INFO,"\nlteApp backup successful\n");
	system("cp /cabApp/cc1310.out /cabApp_bkup/; sync"); 
    CAB_DBG(CAB_GW_INFO,"\ncc1310.out backup successful\n");
	system("cp /cabApp/softwareVersion.txt /cabApp_bkup/; sync"); 
	CAB_DBG(CAB_GW_INFO,"\nCurrent firmware backup successful\n");

	FUNC_EXIT
}

returnCode_e restoreBackup(void){
	FUNC_ENTRY
	if(access("/cabApp_bkup/zigbee/JN5169_module_firmware_1_5_0.bin", F_OK) ||
	   access("/cabApp_bkup/ble/ble_app.zip",F_OK) ||
	   access("/cabApp_bkup/ble/softdevice.zip",F_OK) ||
	   access("/cabApp_bkup/can/canfw",F_OK) ||
	   access("/cabApp_bkup/cc1310.out",F_OK) ||
	   access("/cabApp_bkup/cabAppA7",F_OK) ||
	   access("/cabApp_bkup/cabAppLauncher",F_OK) ) {
		CAB_DBG(CAB_GW_INFO,"\n Required firmware components are not available in backup\n");
		return FILE_NOT_FOUND;
	}
	system("cp /cabApp_bkup/zigbee/JN5169_module_firmware_1_5_0.bin /cabApp/zigbee/; sync");
	system("cp /cabApp_bkup/ble/ble_app.zip /cabApp/ble/; sync"); 
	system("cp /cabApp_bkup/ble/softdevice.zip /cabApp/ble/; sync");
	system("cp -r /cabApp_bkup/can/* /cabApp/can/; sync"); 
	system("cp /cabApp_bkup/cc1310.out /cabApp/; sync"); 
	system("cp /cabApp_bkup/cabAppA7 /cabApp/; sync"); 
	system("cp /cabApp_bkup/cabAppLauncher /cabApp/; sync"); 
    system("cp /cabApp_bkup/remoteDiagApp /cabApp/; sync"); 
    system("cp /cabApp_bkup/lteApp /cabApp/; sync"); 
	system("cp /cabApp_bkup/softwareVersion.txt /cabApp/; sync"); 

    if (capabilityFlag) {
        if (access("/cabApp_bkup/ebyte/cabGateway.bin",F_OK)) {
            CAB_DBG(CAB_GW_INFO,"\n Required firmware components are not available in backup\n");
            return FILE_NOT_FOUND;
        }
        system("cp /cabApp_bkup/ebyte/cabGateway.bin /cabApp/ebyte/; sync"); 
    }
    
	CAB_DBG(CAB_GW_INFO,"\nBackup firmware restored successful\n");
	return GEN_SUCCESS;
	FUNC_EXIT
}

returnCode_e upgradeWithNewFirmware(otaEnv_t *otaEnv)
{
	FUNC_ENTRY
	if(otaEnv->zb_updated){
		if(access("/incr_ota_package/JN5169_module_firmware_1_5_0.bin", F_OK) == 0){
			system("cp /incr_ota_package/JN5169_module_firmware_1_5_0.bin /cabApp/zigbee/; sync");
		}
		else{
			CAB_DBG(CAB_GW_INFO,"\nZigBee firmware file not available in OTA package\n");
			return FILE_NOT_FOUND;
		}
	}
	if(otaEnv->ble_updated){
		if((access("/incr_ota_package/ble_app.zip", F_OK) == 0) && 
		(access("/incr_ota_package/softdevice.zip", F_OK) == 0)){
			system("cp /incr_ota_package/ble_app.zip /cabApp/ble/; sync");
			system("cp /incr_ota_package/softdevice.zip /cabApp/ble/; sync");
		}
		else{
			CAB_DBG(CAB_GW_INFO,"\nBLE firmware file not available in OTA package\n");
			return FILE_NOT_FOUND;
		}
	}
    if(otaEnv->ebyte_updated){
        if(access("/incr_ota_package/cabGateway.bin", F_OK) == 0) {
            system("cp /incr_ota_package/cabGateway.bin /cabApp/ebyte/; sync");
        }
        else {
            CAB_DBG(CAB_GW_INFO, "\nEbyte firmware file not available in OTA package\n");
            return FILE_NOT_FOUND;
        }
    }
	if(otaEnv->can_updated){
		if(access("/incr_ota_package/canfw", F_OK) == 0){
			system("cp /incr_ota_package/canfw /cabApp/can/; sync");
		}
		else{
			CAB_DBG(CAB_GW_INFO,"\nCAN firmware file not available in OTA package\n");
			return FILE_NOT_FOUND;
		}
	}
	if(otaEnv->rf_updated){
		if(access("/incr_ota_package/cc1310.out", F_OK) == 0){
			system("cp /incr_ota_package/cc1310.out /cabApp/; sync");
		}
		else{
			CAB_DBG(CAB_GW_INFO,"\nRF firmware file not available in OTA package\n");
			return FILE_NOT_FOUND;
		}
	}
	if(otaEnv->cabApp_updated){
		if(access("/incr_ota_package/cabAppA7", F_OK) == 0){
			system("cp /incr_ota_package/cabAppA7 /cabApp/; sync");
		}
		else{
			CAB_DBG(CAB_GW_INFO,"\nCabAppA7 firmware file not available in OTA package\n");
			return FILE_NOT_FOUND;
		}
	}
	if(otaEnv->parentApp_updated){
		if(access("/incr_ota_package/cabAppLauncher", F_OK) == 0){
			system("cp /incr_ota_package/cabAppLauncher /cabApp/; sync");
		}
		else{
			CAB_DBG(CAB_GW_INFO,"\ncabAppLauncher file not available in OTA package\n");
			return FILE_NOT_FOUND;
		}
	}
    if(otaEnv->remoteDiagApp_updated){
        if(access("/incr_ota_package/remoteDiagApp", F_OK) == 0){
            system("cp /incr_ota_package/remoteDiagApp /cabApp/; sync");
        }
        else{
            CAB_DBG(CAB_GW_INFO,"\nremoteDiagApp file not available in OTA package\n");
            return FILE_NOT_FOUND;
        }
    }
    if(otaEnv->lteApp_updated){
        if(access("/incr_ota_package/lteApp", F_OK) == 0){
            system("cp /incr_ota_package/lteApp /cabApp/; sync");
        }
        else{
            CAB_DBG(CAB_GW_INFO,"\nlteApp file not available in OTA package\n");
            return FILE_NOT_FOUND;
        }
    }
	if(otaEnv->zb_updated || otaEnv->ble_updated || otaEnv->can_updated || otaEnv->rf_updated || \
	   otaEnv->cabApp_updated || otaEnv->parentApp_updated || otaEnv->remoteDiagApp_updated ||
       otaEnv->lteApp_updated || otaEnv->ebyte_updated){
		system("cp /incr_ota_package/softwareVersion.txt /cabApp/; sync");

	}
	FUNC_EXIT
}
returnCode_e performIncrementalOTA()
{
    FUNC_ENTRY
    returnCode_e retCode;
    otaChecklist_t otaCl;
    otaEnv_t otaEnv; 
    char filename[200];
   
    CAB_DBG(CAB_GW_INFO,"performIncrementalOTA: Entered");
    CAB_DBG(CAB_GW_INFO,"performIncrementalOTA: reading OTA Env");

    /*Read Environment*/
    retCode = readOTAEnv(&otaEnv);
    if(retCode != GEN_SUCCESS){
	CAB_DBG(CAB_GW_INFO,"\nError reading OTA environment(%d)\n",retCode);
	return DATA_INVALID;
    }
    printOtaEnv(&otaEnv,0);
    /*Reinitialize OTA environment*/
    otaEnv.zb_updated = 0;
    otaEnv.ble_updated = 0;
    otaEnv.can_updated = 0;
    otaEnv.rf_updated = 0;
    otaEnv.cabApp_updated = 0;
    otaEnv.parentApp_updated = 0;
    otaEnv.update_available = 0;
    otaEnv.remoteDiagApp_updated = 0;
    otaEnv.lteApp_updated = 0;
    otaEnv.ebyte_updated = 0;

    /*Create back-up directory*/
    prepareBackup();
    otaEnv.backup_available = 1;

   /*Update device with new firmware*/

    //read OTA Checklist*/
    CAB_DBG(CAB_GW_INFO,"performIncrementalOTA: reading OTA checklist");
    memset(filename,0,200);
    sprintf(filename,"%s/%s",INCR_OTA_DIR_PATH,CHECKLIST_FILE_NAME);
    readOTAChecklist(filename, &otaCl);

    /*Update ZigBee Firmware*/
    if(otaCl.zb_update_req){
        if(access("/incr_ota_package/JN5169_module_firmware_1_5_0.bin", F_OK) == 0 ){
		CAB_DBG(CAB_GW_INFO,"\nUpdating ZigBee firmware\n");
        	otaEnv.zb_updated = 1;
    		CAB_DBG(CAB_GW_INFO,"\nZigBee firmware updated successfully!\n");
	}
    }
    else
	CAB_DBG(CAB_GW_INFO,"performIncrementalOTA: zb_update not required");

    /*CAN FW update*/    
    if(otaCl.can_update_req){
        if(access("/incr_ota_package/canfw", F_OK) == 0 ){
		CAB_DBG(CAB_GW_INFO,"\nUpdating can firmware\n");
        	otaEnv.can_updated = 1;
    		CAB_DBG(CAB_GW_INFO,"\nCAN firmware updated successfully!\n");
	}
    }
    else
	CAB_DBG(CAB_GW_INFO,"performIncrementalOTA: can_update not required");
    /*RF FW update*/    
    if(otaCl.rf_update_req){
        if(access("/incr_ota_package/cc1310.out", F_OK) == 0 ){
		CAB_DBG(CAB_GW_INFO,"\nUpdating rf firmware\n");
        	otaEnv.rf_updated = 1;
    		CAB_DBG(CAB_GW_INFO,"\nRF firmware updated successfully!\n");
	}
    }
    else
	CAB_DBG(CAB_GW_INFO,"performIncrementalOTA: rf_update not required");

    /*RF FW update*/    
    if(otaCl.ebyte_update_req && capabilityFlag){
        if(access("/incr_ota_package/cabGateway.bin", F_OK) == 0 ){
        CAB_DBG(CAB_GW_INFO,"\nUpdating EBYTE module firmware\n");
            otaEnv.ebyte_updated = 1;
            CAB_DBG(CAB_GW_INFO,"\nUpdating EBYTE module firmware\n");
            /*Copy new image to working partition*/
            memset(filename,0,200);
            sprintf(filename,"cp %s/cabGateway.bin /cabApp/ebyte/; sync",INCR_OTA_DIR_PATH);
            system(filename);

            retCode = updateEbtyeFirmware();
            if (retCode != GEN_SUCCESS) {
                /*Copy backup ebyte image to working partition*/
                system("cp /cabApp_bkup/ebyte/cabGateway.bin /cabApp/ebyte/; sync");
                CAB_DBG(CAB_GW_INFO,"\nEBYTE firmware load failed\n");
            goto fail_exit;
            }
            CAB_DBG(CAB_GW_INFO,"\nEBYTE firmware updated successfully!\n");
    }
    }
    else
    CAB_DBG(CAB_GW_INFO,"performIncrementalOTA: ebyte_update not required");

    /*Update BLE Firmware*/
    if(otaCl.ble_update_req){
        if((access("/incr_ota_package/ble_app.zip", F_OK) == 0 )&& (access("/incr_ota_package/softdevice.zip", F_OK) == 0)){
        	otaEnv.ble_updated = 1;
		CAB_DBG(CAB_GW_INFO,"\nUpdating BLE module firmware\n");
		/*Copy new image to working partition*/
    		memset(filename,0,200);
		sprintf(filename,"cp %s/ble_app.zip /cabApp/ble/; sync",INCR_OTA_DIR_PATH);
        	system(filename);
    		memset(filename,0,200);
		sprintf(filename,"cp %s/softdevice.zip /cabApp/ble/; sync",INCR_OTA_DIR_PATH);
        	system(filename);
		retCode = updateBleFirmware();
		if(retCode != GEN_SUCCESS){
			/*Copy backup ble image to working partition*/
			system("cp /cabApp_bkup/ble/ble_app.zip /cabApp/ble/; sync");
	                system("cp /cabApp_bkup/ble/softdevice.zip /cabApp/ble/; sync");
			CAB_DBG(CAB_GW_INFO,"\nBle firmware load failed\n");
			goto fail_exit;
		}
    		CAB_DBG(CAB_GW_INFO,"\nBLE Firmware Updated Successfully!\n");
	}
    }
    else
	CAB_DBG(CAB_GW_INFO,"performIncrementalOTA: ble_update not required");

    /*Update CabApp Firmware*/
    if(otaCl.cabApp_update_req){
        if(access("/incr_ota_package/cabAppA7", F_OK) == 0 ){
		CAB_DBG(CAB_GW_INFO,"\nUpdating cabgateway application\n");
        	otaEnv.cabApp_updated = 1;
    		CAB_DBG(CAB_GW_INFO,"\nCabApp Updated Successfully!\n");
	}
    }
    else
	CAB_DBG(CAB_GW_INFO,"performIncrementalOTA: cabApp_update not required");
    
    /*Update ParentApp Firmware*/
    if(otaCl.parentApp_update_req){
        if(access("/incr_ota_package/cabAppLauncher", F_OK) == 0 ){
		CAB_DBG(CAB_GW_INFO,"\nUpdating cabapp launcher application\n");
        	otaEnv.parentApp_updated = 1;
    		CAB_DBG(CAB_GW_INFO,"\nParentApp Updated Successfully!\n");
	}
    }
    else
	CAB_DBG(CAB_GW_INFO,"performIncrementalOTA: parentApp_update not required");

    /*Update remoteDiagApp Firmware*/
    if(otaCl.remoteDiagApp_update_req){
        if(access("/incr_ota_package/remoteDiagApp", F_OK) == 0 ){
        CAB_DBG(CAB_GW_INFO,"\nUpdating Remote Diag app application\n");
            otaEnv.remoteDiagApp_updated = 1;
            CAB_DBG(CAB_GW_INFO,"\nremoteDiagApp Updated Successfully!\n");
    }
    }
    else
    CAB_DBG(CAB_GW_INFO,"performIncrementalOTA: remoteDiagApp_update not required");

    /*Update lteApp Firmware*/
    if(otaCl.lteApp_update_req){
        if(access("/incr_ota_package/lteApp", F_OK) == 0 ){
        CAB_DBG(CAB_GW_INFO,"\nUpdating LTE app application\n");
            otaEnv.lteApp_updated = 1;
            CAB_DBG(CAB_GW_INFO,"\nlteApp Updated Successfully!\n");
    }
    }
    else
    CAB_DBG(CAB_GW_INFO,"performIncrementalOTA: lteApp_update not required");

    otaEnv.update_available = 1;
    otaEnv.boot_count = 0;

    CAB_DBG(CAB_GW_INFO,"\nIncremental OTA performed successfully\n");
fail_exit:
    CAB_DBG(CAB_GW_INFO,"\nUpdating OTA Environment\n");
    retCode = writeOTAEnv(&otaEnv);
    if(retCode != GEN_SUCCESS){
	CAB_DBG(CAB_GW_INFO,"\nError updating OTA environment(%d)\n",retCode);
	FUNC_EXIT
	return DATA_INVALID;
    }
    CAB_DBG(CAB_GW_INFO,"\nCGUPDATE successful !\n");
    printOtaEnv(&otaEnv,1);
    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e checkZIPRecovery(void)
{
    FUNC_ENTRY
    otaEnv_t otaEnv;
    returnCode_e retCode = GEN_SUCCESS;
    FILE * filePtr;
    char* status = GEN_SUCCESS;
    char softwareversion[5+MAX_LEN_SOFTWARE_VERSION] = {0};
    char vfilename[20+MAX_LEN_SOFTWARE_VERSION] = {0};
    int len;

    CAB_DBG(CAB_GW_INFO,"checkZIPRecovery: Entered");

    /*Read Environment*/
    retCode = readOTAEnv(&otaEnv);
    if(retCode != GEN_SUCCESS){
        CAB_DBG(CAB_GW_INFO,"\nError reading OTA environment(%d)\n",retCode);
        return DATA_INVALID;
    }
    printOtaEnv(&otaEnv,0);
    /*No action needed for full OTA*/
    if(!otaEnv.full_update){
    	CAB_DBG(CAB_GW_INFO,"checkZIPRecovery: Last update was incremental. Checking of recovery needed..");


	//Increment boot_count on every boot for parentapp recovery
	otaEnv.boot_count++;
	if((otaEnv.boot_count >= RECOVERY_COUNT_LIMIMT) && (otaEnv.backup_available)){

		filePtr = fopen(FILE_PATH_SOFTWARE_VERSION, "r");
		if(filePtr == NULL) {
			CAB_DBG(CAB_GW_ERR, "Opening software version file failed");
		}

		status = fgets(softwareversion, MAX_LEN_SOFTWARE_VERSION+4, filePtr);
		if(status == NULL) {
			CAB_DBG(CAB_GW_ERR, "%s Reading software version file failed", FILE_PATH_SOFTWARE_VERSION);
			fclose(filePtr);
		}
		//close file
		fclose(filePtr);

		len = strlen(softwareversion);
        if(softwareversion[len - 1] == '\n') {
		    softwareversion[len - 1] = 0;
        }
		sprintf(vfilename, "/cabApp/v%s", softwareversion);
		/* Check if the file representing the current version is present or not,
		   Indicating that CabAppA7 has booted once,
		   if present return without incrementing the boot count.*/
		if(access((char const*)vfilename, F_OK) == 0)
		{
			CAB_DBG(CAB_GW_INFO, "Returning without Incrementing the boot count");
		} else {
			CAB_DBG(CAB_GW_INFO,"checkZIPRecovery: Backup available (bc=%d). Recovery needed",otaEnv.boot_count);
			//Recover updated components
			CAB_DBG(CAB_GW_INFO,"checkZIPRecovery: Restoring backup...");
			retCode = restoreBackup();
			if(retCode != GEN_SUCCESS){
				CAB_DBG(CAB_GW_INFO,"\nError restoring backup firmware\n");
				return retCode; 
			}
			if(otaEnv.ble_updated){
				CAB_DBG(CAB_GW_INFO,"Recovering BLE firmware ...");
				retCode = updateBleFirmware();
				if(retCode != GEN_SUCCESS){
					CAB_DBG(CAB_GW_INFO,"\nError restoring backup BLE firmware\n");
					return retCode; 
				}
			}
			else {
				CAB_DBG(CAB_GW_INFO,"BLE firmware recovery is not required as it was not updated");
			}

			if(otaEnv.ebyte_updated){
				CAB_DBG(CAB_GW_INFO,"Recovering Ebyte firmware ...");
				retCode = updateEbtyeFirmware();
				if(retCode != GEN_SUCCESS){
					CAB_DBG(CAB_GW_INFO,"\nError restoring backup Ebyte firmware\n");
					return retCode; 
				}
			}
			else {
				CAB_DBG(CAB_GW_INFO,"Ebyte firmware recovery is not required as it was not updated");
			}
		}
		//Set boot_count = 0 if recovery succeed
		otaEnv.boot_count = 0;
	}
	else if((otaEnv.boot_count < RECOVERY_COUNT_LIMIMT) && (otaEnv.update_available)){
		CAB_DBG(CAB_GW_INFO,"checkZIPRecovery: Last update was incremental(bc=%d).Copying new fw..",otaEnv.boot_count);
		//If new update available, copy to working directory
		retCode = upgradeWithNewFirmware(&otaEnv);
		if(retCode != GEN_SUCCESS){
			CAB_DBG(CAB_GW_INFO,"\nError upgrading gateway with new firmware\n");
			return retCode;
		}
		//On success, set update_available=0 to avoid copy on next reboot
		otaEnv.update_available=0;
	}

	CAB_DBG(CAB_GW_INFO,"checkZIPRecovery:updating OTA environment");
	/*Write Environment*/
	retCode = writeOTAEnv(&otaEnv);
	if(retCode != GEN_SUCCESS){
		CAB_DBG(CAB_GW_INFO,"\nError writing OTA environment(%d)\n",retCode);
		FUNC_EXIT
			return DATA_INVALID;
	}
	printOtaEnv(&otaEnv,1);    
    }
    else {
	    CAB_DBG(CAB_GW_INFO,"checkZIPRecovery: Last update was of FULL OTA. No recovery operation needed");
    }
ota_rec_success :
    CAB_DBG(CAB_GW_INFO,"\nIncremental OTA recovery succeed\n");
    FUNC_EXIT
    return retCode;
}

static returnCode_e convToInt(const char* strData, uint8_t *num)
{
	FUNC_ENTRY
	char *p;

	errno = 0;
	long conv = strtol(strData, &p, 10);

	if (errno != 0 || *p != '\0' || conv > INT_MAX) {
		CAB_DBG(CAB_GW_INFO,"\n Invalid environment value (%s)\n",strData);
		FUNC_EXIT
		return DATA_INVALID;
	} else {
		*num = conv;    
	}
	FUNC_EXIT
	return GEN_SUCCESS;
}
returnCode_e updateOTAEnvForExtPeripherals(const char* zb, const char* ble, const char* can, 
			  		const char* rf, const char* ebyte, const char* full_update)
{
	FUNC_ENTRY
	otaEnv_t otaEnv;
	returnCode_e retCode = GEN_SUCCESS;

	/*Read Environment*/
	retCode = readOTAEnv(&otaEnv);
	if(retCode != GEN_SUCCESS){
	    CAB_DBG(CAB_GW_INFO,"\nError reading OTA environment(%d)\n",retCode);
	    return DATA_INVALID;
	}
	/*Convert data to uint8_t and set OTA env*/
	if(GEN_SUCCESS != convToInt(zb, &otaEnv.zb_updated)){
		return DATA_INVALID;
	}
	CAB_DBG(CAB_GW_INFO,"otaEnv.zb_updated = %d",otaEnv.zb_updated);
	if(GEN_SUCCESS != convToInt(ble, &otaEnv.ble_updated)){
		return DATA_INVALID;
	}
	CAB_DBG(CAB_GW_INFO,"otaEnv.ble_updated = %d",otaEnv.ble_updated);
	if(GEN_SUCCESS != convToInt(can, &otaEnv.can_updated)){
		return DATA_INVALID;
	}
	CAB_DBG(CAB_GW_INFO,"otaEnv.can_updated = %d",otaEnv.can_updated);
	if(GEN_SUCCESS != convToInt(rf, &otaEnv.rf_updated)){
		return DATA_INVALID;
	}
	CAB_DBG(CAB_GW_INFO,"otaEnv.rf_updated = %d",otaEnv.rf_updated);
    if(GEN_SUCCESS != convToInt(ebyte, &otaEnv.ebyte_updated)){
        return DATA_INVALID;
    }
    CAB_DBG(CAB_GW_INFO,"otaEnv.ebyte_updated = %d",otaEnv.ebyte_updated);
	if(GEN_SUCCESS != convToInt(full_update, &otaEnv.full_update)){
		return DATA_INVALID;
	}
	CAB_DBG(CAB_GW_INFO,"otaEnv.full_update = %d",otaEnv.full_update);

	/*For full update, cabApp, parentApp, remoteDiagApp and lteApp are always updated*/
	if(otaEnv.full_update){
		CAB_DBG(CAB_GW_INFO,"Updating other env update (otaEnv.full_update = %d)",otaEnv.full_update);
		otaEnv.cabApp_updated = 1;
		otaEnv.parentApp_updated = 1;
        otaEnv.remoteDiagApp_updated = 1;
        otaEnv.lteApp_updated = 1;
		otaEnv.boot_count = 0;
		otaEnv.backup_available = 0;
	}	
	retCode = writeOTAEnv(&otaEnv);
	if(retCode != GEN_SUCCESS){
        	CAB_DBG(CAB_GW_INFO,"\nError updating OTA environment(%d)\n",retCode);
	        FUNC_EXIT
        	return DATA_INVALID;
	}
	FUNC_EXIT
	CAB_DBG(CAB_GW_INFO,"\nOTA environment updated successfully\n");
	return retCode;
}

returnCode_e zipUpdateInit(void)
{
   FUNC_ENTRY
   returnCode_e retCode = GEN_SUCCESS;
   retCode = initOTAEnv();
   if(retCode != GEN_SUCCESS){
	CAB_DBG(CAB_GW_INFO,"\nError initilizing cgupdate module(%d)\n",retCode);
	return retCode;
   }
   FUNC_EXIT
   return GEN_SUCCESS;
}

returnCode_e zipUpateDeinit(void)
{
   FUNC_ENTRY
   deinitOTAEnv();
   FUNC_EXIT
   return GEN_SUCCESS; 
}

int main(int argc, char **argv)
{
    FUNC_ENTRY
    returnCode_e retCode;
	
    /*Argument Validation*/
    if(!(((argc == 2) && (0 == strcmp(argv[1],"-r"))) || 
       ((argc == 4) && (0 == strcmp(argv[1],"-c"))) ||
       ((argc == 2) && (0 == strcmp(argv[1],"-p"))) ||
       ((argc == 7) && (0 == strcmp(argv[1],"-se")))
       )){
	CAB_DBG(CAB_GW_ERR,"Invalid Argument");
	printZipUpdateUsage();
    	FUNC_EXIT
		return MODULE_INIT_FAIL;
    }

    retCode = zipUpdateInit();
    if(retCode != GEN_SUCCESS){
	CAB_DBG(CAB_GW_ERR,"zipUpdate Initialization failed");
	FUNC_EXIT
		return retCode;
    }

    retCode = isCC1310OTAAvailable(&capabilityFlag);
    if (retCode != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "Function isCC1310OTAAvailable() failed with code %d", retCode);
    }
    
    if(0 == strcmp(argv[1],"-c")){
	CAB_DBG(CAB_GW_INFO,"Checking ZIP package authenticity ..");
        retCode = checkZIPAuthenticity(argv[2], argv[3]);
		if (retCode != GEN_SUCCESS) {
			CAB_DBG(CAB_GW_ERR, "ZIP Auth failed");
		}
    }
    else if(0 == strcmp(argv[1],"-p")){
	CAB_DBG(CAB_GW_INFO,"Performing incrementing OTA ..");
        retCode =  performIncrementalOTA();
		if (retCode != GEN_SUCCESS) {
			CAB_DBG(CAB_GW_ERR, "ZIP Auth failed");
		}
    }
    else if(0 == strcmp(argv[1],"-r")){
	CAB_DBG(CAB_GW_INFO,"Checking recovery ..");
        retCode = checkZIPRecovery();
		if (retCode != GEN_SUCCESS) {
			CAB_DBG(CAB_GW_ERR, "ZIP Auth failed");
		}
    }
    else if(0 == strcmp(argv[1],"-se")){
	CAB_DBG(CAB_GW_INFO,"Updating OTA environment ..");
        retCode = updateOTAEnvForExtPeripherals(argv[2],argv[3],argv[4],argv[5],argv[6],argv[7]);
		if (retCode != GEN_SUCCESS) {
			CAB_DBG(CAB_GW_ERR, "Update OTA Env failed");
		}
    }

    else{
	CAB_DBG(CAB_GW_ERR,"Invalid Argument");
    }
    zipUpateDeinit();
    FUNC_EXIT
    return retCode;
}
