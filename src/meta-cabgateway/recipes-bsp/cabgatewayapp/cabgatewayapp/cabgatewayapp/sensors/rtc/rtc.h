/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : rtc.h
 * @brief (This files contains the rtc module information like includes, macros, enums, structures,
 *          global/extern variables, callbacks, and API prototypes.)
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Aug/27/2018, VT , First Draft
 **************************************************************/

/*
    NOTE :
    - The flag which is show status of RTC configured or not is handled in M&T module.
    - The timer which is used to sync RTC time periodically is also handled in M&T module.
*/

#ifndef _RTC_H_
#define _RTC_H_

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <linux/rtc.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/ioctl.h>
#include <stdbool.h>
#include "error.h"

/****************************************
 ************* DEFINES ******************
 ****************************************/
#define DEV_RTC0_PATH       ("/dev/rtc0")
#define YEAR_OFFSET         (2000 - 1900)
#define MONTH_OFFSET        (1)
#define RD_BSF_FLG          0x08
#define RTC_RD_BSF          _IOR('p', 0x15, struct rtc_bsf_timestamp) // Custom IOCTL command to read BSF-Flag

/****************************************
 ************* ENUMS ********************
 ****************************************/
enum power_off_mode{
    NO_OP = 0,
    SOFT_PWR_OFF = 1,
    HARD_PWR_OFF = 2
};

/****************************************
 ************ STRUCTURES ****************
 ****************************************/
typedef struct rtc_bsf_timestamp {
    uint8_t bsf_flag;
    struct rtc_time tm;
}rtc_bsf_timestamp_t;

/****************************************
 ******** GLOBAL VARIABLES **************
 ****************************************/
rtc_bsf_timestamp_t bsf;

/****************************************
 ******** EXTERN VARIABLES **************
 ****************************************/

/****************************************
 ************* CALLBACKS ****************
 ****************************************/

/****************************************
 ********* FUNCTION PROTOTYPES **********
 ****************************************/
/******************************************************************
 *@brief (This API is used to initialize RTC module)
 *
 *@param[IN] void
 *@param[OUT] None
 *
 *@return  returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e initRtc(void);

/******************************************************************
 *@brief (This API is used to Deinitialize RTC module)
 *
 *@param[IN] void
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e deInitRtc(void);

/******************************************************************
 *@brief (This API is used to get current date and time from system)
 *
 *@param[IN] None
 *@param[OUT] time_t *(Indicates pointer to time structure where date and time will be saved in it)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e getTime(time_t *);

/******************************************************************
 *@brief (This API is used to set current date and time of RTC)
 *
 *@param[IN] time_t (Indicates time structure from date and time will be set)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e setTime(time_t);

/******************************************************************
 *@brief (This API is used to convert string buffer to Epoch time)
 *
 *@param[IN] uint8_t * (timeBuffer pointer to a buffer holding time in string formate)
 *@param[OUT] None
 *
 *@return time_t (It returns the epoch time)
 *********************************************************************/
time_t stringToEpochTime(uint8_t *);

/******************************************************************
 *@brief (This API is used to get the RTC time)
 *
 *@param[IN] None
 *@param[OUT] time_t *(Indicates pointer to time structure where date and time will be saved in it)
 *
 *@return returnCode_e  (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e getRtcTime(time_t *epochTime);

/******************************************************************
 *@brief (This API is used to get the RTC time when GW is powerd off)
 *
 *@param[IN] None
 *@param[OUT] uint8_t* (Indicates if the GW was hard/Soft Power Off)
 *@param[OUT] time_t * (Indicates pointer to time structure where date and time will be saved in it)
 *
 *@return returnCode_e  (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e getRtcBsfData(uint8_t* hardPowerOff, time_t *epochTime);

#endif      //#ifndef _RTC_H_
