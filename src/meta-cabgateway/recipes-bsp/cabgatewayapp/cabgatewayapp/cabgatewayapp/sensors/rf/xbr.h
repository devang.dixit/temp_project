/*******************************************************************************
 * Copyright(c) <2016>, Volansys Technologies
 *
 * Description: 
 * @file uart.h
 * @brief  Header file for uart communication between M4 and A7
 * 
 * @Author   - Volansys
 *
 *******************************************************************************
 *
 * @History
 *
 * Mar/21/2017 VT Created 
 *
 ******************************************************************************/
#ifndef _XBR_H
#define _XBR_H

/**************************
 * 		INCLUDES
 *************************/
#include<stdint.h>
#include <termios.h>
#include <unistd.h>
#include "watchdog.h"
#include "syscall.h"

/**************************
 * 		MACROS
 *************************/

#define UART_PORT_XBR				"/dev/ttymxc2"
#define UART_BAUDRATE_XBR		B38400

#define	XBR_PKT_LEN	10
#define XBR_QUEUE_NAME	"/XBRQUEUE"

#define MAX_SENSORS	40

/********************************************************
|	StartOfFrame:	2bytes,	[0x55, 0xaa]
|	Header:			3 bytes,[command, lengthOfPayload(2byte)]
|	Payload:		DATA + CRC[2 bytes]
*********************************************************/

/**************************
 *		Enumerations
 *************************/

/*************************
 *		STRUCTURES
 ************************/
void* xbrUartThread(void *arg);
void* xbrProcessThread(void *arg);
int8_t addPressureProSensor(void *sensorID);
int8_t removePressureProSensor(void *sensorID);
/*
 * Callback function to fetch data from relevant RF received module
*/
void XbrDataFetchCB(int sigNum, siginfo_t * sigInfo, void * unused);

#endif	/* end _UART_H */
