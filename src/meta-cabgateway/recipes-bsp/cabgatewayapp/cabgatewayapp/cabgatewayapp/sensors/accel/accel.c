/************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : accel.c
 * @brief : Accelerometer sensor handling
 *
 * @Author     - VT
 *************************************************************************************************
 * History
 *
 * Mar/04/2019, VT , First Draft for sample application
 *************************************************************************************************/
#ifdef ACCEL_SUPPORT
#include "accel.h"
#include "userInterface.h"
#endif

#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>
#include "debug.h"
#include <time.h>
#include <assert.h>
#include <errno.h>
#include <signal.h>


#define INACTIVITY_THRESHOLD_REG_ADDR   (0x1E)
#define INACTIVITY_DURATION_REG_ADDR    (0x1F)
#define ACCEL_INT1_CFG_REG_ADDR         (0x22)
#define ACCEL_ODR_CFG_REG_ADDR          (0x20)
#define ACCEL_TEMP_L_REG_ADDR           (0x0B)
#define ACCEL_TEMP_H_REG_ADDR           (0x0C)
#define ACCEL_ODR_CFG_REG_MASK          (0x70)
#define ACCEL_ODR_CFG_REG_SHIFT         (4)

static sem_t *temp_lock;
static char *name        = "lis2hh12_accel";\
static struct iio_device *dev;
/* IIO structs required for streaming */
static struct iio_context *ctx;



#ifdef ACCEL_SUPPORT
static inactivity = 0;
void accelInterrupt1Handle(int sig)
{
	FUNC_ENTRY
	CAB_DBG(CAB_GW_DEBUG, "Accel int 1 occured");
	if (inactivity == 0) {
		initUIModule(NULL);
		indicateSysStatus(ERROR);
		deInitUIModule();
		inactivity = 1;
	} else {
		initUIModule(NULL);
		indicateSysStatus(NORMAL);
		deInitUIModule();
		inactivity = 0;
	}
	FUNC_EXIT
}

void accelInterrupt2Handle(int sig)
{
	FUNC_ENTRY
	CAB_DBG(CAB_GW_DEBUG, "Accel int 2 occured");
	FUNC_EXIT
}
#endif

returnCode_e Sem1_DeInit()
{
	int return_val;
	return_val = sem_close(temp_lock);
	if (return_val == -1) {
		CAB_DBG(CAB_GW_ERR, "Sem Close Failed");
		return -1;
	}

	return_val = sem_unlink("/temp_sem");
	if (return_val == -1) {
		CAB_DBG(CAB_GW_ERR, "Sem Unlink Failed");
		return -1;
	}
	return GEN_SUCCESS;
}

returnCode_e accelReadTemp(float *accelTemperature)
{
	uint32_t regReadVal=0;
	uint16_t temp_reg = 0;
	struct timespec ts;
	int ret;
	int count = 1;

	if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {
		    /* handle error */
		    return -1;
	}
	ts.tv_sec += 3;
	FUNC_ENTRY

	do {
		if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {
			            return -1;
		}
		ts.tv_sec += 3;
		ret = sem_timedwait(temp_lock, &ts);
		count++;
	} while(ret != 0 && count < 11);

	if (count == 10) {
		ret  = Sem1_DeInit();

		if ((temp_lock = sem_open ("/temp_sem", O_CREAT, 0660, 1)) == SEM_FAILED) {
			printf("sem_open failed \r\n");
			return SEM_FAILED;
		}
		ret = sem_timedwait(temp_lock, &ts);
	}

	if (dev) {
		if (!iio_device_reg_read(dev, ACCEL_TEMP_H_REG_ADDR, &regReadVal)) {
			CAB_DBG(CAB_GW_TRACE, "Temp reg H : 0x%x", regReadVal);
			temp_reg |= (regReadVal & 0xFF) << 8;
			CAB_DBG(CAB_GW_TRACE, "Temp reg  : 0x%x", temp_reg);

			if (!iio_device_reg_read(dev, ACCEL_TEMP_L_REG_ADDR, &regReadVal)) {
				CAB_DBG(CAB_GW_TRACE, "Temp reg L : 0x%x", regReadVal);
				temp_reg |= (regReadVal & 0xFF);
				CAB_DBG(CAB_GW_TRACE, "Temp reg  : 0x%x", temp_reg);
				temp_reg >>= 5;
				CAB_DBG(CAB_GW_TRACE, "Temp reg  : 0x%x", temp_reg);
				if (temp_reg & 0x400) {
					temp_reg -= 2048;
				}
				*accelTemperature = ((int16_t)temp_reg/8.0) + 25.0;
				CAB_DBG(CAB_GW_TRACE, "Temp final : %f", *accelTemperature);
			} else {
				CAB_DBG(CAB_GW_ERR, "Accel read api failed in iio read reg");
				return GEN_API_FAIL;
			}
		} else {
			CAB_DBG(CAB_GW_ERR, "Accel read api failed in iio read reg");
			return GEN_API_FAIL;
		}
	} else {
		CAB_DBG(CAB_GW_ERR, "No device inited");
		return GEN_API_FAIL;
	}

	sem_post(temp_lock);
	FUNC_EXIT
	return GEN_SUCCESS;
}

/* cleanup and exit */
static void shutdown()
{
	CAB_DBG(CAB_GW_TRACE, "* Destroying context");
	if (ctx) { 
		iio_context_destroy(ctx); 
	}
}


returnCode_e accelInit()
{
	uint32_t regReadVal=0, writeVal = 0;
	
	if ((temp_lock = sem_open ("/temp_sem", O_CREAT, 0660, 1)) == SEM_FAILED) {
		printf("sem_open failed");
		return SEM_FAILED;
	}

	CAB_DBG(CAB_GW_TRACE, "Acquiring IIO context");
	ctx = iio_create_default_context();
	if (ctx == NULL) {
		CAB_DBG(CAB_GW_ERR, "create context failed");
		return MODULE_INIT_FAIL;
	}

	if(iio_context_get_devices_count(ctx) <= 0) {
		CAB_DBG(CAB_GW_ERR, "IIO get device failed");
		return MODULE_INIT_FAIL;
	}
	
	CAB_DBG(CAB_GW_TRACE," Acquiring device %s", name);
	dev = iio_context_find_device(ctx, name);
	if (!dev) {
		CAB_DBG(CAB_GW_ERR, "No device found");
		return MODULE_INIT_FAIL;
	}   
	
	if (!iio_device_reg_read(dev, ACCEL_ODR_CFG_REG_ADDR, &regReadVal)) {
		writeVal = (0x01 << ACCEL_ODR_CFG_REG_SHIFT) | (regReadVal & ~ACCEL_ODR_CFG_REG_MASK);
		if (iio_device_reg_write(dev, ACCEL_ODR_CFG_REG_ADDR, writeVal)){
			CAB_DBG(CAB_GW_ERR, "ODR reg write failed");
			shutdown();
			return MODULE_INIT_FAIL;
		}
	} else {
		CAB_DBG(CAB_GW_ERR, "ODR reg read failed");
		shutdown();
		return MODULE_INIT_FAIL;
	}
	return GEN_SUCCESS;
}

