#include "utils/sysTimer.h"
#include <fcntl.h>
#include <errno.h>
#include <syscall.h>
#include <sys/stat.h>
#include <mqueue.h>
#include <pthread.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include "error.h"
#include "debug.h"
#include "cJSON.h"
#include "dataStorage.h"
#include "dataControl.h"
#include "cloud.h"
#include "LteModule.h"
#include "eg91ATCmd.h"
#include "watchdog.h"
#include "memory.h"
#include "sysLogStorage.h"
#include "temperature_monitor.h"
#include "hwVersion.h"
#include "powerControl.h"

#define ONE_MIN						(60)
#define ONE_HOUR					(3600)
#define TIMER_RESOLUTION_SEC				(60)
#define PID_CHECK_SEC					(1 * ONE_MIN)
#define LTE_CHECK_SEC					(5 * ONE_MIN)
#define INTERNET_CHECK_SEC				(30 * ONE_MIN)
/***************** NOTE: BELOW MACRO SIM NOT PRESENT IS DEPENDENT ON
 * 			 INTERNET_CHECK_SEC, UPDATE IT AS 30% INTERNET_CHECK_SEC in minutes
 * ****/
#define SIM_NOT_PRESENT_CONFD_COUNT			(10) /*confirm sim not present if 15 sim not present in 30 minutes*/
#define SYS_RSET_CHECK_SEC				(3 * ONE_HOUR)
#define PID_CHECK_COUNT					(PID_CHECK_SEC/TIMER_RESOLUTION_SEC)
#define LTE_CONNECTIVITY_CHECK_COUNT	(LTE_CHECK_SEC/TIMER_RESOLUTION_SEC)
#define PID_CHECK_MSGQ_MAX_SIZE				(10)
#define PID_MONITOR_QUEUE				"/pid_monitor_queue"
#define MQ_SEND_PID_MONITOR_TOUT_SEC			(3 * ONE_MIN)
#define PID_MONITOR_MAX_BUF				(10)
#define LTE_STATE_MSGQ_MAX_SIZE				(50)
#define LTE_CMD_MSGQ_MAX_SIZE				(50)
#define LTE_MAX_INTERNET_CONN_FAIL_CNT 			(INTERNET_CHECK_SEC/LTE_CHECK_SEC)
#define LTE_MAX_SYS_RESET_CNT 				(SYS_RSET_CHECK_SEC/LTE_CHECK_SEC)
#define LTE_SW_RST_NW_AVAIL_THRESH			(LTE_MAX_INTERNET_CONN_FAIL_CNT - (LTE_MAX_INTERNET_CONN_FAIL_CNT/4))
#define LTE_SYSRST_NW_AVAIL_THRESH			(LTE_MAX_SYS_RESET_CNT - (LTE_MAX_SYS_RESET_CNT/4))
#define LTE_STATE_QUEUE_NAME_FOR_REMOTE_DIAG_APP	"/lte_state_queue_for_remote_diag_app"
#define LTE_CMD_QUEUE_NAME_FOR_REMOTE_DIAG_APP		"/lte_cmd_queue_for_remote_diag_app"
#define PPPD_USB_PORT					"/dev/ttyUSB3"
#define MQ_SEND_LTE_STATUS_TIMEOUT      		(5 * ONE_MINUTE)
#define WATCHDOG_TIMER_INTERVAL         		(45 * ONE_MINUTE)
#define SYSLOG_TIMER_INTERVAL         			(4 * ONE_HOUR)
#define WATCHDOG_TIMER_COUNT				(WATCHDOG_TIMER_INTERVAL/TIMER_RESOLUTION_SEC)
#define SYSLOG_COLLECT_TIMER_COUNT			(SYSLOG_TIMER_INTERVAL/TIMER_RESOLUTION_SEC)

#define ENABLE_WD

typedef enum {
	CHECK_PID_AND_USB_PORT_AVAIL = 0,
	LTE_CONNECTIVITY_CHECK,
	WATCHDOG_EVENT,
	MAX_PID_MONITOR_EVENT
} pidMonitorEvent_e;

typedef struct pidMonitorStruct {
	pidMonitorEvent_e eventType;
} pidMonitorStruct_t;

static mqd_t mqRemoteDiagAppReqQueue = -1;
static mqd_t mqRemoteDiagAppResQueue = -1;
static mqd_t pidMonitorQueue = -1;
static mqd_t mqLteStateQueue = -1;
static mqd_t mqLteCmdQueue = -1;
static pthread_t pidAndUsbPortMonitorThreadID_t;
static pthread_t rDiagResHandlerThreadID_t;
static pthread_t lteStateHandlerThreadID_t;
static pthread_t tempMonitorThreadID_t;
static uint32_t lteAppPid = 0;
static uint32_t cabAppLauncherPid = 0;
static bool isLTEConnected = false;
static bool isCloudInitDone = false;
static uint16_t lteDiagAvailCount = 0;
static uint16_t lteDiagAvailCountSysRst = 0;
static uint16_t g_simNotPresentCount = 0;
pthread_mutex_t lteStatusLock;
pthread_mutex_t lteDiagVarLock;
pthread_mutex_t simNotPrVarLock;
cloudInitInfo_t initCloudConfig = {
	.url = {0}, .gatewayID = {0}
};

lteDiagInfo_t g_lteDiag;
static char initSoftwareVersion[MAX_LEN_SOFTWARE_VERSION] =  "3.0.0.1";

static TIMER_HANDLE pidMonitorTimerHandle  = INVALID_TIMER_HANDLE;
static TIMER_HANDLE watchDogTimerHandle  = INVALID_TIMER_HANDLE;
static TIMER_HANDLE syslogZipTimerHandle  = INVALID_TIMER_HANDLE;
static TIMER_HANDLE syslogCollectTimerHandle  = INVALID_TIMER_HANDLE;
static TIMER_HANDLE LteConnectionTimerHandler = INVALID_TIMER_HANDLE;

static pthread_mutex_t pidAndUsbPortMonitorThreadAliveLock = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t rDiagResHandlerThreadAliveLock = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t lteStateHandlerThreadAliveLock = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t tempMonitorThreadAliveLock = PTHREAD_MUTEX_INITIALIZER;

static bool g_pidAndUsbPortMonitorThreadAliveStatus = true;
static bool g_rDiagResHandlerThreadAliveStatus = true;
static bool g_lteStateHandlerThreadAliveStatus = true;
static bool g_tempMonitorThreadAliveStatus = true;

static char g_errorJSONFileName[MAX_FILENAME_LEN] = {'\0'};
bool hwBit = false;

void setLteDiagAvailSysRstCount(uint16_t lteDiagCount)
{
        pthread_mutex_lock(&lteDiagVarLock);
	lteDiagAvailCountSysRst = lteDiagCount;
        pthread_mutex_unlock(&lteDiagVarLock);
}

uint16_t getLteDiagAvailSysRstCount()
{
        uint16_t outLteDiagCount;
        pthread_mutex_lock(&lteDiagVarLock);
	outLteDiagCount = lteDiagAvailCountSysRst;
        pthread_mutex_unlock(&lteDiagVarLock);
        return outLteDiagCount;
}

void setLteDiagAvailCount(uint16_t lteDiagCount)
{
        pthread_mutex_lock(&lteDiagVarLock);
	lteDiagAvailCount = lteDiagCount;
        pthread_mutex_unlock(&lteDiagVarLock);
}

uint16_t getLteDiagAvailCount()
{
        uint16_t outLteDiagCount;
        pthread_mutex_lock(&lteDiagVarLock);
	outLteDiagCount = lteDiagAvailCount;
        pthread_mutex_unlock(&lteDiagVarLock);
        return outLteDiagCount;
}

void setSimNotPresentCount(uint16_t simNotPresentCount)
{
        pthread_mutex_lock(&simNotPrVarLock);
	g_simNotPresentCount = simNotPresentCount;
        pthread_mutex_unlock(&simNotPrVarLock);
}

uint16_t getSimNotPresentCount()
{
        uint16_t simNotPresentCount;
        pthread_mutex_lock(&simNotPrVarLock);
	simNotPresentCount = g_simNotPresentCount;
        pthread_mutex_unlock(&simNotPrVarLock);
        return simNotPresentCount;
}

void setLteState(bool state)
{
        pthread_mutex_lock(&lteStatusLock);
        isLTEConnected = state;
        pthread_mutex_unlock(&lteStatusLock);
}

bool getLteState()
{
        bool outLteState;
        pthread_mutex_lock(&lteStatusLock);
        outLteState = isLTEConnected;
        pthread_mutex_unlock(&lteStatusLock);
        return outLteState;
}

static uint16_t getCurrentZipCount()
{
	uint16_t zipCount;
	returnCode_e retCode = GEN_SUCCESS;

	// Get the current zip files for remote diag App.
	retCode = getZipCount(&zipCount);

	if (GEN_SUCCESS == retCode) {
		return zipCount;
	}
	else {
		return -1;
	}
}

void rDiagcloudConnectionStatusNotifycallback(void * data)
{
    FUNC_ENTRY

    bool status;
    callBackNotification_t callBackData;

    if(!data)
    {
        CAB_DBG(CAB_GW_ERR, "Invalid argument");
        return;
    }

    memcpy(&callBackData, (callBackNotification_t *)data, sizeof(callBackNotification_t));

    switch (callBackData.CBNotifType)
    {
    case CLOUD_OTA_DATA:
    {
        /* Do nothing */
    } break;
    case CLOUD_CONNECTION_STATUS:
    {
        status = callBackData.cloudConnectionStatus;
        sendCmdToCabAppA7(CLOUD_CONNECTION_STATUS_UPDATE, &status);
    } break;
    default:
        break;
    }

    FUNC_EXIT
}

void rDiagZipUploadNotifycallback(DataFilezipType_e fielType, void * uploadStatus)
{
	FUNC_ENTRY

	if(!uploadStatus)
	{
		CAB_DBG(CAB_GW_ERR, "Invalid argument");
		return;
	}

	/* Place holder to notify the rDiag zip file upload status */

	FUNC_EXIT
}

static returnCode_e remoteDiagAppReqQueueSetup()
{
	returnCode_e retCode = GEN_SUCCESS;
	struct mq_attr attr;
	FUNC_ENTRY

	/* initialize the queue attributes */
	attr.mq_flags = 0;
	attr.mq_maxmsg = RDIAG_MAX_MSG; // request buffer size
	attr.mq_msgsize = sizeof(remoteDiagWrapper_t); 
	attr.mq_curmsgs = 0;

	mq_unlink(RDIAG_RES_QUEUE);

	/* create the message queue */
	mqRemoteDiagAppReqQueue = mq_open(RDIAG_RES_QUEUE, O_CREAT | O_RDWR, 0666, &attr);
	if(mqRemoteDiagAppReqQueue < 0)
	{
		CAB_DBG(CAB_GW_ERR, "mq_open for Remote Diag Request queue fail with error %s", strerror(errno));
		FUNC_EXIT
		return MQ_OPEN_ERROR;
	}

	FUNC_EXIT
	return retCode;
}

static returnCode_e remoteDiagAppResQueueSetup()
{
	returnCode_e retCode = GEN_SUCCESS;
	struct mq_attr attr;
	FUNC_ENTRY

	/* initialize the queue attributes */
	attr.mq_flags = 0;
	attr.mq_maxmsg = RDIAG_MAX_MSG;
	attr.mq_msgsize = sizeof(remoteDiagWrapper_t);
	attr.mq_curmsgs = 0;

	mq_unlink(RDIAG_REQ_QUEUE);

	/* create the message queue */
	mqRemoteDiagAppResQueue = mq_open(RDIAG_REQ_QUEUE, O_CREAT | O_RDWR, 0666, &attr);
	if(mqRemoteDiagAppResQueue < 0)
	{
		perror("mq failed");
		CAB_DBG(CAB_GW_ERR, "mq_open for Remote Diag Response queue fail with error %s", strerror(errno));
		FUNC_EXIT
		return MQ_OPEN_ERROR;
	}

	FUNC_EXIT
	return retCode;
}

static returnCode_e lteStateQueueSetup()
{
        returnCode_e retCode = GEN_SUCCESS;
        struct mq_attr attr;
        FUNC_ENTRY

        /* initialize the queue attributes */
        attr.mq_flags = 0;
        attr.mq_maxmsg = LTE_STATE_MSGQ_MAX_SIZE;
        attr.mq_msgsize = sizeof(lteState_t);
        attr.mq_curmsgs = 0;

        /* Cleanup the msg queue if exist */
        mq_unlink(LTE_STATE_QUEUE_NAME_FOR_REMOTE_DIAG_APP);

        /* create the message queue */
        mqLteStateQueue = mq_open(LTE_STATE_QUEUE_NAME_FOR_REMOTE_DIAG_APP, O_CREAT | O_RDWR, 0666, &attr);
        if(mqLteStateQueue < 0)
        {
                CAB_DBG(CAB_GW_ERR, "mq_open for LTE state queue fail with error %s", strerror(errno));
                FUNC_EXIT
                return MQ_OPEN_ERROR;
        }

        FUNC_EXIT
        return retCode;
}

static returnCode_e lteCmdQueueSetup()
{
        returnCode_e retCode = GEN_SUCCESS;
        struct mq_attr attr;
        FUNC_ENTRY

        /* initialize the queue attributes */
        attr.mq_flags = 0;
        attr.mq_maxmsg = LTE_CMD_MSGQ_MAX_SIZE;
        attr.mq_msgsize = sizeof(lteState_t);
        attr.mq_curmsgs = 0;

        /* Cleanup the msg queue if exist */
        mq_unlink(LTE_CMD_QUEUE_NAME_FOR_REMOTE_DIAG_APP);

	mqLteCmdQueue = mq_open(LTE_CMD_QUEUE_NAME_FOR_REMOTE_DIAG_APP, O_CREAT | O_WRONLY, 0666, &attr);
        if(mqLteCmdQueue < 0)
        {
                CAB_DBG(CAB_GW_ERR, "mq_open for LTE command queue fail with error %s", strerror(errno));
                FUNC_EXIT
                return MQ_OPEN_ERROR;
        }

        FUNC_EXIT
        return retCode;
}

static returnCode_e pidMonitoringQueueSetup()
{
	returnCode_e retCode = GEN_SUCCESS;
	struct mq_attr attr;
	FUNC_ENTRY

	/* initialize the queue attributes */
	attr.mq_flags = 0;
	attr.mq_maxmsg = PID_MONITOR_MAX_BUF;
	attr.mq_msgsize = sizeof(pidMonitorStruct_t);
	attr.mq_curmsgs = 0;

	/* Cleanup the msg queue if exist */
	mq_unlink(PID_MONITOR_QUEUE);

	/* create the message queue */
	pidMonitorQueue = mq_open(PID_MONITOR_QUEUE, O_CREAT | O_RDWR, 0644, &attr);
	if(pidMonitorQueue < 0)
	{
		CAB_DBG(CAB_GW_ERR, "mq_open for pid monitor queue fail with error %s", strerror(errno));
		FUNC_EXIT
		return MQ_OPEN_ERROR;
	}

	FUNC_EXIT
	return retCode;
}

static returnCode_e setupMsgQueue()
{
	returnCode_e retCode = GEN_SUCCESS;
	FUNC_ENTRY

	retCode = remoteDiagAppReqQueueSetup();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "remoteDiagAppReqQueueSetup() fail with error %d", retCode);
		FUNC_EXIT
		return retCode;
	}

	retCode = remoteDiagAppResQueueSetup();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "remoteDiagAppResQueueSetup() fail with error %d", retCode);
		FUNC_EXIT
		return retCode;
	}

	retCode = lteStateQueueSetup();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "lteStateQueueSetup() fail with error %d", retCode);
		FUNC_EXIT
		return retCode;
	}

	retCode = lteCmdQueueSetup();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "lteCmdQueueSetup() fail with error %d", retCode);
		FUNC_EXIT
		return retCode;
	}

	retCode = pidMonitoringQueueSetup();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "pidMonitoringQueueSetup() fail with error %d", retCode);
		FUNC_EXIT
		return retCode;
	}

	FUNC_EXIT
	return retCode;
}

static char* createJsonPayload(void *msg)
{
        FUNC_ENTRY

        char *payloadAddr = NULL;
        cJSON *root, *fmt, *fmt1;
	remoteDiagInfoSend_t *data = (remoteDiagInfoSend_t *)msg;
	int i=0;

	root = cJSON_CreateObject();
	if (!root)
		return NULL;

	cJSON_AddNumberToObject(root, "timestamp", data->time);
	cJSON_AddNumberToObject(root, "error_type", data->errorType);
	cJSON_AddStringToObject(root, "message", data->msg);
	cJSON_AddNumberToObject(root, "log_level", data->level);

	cJSON_AddItemToObject(root, "logs", fmt = cJSON_CreateArray());
	if(!fmt)
		return NULL;

	for(i=0; i<data->noOfRDiagLogs; i++)
	{
		fmt1 = cJSON_CreateObject();
		if (!fmt1)
			return NULL;

		cJSON_AddNumberToObject(fmt1, "timestamp", data->rDiagLogs[i].time);
		cJSON_AddStringToObject(fmt1, "message", data->rDiagLogs[i].msg);
		cJSON_AddNumberToObject(fmt1, "log_level", data->rDiagLogs[i].level);

		cJSON_AddItemToArray(fmt, fmt1);
	}

	payloadAddr = cJSON_PrintUnformatted(root);

	return payloadAddr;
}

//*****************************************************************************
//  rDiagCloudInit()
//  Param:
//      IN :    bool    //updateURL
//              bool    //updateID
//      OUT:    NONE
//  Returns:
//              returnCode_e    //return code
//  Description:
//      This function initializes the cloud for applicationt to use.
//      In addition to that it supports updation of cloudURL and GWID if 
//      any change is requested.
//
//  [Pre-condition:]
//      initSysTimer() : The init cloud configuration must be valid
//  [Constraints:]
//      None
//
//*****************************************************************************
static returnCode_e rDiagCloudInit(bool udpdateURL, bool updateID)
{

	returnCode_e retCode = GEN_SUCCESS;
	char tempurl[MAX_LEN_CLOUD_URI] = {0};
	char tempgatewayID[MAX_LEN_GATEWAY_ID] = {0};

	if ( (memcmp(tempurl, initCloudConfig.url, sizeof(initCloudConfig.url)) == 0 ) || 
 	     (memcmp(tempgatewayID, initCloudConfig.gatewayID, sizeof(initCloudConfig.gatewayID)) == 0 )) {
		CAB_DBG(CAB_GW_WARNING, "Init cloud config are not proper");
		return GEN_API_FAIL;
	}

	if (udpdateURL && isCloudInitDone)
	{
	    retCode = updateCloudUrl(initCloudConfig.url);
	    if(retCode != GEN_SUCCESS) {
	        CAB_DBG(CAB_GW_ERR, "update cloud URI fail with error %d", retCode);
	        return retCode;
	    }
	}

	if (updateID && isCloudInitDone)
	{
	    retCode = validateGWID(initCloudConfig.gatewayID);
	    if(retCode != GEN_SUCCESS) {
	        CAB_DBG(CAB_GW_ERR, "Gateway ID validation fail with error %d", retCode);
	        sendCmdToCabAppA7(GW_ID_REQ, NULL);
	        return retCode;
	    }

	    retCode = updateGWID(initCloudConfig.gatewayID);
	    if(retCode != GEN_SUCCESS) {
	        CAB_DBG(CAB_GW_ERR, "update gateway ID fail with error %d", retCode);
	        return retCode;
	    }
	}

	retCode = cloudInit(&initCloudConfig, initSoftwareVersion, (cloudConStatusNotifCB)rDiagcloudConnectionStatusNotifycallback);
	if(retCode != GEN_SUCCESS)
	{
	    isCloudInitDone = false;
	    CAB_DBG(CAB_GW_ERR, "initCloud fail with error %d", retCode);
	    return retCode;
	}
	else
	{
	    isCloudInitDone = true;
	}

	return retCode;
}

static returnCode_e sendLteCmd(lteCmd_e cmd)
{
        returnCode_e retCode = GEN_SUCCESS;
        lteCmd_t lteCmdData;
        struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0}; 
        clock_gettime(CLOCK_REALTIME, &abs_timeout);
        abs_timeout.tv_sec += MQ_SEND_LTE_CMD_TIMEOUT;
        FUNC_ENTRY

        memset(&lteCmdData, 0, sizeof(lteCmdData));
        lteCmdData.cmd = cmd;

        CAB_DBG(CAB_GW_TRACE, "Sending LTE %d command", lteCmdData.cmd);

        if(mq_timedsend(mqLteCmdQueue, (const char *)&lteCmdData, sizeof(lteCmd_t), 0, &abs_timeout) < 0)
        {   
                CAB_DBG(CAB_GW_ERR, "mq timed send fail for LTE command message queue");
                retCode = MQ_SEND_ERROR;
        }   

        FUNC_EXIT
        return retCode;
}

void sendCmdToCabAppA7(rDiagMsgType_e cmd, void * data)
{
	remoteDiagWrapper_t msg;
        struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0};
        clock_gettime(CLOCK_REALTIME, &abs_timeout);
        abs_timeout.tv_sec += MQ_SEND_TOUT_SEC;
        FUNC_ENTRY

        memset(&msg, '\0', sizeof(remoteDiagWrapper_t));

        // Need to send last Internet connectivity time stamp to CabAppA7
        if (cmd == INTERNET_CONNECTIVITY_TIMESTAMP_UPDATE && data != NULL)
        {
        	memcpy(&(msg.timeStamp), data, sizeof(time_t));
        }
        else if (cmd == CLOUD_CONNECTION_STATUS_UPDATE && data != NULL)
        {
        	memcpy(&(msg.cloudConnectionStatus), data, sizeof(bool));
        }

        msg.type = cmd;

        if (mq_timedsend(mqRemoteDiagAppReqQueue, (const char *)&msg, sizeof(remoteDiagWrapper_t), 0, &abs_timeout) < 0) {
		CAB_DBG(CAB_GW_ERR, "Message queue send failed to get gateway ID");
	}
        FUNC_EXIT
}

static bool getRDiagResHandlerThreadAliveStatus(void)
{
        bool status;
        pthread_mutex_lock(&rDiagResHandlerThreadAliveLock);
        status = g_rDiagResHandlerThreadAliveStatus;
        pthread_mutex_unlock(&rDiagResHandlerThreadAliveLock);
        return status;
}

static void setRDiagResHandlerThreadAliveStatus(bool value)
{
        pthread_mutex_lock(&rDiagResHandlerThreadAliveLock);
        g_rDiagResHandlerThreadAliveStatus = value;
        pthread_mutex_unlock(&rDiagResHandlerThreadAliveLock);
}

bool rDiagResHandlerCallback(void)
{
        bool status = false;
	remoteDiagWrapper_t msg;
        struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0};
        clock_gettime(CLOCK_REALTIME, &abs_timeout);
        abs_timeout.tv_sec += MQ_SEND_TOUT_SEC;

        if(getRDiagResHandlerThreadAliveStatus() == true)
        {
                status = true;
        }

        setRDiagResHandlerThreadAliveStatus(false);

	memset(&msg, '\0', sizeof(msg));
	msg.type = WATCHDOG_TIMER_MSG;

        if (mq_timedsend(mqRemoteDiagAppResQueue, (const char *)&msg, sizeof(remoteDiagWrapper_t), 0, &abs_timeout) < 0) {
		CAB_DBG(CAB_GW_ERR, "message queue send failed");
		status = false;
	}

        return status;
}

//*****************************************************************************
//  createErrorJsonFile()
//  Param:
//      IN :    chanr * (JSON payload to be dumped into file)
//      OUT:    NONE
//  Returns:
//     returnCode_e    //return code
//  Description:
//      This function creates the error.json files and dump JSON payload to
//      the file.
//
//  [Pre-condition:]
//      None
//  [Constraints:]
//      None
//
//*****************************************************************************
static returnCode_e createErrorJsonFile(char *payload)
{
    /* In case initSDC() is not done yet and got message for RDIAG_MSG, error.json will
       be created. But zip file will not be created. Due to this, multiple JSON objects 
       get appended in erro.json file and make it invalid to be used in backend. The 
       issue arises with the specified pre-condition only.

       To handle this issue, we will create a new json file each time CabApp sends
       rDiagSendMessage(). Once initSDC() is completed, along with the next 
       rDiagSendMessage() we will create zip file for all previous error.json files.
    */

    FUNC_ENTRY

    struct stat buffer = {0};
    FILE * outFile;	    //file descriptor
    time_t timeData;
    uint32_t ret;
    char tempFileName[MAX_FILENAME_LEN];

    int exist = stat(g_errorJSONFileName, &buffer);

    if (exist == 0) {
        // If error.json file exists we need to handle it as explained earlier
        time(&timeData);

        snprintf(tempFileName, MAX_FILENAME_LEN, "%s_%ld", g_errorJSONFileName, timeData);

        outFile = fopen(tempFileName, "a");
        if(outFile == NULL) {
            CAB_DBG(CAB_GW_ERR, "%s file opening failed.", tempFileName);
            FUNC_EXIT
            return GEN_NULL_POINTING_ERROR;
        }

        ret = fprintf(outFile, "%s", payload);
        if(ret < 0) {
            CAB_DBG(CAB_GW_ERR, "%s file writing failed.", tempFileName);
            fclose(outFile);
            FUNC_EXIT
            return FILE_WRITE_ERROR;
        }
    } 
    else {
        // Open error.json file. If not exist, create it.
        outFile = fopen(g_errorJSONFileName, "a");
        if(outFile == NULL) {
            CAB_DBG(CAB_GW_ERR, "%s file opening failed.", g_errorJSONFileName);
            FUNC_EXIT
            return GEN_NULL_POINTING_ERROR;
        }

        ret = fprintf(outFile, "%s", payload);
        if(ret < 0) {
            CAB_DBG(CAB_GW_ERR, "%s file writing failed.", g_errorJSONFileName);
            fclose(outFile);
            FUNC_EXIT
            return FILE_WRITE_ERROR;
        }
    }

    fflush(outFile);
    fsync(fileno(outFile));
    fclose(outFile);

    FUNC_EXIT

    return GEN_SUCCESS;
}

static void *rDiagResHandler(void *arg)
{
	ssize_t bytes_read;
	remoteDiagWrapper_t msgRecv;
	char *payload = NULL;
	time_t timeData;
	returnCode_e retCode;
	watchDogTimer_t newWatchdogNode;
	bool cloudStatus = false;

	FUNC_ENTRY

	newWatchdogNode.threadId = syscall(SYS_gettid);
	newWatchdogNode.CallbackFn = rDiagResHandlerCallback;

	CAB_DBG(CAB_GW_INFO, "Remote Diag Response handler thread id : %ld", syscall(SYS_gettid));
	retCode = threadRegister(&newWatchdogNode);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "Remote Diag Response handler thread registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_REGISTRATION_FAIL, GW_ERR, "Remote Diag Response handler thread registration failed");
        }                     

	if (sprintf(g_errorJSONFileName, ERROR_JSON_FILE, DIR_PATH_DATA_RDIAG) <= 0) {
		CAB_DBG(CAB_GW_ERR, "not able to set error json file");
		/*TODO proper remote diag recover*/
	}

	while(1)
	{
		memset(&msgRecv, '\0', sizeof(remoteDiagWrapper_t));
		bytes_read = mq_receive(mqRemoteDiagAppResQueue, (char *)&(msgRecv), sizeof(remoteDiagWrapper_t), NULL);
		if(bytes_read >= 0)
		{
			switch(msgRecv.type) {
				case RDIAG_MSG:
				{
					payload = (char *)createJsonPayload((void *)&(msgRecv.sendRDiagMsg));
					if(payload == NULL)
					{
						CAB_DBG(CAB_GW_ERR, "creating json payload fail");
						FUNC_EXIT
					}

					retCode = createErrorJsonFile(payload);
					if (retCode != GEN_SUCCESS) {
						CAB_DBG(CAB_GW_ERR,"Creating JSON file failed.");
					}
					
					time(&timeData);
					retCode = createFilePackage(timeData, JSON_DATA);
					if (retCode == GEN_SUCCESS) {
						CAB_DBG(CAB_GW_TRACE,"Create FILE Package SUCCESS");
					}
					retCode = reportEventToSDC(TRANSMIT_EV, (void *)&timeData, JSON_DATA);
					if (GEN_SUCCESS != retCode) {
						CAB_DBG(CAB_GW_ERR, "Report event from remotediag failed with code %d", retCode);
					}
				}
				break;
				case INIT_CONFIG:
				{
				    bool updateURL=false, updateID=false;
					/*TODO: validation of message*/
					retCode = initSDC(msgRecv.sendInitConfig.maxZipCount, (uploadzipNotifCB)rDiagZipUploadNotifycallback, msgRecv.sendInitConfig.softwareVersion, 2345, DIR_PATH_DATA_RDIAG);
					if(retCode != GEN_SUCCESS && retCode != SDC_ALREADY_INIT_ERR) {
						CAB_DBG(CAB_GW_ERR, "init SDC failed with code %d", retCode);
					}

                    /* Reset the previous cloud connection status */
                    sendCmdToCabAppA7(CLOUD_CONNECTION_STATUS_UPDATE, &cloudStatus);

					/* Only update the URL and gatewayID if something is changed */

					if  (memcmp(msgRecv.sendInitConfig.cloudURI, initCloudConfig.url, sizeof(initCloudConfig.url)) != 0 )
					{
					    updateURL = true;
					    memcpy(initCloudConfig.url, msgRecv.sendInitConfig.cloudURI, sizeof(initCloudConfig.url));
					}

					if (memcmp(msgRecv.sendInitConfig.gatewayId, initCloudConfig.gatewayID, sizeof(initCloudConfig.gatewayID)) != 0 )
					{
					    updateID = true;
					    memset(&initCloudConfig.gatewayID, '\0', MAX_LEN_GATEWAY_ID);
					    memcpy(initCloudConfig.gatewayID, msgRecv.sendInitConfig.gatewayId, sizeof(initCloudConfig.gatewayID));
					}

					if  (memcmp(initSoftwareVersion, msgRecv.sendInitConfig.softwareVersion, sizeof(initSoftwareVersion)) != 0 )
					{
					    memcpy(initSoftwareVersion, msgRecv.sendInitConfig.softwareVersion, sizeof(initSoftwareVersion));
					}

					if (updateURL || updateID)
					{
					    retCode = rDiagCloudInit(updateURL, updateID);
					    if (retCode != GEN_SUCCESS && retCode != GEN_API_FAIL) {
					        CAB_DBG(CAB_GW_ERR, "rDiagCloudInit failed with code %d", retCode);
					        isCloudInitDone = false;
					        sendCmdToCabAppA7(SEND_BACK_INIT_CONFIG, NULL);
					        continue;
					    } else {
                            isCloudInitDone = true;

                            /*try sending zip files if proper cloud init was done in this case instad of CONNECTED state*/
                            time(&timeData);
                            retCode = reportEventToSDC(TRANSMIT_EV, (void *)&timeData, JSON_DATA);
                            if (GEN_SUCCESS != retCode)
                            {
                                CAB_DBG(CAB_GW_ERR, "Report event from remotediag failed with code %d", retCode);
                            }
                        }
					}
				}
				break;
				case WATCHDOG_TIMER_MSG:
				{
        				setRDiagResHandlerThreadAliveStatus(true);
				}
				break;
				case GW_ID_RES:
				{
					//store latest GW ID to global structure
					memcpy(initCloudConfig.gatewayID, msgRecv.sendInitConfig.gatewayId, sizeof(initCloudConfig.gatewayID));  	 
				}
				break;
				case LTE_DIAG:
				{
					pthread_mutex_lock(&lteDiagVarLock);
					memcpy(&g_lteDiag, &(msgRecv.sendLTEDebug), sizeof(lteDiagInfo_t));
					pthread_mutex_unlock(&lteDiagVarLock);
					if (strcmp(msgRecv.sendLTEDebug.operatorName, NO_SIM_PRESENT_STR) != 0) {
						if ((strcmp(msgRecv.sendLTEDebug.operatorName, NO_OPER_INFO_STR) != 0)) {
							CAB_DBG(CAB_GW_INFO, "######### Valid Operator %s #########", msgRecv.sendLTEDebug.operatorName);
							setLteDiagAvailCount(getLteDiagAvailCount() + 1);
							setLteDiagAvailSysRstCount(getLteDiagAvailSysRstCount() + 1);
						} else {
							CAB_DBG(CAB_GW_INFO, "######### inValid Operator %s #########", msgRecv.sendLTEDebug.operatorName);
						}
					} else {
						CAB_DBG(CAB_GW_DEBUG, "sim not present count :%d", getSimNotPresentCount());
						setSimNotPresentCount(getSimNotPresentCount()+1);
					}
				}
				break;
				case FACTORY_RESET_FROM_CABAPP:
				{
					deleteConfigAndDataStorage(DIR_PATH_DATA_RDIAG);
				}
				break;
				default:
				{
					CAB_DBG(CAB_GW_DEBUG, "default case");
				}
				break;
			}
		}
	}
	retCode = threadDeregister(newWatchdogNode.threadId);
        if(retCode != GEN_SUCCESS)     
        {
                CAB_DBG(CAB_GW_ERR, "Remote Diag Response handler thread de-registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "Remote Diag Response handler thread de-registration failed");
        }                     
	FUNC_EXIT
}

static bool getLteStateHandlerAliveStatus(void)
{
        bool status;
        pthread_mutex_lock(&lteStateHandlerThreadAliveLock);
        status = g_lteStateHandlerThreadAliveStatus;
        pthread_mutex_unlock(&lteStateHandlerThreadAliveLock);
        return status;
}

static void setLteStateHandlerAliveStatus(bool value)
{
        pthread_mutex_lock(&lteStateHandlerThreadAliveLock);
        g_lteStateHandlerThreadAliveStatus = value;
        pthread_mutex_unlock(&lteStateHandlerThreadAliveLock);
}

bool lteStateHandlerCallback(void)
{
        bool status = false;
        lteState_t lteStateData;
        struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0}; 
        FUNC_ENTRY

        clock_gettime(CLOCK_REALTIME, &abs_timeout);
        abs_timeout.tv_sec += MQ_SEND_LTE_STATUS_TIMEOUT;

        memset(&lteStateData, 0, sizeof(lteStateData));
        lteStateData.state = WATCHDOG_LTE_STATE;

        if(getLteStateHandlerAliveStatus() == true)
        {
                status = true;
        }

        setLteStateHandlerAliveStatus(false);

        if(mq_timedsend(mqLteStateQueue, (const char *)&lteStateData, sizeof(lteState_t), 0, &abs_timeout) < 0)
        {   
                CAB_DBG(CAB_GW_ERR, "mq timed send fail for LTE state message queue");
		status = false;
        }   

        return status;
}

static void *lteStateHandler(void *arg)
{
        ssize_t bytes_read;
        lteState_t lteStateData;
	returnCode_e retCode = GEN_SUCCESS;
	time_t timeData;
	watchDogTimer_t newWatchdogNode;

	FUNC_ENTRY

	newWatchdogNode.threadId = syscall(SYS_gettid);
	newWatchdogNode.CallbackFn = lteStateHandlerCallback;

	CAB_DBG(CAB_GW_INFO, "LTE state handler thread id : %ld", syscall(SYS_gettid));
	retCode = threadRegister(&newWatchdogNode);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "LTE state handler thread registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_REGISTRATION_FAIL, GW_ERR, "LTE state handler thread registration failed");
        }                     


        while(1)
        {
                memset(&lteStateData, 0, sizeof(lteStateData));
                bytes_read = mq_receive(mqLteStateQueue, (char *)&(lteStateData), sizeof(lteState_t), NULL);
                if(bytes_read >= 0)
                {
                        switch(lteStateData.state)
                        {
                                case CONNECTED_LTE_STATE:
					setLteState(true);
					// Set the Internet connectivity time stamp for installation status when LTE is connected
					time(&timeData);
					sendCmdToCabAppA7(INTERNET_CONNECTIVITY_TIMESTAMP_UPDATE, &timeData);

					if (isCloudInitDone == false)
					{
						retCode = rDiagCloudInit(false, false);
						if (retCode != GEN_SUCCESS)
						{
							CAB_DBG(CAB_GW_ERR, "rDiagCloudInit failed with code %d", retCode);
						} else {
                            CAB_DBG(CAB_GW_ERR, "rDiagCloudInit success");
                            isCloudInitDone = true;
                            time(&timeData);
                            retCode = reportEventToSDC(TRANSMIT_EV, (void *)&timeData, JSON_DATA);
                            if (GEN_SUCCESS != retCode)
                            {
                                CAB_DBG(CAB_GW_ERR, "Report event from remotediag failed with code %d", retCode);
                            }
                        }
					}
					break;
                                case DISCONNECTED_LTE_STATE:
					setLteState(false);
					retCode = sendLteCmd(CONNECT);
					if (retCode != GEN_SUCCESS)
					{
						CAB_DBG(CAB_GW_ERR, "sendLteCmd() for CONNECT request failed with code %d", retCode);
					}
                                        break;
				case WATCHDOG_LTE_STATE:
					{
						setLteStateHandlerAliveStatus(true);
					}
					break;
                                default:
                                        break;
                        }
        		CAB_DBG(CAB_GW_TRACE, "LTE status: %s", getLteState() ? "true" : "false" );
                }
        }
	retCode = threadDeregister(newWatchdogNode.threadId);
        if(retCode != GEN_SUCCESS)     
        {
                CAB_DBG(CAB_GW_ERR, "LTE state handler thread de-registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "LTE state handler thread de-registration failed");
        }                     
        FUNC_EXIT
}

static void sendEventToPidAndUsbCheck(pidMonitorEvent_e evType)
{
	pidMonitorStruct_t eventData;
	struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0};
	FUNC_ENTRY

	clock_gettime(CLOCK_REALTIME, &abs_timeout);
	abs_timeout.tv_sec += MQ_SEND_PID_MONITOR_TOUT_SEC;

	memset(&eventData, 0, sizeof(eventData));
	eventData.eventType = evType;
	mq_timedsend(pidMonitorQueue, (const char *)&eventData, sizeof(eventData), 0, &abs_timeout);
	FUNC_EXIT
}

void pidMonitorAndUsbPortCheckCB(uint32_t data)
{
	FUNC_ENTRY
	sendEventToPidAndUsbCheck(CHECK_PID_AND_USB_PORT_AVAIL);
	FUNC_EXIT
}

void LteConnectionCheckCB(uint32_t data)
{
	FUNC_ENTRY
	sendEventToPidAndUsbCheck(LTE_CONNECTIVITY_CHECK);
	FUNC_EXIT
}

static bool getTempMonitorThreadAliveStatus(void)
{
        bool status;
        pthread_mutex_lock(&tempMonitorThreadAliveLock);
        status = g_tempMonitorThreadAliveStatus;
        pthread_mutex_unlock(&tempMonitorThreadAliveLock);
        return status;
}

static void setTempMonitorThreadAliveStatus(bool value)
{
        pthread_mutex_lock(&tempMonitorThreadAliveLock);
        g_tempMonitorThreadAliveStatus = value;
        pthread_mutex_unlock(&tempMonitorThreadAliveLock);
}

bool temperatureMonitorThreadCB(void)
{
	bool status = false;
	CAB_DBG(CAB_GW_TRACE, "In func : %s", __func__);
	if (getTempMonitorThreadAliveStatus() == true)
	{
		status = true;
	}
	setTempMonitorThreadAliveStatus(false);

	return status;
}

static void *temperatureMonitor(void *data)
{
	returnCode_e retCode = GEN_SUCCESS;
	returnCode_e deregReturn = GEN_SUCCESS;
	temp_shutoff_record_data_t *temperatureData = (temp_shutoff_record_data_t *)data;
	watchDogTimer_t newWatchdogNode;

	newWatchdogNode.threadId = syscall(SYS_gettid);
	newWatchdogNode.CallbackFn = temperatureMonitorThreadCB;

	CAB_DBG(CAB_GW_INFO, "Temperature monitor mgr : %ld", syscall(SYS_gettid));
	retCode = threadRegister(&newWatchdogNode);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "Temperature monitor thread registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_REGISTRATION_FAIL, GW_ERR, "Temperature monitor thread registration failed");
        }

	CAB_DBG(CAB_GW_TRACE, "Temperature Monitor Thread Start");
	while (1) {
		retCode = measureSysTemp(temperatureData);
		setTempMonitorThreadAliveStatus(true);
		if (GEN_SUCCESS != retCode) {
			CAB_DBG(CAB_GW_ERR, "measureSysTemp API failed with error code %d from thread", retCode);
			Sem1_DeInit();
			pthread_exit(0);
			goto exit;
		}
		sleep(10);
	}

exit:	deregReturn = threadDeregister(newWatchdogNode.threadId);
        if(deregReturn != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "Temperature monitor thread de-registration failed with error %d", deregReturn);
                rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "Temperature monitor thread de-registration failed");
        }
	return retCode;
}

void waitFunc(int signum) 
{
	FUNC_ENTRY
	CAB_DBG(CAB_GW_DEBUG, "In waiting to complete the child process");
	wait(NULL);
	sendEventToPidAndUsbCheck(CHECK_PID_AND_USB_PORT_AVAIL);
	FUNC_EXIT
}

static uint32_t createNewProcess(const char *cmd)
{
	pid_t pid = -1;
	char *argv[2];
   	char cmd1[8192];

	FUNC_ENTRY

	if(cmd == NULL) {
		CAB_DBG(CAB_GW_ERR, "Null Pointer Error");
		FUNC_EXIT
		return GEN_NULL_POINTING_ERROR;
	}

	strcpy(cmd1,cmd);
	argv[0] = cmd1;
	argv[1] = NULL;

	if((pid = fork()) < 0) {
		CAB_DBG(CAB_GW_ERR, "%s application launching fail with error: %s", (char *)cmd, strerror(errno));
		FUNC_EXIT
		return -1;
	} else if(pid == 0) {
		if(execvp(cmd, argv) < 0) {
			CAB_DBG(CAB_GW_ERR, "%s application launching fail with error: %s", (char *)cmd, strerror(errno));
			FUNC_EXIT
			return -1;
		} else {
			pid = getpid();
		}
        } else {
		signal(SIGCHLD, waitFunc);
	}

	FUNC_EXIT
	return pid;
}

static bool getPidAndUsbPortMonitorThreadAliveStatus(void)
{
        bool status;
        pthread_mutex_lock(&pidAndUsbPortMonitorThreadAliveLock);
        status = g_pidAndUsbPortMonitorThreadAliveStatus;
        pthread_mutex_unlock(&pidAndUsbPortMonitorThreadAliveLock);
        return status;
}

static void setPidAndUsbPortMonitorThreadAliveStatus(bool value)
{
        pthread_mutex_lock(&pidAndUsbPortMonitorThreadAliveLock);
        g_pidAndUsbPortMonitorThreadAliveStatus = value;
        pthread_mutex_unlock(&pidAndUsbPortMonitorThreadAliveLock);
}

bool pidAndUsbPortMonitorCallback(void)
{
        bool status = false;

        if(getPidAndUsbPortMonitorThreadAliveStatus() == true)
        {
                status = true;
        }

        setPidAndUsbPortMonitorThreadAliveStatus(false);

	sendEventToPidAndUsbCheck(WATCHDOG_EVENT);

        return status;
}

static void *pidAndUsbPortMonitorManager(void *arg)
{
	pidMonitorStruct_t eventData;
	returnCode_e retCode = GEN_SUCCESS;
	uint8_t lteStat = 0;
	ssize_t bytes_read;
	watchDogTimer_t newWatchdogNode;
	static uint32_t internetCheckcount = 0;
	static uint32_t sysResetCheckcount = 0;

        FUNC_ENTRY

	newWatchdogNode.threadId = syscall(SYS_gettid);
	newWatchdogNode.CallbackFn = pidAndUsbPortMonitorCallback;

	CAB_DBG(CAB_GW_INFO, "PID monitor mgr : %ld", syscall(SYS_gettid));
	retCode = threadRegister(&newWatchdogNode);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "PID and USB port monitor thread registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_REGISTRATION_FAIL, GW_ERR, "PID and USB port monitor thread registration failed");
        }                     


	while(1)
	{
		memset(&eventData, 0, sizeof(eventData));
		bytes_read = mq_receive(pidMonitorQueue, (char *)&(eventData), sizeof(eventData), NULL);
		if(bytes_read >= 0)
		{
			switch(eventData.eventType)
			{
				case WATCHDOG_EVENT:
				{
					setPidAndUsbPortMonitorThreadAliveStatus(true);
					break;
				}
				case CHECK_PID_AND_USB_PORT_AVAIL:
				{
					uint32_t temp=0;
					checkSelfProcMem(&temp);
					if (temp > MAX_MEM_LEAK) {
						CAB_DBG(CAB_GW_ERR, "Memory leak in remote diag, rebooting system");
						printf("\033[1m###### Rebooting system ############\033[22m\r\n");
						rebootSystem();
					}

					/* Check for the lteApp pid */
					if(kill(lteAppPid, 0) != 0)
					{
						lteAppPid = createNewProcess("/cabApp/lteApp");
						if(lteAppPid < 0)
						{
							sendEventToPidAndUsbCheck(CHECK_PID_AND_USB_PORT_AVAIL);
						}
					}

					/* Check for the cabAppLauncher pid */
					if(kill(cabAppLauncherPid, 0) != 0)
					{
						cabAppLauncherPid = createNewProcess("/cabApp/cabAppLauncher");
						if(cabAppLauncherPid < 0)
						{
							sendEventToPidAndUsbCheck(CHECK_PID_AND_USB_PORT_AVAIL);
						}
					}

					/* Check for the USB port /dev/ttyUSB2 and /dev/ttyUSB3 */
					if( (access( EG91_USB_PORT, F_OK ) != 0) || (access( PPPD_USB_PORT, F_OK ) != 0) )
					{
						retCode = powerOffEG91();
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR,"powerOffEG91() failed with code %d", retCode);
							continue;
						}
						sleep(1);
						retCode = powerOnEG91();
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR,"powerOnEG91() failed with code %d", retCode);
							continue;
						}
					}
					break;
				}
				case LTE_CONNECTIVITY_CHECK:
				{
					CAB_DBG(CAB_GW_INFO, "Checking Cloud Connectivity : %d, %d, %d", sysResetCheckcount, internetCheckcount, getLteDiagAvailCount());
					CAB_DBG(CAB_GW_INFO, "Checking Cloud Connectivity : %d, %d, %d", LTE_MAX_SYS_RESET_CNT, LTE_MAX_INTERNET_CONN_FAIL_CNT, LTE_SW_RST_NW_AVAIL_THRESH);

					retCode = getLTEConnStat(&lteStat); 
					if (retCode == GEN_SUCCESS) {
						if (lteStat == 0) {
							internetCheckcount++;
							if (internetCheckcount >= LTE_MAX_INTERNET_CONN_FAIL_CNT) {
								if (getSimNotPresentCount() < SIM_NOT_PRESENT_CONFD_COUNT) {
									//CAB_DBG(CAB_GW_INFO, "###### Sending Switchover Req ############");
									//sendCmdToCabAppA7(SWOVER_REQ, NULL);
								} else {
									CAB_DBG(CAB_GW_INFO, "Switch skipped due to sim not present error");
								}
								setLteDiagAvailCount(0);
								internetCheckcount = 0;
							}
							sysResetCheckcount++;
							if (sysResetCheckcount >= LTE_MAX_SYS_RESET_CNT) {
								setLteDiagAvailSysRstCount(0);
								sysResetCheckcount = 0;
								CAB_DBG(CAB_GW_ERR, "No internet connectivity for 3 hours, rebooting system");
								printf("\033[1m###### Rebooting system ############\033[22m\r\n");
								rebootSystem();
							}
						} else {
							time_t timeData;
							CAB_DBG(CAB_GW_INFO, "############ LTE connected ############");
							/* This check is done to send any zip file created while the Internet connectivity
							 * was not available. */
							if (getCurrentZipCount() > 0) {
								time(&timeData);
								retCode = reportEventToSDC(TRANSMIT_EV, (void *)&timeData, JSON_DATA);
								if (GEN_SUCCESS != retCode) {
									CAB_DBG(CAB_GW_ERR, "Report event from remotediag failed with code %d", retCode);
								}
							}
							// Set the last Internet connectivity time stamp for installation status
							time(&timeData);
							sendCmdToCabAppA7(INTERNET_CONNECTIVITY_TIMESTAMP_UPDATE, &timeData);
							setLteDiagAvailCount(0);
							internetCheckcount = 0;
							setLteDiagAvailSysRstCount(0);
							sysResetCheckcount = 0;
						}
					} else {
						CAB_DBG(CAB_GW_ERR, "LTE Internet connection check failed with code %d", retCode);
					}
					break;
				}
				default:
					CAB_DBG(CAB_GW_ERR,"Invalid event type");
					break;
			}
		}
	}
	retCode = threadDeregister(newWatchdogNode.threadId);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "PID and USB port monitor thread de-registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "PID and USB port monitor thread de-registration failed");
        }                     
	FUNC_EXIT
}

void watchDogTimerCB(uint32_t data)
{
        FUNC_ENTRY
        watchDogTimer_t *localWatchDogTimerNode;
        bool status = true;

        CAB_DBG(CAB_GW_DEBUG, "WatchDog timer callback from Remote Diag App...");


		localWatchDogTimerNode = getWatchDogTimerHead();
                while(localWatchDogTimerNode != NULL)
                {
                		CAB_DBG(CAB_GW_DEBUG, "thread id from Remote Diag App is %ld...", localWatchDogTimerNode->threadId);
                        status = localWatchDogTimerNode->CallbackFn();
                        if(status == false)
                        {
				CAB_DBG(CAB_GW_ERR, "Reboot the system due to issue with thread id:%ld from Remote Diag App...", localWatchDogTimerNode->threadId);
				rebootSystem();
                        }
                        localWatchDogTimerNode = localWatchDogTimerNode->next;

                }

        CAB_DBG(CAB_GW_DEBUG, "Exiting WatchDog timer callback from Remote Diag App...");
        FUNC_EXIT
}

/******************************************************************
 *@brief  (This API is used to get the time stamp of current EOD)
 *
 *@param[IN]  None
 *@param[OUT] None
 *
 *@return time_t timestamp value
 *********************************************************************/
 static time_t getEODTimeStamp()
 {
    FUNC_ENTRY
    struct tm *date;
    time_t t = time(NULL);

    date = localtime(&t);
    date->tm_hour = 23;
    date->tm_min = 59;
    date->tm_sec = 59;
    time_t t1 = mktime(date);

    FUNC_EXIT
    return t1;
 }

 void syslogCollectTimerCB(uint32_t data)
 {
    FUNC_ENTRY
    CAB_DBG(CAB_GW_INFO, "Merging the SYSLOG from RAM");
    if (getAndMergeLogsFromRAM() == GEN_SUCCESS) {
        CAB_DBG(CAB_GW_INFO, "Merged the SYSLOG successfully");
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Merging the SYSLOG failed");
    }
    reloadTimer(syslogCollectTimerHandle, SYSLOG_COLLECT_TIMER_COUNT);
    FUNC_EXIT
}

void syslogMergeimerCB(uint32_t data)
{
    FUNC_ENTRY
    returnCode_e retCode = -1;

    CAB_DBG(CAB_GW_DEBUG, "Storing SYSLOG");
    // Reload syslog collector timer before as we are just going tp get all the logs from RAM
    reloadTimer(syslogCollectTimerHandle, SYSLOG_COLLECT_TIMER_COUNT);

    retCode = createFilePackage_Syslog(DAY_CHANGE);
    if (retCode != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "End of day time SYSlog package creation failed with code %d", retCode);
    }
    // Reload the 24 hrs timer for zipping, wait for the minimum 5 timer resolution to make sure the day is changed.
    sleep(TIMER_RESOLUTION_SEC*5);
    reloadTimer(syslogZipTimerHandle, (uint32_t) difftime(getEODTimeStamp(), time(NULL))/TIMER_RESOLUTION_SEC);
    CAB_DBG(CAB_GW_DEBUG, "SYSLOG stored");
    FUNC_EXIT
}

/* start timers for watching other process */
static returnCode_e startTimer(void)
{
	FUNC_ENTRY

	TIMER_STATUS_e timerRetSTat = TIMER_SUCCESS;
	TIMER_INFO_t isPidRunningTimerInfo = { \
			.count = PID_CHECK_COUNT, \
			.funcPtr = pidMonitorAndUsbPortCheckCB, \
			.data = 0xA4};

        TIMER_INFO_t watchDogTimerInfo = { \
                .count = WATCHDOG_TIMER_COUNT, \
                .funcPtr = watchDogTimerCB, \
                .data = 0xB1};

        TIMER_INFO_t syslogCollectTimerInfo = { \
                .count = SYSLOG_COLLECT_TIMER_COUNT, \
                .funcPtr = syslogCollectTimerCB, \
                .data = 0xB2};

        TIMER_INFO_t LteConnectionCheckTimerinfo = { \
        		.count = LTE_CONNECTIVITY_CHECK_COUNT, \
				.funcPtr = LteConnectionCheckCB, \
				.data = 0xC1};

        /* The timer resolution is 60 seconds, so the zip timer may overflow a minute earlier than exact day change */
        TIMER_INFO_t syslogZipTimerInfo = { \
                .count = (uint32_t) difftime(getEODTimeStamp(), time(NULL))/TIMER_RESOLUTION_SEC, \
                .funcPtr = syslogMergeimerCB, \
                .data = 0xC2};

	timerRetSTat = initSysTimer(TIMER_RESOLUTION_SEC);
	if (timerRetSTat != TIMER_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Init timer failed with timer module code : %d", timerRetSTat);
		FUNC_EXIT
		return MODULE_INIT_FAIL;
	}

	timerRetSTat = startSystemTimer(isPidRunningTimerInfo, &pidMonitorTimerHandle);
	if (timerRetSTat != TIMER_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Start system timer failed with timer module code : %d", timerRetSTat);
		FUNC_EXIT
		return MODULE_INIT_FAIL;
	}

	timerRetSTat = startSystemTimer(watchDogTimerInfo, &watchDogTimerHandle);
	if (timerRetSTat != TIMER_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Start watchdog timer failed with timer module code : %d", timerRetSTat);
		FUNC_EXIT
		return MODULE_INIT_FAIL;
	}

	timerRetSTat = startSystemTimer(syslogCollectTimerInfo, &syslogCollectTimerHandle);
	if (timerRetSTat != TIMER_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Start syslog timer failed with timer module code : %d", timerRetSTat);
		FUNC_EXIT
		return MODULE_INIT_FAIL;
	}

	timerRetSTat = startSystemTimer(syslogZipTimerInfo, &syslogZipTimerHandle);
	if (timerRetSTat != TIMER_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Start syslog timer failed with timer module code : %d", timerRetSTat);
		FUNC_EXIT
		return MODULE_INIT_FAIL;
	}

	timerRetSTat = startSystemTimer(LteConnectionCheckTimerinfo, &LteConnectionTimerHandler);
	if (timerRetSTat != TIMER_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Start LTE Internet connection timer failed with timer module code : %d", timerRetSTat);
		FUNC_EXIT
		return MODULE_INIT_FAIL;
	}

	return GEN_SUCCESS;
}


#if 0
//To do : need to complete this deinit sequence
int deInit(void)
{
	pthread_mutex_destroy(&lteStatusLock);
	pthread_cancel();
	powerOffEG91();

}
#endif


int main(void)
{
	returnCode_e retCode = GEN_SUCCESS;
	FUNC_ENTRY
	FILE *fp;
	int fd;
	temp_shutoff_record_data_t temperatureData;

	system("ulimit -q 10000000");
	system("fw_setenv bootcount 0");
	system("fw_setenv upgrade_available 0");

	memset(&g_lteDiag, '\0', sizeof(lteDiagInfo_t));
	setLteDiagAvailSysRstCount(0);
	setLteDiagAvailCount(0);

	retCode = initSysLogModule(MAX_SYSLOG_PACKAGE);
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "SYS log storage init failed with code %d", retCode);
	}

	retCode = createFilePackage_Syslog(INIT_TIME);
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Init time SYSlog package creation failed with code %d", retCode);
	}

	retCode = isThermalShutOffAvailable(&hwBit);
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Function isThermalShutOffAvailable() failed with code %d", retCode);
	}

	if(hwBit) {
		retCode = init_temp_modules(&temperatureData);
		if (GEN_SUCCESS != retCode) {
			CAB_DBG(CAB_GW_ERR, "Temperature Init Module Failed %d", retCode);
			return retCode;
		}
		retCode = accelerometerInit();
		if (GEN_SUCCESS != retCode) {
			CAB_DBG(CAB_GW_ERR, "Accel Init Failed %d", retCode);
			return retCode;
		}

		fd = access(CFG_TEMP_SHUTOFF_FILE, F_OK);
		if(fd == -1) { 
			CAB_DBG(CAB_GW_DEBUG, "Temp-ShutOFF File doesnot exist...Creating File");
			fp = fopen(CFG_TEMP_SHUTOFF_FILE, "a");
			if(fp == NULL) {
				CAB_DBG(CAB_GW_ERR, "File operation Failed %d", retCode);
				return retCode;
			}   
			fclose(fp);
			retCode = write_init_configparam();
			if (GEN_SUCCESS != retCode) {
				CAB_DBG(CAB_GW_ERR, "Init Configuration failed %d", retCode);
				return retCode;
			}
		}

		retCode = measureSysTemp(&temperatureData);
		if (GEN_SUCCESS != retCode) {
			CAB_DBG(CAB_GW_ERR, "measureSysTemp API failed %d", retCode);
			return retCode;
		}
	}

	retCode = powerOnEG91();
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "powerOnEG91() failed with code %d", retCode);
		FUNC_EXIT
		return retCode;
	}
        
        if((retCode = sem_unlink("lteSMS1")) < 0) {
		CAB_DBG(CAB_GW_ERR, "sem_unlink lteSMS fail with error: %d", retCode);
	}
	retCode = initEG91();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "initEG91() fail with error: %d", retCode);
		return retCode;
	}
        
        uint8_t isValid = 0;
	retCode = CheckXdataisValid(&isValid);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "CheckXdataisValid() fail with error: %d", retCode);
	}
        
	if(!isValid) {
		retCode = enableGPSOneXtra();
		if(retCode != GEN_SUCCESS)
		{
			CAB_DBG(CAB_GW_ERR, "enableGPSOneXtra() fail with error: %d", retCode);
		}
	}
        deInitEG91();

	retCode = signalRegister();
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "signal reg failed with code %d", retCode);
		FUNC_EXIT
		return retCode;
	}

	retCode = pthread_mutex_init(&lteStatusLock, NULL);
	if (retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "LTE Status mutex init failed");
		FUNC_EXIT
		return retCode;
	}

	retCode = pthread_mutex_init(&lteDiagVarLock, NULL);
	if (retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "LTE Diag var lock init failed");
		FUNC_EXIT
		return retCode;
	}

	retCode = pthread_mutex_init(&simNotPrVarLock, NULL);
	if (retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "Sim not present variable lock init failed");
		FUNC_EXIT
		return retCode;
	}

	retCode = startTimer();
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "startTimer() failed with code %d", retCode);
		FUNC_EXIT
		return retCode;
	}

	retCode = setupMsgQueue();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "setupMsgQueue failed with error %d", retCode);
		FUNC_EXIT
		return retCode;
	}

	lteAppPid = createNewProcess("/cabApp/lteApp");
	if(lteAppPid < 0)
	{
		sendEventToPidAndUsbCheck(CHECK_PID_AND_USB_PORT_AVAIL);
	}

	cabAppLauncherPid = createNewProcess("/cabApp/cabAppLauncher");
	if(cabAppLauncherPid < 0)
	{
		sendEventToPidAndUsbCheck(CHECK_PID_AND_USB_PORT_AVAIL);
	}


	retCode = pthread_create(&pidAndUsbPortMonitorThreadID_t, NULL, &pidAndUsbPortMonitorManager, NULL);
	if (retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "thread creation failed for PID monitoring with code %d", retCode);
		FUNC_EXIT
		return retCode;
	}

	retCode = pthread_create(&rDiagResHandlerThreadID_t, NULL, &rDiagResHandler, NULL);
	if (retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "thread creation failed for remote diag response handler with code %d", retCode);
		FUNC_EXIT
		return retCode;
	}

	retCode = pthread_create(&lteStateHandlerThreadID_t, NULL, &lteStateHandler, NULL);
	if (retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "thread creation failed for LTE state handler with code %d", retCode);
		FUNC_EXIT
		return retCode;
	}

	if(hwBit) {
		retCode = pthread_create(&tempMonitorThreadID_t, NULL, &temperatureMonitor, (void *)&temperatureData);
		if (retCode != GEN_SUCCESS)
		{
			CAB_DBG(CAB_GW_ERR, "thread creation failed for Temperature monitor with code %d", retCode);
			FUNC_EXIT
			return retCode;
		}
	}

	pthread_join(pidAndUsbPortMonitorThreadID_t, NULL);
	pthread_join(rDiagResHandlerThreadID_t, NULL);
	pthread_join(lteStateHandlerThreadID_t, NULL);
	if(hwBit) {
		pthread_join(tempMonitorThreadID_t, NULL);
	}

	retCode = cloudDeinit();
	if(retCode != GEN_SUCCESS)
	{
	    CAB_DBG(CAB_GW_ERR, "deinitCloud() fail with error %d", retCode);
	}
}
