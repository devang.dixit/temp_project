/***************************************************************
 * Copyright(c) <2018>, Volansys
 * Description
 * @file : bleUart_command_handlers.c
 * @brief : This file provides command handlers for BLE commands
 * @Author     - Volansys
 * History
 *
 * Sept/01/2020, VT , First draft
 **************************************************************/

#include "bleUart.h"
#include "cJSON.h"
#include "diagnostic.h"
#include "debug.h"

/** @brief CFG_GW_TPMS_UNPAIR_CMD command handler function
 *
 *      handler function for the CFG_GW_TPMS_UNPAIR_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_CFG_GW_TPMS_UNPAIR_CMD(void * data)
{
    uint32_t tpmsId;
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;

    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = convertJsonToSystemPayload(uartFrame->cmd, (char *)uartFrame->data, (void *)&tpmsId);
    if (retCode == GEN_SUCCESS) {
        retCode = setConfigModuleParam(uartFrame->cmd, (void *)&tpmsId);
    }
    sendCommandStatus(uartFrame->cmd, retCode);
}

/** @brief CFG_GW_TPMS_VENDOR_CMD command handler function
 *
 *      handler function for the CFG_GW_TPMS_VENDOR_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_CFG_GW_TPMS_VENDOR_CMD(void * data)
{
    char vendorName[64];
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;

    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = convertJsonToSystemPayload(uartFrame->cmd, (char *)uartFrame->data, (void *)&vendorName);
    if (retCode == GEN_SUCCESS)
        retCode = setConfigModuleParam(uartFrame->cmd, (void *)&vendorName);
    sendCommandStatus(uartFrame->cmd, retCode);
}

/** @brief CFG_GW_TARGET_PRESSURE_CMD command handler function
 *
 *      handler function for the CFG_GW_TARGET_PRESSURE_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_CFG_GW_TARGET_PRESSURE_CMD(void * data)
{
    targetPressureInfo_t targetPressureInfo;
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;

    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = convertJsonToSystemPayload(uartFrame->cmd, (char *)uartFrame->data, (void *)&targetPressureInfo);
    if (retCode == GEN_SUCCESS)
        retCode = setConfigModuleParam(uartFrame->cmd, (void *)&targetPressureInfo);
    sendCommandStatus(uartFrame->cmd, retCode);
}

/** @brief CFG_GW_BATTERY_CMD command handler function
 *
 *      handler function for the CFG_GW_BATTERY_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_CFG_GW_BATTERY_CMD(void * data)
{
    int lowBatteryThreshold;
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;

    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = convertJsonToSystemPayload(uartFrame->cmd, (char *)uartFrame->data, (void *)&lowBatteryThreshold);
    if (retCode == GEN_SUCCESS)
        retCode = setConfigModuleParam(uartFrame->cmd, (void *)&lowBatteryThreshold);
    sendCommandStatus(uartFrame->cmd, retCode);
}

/** @brief CFG_GW_TPMS_PAIR_CMD command handler function
 *
 *      handler function for the CFG_GW_TPMS_PAIR_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_CFG_GW_TPMS_PAIR_CMD(void * data)
{
    tpmsInfo_t tpmsInfo={0};
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;

    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = convertJsonToSystemPayload(uartFrame->cmd, (char *)uartFrame->data, &tpmsInfo);
    if (retCode == GEN_SUCCESS)
        retCode = setConfigModuleParam(uartFrame->cmd, (void *)&tpmsInfo);
    sendCommandStatus(uartFrame->cmd, retCode);
}

/** @brief CFG_GW_ATTACH_HALO_SN_CMD command handler function
 *
 *      handler function for the CFG_GW_ATTACH_HALO_SN_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_CFG_GW_ATTACH_HALO_SN_CMD(void * data)
{
    haloSNInfo_t haloSNInfo={0};
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = convertJsonToSystemPayload(uartFrame->cmd, (char *)uartFrame->data, &haloSNInfo);
    if (retCode == GEN_SUCCESS)
        retCode = setConfigModuleParam(uartFrame->cmd, (void *)&haloSNInfo);
    sendCommandStatus(uartFrame->cmd, retCode);
}

/** @brief CFG_GW_DETACH_HALO_SN_CMD command handler function
 *
 *      handler function for the CFG_GW_DETACH_HALO_SN_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_CFG_GW_DETACH_HALO_SN_CMD(void * data)
{
    haloSNInfo_t haloSNInfo={0};
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = convertJsonToSystemPayload(uartFrame->cmd, (char *)uartFrame->data, &haloSNInfo);
    if (retCode == GEN_SUCCESS)
        retCode = setConfigModuleParam(uartFrame->cmd, (void *)&haloSNInfo);
    sendCommandStatus(uartFrame->cmd, retCode);
}

/** @brief CFG_GW_FLUSH_TYRE_DETAILS_CMD command handler function
 *
 *      handler function for the CFG_GW_FLUSH_TYRE_DETAILS_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_CFG_GW_FLUSH_TYRE_DETAILS_CMD(void * data)
{
    tyrePosInfo_t tyrePosInfo={0};
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = convertJsonToSystemPayload(uartFrame->cmd, (char *)uartFrame->data, &tyrePosInfo);
    if (retCode == GEN_SUCCESS)
        retCode = setConfigModuleParam(uartFrame->cmd, (void *)&tyrePosInfo);
    sendCommandStatus(uartFrame->cmd, retCode);
}

/** @brief CFG_GW_RECORD_TREAD_DETAILS_CMD command handler function
 *
 *      handler function for the CFG_GW_RECORD_TREAD_DETAILS_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_CFG_GW_RECORD_TREAD_DETAILS_CMD(void * data)
{
    treadRecordInfo_t treadRecordInfo = {0};
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = convertJsonToSystemPayload(uartFrame->cmd, (char *)uartFrame->data, &treadRecordInfo);
    if (retCode == GEN_SUCCESS)
        retCode = setConfigModuleParam(uartFrame->cmd, (void *)&treadRecordInfo);
    sendCommandStatus(uartFrame->cmd, retCode);
}

/** @brief CFG_GW_SYSTEM_CMD command handler function
 *
 *      handler function for the CFG_GW_SYSTEM_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_CFG_GW_SYSTEM_CMD(void * data)
{
    gatewayInfo_t gatewayInfo;
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    memset(&gatewayInfo, '\0', sizeof(gatewayInfo_t));
    retCode = convertJsonToSystemPayload(uartFrame->cmd, (char *)uartFrame->data, (void *)&gatewayInfo); 
    if (retCode == GEN_SUCCESS)
        retCode = setConfigModuleParam(uartFrame->cmd, (void *)&gatewayInfo);
    sendCommandStatus(uartFrame->cmd, retCode);
}

/** @brief CFG_MOBILE_UTC_TIME_CMD command handler function
 *
 *      handler function for the CFG_MOBILE_UTC_TIME_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_CFG_MOBILE_UTC_TIME_CMD(void * data)
{
    time_t timestamp;
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = convertJsonToSystemPayload(uartFrame->cmd, (char *)uartFrame->data, (void *)&timestamp);
    if (retCode == GEN_SUCCESS)
        retCode = setConfigModuleParam(uartFrame->cmd, (void *)&timestamp);
    sendCommandStatus(uartFrame->cmd, retCode);
}

/** @brief CFG_BLE_PASSKEY_CMD command handler function
 *
 *      handler function for the CFG_BLE_PASSKEY_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_CFG_BLE_PASSKEY_CMD(void * data)
{
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = convertJsonToSystemPayload(uartFrame->cmd, (char *)uartFrame->data, uartFrame->data);
    if (retCode == GEN_SUCCESS) {
        UNUSED_VARIABLE(setConfigModuleParam(uartFrame->cmd, (void *)uartFrame->data));
        retCode = sendDataToBle(uartFrame->cmd, uartFrame->data);
    }
    sendCommandStatus(uartFrame->cmd, retCode);
}

/** @brief CFG_GW_OPT_CMD command handler function
 *
 *      handler function for the CFG_GW_OPT_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_CFG_GW_OPT_CMD(void * data)
{
    vehicleOptInfo_t vehicleOptionalInfo;
    returnCode_e retCode;
    memset(&vehicleOptionalInfo, '\0', sizeof(vehicleOptInfo_t));
    uartFrame_t *uartFileFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFileFrame = (uartFrame_t *) data;

    retCode = convertJsonToSystemPayload(uartFileFrame->subCommand, (char *)uartFileFrame->filePayload, (void *)&vehicleOptionalInfo);
    if (retCode == GEN_SUCCESS)
        retCode = setConfigModuleParam(uartFileFrame->subCommand, (void *)&vehicleOptionalInfo);

    sendCommandStatus(uartFileFrame->subCommand, retCode);
}

/** @brief CFG_GW_CLOUD_CMD command handler function
 *
 *      handler function for the CFG_GW_CLOUD_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_CFG_GW_CLOUD_CMD(void * data)
{
    uint8_t url[MAX_CLOUD_URL_LEN];
    returnCode_e retCode;
    uartFrame_t *uartFileFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFileFrame = (uartFrame_t *) data;

    retCode = convertJsonToSystemPayload(uartFileFrame->subCommand, (char *)uartFileFrame->filePayload, url);
    if (retCode == GEN_SUCCESS)
        retCode = setConfigModuleParam(uartFileFrame->subCommand, (void *)url);

    sendCommandStatus(uartFileFrame->subCommand, retCode);
}

/** @brief CFG_GW_UPDATE_TYRE_DETAILS_CMD command handler function
 *
 *      handler function for the CFG_GW_UPDATE_TYRE_DETAILS_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_CFG_GW_UPDATE_TYRE_DETAILS_CMD(void * data)
{
    tyreInfo_t tyreInfo={0};
    returnCode_e retCode;
    uartFrame_t *uartFileFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFileFrame = (uartFrame_t *) data;

    retCode = convertJsonToSystemPayload(uartFileFrame->subCommand, (char *)uartFileFrame->filePayload, &tyreInfo);
    if (retCode == GEN_SUCCESS)
        retCode = setConfigModuleParam(uartFileFrame->subCommand, (void *)&tyreInfo);

    sendCommandStatus(uartFileFrame->subCommand, retCode);
}

/** @brief GET_CFG_GW_ALL_HALO_SN_CMD command handler function
 *
 *      handler function for the GET_CFG_GW_ALL_HALO_SN_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_GET_CFG_GW_ALL_HALO_SN_CMD(void * data)
{
    attachedHaloSNInfo_t attachedHaloSNInfo={0};
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = getConfigForBle(uartFrame->cmd, (void *)&attachedHaloSNInfo);

    if (retCode != GEN_SUCCESS) {
        sendCommandStatus(uartFrame->cmd, retCode);
    } else {
        sendDataToBle(uartFrame->cmd, (void *)&attachedHaloSNInfo);
    }
}

/** @brief GET_CFG_GW_TYRE_DETAILS_CMD command handler function
 *
 *      handler function for the GET_CFG_GW_TYRE_DETAILS_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_GET_CFG_GW_TYRE_DETAILS_CMD(void * data)
{
    tyreInfo_t tyreInfo={0};
    tyrePosInfo_t tyrePosInfo={0};
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    /* First get the tire position */
    retCode = convertJsonToSystemPayload(uartFrame->cmd, (char *)uartFrame->data, &tyrePosInfo);
    if (retCode == GEN_SUCCESS) {
        tyreInfo.axleNum = tyrePosInfo.axleNum;
        tyreInfo.vehicleSide = tyrePosInfo.vehicleSide;
        tyreInfo.tyrePos = tyrePosInfo.tyrePos;
        retCode = getConfigForBle(uartFrame->cmd, (void *)&tyreInfo);
        if (retCode == GEN_SUCCESS)
            sendDataToBle(uartFrame->cmd, (void *)&tyreInfo);
    }
    if (retCode != GEN_SUCCESS)
        sendCommandStatus(uartFrame->cmd, retCode);
}

/** @brief GET_CFG_GW_TREAD_HISTORY_CMD command handler function
 *
 *      handler function for the GET_CFG_GW_TREAD_HISTORY_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_GET_CFG_GW_TREAD_HISTORY_CMD(void * data)
{
    configuredTreadRecordInfo_t configuredTreadRecordInfo = {0};
    tyrePosInfo_t tyrePosInfo={0};
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    /* First get the tire position */
    retCode = convertJsonToSystemPayload(uartFrame->cmd, (char *)uartFrame->data, &tyrePosInfo);
    if (retCode == GEN_SUCCESS) {
        memcpy(&(configuredTreadRecordInfo.tyrePosInfo), &tyrePosInfo, sizeof(tyrePosInfo));
        retCode = getConfigForBle(uartFrame->cmd, (void *)&configuredTreadRecordInfo);
        if (retCode == GEN_SUCCESS)
            sendDataToBle(uartFrame->cmd, (void *)&configuredTreadRecordInfo);
    }
    if (retCode != GEN_SUCCESS)
        sendCommandStatus(uartFrame->cmd, retCode);
}

/** @brief GET_CFG_GW_SYSTEM_CMD command handler function
 *
 *      handler function for the GET_CFG_GW_SYSTEM_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_GET_CFG_GW_SYSTEM_CMD(void * data)
{
    gatewayInfo_t gatewayInfo;
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = getConfigForBle(uartFrame->cmd, (void *)&gatewayInfo);
    if (retCode != GEN_SUCCESS) {
        sendCommandStatus(uartFrame->cmd, retCode);
    } else {
        sendDataToBle(uartFrame->cmd, (void *)&gatewayInfo);
    }
}

/** @brief GET_CFG_GW_OPT_CMD command handler function
 *
 *      handler function for the GET_CFG_GW_OPT_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_GET_CFG_GW_OPT_CMD(void * data)
{
    vehicleOptInfo_t vehicleOptionalInfo;
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = getConfigForBle(uartFrame->cmd, (void *)&vehicleOptionalInfo);
    if (retCode != GEN_SUCCESS) {
        sendCommandStatus(uartFrame->cmd, retCode);
    } else {
        sendDataToBle(uartFrame->cmd, (void *)&vehicleOptionalInfo);
    }
}

/** @brief GET_CFG_GW_TPMS_VENDOR_CMD command handler function
 *
 *      handler function for the GET_CFG_GW_TPMS_VENDOR_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_GET_CFG_GW_TPMS_VENDOR_CMD(void * data)
{
    char vendorName[64];
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = getConfigForBle(uartFrame->cmd, (void *)&vendorName);
    if (retCode != GEN_SUCCESS) {
        sendCommandStatus(uartFrame->cmd, retCode);
    } else {
        sendDataToBle(uartFrame->cmd, (void *)&vendorName);
    }
}

/** @brief GET_CFG_GW_CLOUD_CMD command handler function
 *
 *      handler function for the GET_CFG_GW_CLOUD_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_GET_CFG_GW_CLOUD_CMD(void * data)
{
    char cloudUrl[2048];
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = getConfigForBle(uartFrame->cmd, (void *)&cloudUrl);
    if (retCode != GEN_SUCCESS) {
        sendCommandStatus(uartFrame->cmd, retCode);
    } else {
        sendDataToBle(uartFrame->cmd, (void *)&cloudUrl);
    }
}

/** @brief GET_CFG_GW_TARGET_PRESSURE_CMD command handler function
 *
 *      handler function for the GET_CFG_GW_TARGET_PRESSURE_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_GET_CFG_GW_TARGET_PRESSURE_CMD(void * data)
{
    targetPressureInfo_t targetPressureInfo;
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = getConfigForBle(uartFrame->cmd, (void *)&targetPressureInfo);
    if (retCode != GEN_SUCCESS) {
        sendCommandStatus(uartFrame->cmd, retCode);
    } else {
        sendDataToBle(uartFrame->cmd, (void *)&targetPressureInfo);
    }
}

/** @brief GET_CFG_GW_BATTERY_CMD command handler function
 *
 *      handler function for the GET_CFG_GW_BATTERY_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_GET_CFG_GW_BATTERY_CMD(void * data)
{
    uint8_t lowBatteryThreshold;
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = getConfigForBle(uartFrame->cmd, (void *)&lowBatteryThreshold);
    if (retCode != GEN_SUCCESS) {
        sendCommandStatus(uartFrame->cmd, retCode);
    } else {
        sendDataToBle(uartFrame->cmd, (void *)&lowBatteryThreshold);
    }
}

/** @brief DIAG_GW_INFO_CMD command handler function
 *
 *      handler function for the DIAG_GW_INFO_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_DIAG_GW_INFO_CMD(void * data)
{
    sysDiagInfo_t  sysDiagInfo;
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = getDiagInfo(uartFrame->cmd, (void *)&sysDiagInfo);
    if (retCode != GEN_SUCCESS) {
        sendCommandStatus(uartFrame->cmd, retCode);
    } else {
        sendDataToBle(uartFrame->cmd, (void *)&sysDiagInfo);
    }
}

/** @brief DIAG_GW_PAIRED_SENSOR_CMD command handler function
 *
 *      handler function for the DIAG_GW_PAIRED_SENSOR_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_DIAG_GW_PAIRED_SENSOR_CMD(void * data)
{
    tpmsPairedDiagInfo_t tpmsPairedDiagInfo;
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = getDiagInfo(uartFrame->cmd, (void *)&tpmsPairedDiagInfo);

    if (retCode != GEN_SUCCESS) {
        sendCommandStatus(uartFrame->cmd, retCode);
    } else {
        sendDataToBle(uartFrame->cmd, (void *)&tpmsPairedDiagInfo);
    }
}

/** @brief GET_CFG_GW_TPMS_PAIR_CMD command handler function
 *
 *      handler function for the GET_CFG_GW_TPMS_PAIR_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_GET_CFG_GW_TPMS_PAIR_CMD(void * data)
{
    tpmsPairedDiagInfo_t tpmsPairedDiagInfo;
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    bool with_pn_mn = false;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = convertJsonToSystemPayload(uartFrame->cmd, (char *)uartFrame->data, &with_pn_mn);
    if (retCode == GEN_SUCCESS) {

        retCode = getConfigForBle(uartFrame->cmd, (void *)&tpmsPairedDiagInfo);

        if (retCode == GEN_SUCCESS) {
            sendDataToBle(uartFrame->cmd, (void *)&tpmsPairedDiagInfo);
        }
    }

    if (retCode != GEN_SUCCESS)
        sendCommandStatus(uartFrame->cmd, retCode);
}

/** @brief DIAG_GW_GPS_INFO_CMD command handler function
 *
 *      handler function for the DIAG_GW_GPS_INFO_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_DIAG_GW_GPS_INFO_CMD(void * data)
{
    gpsDiagInfo_t  gpsDiagInfo;
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = getDiagInfo(uartFrame->cmd, (void *)&gpsDiagInfo);
    if (retCode != GEN_SUCCESS) {
        sendCommandStatus(uartFrame->cmd, retCode);
    } else {
        sendDataToBle(uartFrame->cmd, (void *)&gpsDiagInfo);
    }
}

/** @brief DIAG_GW_CELLULAR_INFO_CMD command handler function
 *
 *      handler function for the DIAG_GW_CELLULAR_INFO_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_DIAG_GW_CELLULAR_INFO_CMD(void * data)
{
    lteDiagInfo_t lteDiagInfo;
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = getDiagInfo(uartFrame->cmd, (void *)&lteDiagInfo);
    if (retCode != GEN_SUCCESS) {
        sendCommandStatus(uartFrame->cmd, retCode);
    } else {
        sendDataToBle(uartFrame->cmd, (void *)&lteDiagInfo);
    }
}

/** @brief DIAG_GW_TPMS_DATA_CMD command handler function
 *
 *      handler function for the DIAG_GW_TPMS_DATA_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_DIAG_GW_TPMS_DATA_CMD(void * data)
{
    tpmsDiagDataInfo_t tpmsDiagDataInfo;
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = getDiagInfo(uartFrame->cmd, (void *)&tpmsDiagDataInfo);
    if (retCode != GEN_SUCCESS) {
        sendCommandStatus(uartFrame->cmd, retCode);
    } else {
        sendDataToBle(uartFrame->cmd, (void *)&tpmsDiagDataInfo);
    }
}

/** @brief DIAG_GW_TPMS_BY_ID_CMD command handler function
 *
 *      handler function for the DIAG_GW_TPMS_BY_ID_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_DIAG_GW_TPMS_BY_ID_CMD(void * data)
{
    tpmsDMData_t tpmsDiagDataInfo;
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = convertJsonToSystemPayload(uartFrame->cmd, (char *)uartFrame->data, &tpmsDiagDataInfo.tpmsData.sensorID);
    if (retCode == GEN_SUCCESS) {
        retCode = getDiagInfo(uartFrame->cmd, (void *)&tpmsDiagDataInfo);
        if (retCode == GEN_SUCCESS)
            sendDataToBle(uartFrame->cmd, (void *)&tpmsDiagDataInfo);
    }
    if (retCode != GEN_SUCCESS)
        sendCommandStatus(uartFrame->cmd, retCode);
}

/** @brief DIAG_GW_BATTERY_EV_LOG_CMD command handler function
 *
 *      handler function for the DIAG_GW_BATTERY_EV_LOG_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_DIAG_GW_BATTERY_EV_LOG_CMD(void * data)
{
    batteryDiagInfo_t batteryDiagInfo;
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = getDiagInfo(uartFrame->cmd, (void *)&batteryDiagInfo);
    if (retCode != GEN_SUCCESS) {
        sendCommandStatus(uartFrame->cmd, retCode);
    } else {
        sendDataToBle(uartFrame->cmd, (void *)&batteryDiagInfo);
    }
}

/** @brief SELF_DIAG_GW_CMD command handler function
 *
 *      handler function for the SELF_DIAG_GW_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_SELF_DIAG_GW_CMD(void * data)
{
    selfDiagStatus_t selfDiagTestResult;
    selfDiagReq_t selfDiagReq;
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = convertJsonToSystemPayload(uartFrame->cmd, (char *)uartFrame->data, (void *)&selfDiagReq);
    if (retCode != GEN_SUCCESS) {
        sendCommandStatus(uartFrame->cmd, retCode);
    } else {
        retCode = setConfigModuleParam(SELF_DIAG_GW_REQ_CMD, (void *)&selfDiagReq);
        if (retCode != GEN_SUCCESS) {
            sendCommandStatus(uartFrame->cmd, retCode);
        } else {
            sleep(1);
            retCode = getConfigModuleParam(SELF_DIAG_GW_RESP_CMD, (void *)&selfDiagTestResult);
            if(retCode != GEN_SUCCESS) {
                sendCommandStatus(uartFrame->cmd, retCode);
            } else {
                sendDataToBle(uartFrame->cmd, (void *)&selfDiagTestResult);
            }
        }
    }
}

/** @brief INSTALLATION_STATUS_CMD command handler function
 *
 *      handler function for the INSTALLATION_STATUS_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_INSTALLATION_STATUS_CMD(void * data)
{
    cabGatewayStatus_t installationStatus;
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;

    retCode = getConfigModuleParam(INSTALLATION_STATUS_CMD, (void *)&installationStatus);
    if (retCode != GEN_SUCCESS) {
        sendCommandStatus(uartFrame->cmd, retCode);
    } else {
        retCode = sendDataToBle(uartFrame->cmd, (void *)&installationStatus);
        if (retCode  != GEN_SUCCESS)
        {
            CAB_DBG(CAB_GW_ERR,"Failed to send the data to BLE with error %d", retCode);
        }
    }
}

/** @brief SYS_GW_RESET_CMD command handler function
 *
 *      handler function for the SYS_GW_RESET_CMD BLE command
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_SYS_GW_RESET_CMD(void * data)
{
    char param[MAX_RESET_CMD_LEN] = {'\0'};
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    
    if (!data)
        return;

    uartFrame = (uartFrame_t *) data;
    retCode = convertJsonToSystemPayload(uartFrame->cmd, (char *)uartFrame->data, param);
    if (retCode == GEN_SUCCESS) { 
        retCode = setConfigModuleParam(uartFrame->cmd, param);
    }
    sendCommandStatus(uartFrame->cmd, retCode);
    CAB_DBG(CAB_GW_TRACE,"Send Command Status for %d with retCode %d\n", uartFrame->cmd, retCode);
}
