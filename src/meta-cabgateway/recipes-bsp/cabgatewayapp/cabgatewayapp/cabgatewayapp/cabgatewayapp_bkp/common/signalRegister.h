/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file signalRegister.h
 * @brief Header file for signal registration
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Sep/18/2018  VT Added first draft
***************************************************************/

#include <stdint.h>
#include "sys/signal.h"
#include "error.h"
#include "watchdog.h"
#include "syscall.h"

#define DEBUG_FS_FILE       "/sys/kernel/debug/signalconfpid"
#define DISABLE_KEY_FILE	"/sys/class/input/input1/device/disabled_keys"

#define BATT_SIG 52
#define SUB1_SIG 51
#define TIMER_SIG SIGRTMIN
#ifdef ACCEL_SUPPORT
#define SIG_ACC_INT1            53
#define SIG_ACC_INT2            54
#endif



returnCode_e signalRegister(void);
returnCode_e signalDeRegister(void);
