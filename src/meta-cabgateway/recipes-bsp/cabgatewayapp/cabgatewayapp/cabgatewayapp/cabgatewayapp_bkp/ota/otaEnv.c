#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <dirent.h>
#include "otaEnv.h"

/****************************************
 ******** STATIC VARIABLES **************
 ****************************************/
static pthread_mutex_t g_otaEnvFileLock = PTHREAD_MUTEX_INITIALIZER;


void printOtaEnv(otaEnv_t *otaEnv, uint8_t fresh_read)
{
	if(fresh_read){
		readOTAEnv(otaEnv);
	}
	CAB_DBG(CAB_GW_INFO,"\n******* OTA ENV **********");
	CAB_DBG(CAB_GW_INFO,"***update_available = %d",otaEnv->update_available);
	CAB_DBG(CAB_GW_INFO,"***backup_available = %d",otaEnv->backup_available);
	CAB_DBG(CAB_GW_INFO,"***zb_updated = %d",otaEnv->zb_updated);
	CAB_DBG(CAB_GW_INFO,"***ble_updated = %d",otaEnv->ble_updated);
	CAB_DBG(CAB_GW_INFO,"***can_updated = %d",otaEnv->can_updated);
	CAB_DBG(CAB_GW_INFO,"***rf_updated = %d",otaEnv->rf_updated);
	CAB_DBG(CAB_GW_INFO,"***ebyte_updated = %d",otaEnv->ebyte_updated);
	CAB_DBG(CAB_GW_INFO,"***cabApp_updated = %d",otaEnv->cabApp_updated);
	CAB_DBG(CAB_GW_INFO,"***parentApp_updated = %d",otaEnv->parentApp_updated);
	CAB_DBG(CAB_GW_INFO,"***remoteDiagApp_updated = %d",otaEnv->remoteDiagApp_updated);
	CAB_DBG(CAB_GW_INFO,"***lteApp_updated = %d",otaEnv->lteApp_updated);
	CAB_DBG(CAB_GW_INFO,"***boot_count = %d",otaEnv->boot_count);
	CAB_DBG(CAB_GW_INFO,"***full_update = %d",otaEnv->full_update);
}

returnCode_e readOTAEnv(otaEnv_t *otaEnv)
{
	FUNC_ENTRY

	int envFile;
	uint32_t ret;

	/*Input argument validation*/
	if(otaEnv == NULL) {
		CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
		return GEN_NULL_POINTING_ERROR;
	}

	//opening event summary file for read
	pthread_mutex_lock(&g_otaEnvFileLock);
	envFile = open(OTA_ENV_FILE_PATH, O_RDONLY, 0644);
	if(envFile == -1) {
		CAB_DBG(CAB_GW_ERR, "%s file opening error.", OTA_ENV_FILE_PATH);
		pthread_mutex_unlock(&g_otaEnvFileLock);
		return FILE_OPEN_FAIL;
	}

	CAB_DBG(CAB_GW_DEBUG, "reading OTA environment file");
	//reading event summary structure data from file
	ret = read(envFile, otaEnv, sizeof(otaEnv_t));
	if(ret == -1) {
		CAB_DBG(CAB_GW_ERR, "%s file reading error.", OTA_ENV_FILE_PATH);
		close(envFile);
		pthread_mutex_unlock(&g_otaEnvFileLock);
		return FILE_READ_ERROR;
	}
	//close event summary file
	close(envFile);
	pthread_mutex_unlock(&g_otaEnvFileLock);

	FUNC_EXIT
	return GEN_SUCCESS;
}

returnCode_e writeOTAEnv(otaEnv_t *otaEnv)
{
	FUNC_ENTRY

	int outFile;                               //file descriptor
	uint32_t ret;

	/*Input argument validation*/
	if(otaEnv == NULL) {
		CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
		return GEN_NULL_POINTING_ERROR;
	}

	//opening event summary file for write
	pthread_mutex_lock(&g_otaEnvFileLock);
	outFile = open(OTA_ENV_FILE_PATH, O_WRONLY | O_CREAT, 0644);
	if(outFile == -1) {
		CAB_DBG(CAB_GW_ERR, "%s file opening error.", OTA_ENV_FILE_PATH);
		pthread_mutex_unlock(&g_otaEnvFileLock);
		return FILE_OPEN_FAIL;
	}

	CAB_DBG(CAB_GW_DEBUG, "writing OTA environment file");
	//writing event summery structure data to file
	ret = write(outFile, otaEnv, sizeof(otaEnv_t));
	if(ret == -1) {
		CAB_DBG(CAB_GW_ERR, "%s file reading error.", OTA_ENV_FILE_PATH);
		close(outFile);
		pthread_mutex_unlock(&g_otaEnvFileLock);
		return FILE_WRITE_ERROR;
	}

	fsync(outFile);
	//close event summary file
	close(outFile);
	pthread_mutex_unlock(&g_otaEnvFileLock);

	FUNC_EXIT
	return GEN_SUCCESS;
}


returnCode_e incrementBootCount(uint8_t *limitCrossed)
{
	FUNC_ENTRY

	returnCode_e retCode;
	otaEnv_t otaEnv;
        FILE * filePtr;
        char* status = GEN_SUCCESS;
	*limitCrossed = 0;
        char softwareversion[5+MAX_LEN_SOFTWARE_VERSION] = {0};
        char vfilename[20+MAX_LEN_SOFTWARE_VERSION] = {0};
        int len;
		
	retCode = readOTAEnv(&otaEnv);
	if(retCode != GEN_SUCCESS){
		CAB_DBG(CAB_GW_ERR, "Error reading OTA Env file");
		FUNC_EXIT
		return retCode;
	}

        filePtr = fopen(FILE_PATH_SOFTWARE_VERSION, "r");
        if(filePtr == NULL) {
            CAB_DBG(CAB_GW_ERR, "Opening software version file failed");
        }

        status = fgets(softwareversion, MAX_LEN_SOFTWARE_VERSION+4, filePtr);
        if(status == NULL) {
            CAB_DBG(CAB_GW_ERR, "%s Reading software version file failed", FILE_PATH_SOFTWARE_VERSION);
            fclose(filePtr);
        }
        //close file
        fclose(filePtr);

        len = strlen(softwareversion);
        if(softwareversion[len - 1] == '\n') {
                    softwareversion[len - 1] = 0;
        }


	sprintf(vfilename, "/cabApp/v%s", softwareversion);
        /* Check if the file representing the current version is present or not,
           Indicating that CabAppA7 has booted once,
           if present return without incrementing the boot count.*/
	if(access((char const*)vfilename, F_OK) == 0)
        {
           CAB_DBG(CAB_GW_INFO, "Returning without Incrementing the boot count");
           return GEN_SUCCESS;
        }

	otaEnv.boot_count++;
	if(otaEnv.boot_count >= RECOVERY_COUNT_LIMIMT) 
		*limitCrossed = 1;
	retCode = writeOTAEnv(&otaEnv);
        if(retCode != GEN_SUCCESS){
                CAB_DBG(CAB_GW_ERR, "Error writing OTA Env file");
		FUNC_EXIT
                return retCode;
        }
	FUNC_EXIT
	return GEN_SUCCESS;
}

returnCode_e setOtaEnvMemberValue(otaEnvInfo_e otaEnvInfo, uint8_t value)
{
	FUNC_ENTRY
        returnCode_e retCode;
        otaEnv_t otaEnv;

        retCode = readOTAEnv(&otaEnv);
        if(retCode != GEN_SUCCESS){
                CAB_DBG(CAB_GW_ERR, "Error reading OTA Env file");
                FUNC_EXIT
                return retCode;
        }
	switch(otaEnvInfo){
		case OTA_ZIGBEE:
			otaEnv.zb_updated = value;
			break;	
		case OTA_BLE:
			otaEnv.ble_updated = value;
			break;	
		case OTA_CAN:
			otaEnv.can_updated = value;
			break;	
		case OTA_RF:
			otaEnv.rf_updated = value;
			break;	
        case OTA_EBYTE:
            otaEnv.ebyte_updated = value;
            break;
		case OTA_CABAPP:
			otaEnv.cabApp_updated = value;
			break;	
		case OTA_PARENTAPP:
			otaEnv.parentApp_updated = value;
			break;	
        case OTA_REMOTEDIAGAPP:
            otaEnv.remoteDiagApp_updated = value;
            break;
        case OTA_LTEAPP:
            otaEnv.lteApp_updated = value;
            break;
		case OTA_BOOT_COUNT:
			otaEnv.boot_count = value;
			break;	
		case OTA_FULL_UPDATE:
			otaEnv.full_update = value;
			break;	
		case OTA_UPDATE_AVAILABLE:
			otaEnv.update_available = value;
			break;	
		case OTA_BACKUP_AVAILABLE:
			otaEnv.backup_available = value;
			break;	
		default:	
			CAB_DBG(CAB_GW_ERR, "Invalid OTA enviroment member(%d)",otaEnvInfo);
			FUNC_EXIT
			return DATA_INVALID;
	}
        retCode = writeOTAEnv(&otaEnv);
        if(retCode != GEN_SUCCESS){
                CAB_DBG(CAB_GW_ERR, "Error writing OTA Env file");
                FUNC_EXIT
                return retCode;
        }
	FUNC_EXIT
	return retCode;
}

returnCode_e resetBootCount(void)
{
	return setOtaEnvMemberValue(OTA_BOOT_COUNT,0);
}

returnCode_e setOtaUpdateType(otaType_e otaType)
{
	return setOtaEnvMemberValue(OTA_FULL_UPDATE,otaType);
}

returnCode_e setOtaEnvUpdateAvail(uint8_t update_avail)
{
	return setOtaEnvMemberValue(OTA_UPDATE_AVAILABLE,update_avail);
}
returnCode_e initOTAEnv(void)
{
	FUNC_ENTRY
	returnCode_e retCode;
	otaEnv_t otaEnv;
    struct stat st = {0};

    CAB_DBG(CAB_GW_INFO, "Initializing OTA environment");

    /* To check for the ota directory and if it not exist then create it */
    if(stat(OTA_ENV_DIR_PATH, &st) == -1) {
        if((mkdir(OTA_ENV_DIR_PATH, 0755)) == -1) {
            CAB_DBG(CAB_GW_ERR, "OTA directory not created");
            return DIR_CREATE_FAIL;
        }
    }

    if(access(OTA_ENV_FILE_PATH, F_OK) != 0) {
        memset((void*)(&otaEnv), 0, sizeof(otaEnv_t));
        otaEnv.full_update = 1;
        retCode = writeOTAEnv(&otaEnv);
        if(retCode != GEN_SUCCESS) {
            CAB_DBG(CAB_GW_ERR, "Error writing OTA Env file");
            FUNC_EXIT
            return retCode;
        }
    }
    /* This else condition will be used to write the additional parameters in the structure with
        initialized as 0. */
    else {
        memset((void*)(&otaEnv), 0, sizeof(otaEnv_t));

        /* First read the existing structure from the env file with old values */
        retCode = readOTAEnv(&otaEnv);
        if (retCode != GEN_SUCCESS) {
            CAB_DBG(CAB_GW_ERR, "Error reading OTA Env file");
            FUNC_EXIT
            return retCode;
        }

        /* Update the env file by writing the new structure */
        retCode = writeOTAEnv(&otaEnv);
        if(retCode != GEN_SUCCESS){
            CAB_DBG(CAB_GW_ERR, "Error writing OTA Env file");
            FUNC_EXIT
            return retCode;
        }
    }
    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e deinitOTAEnv(void)
{
        FUNC_ENTRY
        returnCode_e retCode = GEN_SUCCESS;

        pthread_mutex_destroy(&g_otaEnvFileLock);

        FUNC_EXIT
        return retCode;
}

