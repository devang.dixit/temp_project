#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "linkedList.h"
#include "debug.h"
#include "error.h"
#include "LteModule.h"

typedef struct {
	uint8_t opState;
	char longAlphaNum[30]; 
	char shortAlphaNum[20];
	uint8_t numericCode[8]; /* max 8 BCD digits. */
	uint8_t accessType;
} lteOpsDetails_t;

typedef struct {
	uint8_t triedState;
	uint8_t accessType;
	uint32_t supportedNumCode;
} supportedOpsDetails_t;

#define SIZE_OF_ARRAY(x) sizeof(x)/sizeof(x[0])

static uint32_t supportedOpNumCodes[] = {310030, 310150, 310170, 310280, 310380, 310410, 31016, 310160, 310200, 31024, \
					310240, 31025, 310250, 31026, 310260, 31027, 310270, 31031, 310310, 31049, 310490, 310120, \
					365840, \
					344920, \
					722310, \
					7227, \
					3632, \
					342600, \
					70267, \
					7361, \
					72402, 72403, 72404, \
					72406, 72410, 72411, 72423, \
					7245, \
					348170, \
					302500, 302520, \
					30237, 302370, 30272, 302720, \
					30278, 302780, \
					302610, \
					302220, \
					346140, \
					73002, 73007, \
					73009, \
					73003, \
					73001, \
					732101, \
					732123, \
					71204, \
					71203, \
					366110, \
					37002, \
					37004, \
					37001, 37003, \
					74000, \
					74001, \
					70601, \
					70604, \
					34001, \
					34020, \
					352110, \
					70403, \
					70401, \
					73801, \
					37203, \
					70802, \
					708001, 708040, \
					33805, 338050, \
					338180, \
					33403, 334030, \
					33420, \
					354860, \
					71021, 71073, \
					710300, \
					7143, \
					71420, \
					7144, \
					7442, \
					71610, \
					71615, \
					330110, \
					356110, \
					358110, \
					360110, \
					74603, \
					374130, \
					376350, \
					74801, \
					73401, 73402, 73403 \
					};


static uint8_t supportedAccessTypes[] = {0, 2, 3, 4, 5, 6, 7};
static supportedOpsDetails_t supportedOps[SIZE_OF_ARRAY(supportedOpNumCodes)*SIZE_OF_ARRAY(supportedAccessTypes)];

char *multi_tok(char *input, char *delimiter) {
	static char *string;
	if (input != NULL)
		string = input;

	if (string == NULL)
		return string;

	char *end = strstr(string, delimiter);
	if (end == NULL) {
		char *temp = string;
		string = NULL;
		return temp;
	}

	char *temp = string;

	*end = '\0';
	string = end + strlen(delimiter);
	return temp;
}

uint8_t parseAndAddNode(linkedList_t *tempNode, char *operString, uint32_t operatorCount)
{
	uint8_t scanCount = 0;
	char delim4[2]=",";
	char *outtok1 = NULL;

	if (tempNode == NULL || operString == NULL) {
		return 1;
	}
	
	memset(tempNode, '\0', sizeof(linkedList_t));
	tempNode->nodeId = operatorCount;
	outtok1 = strtok(operString, delim4);
	//printf("\r\n");
	//printf("##%s##",outtok1);
	sscanf(outtok1, "%d", (uint8_t *)&(tempNode->opState));
	//printf("::%d::", tempNode->opState);
	while ( (outtok1=strtok(NULL,delim4)) != NULL) {
		//printf("##%s##",outtok1);
		switch(scanCount) {
			case 0:
				sscanf(outtok1, "\"%[^s\"]", (char *)(tempNode->longAlphaNum));
				//printf("::%s::", tempNode->longAlphaNum);
				break;
			case 1:
				sscanf(outtok1, "\"%[^s\"]", (char *)(tempNode->shortAlphaNum));
				//printf("::%s::", tempNode->shortAlphaNum);
				break;
			case 2:
				sscanf(outtok1, "\"%d\"", (uint32_t *)&(tempNode->numericCode));
				//printf("::%d::", tempNode->numericCode);
				break;
			case 3:
				sscanf(outtok1, "%d", (uint8_t *)&(tempNode->accessType));
				//printf("::%d::", tempNode->accessType);
				break;
			default:
				break;
		}
		scanCount++;
	}
	if (scanCount >= 3) {
		addNode(tempNode);
	} else {
		CAB_DBG(CAB_GW_ERR, "Not a valid node");
		return 1;
	}
	return 0;
}

void resetSupportedOpsState()
{
	uint32_t nodeIndex=0, nodeIndex2=0;
	uint32_t accessTypeIndex=0;
	for (nodeIndex=0; nodeIndex<SIZE_OF_ARRAY(supportedOpNumCodes); nodeIndex++) {
		for (accessTypeIndex = 0; accessTypeIndex<SIZE_OF_ARRAY(supportedAccessTypes); accessTypeIndex++) {
			supportedOps[nodeIndex2].supportedNumCode = supportedOpNumCodes[nodeIndex];
			supportedOps[nodeIndex2].accessType = supportedAccessTypes[accessTypeIndex];
			supportedOps[nodeIndex2].triedState = 0;
			//printf("Supported op : %d, %d, %d\r\n", supportedOps[nodeIndex2].supportedNumCode, 
			//		supportedOps[nodeIndex2].accessType, 
		      	//		supportedOps[nodeIndex2].triedState );
			nodeIndex2++;
		}
	}
}

void swOver(char *atCmdString) {
	char delim[5]=": (";
	char delim2[4]="),(";
	char delim3[4]="),,";
	char *token = NULL;
	char *outtok = NULL;
	linkedList_t tempNode;
	linkedList_t *tempNode1=NULL;
	linkedList_t currentNode;
	linkedList_t nextNode;
	uint8_t operatorCount = 0, nextOpSelected = 0;
	uint32_t nodeIndex=0, nodeIndex2=0;
	uint32_t parsedNodeCount = 0, alreadyTriedCount = 0, notSupportedCount = 0, matchFound = 0;
	uint32_t currentNodeFound = 0, unknownForbiddnCount = 0;
	char currentOp[50]="";
	returnCode_e retCode = GEN_SUCCESS;
	uint8_t parseNodeRet = 0;

	if(atCmdString == NULL) {
		CAB_DBG(CAB_GW_ERR, "Invalid args");
		return GEN_NULL_POINTING_ERROR;
	}

	memset((void *)&currentNode, '\0', sizeof(linkedList_t));
	memset((void *)&nextNode, '\0', sizeof(linkedList_t));
	memset((void *)&tempNode, '\0', sizeof(linkedList_t));

	CAB_DBG(CAB_GW_DEBUG, "######%s#######", atCmdString);
	token = multi_tok(NULL, delim);
	if (token!=NULL) {
		CAB_DBG(CAB_GW_DEBUG, "%d,****%s******", __LINE__, token);
	} else {
		return;
	}
	outtok = multi_tok(token, delim3);
	if (outtok!=NULL) {
		CAB_DBG(CAB_GW_DEBUG,"%d,****%s******", __LINE__, outtok);
	} else {
		return;
	}
	outtok = multi_tok(outtok, delim2);
	if (outtok!=NULL) {
		CAB_DBG(CAB_GW_DEBUG,"%d,****%s******", __LINE__, outtok);
	} else {
		return;
	}

	notSupportedCount = 0;
	alreadyTriedCount = 0;
	parsedNodeCount = 0;
	parseNodeRet = parseAndAddNode(&tempNode, outtok, operatorCount);
	if (parseNodeRet == 0) {
		operatorCount++;
		parsedNodeCount++;
		if (tempNode.opState == 2) {
			CAB_DBG(CAB_GW_DEBUG, "copying temp node to current node");
			memcpy((void *)&currentNode, (void *)&tempNode, sizeof(linkedList_t));
			currentNodeFound = 1;
		} else if (tempNode.opState == 1) {
			if (nextOpSelected == 0) {
				matchFound = 0;
				for (nodeIndex2=0; nodeIndex2<SIZE_OF_ARRAY(supportedOps); nodeIndex2++) {
					if (tempNode.numericCode == supportedOps[nodeIndex2].supportedNumCode &&
					    tempNode.accessType == supportedOps[nodeIndex2].accessType) {
					    matchFound = 1;
					    if(supportedOps[nodeIndex2].triedState == 0) {
						CAB_DBG(CAB_GW_INFO, "%%%%Potential op %d%%%%", tempNode.numericCode);
						supportedOps[nodeIndex2].triedState = 1;
						nextOpSelected = 1;
						memcpy((void *)&nextNode, (void *)&tempNode, sizeof(linkedList_t));
						break;
					    } 
					} 
				}
				if (matchFound == 0) {
					notSupportedCount++;
				}
			}
		} else {
			CAB_DBG(CAB_GW_WARNING, "Operator state unknown or forbidden");
			unknownForbiddnCount++;
		}

		while (outtok != NULL) {
			outtok = multi_tok(NULL, delim2);
			if (outtok!=NULL) {
				parseNodeRet = parseAndAddNode(&tempNode, outtok, operatorCount);
				if (parseNodeRet == 1) {
					CAB_DBG(CAB_GW_ERR, "Invalid operator");
					break;
				}
				operatorCount++;
				parsedNodeCount++;
				if (tempNode.opState == 2) {
					CAB_DBG(CAB_GW_DEBUG, "copying temp node to current node");
					memcpy((void *)&currentNode, (void *)&tempNode, sizeof(linkedList_t));
					currentNodeFound = 1;
				} else if (tempNode.opState == 1) {
					if (nextOpSelected == 0) {
						matchFound = 0;
						for (nodeIndex2=0; nodeIndex2<SIZE_OF_ARRAY(supportedOps); nodeIndex2++) {
							if (tempNode.numericCode == supportedOps[nodeIndex2].supportedNumCode &&
							    tempNode.accessType == supportedOps[nodeIndex2].accessType) {
							    matchFound = 1;
							    if(supportedOps[nodeIndex2].triedState == 0) {
								CAB_DBG(CAB_GW_INFO, "%%%%Potential op %d%%%%", tempNode.numericCode);
								supportedOps[nodeIndex2].triedState = 1;
								nextOpSelected = 1;
								memcpy((void *)&nextNode, (void *)&tempNode, sizeof(linkedList_t));
								break;
							    } 
							} 
						}
						if (matchFound == 0) {
							notSupportedCount++;
						}
					}
				} else {
					CAB_DBG(CAB_GW_WARNING, "Operator state unknown or forbidden");
					unknownForbiddnCount++;
				}
			}

		}


		for (nodeIndex2=0; nodeIndex2<SIZE_OF_ARRAY(supportedOps); nodeIndex2++) {
			if (supportedOps[nodeIndex2].triedState == 1) {
				for (nodeIndex = 0; nodeIndex < operatorCount; nodeIndex++) {
					tempNode1 = getLinkedListNode((uint32_t)nodeIndex);
					if (tempNode1 == NULL) {
					        supportedOps[nodeIndex2].triedState = 0;
					} else if (tempNode1->numericCode == supportedOps[nodeIndex2].supportedNumCode &&
					    tempNode1->accessType == supportedOps[nodeIndex2].accessType) {
						//printf("\r\n==========Supported Op :%d,%d===============\r\n", 
						//		supportedOps[nodeIndex2].supportedNumCode, 
						//		supportedOps[nodeIndex2].accessType);
						alreadyTriedCount++;
					}
				}
			}
		}

		if(nextOpSelected == 1) {
			CAB_DBG(CAB_GW_INFO, "==--==Next Operaor: %s===--===", nextNode.longAlphaNum);
			setNewOp(nextNode.longAlphaNum, nextNode.accessType);
			getLTEOperatorInfo(&currentOp);
			CAB_DBG(CAB_GW_INFO, "$$$Current Op: %s$$$", currentOp);
		}
		CAB_DBG(CAB_GW_INFO, "nextOpSelected:%d, alreadyTriedCount:%d, notSupportedCount:%d, parsedNodeCount:%d, currentNodeFound:%d, unknownForbiddnCount:%d",\
				nextOpSelected, alreadyTriedCount, notSupportedCount, parsedNodeCount, currentNodeFound, unknownForbiddnCount);
		if (nextOpSelected == 0 && ((alreadyTriedCount + notSupportedCount + currentNodeFound + unknownForbiddnCount) >= parsedNodeCount) ) {
			resetSupportedOpsState(supportedOps);
			retCode = powerOffEG91();
			if (retCode != GEN_SUCCESS) {
				        CAB_DBG(CAB_GW_ERR,"powerOffEG91() failed with code %d", retCode);
			}
			sleep(1);
			retCode = powerOnEG91();
			if (retCode != GEN_SUCCESS) {
				        CAB_DBG(CAB_GW_ERR,"powerOnEG91() failed with code %d", retCode);
			}
		}

		for (nodeIndex = 0; nodeIndex < operatorCount; nodeIndex++) {
			removeNode((uint32_t)nodeIndex);
		}
		CAB_DBG(CAB_GW_DEBUG, "#### Current Node ####");
		if (currentNode.opState == 2) {
			CAB_DBG(CAB_GW_DEBUG, "::%s::", currentNode.longAlphaNum);
			CAB_DBG(CAB_GW_DEBUG, "::%s::", currentNode.shortAlphaNum);
			CAB_DBG(CAB_GW_DEBUG, "::%d::", currentNode.opState);
			CAB_DBG(CAB_GW_DEBUG, "::%d::", currentNode.accessType);
		}
	} else {
		CAB_DBG(CAB_GW_DEBUG, "Not a valid operator");
	}
}


void swOverInit() {
	int nodeIndex=0, accessTypeIndex=0, nodeIndex2=0;

	nodeIndex2 = 0;
	for (nodeIndex=0; nodeIndex<SIZE_OF_ARRAY(supportedOpNumCodes); nodeIndex++) {
		for (accessTypeIndex = 0; accessTypeIndex<SIZE_OF_ARRAY(supportedAccessTypes); accessTypeIndex++) {
			supportedOps[nodeIndex2].supportedNumCode = supportedOpNumCodes[nodeIndex];
			supportedOps[nodeIndex2].accessType = supportedAccessTypes[accessTypeIndex];
			supportedOps[nodeIndex2].triedState = 0;
			//printf("Supported op : %d, %d, %d\r\n", supportedOps[nodeIndex2].supportedNumCode, 
			//		supportedOps[nodeIndex2].accessType, 
		      	//		supportedOps[nodeIndex2].triedState );
			nodeIndex2++;
		}
	}
}
