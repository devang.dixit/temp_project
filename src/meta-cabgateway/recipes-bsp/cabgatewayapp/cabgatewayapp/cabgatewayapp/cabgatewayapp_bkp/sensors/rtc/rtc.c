/*******************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : rtc.c
 * @brief : This file contains the defination of RTC module APIs.
 *
 * @Author     -    VT
 *****************************************************************
 * History
 *
 * Aug/27/2018, VT , First Draft
***************************************************************/

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include "rtc.h"
#include "debug.h"

/****************************************
 ******** STATIC VARIABLES **************
 ****************************************/
static pthread_mutex_t g_RTCMutex;

/****************************************
 ********* FUNCTION DEFINATION **********
 ****************************************/
/******************************************************************
 *@brief (This API is used to set the RTC time)
 *
 *@param[IN] time_t (Indicates time structure from date and time will be set)
 *@param[OUT] None
 *
 *@return returnCode_e  (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e setRtcTime(time_t epochTime)
{
    FUNC_ENTRY

    //Lock mutex to protect critical section
    pthread_mutex_lock(&g_RTCMutex);

    int fd;
    int retval;
    struct rtc_time rtcTime;
    struct tm *timestamp;

    timestamp = localtime(&epochTime);

    rtcTime.tm_sec = timestamp->tm_sec;
    rtcTime.tm_min = timestamp->tm_min;
    rtcTime.tm_hour = timestamp->tm_hour;
    rtcTime.tm_mday = timestamp->tm_mday;
    rtcTime.tm_mon = timestamp->tm_mon;
    rtcTime.tm_year = timestamp->tm_year;

    /* open rtc device file for reading */
    fd = open(DEV_RTC0_PATH, O_RDONLY);
    if(fd ==  -1) {
        CAB_DBG(CAB_GW_ERR, "Failed to open: %s", DEV_RTC0_PATH);
        //Unlock the mutex lock
        pthread_mutex_unlock(&g_RTCMutex);
        /* close device file */
        close(fd);
        return FILE_OPEN_FAIL;
    }

    /* set the rtc time */
    retval = ioctl(fd, RTC_SET_TIME, &rtcTime);

    if(retval == -1) {
        CAB_DBG(CAB_GW_ERR, "IOCTL Failed");
        //Unlock the mutex lock
        pthread_mutex_unlock(&g_RTCMutex);
        /* close device file */
        close(fd);
        return RTC_IOCTL_FAILED;
    }

    //Unlock the mutex lock
    pthread_mutex_unlock(&g_RTCMutex);
    /* close device file */
    close(fd);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief (This API is used to set the System time)
 *
 *@param[IN] time_t (Indicates time structure from date and time will be set)
 *@param[OUT] None
 *
 *@return returnCode_e  (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e setSysTime(time_t epochTime)
{
    FUNC_ENTRY
    //Lock mutex to protect critical section
    pthread_mutex_lock(&g_RTCMutex);

    struct timeval tv;
    int8_t status = GEN_SUCCESS;

    /* assign epoch time to timeval */
    tv.tv_sec = epochTime;
    tv.tv_usec = 0;

    /* TODO : just for debugging purpose, It can be removed in final phase */
    struct tm *debugTime = localtime(&epochTime);
    CAB_DBG(CAB_GW_TRACE, "A7's current time set to : 20%d-%d-%d  %d:%d:%d", \
            debugTime->tm_year - YEAR_OFFSET,   \
            debugTime->tm_mon + MONTH_OFFSET,   \
            debugTime->tm_mday,     \
            debugTime->tm_hour,     \
            debugTime->tm_min,      \
            debugTime->tm_sec);

    /* will set the jiffies time */
    if(-1 == settimeofday(&tv, NULL)) {
        CAB_DBG(CAB_GW_ERR, "setttimeofday failed.");
        status = RTC_SYSSET_FAILED;
    }

    //Unlock the mutex lock
    pthread_mutex_unlock(&g_RTCMutex);

    FUNC_EXIT
    return status;
}

/******************************************************************
 *@brief (This API is used to get the RTC time)
 *
 *@param[IN] None
 *@param[OUT] time_t *(Indicates pointer to time structure where date and time will be saved in it)
 *
 *@return returnCode_e  (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e getRtcTime(time_t *epochTime)
{
    FUNC_ENTRY

    //Lock mutex to protect critical section
    pthread_mutex_lock(&g_RTCMutex);

    int fd;
    int retval;
    struct rtc_time rtc_tm;
    struct tm timestamp;

    /* open rtc device file for reading */
    fd = open(DEV_RTC0_PATH, O_RDONLY);
    if(fd ==  -1) {
        CAB_DBG(CAB_GW_ERR, "Failed to open: %s", DEV_RTC0_PATH);
        //Unlock the mutex lock
        pthread_mutex_unlock(&g_RTCMutex);
        /* close device file */
        close(fd);
        return FILE_OPEN_FAIL;
    }
    retval = ioctl(fd, RTC_RD_TIME, &rtc_tm);
    if(retval == -1) {
        CAB_DBG(CAB_GW_ERR, "IOCTL Failed");
        //Unlock the mutex lock
        pthread_mutex_unlock(&g_RTCMutex);
        /* close device file */
        close(fd);
        return RTC_IOCTL_FAILED;
    }

    /* close device file */
    close(fd);

    timestamp.tm_sec = rtc_tm.tm_sec;
    timestamp.tm_min = rtc_tm.tm_min;
    timestamp.tm_hour = rtc_tm.tm_hour;
    timestamp.tm_mday = rtc_tm.tm_mday;
    timestamp.tm_mon = rtc_tm.tm_mon;
    timestamp.tm_year = rtc_tm.tm_year;

    CAB_DBG(CAB_GW_TRACE, "A7's current time set to : 20%d-%d-%d  %d:%d:%d", \
            timestamp.tm_year - YEAR_OFFSET,   \
            timestamp.tm_mon + MONTH_OFFSET,   \
            timestamp.tm_mday,     \
            timestamp.tm_hour,     \
            timestamp.tm_min,      \
            timestamp.tm_sec);
    *(time_t *)epochTime = mktime(&timestamp);

    //Unlock the mutex lock
    pthread_mutex_unlock(&g_RTCMutex);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief (This API is used to get the RTC time when GW is powerd off)
 *
 *@param[IN] None
 *@param[OUT] uint8_t (Indicates if the GW was hard/Soft Power Off)
 *@param[OUT] time_t *(Indicates pointer to time structure where date and time will be saved in it)
 *
 *@return returnCode_e  (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e getRtcBsfData(uint8_t *hardPowerOff, time_t *epochTime)
{
    FUNC_ENTRY

    //Lock mutex to protect critical section
    pthread_mutex_lock(&g_RTCMutex);

    int fd;
    int retval;
    struct tm timestamp;

    /* open rtc device file for reading */
    fd = open(DEV_RTC0_PATH, O_RDONLY);
    if(fd ==  -1) {
        CAB_DBG(CAB_GW_ERR, "Failed to open: %s", DEV_RTC0_PATH);
        //Unlock the mutex lock
        pthread_mutex_unlock(&g_RTCMutex);
        /* close device file */
        close(fd);
        return FILE_OPEN_FAIL;
    }

    retval = ioctl(fd, RTC_RD_BSF, &bsf);
    if(retval == -1) {
        CAB_DBG(CAB_GW_ERR, "IOCTL Failed");
        //Unlock the mutex lock
        pthread_mutex_unlock(&g_RTCMutex);
        /* close device file */
        close(fd);
        return RTC_IOCTL_FAILED;
    }
    CAB_DBG(CAB_GW_INFO, "BSF flag = %d\n", bsf.bsf_flag); 

    if(bsf.bsf_flag == RD_BSF_FLG) {
        *hardPowerOff = HARD_PWR_OFF;
    }
    bsf.bsf_flag = 0;
    /* close device file */
    close(fd);
    
    if(*hardPowerOff != HARD_PWR_OFF) {
        *(time_t *)epochTime = 0;
        pthread_mutex_unlock(&g_RTCMutex);
        FUNC_EXIT
        return GEN_SUCCESS;
    }
        
    timestamp.tm_sec = bsf.tm.tm_sec;
    timestamp.tm_min = bsf.tm.tm_min;
    timestamp.tm_hour = bsf.tm.tm_hour;
    timestamp.tm_mday = bsf.tm.tm_mday;
    timestamp.tm_mon = bsf.tm.tm_mon;
    timestamp.tm_year = bsf.tm.tm_year;
    
    CAB_DBG(CAB_GW_INFO, "Hard Power Off timestamp : 20%d-%d-%d  %d:%d:%d", \
            timestamp.tm_year - YEAR_OFFSET,   \
            timestamp.tm_mon + MONTH_OFFSET,   \
            timestamp.tm_mday,     \
            timestamp.tm_hour,     \
            timestamp.tm_min,      \
            timestamp.tm_sec);
    *(time_t *)epochTime = mktime(&timestamp);

    //Unlock the mutex lock
    pthread_mutex_unlock(&g_RTCMutex);

    FUNC_EXIT
    return GEN_SUCCESS;

}

/******************************************************************
 *@brief (This API is used to get the System time)
 *
 *@param[IN] None
 *@param[OUT] time_t *(Indicates pointer to time structure where date and time will be saved in it)
 *
 *@return returnCode_e  (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e getSysTime(time_t *epochTime)
{
    FUNC_ENTRY

    //Lock mutex to protect critical section
    pthread_mutex_lock(&g_RTCMutex);

    *epochTime = time(NULL);
    if(-1 == *epochTime) {
        CAB_DBG(CAB_GW_ERR, "Time is failed to fetch epoch time.");
        //Unlock the mutex lock
        pthread_mutex_unlock(&g_RTCMutex);
        return RTC_SYSGET_FAILED;
    }

    /* TODO : just for debugging purpose, It can be removed in final phase */
    struct tm *debugTime = localtime(epochTime);
    CAB_DBG(CAB_GW_TRACE, "current time is : 20%d,%d,%d  %d:%d:%d", \
            debugTime->tm_year - YEAR_OFFSET,   \
            debugTime->tm_mon + MONTH_OFFSET,   \
            debugTime->tm_mday, \
            debugTime->tm_hour, \
            debugTime->tm_min,  \
            debugTime->tm_sec);

    //Unlock the mutex lock
    pthread_mutex_unlock(&g_RTCMutex);

    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e initRtc(void)
{
    FUNC_ENTRY
    CAB_DBG(CAB_GW_INFO, "Initializing RTC interface");
    if(pthread_mutex_init(&g_RTCMutex, NULL) != 0) {
        CAB_DBG(CAB_GW_ERR, "RTC Mutex initialization failed");
        return MODULE_INIT_FAIL;
    }

    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e deInitRtc(void)
{
    FUNC_ENTRY
    CAB_DBG(CAB_GW_INFO, "Deinitializing RTC interface");
    pthread_mutex_destroy(&g_RTCMutex);
    FUNC_EXIT
    return GEN_SUCCESS;
}

time_t stringToEpochTime(uint8_t * timeBuffer)
{
    FUNC_ENTRY

    struct tm currentTime = {0};
    time_t epochTime = 0;

    /* Modification with year/month is needed to set 21st century */
    currentTime.tm_year = timeBuffer[5] + YEAR_OFFSET;
    currentTime.tm_mon  = timeBuffer[4] - MONTH_OFFSET;
    currentTime.tm_mday = timeBuffer[3];
    currentTime.tm_hour = timeBuffer[0];
    currentTime.tm_min  = timeBuffer[1];
    currentTime.tm_sec  = timeBuffer[2];

    epochTime = mktime(&currentTime);
    if(epochTime == -1) {
        CAB_DBG(CAB_GW_ERR, "mktime failed.");
    }

    /* In case of Failue it will return -1 */
    FUNC_EXIT
    return epochTime;
}

returnCode_e getTime(time_t * timestamp)
{
    FUNC_ENTRY

    int status;
    status = getSysTime(timestamp);
    if(status == FAILURE) {
        CAB_DBG(CAB_GW_ERR, "Error in get system time");
        return RTC_SYSGET_FAILED;
    }
    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e setTime(time_t timestamp)
{
    FUNC_ENTRY

    int status;
    time_t tempTime;            //Time structure used to hold time data temporary
    status = setRtcTime(timestamp);
    if(status != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "Error in set RTC time");
        return RTC_RTCSET_FAILED;
    }
    status = getRtcTime(&tempTime);
    if(status != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "Error in get RTC time");
        return RTC_RTCGET_FAILED;
    }
    status = setSysTime(tempTime);
    if(status != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "Error in set system time");
        return RTC_SYSSET_FAILED;
    }

    FUNC_EXIT
    return GEN_SUCCESS;
}
