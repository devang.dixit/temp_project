/*******************************************************************************
 * Copyright(c) <2016>, Volansys Technologies
 *
 * Description:
 * @file uart.c
 * @brief  Communication between M3 and A7.
 *
 * @Author   - Volansys
 *
 *******************************************************************************
 *
 * @History
 *
 * Aug/20/2018 VT, Created
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <mqueue.h>

#include <pthread.h>
#include "uart.h"
#include "sub1.h"
#include "hashTable.h"
#include "error.h"
#include "xbr.h"
#include "commonData.h"

/*  Uart File Descriptor    */
static int g_uartfd;

static int8_t isXBRAlive = 0;

mqd_t xbrMsgQueue;
static pthread_t g_xbrUartThreadId;
static pthread_attr_t g_xbrUartThreadAttr;

static pthread_t g_xbrProcessThreadId;
static pthread_attr_t g_xbrProcessThreadAttr;

static pthread_mutex_t xbrUartThreadAliveLock = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t xbrProcessThreadAliveLock = PTHREAD_MUTEX_INITIALIZER;

static bool g_xbrUartThreadAliveStatus = true;
static bool g_xbrProcessThreadAliveStatus = true;

static hashHandle_t *xbrHashTable;

typedef struct {
    /*  For XBR, will not used for other vendors    */
    union {
        struct {
            uint8_t unused : 3; //Filler (old temperature)
            bool rptrflg : 1; //Repeater flag from old packet
            bool batflag : 1; //Battery status flag from sensor
            uint8_t reason : 3; //Old 'reason' code, or new 'invented'
        } flgs;
        uint8_t all;
    } packed;
    uint8_t rfNoiseLevel;
} g_xbrExtraInfo_t;

static ssize_t Process_RFPKT(const uint8_t *RFPKT_bufPtr, tpmsData_t *tpmsData)
{
	uint8_t index = 0;
	uint8_t crc = 0;
	int8_t tmpr = 0;
	float pressure = 0; 
	uint32_t sensorID;
	HASH_STATUS retHash = HASH_SUCCESS;
	g_xbrExtraInfo_t xbrExtraData = {0};

	for(index = 0; index < XBR_PKT_LEN - 1; index++) {
		crc = crc + RFPKT_bufPtr[index];
		//CAB_DBG(CAB_GW_INFO,("%d:%02x ", index, RFPKT_bufPtr[index]);
	}

	if( crc == 0 ) {

		//Zero S/Ns are invalid
		if((RFPKT_bufPtr[3] | RFPKT_bufPtr[2] | RFPKT_bufPtr[1]) != 0 ) { 
			tpmsData->sensorID = RFPKT_bufPtr[1] << 16 | RFPKT_bufPtr[2] << 8 | RFPKT_bufPtr[3];

			retHash = hashSearch(xbrHashTable, tpmsData->sensorID, &sensorID);
			if (retHash != HASH_SUCCESS) {
				return FAILURE;
			}
			pressure = RFPKT_bufPtr[4];
			if(pressure > 250) {
				//Don't allow confusion with internal PSI codes
				pressure = 250; 
			}
			tpmsData->pressure = pressure;

			//Original RF packet form: 84 or 8C
			if((RFPKT_bufPtr[0] & 0xF7) == 0x84) {
				xbrExtraData.packed.all = RFPKT_bufPtr[5]; //Start with reason, clear else
				tmpr = (RFPKT_bufPtr[5] << 4) & 0x70;
				//Fake improved temp resolution
				tmpr -= 20; //Remove offset	
			}
			else { //New Expanded temperature packet!
				if(RFPKT_bufPtr[0] == 0x88) { //'regular' type
					xbrExtraData.packed.all = 0; //Reason = 'regular report'
				}
				else { //Flagged packet, new air or new mag
					if((RFPKT_bufPtr[5] & 0x80) != 0) { //MSB now flags new air/magnet
						xbrExtraData.packed.all = 7 << 5; //Reason = 'magnet'
					}
					else {
						xbrExtraData.packed.all = 6 << 5; //Reason = 'new air'
					}
				}
				//Improved temp resolution
				tmpr = RFPKT_bufPtr[5] & 0x7F; //Keep to only 7 bits, 2.5F per LSB
				tmpr -= (uint8_t)(50 / 2.5); //Offset correction of 50F
			}

			tpmsData->temperature = (float) tmpr;
			tpmsData->temperature *= 2.5;		/*	2.5F per bit	*/
			tpmsData->temperature = (tpmsData->temperature - 32) * (5.0/9);		/*	ferenhit to celcius	*/

/*Updated RSSI caculation method as per communication with  PressurePro representative*/
#if 0
			xbrExtraData.rfNoiseLevel = RFPKT_bufPtr[7];
			//Remove presumed 'idle' RSSI Signal level
			tmpRF = RFPKT_bufPtr[6];
			CAB_DBG(CAB_GW_INFO,"RSSI : %d\r\n", tmpRF);
			CAB_DBG(CAB_GW_INFO,"Noise : %d\r\n", xbrExtraData.rfNoiseLevel);
			if( tmpRF < xbrExtraData.rfNoiseLevel) {
				tmpRF = xbrExtraData.rfNoiseLevel;
			}
			tmpRF -= xbrExtraData.rfNoiseLevel;
			if(tmpRF <= 100) {
				tpmsData->rssi = tmpRF;
			}
			else {
				tpmsData->rssi = 100; //Limit RFlevel to 0..100 scale
			}
#else
			tpmsData->rssi = RFPKT_bufPtr[6];
#endif
		}
		return GEN_SUCCESS;
	}
	else {
		// printf("\r\n CRC : %d\r\n", crc);
		return FAILURE;
	}
}

static void startXBR(void)
{
	struct mq_attr rfMsgQueueAttr;

	// printf("%d\r\n", __LINE__);
	initHashTable(&xbrHashTable, MAX_SENSORS, sizeof(uint32_t));

	/* initialize the queue attributes */
	rfMsgQueueAttr.mq_flags = 0;
	rfMsgQueueAttr.mq_maxmsg = MAX_SENSORS;
	rfMsgQueueAttr.mq_msgsize = XBR_PKT_LEN - 1;
	rfMsgQueueAttr.mq_curmsgs = 0;

	/* create the message queue */
	xbrMsgQueue = mq_open(XBR_QUEUE_NAME, O_CREAT | O_RDWR , 0644, &rfMsgQueueAttr);
	if(xbrMsgQueue < 0) {
		perror("Qopen");
	}

	pthread_create(&g_xbrUartThreadId, &g_xbrUartThreadAttr, xbrUartThread, NULL);
	pthread_create(&g_xbrProcessThreadId, &g_xbrProcessThreadAttr, xbrProcessThread, NULL);
}

static void stopXBR(void)
{
	pthread_cancel(g_xbrProcessThreadId);
	pthread_cancel(g_xbrUartThreadId);
	deInitHashTable(xbrHashTable);
	mq_close(xbrMsgQueue);
}

static bool getXbrUartThreadAliveStatus(void)
{
        bool status;
        pthread_mutex_lock(&xbrUartThreadAliveLock);
        status = g_xbrUartThreadAliveStatus;
        pthread_mutex_unlock(&xbrUartThreadAliveLock);
        return status;
}

static void setXbrUartThreadAliveStatus(bool value)
{
        pthread_mutex_lock(&xbrUartThreadAliveLock);
        g_xbrUartThreadAliveStatus = value;
        pthread_mutex_unlock(&xbrUartThreadAliveLock);
}

bool xbrUartThreadCallback(void)
{
        bool status = false;
        CAB_DBG(CAB_GW_TRACE, "In func : %s", __func__);
        if(getXbrUartThreadAliveStatus() == true)
        {
                status = true;
        }

        setXbrUartThreadAliveStatus(false);

        return status;
}

void* xbrUartThread(void *arg)
{
	uint8_t index = 0;
	uint8_t readLength = XBR_PKT_LEN - 1;
	int8_t readBytes = 0;
	uint8_t uartFrame[XBR_PKT_LEN]; //Buffer(s) for received sensor packets
	int8_t retVal;
	returnCode_e retCode = GEN_SUCCESS;

	watchDogTimer_t newWatchdogNode;

	newWatchdogNode.threadId = syscall(SYS_gettid);
	newWatchdogNode.CallbackFn = xbrUartThreadCallback;

	CAB_DBG(CAB_GW_INFO, "XBR UART thread id : %ld", syscall(SYS_gettid));
	retCode = threadRegister(&newWatchdogNode);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "XBR UART thread registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_REGISTRATION_FAIL, GW_ERR, "XBR UART thread registration failed");
        }

	while(1) {
	        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
		readBytes = read(g_uartfd, uartFrame, readLength);
		if(readBytes > 0) {
		        pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
			setXbrUartThreadAliveStatus(true);
			#if 1
			switch(uartFrame[0]) {
				case 0xFA :
					//printf("#\r\n");
					isXBRAlive = 1;
					break;
				case 0x84 :
				case 0x8C :
				case 0x88 :
				case 0x98 :
				case 0x80 :
				case 0x90 :
					retVal = mq_send(xbrMsgQueue, uartFrame, XBR_PKT_LEN - 1, 0);
					if(retVal < 0) {
						perror("QSend");
					}
					break;
				default :
					CAB_DBG(CAB_GW_DEBUG,"Other data");
					for(index = 0; index < readBytes; index++)
						CAB_DBG(CAB_GW_DEBUG,"%d:%02x ", index, uartFrame[index]);	
					CAB_DBG(CAB_GW_DEBUG,"\r\n");
					break;
			}
			#else
			for(index = 0; index < readBytes; index++)
				CAB_DBG(CAB_GW_DEBUG,"%d:%02x ", index, uartFrame[index]);
			#endif
		}
	}

        retCode = threadDeregister(newWatchdogNode.threadId);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "XBR UART thread de-registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "XBR UART thread de-registration failed");
        }
}

static bool getXbrProcessThreadAliveStatus(void)
{
        bool status;
        pthread_mutex_lock(&xbrProcessThreadAliveLock);
        status = g_xbrProcessThreadAliveStatus;
        pthread_mutex_unlock(&xbrProcessThreadAliveLock);
        return status;
}

static void setXbrProcessThreadAliveStatus(bool value)
{
        pthread_mutex_lock(&xbrProcessThreadAliveLock);
        g_xbrProcessThreadAliveStatus = value;
        pthread_mutex_unlock(&xbrProcessThreadAliveLock);
}

bool xbrProcessThreadCallback(void)
{
        bool status = false;
	ssize_t retVal;
	uint8_t uartFrame[XBR_PKT_LEN] = {'D','U','M','M','Y','M','S','G'};

        CAB_DBG(CAB_GW_TRACE, "In func : %s", __func__);
        if(getXbrProcessThreadAliveStatus() == true)
        {
                status = true;
        }

        setXbrProcessThreadAliveStatus(false);

	retVal = mq_send(xbrMsgQueue, uartFrame, XBR_PKT_LEN - 1, 0);
	if(retVal < 0) {
		perror("QSend");
		status = false;
	}

        return status;
}

void* xbrProcessThread(void *arg)
{
	ssize_t retValue;
	uint8_t xbrFrame[XBR_PKT_LEN];
	tpmsData_t tpms = {0};
        returnCode_e retCode = GEN_SUCCESS;
	char errorMsg[100];

	watchDogTimer_t newWatchdogNode;

	newWatchdogNode.threadId = syscall(SYS_gettid);
	newWatchdogNode.CallbackFn = xbrProcessThreadCallback;

	CAB_DBG(CAB_GW_INFO, "XBR Process thread id : %ld", syscall(SYS_gettid));
	retCode = threadRegister(&newWatchdogNode);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "XBR Process thread registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_REGISTRATION_FAIL, GW_ERR, "XBR Process thread registration failed");
        }

	while(1) {
		memset(&tpms, 0, sizeof(tpms));
		tpms.battery = -1;
	        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
		retValue = mq_receive(xbrMsgQueue, xbrFrame, XBR_PKT_LEN, NULL);
	        pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
			if(retValue < 0) {
				sprintf(errorMsg, "Sub1 mq_receive failed with error %s", strerror(errno));
				rDiagSendMessage(MESSAGE_QUEUE_OVERFLOW, GW_WARNING, errorMsg);
			}

		if(memcmp(xbrFrame, "DUMMYMSG", sizeof(uint8_t)*7) == 0)
		{
			CAB_DBG(CAB_GW_TRACE,"In dummy msg event!");
			setXbrProcessThreadAliveStatus(true);
		}
		else
		{
			retValue = Process_RFPKT(xbrFrame, &tpms);
			if(retValue >= 0) {
				#if 0
				printf("ID : 0x%x\t", tpms.sensorID);
				printf("P : %f\t", tpms.pressure);
				printf("T : %f\t", tpms.temperature);
				printf("B : %f\t", tpms.battery);
				printf("R : %d\r\n", tpms.rssi);
				#endif
				if(sendDataToDM != NULL) {
					sendDataToDM(&tpms);
				}
			}
		}
	}

	retCode = threadDeregister(newWatchdogNode.threadId);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "XBR Process thread de-registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "XBR Process thread de-registration failed");
        }
}

int8_t initXBR(const rfInfo_t sub1Info)
{
	int8_t retVal;
	//FUNC_ENTRY
	if((sub1Info.vendorType < 0) || (sub1Info.vendorType >= MAX_VENDORS)) {
		/*	@TODO return error code	for invald vendor type	*/
		return ;
	}

	retVal = uartInit(&g_uartfd, UART_BAUDRATE_XBR, UART_PORT_XBR);

	sendDataToDM = sub1Info.sendTpmsDataToDM;

	startXBR();

	return retVal;
	//FUNC_EXIT
}

int8_t deInitXBR()
{
	//FUNC_ENTRY
	int8_t retVal;
	stopXBR();

	retVal = uartDeInit(g_uartfd);

	return retVal;
	//FUNC_EXIT
}

int8_t addPressureProSensor(void *sensorID)
{
	HASH_STATUS retHash = HASH_SUCCESS;
	/*	@TODO: add support for multiple sensors, "sensorId" will have data 
		for numberOfSensors and sensorID	*/
	//uint8_t	numberOfSensors = 1;
	retHash = hashInsert(xbrHashTable, *(uint32_t *)sensorID, sensorID);
	if (retHash != HASH_SUCCESS) {
		CAB_DBG(CAB_GW_ERR,"Error Inserting Sensor ID");
	}
}

int8_t  removePressureProSensor(void *sensorID)
{
	HASH_STATUS retHash = HASH_SUCCESS;
	int8_t tempXBRAliveStatus;
	/*	@TODO: add support for multiple sensors, "sensorId" will have data 
		for numberOfSensors and sensorID	*/
	//uint8_t numberOfSensors = 1;
	if ((*(uint32_t *)sensorID) == 0) {
		tempXBRAliveStatus = isXBRAlive;
		isXBRAlive = 0;
		if (tempXBRAliveStatus == 1) {
			return GEN_SUCCESS;
		} else {
			return GEN_API_FAIL;
		}
	}
	retHash = hashDelete(xbrHashTable, *(uint32_t *)sensorID);
	if (retHash != HASH_SUCCESS) {
		CAB_DBG(CAB_GW_ERR,"Error Deleting Sensor ID");
	}
}


/*
 * Callback function to fetch data from relevant RF received module
*/
void XbrDataFetchCB(int sigNum, siginfo_t * sigInfo, void * unused){
	printf("\nReceived XbrDataFetchCB\n");
	return;
}
