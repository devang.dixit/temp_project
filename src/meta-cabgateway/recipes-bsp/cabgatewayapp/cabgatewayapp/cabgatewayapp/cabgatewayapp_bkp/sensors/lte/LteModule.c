#include "LteModule.h"
#include "eg91ATCmd.h"
#include "debug.h"
#include <semaphore.h>
#include "syscall.h"

#define DEBUG_LTE

static pthread_t lteStatusThreadID_t = PTHREAD_MUTEX_INITIALIZER; 
static pthread_mutex_t lteStatusHandlerThreadAliveLock = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t lteModuleMutex = PTHREAD_MUTEX_INITIALIZER;
lteCallback g_LTECBFunction = NULL;
static mqd_t mqLteStateQueue = -1; 
static mqd_t mqLteCmdQueue = -1; 
static bool g_lteStatusHandlerThreadAliveStatus = true;
static bool g_lteStateFlag = false;

returnCode_e setNewOp(char *opLongAlphaName, uint8_t accessType)
{
	returnCode_e retCode = GEN_SUCCESS;
	char writeCmdBuffChangeOp[] = "AT+COPS=";
	char writeCmdBuffUnreg[] = "AT+COPS=2";
	uint8_t mode = 4; /*manual mode selection*/
	uint8_t format = 0; /*format numeric code*/
	char writeBuff[50] = "";
	int  readBytes = 0;
	char readBuff[1024];
	snprintf(writeBuff, sizeof(writeBuff), "%s", writeCmdBuffUnreg);
	CAB_DBG(CAB_GW_DEBUG, "writ buf = %s$$", writeBuff);
	retCode = ATTransactEG91_LongSleep(writeBuff, strlen(writeBuff), "OK", readBuff, &readBytes);
	CAB_DBG(CAB_GW_DEBUG, "read buf = %s", readBuff);
	if (GEN_SUCCESS != retCode) {
		CAB_DBG(CAB_GW_ERR, "error unreg current op");
		return retCode;
	}

	/**/
	snprintf(writeBuff, sizeof(writeBuff), "%s%d,%d,\"%s\",%d", writeCmdBuffChangeOp, mode, format, opLongAlphaName, accessType);
	CAB_DBG(CAB_GW_DEBUG, "writ buf = %s$$", writeBuff);
	retCode = ATTransactEG91_LongSleep(writeBuff, strlen(writeBuff), "OK", readBuff, &readBytes);
	CAB_DBG(CAB_GW_DEBUG, "read buf = %s", readBuff);
	if (GEN_SUCCESS != retCode) {
		CAB_DBG(CAB_GW_ERR, "error in setting new operator");
		return retCode;
	}
	return GEN_SUCCESS;
}

returnCode_e getAvailOpInfo(char *opName)
{
	returnCode_e retCode = GEN_SUCCESS;
	char writeBuff[] = "AT+COPS=?";
	int  readBytes = 0;
	char readBuff[1024];
	int i = 0;
	uint8_t retyrCount = 0;

	FUNC_ENTRY

	if(!opName)
	{
		CAB_DBG(CAB_GW_ERR, "Invalid argument");
		FUNC_EXIT
		return GEN_NULL_POINTING_ERROR;
	}

	while(retyrCount++ <= 3) {
		retCode = ATTransactEG91_LongSleep(writeBuff, strlen(writeBuff), "+COPS: ", readBuff, &readBytes);
		if(retCode != GEN_SUCCESS)
		{
			CAB_DBG(CAB_GW_ERR, "ATTransactEG91() failed with %d", retCode);
			FUNC_EXIT
			return retCode;
		} else {
			break;
		}
	}

	CAB_DBG(CAB_GW_INFO, "%s", readBuff);
	if(strstr(readBuff, "+COPS: "))
	{
		strncpy(opName, readBuff, strlen(readBuff)+1);
#ifdef DEBUG_LTE
		CAB_DBG(CAB_GW_INFO, "Reading ...");
		for(i = 0; i < readBytes; i++)
		{
			CAB_DBG(CAB_GW_INFO, "%c", readBuff[i]);
		}
#endif
	}
	else
	{
		FUNC_EXIT
		return AT_CMD_FAIL;
	}

	FUNC_EXIT
	return retCode;
}

returnCode_e getLTEOperatorInfo(char *opName)
{
	returnCode_e retCode = GEN_SUCCESS;
	char writeBuff[] = "AT+COPS\?";
	int  readBytes = 0;
	char readBuff[512];
	int i = 0;
	char *token = NULL;
	const char delim[2] = "\"";

	FUNC_ENTRY

	if(!opName)
	{
		CAB_DBG(CAB_GW_ERR, "Invalid argument");
		FUNC_EXIT
		return GEN_NULL_POINTING_ERROR;
	}

	retCode = ATTransactEG91(writeBuff, strlen(writeBuff), "+COPS: ", readBuff, &readBytes);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "ATTransactEG91() failed with %d", retCode);
		FUNC_EXIT
		return retCode;
	}

	if(strstr(readBuff, "+COPS: "))
	{
		token = strtok(readBuff, delim);
		token = strtok(NULL, delim);
		if(!token)
		{
			CAB_DBG(CAB_GW_ERR, "Operator name couldn't fetch");
			FUNC_EXIT
			return AT_CMD_FAIL;
		}
		strncpy(opName, token, strlen(token)+1);

#ifdef DEBUG_LTE
		for(i = 0; i < readBytes; i++)
		{
			CAB_DBG(CAB_GW_TRACE, "%c", readBuff[i]);
		}
#endif
	}
	else
	{
		FUNC_EXIT
		return AT_CMD_FAIL;
	}

	FUNC_EXIT
	return retCode;
}

returnCode_e getLTEConnStat(uint8_t *stat)
{
        char *hostname;
        struct hostent *hostinfo;
        returnCode_e retCode = GEN_SUCCESS;
        FUNC_ENTRY

        if(!stat)
        {
                CAB_DBG(CAB_GW_ERR, "Invalid argument");
                FUNC_EXIT
                return GEN_NULL_POINTING_ERROR;
        }

        hostname = "google.com";
        hostinfo = (struct hostent *)gethostbyname(hostname);

        if (hostinfo == NULL)
        {
#ifdef DEBUG_LTE
                CAB_DBG(CAB_GW_DEBUG, "no connection!");
#endif
                *stat = 0;
        }
        else
        {
#ifdef DEBUG_LTE
                CAB_DBG(CAB_GW_DEBUG, "Connection established!");
#endif
                *stat = 1;
        }

        FUNC_EXIT
        return retCode;
}


returnCode_e getLTESigQuality(int *rssi, int *bErrorRate)
{
	char writeBuff[] = "AT+CSQ";
	int  readBytes = 0;
	char readBuff[512];
	int i = 0;
	char *token = NULL;
	const char delim1[2] = " ";
	const char delim2[2] = ",";
	returnCode_e retCode = GEN_SUCCESS;

	FUNC_ENTRY

	if(!rssi || !bErrorRate)
	{
		CAB_DBG(CAB_GW_ERR, "Invalid argument");
		FUNC_EXIT
		return GEN_NULL_POINTING_ERROR;
	}

	retCode = ATTransactEG91(writeBuff, strlen(writeBuff), "+CSQ:", readBuff, &readBytes);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "ATTransactEG91() failed with %d", retCode);
		FUNC_EXIT
		return retCode;
	}

	if(strstr(readBuff, "+CSQ:"))
	{
#ifdef DEBUG_LTE
		CAB_DBG(CAB_GW_TRACE, "Reading ...");
		for(i = 0; i < readBytes; i++)
		{
			CAB_DBG(CAB_GW_TRACE, "%c", readBuff[i]);
		}
#endif
		token = strtok(readBuff, delim1);
		token = strtok(NULL, delim1);
		if(!token)
		{
			CAB_DBG(CAB_GW_ERR,  "Couldn't fetch signal strength");
			FUNC_EXIT
			return AT_CMD_FAIL;
		}
		token = strtok(token, delim2);
		if(!token)
		{
			CAB_DBG(CAB_GW_ERR,  "Couldn't fetch signal strength");
			FUNC_EXIT
			return AT_CMD_FAIL;
		}
		*rssi = atoi(token);

		token = strtok(NULL, delim2);
		if(!token)
		{
			CAB_DBG(CAB_GW_ERR,  " Couldn't fetch signal quality\r\n");
			FUNC_EXIT
			return AT_CMD_FAIL;
		}
		*bErrorRate = atoi(token);
	}
	else
	{
		FUNC_EXIT
		return AT_CMD_FAIL;
	}

	FUNC_EXIT

	return retCode;
}

returnCode_e getLteICCIDNum(char *iccId)
{
	char writeBuff[] = "AT+QCCID";
	int  readBytes = 0;
	char readBuff[512] = {'\0'};
	int i = 0;
	char *token = NULL;
	const char delim1[2] = " ";
	returnCode_e retCode = GEN_SUCCESS;

	FUNC_ENTRY

	if(!iccId)
	{
		CAB_DBG(CAB_GW_ERR, "Invalid argument");
		FUNC_EXIT
		return GEN_NULL_POINTING_ERROR;
	}

	retCode = ATTransactEG91(writeBuff, strlen(writeBuff), "+QCCID:", readBuff, &readBytes);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "ATTransactEG91() failed with %d", retCode);
		FUNC_EXIT
		return retCode;
	}

	if(strstr(readBuff, "+QCCID:"))
	{
#ifdef DEBUG_LTE
		CAB_DBG(CAB_GW_TRACE, "Reading ...");
		for(i = 0; i < readBytes; i++)
		{
			CAB_DBG(CAB_GW_TRACE, "%c", readBuff[i]);
		}
#endif
		token = strtok(readBuff, delim1);
		token = strtok(NULL, delim1);

		if(!token)
		{
			CAB_DBG(CAB_GW_ERR, "Couldn't fetch iccId");
			FUNC_EXIT
			return AT_CMD_FAIL;
		}
		strncpy(iccId, token, strlen(token)+1);
		for (i=0; i <= strlen(token)+1; i++) {
			CAB_DBG(CAB_GW_TRACE, "ind :%d, data:%d", i, iccId[i]);
			if (!isalnum(iccId[i])) {
				/*Remove last extra don't care character*/
				if (i == 20) {
					iccId[i-1] = '\0';
				}
				iccId[i] = '\0';
			}
		}
	}
	else
	{
		FUNC_EXIT
		return AT_CMD_FAIL;
	}
	
	FUNC_EXIT	

	return retCode;
}

returnCode_e checkSIMDetection(uint8_t *present)
{
#if 0 
	char writeBuff1[] = "AT+QSIMDET=1,1";
	char writeBuff2[] = "AT+QSIMSTAT=1";
	char writeBuff3[] = "AT+QSIMSTAT?";
#endif
	char writeBuff1[] = "AT+CPIN?";
	char writeBuff2[] = "AT+QINISTAT";
	int  readBytes = 0;
	char readBuff[512];
	int i = 0;
	char *token = NULL;
	const char delim1[2] = " ";
	const char delim2[2] = ",";
	returnCode_e retCode = GEN_SUCCESS;

	FUNC_ENTRY

	if(!present)
	{
		CAB_DBG(CAB_GW_ERR, "Invalid argument");
		FUNC_EXIT
		return GEN_NULL_POINTING_ERROR;
	}

	*present = 0;
	retCode = ATTransactEG91(writeBuff1, strlen(writeBuff1), "+CPIN:", readBuff, &readBytes);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "ATTransactEG91() failed with %d", retCode);
		FUNC_EXIT
		return retCode;
	}

	if(!strstr(readBuff, "+CPIN: READY"))
	{
        if (strstr(readBuff, "+CME ERROR: 10")) {
            CAB_DBG(CAB_GW_INFO, "SIM not present CME Error #10");
            FUNC_EXIT
            return GEN_SUCCESS;
        }
		CAB_DBG(CAB_GW_ERR, "AT command fail");
		FUNC_EXIT
		return AT_CMD_FAIL;
	}

	retCode = ATTransactEG91(writeBuff2, strlen(writeBuff2), "+QINISTAT:", readBuff, &readBytes);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "ATTransactEG91() failed with %d", retCode);
		FUNC_EXIT
		return retCode;
	}

    if(strstr(readBuff, "+QINISTAT:"))
    {
#ifdef DEBUG_LTE
		CAB_DBG(CAB_GW_TRACE, "Reading ...");
		for(i = 0; i < readBytes; i++)
		{
			CAB_DBG(CAB_GW_TRACE, "%c", readBuff[i]);
		}
#endif
        token = strtok(readBuff, delim1);
        token = strtok(NULL, delim1);
        if(!token)
        {
			CAB_DBG(CAB_GW_ERR,  "Fail to check SIM presence");
			FUNC_EXIT
			return AT_CMD_FAIL;
        }
        *present = atoi(token);
    }
	else
	{
		FUNC_EXIT
		return AT_CMD_FAIL;
    }

#if 0
	if(!strstr(readBuff, "OK"))
	{
		CAB_DBG(CAB_GW_ERR, "AT command fail");
		FUNC_EXIT
		return AT_CMD_FAIL;
	}
	retCode = ATTransactEG91(writeBuff3, strlen(writeBuff3), "+QSIMSTAT:", readBuff, &readBytes);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "ATTransactEG91() failed with %d", retCode);
		FUNC_EXIT
		return retCode;
	}

	if(strstr(readBuff, "+QSIMSTAT:"))
	{
#ifdef DEBUG_LTE
		CAB_DBG(CAB_GW_TRACE, "Reading ...");
		for(i = 0; i < readBytes; i++)
		{
			CAB_DBG(CAB_GW_TRACE, "%c", readBuff[i]);
		}
#endif
		token = strtok(readBuff, delim1);
		token = strtok(NULL, delim1);
		if(!token)
		{
			CAB_DBG(CAB_GW_ERR,  "Fail to check SIM presence");
			FUNC_EXIT
			return AT_CMD_FAIL;
		}

		token = strtok(token, delim2);
		token = strtok(NULL, delim2);
		if(!token)
		{
			CAB_DBG(CAB_GW_ERR,  "Fail to check SIM presence");
			FUNC_EXIT
			return AT_CMD_FAIL;
		}
		*present = atoi(token);
	}
	else
	{
		FUNC_EXIT
		return AT_CMD_FAIL;
	}
#endif

	FUNC_EXIT

	return retCode;
}

returnCode_e getLteNetTime(time_t *currTime)
{
	returnCode_e retCode = GEN_SUCCESS;
	char writeBuff[] = "AT+QLTS=1";
	int  readBytes = 0;
	char readBuff[512];
	int i = 0;
	struct tm tmTime;

	FUNC_ENTRY

	if(!currTime)
	{
		CAB_DBG(CAB_GW_ERR, "Invalid argument");
		FUNC_EXIT
		return GEN_NULL_POINTING_ERROR;
	}

	retCode = ATTransactEG91(writeBuff, strlen(writeBuff), "+QLTS:", readBuff, &readBytes);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "ATTransactEG91() failed with %d", retCode);
		FUNC_EXIT
		return retCode;
	}

	if(strstr(readBuff, "+QLTS:"))
	{
#ifdef DEBUG_LTE
		CAB_DBG(CAB_GW_TRACE, "Reading ...");
		for(i = 0; i < readBytes; i++)
		{
			CAB_DBG(CAB_GW_TRACE, "%c", readBuff[i]);
		}
#endif
		memset(&tmTime, 0, sizeof(struct tm));
		strptime(readBuff, "+QLTS: \"%Y/%m/%d,%H:%M:%S", &tmTime);
		*currTime = mktime(&tmTime);
	}
	else
	{
		FUNC_EXIT
		return AT_CMD_FAIL;
	}

	FUNC_EXIT

	return retCode;
}

returnCode_e getLteIMEINum(uint8_t *imeiNum)
{
	char writeBuff[] = "AT+CGSN";
	int  readBytes = 0;
	char readBuff[512];
	int i = 0;
	returnCode_e retCode = GEN_SUCCESS;

	FUNC_ENTRY

	if(!imeiNum)
	{
		CAB_DBG(CAB_GW_ERR, "Invalid argument");
		FUNC_EXIT
		return GEN_NULL_POINTING_ERROR;
	}

	retCode = ATTransactEG91(writeBuff, strlen(writeBuff), "imei", readBuff, &readBytes);
	//printf("\r\nread bytes : %d\r\n", readBytes);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "ATTransactEG91() failed with %d", retCode);
		FUNC_EXIT
		return retCode;
	}

	if(readBytes < MAX_IMEI_LEN)
	{
		CAB_DBG(CAB_GW_ERR, "Fail to read IMEI number");
		FUNC_EXIT
		return AT_CMD_FAIL;
	}

#ifdef DEBUG_LTE
	CAB_DBG(CAB_GW_TRACE, "Reading ...");
	for(i = 0; i < readBytes; i++)
	{
		CAB_DBG(CAB_GW_TRACE, "%c", readBuff[i]);
	}
#endif

	memcpy(imeiNum, readBuff, MAX_IMEI_LEN);
	for (i=0; i <= MAX_IMEI_LEN; i++) {
		if (!isalnum(imeiNum[i])) {
			imeiNum[i] = '\0';
		}
		imeiNum[i] = imeiNum[i] & 0x0F;
	}

	FUNC_EXIT

	return retCode;
}

returnCode_e getLTEDiagInfo(lteDiagInfo_t *diagInfo)
{
	int rssi = 0;
	int ber = 0;
	char opName[MAX_LEN_OPERATOR_NAME];
	uint8_t present = 0;
	char simNotPresentStr[] = NO_SIM_PRESENT_STR;
	char noOpInfoStr[] = NO_OPER_INFO_STR;
	returnCode_e retCode = GEN_SUCCESS;

	FUNC_ENTRY

	if(!diagInfo)
	{
		CAB_DBG(CAB_GW_ERR, "Invalid argument");
		FUNC_EXIT
		return GEN_NULL_POINTING_ERROR;
	}

 	retCode = checkSIMDetection(&present);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "checkSIMDetection fail with error : %d", retCode);
		FUNC_EXIT
		return retCode;
	}
	
	CAB_DBG(CAB_GW_DEBUG, "Sim status : %d", present);
	memset(diagInfo, 0, sizeof(lteDiagInfo_t));

	if (present > 1) {
		retCode = getLTESigQuality(&rssi, &ber);
		if(retCode != GEN_SUCCESS)
		{
			/* Sending proeper diag info to user instead of failing */
			CAB_DBG(CAB_GW_ERR, "getLTESigQuality() fail with error : %d", retCode);
			diagInfo->signalStrength = 0;
			diagInfo->signalQuality = 0;
			retCode = GEN_SUCCESS;
		} else {
			diagInfo->signalStrength = rssi;
			diagInfo->signalQuality = ber;
		}

		retCode = getLTEOperatorInfo(opName);
		if(retCode != GEN_SUCCESS)
		{
			/* Sending proeper diag info to user instead of failing */
			CAB_DBG(CAB_GW_ERR, "getLTEOperatorInfo() fail with error : %d", retCode);
			strncpy(diagInfo->operatorName, noOpInfoStr, strlen(noOpInfoStr)+1);
			retCode = GEN_SUCCESS;
		} else {
			strncpy(diagInfo->operatorName, opName, strlen(opName)+1);
		}

	} else {
		strncpy(diagInfo->operatorName, simNotPresentStr, strlen(simNotPresentStr)+1);
	}

	FUNC_EXIT

	return retCode;
}


static returnCode_e sendLteCmd(lteCmd_e cmd)
{
	returnCode_e retCode = GEN_SUCCESS;
        lteCmd_t lteCmdData;
        struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0};
        clock_gettime(CLOCK_REALTIME, &abs_timeout);
        abs_timeout.tv_sec += MQ_SEND_LTE_CMD_TIMEOUT;
	char errorMsg[100] = "";
        FUNC_ENTRY

        memset(&lteCmdData, 0, sizeof(lteCmdData));
        lteCmdData.cmd = cmd;

        CAB_DBG(CAB_GW_DEBUG, "Sending LTE %d command", lteCmdData.cmd);

        if(mq_timedsend(mqLteCmdQueue, (const char *)&lteCmdData, sizeof(lteCmd_t), 0, &abs_timeout) < 0)
	{
		CAB_DBG(CAB_GW_WARNING, "mq_timedsend for %s failed with error %s", LTE_CMD_QUEUE_NAME, strerror(errno));
		sprintf(errorMsg, "mq_timedsend for %s failed with error %s", LTE_CMD_QUEUE_NAME, strerror(errno));
		rDiagSendMessage(MESSAGE_QUEUE_OVERFLOW, GW_WARNING, errorMsg);
		retCode = MQ_SEND_ERROR;
	}

        FUNC_EXIT
	return retCode;
}

returnCode_e connectLTE()
{
	returnCode_e retCode = GEN_SUCCESS;
	FUNC_ENTRY

	retCode = sendLteCmd(CONNECT);

        FUNC_EXIT
	return retCode;
}

returnCode_e disconnectLTE()
{
	returnCode_e retCode = GEN_SUCCESS;
	FUNC_ENTRY

	retCode = sendLteCmd(DISCONNECT);

        FUNC_EXIT
	return retCode;
}

static returnCode_e lteCmdQueueSetup()
{
        returnCode_e retCode = GEN_SUCCESS;
        FUNC_ENTRY

	mqLteCmdQueue = mq_open(LTE_CMD_QUEUE_NAME, O_WRONLY);
        if(mqLteCmdQueue < 0)
        {
                CAB_DBG(CAB_GW_ERR, "mq_open for LTE command queue fail with error %s", strerror(errno));
		FUNC_EXIT
                return MQ_OPEN_ERROR;
        }
        CAB_DBG(CAB_GW_DEBUG, "mq_open /lte_cmd_queue success");

        FUNC_EXIT
        return retCode;
}

static returnCode_e lteStateQueueSetup()
{
        returnCode_e retCode = GEN_SUCCESS;
        FUNC_ENTRY

	mqLteStateQueue = mq_open(LTE_STATE_QUEUE_NAME, O_RDWR);
        if(mqLteStateQueue < 0) 
        {    
                CAB_DBG(CAB_GW_ERR, "mq_open for LTE state queue fail with error %s", strerror(errno));
        	FUNC_EXIT
                return MQ_OPEN_ERROR;
        }    

        FUNC_EXIT
        return retCode;
}

static returnCode_e setupMsgQueue()
{
	FUNC_ENTRY
	returnCode_e retCode = GEN_SUCCESS;

	retCode = lteCmdQueueSetup();
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "lteCmdQueueSetup() fail with error %d", retCode);
		FUNC_EXIT
                return retCode;
        }

	retCode = lteStateQueueSetup();
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "lteStateQueueSetup() fail with error %d", retCode);
		FUNC_EXIT
                return retCode;
        }

	FUNC_EXIT
	return retCode;
}

static bool getLteStatusHandlerAliveStatus(void)
{
        bool status;
        pthread_mutex_lock(&lteStatusHandlerThreadAliveLock);
        status = g_lteStatusHandlerThreadAliveStatus;
        pthread_mutex_unlock(&lteStatusHandlerThreadAliveLock);
        return status;
}

static void setLteStatusHandlerAliveStatus(bool value)
{
        pthread_mutex_lock(&lteStatusHandlerThreadAliveLock);
        g_lteStatusHandlerThreadAliveStatus = value;
        pthread_mutex_unlock(&lteStatusHandlerThreadAliveLock);
}

bool lteStatusHandlerCallback(void)
{
        bool status = false;
        lteState_t lteStateData;       
        struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0};
		char errorMsg[100] = "";

        clock_gettime(CLOCK_REALTIME, &abs_timeout);
        abs_timeout.tv_sec += MQ_SEND_LTE_STATUS_TIMEOUT;

        CAB_DBG(CAB_GW_TRACE, "In func : %s\n", __func__);
        if(getLteStatusHandlerAliveStatus() == true)
        {
                status = true;
        }

        setLteStatusHandlerAliveStatus(false);

        memset(&lteStateData, 0, sizeof(lteStateData));
        lteStateData.state = WATCHDOG_LTE_STATE;

        if(mq_timedsend(mqLteStateQueue, (const char *)&lteStateData, sizeof(lteState_t), 0, &abs_timeout) < 0)
        {
                CAB_DBG(CAB_GW_WARNING, "mq_timedsend for %s failed with error %s", LTE_STATE_QUEUE_NAME, strerror(errno));
				sprintf(errorMsg, "mq_timedsend for %s failed with error %s", LTE_STATE_QUEUE_NAME, strerror(errno));
				rDiagSendMessage(MESSAGE_QUEUE_OVERFLOW, GW_WARNING, errorMsg);
				status = false;
        }

        return status;
}

static void *lteStatusHandler(void *arg)
{
	ssize_t bytes_read;
	lteState_t lteStateData;
	lteCBData_t cbData = {.eventType = MAX_LTE_CB_TYPE};
	watchDogTimer_t newWatchdogNode;
	returnCode_e retCode = GEN_SUCCESS;
	char errorMsg[100] = "";

	FUNC_ENTRY

	newWatchdogNode.threadId = syscall(SYS_gettid);
	newWatchdogNode.CallbackFn = lteStatusHandlerCallback;

	CAB_DBG(CAB_GW_INFO, "LTE status handler thread id : %ld", syscall(SYS_gettid));
	retCode = threadRegister(&newWatchdogNode);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "LTE status handler thread registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_REGISTRATION_FAIL, GW_ERR, "LTE status handler thread registration failed");
        }

	while(1)
	{
		memset(&lteStateData, 0, sizeof(lteStateData));
		pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
		bytes_read = mq_receive(mqLteStateQueue, (char *)&(lteStateData.state), sizeof(lteStatus_e), NULL);
		pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
		if(bytes_read >= 0)
		{
			switch(lteStateData.state)
			{
				case CONNECTED_LTE_STATE:
					if (g_LTECBFunction != NULL) {
					        cbData.eventType = CONNECTED;
					        g_LTECBFunction(&cbData);
					}
					break;
				case DISCONNECTED_LTE_STATE:
					if (g_LTECBFunction != NULL) {
					        cbData.eventType = DISCONNECTED;
					        g_LTECBFunction(&cbData);
					}
					break;
				case WATCHDOG_LTE_STATE:
					{
						setLteStatusHandlerAliveStatus(true);
					}
					break;
				default:
					break;
			}
		} else {
			CAB_DBG(CAB_GW_WARNING, "mq_rec for %s failed with error %s", LTE_STATE_QUEUE_NAME, strerror(errno));
			sprintf(errorMsg, "mq_rec for %s failed with error %s", LTE_STATE_QUEUE_NAME, strerror(errno));
			rDiagSendMessage(MESSAGE_QUEUE_OVERFLOW, GW_WARNING, errorMsg);
		}
	}

	retCode = threadDeregister(newWatchdogNode.threadId);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "LTE status handler thread de-registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "LTE status handler thread de-registration failed");
        }
	FUNC_EXIT
	return NULL;
}

returnCode_e getLTEInitState(void)
{
        bool state;
        pthread_mutex_lock(&lteModuleMutex);
        state = g_lteStateFlag;
        pthread_mutex_unlock(&lteModuleMutex);
        return state;
}

void setLTEInitState(bool state)
{
        pthread_mutex_lock(&lteModuleMutex);
        g_lteStateFlag = state;
        pthread_mutex_unlock(&lteModuleMutex);

}

returnCode_e initLTE(lteCallback cbFunction)
{
	returnCode_e retCode = GEN_SUCCESS;
	FUNC_ENTRY

	CAB_DBG(CAB_GW_INFO, "Initializing LTE module");

	if (getLTEInitState() == true) {
		CAB_DBG(CAB_GW_ERR, "initLTE is already inited");
		retCode = MODULE_ALREADY_INIT;
		return retCode;
	}

	g_LTECBFunction = cbFunction;

	retCode = initEG91();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "initEG91() fail with error: %d", retCode);
		return retCode;
	}

	retCode = setupMsgQueue();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "setupMsgQueue() fail with error %d", retCode);
		FUNC_EXIT
		return retCode;
	}

	// create thread to receive the command from LTE module
	pthread_create(&lteStatusThreadID_t, NULL, &lteStatusHandler, NULL);

	setLTEInitState(true);

	FUNC_EXIT
	return retCode;
}

returnCode_e deInitLTE()
{
	returnCode_e retCode = GEN_SUCCESS;
	FUNC_ENTRY

	if (getLTEInitState() == false) {
		CAB_DBG(CAB_GW_WARNING, "deInitLTE is already deinited");
		return retCode;
	}

	CAB_DBG(CAB_GW_INFO, "Deinitializing LTE module");

	retCode = sendLteCmd(DEINIT);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "sendLteCmd() for DEINIT state fail with error: %d", retCode);
	}

	retCode = deInitEG91();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "deInitEG91() fail with error: %d", retCode);
		return retCode;
	}

	pthread_cancel(lteStatusThreadID_t);
	//retCode = pthread_join(lteStatusThreadID_t, NULL);
	//if(retCode != GEN_SUCCESS)
	//{
	//	CAB_DBG(CAB_GW_ERR, "Thread joining is getting fail with error %d\r\n", retCode);
	//	FUNC_EXIT
	//	return retCode;
	//}

	/* Queue cleanup */
	mq_close(mqLteStateQueue);
	//mq_unlink(LTE_STATE_QUEUE_NAME);
	mq_close(mqLteCmdQueue);
	//mq_unlink(LTE_CMD_QUEUE_NAME);

	setLTEInitState(false);

	FUNC_EXIT
	return retCode;
}
