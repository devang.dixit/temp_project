/**************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : gpio.h
 * @brief (This file contains the information of includes, macros, enums and API prototypes to
 *          handle GPIOs.)
 *
 * @Author     - VT
 **************************************************************************************************
 * History
 *
 * Aug/24/2018, VT , First Draft
 *************************************************************************************************/

#ifndef _GPIO_H_
#define _GPIO_H_

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include "error.h"

/****************************************
 ************* DEFINES ******************
 ****************************************/
#define GPIO_EXPORT_PATH        ("/sys/class/gpio/export")
#define GPIO_UNEXPORT_PATH      ("/sys/class/gpio/unexport")
#define GPIO_VALUE_PATH         ("/sys/class/gpio/gpio%d/value")
#define GPIO_DIRECTION_PATH     ("/sys/class/gpio/gpio%d/direction")

/****************************************
 ************* TYPEDEFS *****************
 ****************************************/
typedef uint32_t gpioPinNum;

/****************************************
 ************* ENUMS ********************
 ****************************************/
typedef enum gpioDirection {
    IN = 0,         /**< Input direction */
    OUT,            /**< Output direction */
    MAX_GPIO_DIR,   /**< Max number of direction */
} gpioDirection_e;

/****************************************
 ************ STRUCTURES ****************
 ****************************************/

/****************************************
 ******** GLOBAL VARIABLES **************
 ****************************************/

/****************************************
 ******** EXTERN VARIABLES **************
 ****************************************/

/****************************************
 ************* CALLBACKS ****************
 ****************************************/

/****************************************
 ********* FUNCTION PROTOTYPES **********
 ****************************************/
/******************************************************************
 *@brief (To initialize GPIO pins and exports all the required gpio's.)
 *
 *@param[IN] gpioPinNum (It indicates the gpio pin number to export that gpio.)
 *@param[IN] gpioDirection_e (It indicates the gpio pin direction.)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e gpioInit(gpioPinNum , gpioDirection_e);

/******************************************************************
 *@brief (This function gives the current value of gpio pin.)
 *
 *@param[IN] gpioPinNum (Indicates GPIO pin number)
 *@param[OUT] uint8_t* (Indicates pointer to store GPIO data)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e gpioRead(gpioPinNum, uint8_t*);

/******************************************************************
 *@brief (This function writes the value to particular gpio pin received from argument.)
 *
 *@param[IN] gpioPinNum (Indicates GPIO pin number)
 *@param[IN] bool (Indicates value to write)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e gpioWrite(gpioPinNum, bool);

/******************************************************************
 *@brief (To deinitialize GPIO pins and unexports all the exported gpio's.)
 *
 *@param[IN] gpioPinNum (It indicates the gpio pin number to export that gpio.)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e gpioDeInit(gpioPinNum);

#endif      //#ifndef _GPIO_H_
