/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file configuration.c
 * @brief (This file contains APIs defination of configuration module along with database for
 *          for vehicles and LED patterns.)
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * July/19/2018, VT , First Draft
 * April/9/2020, VT , Migtrating config struct to JSON format
 *****************************************************************

 Design change idea for migrating the configuration to the JSON format. Reading and writing mechanism 

                                                   +-------------------+                           +-------------------+
                                                   | Set config event  |                           | Get config event  |
                                                   | from BLE          |                           | from BLE or FW    |
                                                   +---------+---------+                           +---------+---------+
                                                             |                                               |
+------------------------------------------------------------v-----------------------------------------------v------------+
   +-------------------+      +----------------+
   | After flashing    |      | Create JSON    |
   |     the GW        |      | object with    |
   +-------------------+      | default values |
   |                   +------> and initialize +-----+
   |  No configuration |      | config struct  |     |
   |  file exists      |      | with default   |     |
   |                   |      | Value as well  |     |
   +-------------------+      +----------------+     |
                                                     |
   +-------------------+      +----------------+     |
   | GW is configured  |      |                |   +-v-----------------+   +--------------------+  +-------------------+
   +-------------------+      | Migrate the    |   |                   |   | Migrate the        |  | Get the requested |
   |                   |      | existing       |   | Update the config |   | updated config     |  | parameter value   |
   | configuration     +------> config struct  +-->+ struct as per     +-->+ struct to JSON     +->+ from the config   |
   | file exists       |      | to JSON        |   | command           |   | and write it to    |  | struct and return |
   |                   |      |                |   |                   |   | the new config file|  |        back       |
   +-------------------+      +----------------++  +-^-----------------+   +--------------------+  +-------------------+
                                                     |
   +-------------------+      +----------------+     |
   | GW is configured  |      |                |     |
   +-------------------+      | Read the JSON  |     |
   |                   |      | config and     |     |
   | Migrated          +------> initialize the +-----+
   | configuration     |      | config struct  |
   | JSON exists       |      | using it       |
   |                   |      |                |
   +-------------------+      +----------------+
*
***************************************************************/

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include "configuration.h"
#include "commonData.h"
#include "hashTable.h"
#include "debug.h"
#include "cJSON.h"

/****************************************
 ********* VEHICLE DATABASE *************
 ****************************************/
/*vehicleDef_t vehicleType = {maxTyre, axleNum,
                                {sensorID, wheelID(tyrePos, wheelSide, Axle),
                                    {isHaloEnable, attachedType, tyrePos, wheelSide, axleNum}}};
*/
const vehicleDef_t tandemDualTractor = {10, 3, {
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 1, {DISABLE, TYRE, INNER, LEFT, 1}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 17, {DISABLE, TYRE, INNER, RIGHT, 1}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 50, {DISABLE, TYRE, OUTER, RIGHT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 34, {DISABLE, TYRE, OUTER, LEFT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 18, {DISABLE, TYRE, INNER, RIGHT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 2, {DISABLE, TYRE, INNER, LEFT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 51, {DISABLE, TYRE, OUTER, RIGHT, 3}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 35, {DISABLE, TYRE, OUTER, LEFT, 3}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 19, {DISABLE, TYRE, INNER, RIGHT, 3}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 3, {DISABLE, TYRE, INNER, LEFT, 3}},
#ifndef FLEXIBLE_HALO_POSITION
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 114, {DISABLE, HALO, OUTER, RIGHT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 98, {DISABLE, HALO, OUTER, LEFT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 82, {DISABLE, HALO, INNER, RIGHT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 66, {DISABLE, HALO, INNER, LEFT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 115, {DISABLE, HALO, OUTER, RIGHT, 3}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 99, {DISABLE, HALO, OUTER, LEFT, 3}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 83, {DISABLE, HALO, INNER, RIGHT, 3}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 67, {DISABLE, HALO, INNER, LEFT, 3}},
#endif
    }
};

const vehicleDef_t tandemWideTractor = {6, 3, {
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 1, {DISABLE, TYRE, INNER, LEFT, 1}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 17, {DISABLE, TYRE, INNER, RIGHT, 1}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 18, {DISABLE, TYRE, INNER, RIGHT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 2, {DISABLE, TYRE, INNER, LEFT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 19, {DISABLE, TYRE, INNER, RIGHT, 3}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 3, {DISABLE, TYRE, INNER, LEFT, 3}},
#ifndef FLEXIBLE_HALO_POSITION
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 82, {DISABLE, HALO, INNER, RIGHT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 66, {DISABLE, HALO, INNER, LEFT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 83, {DISABLE, HALO, INNER, RIGHT, 3}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 67, {DISABLE, HALO, INNER, LEFT, 3}},
#endif
    }
};

const vehicleDef_t singleDualTractor = {6, 2, {
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 1, {DISABLE, TYRE, INNER, LEFT, 1}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 17, {DISABLE, TYRE, INNER, RIGHT, 1}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 18, {DISABLE, TYRE, INNER, RIGHT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 2, {DISABLE, TYRE, INNER, LEFT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 50, {DISABLE, TYRE, OUTER, RIGHT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 34, {DISABLE, TYRE, OUTER, LEFT, 2}},
#ifndef FLEXIBLE_HALO_POSITION
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 82, {DISABLE, HALO, INNER, RIGHT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 66, {DISABLE, HALO, INNER, LEFT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 114, {DISABLE, HALO, OUTER, RIGHT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 98, {DISABLE, HALO, OUTER, LEFT, 2}},
#endif
   }
};

#ifdef NEW_VEHICLE_TYPE
const vehicleDef_t singleDualDeliveryVan = {6, 2, {
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 1, {DISABLE, TYRE, INNER, LEFT, 1}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 17, {DISABLE, TYRE, INNER, RIGHT, 1}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 18, {DISABLE, TYRE, INNER, RIGHT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 2, {DISABLE, TYRE, INNER, LEFT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 50, {DISABLE, TYRE, OUTER, RIGHT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 34, {DISABLE, TYRE, OUTER, LEFT, 2}},
#if 0
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 82, {DISABLE, HALO, INNER, RIGHT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 66, {DISABLE, HALO, INNER, LEFT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 114, {DISABLE, HALO, OUTER, RIGHT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 98, {DISABLE, HALO, OUTER, LEFT, 2}},
#endif
   }
};

const vehicleDef_t singleSingleDeliveryVan = {4, 2, {
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 1, {DISABLE, TYRE, INNER, LEFT, 1}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 17, {DISABLE, TYRE, INNER, RIGHT, 1}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 18, {DISABLE, TYRE, INNER, RIGHT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 2, {DISABLE, TYRE, INNER, LEFT, 2}},
#if 0
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 82, {DISABLE, HALO, INNER, RIGHT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 66, {DISABLE, HALO, INNER, LEFT, 2}},
#endif
    }
};

const vehicleDef_t singleDualStraightTruck = {6, 2, {
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 1, {DISABLE, TYRE, INNER, LEFT, 1}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 17, {DISABLE, TYRE, INNER, RIGHT, 1}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 18, {DISABLE, TYRE, INNER, RIGHT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 2, {DISABLE, TYRE, INNER, LEFT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 50, {DISABLE, TYRE, OUTER, RIGHT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 34, {DISABLE, TYRE, OUTER, LEFT, 2}},
#ifndef FLEXIBLE_HALO_POSITION
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 82, {DISABLE, HALO, INNER, RIGHT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 66, {DISABLE, HALO, INNER, LEFT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 114, {DISABLE, HALO, OUTER, RIGHT, 2}},
  {0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 98, {DISABLE, HALO, OUTER, LEFT, 2}},
#endif
   }
};

#endif

const vehicleDef_t dummyVehicle = {22, 6, {
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (1 | (LEFT << 4) | (INNER << 5) | (TYRE << 6)), {DISABLE, TYRE, INNER, LEFT,   1}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (1 | (RIGHT << 4) | (INNER << 5) | (TYRE << 6)), {DISABLE, TYRE, INNER, RIGHT, 1}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (2 | (LEFT << 4) | (INNER << 5) | (TYRE << 6)), {DISABLE, TYRE, INNER, LEFT,  2}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (2 | (LEFT << 4) | (OUTER << 5) | (TYRE << 6)), {DISABLE, TYRE, OUTER, LEFT,  2}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (2 | (RIGHT << 4) | (INNER << 5) | (TYRE << 6)), {DISABLE, TYRE, INNER, RIGHT, 2}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (2 | (RIGHT << 4) | (OUTER << 5) | (TYRE << 6)), {DISABLE, TYRE, OUTER, RIGHT, 2}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (3 | (LEFT << 4) | (INNER << 5) | (TYRE << 6)), {DISABLE, TYRE, INNER, LEFT,  3}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (3 | (LEFT << 4) | (OUTER << 5) | (TYRE << 6)), {DISABLE, TYRE, OUTER, LEFT,  3}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (3 | (RIGHT << 4) | (INNER << 5) | (TYRE << 6)), {DISABLE, TYRE, INNER, RIGHT, 3}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (3 | (RIGHT << 4) | (OUTER << 5) | (TYRE << 6)), {DISABLE, TYRE, OUTER, RIGHT, 3}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (4 | (LEFT << 4) | (INNER << 5) | (TYRE << 6)), {DISABLE, TYRE, INNER, LEFT,  4}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (4 | (LEFT << 4) | (OUTER << 5) | (TYRE << 6)), {DISABLE, TYRE, OUTER, LEFT,  4}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (4 | (RIGHT << 4) | (INNER << 5) | (TYRE << 6)), {DISABLE, TYRE, INNER, RIGHT, 4}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (4 | (RIGHT << 4) | (OUTER << 5) | (TYRE << 6)), {DISABLE, TYRE, OUTER, RIGHT, 4}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (5 | (LEFT << 4) | (INNER << 5) | (TYRE << 6)), {DISABLE, TYRE, INNER, LEFT,  5}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (5 | (LEFT << 4) | (OUTER << 5) | (TYRE << 6)), {DISABLE, TYRE, OUTER, LEFT,  5}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (5 | (RIGHT << 4) | (INNER << 5) | (TYRE << 6)), {DISABLE, TYRE, INNER, RIGHT, 5}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (5 | (RIGHT << 4) | (OUTER << 5) | (TYRE << 6)), {DISABLE, TYRE, OUTER, RIGHT, 5}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (6 | (LEFT << 4) | (INNER << 5) | (TYRE << 6)), {DISABLE, TYRE, INNER, LEFT,  6}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (6 | (LEFT << 4) | (OUTER << 5) | (TYRE << 6)), {DISABLE, TYRE, OUTER, LEFT,  6}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (6 | (RIGHT << 4) | (INNER << 5) | (TYRE << 6)), {DISABLE, TYRE, INNER, RIGHT, 6}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (6 | (RIGHT << 4) | (OUTER << 5) | (TYRE << 6)), {DISABLE, TYRE, OUTER, RIGHT, 6}},
#ifndef FLEXIBLE_HALO_POSITION
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (2 | (LEFT << 4) | (INNER << 5) | (HALO << 6)), {DISABLE, HALO, INNER, LEFT,  2}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (2 | (LEFT << 4) | (OUTER << 5) | (HALO << 6)), {DISABLE, HALO, OUTER, LEFT,  2}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (2 | (RIGHT << 4) | (INNER << 5) | (HALO << 6)), {DISABLE, HALO, INNER, RIGHT, 2}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (2 | (RIGHT << 4) | (OUTER << 5) | (HALO << 6)), {DISABLE, HALO, OUTER, RIGHT, 2}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (3 | (LEFT << 4) | (INNER << 5) | (HALO << 6)), {DISABLE, HALO, INNER, LEFT,  3}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (3 | (LEFT << 4) | (OUTER << 5) | (HALO << 6)), {DISABLE, HALO, OUTER, LEFT,  3}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (3 | (RIGHT << 4) | (INNER << 5) | (HALO << 6)), {DISABLE, HALO, INNER, RIGHT, 3}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (3 | (RIGHT << 4) | (OUTER << 5) | (HALO << 6)), {DISABLE, HALO, OUTER, RIGHT, 3}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (4 | (LEFT << 4) | (INNER << 5) | (HALO << 6)), {DISABLE, HALO, INNER, LEFT,  4}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (4 | (LEFT << 4) | (OUTER << 5) | (HALO << 6)), {DISABLE, HALO, OUTER, LEFT,  4}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (4 | (RIGHT << 4) | (INNER << 5) | (HALO << 6)), {DISABLE, HALO, INNER, RIGHT, 4}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (4 | (RIGHT << 4) | (OUTER << 5) | (HALO << 6)), {DISABLE, HALO, OUTER, RIGHT, 4}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (5 | (LEFT << 4) | (INNER << 5) | (HALO << 6)), {DISABLE, HALO, INNER, LEFT,  5}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (5 | (LEFT << 4) | (OUTER << 5) | (HALO << 6)), {DISABLE, HALO, OUTER, LEFT,  5}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (5 | (RIGHT << 4) | (INNER << 5) | (HALO << 6)), {DISABLE, HALO, INNER, RIGHT, 5}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (5 | (RIGHT << 4) | (OUTER << 5) | (HALO << 6)), {DISABLE, HALO, OUTER, RIGHT, 5}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (6 | (LEFT << 4) | (OUTER << 5) | (HALO << 6)), {DISABLE, HALO, OUTER, LEFT,  6}},
	{0, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, (6 | (RIGHT << 4) | (OUTER << 5) | (HALO << 6)), {DISABLE, HALO, OUTER, RIGHT, 6}},
#endif
   }
};
const vehicleDef_t * vehicleDef[MAX_VEHICLE_TYPE] = {&tandemDualTractor, &tandemWideTractor, \
                                                     &singleDualTractor, &singleDualDeliveryVan, \
                                                     &singleSingleDeliveryVan, &singleDualStraightTruck, \
                                                     &dummyVehicle
                                                    };

/****************************************
 ********* LED PATTERN DATABASE *********
 ****************************************/
/** ledPattern_t user_indication = {count, totalTime, onTime, offTime};
                                    (NOTE : All times are in milli seconds)     */

/** TODO : These data base will be upadated later after time interval and count finalized */

const ledPattern_t normal = {NORMAL_TYPE_COUNT, 1000, 500, 500};

const ledPattern_t alert = {ALERT_TYPE_COUNT, 1000, 800, 200};

const ledPattern_t error = {ERROR_TYPE_COUNT, 1000, 1000, 0};

const ledPattern_t * ledPattern[MAX_SYS_STATUS_TYPE] = {&normal, &alert, &error};

/****************************************
 **** DEFAULT TARGET PRESSURE VALUE *****
 ****************************************/
/**< TODO : Default threshold values should be confirm from client */
static const int defPressureThresholdVal[][MAX_AXLE_NO] = {
    [TENDEM_DUAL_TRACTOR] = {100, 100, 100},
    [TENDEM_WIDE_TRACTOR] = {100, 100, 100},
    [SINGLE_DUAL_TRACTOR] = {100, 100},
#ifdef NEW_VEHICLE_TYPE
    [SINGLE_DUAL_DELIVERY_VAN] = {100, 100},
    [SINGLE_SINGLE_DELIVERY_VAN] = {100, 100},
    [SINGLE_DUAL_STRAIGHT_TRUCK] = {100, 100},
#endif
    [DUMMY_VEHICLE] = {100, 100, 100, 100, 100, 100}
};

/****************************************
 ******** STATIC VARIABLES **************
 ****************************************/
static updatedConfigNotifCB g_notifFunPtr;
static const char *vehicleTypeString[] = {
    [TENDEM_DUAL_TRACTOR] = "TENDEM_DUAL_TRACTOR",
    [TENDEM_WIDE_TRACTOR] = "TENDEM_WIDE_TRACTOR",
    [SINGLE_DUAL_TRACTOR] = "SINGLE_DUAL_TRACTOR",
#ifdef NEW_VEHICLE_TYPE
    [SINGLE_DUAL_DELIVERY_VAN] = "SINGLE_DUAL_DELIVERY_VAN",
    [SINGLE_SINGLE_DELIVERY_VAN] = "SINGLE_SINGLE_DELIVERY_VAN",
    [SINGLE_DUAL_STRAIGHT_TRUCK] = "SINGLE_DUAL_STRAIGHT_TRUCK",
#endif
    [DUMMY_VEHICLE] = "DUMMY_VEHICLE",
    [MAX_VEHICLE_TYPE] = ""
};
static const char *vendorTypeString[] = {
    [VENDOR_AVE_TECH] = "VENDOR_AVE_TECH",
    [VENDOR_PRESSURE_PRO] = "VENDOR_PRESSURE_PRO",
    [MAX_VENDORS] = ""
};
static pthread_mutex_t g_lock = PTHREAD_MUTEX_INITIALIZER;  //Initialize mutex variable
static pthread_mutex_t resetStateLock = PTHREAD_MUTEX_INITIALIZER;  //Initialize mutex variable
static cabGatewayConfig_t g_gatewayConfig;      //Declare cab gateway configuration structure
static char *g_gatewayConfigJSON = NULL;        //Declare JSON cab gateway configuration struture
static cabGatewayStatus_t g_gatewayConfigStatus; //Declare cab gateway configuration status structure
static selfDiagReq_t g_selfDiagReq;	//Declare self diagnostics request structure
static selfDiagStatus_t g_selfDiagStatus;	//Declare self diagnostics status structure
static bool isGatewayConfigured = false;        //to get status of gateway configuration
static bool resetState = false;        //Gateway reboot request has been sent or not

static uint8_t g_defaultBLEMAC[MAX_LEN_BLE_MAC] = DEFAULT_BLE_MAC;
static uint8_t g_defaultLTEIMEI[MAX_LEN_LTE_IMEI] = DEFAULT_LTE_IMEI;

static void dumpConfig(); //  Utility function to print current config strucutre snapshot

/****************************************
 ******** FUNCTION DEFINATIONS **********
 ****************************************/
/******************************************************************
 *@brief  (It is used get index or type of vehicle from string of vehicle type)
 *
 *@param[IN] char* (Indicates pointer to string of vehicle type)
 *@param[OUT] None
 *
 *@return int8_t (It returns the index of vehicle type if data matches else -1)
 *********************************************************************/
static int8_t getVehType(char* data)
{
    FUNC_ENTRY

    int8_t loopCnt;
    for(loopCnt = 0; loopCnt < MAX_VEHICLE_TYPE; loopCnt++) {
	CAB_DBG(CAB_GW_DEBUG, "%d - %s", loopCnt, vehicleTypeString[loopCnt]);
        if(strcmp(vehicleTypeString[loopCnt], data) == 0) {
            return loopCnt;
        }
    }

    FUNC_EXIT
    /*If no data match then returns -1*/
    return -1;
}

bool isMemSame(char *s1, char *s2, uint16_t len)
{
	uint16_t checkInd = 0;
	FUNC_ENTRY
	for (checkInd = 0; checkInd < len; checkInd++) {
		if (s1[checkInd] != s2[checkInd]) {
			return false;
		}
	}
	FUNC_EXIT
	return true;
}

/******************************************************************
 *@brief  (It is used get index or type of sensor vendor from string of vendor type)
 *
 *@param[IN] char* (Indicates pointer to string of vendor type)
 *@param[OUT] None
 *
 *@return int8_t (It returns the index of vendor type if data matches else -1)
 *********************************************************************/
static int8_t getVendorType(char* data)
{
    FUNC_ENTRY

    int8_t loopCnt;
    for(loopCnt = 0; loopCnt < MAX_VENDORS; loopCnt++) {
        if(strcmp(vendorTypeString[loopCnt], data) == 0) {
            return loopCnt;
        }
    }

    FUNC_EXIT
    /*If no data match then returns -1*/
    return -1;
}

/*************************************************************************
 *@brief  (It is used to create wheel id from data of wheel position)
 *
 *@param[IN] void * (Indicates pointer to wheel position information)
 *@param[OUT] None
 *
 *@return uint16_t (It returns the wheel id which is generated from provided wheel position)
 *********************************************************************/
static uint16_t getWheelId(void * data)
{
    FUNC_ENTRY

    tpmsInfo_t tempData = *(tpmsInfo_t*)data;
    uint16_t wheelId = 0;
    wheelId = (tempData.axleNum) | (tempData.vehicleSide << 4) | (tempData.tyrePos << 5) \
              | (tempData.attachedType << 6);

    FUNC_EXIT
    return wheelId;
}

/*************************************************************************
 *@brief  (It is used to set up the supporting parameters for HALO SN 
 *         config. These parameters will be needed to assign the HALO SN
 *         to the requested position.)
 *
 *@param[IN] None
 *@param[OUT] None
 *
 *@return None
 *********************************************************************/
static void setHaloSNConfig (void) 
{
    FUNC_ENTRY

    /* Set HALO SN config */
    for(uint8_t loopCnt = 0; loopCnt < MAX_HALO_SN; loopCnt=loopCnt+2) {
        for(uint8_t i=0; i < MAX_WHEEL_SIDE; i++) {
            strncpy(g_gatewayConfig.haloSNConfig[loopCnt+i].serialNo, DEFAULT_HALO_SN, MAX_LEN_HALO_SN);
            if (loopCnt < g_gatewayConfig.vehicleDef_s.maxAxle*2) {
                g_gatewayConfig.haloSNConfig[loopCnt+i].vehicleSide = i;
                g_gatewayConfig.haloSNConfig[loopCnt+i].axleNum = (loopCnt/2)+1;
            } else {
                g_gatewayConfig.haloSNConfig[loopCnt+i].vehicleSide = 0;
                g_gatewayConfig.haloSNConfig[loopCnt+i].axleNum = 0;
            }
        }
    }

    FUNC_EXIT
}

/*************************************************************************
 *@brief  (It is used to set up the supporting parameters for tyre 
 *         config. These parameters will be needed to assign the tyre details
 *         to the requested position.)
 *
 *@param[IN] None
 *@param[OUT] None
 *
 *@return None
 *********************************************************************/
static void setTyreConfig (void)
{
    FUNC_ENTRY

    /* First 2 entries are for 1st axle which have only 2 tyres */
    for(uint8_t loopCnt = 0; loopCnt < 2; loopCnt++) {
        g_gatewayConfig.tyreConfig[loopCnt].axleNum = (loopCnt/2)+1;
        g_gatewayConfig.tyreConfig[loopCnt].vehicleSide = loopCnt;
        g_gatewayConfig.tyreConfig[loopCnt].tyrePos = 0;
        g_gatewayConfig.tyreConfig[loopCnt].isTyreDetailConfigured = DEFAULT_TYRE_DETAILS_STATUS;

        strncpy(g_gatewayConfig.tyreConfig[loopCnt].tyreSerialNo, DEFAULT_TYRE_SERIAL_NO, MAX_LEN_TYRE_SERIAL_NO);
        strncpy(g_gatewayConfig.tyreConfig[loopCnt].tyreTIN, DEFAULT_TYRE_TIN, MAX_LEN_TYRE_TIN);
        strncpy(g_gatewayConfig.tyreConfig[loopCnt].tyreMake, DEFAULT_TYRE_MAKE, MAX_LEN_TYRE_MAKE_STR);
        strncpy(g_gatewayConfig.tyreConfig[loopCnt].tyreModel, DEFAULT_TYRE_MODEL, MAX_LEN_TYRE_MODEL);
        strncpy(g_gatewayConfig.tyreConfig[loopCnt].tyreWidth, DEFAULT_TYRE_WIDTH, MAX_LEN_TYRE_WIDTH);
        strncpy(g_gatewayConfig.tyreConfig[loopCnt].ratio, DEFAULT_RATIO, MAX_LEN_RATIO);
        strncpy(g_gatewayConfig.tyreConfig[loopCnt].diameter, DEFAULT_DIAMETER, MAX_LEN_DIAMETER);
        strncpy(g_gatewayConfig.tyreConfig[loopCnt].loadRating, DEFAULT_LOAD_RATING, MAX_LEN_LOAD_RATING);

        for(uint8_t j=0; j<MAX_TREAD_DEPTH_RECORD; j++) {
            strncpy(g_gatewayConfig.tyreConfig[loopCnt].treadDepthHistory[j].treadDepthValue, \
                DEFAULT_TREAD_DEPTH, MAX_LEN_TREAD_DEPTH);
            g_gatewayConfig.tyreConfig[loopCnt].treadDepthHistory[j].vehicleMileage = DEFAULT_VEHICLE_MILEAGE;
            g_gatewayConfig.tyreConfig[loopCnt].treadDepthHistory[j].recordTime = DEFAULT_RECORD_TIME;
        }
    }

    /* Set Rest of them as per 4 tyres per Axle */
    for(uint8_t loopCnt = 2; loopCnt < MAX_TYRE_NO; loopCnt=loopCnt+4) {
        for(uint8_t i=0; i < MAX_WHEEL_SIDE*2; i=i+2) {
            for(uint8_t k=0; k < MAX_TYRE_POS; k++) {
                if (loopCnt < (g_gatewayConfig.vehicleDef_s.maxAxle*4)-2) {
                    g_gatewayConfig.tyreConfig[loopCnt+i+k].vehicleSide = (i==2) ? 1 : 0;
                    g_gatewayConfig.tyreConfig[loopCnt+i+k].axleNum = (loopCnt/4)+2; // Offset is 2 as 1st axle is done
                    g_gatewayConfig.tyreConfig[loopCnt+i+k].tyrePos = k%2;
                } else {
                    g_gatewayConfig.tyreConfig[loopCnt+i+k].vehicleSide = 0;
                    g_gatewayConfig.tyreConfig[loopCnt+i+k].axleNum = 0;
                    g_gatewayConfig.tyreConfig[loopCnt+i+k].tyrePos = 0;
                }

                g_gatewayConfig.tyreConfig[loopCnt+i+k].isTyreDetailConfigured = DEFAULT_TYRE_DETAILS_STATUS;
                strncpy(g_gatewayConfig.tyreConfig[loopCnt+i+k].tyreSerialNo, DEFAULT_TYRE_SERIAL_NO, MAX_LEN_TYRE_SERIAL_NO);
                strncpy(g_gatewayConfig.tyreConfig[loopCnt+i+k].tyreTIN, DEFAULT_TYRE_TIN, MAX_LEN_TYRE_TIN);
                strncpy(g_gatewayConfig.tyreConfig[loopCnt+i+k].tyreMake, DEFAULT_TYRE_MAKE, MAX_LEN_TYRE_MAKE_STR);
                strncpy(g_gatewayConfig.tyreConfig[loopCnt+i+k].tyreModel, DEFAULT_TYRE_MODEL, MAX_LEN_TYRE_MODEL);
                strncpy(g_gatewayConfig.tyreConfig[loopCnt+i+k].tyreWidth, DEFAULT_TYRE_WIDTH, MAX_LEN_TYRE_WIDTH);
                strncpy(g_gatewayConfig.tyreConfig[loopCnt+i+k].ratio, DEFAULT_RATIO, MAX_LEN_RATIO);
                strncpy(g_gatewayConfig.tyreConfig[loopCnt+i+k].diameter, DEFAULT_DIAMETER, MAX_LEN_DIAMETER);
                strncpy(g_gatewayConfig.tyreConfig[loopCnt+i+k].loadRating, DEFAULT_LOAD_RATING, MAX_LEN_LOAD_RATING);

                for(uint8_t j=0; j<MAX_TREAD_DEPTH_RECORD; j++) {
                    strncpy(g_gatewayConfig.tyreConfig[loopCnt+i+k].treadDepthHistory[j].treadDepthValue, \
                        DEFAULT_TREAD_DEPTH, MAX_LEN_TREAD_DEPTH);
                    g_gatewayConfig.tyreConfig[loopCnt+i+k].treadDepthHistory[j].vehicleMileage = DEFAULT_VEHICLE_MILEAGE;
                    g_gatewayConfig.tyreConfig[loopCnt+i+k].treadDepthHistory[j].recordTime = DEFAULT_RECORD_TIME;
                }
            }
        }
    }

    FUNC_EXIT
}

/*************************************************************************
 *@brief  (It is used to set up the new configuration parameters before 
 *         migrating to JSON. This helps to plug and configure the new
 *         parameters on the fly.)
 *
 *@param[IN] None
 *@param[OUT] None
 *
 *@return None
 *********************************************************************/
static void initializeNewParameters()
{
    FUNC_ENTRY

    /* @TODO: Once the config is switched to JSON, this path will
                not be hit. So we can remove the function. */

    /* If mandatory information is available, then we need to set up the 
       supporting parameters which will be needed at the time of setting up
       the actual configuration value */

    if (g_gatewayConfig.isMandatoryInfoAvailable) {
        setHaloSNConfig();
        setTyreConfig();
    }

    FUNC_EXIT
}

/******************************************************************
 *@brief  (It is used to add sensor id for given tyre id and vehicle type)
 *
 *@param[IN] void * (Indicates pointer to configuration parameter which comes from BLE module)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e addSensorID(void * param)
{
    FUNC_ENTRY

    uint8_t index, loopCnt, isWheelIdMatch;             //Take index to get index of wheel ID
    tpmsConfig_t tpmsMap;

    /*Check if axle number is supported for selected vehicle type*/
    if((*(tpmsInfo_t*)param).axleNum > g_gatewayConfig.vehicleDef_s.maxAxle){
	CAB_DBG(CAB_GW_ERR, "Invalid TPMS. Axle number not supported for selected vehicle type");
	return CONFIG_TPMS_FAIL;
    }

    /*Check for sensor id 0xffffffff*/
    if(((*(tpmsInfo_t*)param).sensorID) == DUMMY_DATA){
        CAB_DBG(CAB_GW_ERR, "Invalid Sensor Id for TPMS");
        return CONFIG_TPMS_FAIL;
    }

    /*Initialize TMPS structure*/
    memset(&tpmsMap, 0, sizeof(tpmsMap));

    tpmsMap.sensorID = (*(tpmsInfo_t*)param).sensorID;
    tpmsMap.wheelID = getWheelId(param);
    isWheelIdMatch = false;
    for(loopCnt = 0; ((loopCnt < MAX_TPMS) && \
                      (g_gatewayConfig.vehicleDef_s.tpmsConfig[loopCnt].wheelID != 0)); loopCnt++) {
        if(g_gatewayConfig.vehicleDef_s.tpmsConfig[loopCnt].wheelID == tpmsMap.wheelID) {
            /* Check for wheel ID paired status */
            if(g_gatewayConfig.vehicleDef_s.tpmsConfig[loopCnt].sensorID == 0) {
                index = loopCnt;           //Index value for given wheel ID
                isWheelIdMatch = true;
            } else {
                CAB_DBG(CAB_GW_ERR, "Given wheel ID is already paired with TPMS sensor");
                return CONFIG_WHEEL_PAIRED;
            }
        }
        /* Check for sensor ID paired status */
        if(g_gatewayConfig.vehicleDef_s.tpmsConfig[loopCnt].sensorID == tpmsMap.sensorID) {
            CAB_DBG(CAB_GW_ERR, "Given sensor ID is already paired");
            return CONFIG_SENSOR_ID_PAIRED;
        }
    }
#ifdef FLEXIBLE_HALO_POSITION
    if(loopCnt == MAX_TPMS && isWheelIdMatch == false) {
        CAB_DBG(CAB_GW_ERR, "NO more TPMS configured as it reaches maximum limit");
        return CONFIG_TPMS_FAIL;
    }
    if(isWheelIdMatch == false) {
        if(((*(tpmsInfo_t*)param).attachedType == TYRE) ||  \
                ((*(tpmsInfo_t*)param).axleNum > g_gatewayConfig.vehicleDef_s.maxAxle)) {
            CAB_DBG(CAB_GW_ERR, "No more TPMS configured as there is data mismatch");
            return CONFIG_TPMS_FAIL;
        }
        index = loopCnt;            //Index value for given wheel ID
        g_gatewayConfig.vehicleDef_s.tpmsConfig[index].wheelID = tpmsMap.wheelID;
        g_gatewayConfig.vehicleDef_s.tpmsConfig[index].wheelInfo.tyrePos = \
                (*(tpmsInfo_t*)param).tyrePos;
        g_gatewayConfig.vehicleDef_s.tpmsConfig[index].wheelInfo.vehicleSide = \
                (*(tpmsInfo_t*)param).vehicleSide;
        g_gatewayConfig.vehicleDef_s.tpmsConfig[index].wheelInfo.axleNum =  \
                (*(tpmsInfo_t*)param).axleNum;
        g_gatewayConfig.vehicleDef_s.tpmsConfig[index].wheelInfo.attachedType = \
                (*(tpmsInfo_t*)param).attachedType;
    }
    g_gatewayConfig.vehicleDef_s.tpmsConfig[index].sensorID = tpmsMap.sensorID;
    strncpy(g_gatewayConfig.vehicleDef_s.tpmsConfig[index].sensorPN, *(tpmsInfo_t*)param.sensorPN, MAX_LEN_SENSOR_PART_NO);
    strncpy(g_gatewayConfig.vehicleDef_s.tpmsConfig[index].sensorModel, *(tpmsInfo_t*)param.sensorModel, MAX_LEN_SENSOR_MODEL);
    g_gatewayConfig.vehicleDef_s.tpmsConfig[index].wheelInfo.isHaloEnable = \
            (*(tpmsInfo_t*)param).isHaloEnable;
    memset(&tpmsMap, 0, sizeof(tpmsMap));             //Reset the TPMS structure
#else
    if(isWheelIdMatch == false){
	CAB_DBG(CAB_GW_ERR, "Either Invalid TPMS data as per vehicle type definition OR max TPMS limimt over");
        return CONFIG_TPMS_FAIL;
    }
    else{
	    g_gatewayConfig.vehicleDef_s.tpmsConfig[index].sensorID = tpmsMap.sensorID;
	    g_gatewayConfig.vehicleDef_s.tpmsConfig[index].wheelInfo.isHaloEnable = \
            (*(tpmsInfo_t*)param).isHaloEnable;
      strncpy(g_gatewayConfig.vehicleDef_s.tpmsConfig[index].sensorPN, (*(tpmsInfo_t*)param).sensorPN, MAX_LEN_SENSOR_PART_NO);
      strncpy(g_gatewayConfig.vehicleDef_s.tpmsConfig[index].sensorModel, (*(tpmsInfo_t*)param).sensorModel, MAX_LEN_SENSOR_MODEL);
    }
#endif

    /** To notify to get updated sensor data */
    (*g_notifFunPtr)(ADD_SENSOR_ID, &g_gatewayConfig.vehicleDef_s.tpmsConfig[index]);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (It is used to attach HALO SN for axle number and vehicle side)
 *
 *@param[IN] void * (Indicates pointer to configuration parameter which comes from BLE module)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e attachHaloSN(void * param)
{
    FUNC_ENTRY
    uint8_t index, loopCnt, isPosMatch=false;             //Take index to get the position
    haloSNInfo_t *tempHaloSNInfo = (haloSNInfo_t*) param;

    if (tempHaloSNInfo->axleNum > g_gatewayConfig.vehicleDef_s.maxAxle) {
        CAB_DBG(CAB_GW_ERR, "Invalid HALO SN. Axle number not supported for \
            selected vehicle type");
        pthread_mutex_unlock(&g_lock);
        return CONFIG_HALO_SN_FAIL;
    }

    if (strncmp(tempHaloSNInfo->serialNo, DEFAULT_HALO_SN, MAX_LEN_HALO_SN) == 0) {
        CAB_DBG(CAB_GW_ERR, "Invalid HALO Serial number");
        pthread_mutex_unlock(&g_lock);
        return CONFIG_HALO_SN_FAIL;
    }

    for (loopCnt = 0; ((loopCnt < MAX_HALO_SN) && \
        (g_gatewayConfig.haloSNConfig[loopCnt].axleNum != 0)); loopCnt++) {

        if (tempHaloSNInfo->axleNum == g_gatewayConfig.haloSNConfig[loopCnt].axleNum && \
            tempHaloSNInfo->vehicleSide == g_gatewayConfig.haloSNConfig[loopCnt].vehicleSide) {
            if (strncmp(g_gatewayConfig.haloSNConfig[loopCnt].serialNo, DEFAULT_HALO_SN, MAX_LEN_HALO_SN) == 0) {
                index = loopCnt;
                isPosMatch = true;
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Given tire position is attached with HALO SN");
                return CONFIG_TIRE_POS_ATTACHED;
            }
        }

        if (strncmp(g_gatewayConfig.haloSNConfig[loopCnt].serialNo, tempHaloSNInfo->serialNo, MAX_LEN_HALO_SN) == 0) {
            CAB_DBG(CAB_GW_ERR, "HALO Serial number already attached");
            pthread_mutex_unlock(&g_lock);
            return CONFIG_HALO_SN_ATTACHED;
        }
    }

    if(isPosMatch == false){
        CAB_DBG(CAB_GW_ERR, "Either Invalid data as per vehicle type");
        return CONFIG_HALO_SN_FAIL;
    }
    else{
        strncpy(g_gatewayConfig.haloSNConfig[index].serialNo, tempHaloSNInfo->serialNo, MAX_LEN_HALO_SN);
        CAB_DBG(CAB_GW_DEBUG, "Attached HALO SN : %s", g_gatewayConfig.haloSNConfig[index].serialNo);
    }

    /* To notify to get updated data */
    (*g_notifFunPtr)(ATTACH_HALO_SN, &g_gatewayConfig.haloSNConfig[index]);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (It is used to update tyre info for given tyre position)
 *
 *@param[IN] void * (Indicates pointer to configuration parameter which comes from BLE module)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e updateTyreInfo(void * param)
{
    FUNC_ENTRY
    uint8_t index, loopCnt, isPosMatch=false;             //Take index to get the position
    tyreInfo_t *tempTyreInfo = (tyreInfo_t*) param;

    if (tempTyreInfo->axleNum > g_gatewayConfig.vehicleDef_s.maxAxle) {
        CAB_DBG(CAB_GW_ERR, "Invalid Tyre information. Axle number not supported for \
            selected vehicle type");
        pthread_mutex_unlock(&g_lock);
        return CONFIG_TYRE_DETAIL_FAIL;
    }
    /* Make sure not a single tyre detail is missed exept serial No and TIN as they are optional */
    if (strncmp(tempTyreInfo->tyreMake, DEFAULT_TYRE_MAKE, MAX_LEN_TYRE_MAKE_STR) == 0 || \
        strncmp(tempTyreInfo->tyreModel, DEFAULT_TYRE_MODEL, MAX_LEN_TYRE_MODEL) == 0 || \
        strncmp(tempTyreInfo->tyreWidth, DEFAULT_TYRE_WIDTH, MAX_LEN_TYRE_WIDTH) == 0 || \
        strncmp(tempTyreInfo->ratio, DEFAULT_RATIO, MAX_LEN_RATIO) == 0 || \
        strncmp(tempTyreInfo->diameter, DEFAULT_DIAMETER, MAX_LEN_DIAMETER) == 0 || \
        strncmp(tempTyreInfo->loadRating, DEFAULT_LOAD_RATING, MAX_LEN_LOAD_RATING) == 0 || \
        strncmp(tempTyreInfo->treadDepthHistory[0].treadDepthValue, DEFAULT_TREAD_DEPTH, MAX_LEN_TREAD_DEPTH) == 0 || \
        tempTyreInfo->treadDepthHistory[0].vehicleMileage == 0) {
        CAB_DBG(CAB_GW_ERR, "Invalid Tire Details");
        pthread_mutex_unlock(&g_lock);
        return CONFIG_TYRE_DETAIL_FAIL;
    }

    for (loopCnt = 0; ((loopCnt < MAX_TYRE_NO) && \
      (g_gatewayConfig.tyreConfig[loopCnt].axleNum != 0)); loopCnt++) {
        /* Confirm the tyre postion */
        if (tempTyreInfo->axleNum == g_gatewayConfig.tyreConfig[loopCnt].axleNum && \
            tempTyreInfo->vehicleSide == g_gatewayConfig.tyreConfig[loopCnt].vehicleSide && \
            tempTyreInfo->tyrePos == g_gatewayConfig.tyreConfig[loopCnt].tyrePos) {
            if (!g_gatewayConfig.tyreConfig[loopCnt].isTyreDetailConfigured) {
                index = loopCnt;
                isPosMatch = true;
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Given Tyre position already has tyre details");
                return CONFIG_TIRE_POS_ATTACHED;
            }
        }
        /* If the serial No/TIN don't have default value and the similar value are provided then it is a problem */
        if (g_gatewayConfig.tyreConfig[loopCnt].isTyreDetailConfigured && \
            (strncmp(tempTyreInfo->tyreSerialNo, DEFAULT_TYRE_SERIAL_NO, MAX_LEN_TYRE_SERIAL_NO) != 0 && \
            strncmp(g_gatewayConfig.tyreConfig[loopCnt].tyreSerialNo, tempTyreInfo->tyreSerialNo, \
            MAX_LEN_TYRE_SERIAL_NO) == 0) && \
            (strncmp(tempTyreInfo->tyreTIN, DEFAULT_TYRE_TIN, MAX_LEN_TYRE_TIN) != 0 && \
            strncmp(g_gatewayConfig.tyreConfig[loopCnt].tyreTIN, tempTyreInfo->tyreTIN, \
            MAX_LEN_TYRE_TIN) == 0)) {
            CAB_DBG(CAB_GW_ERR, "Tyre serial number/TIN is already attached");
            pthread_mutex_unlock(&g_lock);
            return CONFIG_TIRE_POS_ATTACHED;
        }
    }

    if(isPosMatch == false){
        CAB_DBG(CAB_GW_ERR, "Invalid tire details");
        return CONFIG_TYRE_DETAIL_FAIL;
    }
    else{
        memcpy(&g_gatewayConfig.tyreConfig[index], tempTyreInfo, sizeof(tyreInfo_t));
        /* Record the timestamp */
        g_gatewayConfig.tyreConfig[index].treadDepthHistory[0].recordTime = time(NULL);
        g_gatewayConfig.tyreConfig[index].isTyreDetailConfigured = true;
        CAB_DBG(CAB_GW_DEBUG, "Tire Details updated");
    }

    /* To notify to get updated data */
    (*g_notifFunPtr)(UPDATE_TYRE_DETAILS, &g_gatewayConfig.tyreConfig[index]);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (It is used to tread depth history for given tyre position)
 *
 *@param[IN] void * (Indicates pointer to configuration parameter which comes from BLE module)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e updateTreadDepthHistory(void * param)
{
    FUNC_ENTRY

    uint8_t index, loopCnt;
    treadRecordInfo_t *treadRecordInfo = (treadRecordInfo_t *) param;

    if (treadRecordInfo->tyrePosInfo.axleNum > g_gatewayConfig.vehicleDef_s.maxAxle) {
        CAB_DBG(CAB_GW_ERR, "Invalid Tyre information. Axle number not supported for \
            selected vehicle type");
        pthread_mutex_unlock(&g_lock);
        return CONFIG_TYRE_DETAIL_FAIL;
    }

    if (strncmp(treadRecordInfo->treadDepthInfo.treadDepthValue, DEFAULT_TREAD_DEPTH, MAX_LEN_TREAD_DEPTH) == 0 || \
        treadRecordInfo->treadDepthInfo.vehicleMileage == DEFAULT_VEHICLE_MILEAGE) {
        CAB_DBG(CAB_GW_ERR, "Invalid Tread depth Details");
        pthread_mutex_unlock(&g_lock);
        return CONFIG_TYRE_DETAIL_FAIL;
    }

    for (loopCnt = 0; ((loopCnt < MAX_TYRE_NO) && \
        (g_gatewayConfig.tyreConfig[loopCnt].axleNum != 0)); loopCnt++) {
        /* Confirm the tyre postion */
        if (treadRecordInfo->tyrePosInfo.axleNum == g_gatewayConfig.tyreConfig[loopCnt].axleNum && \
            treadRecordInfo->tyrePosInfo.vehicleSide == g_gatewayConfig.tyreConfig[loopCnt].vehicleSide && \
            treadRecordInfo->tyrePosInfo.tyrePos == g_gatewayConfig.tyreConfig[loopCnt].tyrePos) {
            index = loopCnt;

            /* Shift the existing record to right and enter new record at the top */
            for (uint8_t i = MAX_TREAD_DEPTH_RECORD-1; i > 0; i--) {
                g_gatewayConfig.tyreConfig[loopCnt].treadDepthHistory[i] = \
                    g_gatewayConfig.tyreConfig[loopCnt].treadDepthHistory[i-1];
            }

            memcpy(&g_gatewayConfig.tyreConfig[loopCnt].treadDepthHistory[0], \
                &treadRecordInfo->treadDepthInfo, sizeof(treadDepthInfo_t));
            /* Record the timestamp */
            g_gatewayConfig.tyreConfig[loopCnt].treadDepthHistory[0].recordTime = time(NULL);
            break;
        }
    }

    /* To notify to get updated data */
    (*g_notifFunPtr)(UPDATE_TREAD_DEPTH_VALUE, &g_gatewayConfig.tyreConfig[index]);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (This API is used to read configuration parameters from configuration storage file which
 *          contains all configurations)
 *
 *@param[IN] None
 *@param[OUT] cabGatewayConfig_t* (Pointer to structure of configuration parameter data)
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e readConfigParam(cabGatewayConfig_t * data)
{
    FUNC_ENTRY

    int file;                               //file descriptor
    uint32_t ret;

    /*Input argument validation*/
    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //opening Configuration parameter file for read
    file = open(FILE_PATH_CONFIG_PARAM, O_RDONLY, 0644);
    if(file == -1) {
        CAB_DBG(CAB_GW_ERR, "%s file opening failed.", FILE_PATH_CONFIG_PARAM);
        return FILE_OPEN_FAIL;
    }

    //reading Configuration parameter structure data from file
    ret = read(file, data, sizeof(cabGatewayConfig_t));
    if(ret == -1) {
        CAB_DBG(CAB_GW_ERR, "%s file reading failed.", FILE_PATH_CONFIG_PARAM);
        close(file);
        return FILE_READ_ERROR;
    }
    //close Configuration parameter file
    close(file);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (This API is used to read configuration parameters from configuration storage file which
 *          contains all configurations)
 *
 *@param[IN] None
 *@param[OUT] char * (Pointer to string of configuration parameter JSON object)
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e readConfigJSON(char * data)
{
    FUNC_ENTRY

    FILE *fp;  //file descriptor
    long length = 0;
    uint32_t ret;

    /*Input argument validation*/
    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //opening Configuration parameter file for read
    fp = fopen(NEW_FILE_PATH_CONFIG_PARAM, "rb");
    if(fp == NULL) {
        CAB_DBG(CAB_GW_ERR, "%s file opening failed.", NEW_FILE_PATH_CONFIG_PARAM);
        return FILE_OPEN_FAIL;
    }

    if(fseek(fp, 0, SEEK_END) != 0) {
        CAB_DBG(CAB_GW_ERR, "%s file seeking end failed.", NEW_FILE_PATH_CONFIG_PARAM);
        fclose(fp);
        return FILE_OPEN_FAIL;
    }

    length = ftell(fp);
    if (length < 0) {
        CAB_DBG(CAB_GW_ERR, "%s file getting length failed.", NEW_FILE_PATH_CONFIG_PARAM);
        fclose(fp);
        return FILE_OPEN_FAIL;
    }

    if (fseek(fp, 0, SEEK_SET) != 0) {
        CAB_DBG(CAB_GW_ERR, "%s file seeking start failed.", NEW_FILE_PATH_CONFIG_PARAM);
        fclose(fp);
        return FILE_OPEN_FAIL;
    }

    //reading Configuration parameter JSON data from file
    ret = fread(data, sizeof(char), (size_t)length, fp);
    if((long)ret != length) {
        CAB_DBG(CAB_GW_ERR, "%s file reading failed.", NEW_FILE_PATH_CONFIG_PARAM);
        fclose(fp);
        return FILE_READ_ERROR;
    }

    data[ret] = '\0';

    //close Configuration parameter file
    fclose(fp);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (This API is used to read configuration status parameters from configuration status file which
 *          contains all configurations)
 *
 *@param[IN] None
 *@param[OUT] cabGatewayStatus_t* (Pointer to structure of configuration status data)
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e readConfigStatus(cabGatewayStatus_t * data)
{
    FUNC_ENTRY

    int file;                               //file descriptor
    uint32_t ret;

    /*Input argument validation*/
    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //opening Configuration status file for read
    file = open(FILE_PATH_STATUS_PARAM, O_RDONLY, 0644);
    if(file == -1) {
        CAB_DBG(CAB_GW_ERR, "%s file opening failed.", FILE_PATH_STATUS_PARAM);
        return FILE_OPEN_FAIL;
    }

    //reading Configuration status structure data from file
    ret = read(file, data, sizeof(cabGatewayStatus_t));
    if(ret != sizeof(cabGatewayStatus_t)) {
        CAB_DBG(CAB_GW_ERR, "%s file reading failed.", FILE_PATH_STATUS_PARAM);
        close(file);
        return FILE_READ_ERROR;
    }
    //close Configuration parameter file
    close(file);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (This API is used to write configuration parameters from 
 *          configuration storage file which contains all configurations)
 *
 *@param[IN] char * (Pointer to string of configuration parameter JSON object)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e writeConfigJSON(char * data)
{
    FUNC_ENTRY

    FILE *fp;   //file descriptor
    uint32_t ret;

    /*Input argument validation*/
    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //opening Configuration parameter file for write
    fp = fopen(NEW_FILE_PATH_CONFIG_PARAM, "w");
    if(fp == NULL) {
        CAB_DBG(CAB_GW_ERR, "%s file opening failed.", NEW_FILE_PATH_CONFIG_PARAM);
        return FILE_OPEN_FAIL;
    }
    
    // writing Configuration parameter JSON data to file
    ret = fprintf(fp, "%s\n", data);
    if(ret < 0) {
        CAB_DBG(CAB_GW_ERR, "%s file writing failed.", NEW_FILE_PATH_CONFIG_PARAM);
        fclose(fp);
        return FILE_WRITE_ERROR;
    }

    fflush(fp);
    fsync(fileno(fp));
    //close Configuration parameter file
    fclose(fp);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (This API is used to write configuration status parameters to configuration status file which
 *          contains all configurations status)
 *
 *@param[IN] cabGatewayStatus_t* (Pointer to structure of configuration status data)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e writeConfigStatus(cabGatewayStatus_t * data)
{
    FUNC_ENTRY

    int file;                               //file descriptor
    uint32_t ret;

    /*Input argument validation*/
    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //opening Configuration status file for write
    file = open(FILE_PATH_STATUS_PARAM, O_WRONLY | O_CREAT, 0644);
    if(file == -1) {
        CAB_DBG(CAB_GW_ERR, "%s file opening failed.", FILE_PATH_STATUS_PARAM);
        return FILE_OPEN_FAIL;
    }

    //writing Configuration status structure data to file
    ret = write(file, data, sizeof(cabGatewayStatus_t));
	/*check ret with no of bytes structures*/
    if(ret != sizeof(cabGatewayStatus_t)) {
        CAB_DBG(CAB_GW_ERR, "%s file writing failed.", FILE_PATH_STATUS_PARAM);
        close(file);
        return FILE_WRITE_ERROR;
    }

    fsync(file);
    //close event summary file
    close(file);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (This API is used to add the new config parameters to 
 *         existing JSON configuration.
 *
 *@param[IN] char * (The ponter to current JSON object string)
 *@param[OUT] 
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e addNewConfigParam(char * JSONData)
{
    /* @TODO: Once the update is rolled out with new config paramters, this function should be cleaned up.
                Remove all the changes for previously added paramters.
                Please maintain the history so that the changes in function can be tracked. */

    /* 3.9.0.1 - Config migrated to JSON */
    /* 3.9.0.2 - Added HALO SN config to JSON */
    /* 3.9.0.7 - Added Tyre config to JSON */
    /* 3.11.0.1 - Added part number and model number to tpms config JSON */

    FUNC_ENTRY

    cJSON *root = NULL;
    cJSON *item = NULL;
    cJSON *tpmsConfig = NULL;
    cJSON *pn = NULL;
    cJSON *mn = NULL;
    cJSON *array = NULL;
    char *payload = NULL;
    bool configUpdated = false;

    if (!JSONData) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    root = cJSON_Parse(JSONData);

    if (!root) {
        CAB_DBG(CAB_GW_ERR, "CJSON root paramter NULL");
        FUNC_EXIT
        return GEN_API_FAIL;
    }

    item = cJSON_GetObjectItem(root, "vehicleDef");
    if (item) {
        /* Add part number and model number in tpmsConfig */
        tpmsConfig = cJSON_GetObjectItem(item, "tpmsConfig");
        if (!tpmsConfig)
        {
            CAB_DBG(CAB_GW_ERR, "Invalid tpmsConfig");
            return DATA_INVALID;
        }

        for (int i=0; i<MAX_TPMS; i++) {
            cJSON *tpms = cJSON_GetArrayItem(tpmsConfig, i);

            pn = cJSON_GetObjectItem(tpms, "pn");
            mn = cJSON_GetObjectItem(tpms, "mn");

            if (!pn && !mn) {
                cJSON_AddStringToObject(tpms, "pn", DEFAULT_SENSOR_PART_NO);
                cJSON_AddStringToObject(tpms, "mn", DEFAULT_SENSOR_MODEL);
            }
        }
        configUpdated = true;
    }
    else {
        CAB_DBG(CAB_GW_ERR, "CJSON vehicleDef paramter NULL");
        cJSON_Delete(root);
        FUNC_EXIT
        return GEN_API_FAIL;
    }

    /* Save the created JSON string to the global object */
    payload = cJSON_PrintUnformatted(root);

    if (configUpdated) {
        strncpy(g_gatewayConfigJSON, payload, MAX_JSON_STRING_LEN);
    }

    cJSON_Delete(root);
    free(payload);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (This API is used to migrate the config values from the 
 *         JSON object.(g_gatewayConfigJSON->g_gatewayConfig)
 *
 *@param[IN] char * (The ponter to current JSON object string)
 *@param[OUT] cabGatewayConfig_t * (The ponter to updated config structure)
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e migrateFromJSON(char * JSONData, cabGatewayConfig_t * cabGatewayConfigData)
{
    FUNC_ENTRY

    cJSON *root = NULL;
    cJSON *item = NULL;
    cJSON *value = NULL;
    cJSON *array = NULL;
    cJSON *temp = NULL;

    if (!JSONData || !cabGatewayConfigData) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    root = cJSON_Parse(JSONData);

    if (!root) {
        CAB_DBG(CAB_GW_ERR, "CJSON root paramter NULL");
        FUNC_EXIT
        return GEN_API_FAIL;
    }

    /* <vehicleVin> */
    item = cJSON_GetObjectItem(root, "vehicleVin");
    if (item && item->valuestring && strlen(item->valuestring) <= sizeof(cabGatewayConfigData->vehicleVin)) {
        snprintf(cabGatewayConfigData->vehicleVin, sizeof(cabGatewayConfigData->vehicleVin), "%s", item->valuestring);
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Invalid vehicle vin");
        return DATA_INVALID;
    }
    /* </vehicleVin> */

    /* <customerID> */
    item = cJSON_GetObjectItem(root, "customerID");
    if (item && item->valuestring && strlen(item->valuestring) <= sizeof(cabGatewayConfigData->customerID)) {
        snprintf(cabGatewayConfigData->customerID, sizeof(cabGatewayConfigData->customerID), "%s", item->valuestring);
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Invalid customer ID");
        return DATA_INVALID;
    }
    /* </customerID> */

    /* <vehicleType> */
    item = cJSON_GetObjectItem(root, "vehicleType");
    if (cJSON_IsNumber(item)) {
        cabGatewayConfigData->vehicleType = item->valueint;
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Here 1");
        CAB_DBG(CAB_GW_ERR, "Invalid vehicle Type");
        return DATA_INVALID;
    }
    /* </vehicleType> */

    /* <fleetName> */
    item = cJSON_GetObjectItem(root, "fleetName");
    if (item && item->valuestring && strlen(item->valuestring) <= sizeof(cabGatewayConfigData->fleetName)) {
        snprintf(cabGatewayConfigData->fleetName, sizeof(cabGatewayConfigData->fleetName), "%s", item->valuestring);
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Invalid fleet name");
        return DATA_INVALID;
    }
    /* </fleetName> */

    /* <fleetID> */
    item = cJSON_GetObjectItem(root, "fleetID");
    if (item && item->valuestring && strlen(item->valuestring) <= sizeof(cabGatewayConfigData->fleetID)) {
        snprintf(cabGatewayConfigData->fleetID, sizeof(cabGatewayConfigData->fleetID), "%s", item->valuestring);
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Invalid fleet ID");
        return DATA_INVALID;
    }
    /* </fleetID> */

    /* <vehicleMake> */
    item = cJSON_GetObjectItem(root, "vehicleMake");
    if (item && item->valuestring && strlen(item->valuestring) <= sizeof(cabGatewayConfigData->vehicleMake)) {
        snprintf(cabGatewayConfigData->vehicleMake, sizeof(cabGatewayConfigData->vehicleMake), "%s", item->valuestring);
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Invalid vehicle Make");
        return DATA_INVALID;
    }
    /* </vehicleMake> */

    /* <vehicleYear> */
    item = cJSON_GetObjectItem(root, "vehicleYear");
    if (cJSON_IsNumber(item)) {
        cabGatewayConfigData->vehicleYear = item->valueint;
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Invalid vehicle Year");
        return DATA_INVALID;
    }
    /* </vehicleYear> */

    /* <tyreMake> */
    item = cJSON_GetObjectItem(root, "tyreMake");
    if (item && item->valuestring && strlen(item->valuestring) <= sizeof(cabGatewayConfigData->tyreMake)) {
        snprintf(cabGatewayConfigData->tyreMake, sizeof(cabGatewayConfigData->tyreMake), "%s", item->valuestring);
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Invalid tyreMake");
        return DATA_INVALID;
    }
    /* </tyreMake> */

    /* <tyreSize> */
    item = cJSON_GetObjectItem(root, "tyreSize");
    if (item && item->valuestring && strlen(item->valuestring) <= sizeof(cabGatewayConfigData->tyreSize)) {
        snprintf(cabGatewayConfigData->tyreSize, sizeof(cabGatewayConfigData->tyreSize), "%s", item->valuestring);
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Invalid tyreSize");
        return DATA_INVALID;
    }
    /* </tyreSize> */

    /* <vehicleDef> */
    item = cJSON_GetObjectItem(root, "vehicleDef");
    if (item) {
        cJSON *maxTyre = cJSON_GetObjectItem(item, "maxTyre");
        if (cJSON_IsNumber(maxTyre)) {
            cabGatewayConfigData->vehicleDef_s.maxTyre = maxTyre->valueint;
        }
        else {
            CAB_DBG(CAB_GW_ERR, "Invalid maxTyre");
            return DATA_INVALID;
        }

        cJSON *maxAxle = cJSON_GetObjectItem(item, "maxAxle");
        if (cJSON_IsNumber(maxAxle)) {
            cabGatewayConfigData->vehicleDef_s.maxAxle = maxAxle->valueint;
        }
        else {
            CAB_DBG(CAB_GW_ERR, "Invalid maxAxle");
            return DATA_INVALID;
        }

        array = cJSON_GetObjectItem(item, "tpmsConfig");
        if (!array)
        {
            CAB_DBG(CAB_GW_ERR, "Invalid tpmsConfig");
            return DATA_INVALID;
        }

        for (int i=0; i<MAX_TPMS; i++) {
            cJSON *tpms = cJSON_GetArrayItem(array, i);
            value = cJSON_GetObjectItem(tpms, "sensorID");
            if (cJSON_IsNumber(value)) {
                cabGatewayConfigData->vehicleDef_s.tpmsConfig[i].sensorID = (uint32_t) value->valuedouble;
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid sensor Id");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(tpms, "pn");
            if (value && value->valuestring && 
                strlen(value->valuestring) <= sizeof(cabGatewayConfigData->vehicleDef_s.tpmsConfig[i].sensorPN)) {
              snprintf(cabGatewayConfigData->vehicleDef_s.tpmsConfig[i].sensorPN,
                sizeof(cabGatewayConfigData->vehicleDef_s.tpmsConfig[i].sensorPN), "%s", value->valuestring);
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid pn");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(tpms, "mn");
            if (value && value->valuestring && 
                strlen(value->valuestring) <= sizeof(cabGatewayConfigData->vehicleDef_s.tpmsConfig[i].sensorModel)) {
              snprintf(cabGatewayConfigData->vehicleDef_s.tpmsConfig[i].sensorModel,
                sizeof(cabGatewayConfigData->vehicleDef_s.tpmsConfig[i].sensorModel), "%s", value->valuestring);
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid mn");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(tpms, "wheelID");
            if (cJSON_IsNumber(value)) {
                cabGatewayConfigData->vehicleDef_s.tpmsConfig[i].wheelID = value->valueint;
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid wheel Id");
                return DATA_INVALID;
            }

            cJSON *wheelInfo = cJSON_GetObjectItem(tpms, "wheelInfo");

            if (!wheelInfo)  {
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(wheelInfo, "isHaloEnable");
            if (cJSON_IsNumber(value)) {
                cabGatewayConfigData->vehicleDef_s.tpmsConfig[i].wheelInfo.isHaloEnable = value->valueint;
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid isHaloEnable flag");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(wheelInfo, "attachedType");
            if (cJSON_IsNumber(value)) {
                cabGatewayConfigData->vehicleDef_s.tpmsConfig[i].wheelInfo.attachedType = value->valueint;
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid attachedType flag");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(wheelInfo, "tyrePos");
            if (cJSON_IsNumber(value)) {
                cabGatewayConfigData->vehicleDef_s.tpmsConfig[i].wheelInfo.tyrePos = value->valueint;
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid tyrePos flag");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(wheelInfo, "vehicleSide");
            if (cJSON_IsNumber(value)) {
                cabGatewayConfigData->vehicleDef_s.tpmsConfig[i].wheelInfo.vehicleSide = value->valueint;
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid vehicleSide flag");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(wheelInfo, "axleNum");
            if (cJSON_IsNumber(value)) {
                cabGatewayConfigData->vehicleDef_s.tpmsConfig[i].wheelInfo.axleNum = value->valueint;
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid axleNum flag");
                return DATA_INVALID;
            }
        }

        temp = cJSON_GetObjectItem(item, "axleConfig");
        if (!temp) {
            CAB_DBG(CAB_GW_ERR, "Invalid axleConfig");
            return DATA_INVALID;
        }

        /* </haloSNConfig> */
        array = cJSON_GetObjectItem(temp, "haloSNConfig");
        if (!array) {
            CAB_DBG(CAB_GW_ERR, "Invalid haloSNConfig");
            return DATA_INVALID;
        }

        for (int i=0; i<MAX_HALO_SN; i++) {
            cJSON *haloSN = cJSON_GetArrayItem(array, i);

            value = cJSON_GetObjectItem(haloSN, "serialNo");
            if (value && value->valuestring && \
                strlen(value->valuestring) <= sizeof(cabGatewayConfigData->haloSNConfig[i].serialNo)) {
                snprintf(cabGatewayConfigData->haloSNConfig[i].serialNo, \
                    sizeof(cabGatewayConfigData->haloSNConfig[i].serialNo), "%s", value->valuestring);
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid serialNo");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(haloSN, "vehicleSide");
            if (cJSON_IsNumber(value)) {
                cabGatewayConfigData->haloSNConfig[i].vehicleSide = value->valueint;
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid vehicleSide flag");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(haloSN, "axleNum");
            if (cJSON_IsNumber(value)) {
                cabGatewayConfigData->haloSNConfig[i].axleNum = value->valueint;
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid axleNum value");
                return DATA_INVALID;
            }
        }
        /* </haloSNConfig> */

        /* </tyreConfig> */
        array = cJSON_GetObjectItem(temp, "tyreConfig");
        if (!array) {
            CAB_DBG(CAB_GW_ERR, "Invalid tyreConfig");
            return DATA_INVALID;
        }

        for (int i=0; i<MAX_TYRE_NO; i++) {
            cJSON *tyreInfo = cJSON_GetArrayItem(array, i);

            value = cJSON_GetObjectItem(tyreInfo, "axleNum");
            if (cJSON_IsNumber(value)) {
                cabGatewayConfigData->tyreConfig[i].axleNum = value->valueint;
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid axleNum value");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(tyreInfo, "vehicleSide");
            if (cJSON_IsNumber(value)) {
                cabGatewayConfigData->tyreConfig[i].vehicleSide = value->valueint;
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid vehicleSide flag");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(tyreInfo, "tyrePos");
            if (cJSON_IsNumber(value)) {
                cabGatewayConfigData->tyreConfig[i].tyrePos = value->valueint;
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid tyrePos flag");
                return DATA_INVALID;
            }

            item = cJSON_GetObjectItem(tyreInfo, "isTyreDetailConfigured");
            if (cJSON_IsBool(item)) {
                cabGatewayConfigData->tyreConfig[i].isTyreDetailConfigured = item->valueint;
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid isTyreDetailConfigured");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(tyreInfo, "tyreSerialNo");
            if (value && value->valuestring && \
                strlen(value->valuestring) <= sizeof(cabGatewayConfigData->tyreConfig[i].tyreSerialNo)) {
                snprintf(cabGatewayConfigData->tyreConfig[i].tyreSerialNo, \
                    sizeof(cabGatewayConfigData->tyreConfig[i].tyreSerialNo), "%s", value->valuestring);
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid tyreSerialNo");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(tyreInfo, "tyreTIN");
            if (value && value->valuestring && \
                strlen(value->valuestring) <= sizeof(cabGatewayConfigData->tyreConfig[i].tyreTIN)) {
                snprintf(cabGatewayConfigData->tyreConfig[i].tyreTIN, \
                    sizeof(cabGatewayConfigData->tyreConfig[i].tyreTIN), "%s", value->valuestring);
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid tyreTIN");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(tyreInfo, "tyreMake");
            if (value && value->valuestring && \
                strlen(value->valuestring) <= sizeof(cabGatewayConfigData->tyreConfig[i].tyreMake)) {
                snprintf(cabGatewayConfigData->tyreConfig[i].tyreMake, \
                    sizeof(cabGatewayConfigData->tyreConfig[i].tyreMake), "%s", value->valuestring);
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid tyreMake");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(tyreInfo, "tyreModel");
            if (value && value->valuestring && \
                strlen(value->valuestring) <= sizeof(cabGatewayConfigData->tyreConfig[i].tyreModel)) {
                snprintf(cabGatewayConfigData->tyreConfig[i].tyreModel, \
                    sizeof(cabGatewayConfigData->tyreConfig[i].tyreModel), "%s", value->valuestring);
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid tyreModel");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(tyreInfo, "tyreWidth");
            if (value && value->valuestring && \
                strlen(value->valuestring) <= sizeof(cabGatewayConfigData->tyreConfig[i].tyreWidth)) {
                snprintf(cabGatewayConfigData->tyreConfig[i].tyreWidth, \
                    sizeof(cabGatewayConfigData->tyreConfig[i].tyreWidth), "%s", value->valuestring);
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid tyreWidth");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(tyreInfo, "ratio");
            if (value && value->valuestring && \
                strlen(value->valuestring) <= sizeof(cabGatewayConfigData->tyreConfig[i].ratio)) {
                snprintf(cabGatewayConfigData->tyreConfig[i].ratio, \
                    sizeof(cabGatewayConfigData->tyreConfig[i].ratio), "%s", value->valuestring);
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid ratio");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(tyreInfo, "diameter");
            if (value && value->valuestring && \
                strlen(value->valuestring) <= sizeof(cabGatewayConfigData->tyreConfig[i].diameter)) {
                snprintf(cabGatewayConfigData->tyreConfig[i].diameter, \
                    sizeof(cabGatewayConfigData->tyreConfig[i].diameter), "%s", value->valuestring);
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid diameter");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(tyreInfo, "loadRating");
            if (value && value->valuestring && \
                strlen(value->valuestring) <= sizeof(cabGatewayConfigData->tyreConfig[i].loadRating)) {
                snprintf(cabGatewayConfigData->tyreConfig[i].loadRating, \
                    sizeof(cabGatewayConfigData->tyreConfig[i].loadRating), "%s", value->valuestring);
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Invalid loadRating");
                return DATA_INVALID;
            }

            cJSON *array1 = cJSON_GetObjectItem(tyreInfo, "treadDepthHistory");
            if (!array1) {
                CAB_DBG(CAB_GW_ERR, "Invalid treadDepthHistory");
                return DATA_INVALID;
            }

            for (int j=0; j<MAX_TREAD_DEPTH_RECORD; j++) {
                cJSON *treadRecord = cJSON_GetArrayItem(array1, j);

                value = cJSON_GetObjectItem(treadRecord, "treadDepthValue");
                if (value && value->valuestring && \
                    strlen(value->valuestring) <= sizeof(cabGatewayConfigData->tyreConfig[i].treadDepthHistory[j].treadDepthValue)) {
                    snprintf(cabGatewayConfigData->tyreConfig[i].treadDepthHistory[j].treadDepthValue, \
                        sizeof(cabGatewayConfigData->tyreConfig[i].treadDepthHistory[j].treadDepthValue), \
                        "%s", value->valuestring);
                }
                else {
                    CAB_DBG(CAB_GW_ERR, "Invalid treadDepthValue");
                    return DATA_INVALID;
                }

                value = cJSON_GetObjectItem(treadRecord, "vehicleMileage");
                if (cJSON_IsNumber(value)) {
                    cabGatewayConfigData->tyreConfig[i].treadDepthHistory[j].vehicleMileage = value->valueint;
                }
                else {
                    CAB_DBG(CAB_GW_ERR, "Invalid vehicleMileage value");
                    return DATA_INVALID;
                }

                value = cJSON_GetObjectItem(treadRecord, "recordTime");
                if (cJSON_IsNumber(value)) {
                    cabGatewayConfigData->tyreConfig[i].treadDepthHistory[j].recordTime = value->valueint;
                }
                else {
                    CAB_DBG(CAB_GW_ERR, "Invalid recordTime value");
                    return DATA_INVALID;
                }
            }

        }
        /* </tyreConfig> */
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Invalid vehicleDef");
        return DATA_INVALID;
    }
    /* </vehicleDef> */

    /* <sensorType> */
    item = cJSON_GetObjectItem(root, "sensorType");
    if (cJSON_IsNumber(item)) {
        cabGatewayConfigData->sensorType = item->valueint;
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Invalid sensor Type");
        return DATA_INVALID;
    }
    /* </sensorType> */

    /* <targetPressInfo> */
    item = cJSON_GetObjectItem(root, "targetPressInfo");
    if (!item)  {
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "numberOfAxle");
    if (cJSON_IsNumber(value)) {
        cabGatewayConfigData->targetPressInfo.numberOfAxle = value->valueint;
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Invalid numberOfAxle value");
        return DATA_INVALID;
    }

    array = cJSON_GetObjectItem(item, "tpmsThreshold");
    for (int i=0; i<MAX_AXLE_NO; i++) {
        cabGatewayConfigData->targetPressInfo.tpmsThreshold[i] = cJSON_GetArrayItem(array, i)->valueint;
    }
    /* </targetPressInfo> */

    /* <lowBatteryThreshold> */
    item = cJSON_GetObjectItem(root, "lowBatteryThreshold");
    if (cJSON_IsNumber(item)) {
        cabGatewayConfigData->lowBatteryThreshold = item->valueint;
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Invalid lowBatteryThreshold");
        return DATA_INVALID;
    }
    /* </lowBatteryThreshold> */

    /* <cloudURI> */
    item = cJSON_GetObjectItem(root, "cloudURI");
    if (item && item->valuestring && strlen(item->valuestring) <= sizeof(cabGatewayConfigData->cloudURI)) {
        snprintf(cabGatewayConfigData->cloudURI, sizeof(cabGatewayConfigData->cloudURI), "%s", item->valuestring);
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Invalid cloudURI");
        return DATA_INVALID;
    }
    /* </cloudURI> */

    /* <imeiNumber> */
    array = cJSON_GetObjectItem(root, "imeiNumber");
    if (!array)  {
        return DATA_INVALID;
    }

    for (int i=0; i<MAX_LEN_LTE_IMEI; i++) {
        cabGatewayConfigData->imeiNumber[i] = cJSON_GetArrayItem(array, i)->valueint;
    }
    /* </imeiNumber> */

    /* <blePassKey> */
    item = cJSON_GetObjectItem(root, "blePassKey");
    if (item && item->valuestring && strlen(item->valuestring) <= sizeof(cabGatewayConfigData->blePassKey)) {
        strncpy(cabGatewayConfigData->blePassKey, item->valuestring, MAX_LEN_BLE_PASSKEY);
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Invalid blePassKey");
        return DATA_INVALID;
    }
    /* </blePassKey> */

    /* <bleMac> */
    array = cJSON_GetObjectItem(root, "bleMac");
    if (!array)  {
        return DATA_INVALID;
    }

    for (int i=0; i<MAX_LEN_BLE_MAC; i++) {
        cabGatewayConfigData->bleMac[i] = cJSON_GetArrayItem(array, i)->valueint;
    }
    /* </bleMac> */

    /* <gatewayId> */
    array = cJSON_GetObjectItem(root, "gatewayId");
    if (!array)  {
        return DATA_INVALID;
    }

    for (int i=0; i<MAX_LEN_GATEWAY_ID; i++) {
        cabGatewayConfigData->gatewayId[i] = cJSON_GetArrayItem(array, i)->valueint;
    }
    /* </gatewayId> */

    /* <hardwareVersion> */
    item = cJSON_GetObjectItem(root, "hardwareVersion");
    if (item && item->valuestring && strlen(item->valuestring) <= sizeof(cabGatewayConfigData->hardwareVersion)) {
        snprintf(cabGatewayConfigData->hardwareVersion, sizeof(cabGatewayConfigData->hardwareVersion), \
            "%s", item->valuestring);
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Invalid hardwareVersion");
        return DATA_INVALID;
    }
    /* </hardwareVersion> */

    /* <softwareVersion> */
    item = cJSON_GetObjectItem(root, "softwareVersion");
    if (item && item->valuestring && strlen(item->valuestring) <= sizeof(cabGatewayConfigData->softwareVersion)) {
        snprintf(cabGatewayConfigData->softwareVersion, sizeof(cabGatewayConfigData->softwareVersion), \
            "%s", item->valuestring);
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Invalid softwareVersion");
        return DATA_INVALID;
    }
    /* </softwareVersion> */

    /* <simIccid> */
    item = cJSON_GetObjectItem(root, "simIccid");
    if (item && item->valuestring && strlen(item->valuestring) <= sizeof(cabGatewayConfigData->simIccid)) {
        snprintf(cabGatewayConfigData->simIccid, sizeof(cabGatewayConfigData->simIccid), "%s", item->valuestring);
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Invalid simIccid");
        return DATA_INVALID;
    }
    /* </simIccid> */

    /* <isRtcSync> */
    item = cJSON_GetObjectItem(root, "isRtcSync");
    if (cJSON_IsNumber(item)) {
        cabGatewayConfigData->isRtcSync = item->valueint;
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Invalid isRtcSync");
        return DATA_INVALID;
    }
    /* </isRtcSync> */

    /* <isMandatoryInfoAvailable> */
    item = cJSON_GetObjectItem(root, "isMandatoryInfoAvailable");
    if (cJSON_IsBool(item)) {
        cabGatewayConfigData->isMandatoryInfoAvailable = item->valueint;
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Invalid isMandatoryInfoAvailable");
        return DATA_INVALID;
    }
    /* </isMandatoryInfoAvailable> */

    /* <diagLTETime> */
    item = cJSON_GetObjectItem(root, "diagLTETime");
    if (cJSON_IsNumber(item)) {
        cabGatewayConfigData->diagLTETime = item->valuedouble;
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Invalid diagLTETime");
        return DATA_INVALID;
    }
    /* </diagLTETime> */

    cJSON_Delete(root);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (This API is used to migrate the config JSON object with
 *         default or existing config values as per valid data types 
 *         in JSON format. (g_gatewayConfig->g_gatewayConfigJSON)
 *
 *@param[IN] configInitType_e (To decide whether we need to use 
 *                             existing configuration or default.)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e migrateToJSON(configInitType_e initType)
{
    FUNC_ENTRY
    cJSON *root = NULL;
    cJSON *targetPressInfo = NULL;
    cJSON *vehicleDef = NULL;
    cJSON *tpmsConfig = NULL;
    cJSON *axleConfig = NULL;
    cJSON *haloSNConfig = NULL;
    cJSON *tyreConfig = NULL;
    char *payload = NULL;

    root = cJSON_CreateObject();

    if (!root) {
        CAB_DBG(CAB_GW_ERR, "CJSON root paramter NULL");
        FUNC_EXIT
        return GEN_API_FAIL;
    }

    cJSON_AddStringToObject(root, "vehicleVin", \
        (initType == EXISTING_CONFIG) ? g_gatewayConfig.vehicleVin : DEFAULT_VEH_NUM);

    cJSON_AddStringToObject(root, "customerID", \
        (initType == EXISTING_CONFIG) ? g_gatewayConfig.customerID : DEFAULT_CUSTOMER_ID);

    cJSON_AddNumberToObject(root, "vehicleType", \
        (initType == EXISTING_CONFIG) ? g_gatewayConfig.vehicleType : DEFAULT_VEH_TYPE);

    cJSON_AddStringToObject(root, "fleetName", \
        (initType == EXISTING_CONFIG) ? g_gatewayConfig.fleetName : DEFAULT_FLEET_NAME);

    cJSON_AddStringToObject(root, "fleetID", \
        (initType == EXISTING_CONFIG) ? g_gatewayConfig.fleetID : DEFAULT_FLEET_ID);

    cJSON_AddStringToObject(root, "vehicleMake", \
        (initType == EXISTING_CONFIG) ? g_gatewayConfig.vehicleMake : DEFAULT_VEHICLE_MAKE);

    cJSON_AddNumberToObject(root, "vehicleYear", \
        (initType == EXISTING_CONFIG) ? g_gatewayConfig.vehicleYear : DEFAULT_VEHICLE_YEAR);

    cJSON_AddStringToObject(root, "tyreMake",  \
        (initType == EXISTING_CONFIG) ? g_gatewayConfig.tyreMake : DEFAULT_TYRE_MAKE);

    cJSON_AddStringToObject(root, "tyreSize", \
        (initType == EXISTING_CONFIG) ? g_gatewayConfig.tyreSize : DEFAULT_TYRE_SIZE);

    cJSON_AddItemToObject(root, "vehicleDef", vehicleDef = cJSON_CreateObject());

    if (!vehicleDef) {
        CAB_DBG(CAB_GW_ERR, "CJSON vehicleDef paramter NULL");
        cJSON_Delete(root);
        FUNC_EXIT
        return GEN_API_FAIL;
    }

    cJSON_AddNumberToObject(vehicleDef, "maxTyre", \
        (initType == EXISTING_CONFIG) ? g_gatewayConfig.vehicleDef_s.maxTyre : 0);

    cJSON_AddNumberToObject(vehicleDef, "maxAxle", \
        (initType == EXISTING_CONFIG) ? g_gatewayConfig.vehicleDef_s.maxAxle : 0);

    tpmsConfig = cJSON_AddArrayToObject(vehicleDef, "tpmsConfig");

    if (tpmsConfig == NULL) {
        CAB_DBG(CAB_GW_ERR, "CJSON tpmsConfig paramter NULL");
        cJSON_Delete(root);
        FUNC_EXIT
        return GEN_API_FAIL;
    }

    for (int i=0; i<MAX_TPMS; i++) {
        cJSON *wheelInfo = NULL;
        cJSON *tpms = NULL;
        tpms = cJSON_CreateObject();

        cJSON_AddNumberToObject(tpms, "sensorID", (initType == EXISTING_CONFIG) ? (unsigned int) \
            g_gatewayConfig.vehicleDef_s.tpmsConfig[i].sensorID : 0);

        cJSON_AddStringToObject(tpms, "pn",  \
            (initType == EXISTING_CONFIG) ? g_gatewayConfig.vehicleDef_s.tpmsConfig[i].sensorPN : DEFAULT_SENSOR_PART_NO);

        cJSON_AddStringToObject(tpms, "mn",  \
            (initType == EXISTING_CONFIG) ? g_gatewayConfig.vehicleDef_s.tpmsConfig[i].sensorModel : DEFAULT_SENSOR_MODEL);

        cJSON_AddNumberToObject(tpms, "wheelID", (initType == EXISTING_CONFIG) ? (unsigned int) \
            g_gatewayConfig.vehicleDef_s.tpmsConfig[i].wheelID : 0);

        cJSON_AddItemToObject(tpms, "wheelInfo", wheelInfo = cJSON_CreateObject());

        if (wheelInfo == NULL) {
            CAB_DBG(CAB_GW_ERR, "CJSON wheelInfo paramter NULL");
            cJSON_Delete(root);
            FUNC_EXIT
            return GEN_API_FAIL;
        }

        cJSON_AddNumberToObject(wheelInfo, "isHaloEnable", (initType == EXISTING_CONFIG) ? \
            g_gatewayConfig.vehicleDef_s.tpmsConfig[i].wheelInfo.isHaloEnable : 0);

        cJSON_AddNumberToObject(wheelInfo, "attachedType", (initType == EXISTING_CONFIG) ? \
            g_gatewayConfig.vehicleDef_s.tpmsConfig[i].wheelInfo.attachedType : 0);

        cJSON_AddNumberToObject(wheelInfo, "tyrePos", (initType == EXISTING_CONFIG) ? \
            g_gatewayConfig.vehicleDef_s.tpmsConfig[i].wheelInfo.tyrePos : 0);

        cJSON_AddNumberToObject(wheelInfo, "vehicleSide", (initType == EXISTING_CONFIG) ?  \
            g_gatewayConfig.vehicleDef_s.tpmsConfig[i].wheelInfo.vehicleSide : 0);

        cJSON_AddNumberToObject(wheelInfo, "axleNum", (initType == EXISTING_CONFIG) ?  \
            (unsigned int) g_gatewayConfig.vehicleDef_s.tpmsConfig[i].wheelInfo.axleNum : 0);

        /* Add the single tpms config data to the array */
        
        cJSON_AddItemToArray(tpmsConfig, tpms);
    }

    cJSON_AddItemToObject(vehicleDef, "axleConfig", axleConfig = cJSON_CreateObject());

    if (axleConfig == NULL) {
        CAB_DBG(CAB_GW_ERR, "CJSON axleConfig paramter NULL");
        cJSON_Delete(root);
        FUNC_EXIT
        return GEN_API_FAIL;
    }

    haloSNConfig = cJSON_AddArrayToObject(axleConfig, "haloSNConfig");

    if (haloSNConfig == NULL) {
        CAB_DBG(CAB_GW_ERR, "CJSON haloSNConfig paramter NULL");
        cJSON_Delete(root);
        FUNC_EXIT
        return GEN_API_FAIL;
    }

    for (int i=0; i<MAX_HALO_SN; i++) {
        cJSON *haloSNInfo = NULL;
        haloSNInfo = cJSON_CreateObject();

        cJSON_AddStringToObject(haloSNInfo, "serialNo", \
        (initType == EXISTING_CONFIG) ? g_gatewayConfig.haloSNConfig[i].serialNo : DEFAULT_HALO_SN);

        cJSON_AddNumberToObject(haloSNInfo, "vehicleSide", (initType == EXISTING_CONFIG) ?  \
            g_gatewayConfig.haloSNConfig[i].vehicleSide : 0);

        cJSON_AddNumberToObject(haloSNInfo, "axleNum", (initType == EXISTING_CONFIG) ?  \
            (unsigned int) g_gatewayConfig.haloSNConfig[i].axleNum : 0);

        /* Add the single HALO SN data to the array */    
        cJSON_AddItemToArray(haloSNConfig, haloSNInfo);
    }

    tyreConfig = cJSON_AddArrayToObject(axleConfig, "tyreConfig");

    if (tyreConfig == NULL) {
        CAB_DBG(CAB_GW_ERR, "CJSON tyreConfig paramter NULL");
        cJSON_Delete(root);
        FUNC_EXIT
        return GEN_API_FAIL;
    }

    for (int i=0; i<MAX_TYRE_NO; i++) {
        cJSON *tyreInfo = NULL;
        tyreInfo = cJSON_CreateObject();

        cJSON_AddNumberToObject(tyreInfo, "axleNum", (initType == EXISTING_CONFIG) ?  \
            (unsigned int) g_gatewayConfig.tyreConfig[i].axleNum : 0);

        cJSON_AddNumberToObject(tyreInfo, "vehicleSide", (initType == EXISTING_CONFIG) ?  \
            g_gatewayConfig.tyreConfig[i].vehicleSide : 0);

        cJSON_AddNumberToObject(tyreInfo, "tyrePos", (initType == EXISTING_CONFIG) ? \
             g_gatewayConfig.tyreConfig[i].tyrePos : 0);

        cJSON_AddItemToObject(tyreInfo, "isTyreDetailConfigured", \
            (initType == EXISTING_CONFIG) ? cJSON_CreateBool(g_gatewayConfig.tyreConfig[i].isTyreDetailConfigured) : \
            cJSON_CreateBool(DEFAULT_TYRE_DETAILS_STATUS));

        cJSON_AddStringToObject(tyreInfo, "tyreSerialNo", \
            (initType == EXISTING_CONFIG) ? g_gatewayConfig.tyreConfig[i].tyreSerialNo : DEFAULT_TYRE_SERIAL_NO);

        cJSON_AddStringToObject(tyreInfo, "tyreTIN", \
            (initType == EXISTING_CONFIG) ? g_gatewayConfig.tyreConfig[i].tyreTIN : DEFAULT_TYRE_TIN);

        cJSON_AddStringToObject(tyreInfo, "tyreMake", \
            (initType == EXISTING_CONFIG) ? g_gatewayConfig.tyreConfig[i].tyreMake : DEFAULT_TYRE_MAKE);

        cJSON_AddStringToObject(tyreInfo, "tyreModel", \
            (initType == EXISTING_CONFIG) ? g_gatewayConfig.tyreConfig[i].tyreModel : DEFAULT_TYRE_MODEL);

        cJSON_AddStringToObject(tyreInfo, "tyreWidth", \
            (initType == EXISTING_CONFIG) ? g_gatewayConfig.tyreConfig[i].tyreWidth : DEFAULT_TYRE_WIDTH);

        cJSON_AddStringToObject(tyreInfo, "ratio", \
            (initType == EXISTING_CONFIG) ? g_gatewayConfig.tyreConfig[i].ratio : DEFAULT_RATIO);

        cJSON_AddStringToObject(tyreInfo, "diameter", \
            (initType == EXISTING_CONFIG) ? g_gatewayConfig.tyreConfig[i].diameter : DEFAULT_DIAMETER);

        cJSON_AddStringToObject(tyreInfo, "loadRating", \
            (initType == EXISTING_CONFIG) ? g_gatewayConfig.tyreConfig[i].loadRating : DEFAULT_LOAD_RATING);

        cJSON *treadDepthHistory = NULL;
        treadDepthHistory = cJSON_AddArrayToObject(tyreInfo, "treadDepthHistory");

        if (treadDepthHistory == NULL) {
            CAB_DBG(CAB_GW_ERR, "CJSON treadDepthHistory paramter NULL");
            cJSON_Delete(root);
            FUNC_EXIT
            return GEN_API_FAIL;
        }

        for (int j=0; j<MAX_TREAD_DEPTH_RECORD; j++) {
            cJSON *treadRecord = NULL;
            treadRecord = cJSON_CreateObject();

            cJSON_AddStringToObject(treadRecord, "treadDepthValue", \
                (initType == EXISTING_CONFIG) ? g_gatewayConfig.tyreConfig[i].treadDepthHistory[j].treadDepthValue \
                : DEFAULT_TREAD_DEPTH);

            cJSON_AddNumberToObject(treadRecord, "vehicleMileage", (initType == EXISTING_CONFIG) ? \
                g_gatewayConfig.tyreConfig[i].treadDepthHistory[j].vehicleMileage : DEFAULT_VEHICLE_MILEAGE);

            cJSON_AddNumberToObject(treadRecord, "recordTime", (initType == EXISTING_CONFIG) ? \
                g_gatewayConfig.tyreConfig[i].treadDepthHistory[j].recordTime : DEFAULT_RECORD_TIME);

            /* Add the single history record to the array */    
            cJSON_AddItemToArray(treadDepthHistory, treadRecord);
        }

        /* Add the single tyre config data to the array */    
        cJSON_AddItemToArray(tyreConfig, tyreInfo);
    }

    cJSON_AddNumberToObject(root, "sensorType", \
        (initType == EXISTING_CONFIG) ? g_gatewayConfig.sensorType : DEFAULT_SENSOR_TYPE);

    /* The JSON does not take the uint8_t, need to convert it to int */
    unsigned int pressureInfo[] = DEFAULT_TPMS_THRESHOLD;

    if (initType == EXISTING_CONFIG) {
        for (int i=0; i<g_gatewayConfig.targetPressInfo.numberOfAxle; i++) {
            pressureInfo[i] = (unsigned int) g_gatewayConfig.targetPressInfo.tpmsThreshold[i];
        }
    }

    targetPressInfo = cJSON_CreateObject();

    cJSON_AddNumberToObject(targetPressInfo, "numberOfAxle", \
        (initType == EXISTING_CONFIG) ? g_gatewayConfig.targetPressInfo.numberOfAxle : MAX_AXLE_NO);

    cJSON_AddItemToObject(targetPressInfo,"tpmsThreshold", cJSON_CreateIntArray(pressureInfo, MAX_AXLE_NO));

    cJSON_AddItemToObject(root, "targetPressInfo", targetPressInfo);

    cJSON_AddNumberToObject(root, "lowBatteryThreshold", \
        (initType == EXISTING_CONFIG) ? g_gatewayConfig.lowBatteryThreshold : DEFAULT_LOW_BAT_THRESHOLD);

    cJSON_AddStringToObject(root, "cloudURI", \
        (initType == EXISTING_CONFIG) ? g_gatewayConfig.cloudURI : DEFAULT_CLOUD_URI);

    /* The JSON does not take the uint8_t, need to convert it to int */
    unsigned int imeiInfo[MAX_LEN_LTE_IMEI] = {};

    if (initType == EXISTING_CONFIG) {
        for (int i=0; i<MAX_LEN_LTE_IMEI; i++) {
            imeiInfo[i] = (unsigned int) g_gatewayConfig.imeiNumber[i];
        }
    }

    (initType == EXISTING_CONFIG) ? cJSON_AddItemToObject(root, "imeiNumber", \
        cJSON_CreateIntArray(imeiInfo, MAX_LEN_LTE_IMEI)) : cJSON_AddStringToObject(root, \
        "imeiNumber", DEFAULT_LTE_IMEI);

    char passKey[MAX_LEN_BLE_PASSKEY+1] = {'\0'};
    strncpy(passKey, g_gatewayConfig.blePassKey, MAX_LEN_BLE_PASSKEY);

    cJSON_AddStringToObject(root, "blePassKey", \
        (initType == EXISTING_CONFIG) ? passKey : DEFAULT_BLE_PASSKEY);

    /* The JSON does not take the uint8_t, need to convert it to int */
    unsigned int bleMacInfo[MAX_LEN_BLE_MAC] = {};

    if (initType == EXISTING_CONFIG) {
        for (int i=0; i<MAX_LEN_BLE_MAC; i++) {
            bleMacInfo[i] = (unsigned int) g_gatewayConfig.bleMac[i];
        }
    }

    (initType == EXISTING_CONFIG) ? cJSON_AddItemToObject(root, "bleMac", \
        cJSON_CreateIntArray(bleMacInfo, MAX_LEN_BLE_MAC)) : cJSON_AddStringToObject(root, "bleMac", DEFAULT_BLE_MAC);

    /* The JSON does not take the uint8_t, need to convert it to int */
    unsigned int gatwayIdInfo[MAX_LEN_GATEWAY_ID] = {};

    if (initType == EXISTING_CONFIG) {
        for (int i=0; i<MAX_LEN_GATEWAY_ID; i++) {
            gatwayIdInfo[i] = (unsigned int) g_gatewayConfig.gatewayId[i];
        }
    }

    (initType == EXISTING_CONFIG) ? cJSON_AddItemToObject(root, "gatewayId", \
        cJSON_CreateIntArray(gatwayIdInfo, MAX_LEN_GATEWAY_ID)) : cJSON_AddStringToObject(root, \
        "gatewayId", DEFAULT_GATEWAY_ID);

    cJSON_AddStringToObject(root, "hardwareVersion", \
        (initType == EXISTING_CONFIG) ? g_gatewayConfig.hardwareVersion : DEFAULT_HARDWARE_VERSION);

    cJSON_AddStringToObject(root, "softwareVersion", \
        (initType == EXISTING_CONFIG) ? g_gatewayConfig.softwareVersion : DEFAULT_SOFTWARE_VERSION);

    cJSON_AddStringToObject(root, "simIccid", \
        (initType == EXISTING_CONFIG) ? g_gatewayConfig.simIccid : DEFAULT_LTE_ICCID);

    cJSON_AddNumberToObject(root, "isRtcSync", \
        (initType == EXISTING_CONFIG) ? g_gatewayConfig.isRtcSync : DEFAULT_RTC_STATUS);

    cJSON_AddItemToObject(root, "isMandatoryInfoAvailable", \
        (initType == EXISTING_CONFIG) ? cJSON_CreateBool(g_gatewayConfig.isMandatoryInfoAvailable) : \
        cJSON_CreateBool(DEFAULT_MANDATORY_STATUS));

    cJSON_AddNumberToObject(root, "diagLTETime", \
        (initType == EXISTING_CONFIG) ? g_gatewayConfig.diagLTETime : 0);

    /* Save the created JSON string to the global object */
    payload = cJSON_PrintUnformatted(root);
    strncpy(g_gatewayConfigJSON, payload, MAX_JSON_STRING_LEN);

    cJSON_Delete(root);
    free(payload);

    FUNC_EXIT

    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (This API is used to print current config structure
 *         for debugging purpose only)
 *
 *@param[IN] None
 *@param[OUT] None
 *
 *@return None
 *******************************************************************/

static void dumpConfig()
{
    printf("\nThe existing configuation is as following : \r\n");
    printf("vehicleVin : %s\n", g_gatewayConfig.vehicleVin);
    printf("customerID : %s\n", g_gatewayConfig.customerID);
    printf("vehicleType : %d\n", g_gatewayConfig.vehicleType);
    printf("fleetName : %s\n", g_gatewayConfig.fleetName);
    printf("fleetID : %s\n", g_gatewayConfig.fleetID);
    printf("vehicleMake : %s\n", g_gatewayConfig.vehicleMake);
    printf("vehicleYear : %d\n", g_gatewayConfig.vehicleYear);
    printf("tyreMake : %s\n", g_gatewayConfig.tyreMake);
    printf("tyreSize : %s\n", g_gatewayConfig.tyreSize);
    printf("vehicleDef_s.maxTyre : %d\n", g_gatewayConfig.vehicleDef_s.maxTyre);
    printf("vehicleDef_s.maxAxle : %d\n", g_gatewayConfig.vehicleDef_s.maxAxle);
    printf("vehicleDef_s.tpmsConfig : \n");
    for (int i=0; i<MAX_TPMS; i++) {
        printf("\t[\n");
        printf("\t sensorID : %u \n", g_gatewayConfig.vehicleDef_s.tpmsConfig[i].sensorID);
        printf("\t wheelID : %u \n", g_gatewayConfig.vehicleDef_s.tpmsConfig[i].wheelID);
        printf("\t\t[\n");
        printf("\t\t isHaloEnable : %s \n", (g_gatewayConfig.vehicleDef_s.tpmsConfig[i].wheelInfo.isHaloEnable) \
            ? "DISABLE (1)" : "ENABLE (0)");
        printf("\t\t attachedType : %s \n", (g_gatewayConfig.vehicleDef_s.tpmsConfig[i].wheelInfo.attachedType) \
            ? "HALO (1)" : "TYRE (0)");
        printf("\t\t tyrePos : %s \n", (g_gatewayConfig.vehicleDef_s.tpmsConfig[i].wheelInfo.tyrePos) \
            ? "OUTER (1)" : "INNER (0)");
        printf("\t\t vehicleSide : %s \n", (g_gatewayConfig.vehicleDef_s.tpmsConfig[i].wheelInfo.vehicleSide) \
            ? "RIGHT (1)" : "LEFT (0)");
        printf("\t\t axleNum : %u \n", g_gatewayConfig.vehicleDef_s.tpmsConfig[i].wheelInfo.axleNum);
        printf("\t\t]\n");
        printf("\t]\n");
    }
    printf("sensorType : %d\n", g_gatewayConfig.sensorType);
    printf("targetPressInfo.numberOfAxle : %d\n", g_gatewayConfig.targetPressInfo.numberOfAxle);
    printf("targetPressInfo.tpmsThreshold : [");
    for (int i=0; i<MAX_AXLE_NO; i++) {
        printf(" %u ", g_gatewayConfig.targetPressInfo.tpmsThreshold[i]);
    }
    printf("]\n");
    printf("lowBatteryThreshold : %d\n", g_gatewayConfig.lowBatteryThreshold);
    printf("cloudURI : %s\n", g_gatewayConfig.cloudURI);
    printf("imeiNumber : [");
    for (int i=0; i<MAX_LEN_LTE_IMEI; i++) {
        printf(" %u ", g_gatewayConfig.imeiNumber[i]);
    }
    printf("]\n");
    printf("blePassKey : %s\n", g_gatewayConfig.blePassKey);
    printf("bleMac : [");
    for (int i=0; i<MAX_LEN_BLE_MAC; i++) {
        printf(" %u ", g_gatewayConfig.bleMac[i]);
    }
    printf("]\n");
    printf("gatewayId : [");
    for (int i=0; i<MAX_LEN_GATEWAY_ID; i++) {
        printf(" %u ", g_gatewayConfig.gatewayId[i]);
    }
    printf("]\n");
    printf("hardwareVersion : %s\n", g_gatewayConfig.hardwareVersion);
    printf("softwareVersion : %s\n", g_gatewayConfig.softwareVersion);
    printf("simIccid : %s\n", g_gatewayConfig.simIccid);
    printf("isRtcSync : %d\n", g_gatewayConfig.isRtcSync);
    printf("isMandatoryInfoAvailable : %d\n", g_gatewayConfig.isMandatoryInfoAvailable);
    printf("diagLTETime : %ld\n", g_gatewayConfig.diagLTETime);
}


returnCode_e initConfigModule(updatedConfigNotifCB funPtr)   //Initialize configuration module
{
    FUNC_ENTRY

    struct stat st = {0};
    uint8_t status = GEN_SUCCESS;
    FILE * filePtr;
    uint16_t index = 0;

    CAB_DBG(CAB_GW_INFO, "Initializing configuration module");

    memset(&g_gatewayConfig, 0, sizeof(g_gatewayConfig));
    /* A 40 KB buffer to read and write the JSON to/from file */
    g_gatewayConfigJSON = (char *) malloc(sizeof(char)*MAX_JSON_STRING_LEN);
    if (!g_gatewayConfigJSON) {
      CAB_DBG(CAB_GW_ERR, "NULL pointing error");
      return GEN_NULL_POINTING_ERROR;
    }

    //Initializing mutext lock
    pthread_mutex_init(&g_lock, NULL);
    pthread_mutex_init(&resetStateLock, NULL);

    //lock to protect gateway configuration
    pthread_mutex_lock(&g_lock);
    pthread_mutex_lock(&resetStateLock);

    if(funPtr == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL poiting error");
        //unlock mutex lock
        pthread_mutex_unlock(&g_lock);
        pthread_mutex_unlock(&resetStateLock);
        return GEN_NULL_POINTING_ERROR;
    }
    g_notifFunPtr = funPtr;

    //To check for the configStorage directory and if it not exist then create it
    if(stat(DIR_PATH_CONFIG_STORAGE, &st) == -1) {
        if((mkdir(DIR_PATH_CONFIG_STORAGE, 0755)) == -1) {
            CAB_DBG(CAB_GW_ERR, "configStorage directory not created");
            //unlock mutex lock
            pthread_mutex_unlock(&g_lock);
            pthread_mutex_unlock(&resetStateLock);
            return DIR_CREATE_FAIL;
        }
    }

    /* If Configuration parameter JSON file does not exist then either we
       need to create a new file or migrate the existing data from the old file. */
    if (access(NEW_FILE_PATH_CONFIG_PARAM, F_OK) == -1) {
        CAB_DBG(CAB_GW_DEBUG, "Can't Access JSON config file, creating new");

        /* Neither old nor new file exists, so let's create the new JSON configuration file */
        if (access(FILE_PATH_CONFIG_PARAM, F_OK) == -1) {
            status = migrateToJSON(DEFAULT_CONFIG);
            if (status != GEN_SUCCESS) {
                CAB_DBG(CAB_GW_ERR, "Creating config JSON object for default configuration failed");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                pthread_mutex_unlock(&resetStateLock);
                FUNC_EXIT
                return status;
            }

            /* Udpdate the g_gatewayConfig to default values to be in sync with config JSON */
            /* One more reason to initialize is, some of the elements which are basically array, but
               initialized with the "". So when we try to sync it creates problem */
            g_gatewayConfig = (cabGatewayConfig_t) {
                .vehicleVin = DEFAULT_VEH_NUM,
                .customerID = DEFAULT_CUSTOMER_ID,
                .vehicleType = DEFAULT_VEH_TYPE,
                .fleetName = DEFAULT_FLEET_NAME,
                .fleetID = DEFAULT_FLEET_ID,
                .vehicleMake = DEFAULT_VEHICLE_MAKE,
                .vehicleYear = DEFAULT_VEHICLE_YEAR,
                .tyreMake = DEFAULT_TYRE_MAKE,
                .tyreSize = DEFAULT_TYRE_SIZE,
                .sensorType = DEFAULT_SENSOR_TYPE,
                .targetPressInfo.numberOfAxle = MAX_AXLE_NO,
                .targetPressInfo.tpmsThreshold = DEFAULT_TPMS_THRESHOLD,
                .lowBatteryThreshold = DEFAULT_LOW_BAT_THRESHOLD,
                .cloudURI = DEFAULT_CLOUD_URI,
                .imeiNumber = DEFAULT_LTE_IMEI,
                .blePassKey = DEFAULT_BLE_PASSKEY,
                .bleMac = DEFAULT_BLE_MAC,
                .gatewayId = DEFAULT_GATEWAY_ID,
                .simIccid = DEFAULT_LTE_ICCID,
                .hardwareVersion = DEFAULT_HARDWARE_VERSION,
                .isRtcSync = DEFAULT_RTC_STATUS,
                .isMandatoryInfoAvailable = DEFAULT_MANDATORY_STATUS,
                .ledPattern_s = *ledPattern[DEFAULT_LED_PATTERN]
            };

            /* Initialize the part no and model no to default values */
            for (uint8_t i; i < MAX_TPMS; i++) {
              strncpy(g_gatewayConfig.vehicleDef_s.tpmsConfig[i].sensorPN, DEFAULT_SENSOR_PART_NO, MAX_LEN_SENSOR_PART_NO);
              strncpy(g_gatewayConfig.vehicleDef_s.tpmsConfig[i].sensorModel, DEFAULT_SENSOR_MODEL, MAX_LEN_SENSOR_MODEL);
            }
        }
        /* The old configuration file exists, so let's migrate the data. At the 
           end rename the old config file to keep it as reference just in case.*/

        /* HACK: This else conditition plays crutial role if we want to migrate from the original configuration to the
            json config file. The migration will only be hassel free till there is no parameter added/updated in 
            between the config structure cabGatewayConfig_t.

            The config was migrated to json at 3.9.0.0. Till 3.11.0.0 there was no parameters added/updated in between
            the config structure. So this switch can be used to retrive the original config parameters. In 3.11.0.1
            sensorPN and sensorMN were added in between, so now the above functionality will break.

            In case of emergencies, one can get the original configuration -> unpack the binary data to old connfig 
            structure -> use the new parameters default values and pack the data with new structure parameters -> get 
            the new packed config file to location and rename it to configParam_obsolete.txt -> delete the json config
            file and reboot. */

        else {
            CAB_DBG(CAB_GW_INFO, "Reading config from existing configuration file for migrating it to JSON");
            
            status = readConfigParam(&g_gatewayConfig);       //read Configuration parameter file
            if(GEN_SUCCESS != status) {
                CAB_DBG(CAB_GW_ERR, "%s reading configuration failed", FILE_PATH_CONFIG_PARAM);
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                pthread_mutex_unlock(&resetStateLock);
                FUNC_EXIT
                return status;
            }
            else {
                /* Initialize new parameters as per the current configuration */
                initializeNewParameters();
                status = migrateToJSON(EXISTING_CONFIG);
                if (status != GEN_SUCCESS) {
                    CAB_DBG(CAB_GW_ERR, "Creating config JSON object for existing configuration failed");
                    //unlock mutex lock
                    pthread_mutex_unlock(&g_lock);
                    pthread_mutex_unlock(&resetStateLock);
                    FUNC_EXIT
                    return status;
                }

                status = rename(FILE_PATH_CONFIG_PARAM, FILE_PATH_CONFIG_PARAM_OBSOLETE);
                if (status != GEN_SUCCESS) {
                    CAB_DBG(CAB_GW_ERR, "Renaming the existing configuration file failed");
                    //unlock mutex lock
                    pthread_mutex_unlock(&g_lock);
                    pthread_mutex_unlock(&resetStateLock);
                    FUNC_EXIT
                    return FILE_DELETE_ERROR;
                }
            }
        }

        status = writeConfigJSON(g_gatewayConfigJSON);
        if (status != GEN_SUCCESS) {
            CAB_DBG(CAB_GW_ERR, "Writing config JSON object to configuration file failed");
            //unlock mutex lock
            pthread_mutex_unlock(&g_lock);
            pthread_mutex_unlock(&resetStateLock);
            FUNC_EXIT
            return status;
        }
    }
    else {
        CAB_DBG(CAB_GW_INFO, "Reading config from existing JSON config file");

        status = readConfigJSON(g_gatewayConfigJSON);
        if (status != GEN_SUCCESS) {
            CAB_DBG(CAB_GW_ERR, "Reading config JSON object from configuration file failed");
            //unlock mutex lock
            pthread_mutex_unlock(&g_lock);
            pthread_mutex_unlock(&resetStateLock);
            FUNC_EXIT
            return status;
        }

        /* Check and add new parameter if any */
        status = addNewConfigParam(g_gatewayConfigJSON);
        if(GEN_SUCCESS != status) {
            CAB_DBG(CAB_GW_ERR, "Adding new config parameter failed");
            //unlock mutex lock
            pthread_mutex_unlock(&g_lock);
            pthread_mutex_unlock(&resetStateLock);
            FUNC_EXIT
            return status;
        }

        /* update the g_gatewayConfig from current JSON */
        status = migrateFromJSON(g_gatewayConfigJSON, &g_gatewayConfig);
        if(GEN_SUCCESS != status) {
            CAB_DBG(CAB_GW_ERR, "Updating config struct from JSON object for existing configuration failed");
            //unlock mutex lock
            pthread_mutex_unlock(&g_lock);
            pthread_mutex_unlock(&resetStateLock);
            FUNC_EXIT
            return status;
        }
    }

    //If status Configuration parameter file does not exist
    if(access(FILE_PATH_STATUS_PARAM, F_OK) == -1) {
        /*initialize gateway status structure with default value*/
        CAB_DBG(CAB_GW_DEBUG, "Can't Access status file, creating new");
        g_gatewayConfigStatus = (cabGatewayStatus_t) { \
            .configFileUploadStatus = DEFAULT_CONFIG_FILE_UPLOAD_STATUS, \
            .mandatoryConfigFileUpdated = DEFAULT_CONFIG_FILE_UPDATE_STATUS, \
            .latestMandatoryConfigFileName = DEFAULT_CONFIG_FILE_NAME, \
            .rDiagCloudConnectionStatus = DEFAULT_CLOUD_CONNECTION_STATUS, \
            .mntCloudConnectionStatus = DEFAULT_CLOUD_CONNECTION_STATUS, \
            .internetConnectivityTimestamp = DEFAULT_INTERNT_CONNECTION_TIMESTAMP \
        };

        status = writeConfigStatus(&g_gatewayConfigStatus);       //update Configuration status file
        if(GEN_SUCCESS != status) {
            CAB_DBG(CAB_GW_ERR, "%s Initializing status file failed", FILE_PATH_STATUS_PARAM);
        }
    } else {
        //If Configuration status file exist
        CAB_DBG(CAB_GW_DEBUG, "Reading config status from existing file");
        status = readConfigStatus(&g_gatewayConfigStatus);       //read Configuration status file
        if(GEN_SUCCESS != status) {
            CAB_DBG(CAB_GW_ERR, "%s Reading the existing status file failed", FILE_PATH_STATUS_PARAM);
        }
    }

    /* Read s/w version every time as it is not configurable and may be changed after OTA	*/
    filePtr = fopen(FILE_PATH_SOFTWARE_VERSION, "r");
    if(filePtr == NULL) {
        CAB_DBG(CAB_GW_ERR, "Opening software version file failed");
        //unlock mutex lock
        pthread_mutex_unlock(&g_lock);
        pthread_mutex_unlock(&resetStateLock);
        return FILE_OPEN_FAIL;
    }

    status = fgets(&g_gatewayConfig.softwareVersion, MAX_LEN_SOFTWARE_VERSION, filePtr);
    for (index = 0; index < MAX_LEN_SOFTWARE_VERSION; index++) {
        if (! (isalnum(g_gatewayConfig.softwareVersion[index]) || g_gatewayConfig.softwareVersion[index] == '.' )) {
            g_gatewayConfig.softwareVersion[index] = '\0';
        }
    }

    if(status == NULL) {
        CAB_DBG(CAB_GW_ERR, "%s Reading software version file failed", FILE_PATH_SOFTWARE_VERSION);
        fclose(filePtr);
        //unlock mutex lock
        pthread_mutex_unlock(&g_lock);
        pthread_mutex_unlock(&resetStateLock);
        return FILE_READ_ERROR;
    }

    //close file
    fclose(filePtr);

    //update Configuration parameter file
    status = migrateToJSON(EXISTING_CONFIG);
    if (status != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "Creating config JSON object for existing configuration failed");
                    //unlock mutex lock
        pthread_mutex_unlock(&g_lock);
        pthread_mutex_unlock(&resetStateLock);
        FUNC_EXIT
        return status;
    }

    status = writeConfigJSON(g_gatewayConfigJSON);
    if (status != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "Writing config JSON object to configuration file failed");
            //unlock mutex lock
        pthread_mutex_unlock(&g_lock);
        pthread_mutex_unlock(&resetStateLock);
        FUNC_EXIT
        return status;
    }

    //unlock mutex lock
    pthread_mutex_unlock(&g_lock);
    pthread_mutex_unlock(&resetStateLock);

    FUNC_EXIT
    return status;
}

/* Start configuration by giving notification to other modules */
void startDeviceConfiguration()
{
    FUNC_ENTRY
    //lock to protect gateway configuration
    pthread_mutex_lock(&g_lock);

    if(g_gatewayConfig.isMandatoryInfoAvailable == true && \
            g_gatewayConfig.isRtcSync) {
        isGatewayConfigured = true;
    }

    if(isGatewayConfigured == true) {
        (*g_notifFunPtr)(GATEWAY_CONFIGURED, NULL);
    } else {
        (*g_notifFunPtr)(GATEWAY_NOT_CONFIGURED, NULL);
    }
    //unlock mutex lock
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
}

returnCode_e deInitConfigModule(void)           //De-Initialize configuration module
{
    FUNC_ENTRY

    CAB_DBG(CAB_GW_INFO, "Deinitializing configuration module");

    /*Any ongoing file operation will not allow to take this lock untill
      it completes. Once completed, no other thread will spawn to use event
     module as all other modules must have been deinit before */
    pthread_mutex_lock(&g_lock);
    memset(&g_gatewayConfig, 0, sizeof(g_gatewayConfig)); //Reset gateway configuration structure
    memset(&g_gatewayConfigStatus, 0, sizeof(cabGatewayStatus_t)); //Reset gateway configuration status structure
    free(g_gatewayConfigJSON);  //Free the memory allocated for the JSON object string
    pthread_mutex_unlock(&g_lock);

    //destroying lock
    pthread_mutex_destroy(&g_lock);
    pthread_mutex_destroy(&resetStateLock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/*This function is used to set configuration parameters*/
returnCode_e setConfigModuleParam(commandCode_e type, void * param)
{
    FUNC_ENTRY

    //lock to protect gateway configuration
    pthread_mutex_lock(&g_lock);

    int16_t status;
    uint8_t loopCnt,rtcSyncStatus;
    uint32_t tempSensorId;
    gatewayInfo_t *tempGatewayInfo;
    vehicleOptInfo_t *tempVehicleOptInfo;
    tpmsInfo_t *tempTpmsInfo;
    haloSNInfo_t *tempHaloSNInfo;
    tyreInfo_t *tempTyreInfo;
    tyrePosInfo_t *temptyrePosInfo;
    treadRecordInfo_t *tempTreadRecordInfo;
    targetPressureInfo_t *tempTargetPressureInfo;
    vehicleType_e oldVehicleType;
    selfDiagReq_t *selfDiagReqLocal;
    bool statusUpdated = false;

    switch(type) {
        case CFG_GW_SYSTEM_CMD :
            
            tempGatewayInfo = (gatewayInfo_t*)param;
            /*Argument Validation*/
	    if((tempGatewayInfo == NULL) || (strcmp(tempGatewayInfo->customerID,DEFAULT_CUSTOMER_ID)== 0) 
               || (strcmp(tempGatewayInfo->vehicleVin,DEFAULT_VEH_NUM)==0) || (strcmp(tempGatewayInfo->fleetID,DEFAULT_FLEET_ID)==0) 
               || (strcmp(tempGatewayInfo->vehicleType,"")==0)) {
                CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
		pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }

            /*set customer ID*/
            if(strcmp(g_gatewayConfig.customerID, tempGatewayInfo->customerID) != 0) {
                strcpy(g_gatewayConfig.customerID, tempGatewayInfo->customerID);
                CAB_DBG(CAB_GW_TRACE, "CUSTOMER_ID : %s", g_gatewayConfig.customerID);
            }

            /*set vehicle number*/
            if(strcmp(g_gatewayConfig.vehicleVin, tempGatewayInfo->vehicleVin) != 0) {
                strcpy(g_gatewayConfig.vehicleVin, tempGatewayInfo->vehicleVin);
                CAB_DBG(CAB_GW_TRACE, "VEHICLE_NUMBER : %s", g_gatewayConfig.vehicleVin);
            }

            /*set fleet ID*/
            if(strcmp(g_gatewayConfig.fleetID, tempGatewayInfo->fleetID) != 0) {
                strcpy(g_gatewayConfig.fleetID, tempGatewayInfo->fleetID);
                CAB_DBG(CAB_GW_TRACE, "FLEET_ID : %s", g_gatewayConfig.fleetID);
            }

            /*set vehicle type and vehicle def*/
            oldVehicleType = g_gatewayConfig.vehicleType;
            if(strcmp(tempGatewayInfo->vehicleType, \
                      vehicleTypeString[g_gatewayConfig.vehicleType]) != 0) {
                status = getVehType(tempGatewayInfo->vehicleType);
                if(status == -1) {
                    CAB_DBG(CAB_GW_ERR, "Invalid vehicle type");
                    //unlock mutex lock
                    pthread_mutex_unlock(&g_lock);
                    return DATA_INVALID;
                }
                g_gatewayConfig.vehicleType = status;
                g_gatewayConfig.vehicleDef_s = *vehicleDef[g_gatewayConfig.vehicleType];
                g_gatewayConfig.targetPressInfo.numberOfAxle = g_gatewayConfig.vehicleDef_s.maxAxle;
                for(loopCnt = 0; loopCnt < g_gatewayConfig.vehicleDef_s.maxAxle; loopCnt ++) {
                    g_gatewayConfig.targetPressInfo.tpmsThreshold[loopCnt] =  \
                            defPressureThresholdVal[g_gatewayConfig.vehicleType][loopCnt];
                }
                CAB_DBG(CAB_GW_TRACE, "VEHICLE_TYPE : %d", g_gatewayConfig.vehicleType);
            }

            /* set HALO SN config */
            setHaloSNConfig();

            /* Set Tyre config */
            setTyreConfig();

            // Reset the config file upload flag and set the mandatory config file updated flag
            g_gatewayConfigStatus.configFileUploadStatus = false;
            g_gatewayConfigStatus.mandatoryConfigFileUpdated = true;

            (*g_notifFunPtr)(MANDATORY_FIELDS, (void *)&oldVehicleType);

            memset(&tempGatewayInfo, 0, sizeof(tempGatewayInfo));
            g_gatewayConfig.isMandatoryInfoAvailable = true;
            break;
        case CFG_GW_OPT_CMD :
            if(g_gatewayConfig.isMandatoryInfoAvailable == true) {
                tempVehicleOptInfo = (vehicleOptInfo_t*)param;
	        
		/*Argument Validation*/
		if((tempVehicleOptInfo == NULL) || (strcmp(tempVehicleOptInfo->fleetName,DEFAULT_FLEET_NAME)==0)\
                || (strcmp(tempVehicleOptInfo->vehicleMake,DEFAULT_VEHICLE_MAKE)==0)|| (strcmp(tempVehicleOptInfo->tyreMake,DEFAULT_TYRE_MAKE)==0)\
                || (strcmp(tempVehicleOptInfo->tyreSize,DEFAULT_TYRE_SIZE)==0)|| (tempVehicleOptInfo->vehicleYear==DEFAULT_VEHICLE_YEAR)) {
               		CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
                     	pthread_mutex_unlock(&g_lock);
                    	return GEN_NULL_POINTING_ERROR;
                }

                /*set fleet name*/
                if(strcmp(g_gatewayConfig.fleetName, tempVehicleOptInfo->fleetName) != 0) {
                    strcpy(g_gatewayConfig.fleetName, tempVehicleOptInfo->fleetName);
                    CAB_DBG(CAB_GW_TRACE, "FLEET_NAME : %s", g_gatewayConfig.fleetName);
                    (*g_notifFunPtr)(FLEET_NAME, NULL);
                }
                /*set vehicle make*/
                if(strcmp(g_gatewayConfig.vehicleMake, tempVehicleOptInfo->vehicleMake) != 0) {
                    strcpy(g_gatewayConfig.vehicleMake, tempVehicleOptInfo->vehicleMake);
                    CAB_DBG(CAB_GW_TRACE, "VEHICLE_MAKE : %s", g_gatewayConfig.vehicleMake);
                    (*g_notifFunPtr)(VEHICLE_MAKE, NULL);
                }
                /*set tyre make*/
                if(strcmp(g_gatewayConfig.tyreMake, tempVehicleOptInfo->tyreMake) != 0) {
                    strcpy(g_gatewayConfig.tyreMake, tempVehicleOptInfo->tyreMake);
                    CAB_DBG(CAB_GW_TRACE, "TYRE_MAKE : %s", g_gatewayConfig.tyreMake);
                    (*g_notifFunPtr)(TYRE_MAKE, NULL);
                }
                /*set tyre size*/
                if(strcmp(g_gatewayConfig.tyreSize, tempVehicleOptInfo->tyreSize) != 0) {
                    strcpy(g_gatewayConfig.tyreSize, tempVehicleOptInfo->tyreSize);
                    CAB_DBG(CAB_GW_TRACE, "TYRE_SIZE : %s", g_gatewayConfig.tyreSize);
                    (*g_notifFunPtr)(TYRE_SIZE, NULL);
                }
                /*set vehicle year*/
                if(g_gatewayConfig.vehicleYear != tempVehicleOptInfo->vehicleYear) {
                    g_gatewayConfig.vehicleYear = tempVehicleOptInfo->vehicleYear;
                    CAB_DBG(CAB_GW_TRACE, "VEHICLE_YEAR : %d", g_gatewayConfig.vehicleYear);
                    (*g_notifFunPtr)(VEHICLE_YEAR, NULL);
                }
                memset(&tempVehicleOptInfo, 0, sizeof(tempVehicleOptInfo));
            } else {
                CAB_DBG(CAB_GW_ERR, "Mandatory information not available");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return CONFIG_NO_MANDATORY_INFO;
            }
            break;
        case CFG_GW_TPMS_PAIR_CMD :             //Add sensor id for given tyre and vehicle type
            if(g_gatewayConfig.isMandatoryInfoAvailable == true) {
                tempTpmsInfo = (tpmsInfo_t*)param;
                if(tempTpmsInfo == NULL) {
                    CAB_DBG(CAB_GW_ERR, "NULL pointing error");
                    //unlock mutex lock
                    pthread_mutex_unlock(&g_lock);
                    return GEN_NULL_POINTING_ERROR;
                }
                status = addSensorID(param);
                if(GEN_SUCCESS != status) {
                    CAB_DBG(CAB_GW_ERR, "Failed to add sensor ID");
                    //unlock mutex lock
                    pthread_mutex_unlock(&g_lock);
                    return status;
                }
                CAB_DBG(CAB_GW_DEBUG, "Sensor ID successfully added for given wheel id");
                CAB_DBG(CAB_GW_DEBUG, "sID: %X\tE/D : %d\tT/H : %d\tI/O : %d\tL/R: %d\taxle : %d\n", \
                        (*(tpmsInfo_t*)param).sensorID, (*(tpmsInfo_t*)param).isHaloEnable, \
                        (*(tpmsInfo_t*)param).attachedType, (*(tpmsInfo_t*)param).tyrePos,  \
                        (*(tpmsInfo_t*)param).vehicleSide, (*(tpmsInfo_t*)param).axleNum);
            } else {
                CAB_DBG(CAB_GW_ERR, "Mandatory information not available");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return CONFIG_NO_MANDATORY_INFO;
            }
            break;
        case CFG_GW_TPMS_UNPAIR_CMD :           //Remove sensor id for given tyre and vehicle type
            if(g_gatewayConfig.isMandatoryInfoAvailable == true) {
                if(((uint32_t*)param) == 0) {
                    CAB_DBG(CAB_GW_ERR, "NULL pointing error");
                    //unlock mutex lock
                    pthread_mutex_unlock(&g_lock);
                    return GEN_NULL_POINTING_ERROR;
                }
                tempSensorId = *(uint32_t*)param;
                for(loopCnt = 0; ((loopCnt < MAX_TPMS) && \
                                  (g_gatewayConfig.vehicleDef_s.tpmsConfig[loopCnt].wheelID != 0)); loopCnt++) {
                    if(g_gatewayConfig.vehicleDef_s.tpmsConfig[loopCnt].sensorID == tempSensorId) {
                        /**< To notify to get updated sensor data */
                        (*g_notifFunPtr)(REMOVE_SENSOR_ID, &g_gatewayConfig.vehicleDef_s.tpmsConfig[loopCnt]);
                        g_gatewayConfig.vehicleDef_s.tpmsConfig[loopCnt].sensorID = 0;
                        CAB_DBG(CAB_GW_DEBUG, "Sensor ID successfully removed for given wheel id");
                        CAB_DBG(CAB_GW_DEBUG, "Sensor ID Number : %X", tempSensorId);
                    }
                }
            } else {
                CAB_DBG(CAB_GW_ERR, "Mandatory information not available");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return CONFIG_NO_MANDATORY_INFO;
            }
            break;
        case CFG_GW_ATTACH_HALO_SN_CMD:
            if(g_gatewayConfig.isMandatoryInfoAvailable == true) {
                tempHaloSNInfo = (haloSNInfo_t *)param;
                if(tempHaloSNInfo == NULL) {
                    CAB_DBG(CAB_GW_ERR, "NULL pointing error");
                    //unlock mutex lock
                    pthread_mutex_unlock(&g_lock);
                    return GEN_NULL_POINTING_ERROR;
                }

                status = attachHaloSN(param);
                if(GEN_SUCCESS != status) {
                    CAB_DBG(CAB_GW_ERR, "Failed to attach HALO SN");
                    //unlock mutex lock
                    pthread_mutex_unlock(&g_lock);
                    return status;
                }
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Mandatory information not available");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return CONFIG_NO_MANDATORY_INFO;
            }
            break;
        case CFG_GW_DETACH_HALO_SN_CMD:
            if(g_gatewayConfig.isMandatoryInfoAvailable == true) {
                tempHaloSNInfo = (haloSNInfo_t *)param;
                if(tempHaloSNInfo == NULL) {
                    CAB_DBG(CAB_GW_ERR, "NULL pointing error");
                    //unlock mutex lock
                    pthread_mutex_unlock(&g_lock);
                    return GEN_NULL_POINTING_ERROR;
                }

                for (loopCnt = 0; loopCnt < MAX_HALO_SN; loopCnt++) {
                    if (strncmp(g_gatewayConfig.haloSNConfig[loopCnt].serialNo, tempHaloSNInfo->serialNo, MAX_LEN_HALO_SN) == 0) {
                        (*g_notifFunPtr)(DETACH_HALO_SN, &g_gatewayConfig.haloSNConfig[loopCnt]);
                        strncpy(g_gatewayConfig.haloSNConfig[loopCnt].serialNo, DEFAULT_HALO_SN, MAX_LEN_HALO_SN);
                        CAB_DBG(CAB_GW_DEBUG, "Detached HALO SN : %s", tempHaloSNInfo->serialNo);
                        break;
                    }
                }
            } else {
                CAB_DBG(CAB_GW_ERR, "Mandatory information not available");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return CONFIG_NO_MANDATORY_INFO;
            } 
            break;
        case CFG_GW_UPDATE_TYRE_DETAILS_CMD:
            CAB_DBG(CAB_GW_INFO, "---------CFG_GW_UPDATE_TYRE_DETAILS_CMD--------\n");
            if(g_gatewayConfig.isMandatoryInfoAvailable == true) {
                tempTyreInfo = (tyreInfo_t *)param;
                if(tempTyreInfo == NULL) {
                    CAB_DBG(CAB_GW_ERR, "NULL pointing error");
                    //unlock mutex lock
                    pthread_mutex_unlock(&g_lock);
                    return GEN_NULL_POINTING_ERROR;
                }

                status = updateTyreInfo(param);
                if(GEN_SUCCESS != status) {
                    CAB_DBG(CAB_GW_ERR, "Failed to update Tyre details");
                    //unlock mutex lock
                    pthread_mutex_unlock(&g_lock);
                    return status;
                }
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Mandatory information not available");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return CONFIG_NO_MANDATORY_INFO;
            }
            break;
        case CFG_GW_FLUSH_TYRE_DETAILS_CMD:
            CAB_DBG(CAB_GW_INFO, "---------CFG_GW_FLUSH_TYRE_DETAILS_CMD--------\n");
            if(g_gatewayConfig.isMandatoryInfoAvailable == true) {
                temptyrePosInfo = (tyrePosInfo_t *)param;
                if(temptyrePosInfo == NULL) {
                    CAB_DBG(CAB_GW_ERR, "NULL pointing error");
                    //unlock mutex lock
                    pthread_mutex_unlock(&g_lock);
                    return GEN_NULL_POINTING_ERROR;
                }

                for (loopCnt = 0; loopCnt < MAX_TYRE_NO; loopCnt++) {
                    if (g_gatewayConfig.tyreConfig[loopCnt].isTyreDetailConfigured == true && \
                        temptyrePosInfo->axleNum == g_gatewayConfig.tyreConfig[loopCnt].axleNum && \
                        temptyrePosInfo->vehicleSide == g_gatewayConfig.tyreConfig[loopCnt].vehicleSide && \
                        temptyrePosInfo->tyrePos == g_gatewayConfig.tyreConfig[loopCnt].tyrePos) {
                        (*g_notifFunPtr)(FLUSH_TIRE_DETAILS, &g_gatewayConfig.tyreConfig[loopCnt]);
                        g_gatewayConfig.tyreConfig[loopCnt].isTyreDetailConfigured = DEFAULT_TYRE_DETAILS_STATUS;
                        strncpy(g_gatewayConfig.tyreConfig[loopCnt].tyreSerialNo, DEFAULT_TYRE_SERIAL_NO, MAX_LEN_TYRE_SERIAL_NO);
                        strncpy(g_gatewayConfig.tyreConfig[loopCnt].tyreTIN, DEFAULT_TYRE_TIN, MAX_LEN_TYRE_TIN);
                        strncpy(g_gatewayConfig.tyreConfig[loopCnt].tyreMake, DEFAULT_TYRE_MAKE, MAX_LEN_TYRE_MAKE_STR);
                        strncpy(g_gatewayConfig.tyreConfig[loopCnt].tyreModel, DEFAULT_TYRE_MODEL, MAX_LEN_TYRE_MODEL);
                        strncpy(g_gatewayConfig.tyreConfig[loopCnt].tyreWidth, DEFAULT_TYRE_WIDTH, MAX_LEN_TYRE_WIDTH);
                        strncpy(g_gatewayConfig.tyreConfig[loopCnt].ratio, DEFAULT_RATIO, MAX_LEN_RATIO);
                        strncpy(g_gatewayConfig.tyreConfig[loopCnt].diameter, DEFAULT_DIAMETER, MAX_LEN_DIAMETER);
                        strncpy(g_gatewayConfig.tyreConfig[loopCnt].loadRating, DEFAULT_LOAD_RATING, MAX_LEN_LOAD_RATING);

                        for(uint8_t j=0; j<MAX_TREAD_DEPTH_RECORD; j++) {
                            strncpy(g_gatewayConfig.tyreConfig[loopCnt].treadDepthHistory[j].treadDepthValue, \
                                DEFAULT_TREAD_DEPTH, MAX_LEN_TREAD_DEPTH);
                            g_gatewayConfig.tyreConfig[loopCnt].treadDepthHistory[j].vehicleMileage = DEFAULT_VEHICLE_MILEAGE;
                            g_gatewayConfig.tyreConfig[loopCnt].treadDepthHistory[j].recordTime = DEFAULT_RECORD_TIME;
                        }
                        CAB_DBG(CAB_GW_DEBUG, "Flushed Tire details : %d %d %d", temptyrePosInfo->axleNum,
                            temptyrePosInfo->vehicleSide, temptyrePosInfo->tyrePos);
                        break;
                    }
                }
            }
            else {
                CAB_DBG(CAB_GW_ERR, "Mandatory information not available");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return CONFIG_NO_MANDATORY_INFO;
            }
            break;
        case CFG_GW_RECORD_TREAD_DETAILS_CMD:
            CAB_DBG(CAB_GW_INFO, "---------CFG_GW_RECORD_TREAD_DETAILS_CMD--------\n");
            if(g_gatewayConfig.isMandatoryInfoAvailable == true) {
                    tempTreadRecordInfo = (treadRecordInfo_t *)param;
                    if(tempTreadRecordInfo == NULL) {
                        CAB_DBG(CAB_GW_ERR, "NULL pointing error");
                        //unlock mutex lock
                        pthread_mutex_unlock(&g_lock);
                        return GEN_NULL_POINTING_ERROR;
                    }

                    status = updateTreadDepthHistory(param);
                    if(GEN_SUCCESS != status) {
                        CAB_DBG(CAB_GW_ERR, "Failed to update Tread depth record");
                        //unlock mutex lock
                        pthread_mutex_unlock(&g_lock);
                        return status;
                    }
                }
                else {
                    CAB_DBG(CAB_GW_ERR, "Mandatory information not available");
                    //unlock mutex lock
                    pthread_mutex_unlock(&g_lock);
                    return CONFIG_NO_MANDATORY_INFO;
                }
            break;
        case CFG_GW_TARGET_PRESSURE_CMD :
            CAB_DBG(CAB_GW_INFO, "---------CFG_GW_TARGET_PRESSURE_CMD--------\n");
            if(g_gatewayConfig.isMandatoryInfoAvailable == true) {
                tempTargetPressureInfo = (targetPressureInfo_t*)param;
                if(tempTargetPressureInfo == NULL) {
                    CAB_DBG(CAB_GW_ERR, "NULL pointing error");
                    //unlock mutex lock
                    pthread_mutex_unlock(&g_lock);
                    return GEN_NULL_POINTING_ERROR;
                }
                /*To set the target pressure*/
                if(tempTargetPressureInfo->numberOfAxle == g_gatewayConfig.vehicleDef_s.maxAxle) {
                    for(loopCnt = 0; loopCnt < g_gatewayConfig.vehicleDef_s.maxAxle; loopCnt++) {
                        CAB_DBG(CAB_GW_TRACE, "Axle : %d\tTarget pressure : %d", \
                                loopCnt + 1,tempTargetPressureInfo->tpmsThreshold[loopCnt]);
			if( tempTargetPressureInfo->tpmsThreshold[loopCnt] <= 0 || \
				tempTargetPressureInfo->tpmsThreshold[loopCnt] > 200 ){
				CAB_DBG(CAB_GW_ERR, "Invalid Data: Negative/Zero or >200 target pressure");
                    		//unlock mutex lock
		                pthread_mutex_unlock(&g_lock);
                		return DATA_INVALID;
			}
                    }
                    memcpy(&g_gatewayConfig.targetPressInfo.tpmsThreshold, tempTargetPressureInfo->tpmsThreshold, \
                           sizeof(tempTargetPressureInfo->tpmsThreshold));
                    (*g_notifFunPtr)(TPMS_THRESHOLD, NULL);
                } else {
                    CAB_DBG(CAB_GW_ERR, "Number of axle not matched");
                    //unlock mutex lock
                    pthread_mutex_unlock(&g_lock);
                    return DATA_INVALID;
                }
            } else {
                CAB_DBG(CAB_GW_ERR, "Mandatory information not available");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return CONFIG_NO_MANDATORY_INFO;
            }
            break;
        case CFG_GW_BATTERY_CMD :                               //set low battery threshold
            if(g_gatewayConfig.isMandatoryInfoAvailable == true) {
                if(((uint8_t*)param) == 0) {
                    CAB_DBG(CAB_GW_ERR, "NULL pointing error");
                    //unlock mutex lock
                    pthread_mutex_unlock(&g_lock);
                    return GEN_NULL_POINTING_ERROR;
                }
		if( (*((uint8_t*)param) < LOW_BATT_THRESHOLD_LOWER_LIMIT) || (*((uint8_t*)param) > LOW_BATT_THRESHOLD_UPPER_LIMIT) ) {
                    //unlock mutex lock
                    pthread_mutex_unlock(&g_lock);
                    return DATA_INVALID;
                }

                if(g_gatewayConfig.lowBatteryThreshold != *(uint8_t*)param) {
                    g_gatewayConfig.lowBatteryThreshold = *(uint8_t*)param;
                    CAB_DBG(CAB_GW_TRACE, "LOW_BAT_TH : %d", g_gatewayConfig.lowBatteryThreshold);
                    (*g_notifFunPtr)(LOW_BATTERY_THRESHOLD, NULL);
                }
            } else {
                CAB_DBG(CAB_GW_ERR, "Mandatory information not available");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return CONFIG_NO_MANDATORY_INFO;
            }
            break;
        case CFG_GW_CLOUD_CMD :                                     //set cloud URI
            {
		char *temp = NULL;
                if(((char*)param == NULL) || (strcmp((char*)param, "")==0)) {
                    CAB_DBG(CAB_GW_ERR, "NULL pointing error");
                    //unlock mutex lock
                    pthread_mutex_unlock(&g_lock);
                    return GEN_NULL_POINTING_ERROR;
                }

		/*Check if space is present in string or not*/
		temp = (char *)strtok((char *)param, " ");
		if (temp != NULL) {
        	    temp = (char *)strtok(NULL, " ");
		    if (temp != NULL) {
                    	pthread_mutex_unlock(&g_lock);
		        return DATA_INVALID;
		    }
		} else {
                    pthread_mutex_unlock(&g_lock);
		    return DATA_INVALID;
		}

                if(strcmp(g_gatewayConfig.cloudURI, (char*)param) != 0) {
                    strcpy(g_gatewayConfig.cloudURI, (char*)param);
                    CAB_DBG(CAB_GW_TRACE, "CLOUD_URI : %s", g_gatewayConfig.cloudURI);
                    (*g_notifFunPtr)(CLOUD_URI, NULL);
                }
            }
            break;
        case CFG_GW_TPMS_VENDOR_CMD :                   //to set sensor vendor type
		
	    /* TODO : Vendor type is selected based on hardware revision.
		      Can't change from mobile application.*/
	    pthread_mutex_unlock(&g_lock);
	    return DATA_INVALID;
            if(g_gatewayConfig.isMandatoryInfoAvailable == true) {
                if(((char*)param == NULL)) {
                    CAB_DBG(CAB_GW_ERR, "NULL pointing error");
                    //unlock mutex lock
                    pthread_mutex_unlock(&g_lock);
                    return GEN_NULL_POINTING_ERROR;
                }
                if(strcmp((char*)param, vendorTypeString[g_gatewayConfig.sensorType]) != 0) {
                    status = getVendorType((char*)param);
                    if(status == -1) {
                        CAB_DBG(CAB_GW_ERR, "Here 3");
                        CAB_DBG(CAB_GW_ERR, "Invalid vehicle type");
                        //unlock mutex lock
                        pthread_mutex_unlock(&g_lock);
                        return DATA_INVALID;
                    }
                    g_gatewayConfig.sensorType = status;
                    CAB_DBG(CAB_GW_TRACE, "SENSOR_TYPE : %d", g_gatewayConfig.sensorType);
                    (*g_notifFunPtr)(SENSOR_TYPE, NULL);
                }
            } else {
                CAB_DBG(CAB_GW_ERR, "Mandatory information not available");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return CONFIG_NO_MANDATORY_INFO;
            }
            break;
        case SYS_NOTIFY_BLE_CONNECTED :                 //to notify M&T module for BLE connected
            (*g_notifFunPtr)(BLE_CONNECTED, NULL);
            break;
        case SYS_NOTIFY_BLE_DISCONNECTD :               //to notify M&T module for BLE disconnected
            (*g_notifFunPtr)(BLE_DISCONNECTED, NULL);
            break;
        case SYS_ADVERTISEMENT_STARTED :                //To notify M&T module for BLE advertisement started
            (*g_notifFunPtr)(BLE_ADVERTISEMENT_STARTED, NULL);
            CAB_DBG(CAB_GW_DEBUG, "BLE advertisement started event received");
            break;
        case SYS_BLE_MAC_CMD :                          //to set BLE MAC adderess
            if(((uint8_t*)param == NULL)) {
                CAB_DBG(CAB_GW_ERR, "NULL pointing error");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            if(isMemSame((char*)param, (char *)g_defaultBLEMAC, MAX_LEN_BLE_MAC) != true) {
            	memcpy(g_gatewayConfig.bleMac, param, MAX_LEN_BLE_MAC);
            	for(loopCnt = 0; loopCnt < MAX_LEN_BLE_MAC; loopCnt ++) {
            	    CAB_DBG(CAB_GW_DEBUG, "BLE_MAC : %02X", g_gatewayConfig.bleMac[loopCnt]);
            	}
            	(*g_notifFunPtr)(BLE_MAC, NULL);
	    } else {
            	CAB_DBG(CAB_GW_INFO, "BLE_MAC cmd received as default");
	    }
            if(isMemSame((void*)g_gatewayConfig.imeiNumber, (void *)g_defaultLTEIMEI, MAX_LEN_LTE_IMEI-1) != true) {
                (*g_notifFunPtr)(GATEWAY_ID, NULL);
            }
            break;
        case CFG_BLE_PASSKEY_CMD :                      //to set BLE passkey
            if(((char*)param == NULL)) {
                CAB_DBG(CAB_GW_ERR, "NULL pointing error");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(g_gatewayConfig.blePassKey, (char*)param, sizeof(g_gatewayConfig.blePassKey));
            CAB_DBG(CAB_GW_DEBUG, "BLE_PASSKEY : %s", g_gatewayConfig.blePassKey);
            (*g_notifFunPtr)(BLE_PASSKEY, NULL);
            break;
        case SYS_SET_LTE_IMEI_CMD :                     //to set LTE IMEI number
            if(((uint8_t*)param == NULL)) {
                CAB_DBG(CAB_GW_ERR, "NULL pointing error");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(g_gatewayConfig.imeiNumber, param, MAX_LEN_LTE_IMEI);
            for(loopCnt = 0; loopCnt < MAX_LEN_LTE_IMEI; loopCnt ++) {
                CAB_DBG(CAB_GW_DEBUG, "LTE_IMEI : %02X", g_gatewayConfig.imeiNumber[loopCnt]);
            }
            if(isMemSame((char *)g_gatewayConfig.bleMac, (char *)g_defaultBLEMAC, MAX_LEN_BLE_MAC) != true) {
                (*g_notifFunPtr)(GATEWAY_ID, NULL);
            }
            break;
        case SYS_SET_SIM_ICCID_CMD :
            if(((char*)param == NULL)) {
                CAB_DBG(CAB_GW_ERR, "NULL pointing error");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            strcpy(g_gatewayConfig.simIccid, (char*)param);
            //CAB_DBG(CAB_GW_DEBUG, "SIM_ICCID : %s", g_gatewayConfig.simIccid);
            break;
        case CFG_MOBILE_UTC_TIME_CMD :
            if((time_t*)param == NULL) {
                CAB_DBG(CAB_GW_ERR, "NULL pointing error");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
	    /*Allow time configuration from mobile until gateway received network time 
								      from LTE interface*/
            CAB_DBG(CAB_GW_DEBUG, "g_gatewayConfig.isRtcSync=%d", g_gatewayConfig.isRtcSync);
	    rtcSyncStatus = ((g_gatewayConfig.isRtcSync) & (1 << RTC_NW_SYNC_BIT));
            if(!rtcSyncStatus) {
                CAB_DBG(CAB_GW_DEBUG, "Time Set Request = %ld", *(time_t *)param);
                (*g_notifFunPtr)(TIME_STAMP, param);
            }
            break;

	case SYS_DIAG_TIME_CMD:
            if((time_t*)param == NULL) {
                CAB_DBG(CAB_GW_ERR, "NULL pointing error");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
	    CAB_DBG(CAB_GW_DEBUG, "Setting LTE diag time : %ld", *(time_t *)param);
	    memcpy(&g_gatewayConfig.diagLTETime, param, sizeof(time_t));
	    break;
        case SYS_SET_HARDWARE_VERSION :
            if(((char*)param == NULL)) {
                CAB_DBG(CAB_GW_ERR, "NULL pointing error");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            strcpy(g_gatewayConfig.hardwareVersion, (char*)param);
            CAB_DBG(CAB_GW_TRACE, "HARDWARE_VERSION : %s", g_gatewayConfig.hardwareVersion);
            break;
        case SYS_RTC_FLAG :
	    if(((char*)param == NULL)) {
                CAB_DBG(CAB_GW_ERR, "NULL pointing error");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
	    status = *((uint8_t *)param);
	    if(!status)
            	g_gatewayConfig.isRtcSync = 0;
	    else
            	g_gatewayConfig.isRtcSync = (g_gatewayConfig.isRtcSync | status);
            break;
        case SELF_DIAG_GW_REQ_CMD:
            selfDiagReqLocal = (selfDiagReq_t *)param;
            if(selfDiagReqLocal == NULL) {
                CAB_DBG(CAB_GW_ERR, "NULL pointing error");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            for(loopCnt = 0; loopCnt < MAX_SELF_DIAG_TPMS_ID; loopCnt++) {
                g_selfDiagReq.sensorID[loopCnt] = selfDiagReqLocal->sensorID[loopCnt];
                CAB_DBG(CAB_GW_TRACE, "TPMS sensor ID[%d] : %X", loopCnt, selfDiagReqLocal->sensorID[loopCnt]);
            }
            if (g_selfDiagStatus.isTestCompleted == false) {
                (*g_notifFunPtr)(SELF_DIAG_REQ, NULL);
            }
            break;
        case SELF_DIAG_GW_RESP_CMD:
            if(NULL == (selfDiagStatus_t *)param) {
                CAB_DBG(CAB_GW_ERR, "NULL pointing error");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(&g_selfDiagStatus, param, sizeof(g_selfDiagStatus));
            break;
	case SYS_GW_RESET_CMD:
	    {
		    if(((char*)param == NULL)) {
			    CAB_DBG(CAB_GW_ERR, "NULL pointing error");
			    //unlock mutex lock
			    pthread_mutex_unlock(&g_lock);
			    return GEN_NULL_POINTING_ERROR;
		    }

		    pthread_mutex_lock(&resetStateLock);
		    if (strncmp(param, "Full Reset", sizeof(MAX_RESET_CMD_LEN)) == 0) {
			    resetState = true;
			    CAB_DBG(CAB_GW_TRACE, "FACTORY RESET: %s", resetState ? "true" : "false" );
		    } else {
			    resetState = false;
			    CAB_DBG(CAB_GW_TRACE, "FACTORY RESET: %s", resetState ? "true" : "false" );
		    }
		    pthread_mutex_unlock(&resetStateLock);

		    (*g_notifFunPtr)(FACTORY_RESET, NULL);
	    }
	    break;
  case CHANGE_FLEET_ID_CMD:
      {
        if(((char*)param == NULL)) {
          CAB_DBG(CAB_GW_ERR, "NULL pointing error");
          //unlock mutex lock
          pthread_mutex_unlock(&g_lock);
          return GEN_NULL_POINTING_ERROR;
        }

        strncpy(g_gatewayConfig.fleetID, (char *)param, MAX_LEN_FLEET_ID);
        (*g_notifFunPtr)(FLEET_ID, param);
      } break;
	case INTERNET_CONNECTIVITY_TIMESTAMP:
	{
		if(((time_t*)param == NULL)) {
			CAB_DBG(CAB_GW_ERR, "NULL pointing error");
			//unlock mutex lock
			pthread_mutex_unlock(&g_lock);
			return GEN_NULL_POINTING_ERROR;
		}

		memcpy(&g_gatewayConfigStatus.internetConnectivityTimestamp, param, sizeof(time_t));
        statusUpdated = true;
	} break;
	case CONIF_FILE_UPLOAD_STATUS_CMD:
	{
		if(((bool*)param == NULL)) {
			CAB_DBG(CAB_GW_ERR, "NULL pointing error");
			//unlock mutex lock
			pthread_mutex_unlock(&g_lock);
			return GEN_NULL_POINTING_ERROR;
		}

		memcpy(&g_gatewayConfigStatus.configFileUploadStatus, param, sizeof(bool));
        statusUpdated = true;
	} break;
	case CONIF_FILE_CREATION_STATUS_CMD:
	{
		if(((char*)param == NULL)) {
			CAB_DBG(CAB_GW_ERR, "NULL pointing error");
			//unlock mutex lock
			pthread_mutex_unlock(&g_lock);
			return GEN_NULL_POINTING_ERROR;
		}

		strncpy(g_gatewayConfigStatus.latestMandatoryConfigFileName, (char*)param, MAX_FILENAME_LEN);
		g_gatewayConfigStatus.mandatoryConfigFileUpdated = false;
        statusUpdated = true;
	} break;
	case RDIAG_CLOUD_CONNECTION_STATUS_CMD:
	{
		if(((bool*)param == NULL)) {
			CAB_DBG(CAB_GW_ERR, "NULL pointing error");
			//unlock mutex lock
			pthread_mutex_unlock(&g_lock);
			return GEN_NULL_POINTING_ERROR;
		}

		memcpy(&g_gatewayConfigStatus.rDiagCloudConnectionStatus, param, sizeof(bool));
        statusUpdated = true;

	} break;
	case MNT_CLOUD_CONNECTION_STATUS_CMD:
	{
		if(((bool*)param == NULL)) {
			CAB_DBG(CAB_GW_ERR, "NULL pointing error");
			//unlock mutex lock
			pthread_mutex_unlock(&g_lock);
			return GEN_NULL_POINTING_ERROR;
		}

		memcpy(&g_gatewayConfigStatus.mntCloudConnectionStatus, param, sizeof(bool));
        statusUpdated = true;

	} break;
	default:
            CAB_DBG(CAB_GW_ERR, "Invalid option");
            //unlock mutex lock
            pthread_mutex_unlock(&g_lock);
            return GEN_INVALID_TYPE;
    }

    /* Skip updating the config file once Factory request is requested. */
    pthread_mutex_lock(&resetStateLock);
    if ( !resetState )
    {
        /* Only status parameter is updated so update the status file otherwise update the config file */
        if (statusUpdated) {
            status = writeConfigStatus(&g_gatewayConfigStatus);       //update Configuration status file
            if(GEN_SUCCESS != status) {
                CAB_DBG(CAB_GW_ERR, "%s updating the status failed", FILE_PATH_STATUS_PARAM);
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return FILE_WRITE_ERROR;
            }
        }
        else {
            status = migrateToJSON(EXISTING_CONFIG);
            if (status != GEN_SUCCESS) {
                CAB_DBG(CAB_GW_ERR, "Updating config JSON object for existing configuration failed");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return status;
            }

            status = writeConfigJSON(g_gatewayConfigJSON);
            if (status != GEN_SUCCESS) {
                CAB_DBG(CAB_GW_ERR, "%s updating the configuration failed", NEW_FILE_PATH_CONFIG_PARAM);
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return FILE_WRITE_ERROR;
            }
        }
    }
    pthread_mutex_unlock(&resetStateLock);

    //unlock mutex lock
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e setVendorTypeTemp(sensorVendorType_e sensor)
{
	FUNC_ENTRY
        pthread_mutex_lock(&g_lock);
        g_gatewayConfig.sensorType = sensor;
        pthread_mutex_unlock(&g_lock);
        CAB_DBG(CAB_GW_DEBUG, "SENSOR_TYPE : %d", g_gatewayConfig.sensorType);
	FUNC_EXIT
	return GEN_SUCCESS;
}

returnCode_e getVendorTypeTemp(sensorVendorType_e *sensor)
{
	FUNC_ENTRY
	if (sensor == NULL) {
		CAB_DBG(CAB_GW_ERR, "invalid arg in get vendor type");
		return GEN_NULL_POINTING_ERROR;
	}
        pthread_mutex_lock(&g_lock);
        *sensor = g_gatewayConfig.sensorType;
        CAB_DBG(CAB_GW_DEBUG, "SENSOR_TYPE : %d", g_gatewayConfig.sensorType);
        pthread_mutex_unlock(&g_lock);
	FUNC_EXIT
	return GEN_SUCCESS;
}

/*This function is used to get configuration parameters*/
returnCode_e getConfigModuleParam(configParam_e type, void * param)
{
    FUNC_ENTRY

    if(param == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        //unlock mutex lock
        return GEN_NULL_POINTING_ERROR;
    }
    //lock to protect gateway configuration
    pthread_mutex_lock(&g_lock);

    uint8_t loopCnt;
    uint32_t tempWheelID;
    switch(type) {
        case ALL_DATA :                             //get all parameters of cabgateway config
            memcpy(param, &g_gatewayConfig, sizeof(g_gatewayConfig));
            break;
        case VEHICLE_NUMBER :                               //get vehicle no
            if(strcmp(g_gatewayConfig.vehicleVin, DEFAULT_VEH_NUM) == 0) {
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(param, &g_gatewayConfig.vehicleVin, sizeof(g_gatewayConfig.vehicleVin));
            break;
        case CUSTOMER_ID :                                  //get customer ID
            if(strcmp(g_gatewayConfig.customerID, DEFAULT_CUSTOMER_ID) == 0) {
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(param, &g_gatewayConfig.customerID, sizeof(g_gatewayConfig.customerID));
            break;
        case VEHICLE_TYPE :                                 //get vehicle type
            if(g_gatewayConfig.vehicleType == DEFAULT_VEH_TYPE) {
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(param, &g_gatewayConfig.vehicleType, sizeof(g_gatewayConfig.vehicleType));
            break;
        case FLEET_NAME :                                   //get fleet name
            if(strcmp(g_gatewayConfig.fleetName, DEFAULT_FLEET_NAME) == 0) {
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(param, &g_gatewayConfig.fleetName, sizeof(g_gatewayConfig.fleetName));
            break;
        case FLEET_ID :                                     //get fleet ID
            if(strcmp(g_gatewayConfig.fleetID, DEFAULT_FLEET_ID) == 0) {
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(param, &g_gatewayConfig.fleetID, sizeof(g_gatewayConfig.fleetID));
            break;
        case VEHICLE_MAKE :                                 //get vehicle make
            if(strcmp(g_gatewayConfig.vehicleMake, DEFAULT_VEHICLE_MAKE) == 0) {
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(param, &g_gatewayConfig.vehicleMake, sizeof(g_gatewayConfig.vehicleMake));
            break;
        case VEHICLE_YEAR :                                 //get vehicle year
            if(g_gatewayConfig.vehicleYear ==  DEFAULT_VEHICLE_YEAR) {
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(param, &g_gatewayConfig.vehicleYear, sizeof(g_gatewayConfig.vehicleYear));
            break;
        case TYRE_MAKE :                                    //get tyre make
            if(strcmp(g_gatewayConfig.tyreMake, DEFAULT_TYRE_MAKE) == 0) {
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(param, &g_gatewayConfig.tyreMake, sizeof(g_gatewayConfig.tyreMake));
            break;
        case TYRE_SIZE :                                    //get tyre size
            if(strcmp(g_gatewayConfig.tyreSize, DEFAULT_TYRE_SIZE) == 0) {
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(param, &g_gatewayConfig.tyreSize, sizeof(g_gatewayConfig.tyreSize));
            break;
        case GET_SENSOR_ID:                     //get sensor id for given tyre and vehicle type
            if(NULL == (tpmsInfo_t*)param) {
                CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            tempWheelID = getWheelId(param);
            for(loopCnt = 0; ((loopCnt < MAX_TPMS) && \
                              (g_gatewayConfig.vehicleDef_s.tpmsConfig[loopCnt].wheelID != 0)); loopCnt++) {
                if(g_gatewayConfig.vehicleDef_s.tpmsConfig[loopCnt].wheelID == tempWheelID) {
                    (*(tpmsInfo_t*)param).sensorID =    \
                                                        g_gatewayConfig.vehicleDef_s.tpmsConfig[loopCnt].sensorID;
                }
            }
            break;
        case GET_ALL_SENSOR_ID :                //get all tpms information
            memcpy(param, &g_gatewayConfig.vehicleDef_s, sizeof(g_gatewayConfig.vehicleDef_s));
            break;
        case TPMS_THRESHOLD:                  //get TPMS threshold for front and rear group
            memcpy(param, &g_gatewayConfig.targetPressInfo, sizeof(g_gatewayConfig.targetPressInfo));
            break;
        case LOW_BATTERY_THRESHOLD:                            //get low battery threshold
            memcpy(param, &g_gatewayConfig.lowBatteryThreshold, \
                   sizeof(g_gatewayConfig.lowBatteryThreshold));
            break;
        case CLOUD_URI:                                        //get cloud URI
            if(strcmp(g_gatewayConfig.cloudURI, "") == 0) {
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(param, &g_gatewayConfig.cloudURI, sizeof(g_gatewayConfig.cloudURI));
            break;
        case SENSOR_TYPE:                                    //get sensor type
            if(g_gatewayConfig.sensorType == MAX_VENDORS) {
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(param, &g_gatewayConfig.sensorType, sizeof(g_gatewayConfig.sensorType));
            break;
        case BLE_PASSKEY :                                  //get BLE passkey
            if(strcmp(g_gatewayConfig.blePassKey, DEFAULT_BLE_PASSKEY) == 0) {
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(param, &g_gatewayConfig.blePassKey, sizeof(g_gatewayConfig.blePassKey));
            break;
        case BLE_MAC :                                      //get BLE mac adddress
            if(isMemSame((char *)g_gatewayConfig.bleMac, (char *)g_defaultBLEMAC, MAX_LEN_BLE_MAC) == true) {
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(param, &g_gatewayConfig.bleMac, sizeof(g_gatewayConfig.bleMac));
            break;
        case LTE_IMEI :                                     //get IMEI number
            if(isMemSame((void*)g_gatewayConfig.imeiNumber, (void *)g_defaultLTEIMEI, MAX_LEN_LTE_IMEI-1) == true) {
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(param, &g_gatewayConfig.imeiNumber, sizeof(g_gatewayConfig.imeiNumber));
            break;
        case SOFTWARE_VERSION :                             //get software version
            if(strcmp(g_gatewayConfig.softwareVersion, DEFAULT_SOFTWARE_VERSION) == 0) {
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(param, &g_gatewayConfig.softwareVersion, \
                   sizeof(g_gatewayConfig.softwareVersion));
            break;
        case HARDWARE_VERSION :                             //get hardware version
            if(strcmp(g_gatewayConfig.hardwareVersion, DEFAULT_HARDWARE_VERSION) == 0) {
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(param, &g_gatewayConfig.hardwareVersion, \
                   sizeof(g_gatewayConfig.hardwareVersion));
            break;
        case SIM_ICCID :                                    //get sim iccid
            if(strcmp(g_gatewayConfig.simIccid, DEFAULT_LTE_ICCID) == 0) {
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(param, &g_gatewayConfig.simIccid, sizeof(g_gatewayConfig.simIccid));
	    break;
        case GATEWAY_ID :                                   //get Gateway ID
            if((isMemSame( (char *)g_gatewayConfig.bleMac, (char *)g_defaultBLEMAC, MAX_LEN_BLE_MAC) == true) || \
                    (isMemSame((void*)g_gatewayConfig.imeiNumber, (void*)g_defaultLTEIMEI, MAX_LEN_LTE_IMEI-1) == true)) {
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(g_gatewayConfig.gatewayId, g_gatewayConfig.bleMac, \
                   sizeof(g_gatewayConfig.bleMac));
            memcpy(g_gatewayConfig.gatewayId + MAX_LEN_BLE_MAC, &g_gatewayConfig.imeiNumber, \
                   sizeof(g_gatewayConfig.imeiNumber));
            memcpy(param, &g_gatewayConfig.gatewayId, sizeof(g_gatewayConfig.gatewayId));
            break;
        case RTC_FLAG :
            *(uint8_t *)param = g_gatewayConfig.isRtcSync;
            break;
        case MANDATORY_FLAG :
            *(bool *)param = g_gatewayConfig.isMandatoryInfoAvailable;
            break;
	case DIAG_TIME_CMD:
	    CAB_DBG(CAB_GW_DEBUG, "Getting LTE diag time : %ld", g_gatewayConfig.diagLTETime);
	    memcpy(param, &g_gatewayConfig.diagLTETime, sizeof(time_t));
	    break;
        case SELF_DIAG_GW_REQ_CMD:
            if(NULL == (selfDiagReq_t *)param) {
                CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            for(loopCnt = 0; loopCnt < MAX_SELF_DIAG_TPMS_ID; loopCnt++) {
                ((selfDiagReq_t *)param)->sensorID[loopCnt] = g_selfDiagReq.sensorID[loopCnt];
                CAB_DBG(CAB_GW_TRACE, "TPMS sensor ID[%d] : %X", loopCnt, g_selfDiagReq.sensorID[loopCnt]);
            }
            break;
	case SYS_GW_RESET_CMD:
	    pthread_mutex_lock(&resetStateLock);
	    *(char *)param = resetState;
	    pthread_mutex_unlock(&resetStateLock);
	    break;
        case SELF_DIAG_GW_RESP_CMD:
            if(NULL == (selfDiagStatus_t *)param) {
                CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(param, &g_selfDiagStatus, sizeof(g_selfDiagStatus));
            break;
        case INSTALLATION_STATUS_CMD:
            if(NULL == (cabGatewayStatus_t *)param) {
                CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(param, &g_gatewayConfigStatus, sizeof(cabGatewayStatus_t));
            break;
        default:
            CAB_DBG(CAB_GW_ERR, "Invalid option");
            //unlock mutex lock
            pthread_mutex_unlock(&g_lock);
            return GEN_INVALID_TYPE;
    }
    //unlock mutex lock
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e getLedPatterns(void * data)
{
    FUNC_ENTRY

    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }
    memcpy(data, ledPattern, sizeof(ledPattern));

    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e  getConfigForBle(commandCode_e type , void * data)
{
    FUNC_ENTRY

    uint8_t status;
    gatewayInfo_t tempGatewayInfo;
    vehicleOptInfo_t tempVehicleOptInfo;

    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //lock to protect gateway configuration
    //printLocalTime();
    pthread_mutex_lock(&g_lock);

    switch(type) {
        case GET_CFG_GW_SYSTEM_CMD :            /**< get gateway mandatory information */
            tempGatewayInfo = *(gatewayInfo_t*)data;
            strcpy(tempGatewayInfo.customerID, g_gatewayConfig.customerID);
            strcpy(tempGatewayInfo.fleetID, g_gatewayConfig.fleetID);
            strcpy(tempGatewayInfo.vehicleType, vehicleTypeString[g_gatewayConfig.vehicleType]);
            strcpy(tempGatewayInfo.vehicleVin, g_gatewayConfig.vehicleVin);
            memcpy(data, &tempGatewayInfo, sizeof(tempGatewayInfo));
            break;
        case GET_CFG_GW_OPT_CMD :               /**< get vehicle optional information */
            tempVehicleOptInfo = *(vehicleOptInfo_t*)data;
            strcpy(tempVehicleOptInfo.fleetName, g_gatewayConfig.fleetName);
            strcpy(tempVehicleOptInfo.vehicleMake, g_gatewayConfig.vehicleMake);
            strcpy(tempVehicleOptInfo.tyreMake, g_gatewayConfig.tyreMake);
            strcpy(tempVehicleOptInfo.tyreSize, g_gatewayConfig.tyreSize);
            tempVehicleOptInfo.vehicleYear = g_gatewayConfig.vehicleYear;
            memcpy(data, &tempVehicleOptInfo, sizeof(tempVehicleOptInfo));
            break;
        case GET_CFG_GW_TPMS_VENDOR_CMD :       /**< get sensor vendor type */
            strcpy(data, vendorTypeString[g_gatewayConfig.sensorType]);
            break;
        case GET_CFG_GW_CLOUD_CMD :             /**< get cloud URI */
            memcpy(data, &g_gatewayConfig.cloudURI, sizeof(g_gatewayConfig.cloudURI));
            break;
        case GET_CFG_GW_TARGET_PRESSURE_CMD :   /**< get TPMS threshold for all axles */
	    memcpy(data,&g_gatewayConfig.targetPressInfo, sizeof(g_gatewayConfig.targetPressInfo));
            /*tempTargetPressureInfo = *(targetPressureInfo_t*)data;
            memcpy(tempTargetPressureInfo.tpmsThreshold, &g_gatewayConfig.tpmsThreshold, \
                   sizeof(g_gatewayConfig.tpmsThreshold));
            tempTargetPressureInfo.numberOfAxle = g_gatewayConfig.vehicleDef_s.maxAxle;
            memcpy(data, &tempTargetPressureInfo, sizeof(tempTargetPressureInfo));*/
            break;
        case GET_CFG_GW_BATTERY_CMD :           /**< get low battery threshold */
            memcpy(data, &g_gatewayConfig.lowBatteryThreshold, \
                   sizeof(g_gatewayConfig.lowBatteryThreshold));
            break;
        case SYS_GET_LTE_IMEI_CMD :             /**< get lte imei number */
            memcpy(data, &g_gatewayConfig.imeiNumber, sizeof(g_gatewayConfig.imeiNumber));
            break;
        case SYS_GET_SYSTEM_ID :                /**< get system id */
            if((isMemSame((char*)g_gatewayConfig.bleMac, (char *)g_defaultBLEMAC, MAX_LEN_BLE_MAC) == true) || \
                    (isMemSame((void*)g_gatewayConfig.imeiNumber, (void*)g_defaultLTEIMEI, MAX_LEN_LTE_IMEI-1) == true)) {
                //unlock mutex lock
                pthread_mutex_unlock(&g_lock);
                return GEN_NULL_POINTING_ERROR;
            }
            memcpy(g_gatewayConfig.gatewayId, g_gatewayConfig.bleMac, \
                   sizeof(g_gatewayConfig.bleMac));
            memcpy(g_gatewayConfig.gatewayId + MAX_LEN_BLE_MAC, &g_gatewayConfig.imeiNumber, \
                   sizeof(g_gatewayConfig.imeiNumber));
            memcpy(data, &g_gatewayConfig.gatewayId, sizeof(g_gatewayConfig.gatewayId));
            break;
        case GET_CFG_GW_TPMS_PAIR_CMD :
            //unlock mutex lock
            pthread_mutex_unlock(&g_lock);
            status = getPairedTpmsList((tpmsPairedInfo_t*)data);
    	    pthread_mutex_lock(&g_lock);
            if(status != GEN_SUCCESS) {
                CAB_DBG(CAB_GW_ERR, "Failed to get the paired TPMS information");
            	pthread_mutex_unlock(&g_lock);
                return CONFIG_NO_TPMS_PAIRED;
            }
            break;
        case GET_CFG_GW_ALL_HALO_SN_CMD:
            //unlock mutex lock
            pthread_mutex_unlock(&g_lock);
            status = getAttachedHaloSNList((attachedHaloSNInfo_t*)data);
            pthread_mutex_lock(&g_lock);
            if(status != GEN_SUCCESS) {
                CAB_DBG(CAB_GW_ERR, "Failed to get the attached HALO SN information");
                pthread_mutex_unlock(&g_lock);
                return CONFIG_NO_HALO_SN_ATTACHED;
            }
            break;
        case GET_CFG_GW_TYRE_DETAILS_CMD:
            //unlock mutex lock
            pthread_mutex_unlock(&g_lock);
            status = getTyreDetail((tyreInfo_t*)data);
            pthread_mutex_lock(&g_lock);
            if(status != GEN_SUCCESS) {
                CAB_DBG(CAB_GW_ERR, "Failed to get the tire details for given tire position");
                pthread_mutex_unlock(&g_lock);
                return CONFIG_NO_TYRE_DETAIL_AVAILABLE;
            }
            break;
        case GET_CFG_GW_TREAD_HISTORY_CMD:
            //unlock mutex lock
            pthread_mutex_unlock(&g_lock);
            status = getTreadDepthHistory((configuredTreadRecordInfo_t*)data);
            pthread_mutex_lock(&g_lock);
            if(status != GEN_SUCCESS) {
                CAB_DBG(CAB_GW_ERR, "Failed to get the Tread depth history for given tire position");
                pthread_mutex_unlock(&g_lock);
                return CONFIG_NO_TREAD_DEPTH_HISTORY_AVAILABLE;
            }
            break;
        default :
            CAB_DBG(CAB_GW_ERR, "Invalid option");
            //unlock mutex lock
            pthread_mutex_unlock(&g_lock);
            return GEN_INVALID_TYPE;
    }

    //unlock mutex lock
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e  getPairedTpmsList(tpmsPairedInfo_t * data)
{
    FUNC_ENTRY

    uint8_t pairedTpms = 0, loopCnt;

    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //lock mutex
    pthread_mutex_lock(&g_lock);

    for(loopCnt = 0; ((loopCnt < MAX_TPMS) && \
                      (g_gatewayConfig.vehicleDef_s.tpmsConfig[loopCnt].wheelID != 0)); loopCnt++) {
        if(g_gatewayConfig.vehicleDef_s.tpmsConfig[loopCnt].sensorID != 0) {
            data->tpmsInfo[pairedTpms].sensorID = \
                                                  g_gatewayConfig.vehicleDef_s.tpmsConfig[loopCnt].sensorID;
            strncpy(data->tpmsInfo[pairedTpms].sensorPN, g_gatewayConfig.vehicleDef_s.tpmsConfig[loopCnt].sensorPN, 
                                                  MAX_LEN_SENSOR_PART_NO);
            strncpy(data->tpmsInfo[pairedTpms].sensorModel, g_gatewayConfig.vehicleDef_s.tpmsConfig[loopCnt].sensorModel, 
                                                  MAX_LEN_SENSOR_MODEL);
            data->tpmsInfo[pairedTpms].isHaloEnable = \
                    g_gatewayConfig.vehicleDef_s.tpmsConfig[loopCnt].wheelInfo.isHaloEnable;
            data->tpmsInfo[pairedTpms].attachedType = \
                    g_gatewayConfig.vehicleDef_s.tpmsConfig[loopCnt].wheelInfo.attachedType;
            data->tpmsInfo[pairedTpms].tyrePos = \
                                                 g_gatewayConfig.vehicleDef_s.tpmsConfig[loopCnt].wheelInfo.tyrePos;
            data->tpmsInfo[pairedTpms].vehicleSide = \
                    g_gatewayConfig.vehicleDef_s.tpmsConfig[loopCnt].wheelInfo.vehicleSide;
            data->tpmsInfo[pairedTpms].axleNum = \
                                                 g_gatewayConfig.vehicleDef_s.tpmsConfig[loopCnt].wheelInfo.axleNum;

            pairedTpms ++;
        }
    }
    data->pairedNumber = pairedTpms;
    if(pairedTpms == 0) {
        CAB_DBG(CAB_GW_ERR, "No TPMS paired");
        //unlock mutex
        pthread_mutex_unlock(&g_lock);
        return CONFIG_NO_TPMS_PAIRED;
    }

    //unlock mutex
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (It is used to get the tyre details for given tyre position)
 *
 *@param[IN] None
 *@param[OUT] tyreInfo_t * (Pointer to structure of tyre detail)
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e getTyreDetail(tyreInfo_t * data)
{
    FUNC_ENTRY
    uint8_t loopCnt=0;
    bool tyreFound=false;

    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //lock mutex
    pthread_mutex_lock(&g_lock);

    for (loopCnt = 0; loopCnt < MAX_TYRE_NO; loopCnt++) {
        if (data->axleNum == g_gatewayConfig.tyreConfig[loopCnt].axleNum && \
            data->vehicleSide == g_gatewayConfig.tyreConfig[loopCnt].vehicleSide && \
            data->tyrePos == g_gatewayConfig.tyreConfig[loopCnt].tyrePos) {
            memcpy(data, &g_gatewayConfig.tyreConfig[loopCnt], sizeof(tyreInfo_t));
            tyreFound = true;
            break;
        }
    }

    if(!tyreFound) {
        CAB_DBG(CAB_GW_ERR, "Invalid Tyre position.");
        //unlock mutex
        pthread_mutex_unlock(&g_lock);
        return CONFIG_NO_TREAD_DEPTH_HISTORY_AVAILABLE;
    }

    //unlock mutex
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (It is used to get the tread depth history for given tyre position)
 *
 *@param[IN] None
 *@param[OUT] configuredTreadRecordInfo_t * (Pointer to structure of tread depth history)
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e getTreadDepthHistory(configuredTreadRecordInfo_t * data)
{
    FUNC_ENTRY

    uint8_t availableRecords = 0, loopCnt;

    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //lock mutex
    pthread_mutex_lock(&g_lock);

    for(loopCnt = 0; loopCnt < MAX_TYRE_NO; loopCnt++) {
        if (data->tyrePosInfo.axleNum == g_gatewayConfig.tyreConfig[loopCnt].axleNum && \
            data->tyrePosInfo.vehicleSide == g_gatewayConfig.tyreConfig[loopCnt].vehicleSide && \
            data->tyrePosInfo.tyrePos == g_gatewayConfig.tyreConfig[loopCnt].tyrePos) {
            for(uint8_t i = 0; i < MAX_TREAD_DEPTH_RECORD; i++) {
                if (g_gatewayConfig.tyreConfig[loopCnt].treadDepthHistory[i].recordTime != 0) {
                    memcpy(&(data->treadDepthInfo[i]), \
                        &g_gatewayConfig.tyreConfig[loopCnt].treadDepthHistory[i], sizeof(treadDepthInfo_t));
                    availableRecords++;
                }
            }
            break;
        }
    }
    data->numberOfRecords = availableRecords;
    if(availableRecords == 0) {
        CAB_DBG(CAB_GW_ERR, "No Tread depth history records available.");
        //unlock mutex
        pthread_mutex_unlock(&g_lock);
        return CONFIG_NO_TREAD_DEPTH_HISTORY_AVAILABLE;
    }

    //unlock mutex
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (It is used to get the list of attached HALO SN details)
 *
 *@param[IN] None
 *@param[OUT] attachedHaloSNInfo_t * (Pointer to structure of attached HALO SNs)
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e getAttachedHaloSNList(attachedHaloSNInfo_t * data)
{

    FUNC_ENTRY

    uint8_t attachedHaloSN = 0, loopCnt;

    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //lock mutex
    pthread_mutex_lock(&g_lock);

    for(loopCnt = 0; loopCnt < MAX_HALO_SN; loopCnt++) {
        if(strncmp(g_gatewayConfig.haloSNConfig[loopCnt].serialNo, DEFAULT_HALO_SN, MAX_LEN_HALO_SN) != 0) {
            strncpy(data->haloSNInfo[attachedHaloSN].serialNo, \
                g_gatewayConfig.haloSNConfig[loopCnt].serialNo, MAX_LEN_HALO_SN);
            data->haloSNInfo[attachedHaloSN].vehicleSide = g_gatewayConfig.haloSNConfig[loopCnt].vehicleSide;
            data->haloSNInfo[attachedHaloSN].axleNum = g_gatewayConfig.haloSNConfig[loopCnt].axleNum;

            attachedHaloSN ++;
        }
    }
    data->numberOfAttachedHaloSN = attachedHaloSN;
    if(attachedHaloSN == 0) {
        CAB_DBG(CAB_GW_ERR, "No HALO SN attached.");
        //unlock mutex
        pthread_mutex_unlock(&g_lock);
        return CONFIG_NO_HALO_SN_ATTACHED;
    }

    //unlock mutex
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}
