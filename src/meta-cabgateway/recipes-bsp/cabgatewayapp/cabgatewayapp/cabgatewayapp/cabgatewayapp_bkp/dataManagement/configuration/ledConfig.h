/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file ledConfig.h
 * @brief (This file contains all LED related parameters like structures,enums, and macros)
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * July/18/2018, VT , First Draft
***************************************************************/

#ifndef _LED_CONFIG_H_
#define _LED_CONFIG_H_

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include <stdint.h>

/****************************************
 ************* DEFINES ******************
 ****************************************/
#define DEFAULT_LED_TOTAL_TIME              (0)                   //total time in ms
#define DEFAULT_LED_ON_TIME                 (0)                   //on time in ms
#define DEFAULT_LED_OFF_TIME                (0)                   //off time    in ms

#define NORMAL_TYPE_COUNT                   (0)                   //normal type count
#define ALERT_TYPE_COUNT                    (0)                   //alert type count
#define ERROR_TYPE_COUNT                    (0)                   //error type count
#define LOW_BATTERY_TYPE_COUNT              (0)                   //low battery type count
#define LOW_TEMP_TYPE_COUNT                 (0)                   //low temp type count
#define HIGH_TEMP_TYPE_COUNT                (0)                   //high temp type count
#define GATEWAY_PAIRING_TYPE_COUNT          (0)                   //gateway pairing type count
#define GATEWAY_PAIRED_TYPE_COUNT           (0)                   //gateway paired type count
#define LTE_CONNECTING_TYPE_COUNT           (0)                   //LTE connecting type count
#define LTE_CONNECTED_TYPE_COUNT            (0)                   //LTE connected type count
#define NO_LTE_CONNECTIVITY_TYPE_COUNT      (0)                   //no LTE connectivity type count
#define TPMS_PAIRING_TYPE_COUNT             (0)                   //TPMS pairing type count
#define TPMS_PAIRED_TYPE_COUNT              (0)                   //TPMS paired type count
#define OTA_PROCESSING_TYPE_COUT            (0)                   //OTA Processing type count

/****************************************
 ************* ENUMS ********************
 ****************************************/
/*This enum is defines the different type of LED ids*/
typedef enum ledID {                //Led ids used for indication
    RED = 0,
    BLUE = 1,
    MAX_LED_ID
} ledID_e;

/*This enum is defines the different type of system status for user indication*/
typedef enum sysStatusType {    //System status types
    NORMAL = 0,                 //Normal condition
    ALERT,                      //Alert condition
    ERROR,                      //Error indication
    LOW_BATTERY,                //Low battery indicator
    LOW_TEMP,                   //Low temp indicator
    HIGH_TEMP,                  //High temp indicator
    GATEWAY_PAIRING,            //Gateway pairing indication
    GATEWAY_PAIRED,             //Gateway paired indication
    LTE_CONNECTING,             //LTE connection indication
    LTE_CONNECTED,              //LTE connection established
    NO_LTE_CONNECTIVITY,        //NO LTE connectivity indication
    TPMS_PAIRING,               //TPMS pairing
    TPMS_PAIRED,                //TPMS paired
    SELF_DIAG_PATTERN,          // Self Diag test pattern.
    OTA_PROCESS_PATTERN,        // OTA Processing.
    MAX_SYS_STATUS_TYPE         //Max System status type
} sysStatusType_e;

/****************************************
 ************ STRUCTURES ****************
 ****************************************/
/*This structure is used to define LED pattern*/
/* If onTime and offTime both are not 0, then need to flash LED as per on/off time
 * If onTime is 0, then LED is always off
 * If offTime is 0, then LED is always on
 * if onTime and offTime both are 0, then LED is not part of the pattern, disable it.
 *
 * If totalCount is 0, then hold the pattern until changed
 * If totalCount is not 0, the totalTime takes effect and pattern is
 *      hold till the totalTime, and flash/not as per the on/off time value.
 * */
typedef struct ledPattern {
    uint16_t totalCount;           //Total no of count to blink the led
    uint16_t totalTime;            //total amount of time to handle the led in seconds
    uint16_t onTime;               //On time of led in ms
    uint16_t offTime;              //off time of led in ms
} ledPattern_t;

/****************************************
 ******** GLOBAL VARIABLES **************
 ****************************************/

/****************************************
 ******** EXTERN VARIABLES **************
 ****************************************/

/****************************************
 ************* CALLBACKS ****************
 ****************************************/

/****************************************
 ********* FUNCTION PROTOTYPES **********
 ****************************************/

#endif      /*_LED_CONFIG_H_*/
