/************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file dataStorage.h
 * @brief (Header file for Data storage module.)
 *
 * @Author     - VT
 *************************************************************************************************
 * History
 *
 * July/24/2018, VT , First Draft
 *************************************************************************************************/

#ifndef _DATA_STORAGE_H_
#define _DATA_STORAGE_H_

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <fcntl.h>
#include <time.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include "commonData.h"
#include "error.h"
#include "debug.h"
#include "diag.h"

/****************************************
 ************* DEFINES ******************
 ****************************************/
/* File paths for each data file */
#define FILE_PATH_TPMS_DATA         "%s/tpmsData.csv"
#define FILE_PATH_VEHICLE_INFO      "%s/vehicleInfo.csv"
#define FILE_PATH_SYS_OTHER_DATA    "%s/sysOtherData.csv"
#define ERROR_JSON_FILE             "%s/error.json"
/* File path for event summary data */
#define FILE_PATH_EVENT_SUMM        "%s/eventSummary.txt"
#define RDIAG_ZIP_STATUS_FILE       "/home/root/dataRDiag-zip-status-file.txt"
#define TPMS_ZIP_STATUS_FILE        "/home/root/tpms-zip-status-file.txt"
#define CFG_ZIP_STATUS_FILE        "/home/root/cfg-zip-status-file.txt"

/* Directory path to locate all data file */
#define DIR_PATH_DATA_STORAGE       "/media/userdata/dataStorage"
#define DIR_PATH_DATA_TEMP          "/media/userdata/dataTemp"
#define DIR_PATH_DATA_RDIAG         "/media/userdata/dataRDiag"

/*Max directory name length*/
#define MAX_DIRNAME_LEN		    (200)
#define MAX_FILENAME_LEN	    (300)

/* Zip file name format */
#define ZIP_FILE_NAME               "R%02d_%02d_%02d_%02d__CG_%ld.zip"
#define RDIAG_ZIP_FILE_NAME         "R%02d_%02d_%02d_%02d__CG_$(date +%%s).zip"

/* Max length of filename and command */
#define MAX_LEN_COMMAND             (500)

/****************************************
 ************* ENUMS ********************
 ****************************************/
typedef enum {
    JSON_DATA = 0,
    CSV_DATA = 1,
    CFG_DATA = 2,
    MAX_ZIP_TYPE
} zipType_e;

/****************************************
 ************ STRUCTURES ****************
 ****************************************/
/* This stucture is used to define summary data for different sensors */
typedef struct summary {
    uint32_t tpmsDataCnt;
    uint32_t sysOtherDataCnt;
    uint32_t vehicleInfoCnt;
    uint16_t zipFileCnt;
} summary_t;

/****************************************
 ******** GLOBAL VARIABLES **************
 ****************************************/
summary_t g_eventSumm;      //event summary variable which contains information for all sensors

/****************************************
 ******** EXTERN VARIABLES **************
 ****************************************/

/****************************************
 ************* CALLBACKS ****************
 ****************************************/

/****************************************
 ********* FUNCTION PROTOTYPES **********
 ****************************************/
/******************************************************************
 *@brief  (This API is used to initialize data storage module)
 *
 *@param[IN] uint16_t (Indicates maximum number of storage package file.)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e initDataStorageModule(uint16_t noOfPackage, char *swVersion, char *inputDirName);


/******************************************************************
 *@brief  (This API is used to Deinitialize data storage module)
 *
 *@param[IN] void  (No arguments passed)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e deInitDataStorageModule(void);

/******************************************************************
 *@brief  (This API is used to set the event data to specific storage file as per device type)
 *
 *@param[IN] void * (Indicates pointer to structure of device data)
 *@param[IN] sysDataType_e  (Indicates device type)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e eventSet(void * , sysDataType_e);

/******************************************************************
 *@brief  (This API is used to create a file package to send data on cloud)
 *
 *@param[IN] time_t (Indicates epoch time to give unique name to zip file)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e createFilePackage(time_t packageTime, zipType_e zipType);

/******************************************************************
 *@brief  (This API is used to get the path of package file to access that file)
 *
 *@param[IN] None
 *@param[OUT] void * (Indicates pointer to get path of package file)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e getPackageFile(void *);

/******************************************************************
 *@brief  (This API is used to delete all package files from dataStorage directory)
 *
 *@param[IN] None
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e deletePackageFile(void *);

/******************************************************************
 *@brief  (This API is used to get the zip file count from dataStorage directory)
 *
 *@param[IN] None
 *@param[OUT] None
 *
 *@return unit16_t (It returns the current zip file count)
 *********************************************************************/
returnCode_e getZipCount(void *);

#endif      /*_DATA_STORAGE_H_*/
