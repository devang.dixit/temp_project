/********************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file dataStorage.c
 * @brief (This file contains APIs defination of data storage module.)
 *
 * @Author     - VT
 **********************************************************************************************
 * History
 *
 * July/24/2018, VT , First Draft
 **********************************************************************************************/

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include "dataStorage.h"
#include "sysLogStorage.h"
#include "cloud.h"

/****************************************
 ******** STATIC VARIABLES **************
 ****************************************/
static pthread_mutex_t g_lock = PTHREAD_MUTEX_INITIALIZER;  //Initialize mutex variable
static uint16_t g_zipLimit;
static int ver1 = 0, ver2 = 0, ver3 = 0, ver4 = 0;
static char outDirName[MAX_DIRNAME_LEN] = {'\0'};
static char tpmsCSVFileName[MAX_FILENAME_LEN] = {'\0'};
static char vehCSVFileName[MAX_FILENAME_LEN] = {'\0'};
static char sysCSVFileName[MAX_FILENAME_LEN] = {'\0'};
static char errorJSONFileName[MAX_FILENAME_LEN] = {'\0'};
static char eventSumFileName[MAX_FILENAME_LEN] = {'\0'};


/****************************************
 ******** FUNCTION DEFINITIONS **********
 ****************************************/

/******************************************************************
 *@brief  (This API is used to read event summary file which contains all sensors summary)
 *
 *@param[IN] summary_t* (Pointer to structure of event summary data)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e readEventSummary(summary_t * eventSumm)
{
    FUNC_ENTRY

    int summaryFile;                                //file descriptor
    uint32_t ret;

    /*Input argument validation*/
    if(eventSumm == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //opening event summary file for read
    summaryFile = open(eventSumFileName, O_RDONLY, 0644);
    if(summaryFile == -1) {
        CAB_DBG(CAB_GW_ERR, "Failed to open %s file", eventSumFileName);
        return FILE_OPEN_FAIL;
    }

    //reading event summary structure data from file
    ret = read(summaryFile, eventSumm, sizeof(summary_t));
    if(ret == -1) {
        CAB_DBG(CAB_GW_ERR, "Failed to read %s file", eventSumFileName);
        close(summaryFile);
        return FILE_READ_ERROR;
    }
    //close event summary file
    close(summaryFile);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (This API is used to write event summary file which contains all sensors summary)
 *
 *@param[IN] summary_t* (Pointer to structure of event summary data)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e writeEventSummary(summary_t * eventSumm)
{
    FUNC_ENTRY

    int outFile;                               //file descriptor
    uint32_t ret;

    /*Input argument validation*/
    if(eventSumm == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //opening event summary file for write
    outFile = open(eventSumFileName, O_WRONLY | O_CREAT, 0644);
    if(outFile == -1) {
        CAB_DBG(CAB_GW_ERR, "Failed to open %s file", eventSumFileName);
        return FILE_OPEN_FAIL;
    }

    CAB_DBG(CAB_GW_DEBUG, "Zip file cnt :%d", g_eventSumm.zipFileCnt);
    //writing event summery structure data to file
    ret = write(outFile, eventSumm, sizeof(summary_t));
    if(ret == -1) {
        CAB_DBG(CAB_GW_ERR, "Failed to write %s file", eventSumFileName);
        close(outFile);
        return FILE_WRITE_ERROR;
    }

    fsync(outFile);
    //close event summary file
    close(outFile);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/*
 * Check if a file exist using stat() function
 * return 1 if the file exist otherwise return 0
 */
static uint8_t constfileexists(const char* filename)
{
    FUNC_ENTRY
    struct stat buffer;
    int exist = stat(filename,&buffer);
    if (exist == 0) {
    	FUNC_EXIT
        return 1;
    } else {
	FUNC_EXIT
        return 0;
    }
}

/******************************************************************
 *@brief  (This API is used to check if csv present)
 *
 *@param[IN]  None
 *
 *@return returnCode_e GEN_SUCCESS -> data avail. DATA_INVALID-> no data
 *********************************************************************/
static returnCode_e checkDataPresence(zipType_e zipType)
{
    FUNC_ENTRY

    if (zipType == CSV_DATA) {
    	if (constfileexists(tpmsCSVFileName)) {
    	    CAB_DBG(CAB_GW_TRACE, "TPMS Data csv present");
    	    return GEN_SUCCESS;
    	}
    	CAB_DBG(CAB_GW_TRACE, "TPMS Data csv not present");

    	if (constfileexists(vehCSVFileName)) {
    	    CAB_DBG(CAB_GW_TRACE, "vehicle info Data csv present");
    	    return GEN_SUCCESS;
    	}

    	CAB_DBG(CAB_GW_TRACE, "Vehicle info csv not present");
    	if (constfileexists(sysCSVFileName)) {
    	    CAB_DBG(CAB_GW_TRACE, "sys other Data csv present");
    	    return GEN_SUCCESS;
    	}
    	CAB_DBG(CAB_GW_TRACE, "sys other Data csv not present");

    	if (constfileexists(FILE_PATH_DIAG)) {
    	    CAB_DBG(CAB_GW_TRACE, "Diag Data csv present");
    	    return GEN_SUCCESS;
    	}
    	CAB_DBG(CAB_GW_TRACE, "Diag Data csv not present");
    } else if(zipType == JSON_DATA) {
	if (constfileexists(errorJSONFileName)) {
	    CAB_DBG(CAB_GW_TRACE, "Error json present");
	    return GEN_SUCCESS;
	}
	CAB_DBG(CAB_GW_TRACE, "Error json not present");
    } else if(zipType == CFG_DATA) {
    	if (constfileexists("/media/userdata/configStorage/configParam.cfg")) {
    	    CAB_DBG(CAB_GW_TRACE, "Config File present");
    	    return GEN_SUCCESS;
        }
    }

    CAB_DBG(CAB_GW_TRACE, "No data available");
    FUNC_EXIT
    return DATA_INVALID;
}

/******************************************************************
 *@brief  (This API is used to find the oldest zip file name)
 *
 *@param[IN]  None
 *@param[OUT] void * (Indicates pointer to string)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e countNoOfFile(char * dirName, char *checkExt, uint32_t *fileCnt)
{
    FUNC_ENTRY

    struct dirent *dp;                  // Pointer for directory entry
    char ext[5];                        //to get extention of file
    uint8_t len, loopCnt;

    if (dirName == NULL || checkExt == NULL || fileCnt == NULL) {
	    CAB_DBG(CAB_GW_ERR, "Invalid argument in count No of files");
	    return GEN_API_FAIL;
    }

    // opendir() returns a pointer of DIR type.
    DIR *dr = opendir(dirName);

    if(dr == NULL) {  // opendir returns NULL if couldn't open directory
        CAB_DBG(CAB_GW_ERR, "Failed to open directory");
        return DIR_OPEN_FAIL;
    }

    *fileCnt = 0;
    // for readdir()
    while((dp = readdir(dr)) != NULL) {
        if(strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0)
            continue;
        len = strlen(dp->d_name);
        len -= 4;
        for(loopCnt = 0; dp->d_name[len] != '\0'; loopCnt++) {
            ext[loopCnt] = dp->d_name[len];
            len++;
        }
        if(strncmp(ext, checkExt, 4) == 0) {
	    *fileCnt = *fileCnt + 1;
        }
    }

    closedir(dr);
    return GEN_SUCCESS;
}
/******************************************************************
 *@brief  (This API is used to find the oldest zip file name)
 *
 *@param[IN]  None
 *@param[OUT] void * (Indicates pointer to string)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e findOldestFile(void * fileName)
{
    FUNC_ENTRY

    struct dirent *dp;                  // Pointer for directory entry
    time_t oldesttime = 0;              //timestamp for oldest file
    struct stat statbuf;                //to get stat of file
    char buffer[MAX_LEN_FILENAME];      //to temporary store file name
    char oldestFile[MAX_LEN_FILENAME];	//to store oldest file name
    char ext[5];                        //to get extention of file
    uint8_t len, loopCnt;
    fileStorageInfo_t tempFileStorageInfo = *(fileStorageInfo_t*)fileName;

    if (outDirName[0] == NULL) {
	    CAB_DBG(CAB_GW_ERR, "Output direcotry is not set");
	    return GEN_API_FAIL;
    }

    // opendir() returns a pointer of DIR type.
    DIR *dr = opendir(outDirName);

    if(dr == NULL) {  // opendir returns NULL if couldn't open directory
        CAB_DBG(CAB_GW_ERR, "Failed to open directory");
        return DIR_OPEN_FAIL;
    }

    // for readdir()
    while((dp = readdir(dr)) != NULL) {
        if(strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0)
            continue;
        len = strlen(dp->d_name);
        len -= 4;
        for(loopCnt = 0; dp->d_name[len] != '\0'; loopCnt++) {
            ext[loopCnt] = dp->d_name[len];
            len++;
        }
        if(strncmp(ext, ".zip", 4) == 0) {
            sprintf(buffer, "%s/%s", outDirName, dp->d_name);
            lstat(buffer, &statbuf);
            if((statbuf.st_mtime < oldesttime) | (oldesttime == 0)) {
                sprintf(oldestFile, "%s", dp->d_name);
                oldesttime = statbuf.st_mtime;
            }
        }
    }

    sprintf(tempFileStorageInfo.filename, "%s", oldestFile);
    sprintf(tempFileStorageInfo.storagePath, "%s", outDirName);
    memcpy(fileName, &tempFileStorageInfo, sizeof(fileStorageInfo_t));
    CAB_DBG(CAB_GW_DEBUG, "Temp file name : %s", (char *)fileName);

    closedir(dr);
    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (This API is used to set tpms event data in tpms event storage file)
 *
 *@param[IN] void * (Indicates pointer to structure of tpms data)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e tpmsEventSet(void * data)
{
    FUNC_ENTRY

    uint32_t status;
    FILE * outFile;                                 //file descriptor
    tpmsEvent_t tempEvent = *(tpmsEvent_t*) data;   //to get temporary instance to add id
    static time_t gpsLastReportTime = 0;
    uint8_t gpsReportReq = 0;
    static uint32_t prevSensorID = 0;

    //Validate input argument
    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error");
        return GEN_NULL_POINTING_ERROR;
    }

    //Lock to protect event data
    pthread_mutex_lock(&g_lock);

    //Add event id in data comes from M&T module
    tempEvent.eventID = g_eventSumm.tpmsDataCnt + 1;

    /*  Writing TPMS event data to TPMS event file  */

    /*Check if File Already exists ? If not, last available file was packed
      into ZIP so new ZIP is going to be created. GPS data reproting interval
      should be reset in such case.*/
    if( access(tpmsCSVFileName, F_OK) != 0 ) {
	gpsLastReportTime = 0;/*This forces GPS data reporting for very first TPMS data*/
    }
    
    //Open event file. If not exist, create it.
    outFile = fopen(tpmsCSVFileName, "a");
    if(outFile == NULL) {
        CAB_DBG(CAB_GW_ERR, "Failed to read %s file", tpmsCSVFileName);
        //Unlock to protect event data
        pthread_mutex_unlock(&g_lock);
        return FILE_OPEN_FAIL;
    }
    // write entire event structure to event data file
    status = fprintf(outFile, "%d,%ld,%X,%.1f,%.1f,%.1f,%d,%d,%d,%d,%d,%d,%d",  \
                     tempEvent.eventID, tempEvent.tpmsDMData.time,   \
                     tempEvent.tpmsDMData.tpmsData.sensorID, tempEvent.tpmsDMData.tpmsData.pressure, \
                     tempEvent.tpmsDMData.tpmsData.temperature,  \
                     tempEvent.tpmsDMData.tpmsData.battery, tempEvent.tpmsDMData.tpmsData.rssi,  \
                     tempEvent.tpmsDMData.wheelInfo.isHaloEnable, \
                     tempEvent.tpmsDMData.wheelInfo.attachedType, \
                     tempEvent.tpmsDMData.wheelInfo.tyrePos,     \
                     tempEvent.tpmsDMData.wheelInfo.vehicleSide, \
                     tempEvent.tpmsDMData.wheelInfo.axleNum, \
                     tempEvent.alert);
    if(prevSensorID != tempEvent.tpmsDMData.tpmsData.sensorID) {
    //if(gpsReportReq) {
	    status |= fprintf(outFile, ",%f,%f,%f,%.1f",  \
			      tempEvent.tpmsDMData.gpsData.latitude, \
			      tempEvent.tpmsDMData.gpsData.longitude, \
			      tempEvent.tpmsDMData.gpsData.altitude, tempEvent.tpmsDMData.gpsData.speed);
            prevSensorID = tempEvent.tpmsDMData.tpmsData.sensorID;
    } else {
            status |= fprintf(outFile, ",,,,");
    }
    status |= fprintf(outFile, ",%d", tempEvent.tpmsDMData.tpmsData.status);
    status |= fprintf(outFile, "\n");

    if(status < 0) {
        CAB_DBG(CAB_GW_ERR, "Failed to write %s file", tpmsCSVFileName);
        fclose(outFile);
        //Unlock to protect event data
        pthread_mutex_unlock(&g_lock);
        return FILE_WRITE_ERROR;
    }
    /* Write buffered data to the memory right away */
    fflush(outFile);
    fsync(fileno(outFile));
    //Close event file
    fclose(outFile);

    /*  Updating event summary file   */

    //Increase TPMS total event count
    g_eventSumm.tpmsDataCnt ++;

    //Write updated value in summary file
    status = writeEventSummary(&g_eventSumm);
    if(GEN_SUCCESS != status) {
        CAB_DBG(CAB_GW_ERR, "Failed to update %s file", eventSumFileName);
        //Unlock to protect event data
        pthread_mutex_unlock(&g_lock);
        return FILE_WRITE_ERROR;
    }
    //Unlock to protect event data
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (This API is used to set system other event data in system other event storage file)
 *
 *@param[IN] void * (Indicates pointer to structure of system other data)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e sysOtherEventSet(void * data)
{
	FUNC_ENTRY
		
	uint32_t status;
	FILE * outFile;                                         //file descriptor
	sysOtherData_t tempEvent = *(sysOtherData_t*)data;      //to get temporary instance to add id

    //Validate input argument
    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error");
        return GEN_NULL_POINTING_ERROR;
    }

    //Lock to protect event data
    pthread_mutex_lock(&g_lock);

    //Add event id in data comes from M&T module
    tempEvent.eventID = g_eventSumm.sysOtherDataCnt + 1;

    /*  Writing system other event data to system other event file  */

    //Open event file. If not exist, create it.
    outFile = fopen(sysCSVFileName, "a");
    if(outFile == NULL) {
        CAB_DBG(CAB_GW_ERR, "Failed to open %s file", sysCSVFileName);
        //Unlock to protect event data
        pthread_mutex_unlock(&g_lock);
        return FILE_OPEN_FAIL;
    }

    // write entire event structure to event data file
    if(tempEvent.type == BATTERY) {
        status = fprintf(outFile, "%d,%d,%ld,%d,%d\n", tempEvent.eventID, tempEvent.type, \
                         tempEvent.time, tempEvent.batteryData.device, tempEvent.batteryData.alert);
    } else if(tempEvent.type == DIAGNOSTIC_DATA) {
        status = fprintf(outFile, "%d,%d,%ld,%f,%f,%f,%f,%d,%d,%ld,%d,%s,%d,%d,%ld,%d,%d,%d, %d, %ld\n", \
                         tempEvent.eventID, tempEvent.type, \
                         tempEvent.time, \
                         tempEvent.diagData.gpsDiagData.gpsData.latitude,    \
                         tempEvent.diagData.gpsDiagData.gpsData.longitude,   \
                         tempEvent.diagData.gpsDiagData.gpsData.altitude,    \
                         tempEvent.diagData.gpsDiagData.gpsData.speed,   \
                         tempEvent.diagData.gpsDiagData.noOfSatInView,   \
                         tempEvent.diagData.gpsDiagData.fixQuality,  \
                         tempEvent.diagData.lteDiagData.timestamp,  \
                         tempEvent.diagData.lteDiagData.signalStrength,    \
                         tempEvent.diagData.lteDiagData.operatorName,    \
                         tempEvent.diagData.lteDiagData.signalQuality, \
                         tempEvent.diagData.rtcPwrAccDiagData.accelTemperature, \
                         tempEvent.diagData.rtcPwrAccDiagData.powerSWTime, \
                         tempEvent.diagData.rtcPwrAccDiagData.coinCellStatusBool, \
                         tempEvent.diagData.rtcPwrAccDiagData.isInternetConnected, \
			 tempEvent.diagData.rtcPwrAccDiagData.mpuTemperature, \
                         tempEvent.diagData.rtcPwrAccDiagData.PwrOffFlg, \
                         tempEvent.diagData.rtcPwrAccDiagData.PwrOffTmStamp
			 );
    }
    if(status < 0) {
        CAB_DBG(CAB_GW_ERR, "Failed to write %s file", sysCSVFileName);
        fclose(outFile);
        //Unlock to protect event data
        pthread_mutex_unlock(&g_lock);
        return FILE_WRITE_ERROR;
    }
    /* Write buffered data to the memory right away */
    fflush(outFile);
    fsync(fileno(outFile));
    //Close event file
    fclose(outFile);

    /*  Updating event summary file   */

    //Increase TPMS total event count
    g_eventSumm.sysOtherDataCnt ++;

    //Write updated value in summary file
    status = writeEventSummary(&g_eventSumm);
    if(GEN_SUCCESS != status) {
        CAB_DBG(CAB_GW_ERR, "Failed to update %s file", eventSumFileName);
        //Unlock to protect event data
        pthread_mutex_unlock(&g_lock);
        return FILE_WRITE_ERROR;
    }
    //Unlock to protect event data
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (This API is used to set Changed configuration event data in configuration event file)
 *
 *@param[IN] void * (Indicates pointer to structure of changed configuration data)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e configEventSet(void * data)
{
    FUNC_ENTRY

    uint32_t status;
    uint8_t axleCount,axle;
    FILE * outFile;                                               //file descriptor
    changeConfigParam_t tempEvent = *(changeConfigParam_t*)data;  //to get temp instance to add id

    //Validate input argument
    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error");
        return GEN_NULL_POINTING_ERROR;
    }

    //Lock to protect event data
    pthread_mutex_lock(&g_lock);

    //Add event id in data comes from M&T module
    tempEvent.eventID = g_eventSumm.vehicleInfoCnt + 1;

    /*  Writing Changed configuration event data to Changed configuration event file  */

    //Open event file. If not exist, create it.
    outFile = fopen(vehCSVFileName, "a");
    if(outFile == NULL) {
        CAB_DBG(CAB_GW_ERR, "Failed to open %s file", vehCSVFileName);
        //Unlock to protect event data
        pthread_mutex_unlock(&g_lock);
        return FILE_OPEN_FAIL;
    }

    // write entire event structure to event data file
    switch(tempEvent.type) {
        case VEHICLE_NUMBER :
            status = fprintf(outFile, "%d,%d,%ld,%s\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.vehicleNum);
            break;
        case CUSTOMER_ID :
            status = fprintf(outFile, "%d,%d,%ld,%s\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.customerID);
            break;
        case VEHICLE_TYPE :
            status = fprintf(outFile, "%d,%d,%ld,%d\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.vehicleType);
            break;
        case FLEET_NAME :
            status = fprintf(outFile, "%d,%d,%ld,%s\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.fleetName);
            break;
        case FLEET_ID :
            status = fprintf(outFile, "%d,%d,%ld,%s\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.fleetID);
            break;
        case VEHICLE_MAKE :
            status = fprintf(outFile, "%d,%d,%ld,%s\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.vehicleMake);
            break;
        case VEHICLE_YEAR :
            status = fprintf(outFile, "%d,%d,%ld,%d\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.vehicleYear);
            break;
        case TYRE_MAKE :
            status = fprintf(outFile, "%d,%d,%ld,%s\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.tyreMake);
            break;
        case TYRE_SIZE :
            status = fprintf(outFile, "%d,%d,%ld,%s\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.tyreSize);
            break;
        case SENSOR_TYPE :
            status = fprintf(outFile, "%d,%d,%ld,%d\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.sensorType);
            break;
        case ADD_SENSOR_ID :
        case REMOVE_SENSOR_ID :
            status = fprintf(outFile, "%d,%d,%ld,%X,%d,%d,%d,%d,%d\n", tempEvent.eventID, \
                             tempEvent.type, tempEvent.time, tempEvent.tpmsSensorID.sensorID, \
                             tempEvent.tpmsSensorID.wheelInfo.isHaloEnable, \
                             tempEvent.tpmsSensorID.wheelInfo.attachedType,  \
                             tempEvent.tpmsSensorID.wheelInfo.tyrePos,   \
                             tempEvent.tpmsSensorID.wheelInfo.vehicleSide, \
                             tempEvent.tpmsSensorID.wheelInfo.axleNum);
            break;
        case TPMS_THRESHOLD :
	    axleCount = tempEvent.targetPressInfo.numberOfAxle;
            status = fprintf(outFile, "%d,%d,%ld,%d",  \
                             tempEvent.eventID, tempEvent.type, \
			     tempEvent.time,axleCount);
	    for(axle=0;axle<axleCount;axle++)
		fprintf(outFile,",%d",tempEvent.targetPressInfo.tpmsThreshold[axle]);
	    fprintf(outFile,"\n");
            break;
        case LOW_BATTERY_THRESHOLD :
            status = fprintf(outFile, "%d,%d,%ld,%d\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.lowBatteryThreshold);
            break;
        case CLOUD_URI :
            status = fprintf(outFile, "%d,%d,%ld,%s\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.cloudURI);
            break;
        default :
            CAB_DBG(CAB_GW_ERR, "Invalid option");
            //Unlock to protect event data
            pthread_mutex_unlock(&g_lock);
            return GEN_INVALID_TYPE;
    }
    if(status < 0) {
        CAB_DBG(CAB_GW_ERR, "Failed to write %s file", vehCSVFileName);
        fclose(outFile);
        //Unlock to protect event data
        pthread_mutex_unlock(&g_lock);
        return FILE_WRITE_ERROR;
    }
    //Close event file
    fclose(outFile);

    /*  Updating event summary file   */

    //Increase TPMS total event count
    g_eventSumm.vehicleInfoCnt ++;

    //Write updated value in summary file
    status = writeEventSummary(&g_eventSumm);
    if(GEN_SUCCESS != status) {
        CAB_DBG(CAB_GW_ERR, "Failed to update %s file", eventSumFileName);
        //Unlock to protect event data
        pthread_mutex_unlock(&g_lock);
        return FILE_WRITE_ERROR;
    }
    //Unlock to protect event data
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e initDataStorageModule(uint16_t noOfPackage, char *swVersion, char *inputDirName)
{
    FUNC_ENTRY

    uint8_t status = GEN_SUCCESS;
    uint32_t zipIndex = 0;
    struct stat st = {0};
    uint32_t zipFileCnt = 0;
    char checkExt[] = ".zip";
    fileStorageInfo_t tempFileStorageInfo;
  
    if (inputDirName == NULL) {
	    CAB_DBG(CAB_GW_ERR, "Init storage failed due to invalid arg");
	    return MODULE_INIT_FAIL;
    }
    if (strcpy(outDirName, inputDirName) == NULL) {
	    CAB_DBG(CAB_GW_ERR, "strcpy failed in init data storage module");
	    return MODULE_INIT_FAIL;
    }
    if (sprintf(tpmsCSVFileName, FILE_PATH_TPMS_DATA, outDirName) <= 0) {
	    CAB_DBG(CAB_GW_ERR, "TPMS filename set failed");
	    return MODULE_INIT_FAIL;
    }
    if (sprintf(vehCSVFileName, FILE_PATH_VEHICLE_INFO, outDirName) <= 0) {
	    CAB_DBG(CAB_GW_ERR, "veh info filename set failed");
	    return MODULE_INIT_FAIL;
    }
    if (sprintf(sysCSVFileName, FILE_PATH_SYS_OTHER_DATA, outDirName) <= 0) {
	    CAB_DBG(CAB_GW_ERR, "sys other data file set failed");
	    return MODULE_INIT_FAIL;
    }
    if (sprintf(errorJSONFileName, ERROR_JSON_FILE, outDirName) <= 0) {
	    CAB_DBG(CAB_GW_ERR, "error json file set failed");
	    return MODULE_INIT_FAIL;
    }
    if (sprintf(eventSumFileName, FILE_PATH_EVENT_SUMM, outDirName) <= 0) {
	    CAB_DBG(CAB_GW_ERR, "eve summary file set failed");
	    return MODULE_INIT_FAIL;
    }

    CAB_DBG(CAB_GW_INFO, "Initializing Data storage module");


    //Initializing mutext lock
    pthread_mutex_init(&g_lock, NULL);

    //lock to protect event summary data
    pthread_mutex_lock(&g_lock);

    //Copy no of package file into global variable
    if(noOfPackage == 0 || swVersion == NULL) {
        //unlock to protect event summary data
        pthread_mutex_unlock(&g_lock);
	CAB_DBG(CAB_GW_ERR, "Invalid argument");
        return GEN_NULL_POINTING_ERROR;
    }

    sscanf(swVersion, "%d.%d.%d.%d", &ver1, &ver2, &ver3, &ver4);
    

    g_zipLimit = noOfPackage;

    //To check for the dataStorage directory and if it not exist then create it
    if(stat(outDirName, &st) == -1) {
        if((mkdir(outDirName, 0755)) == -1) {
            CAB_DBG(CAB_GW_ERR, "dataStorage directory not created");
            //unlock to protect event summary data
            pthread_mutex_unlock(&g_lock);
            return DIR_CREATE_FAIL;
        }
    }

    //If event summary file does not exist
    if(access(eventSumFileName, F_OK) == -1) {
        g_eventSumm = (summary_t) {
            .tpmsDataCnt = 0, .sysOtherDataCnt = 0, .vehicleInfoCnt = 0, .zipFileCnt = 0
        };
        status = writeEventSummary(&g_eventSumm);       //update event summary file
        if(GEN_SUCCESS != status) {
            CAB_DBG(CAB_GW_ERR, "Failed to write initial values to %s file", eventSumFileName);
        }
    } else {                                           //If event summary file exist
        status = readEventSummary(&g_eventSumm);       //read event summary file
        if(GEN_SUCCESS != status) {
            CAB_DBG(CAB_GW_ERR, "Failed to read existing values to %s file", eventSumFileName);
        }
    }

    /* correct corrupted evebt summary and clean up data dir if zip files are more than limit*/
    status = countNoOfFile(outDirName, checkExt, &zipFileCnt);
    if (GEN_SUCCESS != status) {
	    CAB_DBG(CAB_GW_ERR, "failed to get zip file count with code :%d", status);
    } else {
	if (zipFileCnt > g_zipLimit) {
		CAB_DBG(CAB_GW_ERR, "more zip than limit, cleaning extra old zip files");
		for (zipIndex = 0; zipIndex <  (zipFileCnt - g_zipLimit); zipIndex++) {
			status = findOldestFile((void*)&tempFileStorageInfo);
			if(status != GEN_SUCCESS) {
				CAB_DBG(CAB_GW_ERR, "Not getting oldest file name");
				/* unlock to protect event summary data */
				pthread_mutex_unlock(&g_lock);
				return FILE_NOT_FOUND;
			}    
			/* unlock to protect event summary data */
			pthread_mutex_unlock(&g_lock);
			status = deletePackageFile((void*)&tempFileStorageInfo);
			pthread_mutex_lock(&g_lock);
			if(status != GEN_SUCCESS) {
				CAB_DBG(CAB_GW_ERR, "Delete package error");
				/* unlock to protect event summary data */
				pthread_mutex_unlock(&g_lock);
				return FILE_DELETE_ERROR;
			}    
		}
		g_eventSumm.zipFileCnt = g_zipLimit;
	} else if (zipFileCnt != g_eventSumm.zipFileCnt) {
    	        g_eventSumm.zipFileCnt = zipFileCnt;
    	}
    	status = writeEventSummary(&g_eventSumm);
    	if (GEN_SUCCESS != status) {
    	    CAB_DBG(CAB_GW_ERR, "write event sum failed due to error code : %d", status);
    	}
    }

    //unlock to protect event summary data
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return status;
}

returnCode_e deInitDataStorageModule(void)
{
    FUNC_ENTRY

    CAB_DBG(CAB_GW_INFO, "Deinitializing Data storage module");
    /*Any ongoing file operation will not allow to take this lock untill
      it completes. Once completed, no other thread will spawn to use event
     module as all other modules must have been deinit before */
    pthread_mutex_lock(&g_lock);
    pthread_mutex_unlock(&g_lock);

    //destroying lock
    pthread_mutex_destroy(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e eventSet(void * data, sysDataType_e type)
{
    FUNC_ENTRY

    uint8_t status = GEN_SUCCESS;
    //Take decision based on device type
    switch(type) {
        case TPMS :                 //set tpms data
            status = tpmsEventSet(data);
            break;
        case SYS_OTHER_DATA :       //set system other data
            status = sysOtherEventSet(data);
            break;
        case CONFIG_PARAM :         //set configuration data
            status = configEventSetForCfgFile(data);
            break;
        default :
            CAB_DBG(CAB_GW_ERR, "Invalid option");
            return GEN_INVALID_TYPE;
    }
    FUNC_EXIT
    return status;
}

returnCode_e createFilePackage(time_t time_value, zipType_e zipType)
{
    FUNC_ENTRY

    int32_t status;
    fileStorageInfo_t tempFileStorageInfo;
    char command[MAX_LEN_COMMAND];
    uint8_t cmdStatus;

    if (checkDataPresence(zipType) != GEN_SUCCESS) {
	CAB_DBG(CAB_GW_DEBUG, "No data available, skipping packaging");
	return GEN_SUCCESS;
    }

    //lock to protect event summary data
    pthread_mutex_lock(&g_lock);

    if(g_eventSumm.zipFileCnt >= g_zipLimit) {
        status = findOldestFile((void*)&tempFileStorageInfo);
        if(status != GEN_SUCCESS) {
            CAB_DBG(CAB_GW_ERR, "Not getting oldest file name");
            //unlock to protect event summary data
            pthread_mutex_unlock(&g_lock);
            return FILE_NOT_FOUND;
        }
        //unlock to protect event summary data
        pthread_mutex_unlock(&g_lock);
        status = deletePackageFile((void*)&tempFileStorageInfo);
        pthread_mutex_lock(&g_lock);
        if(status != GEN_SUCCESS) {
	    CAB_DBG(CAB_GW_ERR, "Delete package error");
            //unlock to protect event summary data
            pthread_mutex_unlock(&g_lock);
            return FILE_DELETE_ERROR;
        }
    }

    if(zipType == CFG_DATA) {
        
    	//To create a zip file to send data on cloud
    	//File name is unique because format is CG_<epoch_time>.zip
    	sprintf(command, "cd %s; zip -m ./"ZIP_FILE_NAME" ./*.cfg 2>/dev/null 1>/dev/null; echo $? > \
            "CFG_ZIP_STATUS_FILE" ; mv ./*.zip "DIR_PATH_DATA_STORAGE"/; cd - ",  DIR_PATH_CONFIG_STORAGE, ver1, ver2, ver3, ver4, time_value);
        CAB_DBG(CAB_GW_INFO, "--------command=%s------------\r\n", command);
	status = WEXITSTATUS(system(command));
	CAB_DBG(CAB_GW_DEBUG, "WEXITSTATUS status=%d\r\n", status);
	status = readStatusFile(CFG_ZIP_STATUS_FILE, &cmdStatus);
    	if(status != GEN_SUCCESS) {
    	    CAB_DBG(CAB_GW_ERR, "Error in reading status file, return code: %d", status); 
    	    //unlock to protect event summary data
    	    pthread_mutex_unlock(&g_lock);
    	    return status;
    	}
	if( cmdStatus != GEN_SUCCESS ) {
	    CAB_DBG(CAB_GW_ERR, "Zip File Creation Failed");
    	    pthread_mutex_unlock(&g_lock);
	    return FILE_CREATE_ERROR;
	}
    	memset(command, 0, sizeof(command));
    } else if (zipType == CSV_DATA) {
    	//To create a zip file to send data on cloud
    	//File name is unique because format is CG_<epoch_time>.zip
    	sprintf(command, "cd %s; zip -m ./"ZIP_FILE_NAME" ./*.csv 2>/dev/null 1>/dev/null; echo $? > \
            "TPMS_ZIP_STATUS_FILE"",  outDirName, ver1, ver2, ver3, ver4, time_value);
	status = WEXITSTATUS(system(command));
	CAB_DBG(CAB_GW_DEBUG, "WEXITSTATUS status=%d\r\n", status);
	status = readStatusFile(TPMS_ZIP_STATUS_FILE, &cmdStatus);
    	if(status != GEN_SUCCESS) {
    	    CAB_DBG(CAB_GW_ERR, "Error in reading status file, return code: %d", status); 
    	    //unlock to protect event summary data
    	    pthread_mutex_unlock(&g_lock);
    	    return status;
    	}
	if( cmdStatus != GEN_SUCCESS ) {
	    CAB_DBG(CAB_GW_ERR, "Zip File Creation Failed");
    	    pthread_mutex_unlock(&g_lock);
	    return FILE_CREATE_ERROR;
	}
    	memset(command, 0, sizeof(command));
    } else if (zipType == JSON_DATA) {
    	//To create a zip file to send data on cloud
    	//File name is unique because format is CG_<epoch_time>.zip
        /* The inline bash command handles the situation with more than on error.json[_timestamp] files available.
            Different error.json files were created to avoid making them invalid due to more than one JSON objects
            appended.
            
            The command iterates over all the erro.json* files, rename the erro.json_timestamp to error.json and
            zip then with the latest timestamp. */
	sprintf(command, "cd %s; for f in error.json* ; do if [ \"$f\" != \"error.json\" ]; \
            then mv $f error.json; fi; zip -m ./"RDIAG_ZIP_FILE_NAME" ./error.json 2>/dev/null 1>/dev/null; \
             echo $? > "RDIAG_ZIP_STATUS_FILE"; sleep 1; done;",  outDirName, ver1, ver2, ver3, ver4);
    	status = WEXITSTATUS(system(command));
	CAB_DBG(CAB_GW_DEBUG, "WEXITSTATUS status=%d\r\n", status);
	status = readStatusFile(RDIAG_ZIP_STATUS_FILE, &cmdStatus);
    	if(status != GEN_SUCCESS) {
    	    CAB_DBG(CAB_GW_ERR, "Error in reading status file, return code: %d", status);
    	    //unlock to protect event summary data
    	    pthread_mutex_unlock(&g_lock);
    	    return status;
    	}
	if( cmdStatus != GEN_SUCCESS ) {
	    CAB_DBG(CAB_GW_ERR, "Zip File Creation Failed");
    	    pthread_mutex_unlock(&g_lock);
	    return FILE_CREATE_ERROR;
	}
    	memset(command, 0, sizeof(command));
    }

    /*  Updating event summary file   */

    //Increase zip file count
    g_eventSumm.zipFileCnt ++;

    //Write updated value in summary file
    status = writeEventSummary(&g_eventSumm);
    if(GEN_SUCCESS != status) {
        CAB_DBG(CAB_GW_ERR, "Failed to update %s file", eventSumFileName);
        //Unlock to protect event data
        pthread_mutex_unlock(&g_lock);
        return FILE_WRITE_ERROR;
    }

    //unlock to protect event summary data
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e getPackageFile(void * data)
{
    FUNC_ENTRY

    struct dirent *dp;                              // Pointer for directory entry
    time_t recenttime = 0;                          //timestamp for recent file
    struct stat statbuf;                            //to get stat of file
    char buffer[MAX_LEN_FILENAME];                  //to temporary store file name
    char recentFile[MAX_LEN_FILENAME] = {'\0'};     //to store recent file name
    char ext[5];                                    //to get extention of file
    uint8_t len, loopCnt;
    fileStorageInfo_t tempFileStorageInfo = *(fileStorageInfo_t*)data;

    //lock to protect event summary data
    pthread_mutex_lock(&g_lock);

    // opendir() returns a pointer of DIR type.
    DIR *dr = opendir(outDirName);

    if(dr == NULL) {  // opendir returns NULL if couldn't open directory
        CAB_DBG(CAB_GW_ERR, "Failed to open directory");
        //unlock to protect event summary data
        pthread_mutex_unlock(&g_lock);
        return DIR_OPEN_FAIL;
    }

    // for readdir()
    while((dp = readdir(dr)) != NULL) {
    	if(strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0)
    		continue;
    	len = strlen(dp->d_name);
    	len -= 4;
    	for(loopCnt = 0; dp->d_name[len] != '\0'; loopCnt++) {
    		ext[loopCnt] = dp->d_name[len];
    		len++;
    	}

    	len = strlen(dp->d_name);
    	len -= 4;
    	for(loopCnt = 0; dp->d_name[len] != '\0'; loopCnt++) {
    		ext[loopCnt] = dp->d_name[len];
    		len++;
    	}

    	//Get latest zip file to send it on cloud
    	if(strncmp(ext, ".zip", 4) == 0) {
    		sprintf(buffer, "%s/%s", outDirName, dp->d_name);
    		lstat(buffer, &statbuf);
    		if(statbuf.st_mtime > recenttime) {
    			sprintf(recentFile, "%s", dp->d_name);
    			recenttime = statbuf.st_mtime;
    		}
    	}
    }

    if(recentFile[0] == '\0') {
        CAB_DBG(CAB_GW_DEBUG, "No pending event");
        closedir(dr);
        //unlock to protect event summary data
        pthread_mutex_unlock(&g_lock);
        return MISC_NO_PENDING_EVENT;
    }

    //To send latest file path to get access of file
    sprintf(tempFileStorageInfo.filename, "%s", recentFile);
    sprintf(tempFileStorageInfo.storagePath, "%s", outDirName);
    memcpy(data, &tempFileStorageInfo, sizeof(fileStorageInfo_t));

    closedir(dr);

    //unlock to protect event summary data
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e deletePackageFile(void * data)
{
    FUNC_ENTRY

    int32_t status;
    char buffer[MAX_LEN_FILENAME];                  //to temporary store file name
    fileStorageInfo_t* tempFileStorageInfo = (fileStorageInfo_t*)data;
    
    if(tempFileStorageInfo == NULL) {
    	CAB_DBG(CAB_GW_ERR, "NULL pointing error");
    	return GEN_NULL_POINTING_ERROR;
    }

    //lock to protect event summary data
    pthread_mutex_lock(&g_lock);

    //To delete package files from dataStorage directory which name is provided in argument
	
    sprintf(buffer, "%s/%s", tempFileStorageInfo->storagePath, tempFileStorageInfo->filename);
    status = remove(buffer);
    if(status != 0) {
        CAB_DBG(CAB_GW_ERR, "Error in deleting package file");
        //unlock to protect event summary data
        pthread_mutex_unlock(&g_lock);
        return FILE_DELETE_ERROR;
    }

    /*  Updating event summary file   */

    //Decrease zip file count
    if(g_eventSumm.zipFileCnt > 0) {
        g_eventSumm.zipFileCnt --;

        //Write updated value in summary file
        status = writeEventSummary(&g_eventSumm);
        if(GEN_SUCCESS != status) {
            CAB_DBG(CAB_GW_ERR, "Failed to update %s file", eventSumFileName);
            //Unlock to protect event data
            pthread_mutex_unlock(&g_lock);
            return FILE_WRITE_ERROR;
        }
    }

    //unlock to protect event summary data
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e getZipCount(void * zipCount)
{
	FUNC_ENTRY

	if(zipCount == NULL) {
		CAB_DBG(CAB_GW_ERR, "NULL pointing error");
		return GEN_NULL_POINTING_ERROR;
	}
	//lock to protect event summary data
	pthread_mutex_lock(&g_lock);

	memcpy(zipCount, &(g_eventSumm.zipFileCnt), sizeof(uint16_t));

	//unlock to protect event summary data
	pthread_mutex_unlock(&g_lock);

	FUNC_EXIT

	return GEN_SUCCESS;
}
