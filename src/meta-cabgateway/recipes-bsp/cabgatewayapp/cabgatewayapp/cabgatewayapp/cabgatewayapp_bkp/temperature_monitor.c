/************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : accel.c
 * @brief : Accelerometer Temperature App
 *
 * @Author     - VT
 *************************************************************************************************
 * History
 *
 * Aug/20/2019, VT , First Draft for Temperature ShutOff application
 *************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>
#include <time.h>
#include <fcntl.h>
#include "debug.h"
#include "common/error.h"
#include "cJSON.h"
#include "otp.h"
#include "temperature_monitor.h"

#define TEMP_SHT_OFF_BIT	(0)

configTemperatureData_t configTempArr[4];

returnCode_e burn_otp() 
{
	FUNC_ENTRY
	returnCode_e retCode = GEN_SUCCESS;
	uint8_t retryCount = 0;
	uint32_t value = 0;

	while(retryCount++ < 3) {
		retCode = write_otp(TEMP_SHT_OFF_BIT);
		if (retCode == GEN_SUCCESS) {
			CAB_DBG(CAB_GW_TRACE, "Write OTP success");
			retCode = read_otp(&value);
			if (retCode == GEN_SUCCESS) {
				CAB_DBG(CAB_GW_TRACE, "Value : 0x%08x", value);
				if ( (value & (1<<TEMP_SHT_OFF_BIT)) == 0 ) {
					CAB_DBG(CAB_GW_ERR, "OTP write is not as expected");
					retCode = GEN_API_FAIL;
				} else {
					CAB_DBG(CAB_GW_TRACE, "Write & Read OTP matched");
					retCode = GEN_SUCCESS;
					break;
				}
			} else {
			     CAB_DBG(CAB_GW_ERR, "OTP read API failed");
			     retCode = GEN_API_FAIL;
			}
		} else {
			CAB_DBG(CAB_GW_ERR, "Write OTP API failed");
			retCode = GEN_API_FAIL;
		}
	}
	FUNC_EXIT
	return retCode;
}


static returnCode_e writeTemperDataRecordFile(temp_shutoff_record_data_t *data)
{
	FUNC_ENTRY
	int file;
	uint32_t ret;
	returnCode_e retCode = GEN_SUCCESS;
	if(data == NULL) {
		CAB_DBG(CAB_GW_ERR, "NULL POINTING ERROR %d", retCode);
		FUNC_EXIT
		return GEN_NULL_POINTING_ERROR;
	}

	file = open(THERMAL_DATA_RECORD_FILE, O_WRONLY | O_CREAT, 0644);

	if(file == -1) {
		CAB_DBG(CAB_GW_ERR, "File Open Failed %d", retCode);
		FUNC_EXIT
		return FILE_OPEN_FAIL;
	}

	ret = write(file, data, sizeof(temp_shutoff_record_data_t));

	if(ret == -1) {
		CAB_DBG(CAB_GW_ERR, "File Write failed %d", retCode);
		close(file);
		FUNC_EXIT
		return FILE_WRITE_ERROR;
	}

	fsync(file);
	close(file);

	FUNC_EXIT
	return GEN_SUCCESS;

}

/* Init storage modules */
returnCode_e init_temp_modules(temp_shutoff_record_data_t *temperature_data)
{
	FUNC_ENTRY
	returnCode_e retCode = GEN_SUCCESS;
	memset(temperature_data, 0, sizeof(temp_shutoff_record_data_t));
	struct stat st = {0};

	if(stat(TEMP_SHUTOFF_DIR, &st) == -1) {
		if((mkdir(TEMP_SHUTOFF_DIR, 0755)) == -1) {
			CAB_DBG(CAB_GW_ERR, "Directory creation failed %d", retCode);
			FUNC_EXIT
			return DIR_CREATE_FAIL;
		}
	}

	if(access(THERMAL_DATA_RECORD_FILE, F_OK) == -1) {
		CAB_DBG(CAB_GW_DEBUG, "No Default configuration found Setting default values");
		temperature_data->alarm_count = 0;
		temperature_data->detected_Range = 0;
		temperature_data->last_sleep_time = 0;
		temperature_data->OTP_Bit = false;
		writeTemperDataRecordFile(temperature_data);
	}

	FUNC_EXIT
	return retCode;
}

returnCode_e accelerometerInit(void)
{
	FUNC_ENTRY
	returnCode_e retCode = GEN_SUCCESS;

	retCode = accelInit();
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Accel Init Failed  %d", retCode);
		FUNC_EXIT
		return GEN_API_FAIL;
	}

	FUNC_EXIT
	return retCode;
}

static returnCode_e getmpuTemp(int *temp)
{
	FUNC_ENTRY
	FILE *fp;
	returnCode_e retCode = GEN_SUCCESS;

	fp = fopen(MPU_TEMPERATURE, "r");

	if(fp == NULL) {
		CAB_DBG(CAB_GW_ERR, "NULL Pointer while reading MPU temperature");
		retCode = GEN_NULL_POINTING_ERROR;
		FUNC_EXIT
		return retCode;
	}

	fscanf(fp, "%d", temp);
	fclose(fp);

	FUNC_EXIT
	return retCode;
}

static returnCode_e getaccelTemp(float *acc_temp)
{
	FUNC_ENTRY
	int sample_value = 0;
	returnCode_e retCode = GEN_SUCCESS;
	while(sample_value < 5) {
		sample_value = sample_value + 1;
		retCode = accelReadTemp(acc_temp);

		if (retCode != GEN_SUCCESS) {
			CAB_DBG(CAB_GW_ERR, "Accel Temp Read Failed %d", retCode);
			Sem1_DeInit();
			FUNC_EXIT
			return retCode;
		}
		sleep(2);
	}
	FUNC_EXIT
	return retCode;
}

returnCode_e getAmbientTemp(int *ambientTemp, int *accelerometerAmbTemp, int *mpu_temp, int *measure_delta)
{
	FUNC_ENTRY
	int delta;
	float acc_temp = 0;
	returnCode_e retCode = GEN_SUCCESS;

	/* GET Accel Temperature */
	retCode = getaccelTemp(&acc_temp);
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_WARNING, "Accelerometer Temperature Fetch Failed %d", retCode);
		CAB_DBG(CAB_GW_INFO, "Accelerometer Fetch Failed Swithching to MPU as ambient");
		*(accelerometerAmbTemp) = ((*mpu_temp)/1000) - 15;
		FUNC_EXIT
		return GEN_SUCCESS;
	}

	*(accelerometerAmbTemp) = (int)acc_temp;
	delta = ((*mpu_temp)/1000) - *(accelerometerAmbTemp);

	if (delta >= 20) {
		*measure_delta = 1;
		(*ambientTemp) = ((*mpu_temp)/1000) - 15;
		if (((*ambientTemp) - *(accelerometerAmbTemp)) > 5) {
			(*ambientTemp) = *(accelerometerAmbTemp);
		}
	} else {
		*measure_delta = 0;
	}

	FUNC_EXIT
	return retCode;
}

returnCode_e write_init_configparam(void)
{
	FUNC_ENTRY
	char *payloadAddr = NULL;
	cJSON *root;
	cJSON *fmt;
	cJSON *fmt_new;
	root = cJSON_CreateObject();
	configTemperatureData_t configTemp;
	int status;
	FILE *fp;

	cJSON_AddItemToObject(root, "thermal_shutoff_configs", fmt = cJSON_CreateObject());
	cJSON_AddItemToObject(fmt, "R1", fmt_new = cJSON_CreateObject());

	if (!root) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}

	configTemp.lower_accel_limit = 70;
	configTemp.upper_accel_limit = 85;
	configTemp.normal_sleep_time = 1800;
	configTemp.abv_thr_sleep_time = 3600;
	configTemp.alarm_count = 10;

	cJSON_AddNumberToObject(fmt_new, "lower_limit", configTemp.lower_accel_limit);
	cJSON_AddNumberToObject(fmt_new, "upper_limit", configTemp.upper_accel_limit);
	cJSON_AddNumberToObject(fmt_new, "slp_sec_until_count", configTemp.normal_sleep_time);
	cJSON_AddNumberToObject(fmt_new, "count", configTemp.alarm_count);
	cJSON_AddNumberToObject(fmt_new, "slp_sec_after_count", configTemp.abv_thr_sleep_time);

	cJSON_AddItemToObject(fmt, "R2", fmt_new = cJSON_CreateObject());
	if (!root) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}

	configTemp.lower_accel_limit = 85;
	configTemp.upper_accel_limit = 90;
	configTemp.normal_sleep_time = 900;
	configTemp.abv_thr_sleep_time = 3600;
	configTemp.alarm_count = 2;

	cJSON_AddNumberToObject(fmt_new, "lower_limit", configTemp.lower_accel_limit);
	cJSON_AddNumberToObject(fmt_new, "upper_limit", configTemp.upper_accel_limit);
	cJSON_AddNumberToObject(fmt_new, "slp_sec_until_count", configTemp.normal_sleep_time);
	cJSON_AddNumberToObject(fmt_new, "count", configTemp.alarm_count);
	cJSON_AddNumberToObject(fmt_new, "slp_sec_after_count", configTemp.abv_thr_sleep_time);


	cJSON_AddItemToObject(fmt, "R3", fmt_new = cJSON_CreateObject());
	if (!root) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}

	configTemp.lower_accel_limit = 0;
	configTemp.upper_accel_limit = 90;
	configTemp.normal_sleep_time = 43200;
	configTemp.abv_thr_sleep_time = 0;
	configTemp.alarm_count = 0;

	cJSON_AddNumberToObject(fmt_new, "lower_limit", configTemp.lower_accel_limit);
	cJSON_AddNumberToObject(fmt_new, "upper_limit", configTemp.upper_accel_limit);
	cJSON_AddNumberToObject(fmt_new, "slp_sec_until_count", configTemp.normal_sleep_time);
	cJSON_AddNumberToObject(fmt_new, "count", configTemp.alarm_count);
	cJSON_AddNumberToObject(fmt_new, "slp_sec_after_count", configTemp.abv_thr_sleep_time);


	cJSON_AddItemToObject(fmt, "R4", fmt_new = cJSON_CreateObject());
	if (!root) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}

	configTemp.lower_accel_limit = -40;
	configTemp.upper_accel_limit = 0;
	configTemp.normal_sleep_time = 21600;
	configTemp.abv_thr_sleep_time = 0;
	configTemp.alarm_count = 0;

	cJSON_AddNumberToObject(fmt_new, "lower_limit", configTemp.lower_accel_limit);
	cJSON_AddNumberToObject(fmt_new, "upper_limit", configTemp.upper_accel_limit);
	cJSON_AddNumberToObject(fmt_new, "slp_sec_until_count", configTemp.normal_sleep_time);
	cJSON_AddNumberToObject(fmt_new, "count", configTemp.alarm_count);
	cJSON_AddNumberToObject(fmt_new, "slp_sec_after_count", configTemp.abv_thr_sleep_time);

	payloadAddr = cJSON_PrintUnformatted(root);

	cJSON_Delete(root);

	// Write the Initial Configuration to file
	fp = fopen(CFG_TEMP_SHUTOFF_FILE, "w");
	if (fp == NULL) {
		CAB_DBG(CAB_GW_ERR, "File Open failed");
		FUNC_EXIT
		return GEN_API_FAIL;
	}

	status = fprintf(fp, "%s\n", payloadAddr);

	if(status < 0) {
		CAB_DBG(CAB_GW_ERR, "File Write Failed");
		fclose(fp);
		FUNC_EXIT
		return GEN_API_FAIL;
	}

	fclose(fp);

	FUNC_EXIT
	return GEN_SUCCESS;
}

static returnCode_e read_config_param(void)
{
	FUNC_ENTRY
	FILE *fp;

	cJSON *root;
	cJSON *item;
	cJSON *item_new;
	cJSON *new;

	char buffer[2048];

	fp = fopen(CFG_TEMP_SHUTOFF_FILE, "r");
	fscanf(fp, "%s", buffer);
	fclose(fp);

	root = cJSON_Parse(buffer);
	if (!root) { 
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}

	/* RANGE 1 */
	item = cJSON_GetObjectItem(root, "thermal_shutoff_configs");
	if(!item) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}

	item_new = cJSON_GetObjectItem(item, "R1");
	if (!item_new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}

	new = cJSON_GetObjectItem(item_new, "lower_limit");

	if (!new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}
	configTempArr[0].lower_accel_limit = new->valueint;

	new = cJSON_GetObjectItem(item_new, "upper_limit");
	if (!new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}
	configTempArr[0].upper_accel_limit = new->valueint;

	new = cJSON_GetObjectItem(item_new, "slp_sec_until_count");
	if (!new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}
	configTempArr[0].normal_sleep_time = new->valueint;

	new = cJSON_GetObjectItem(item_new, "count");
	if (!new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}
	configTempArr[0].alarm_count = new->valueint;

	new = cJSON_GetObjectItem(item_new, "slp_sec_after_count");
	if (!new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}
	configTempArr[0].abv_thr_sleep_time = new->valueint;


	/* RANGE 2 */

	item_new = cJSON_GetObjectItem(item, "R2");
	if (!item_new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}

	new = cJSON_GetObjectItem(item_new, "lower_limit");
	if (!new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}
	configTempArr[1].lower_accel_limit = new->valueint;

	new = cJSON_GetObjectItem(item_new, "upper_limit");
	if (!new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}
	configTempArr[1].upper_accel_limit = new->valueint;

	new = cJSON_GetObjectItem(item_new, "slp_sec_until_count");
	if (!new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}
	configTempArr[1].normal_sleep_time = new->valueint;

	new = cJSON_GetObjectItem(item_new, "count");
	if (!new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}
	configTempArr[1].alarm_count = new->valueint;

	new = cJSON_GetObjectItem(item_new, "slp_sec_after_count");
	if (!new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}
	configTempArr[1].abv_thr_sleep_time = new->valueint;

	/* RANGE 3 */
	item_new = cJSON_GetObjectItem(item, "R3");
	if (!item_new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}

	new = cJSON_GetObjectItem(item_new, "lower_limit");
	if (!new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}
	configTempArr[2].lower_accel_limit = new->valueint;

	new = cJSON_GetObjectItem(item_new, "upper_limit");
	if (!new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}
	configTempArr[2].upper_accel_limit = new->valueint;

	new = cJSON_GetObjectItem(item_new, "slp_sec_until_count");
	if (!new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}
	configTempArr[2].normal_sleep_time = new->valueint;

	new = cJSON_GetObjectItem(item_new, "count");
	if (!new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}
	configTempArr[2].alarm_count = new->valueint;

	new = cJSON_GetObjectItem(item_new, "slp_sec_after_count");
	if (!new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}
	configTempArr[2].abv_thr_sleep_time = new->valueint;

	/* RANGE 4*/
	item_new = cJSON_GetObjectItem(item, "R4");
	if (!item_new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}

	new = cJSON_GetObjectItem(item_new, "lower_limit");
	if (!new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}
	configTempArr[3].lower_accel_limit = new->valueint;

	new = cJSON_GetObjectItem(item_new, "upper_limit");
	if (!new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}
	configTempArr[3].upper_accel_limit = new->valueint;

	new = cJSON_GetObjectItem(item_new, "slp_sec_until_count");
	if (!new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}
	configTempArr[3].normal_sleep_time = new->valueint;

	new = cJSON_GetObjectItem(item_new, "count");
	if (!new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}
	configTempArr[3].alarm_count = new->valueint;
	
	new = cJSON_GetObjectItem(item_new, "slp_sec_after_count");
	if (!new) {
		CAB_DBG(CAB_GW_ERR, "CJSON paramter NULL");
		FUNC_EXIT
		return GEN_API_FAIL;
	}
	configTempArr[3].abv_thr_sleep_time = new->valueint;

	cJSON_Delete(root);

	FUNC_EXIT
	return GEN_SUCCESS;
}

static returnCode_e captureData(int  *accelerometer_temperature, int *mpu_temp)
{
	FUNC_ENTRY
	FILE *fPtr;
	int status;
	time_t seconds;

	time(&seconds);

	fPtr = fopen("/media/userdata/thermalShutOFF/temperature_range_limit.csv", "a");
	if(fPtr == NULL) {
		CAB_DBG(CAB_GW_ERR, "File Pointer NULL");
		FUNC_EXIT
		return GEN_NULL_POINTING_ERROR;
	}
	status = fprintf(fPtr, "%ld,%d,%d\n", seconds, *(mpu_temp), *(accelerometer_temperature));
	if(status < 0) {
		CAB_DBG(CAB_GW_ERR, "CSV Write Failed");
		fclose(fPtr);
	}
	fclose(fPtr);
	FUNC_EXIT
	return GEN_SUCCESS;
}

static returnCode_e readTemperDataRecordFile(temp_shutoff_record_data_t *data)
{
	FUNC_ENTRY

	int file;
	uint32_t ret;

	if(data == NULL) {
		CAB_DBG(CAB_GW_ERR, "NULL pointer");
		FUNC_EXIT
		return GEN_NULL_POINTING_ERROR;
	}

	file = open(THERMAL_DATA_RECORD_FILE, O_RDONLY, 0644);
	if(file == -1) {
		CAB_DBG(CAB_GW_ERR, "File Open Failed");
		FUNC_EXIT
		return FILE_OPEN_FAIL;
	}

	ret = read(file, data, sizeof(temp_shutoff_record_data_t));
	if(ret == -1) {
		CAB_DBG(CAB_GW_ERR, "File Read Failed");
		close(file);
		FUNC_EXIT
		return FILE_READ_ERROR;
	}

	close(file);
	FUNC_EXIT
	return GEN_SUCCESS;
}

returnCode_e measureSysTemp(temp_shutoff_record_data_t *temperature_data)
{
	FUNC_ENTRY
	int accelerometer_temperature = 0;
	int mpu_temp = 0;
	int range = 0;
	int alarm_counter = 0;
	int sleep_time;
	int retVal = 0;
	int ambientTemp = 0;
	int measure_delta = 0;
	char *commandBuffer[CMD_BUF_SIZE];
	int count = 0;
	returnCode_e retCode = GEN_SUCCESS;

	commandBuffer[0] = malloc(50);
	commandBuffer[1] = malloc(10);
	commandBuffer[2] = malloc(10);
	commandBuffer[3] = malloc(10);
	commandBuffer[4] = malloc(10);

	snprintf(commandBuffer[0], 50, "/cabApp/snvs_rtc_wakeup");
	snprintf(commandBuffer[1], 10, "-s");
	snprintf(commandBuffer[3], 5, "-d");
	snprintf(commandBuffer[4], 10, "rtc1");

	/* Get MPU Temperature */
	retCode = getmpuTemp(&mpu_temp);
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "MPU Temperature Fetch Failed %d", retCode);
		FUNC_EXIT
		return retCode;
	}

	retCode = getAmbientTemp(&ambientTemp, &accelerometer_temperature, &mpu_temp, &measure_delta);
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Get Ambient Temperature failed : %d", retCode);
		for(count = 0; count < CMD_BUF_SIZE-1 ; count++)
			free(commandBuffer[count]);
		FUNC_EXIT
		return retCode;
	}

	if (measure_delta) {
		accelerometer_temperature = ambientTemp;
	}

	//Upload to cloud Data
#if 0
	if(accelerometer_temperature < NORMAL_TEMP_FOR_CLOUD) {
		//IF any csv is there upload to cloud
		printf("Accel temp less than 66 upload to cloud \r\n");
	}
#endif

	/* Read Upper and Lower Temperature Limit from  CFG_TEMP_SHUTOFF_FILE */
	retCode = read_config_param();
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Read Configuration parameters failed : %d", retCode);
		for(count = 0; count < CMD_BUF_SIZE-1 ; count++)
			free(commandBuffer[count]);
		FUNC_EXIT
		return retCode;
	}

	readTemperDataRecordFile(temperature_data);

	//ALGORITHM
	if (accelerometer_temperature >= configTempArr[0].lower_accel_limit && accelerometer_temperature < configTempArr[0].upper_accel_limit) {

		/* Save recorded Temperature to CSV and update to cloud when temp less than 66C*/
		captureData(&accelerometer_temperature, &mpu_temp);

		alarm_counter = temperature_data->alarm_count;
		range = temperature_data->detected_Range;
		temperature_data->detected_Range = 1;

		if (range != temperature_data->detected_Range) {
			alarm_counter = 0;
			temperature_data->alarm_count = alarm_counter;

		}
		if (alarm_counter < configTempArr[0].alarm_count) {
			alarm_counter = alarm_counter + 1;
			temperature_data->alarm_count = alarm_counter;
			sleep_time = configTempArr[0].normal_sleep_time; // Get sleep time from Config File
			temperature_data->last_sleep_time = sleep_time;
			temperature_data->OTP_Bit = false;
			writeTemperDataRecordFile(temperature_data);
			Sem1_DeInit();

			CAB_DBG(CAB_GW_INFO, "Merging the SYSLOG from RAM");
			if (getAndMergeLogsFromRAM() == GEN_SUCCESS) {
				CAB_DBG(CAB_GW_INFO, "Merged the SYSLOG successfully");
			}
			else {
				CAB_DBG(CAB_GW_ERR, "Merging the SYSLOG failed");
			}

			CAB_DBG(CAB_GW_INFO, "Temperature detected - %d Setting SRTC for %d seconds Counter - %d.....Powering off.....",accelerometer_temperature, sleep_time, alarm_counter);
			snprintf(commandBuffer[2], 10, "%d", sleep_time);
			retVal = setAlarmTime(5, commandBuffer);
			if(retVal) {
				CAB_DBG(CAB_GW_ERR, "Set Alarm for wakeup failed");
				for(count = 0; count < CMD_BUF_SIZE-1 ; count++)
					free(commandBuffer[count]);
				FUNC_EXIT
				return GEN_API_FAIL;
			}
			for(count = 0; count < CMD_BUF_SIZE-1 ; count++)
				free(commandBuffer[count]);
			system("poweroff");
			sleep(5);
		} else {
			alarm_counter = alarm_counter + 1;
			temperature_data->alarm_count = alarm_counter;
			sleep_time = configTempArr[0].abv_thr_sleep_time; // Get sleep time from Config File
			temperature_data->last_sleep_time = sleep_time;
			temperature_data->OTP_Bit = false;
			writeTemperDataRecordFile(temperature_data);
			Sem1_DeInit();

			CAB_DBG(CAB_GW_INFO, "Merging the SYSLOG from RAM");
			if (getAndMergeLogsFromRAM() == GEN_SUCCESS) {
				CAB_DBG(CAB_GW_INFO, "Merged the SYSLOG successfully");
			}
			else {
				CAB_DBG(CAB_GW_ERR, "Merging the SYSLOG failed");
			}

			CAB_DBG(CAB_GW_INFO, "Temperature detected - %d Setting SRTC for %d seconds Counter - %d.....Powering off.....",accelerometer_temperature, sleep_time, alarm_counter);
			snprintf(commandBuffer[2], 10, "%d", sleep_time);
			retVal = setAlarmTime(5, commandBuffer);
			if(retVal) {
				CAB_DBG(CAB_GW_ERR, "Set Alarm for wakeup failed");
				for(count = 0; count < CMD_BUF_SIZE-1 ; count++)
					free(commandBuffer[count]);
				FUNC_EXIT
				return GEN_API_FAIL;
			}
			for(count = 0; count < CMD_BUF_SIZE-1 ; count++)
				free(commandBuffer[count]);
			system("poweroff");
			sleep(5);
		}
	} else if (accelerometer_temperature >= configTempArr[1].lower_accel_limit && accelerometer_temperature < configTempArr[1].upper_accel_limit) {

		/* Save recorded Temperature to CSV*/
		captureData(&accelerometer_temperature, &mpu_temp);
		readTemperDataRecordFile(temperature_data);
		alarm_counter = temperature_data->alarm_count;

		range = temperature_data->detected_Range;
		temperature_data->detected_Range = 2;

		if (range != temperature_data->detected_Range) {
			alarm_counter = 0;
			temperature_data->alarm_count = alarm_counter;
		}

		if (alarm_counter < configTempArr[1].alarm_count) {
			alarm_counter = alarm_counter + 1;
			temperature_data->alarm_count = alarm_counter;
			sleep_time = configTempArr[1].normal_sleep_time; // Get sleep time from Config File
			temperature_data->last_sleep_time = sleep_time;
			temperature_data->OTP_Bit = false;
			writeTemperDataRecordFile(temperature_data);
			Sem1_DeInit();

			CAB_DBG(CAB_GW_INFO, "Merging the SYSLOG from RAM");
			if (getAndMergeLogsFromRAM() == GEN_SUCCESS) {
				CAB_DBG(CAB_GW_INFO, "Merged the SYSLOG successfully");
			}
			else {
				CAB_DBG(CAB_GW_ERR, "Merging the SYSLOG failed");
			}

			CAB_DBG(CAB_GW_INFO, "Temperature detected - %d Setting SRTC for %d seconds Counter - %d.....Powering off.....",accelerometer_temperature, sleep_time, alarm_counter);
			snprintf(commandBuffer[2], 10, "%d", sleep_time);
			retVal = setAlarmTime(5, commandBuffer);
			if(retVal) {
				CAB_DBG(CAB_GW_ERR, "Set Alarm for wakeup failed");
				for(count = 0; count < CMD_BUF_SIZE-1 ; count++)
					free(commandBuffer[count]);
				FUNC_EXIT
				return GEN_API_FAIL;
			}
			for(count = 0; count < CMD_BUF_SIZE-1 ; count++)
				free(commandBuffer[count]);
			system("poweroff");
			sleep(5);
		} else {
			alarm_counter = alarm_counter + 1;
			temperature_data->alarm_count = alarm_counter;
			sleep_time = configTempArr[1].abv_thr_sleep_time; // Get sleep time from Config File
			temperature_data->last_sleep_time = sleep_time;
			temperature_data->OTP_Bit = true;
			writeTemperDataRecordFile(temperature_data);
			Sem1_DeInit();
			
			CAB_DBG(CAB_GW_INFO, "Merging the SYSLOG from RAM");
			if (getAndMergeLogsFromRAM() == GEN_SUCCESS) {
				CAB_DBG(CAB_GW_INFO, "Merged the SYSLOG successfully");
			}
			else {
				CAB_DBG(CAB_GW_ERR, "Merging the SYSLOG failed");
			}

			CAB_DBG(CAB_GW_INFO, "Temperature detected - %d Setting SRTC for %d seconds Counter - %d.....Powering off.....",accelerometer_temperature, sleep_time, alarm_counter);
			//SET OTP Bit TRUE, Write alarm counter, Range, Timer value to Thermal Record File : TBD
			retCode = burn_otp();
			if (GEN_SUCCESS != retCode) {
				CAB_DBG(CAB_GW_ERR, "burn OTP failed in 3 retries.. relying on CSV :(");
			}
			snprintf(commandBuffer[2], 10, "%d", sleep_time);
			retVal = setAlarmTime(5, commandBuffer);
			if(retVal) {
				CAB_DBG(CAB_GW_ERR, "Set Alarm for wakeup failed");
				for(count = 0; count < CMD_BUF_SIZE-1 ; count++)
					free(commandBuffer[count]);
				FUNC_EXIT
				return GEN_API_FAIL;
			}
			for(count = 0; count < CMD_BUF_SIZE-1 ; count++)
				free(commandBuffer[count]);
			system("poweroff");
			sleep(5);
		}
	} else if (accelerometer_temperature >= configTempArr[2].upper_accel_limit) {
		captureData(&accelerometer_temperature, &mpu_temp);
		range = temperature_data->detected_Range;
		temperature_data->detected_Range = 3;
		sleep_time = configTempArr[2].normal_sleep_time; // Get sleep time from Config File
		temperature_data->last_sleep_time = sleep_time;
		temperature_data->OTP_Bit = true;
		writeTemperDataRecordFile(temperature_data);
		retCode = burn_otp();
		if (GEN_SUCCESS != retCode) {
			CAB_DBG(CAB_GW_ERR, "burn OTP failed in 3 retries.. relying on CSV :(");
		}

		Sem1_DeInit();

		CAB_DBG(CAB_GW_INFO, "Merging the SYSLOG from RAM");
		if (getAndMergeLogsFromRAM() == GEN_SUCCESS) {
			CAB_DBG(CAB_GW_INFO, "Merged the SYSLOG successfully");
		}
		else {
			CAB_DBG(CAB_GW_ERR, "Merging the SYSLOG failed");
		}

		CAB_DBG(CAB_GW_INFO, "Temperature detected - %d Setting SRTC for %d seconds.....Powering off.....",accelerometer_temperature, sleep_time);
		snprintf(commandBuffer[2], 10, "%d", sleep_time);
		retVal = setAlarmTime(5, commandBuffer);
		if(retVal) {
			CAB_DBG(CAB_GW_ERR, "Set Alarm for wakeup failed");
			for(count = 0; count < CMD_BUF_SIZE-1 ; count++)
				free(commandBuffer[count]);
			FUNC_EXIT
			return GEN_API_FAIL;
		}
		for(count = 0; count < CMD_BUF_SIZE-1 ; count++)
			free(commandBuffer[count]);
		system("poweroff");
		sleep(5);
	} else if (accelerometer_temperature <= configTempArr[3].lower_accel_limit) {
		captureData(&accelerometer_temperature, &mpu_temp);
		range = temperature_data->detected_Range;
		temperature_data->detected_Range = 4;
		sleep_time = configTempArr[3].normal_sleep_time; // Get sleep time from Config File
		temperature_data->last_sleep_time = sleep_time;
		temperature_data->OTP_Bit = true;
		writeTemperDataRecordFile(temperature_data);

		retCode = burn_otp();
		if (GEN_SUCCESS != retCode) {
			CAB_DBG(CAB_GW_ERR, "burn OTP failed in 3 retries.. relying on CSV :(");
		}

		Sem1_DeInit();
		
		CAB_DBG(CAB_GW_INFO, "Merging the SYSLOG from RAM");
		if (getAndMergeLogsFromRAM() == GEN_SUCCESS) {
			CAB_DBG(CAB_GW_INFO, "Merged the SYSLOG successfully");
		}
		else {
			CAB_DBG(CAB_GW_ERR, "Merging the SYSLOG failed");
		}

		CAB_DBG(CAB_GW_INFO, "Temperature detected - %d Setting SRTC for %d seconds.....Powering off.....",accelerometer_temperature, sleep_time);
		snprintf(commandBuffer[2], 10, "%d", sleep_time);
		retVal = setAlarmTime(5, commandBuffer);
		if(retVal) {
			CAB_DBG(CAB_GW_ERR, "Set Alarm for wakeup failed");
			for(count = 0; count < CMD_BUF_SIZE-1 ; count++)
				free(commandBuffer[count]);
			FUNC_EXIT
			return GEN_API_FAIL; 
		}
		for(count = 0; count < CMD_BUF_SIZE-1 ; count++)
			free(commandBuffer[count]);
		system("poweroff");
		sleep(5);
	} else {
		CAB_DBG(CAB_GW_INFO, "System Temperature - %d",accelerometer_temperature);
		alarm_counter = 0;
		// Once switched to normal temperature, reset the last status
		temperature_data->alarm_count = 0;
		temperature_data->detected_Range = 0;
		temperature_data->last_sleep_time = 0;
		temperature_data->OTP_Bit = false;
		writeTemperDataRecordFile(temperature_data);
		for(count = 0; count < CMD_BUF_SIZE-1 ; count++)
			free(commandBuffer[count]);
	}
	FUNC_EXIT
	return retCode;
}
