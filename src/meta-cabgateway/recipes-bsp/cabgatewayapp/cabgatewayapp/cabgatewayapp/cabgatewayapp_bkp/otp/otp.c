/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : otp.c
 * @brief : This file contains OTP access related APIs
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Nov/14/2019, VT , First Draft
 **************************************************************/


#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "error.h"
#include "debug.h"
#include "otp.h"


returnCode_e write_otp(int bitNo)
{
    FILE *fp;
    char buffer[11] = "";
    uint32_t value = 0;
    int retVal = 0;

    FUNC_ENTRY
    if(bitNo >= 0 && bitNo < 32) {
        value |= 1 << bitNo;
        sprintf(&buffer[0], "0x%08x", value);
        CAB_DBG(CAB_GW_TRACE,"%s\r\n", buffer);

        fp = fopen(OTP_GP0_FILE_NAME, "w");
        if (fp == NULL) {
            return GEN_API_FAIL;
        }
        retVal = fprintf(fp, "%s", buffer);
	if (retVal < 0) {
		CAB_DBG(CAB_GW_ERR, "Fail in writing OTP");
        	fclose(fp);
        	return GEN_API_FAIL;
	}
        fclose(fp);
    } else {
        CAB_DBG(CAB_GW_ERR, "Bit range invalid");
        return GEN_API_FAIL;
    }
    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e read_otp(uint32_t *value)
{
    FILE *fp;

    FUNC_ENTRY
    if (value == NULL) {
	return GEN_API_FAIL;
    }
    fp = fopen(OTP_GP0_FILE_NAME, "r");
    if (fp == NULL) {
        return GEN_API_FAIL;
    }
    fscanf(fp, "0x%08x", value);
    fclose(fp);
    FUNC_EXIT

    return GEN_SUCCESS;
}

