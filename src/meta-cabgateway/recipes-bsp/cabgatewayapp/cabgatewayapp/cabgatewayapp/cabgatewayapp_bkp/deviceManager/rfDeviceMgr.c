/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : rfDeviceMgr.c
 * @brief : This file provides RF Device management APIs
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Aug/30/2018, VR , Updated as per review comments
 * Aug/30/2018, VR , Added data collection from RTC and GPS
 * Aug/24/2018, VR , Added APIs for RF dev manager.
 * Aug/23/2018, VR , Message Queue implementation
 * Aug/17/2018, VT , First Draft
 **************************************************************/

#include "rfDeviceMgr.h"
#include <string.h>

#include <fcntl.h>
#include <mqueue.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include "configuration.h"
#include "commonData.h"
#include <errno.h>
#ifndef RF_SIMULATION
#include "sub1.h"
#endif
#include "rtc.h"
#include "gps.h"
#include "debug.h"

#define MQ_NAME_SIZE  (10)      /**< Maximum Message Queue Name Size. */
#define RF_BUFF_QUEUE  (200)      /**< Buffer message queue, keeping high for GPS data procurement timeout : 1 second */
#define RF_DATA_MQ_NAME "/DMRF1" /**< Message Queue Name for open / unlink APIs*/
#define MQ_SEND_DMRF_TOUT_SEC	10 /**< timeout for mq send from sub1 module*/
#define MQ_SEND_DEINIT_TOUT_SEC	1  /**< timeout for deinit request */


time_t start_time, curr_time;
uint8_t StartTimer = 0;

static pthread_mutex_t rfDataReceiverThreadAliveLock = PTHREAD_MUTEX_INITIALIZER;
//static pthread_mutex_t rfDataGeneratorThreadAliveLock = PTHREAD_MUTEX_INITIALIZER;

static bool rfDataReceiverThreadAliveStatus = true;
//static bool rfDataGeneratorThreadAliveStatus = true;

/**
 * @struct rfCtrlBlock
 * @brief Core Handler structure for RF DM control
 *
 *        This strucure to be used for device controling and will be
 *  defined for each interfaces.
 **/
typedef struct rfCtrlBlock {
    uint8_t isInit;
    uint8_t deinitReq;
    mqd_t rfDataMQ;     /**< Message queue for RF data. */
    struct mq_attr rfDataMQAttr; /**< Message Queue attributes. */
    pthread_t rfDataReceiverThrId; /**< RF Device thread ID */
    pthread_mutex_t rfDevLock;  /**< RF Mutex. */
    char rfMQname[MQ_NAME_SIZE];  /**< Message Queue Name */
    hashHandle_t *rfDevHashTable; /**< HashTable library instance for maintaining device list. */
#ifndef RF_SIMULATION
    rfInfo_t tpmsInfo;  /**< RF device module handle */
#endif
} rfCtrlBlock_t;

/* Static global RF control block. */
static rfCtrlBlock_t rfCtrl  = { .isInit = 0, .deinitReq = 0 };

/*TODO: Print Wheel info is for debug purpose only.
        To be rmeoved in final version. */
static void printWheelInfo(wheelInfo_t inputWheelInfo)
{
    CAB_DBG(CAB_GW_DEBUG,"\r\n\t\t============================================\t\t");
    fflush(stdout);
    CAB_DBG(CAB_GW_DEBUG,"\r\n\t\tHalo Enable\t\t:\t\t");
    fflush(stdout);
    switch(inputWheelInfo.isHaloEnable) {
        case ENABLE:
            CAB_DBG(CAB_GW_DEBUG,"Enabled");
            break;

        case DISABLE:
            CAB_DBG(CAB_GW_DEBUG,"Disabled");
            break;

        default:
            CAB_DBG(CAB_GW_DEBUG,"Invalid");
            break;
    }
    fflush(stdout);

    CAB_DBG(CAB_GW_DEBUG,"\r\n\t\tAttached Type\t\t:\t\t");
    fflush(stdout);
    switch(inputWheelInfo.attachedType) {
        case TYRE:
            CAB_DBG(CAB_GW_DEBUG,"Tyre");
            break;

        case HALO:
            CAB_DBG(CAB_GW_DEBUG,"Halo");
            break;

        default:
            CAB_DBG(CAB_GW_DEBUG,"Invalid");
            break;
    }
    fflush(stdout);

    CAB_DBG(CAB_GW_DEBUG,"\r\n\t\tTyre Position\t\t:\t\t");
    fflush(stdout);
    switch(inputWheelInfo.tyrePos) {
        case INNER:
            CAB_DBG(CAB_GW_DEBUG,"Inner");
            break;

        case OUTER:
            CAB_DBG(CAB_GW_DEBUG,"Outer");
            break;

        default:
            CAB_DBG(CAB_GW_DEBUG,"Invalid");
            break;
    }

    CAB_DBG(CAB_GW_DEBUG,"\r\n\t\tWheel Side\t\t:\t\t");
    fflush(stdout);
    switch(inputWheelInfo.vehicleSide) {
        case LEFT:
            CAB_DBG(CAB_GW_DEBUG,"Left");
            break;

        case RIGHT:
            CAB_DBG(CAB_GW_DEBUG,"Right");
            break;

        default:
            CAB_DBG(CAB_GW_DEBUG,"Invalid");
            break;
    }

    CAB_DBG(CAB_GW_DEBUG,"\r\n\t\tAxle\t\t\t:\t\t %d", inputWheelInfo.axleNum);
    fflush(stdout);
    CAB_DBG(CAB_GW_DEBUG,"\r\n\t\t============================================\t\t");
    fflush(stdout);
}

/* TODO: Simulation code to be removed in final version */
#ifdef RF_SIMULATION
static bool getRfDataGeneratorThreadAliveStatus(void)
{
        bool status;
        pthread_mutex_lock(&rfDataGeneratorThreadAliveLock);
        status = rfDataGeneratorThreadAliveStatus;
        pthread_mutex_unlock(&rfDataGeneratorThreadAliveLock);
        return status;
}

static void setRfDataGeneratorThreadAliveStatus(bool value)
{
        pthread_mutex_lock(&rfDataGeneratorThreadAliveLock);
        rfDataGeneratorThreadAliveStatus = value;
        pthread_mutex_unlock(&rfDataGeneratorThreadAliveLock);
}

bool rfDataGeneratorCallback(void)
{
        bool status = false;
        CAB_DBG(CAB_GW_TRACE, "In func : %s", __func__);
        if(getRfDataGeneratorThreadAliveStatus() == true)
        {
                status = true;
        }

        setRfDataGeneratorThreadAliveStatus(false);

        return status;
}

/* Dummy data generator thread. */
void * rfDataGenerator(void * args)
{
    tpmsData_t localData;
    uint32_t tempSensID[] = {0x819CA1A2, 0x819CA0FF, 0xC01516C3, \
                             0x819CAD5C, 0x819CB8F9, 0xC015167F, \
                             0x819CAAA7, 0x819CA81D, 0xC0151558, \
                             0xC0150C8D
                            };

    FUNC_ENTRY
    int i = 0;
    localData.pressure = 0.5;
    localData.temperature = 10.5;
    localData.battery = -0.5;
    localData.rssi = 12;
    localData.status = 0;
    returnCode_e retCode = GEN_SUCCESS;
    watchDogTimer_t newWatchdogNode;
    
    newWatchdogNode.threadId = syscall(SYS_gettid);
    newWatchdogNode.CallbackFn = rfDataGeneratorCallback;
    
    CAB_DBG(CAB_GW_INFO, "RF data generator thread id : %ld", syscall(SYS_gettid));
    retCode = threadRegister(&newWatchdogNode);
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "RF data generator thread registration failed with error %d", retCode);
            rDiagSendMessage(WATCHDOG_REGISTRATION_FAIL, GW_ERR, "RF data generator thread registration failed");
    }

    while(1) {
        localData.sensorID = tempSensID[i];
        localData.pressure = (localData.pressure < 2.5) ? localData.pressure + 0.5 : 0.5;
        localData.temperature = (localData.temperature > 20.5) ? localData.temperature + 1.5 : 10.5;
        localData.battery = (localData.battery > -2.5) ? localData.battery - 0.5 : -0.5;
        localData.rssi = (localData.rssi < 18) ? localData.rssi + 1 : 12;
        rfDataCallBack(&localData);
        usleep(7.33 * 1e6);
        //i = i<4 ? i+1 : 0;
        i = (i < 9) ? i + 1 : 0;
    }
    retCode = threadDeregister(newWatchdogNode.threadId);
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "RF data generator thread de-registration failed with error %d", retCode);
            rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "RF data generator thread de-registration failed");
    }
    FUNC_EXIT
    return NULL;
}
#endif

/**
 *  This function gets the diag data from the device.
 * */
returnCode_e rfGetDiagData(void * data)
{
    tpmsDiagDataInfo_t *tempTPMSDiagData;
    returnCode_e retCode = GEN_SUCCESS;
    FUNC_ENTRY
    if(data == NULL) {
        return GEN_INVALID_ARG;
    }

    tempTPMSDiagData = (tpmsDiagDataInfo_t *)data;
    pthread_mutex_lock(&(rfCtrl.rfDevLock));
    if(rfCtrl.isInit != 1) {
        pthread_mutex_unlock(&(rfCtrl.rfDevLock));
        return GEN_NOT_INIT;
    }

    getSizeOfHashTable(rfCtrl.rfDevHashTable, &(tempTPMSDiagData->pairedNumber));  
    fetchCurrentDataHashTable(rfCtrl.rfDevHashTable, (void *)(tempTPMSDiagData->data)); 

    pthread_mutex_unlock(&(rfCtrl.rfDevLock));
    FUNC_EXIT
    return retCode;
}

/**
 *  This function adds new device to RF interface. It should be
 * called when new request from mobile to register new TPMS sensor.
 * */
returnCode_e rfAddDevice(void * data)
{
    tpmsConfig_t localConfig;
    tpmsDMData_t localDMData;

    HASH_STATUS retHash = HASH_SUCCESS;
    returnCode_e retCode = GEN_SUCCESS;
    FUNC_ENTRY
    if(data == NULL) {
        return GEN_INVALID_ARG;
    }

    pthread_mutex_lock(&(rfCtrl.rfDevLock));
    if(rfCtrl.isInit != 1) {
        pthread_mutex_unlock(&(rfCtrl.rfDevLock));
        return GEN_NOT_INIT;
    }

    memset(&localDMData, 0, sizeof(tpmsDMData_t));
    memcpy(&localConfig, data, sizeof(tpmsConfig_t));
    memcpy(&(localDMData.wheelInfo), &(localConfig.wheelInfo), sizeof(wheelInfo_t));
    localDMData.tpmsData.sensorID = localConfig.sensorID;

    /* Insert device in Hash table */

    CAB_DBG(CAB_GW_DEBUG,"\r\n\n\t\tRF Added Device : 0x%x \r\n", localConfig.sensorID);
    printWheelInfo(localConfig.wheelInfo);
    retHash = hashInsert(rfCtrl.rfDevHashTable, localConfig.sensorID, &(localDMData));
    if(retHash != HASH_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "Hash Error %s", __FUNCTION__);
        retCode = DM_HASH_ERROR;
        pthread_mutex_unlock(&(rfCtrl.rfDevLock));
        return retCode;
    }

#ifndef RF_SIMULATION
    /* Add RF device */
    retCode = rfCtrl.tpmsInfo.addSensor(&(localConfig.sensorID));
    if (retCode != GEN_SUCCESS) {
	CAB_DBG(CAB_GW_ERR, "Adding RF device failed with code %d", retCode);
        /* Delete from hash table */
        retHash = hashDelete(rfCtrl.rfDevHashTable, localConfig.sensorID);
        pthread_mutex_unlock(&(rfCtrl.rfDevLock));
	return DM_DEV_ADD_ERROR;
    }
#endif

    pthread_mutex_unlock(&(rfCtrl.rfDevLock));
    FUNC_EXIT
    return retCode;
}

/**
 *  This function removes TPMS sensor from RF interface. It would be
 * called from upper application or when request from Mobile.
 * */
returnCode_e rfRemoveDevice(void * data)
{
    tpmsConfig_t localConfig;
    returnCode_e retCode = GEN_SUCCESS;
    HASH_STATUS retHash = HASH_SUCCESS;
    FUNC_ENTRY
    if(data == NULL) {
        return GEN_INVALID_ARG;
    }

    pthread_mutex_lock(&(rfCtrl.rfDevLock));
    if(rfCtrl.isInit != 1) {
        pthread_mutex_unlock(&(rfCtrl.rfDevLock));
        return GEN_NOT_INIT;
    }
    memcpy(&localConfig, data, sizeof(tpmsConfig_t));
#ifndef RF_SIMULATION
    /* remove device from RF module */
    retCode = rfCtrl.tpmsInfo.removeSensor(((uint32_t *)&(localConfig.sensorID)));
    if (retCode != GEN_SUCCESS) {
	CAB_DBG(CAB_GW_ERR, "Remove device failed with code %d", retCode);
        pthread_mutex_unlock(&(rfCtrl.rfDevLock));
        return DM_DEV_REMOVE_ERR;
    }
#endif

    CAB_DBG(CAB_GW_DEBUG,"\r\n\n\t\tRF removed Device : 0x%x\r\n", localConfig.sensorID);
    printWheelInfo(localConfig.wheelInfo);
    /* Delete from hash table */
    retHash = hashDelete(rfCtrl.rfDevHashTable, localConfig.sensorID);
    if(retHash != HASH_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "Hash Error %s", __FUNCTION__);
        pthread_mutex_unlock(&(rfCtrl.rfDevLock));
        retCode = DM_HASH_ERROR;
        return retCode;
    }
    pthread_mutex_unlock(&(rfCtrl.rfDevLock));
    FUNC_EXIT
    return retCode;
}

/**
 *  Dummy function for get device data
 * */
returnCode_e rfGetDeviceData(void * data)
{
    FUNC_ENTRY
    /* NOTE: Kept empty for now to avoid any seg fault in upper application. */
    FUNC_EXIT
    return GEN_API_NOT_AVAIL;
}

/**
 *  Dummy funciton for set device data
 * */
returnCode_e rfSetDeviceData(void * data)
{
    FUNC_ENTRY
    /* NOTE: Kept empty for now to avoid any seg fault in upper application. */
    FUNC_EXIT
    return GEN_API_NOT_AVAIL;
}

returnCode_e rfFlushDevices(void *data)
{
    returnCode_e retCode = GEN_SUCCESS;
    tpmsDMData_t *tpmsSensorArray;
    uint16_t noOfSensors = 0, sensInd = 0;
    FUNC_ENTRY
    CAB_DBG(CAB_GW_DEBUG, "Flushing Devices\r\n");

    HASH_STATUS retHash = HASH_SUCCESS;
    FUNC_ENTRY

    pthread_mutex_lock(&(rfCtrl.rfDevLock));
    if(rfCtrl.isInit != 1) {
        pthread_mutex_unlock(&(rfCtrl.rfDevLock));
        return GEN_NOT_INIT;
    }

    getSizeOfHashTable(rfCtrl.rfDevHashTable, &(noOfSensors));  
    tpmsSensorArray = (tpmsDMData_t *) malloc ( noOfSensors * sizeof(tpmsDMData_t) );
    fetchCurrentDataHashTable(rfCtrl.rfDevHashTable, (void *)(tpmsSensorArray)); 

    /*Flush all sensors from RF module*/
    for(sensInd = 0; sensInd < noOfSensors; sensInd++) {
	CAB_DBG(CAB_GW_DEBUG,  "Removing Sensor : 0x%x", tpmsSensorArray[sensInd].tpmsData.sensorID );
#ifndef RF_SIMULATION
	retCode = rfCtrl.tpmsInfo.removeSensor((uint32_t*)&(tpmsSensorArray[sensInd].tpmsData.sensorID));
    	if (retCode != GEN_SUCCESS) {
        	CAB_DBG(CAB_GW_ERR, "Sensor flush error (error code %d)", retCode);
	        pthread_mutex_unlock(&(rfCtrl.rfDevLock));
        	return DM_DEV_REMOVE_ERR;
    	}
#endif
    }

    /*Flush all sensors from hashtable*/
    retHash = hashFlush(rfCtrl.rfDevHashTable);
    if(retHash != HASH_SUCCESS) {
       	CAB_DBG(CAB_GW_ERR, "Hash Error %s", __FUNCTION__);
        pthread_mutex_unlock(&(rfCtrl.rfDevLock));
       	return DM_HASH_ERROR;
    } 	

    pthread_mutex_unlock(&(rfCtrl.rfDevLock));
    FUNC_EXIT
    return retCode;
}
/* RF Data callback */
void rfDataCallBack(void *tpmsData)
{
    FUNC_ENTRY
	char errorMsg[100] = "";
    struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0};
    clock_gettime(CLOCK_REALTIME, &abs_timeout);
    abs_timeout.tv_sec += MQ_SEND_DMRF_TOUT_SEC;

    CAB_DBG(CAB_GW_DEBUG, "Received data in RF DM");
    int ret = mq_timedsend(rfCtrl.rfDataMQ, (const char *)tpmsData, sizeof(tpmsData_t), 0, &abs_timeout);
    if(ret < 0) {
        CAB_DBG(CAB_GW_WARNING, "mq_timedsend for %s failed with error %s", RF_DATA_MQ_NAME, strerror(errno));
		sprintf(errorMsg, "mq_timedsend for %s failed with error %s", RF_DATA_MQ_NAME, strerror(errno));
		rDiagSendMessage(MESSAGE_QUEUE_OVERFLOW, GW_WARNING, errorMsg);
    }
    fflush(stdout);
    FUNC_EXIT
}

static bool getRfDataReceiverThreadAliveStatus(void)
{
        bool status;
        pthread_mutex_lock(&rfDataReceiverThreadAliveLock);
        status = rfDataReceiverThreadAliveStatus;
        pthread_mutex_unlock(&rfDataReceiverThreadAliveLock);
        return status;
}

static void setRfDataReceiverThreadAliveStatus(bool value)
{
        pthread_mutex_lock(&rfDataReceiverThreadAliveLock);
        rfDataReceiverThreadAliveStatus = value;
        pthread_mutex_unlock(&rfDataReceiverThreadAliveLock);
}

bool rfDataReceiverCallback(void)
{
        bool status = false;
	returnCode_e ret = GEN_SUCCESS;
   	struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0};
   	clock_gettime(CLOCK_REALTIME, &abs_timeout);
	abs_timeout.tv_sec += MQ_SEND_DMRF_TOUT_SEC;
    	tpmsData_t dummyData;

        CAB_DBG(CAB_GW_TRACE, "In func : %s", __func__);
        if(getRfDataReceiverThreadAliveStatus() == true)
        {
                status = true;
        }

        setRfDataReceiverThreadAliveStatus(false);

   	memset(&dummyData, 0, sizeof(tpmsData_t) );
   	ret = mq_timedsend(rfCtrl.rfDataMQ, (const char *)&dummyData, sizeof(tpmsData_t), 0, &abs_timeout);
   	if(ret < 0) {
   		CAB_DBG(CAB_GW_ERR, "mq_timedsend for %s failed with error %s", RF_DATA_MQ_NAME, strerror(errno));
		status = false;
   	}

        return status;
}

/* Recieve thread waiting for data from RF interface. */
void * rfDataReceiver(void * rfHandler)
{
    ssize_t bytes_read;
    returnCode_e retCode = GEN_SUCCESS;
    tpmsDMData_t localDMData;
    tpmsDMData_t hashDMData;
    HASH_STATUS retHash = HASH_SUCCESS;
    watchDogTimer_t newWatchdogNode;
    
    char errorMsg[100] = "";
    double zero = 0;
    FUNC_ENTRY
    
    newWatchdogNode.threadId = syscall(SYS_gettid);
    newWatchdogNode.CallbackFn = rfDataReceiverCallback;
    
    CAB_DBG(CAB_GW_INFO, "RF data receiver thread id : %ld", syscall(SYS_gettid));
    retCode = threadRegister(&newWatchdogNode);
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "RF data receiver thread registration failed with error %d", retCode);
            rDiagSendMessage(WATCHDOG_REGISTRATION_FAIL, GW_ERR, "RF data receiver thread registration failed");
    }

    while(1) {

        /* Reset the local variable to store the data */
        memset(&localDMData, '\0', sizeof(tpmsDMData_t));

        bytes_read = mq_receive(rfCtrl.rfDataMQ, (char *) & (localDMData.tpmsData), sizeof(tpmsData_t), NULL);
        pthread_mutex_lock(&(rfCtrl.rfDevLock));
	if (rfCtrl.deinitReq == 1) {
		CAB_DBG(CAB_GW_DEBUG, "Closing RF DM thread");
                pthread_mutex_unlock(&(rfCtrl.rfDevLock));
		break;
	}
        if(bytes_read >= 0) {

	    if( (localDMData.tpmsData.sensorID == 0) && (localDMData.tpmsData.pressure == 0) && (localDMData.tpmsData.temperature == 0) && (localDMData.tpmsData.battery == 0) && (localDMData.tpmsData.rssi == 0) && (localDMData.tpmsData.status == 0))
	    {
		    setRfDataReceiverThreadAliveStatus(true);
		    pthread_mutex_unlock(&(rfCtrl.rfDevLock));
		    continue;
	    }
            /* Fill up Wheel Info */
            retHash = hashSearch(rfCtrl.rfDevHashTable, localDMData.tpmsData.sensorID, (void *) & (hashDMData));
            if(retHash != HASH_SUCCESS) {
                CAB_DBG(CAB_GW_ERR, "Error in HASH  %d", retHash);
                pthread_mutex_unlock(&(rfCtrl.rfDevLock));
                continue;
            }

            /* Fill Up timestamp info */
            retCode = getTime(&(localDMData.time));
            if(retCode != GEN_SUCCESS) {
                CAB_DBG(CAB_GW_ERR, "Error in get time %d", retCode);
                pthread_mutex_unlock(&(rfCtrl.rfDevLock));
                continue;
            }

            /* Fill Up GPS info */
            retCode = getGPSData(&(localDMData.gpsData));
            if(retCode != GEN_SUCCESS) {
                CAB_DBG(CAB_GW_ERR, "Error in get GPS %d", retCode);
                pthread_mutex_unlock(&(rfCtrl.rfDevLock));
                continue;
            }

	    if((localDMData.gpsData.latitude == zero) && (localDMData.gpsData.longitude == zero) && (localDMData.gpsData.altitude == zero))
	    {
	        if(StartTimer == 0)
	        {
	            StartTimer = 1;
		    start_time = time(NULL);
		} else {
		    curr_time = time(NULL);
		    if(curr_time > (start_time + TWENTY_FOUR_HOUR)) {
			    uint8_t isValid = 0;
			    retCode = CheckXdataisValid(&isValid);
			    if(retCode != GEN_SUCCESS)
			    {
				    CAB_DBG(CAB_GW_ERR, "CheckXdataisValid() fail with error: %d", retCode);
				    return retCode;
			    }       
			    if(!isValid) {
				    rDiagSendMessage(GPS_XDTA_EXPIRED, GW_WARNING, "Rebooting as the GPS XDATA is Expired");
                                    rebootSystem();
			    }
			    CAB_DBG(CAB_GW_INFO, "Reseting GPS");
			    resetGPS();
		        StartTimer = 0;
		    }
		}
	    } else {
	        StartTimer = 0;
	    }

	    /* Copy the current data to hash also */
	    memcpy(&(localDMData.wheelInfo), &(hashDMData.wheelInfo), sizeof(wheelInfo_t));
	    //memcpy(&(hashDMData), &(localDMData), sizeof(tpmsDMData_t));
            retHash = hashUpdateData(rfCtrl.rfDevHashTable, localDMData.tpmsData.sensorID, (void *) & (localDMData));

            CAB_DBG(CAB_GW_DEBUG, "RF DM TPMS Data : %ld,0x%x,%f,%f,%f,%d,%d\r\n", \
                   localDMData.time, \
                   localDMData.tpmsData.sensorID, \
                   localDMData.tpmsData.pressure, \
                   localDMData.tpmsData.temperature, \
                   localDMData.tpmsData.battery, \
                   localDMData.tpmsData.rssi, \
		   localDMData.tpmsData.status);
            pthread_mutex_unlock(&(rfCtrl.rfDevLock));
            /* Call M&T callback. */
            ((interfaceManagerHandler_t *)rfHandler)->callBackFunc((void *)&localDMData);
        } else {
            CAB_DBG(CAB_GW_WARNING, "mq_rec for %s failed with error %s", RF_DATA_MQ_NAME, strerror(errno));
			sprintf(errorMsg, "mq_rec for %s failed with error %s", RF_DATA_MQ_NAME, strerror(errno));
			rDiagSendMessage(MESSAGE_QUEUE_OVERFLOW, GW_WARNING, errorMsg);
            pthread_mutex_unlock(&(rfCtrl.rfDevLock));
        }
        fflush(stdout);
    }

    pthread_mutex_lock(&(rfCtrl.rfDevLock));
    /* cleanup */
    CAB_DBG(CAB_GW_DEBUG, "Closing RF DM thread");
    mq_close(rfCtrl.rfDataMQ);
    mq_unlink(rfCtrl.rfMQname);
    pthread_mutex_unlock(&(rfCtrl.rfDevLock));

    retCode = threadDeregister(newWatchdogNode.threadId);
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "RF data receiver thread de-registration failed with error %d", retCode);
            rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "RF data receiver thread de-registration failed");
    }
    FUNC_EXIT
    return NULL;
}

/**
 *  This function initialize RF device with all required APIs.
 * */
returnCode_e rfInitInterface(interfaceManagerHandler_t *rfHandler, void * initConfig, interruptDataCBType callback)
{
    returnCode_e retCode = GEN_SUCCESS;
    HASH_STATUS retHash = HASH_SUCCESS;
#ifdef RF_SIMULATION
    pthread_t client;
#endif

    FUNC_ENTRY
    CAB_DBG(CAB_GW_INFO, "Initializing RF interface");
    if(rfHandler == NULL || initConfig == NULL || callback == NULL) {
        return GEN_INVALID_ARG;
    }

    rfCtrl.rfDevLock = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;

    pthread_mutex_lock(&(rfCtrl.rfDevLock));
    /* Update interface handler. */
    rfHandler->addDevice = rfAddDevice;
    rfHandler->removeDevice = rfRemoveDevice;
    rfHandler->getData = rfGetDeviceData;
    rfHandler->setData = rfSetDeviceData;
    rfHandler->getDiagData = rfGetDiagData;
    rfHandler->flushDevices = rfFlushDevices;
    rfHandler->initConfiguration = (rfInitConfig_t *) malloc(sizeof(rfInitConfig_t));
    if(rfHandler->initConfiguration == NULL) {
        pthread_mutex_unlock(&(rfCtrl.rfDevLock));
        return GEN_MALLOC_ERROR;
    }
    memcpy(rfHandler->initConfiguration, initConfig, sizeof(rfInitConfig_t)) ;
    rfHandler->callBackFunc = callback;


    /* Device HASH table */
    retHash = initHashTable(&(rfCtrl.rfDevHashTable), \
                            ((rfInitConfig_t*)((interfaceManagerHandler_t *)rfHandler)->initConfiguration)->maxDeviceSupported, \
                            sizeof(tpmsDMData_t));
    if(retHash != HASH_SUCCESS) {
        retCode = DM_INTF_INIT_FAIL;
        free(rfHandler->initConfiguration);
        pthread_mutex_unlock(&(rfCtrl.rfDevLock));
        return retCode;
    }

    /* initialize the queue attributes */
    rfCtrl.rfDataMQAttr.mq_flags = 0;
    rfCtrl.rfDataMQAttr.mq_maxmsg = ((rfInitConfig_t*)((interfaceManagerHandler_t *)rfHandler)->initConfiguration)->maxDeviceSupported + RF_BUFF_QUEUE;
    rfCtrl.rfDataMQAttr.mq_msgsize = sizeof(tpmsData_t);
    rfCtrl.rfDataMQAttr.mq_curmsgs = 0;
    memset((void *)rfCtrl.rfMQname, (int)0, MQ_NAME_SIZE);
    memcpy((void *)rfCtrl.rfMQname, (void *)RF_DATA_MQ_NAME, sizeof(RF_DATA_MQ_NAME));

    /* create the message queue */
    rfCtrl.rfDataMQ = mq_open(RF_DATA_MQ_NAME, O_CREAT | O_RDWR | O_EXCL, 0644, &(rfCtrl.rfDataMQAttr));
    if(rfCtrl.rfDataMQ < 0) {
        CAB_DBG(CAB_GW_ERR, "Failed to open queue %s", RF_DATA_MQ_NAME);
        mq_unlink(RF_DATA_MQ_NAME);
        rfCtrl.rfDataMQ = mq_open(RF_DATA_MQ_NAME, O_CREAT | O_RDWR | O_EXCL , 0644, &(rfCtrl.rfDataMQAttr));
    }

    if(rfCtrl.rfDataMQ >= 0) {
        /* Start RF Reciever thread. */
        pthread_create(&(rfCtrl.rfDataReceiverThrId), NULL, &rfDataReceiver, (void *)rfHandler);
#ifdef RF_SIMULATION
        pthread_create(&client, NULL, &rfDataGenerator, NULL);
#endif

#ifndef RF_SIMULATION
        /* Sub1 interface initialization. */
        rfCtrl.tpmsInfo.vendorType = ((rfInitConfig_t*)((interfaceManagerHandler_t *)rfHandler)->initConfiguration)->sensorVendorType;
        rfCtrl.tpmsInfo.sendTpmsDataToDM = rfDataCallBack;
        retCode = initializeSub1(&(rfCtrl.tpmsInfo));
#endif
        if(retCode != GEN_SUCCESS) {
            deInitHashTable(rfCtrl.rfDevHashTable);
            pthread_cancel(rfCtrl.rfDataReceiverThrId);
            mq_unlink(rfCtrl.rfMQname);
            free(rfHandler->initConfiguration);
            rfHandler->addDevice = NULL;
            rfHandler->removeDevice = NULL;
            rfHandler->getData = NULL;
            rfHandler->setData = NULL;
            rfHandler->getDiagData = NULL;
            rfHandler->flushDevices = NULL;
            retCode = DM_INTF_INIT_FAIL;
            pthread_mutex_unlock(&(rfCtrl.rfDevLock));
            return retCode;
        } else {
            rfCtrl.isInit = 1;
        }
    } else {
        deInitHashTable(rfCtrl.rfDevHashTable);
        mq_unlink(rfCtrl.rfMQname);
        free(rfHandler->initConfiguration);
        rfHandler->addDevice = NULL;
        rfHandler->removeDevice = NULL;
        rfHandler->getData = NULL;
        rfHandler->setData = NULL;
        rfHandler->getDiagData = NULL;
        rfHandler->flushDevices = NULL;
        retCode = DM_INTF_INIT_FAIL;
        pthread_mutex_unlock(&(rfCtrl.rfDevLock));
        return retCode;
    }
    pthread_mutex_unlock(&(rfCtrl.rfDevLock));
    FUNC_EXIT
    return retCode;
}

/**
 *   Deinitializes all RF Device interface APIs.
 **/
returnCode_e rfDeInitInterface(interfaceManagerHandler_t *rfHandler)
{
    returnCode_e retCode = GEN_SUCCESS;
    tpmsData_t dummyData;
    struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0};
    clock_gettime(CLOCK_REALTIME, &abs_timeout);
    abs_timeout.tv_sec += MQ_SEND_DEINIT_TOUT_SEC;


    int ret = 0;
	char errorMsg[100] = "";
    
    FUNC_ENTRY
    CAB_DBG(CAB_GW_INFO, "Deinitializing RF interface");
    if(rfHandler == NULL) {
        retCode = GEN_INVALID_ARG;
        return retCode;
    }
    pthread_mutex_lock(&(rfCtrl.rfDevLock));
    if(rfCtrl.isInit != 1) {
        retCode = GEN_NOT_INIT;
        return retCode;
    }

#ifndef RF_SIMULATION
    deInitializeSub1();
#endif
    deInitHashTable(rfCtrl.rfDevHashTable);
    rfCtrl.deinitReq = 1;
    memset(&dummyData, 0, sizeof(tpmsData_t) );
    ret = mq_timedsend(rfCtrl.rfDataMQ, (const char *)&dummyData, sizeof(tpmsData_t), 0, &abs_timeout);
    if(ret < 0) {
        CAB_DBG(CAB_GW_WARNING, "mq_timedsend for %s failed with error %s", RF_DATA_MQ_NAME, strerror(errno));
		sprintf(errorMsg, "mq_timedsend for %s failed with error %s", RF_DATA_MQ_NAME, strerror(errno));
		rDiagSendMessage(MESSAGE_QUEUE_OVERFLOW, GW_WARNING, errorMsg);
    }
    fflush(stdout);
    pthread_mutex_unlock(&(rfCtrl.rfDevLock));
    pthread_join(rfCtrl.rfDataReceiverThrId, NULL);
    pthread_mutex_lock(&(rfCtrl.rfDevLock));
    mq_close(rfCtrl.rfDataMQ);
    mq_unlink(rfCtrl.rfMQname);
    free(rfHandler->initConfiguration);
    rfHandler->addDevice = NULL;
    rfHandler->removeDevice = NULL;
    rfHandler->getData = NULL;
    rfHandler->setData = NULL;
    rfHandler->getDiagData = NULL;
    rfHandler->flushDevices = NULL;
    rfCtrl.isInit = 0;
    pthread_mutex_unlock(&(rfCtrl.rfDevLock));
    FUNC_EXIT
    return retCode;
}

