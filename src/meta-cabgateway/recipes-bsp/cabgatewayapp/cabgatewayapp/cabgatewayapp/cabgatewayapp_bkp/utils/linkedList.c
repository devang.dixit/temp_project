#include "linkedList.h"
#include <pthread.h>
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#define GEN_SUCCESS 0
#define GEN_NULL_POINTING_ERROR 1
#define GEN_MALLOC_ERROR 2


static linkedList_t *g_linkedListHead = NULL;
static pthread_mutex_t gLinkedListMutex = PTHREAD_MUTEX_INITIALIZER;

int addNode(linkedList_t *tempNode)
{
        pthread_mutex_lock(&gLinkedListMutex);
        linkedList_t *temp = g_linkedListHead;

        if (g_linkedListHead == NULL)
        {
                g_linkedListHead = malloc(sizeof(linkedList_t));
                if (g_linkedListHead == NULL)
                {
                        //CAB_DBG(CAB_GW_ERR, "Linked List head memory allocation fail.\n");
			pthread_mutex_unlock(&gLinkedListMutex);
                        return GEN_MALLOC_ERROR;
                }
                memset((void *)g_linkedListHead, '\0', sizeof(linkedList_t));
                g_linkedListHead->nodeId = tempNode->nodeId;
                g_linkedListHead->opState = tempNode->opState;
                strncpy((char *)g_linkedListHead->longAlphaNum, (char *)tempNode->longAlphaNum, sizeof(tempNode->longAlphaNum));
                strncpy((char *)g_linkedListHead->shortAlphaNum, (char *)tempNode->shortAlphaNum, sizeof(tempNode->shortAlphaNum));
                g_linkedListHead->numericCode = tempNode->numericCode;
                g_linkedListHead->accessType = tempNode->accessType;
                g_linkedListHead->next = NULL;
        }
        else
        {
                while(temp->next != NULL)
                {
                        temp = temp->next;
                }

                temp->next = malloc(sizeof(linkedList_t));
                if (temp->next == NULL)
                {
                        //CAB_DBG(CAB_GW_ERR, "Linked List node memory allocation fail.\n");
                        pthread_mutex_unlock(&gLinkedListMutex);
                        return GEN_MALLOC_ERROR;
                }
		memset(temp->next, 0, sizeof(linkedList_t));
                temp->next->nodeId = tempNode->nodeId;
                temp->nodeId = tempNode->nodeId;
                temp->opState = tempNode->opState;
                strncpy(temp->longAlphaNum, tempNode->longAlphaNum, sizeof(tempNode->longAlphaNum));
                strncpy(temp->shortAlphaNum, tempNode->shortAlphaNum, sizeof(tempNode->shortAlphaNum));
                temp->numericCode = tempNode->numericCode;
                temp->accessType = tempNode->accessType;
                temp->next->next = NULL;
        }

        pthread_mutex_unlock(&gLinkedListMutex);
        //CAB_DBG(CAB_GW_INFO, "Add Node: %d \n", tempNode->nodeId);
        return GEN_SUCCESS;
}

int removeNode(uint32_t nodeId)
{
        pthread_mutex_lock(&gLinkedListMutex);

        linkedList_t *presentNode = g_linkedListHead;
        linkedList_t *pastNode = g_linkedListHead;

        if (g_linkedListHead == NULL)
        {
                //CAB_DBG(CAB_GW_ERR, "Remove node invalid argument\n");
                pthread_mutex_unlock(&gLinkedListMutex);
                return GEN_NULL_POINTING_ERROR;
        }

        if (g_linkedListHead->nodeId == nodeId)
        {
                g_linkedListHead = g_linkedListHead->next;
                free(presentNode);
                presentNode = NULL;
        }
        else
        {
                while((presentNode != NULL) && (presentNode->nodeId != nodeId))
                {
                        pastNode = presentNode;
                        presentNode = presentNode->next;
                }

		if (presentNode == NULL)
                {
                        //CAB_DBG(CAB_GW_ERR, "Node not found. \n");
			pthread_mutex_unlock(&gLinkedListMutex);
                        return GEN_NULL_POINTING_ERROR;
                }
                else if (presentNode->nodeId == nodeId)
                {
                        pastNode->next = presentNode->next;
                        free(presentNode);
			presentNode = NULL;
                }
        }
        pthread_mutex_unlock(&gLinkedListMutex);
        //CAB_DBG(CAB_GW_INFO, "Remove Node: %d \n", nodeId);
        return GEN_SUCCESS;
}

linkedList_t * getLinkedListNode(uint32_t nodeId)
{
        pthread_mutex_lock(&gLinkedListMutex);

        linkedList_t *presentNode = g_linkedListHead;

        if (g_linkedListHead == NULL)
        {
                pthread_mutex_unlock(&gLinkedListMutex);
                return NULL;
        }

        if (g_linkedListHead->nodeId == nodeId)
        {
                pthread_mutex_unlock(&gLinkedListMutex);
                return g_linkedListHead;
        }
        else
        {
                while((presentNode != NULL) && (presentNode->nodeId != nodeId))
                {
                        presentNode = presentNode->next;
                }

		if (presentNode == NULL)
                {
			pthread_mutex_unlock(&gLinkedListMutex);
                        return NULL;
                }
                else if (presentNode->nodeId == nodeId)
                {
			pthread_mutex_unlock(&gLinkedListMutex);
                        return presentNode;
                }
        }
	/*will not be used kept for safe future changes*/
        pthread_mutex_unlock(&gLinkedListMutex);
        return NULL;
}
linkedList_t * getLinkedListHead(void)
{
	linkedList_t *linkedListHeadLocal;
	pthread_mutex_lock(&gLinkedListMutex);
	linkedListHeadLocal =  g_linkedListHead;
	pthread_mutex_unlock(&gLinkedListMutex);
	return linkedListHeadLocal;
}
