/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : debug.h
 * @brief : This file is used for printing debug messages
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Aug/14/2018, VT , First Draft
 **************************************************************/

/*
NOTE :  1) add BUILD macro in make file using "-D BUILD=DEBUG/RELEASE"
        2) if logs not appear in syslog file restart syslog service using below command
            "sudo service rsyslog restart"
*/

#ifndef _DEBUG_H_
#define _DEBUG_H_

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include <stdio.h>
#include <libgen.h>                 //To support basename
#include <syslog.h>                 //For use of syslog
#include "error.h"
#include "remoteDiagModule.h"
#include <sys/time.h>



/****************************************
 ************* DEFINES ******************
 ****************************************/
/* It defines type of debug and release */
#define DEBUG               (10)                    /*Debug*/
#define RELEASE             (11)                    /*Release*/

/* It defines name of application to add log in syslog */
#define APP_NAME            ("CabGateWay")          /*App name*/

/* It defines debug levels  */
#define CAB_GW_ERR          (1)                     /*Error*/
#define CAB_GW_WARNING      (2)                     /*Warning*/
#define CAB_GW_INFO         (3)                     /*Message*/
#define CAB_GW_DEBUG        (4)                     /*Debugging Message*/
#define CAB_GW_TRACE        (5)                     /*Fine level debugging messages*/

/****************************************
 ******** DEBUGGING PRINT DEFINES *******
 ****************************************/
/*It defines API Macro to open and close the syslog service*/
#define OPEN_LOG            openlog(APP_NAME, LOG_PID | LOG_CONS, LOG_LOCAL1);
#define CLOSE_LOG           closelog();

/*This defines enable debug level*/
#if BUILD == DEBUG
#define ENABLE_DBG_LVL  4
#else
#define ENABLE_DBG_LVL  3
#endif

#if ENABLE_DBG_LVL > 0

/* printf("[%s]:[%d]:"msg" \n", __FILE__, __LINE__, ##__VA_ARGS__);\ */
#if BUILD == DEBUG && CABAPPA7 == 1
#ifndef ACCEL_TEMP_APP 
#define CAB_DBG(lvl, msg, ...) \
    do {\
        if (lvl <= ENABLE_DBG_LVL) {\
            char tempStr[MAX_LEN_REMOTE_DIAG_MSG]=""; \
            time_t rawtime; \
            struct tm * timeinfo; \
            extern char *__progname; \
            time(&rawtime); \
            timeinfo = localtime ( &rawtime ); \
            if (CAB_GW_ERR == lvl) {\
                printf("\r\n[%s]:%s:%s: [%d] : \033[1mERROR! :\033[22m "msg"", strtok(asctime(timeinfo), "\n"), __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                fflush(stdout);\
                snprintf(tempStr, sizeof(tempStr), "%s:%s:%d "msg"\n", __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                if(strlen(tempStr) < MAX_LEN_REMOTE_DIAG_MSG-100) {\
                    rDiagAddMessage(rawtime, GW_ERR, tempStr);\
                } \
            }\
            if (CAB_GW_WARNING == lvl) {\
                printf("\r\n[%s]:%s:%s: [%d] : \033[1mWARNING! :\033[22m "msg"", strtok(asctime(timeinfo), "\n"), __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                fflush(stdout); \
                snprintf(tempStr, sizeof(tempStr), "%s:%s:%d "msg"\n", __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                if(strlen(tempStr) < MAX_LEN_REMOTE_DIAG_MSG-100) {\
                    rDiagAddMessage(rawtime, GW_WARNING, tempStr);\
                } \
            }\
            if (CAB_GW_INFO == lvl) {\
                printf("\r\n[%s]:%s:%s: [%d] : INFO! : "msg"", strtok(asctime(timeinfo), "\n"), __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                fflush(stdout); \
                snprintf(tempStr, sizeof(tempStr), "%s:%s:%d "msg"\n", __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                if(strlen(tempStr) < MAX_LEN_REMOTE_DIAG_MSG-100) {\
                    rDiagAddMessage(rawtime, GW_INFO, tempStr);\
                }\
            }\
            if (CAB_GW_DEBUG == lvl) {\
                printf("\r\n[%s]:%s:%s: [%d] : DEBUG! : "msg"", strtok(asctime(timeinfo), "\n"), __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                fflush(stdout); \
                snprintf(tempStr, sizeof(tempStr), "%s:%s:%d "msg"\n", __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                if(strlen(tempStr) < MAX_LEN_REMOTE_DIAG_MSG-100) {\
                    rDiagAddMessage(rawtime, GW_DEBUG, tempStr);\
                }\
            }\
            if (CAB_GW_TRACE == lvl) {\
                printf("\r\n[%s]:%s:%s: [%d] : TRACE! : "msg"", strtok(asctime(timeinfo), "\n"), __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                fflush(stdout); \
            }\
        }\
    } while (0)
#else
#define CAB_DBG(lvl, format, args...) do{}while(0)
#endif
#elif BUILD == RELEASE  && CABAPPA7 == 1
#define CAB_DBG(lvl, msg, ...) \
    do {\
        if (lvl <= ENABLE_DBG_LVL) {\
            char tempStr[MAX_LEN_REMOTE_DIAG_MSG]=""; \
            time_t rawtime; \
            struct tm * timeinfo; \
            extern char *__progname; \
            time(&rawtime); \
            timeinfo = localtime ( &rawtime ); \
            if (CAB_GW_ERR == lvl) {\
                printf("\r\n[%s]:%s:%s: [%d] : \033[1mERROR! :\033[22m "msg"", strtok(asctime(timeinfo), "\n"), __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                fflush(stdout); \
                syslog(LOG_ERR, "%s: [%d] : ERROR! : "msg"\n", basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                snprintf(tempStr, sizeof(tempStr)-10, "%s:%s:%d "msg"\n", __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                if(strlen(tempStr) < MAX_LEN_REMOTE_DIAG_MSG-100) {\
                    rDiagAddMessage(rawtime, GW_ERR, tempStr);\
                } \
            }\
            if(CAB_GW_WARNING == lvl) {\
                printf("\r\n[%s]:%s:%s: [%d] : \033[1mWARNING! :\033[22m "msg"", strtok(asctime(timeinfo), "\n"), __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                fflush(stdout); \
                syslog(LOG_WARNING, "%s: [%d] : WARNING! : "msg"\n", basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                snprintf(tempStr, sizeof(tempStr)-10, "%s:%s:%d "msg"\n", __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                if(strlen(tempStr) < MAX_LEN_REMOTE_DIAG_MSG-100) {\
                    rDiagAddMessage(rawtime, GW_ERR, tempStr);\
                } \
            }\
            if(CAB_GW_INFO == lvl) {\
                printf("\r\n[%s]:%s:%s: [%d] : INFO! : "msg"", strtok(asctime(timeinfo), "\n"), __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                fflush(stdout); \
                syslog(LOG_INFO, "%s: [%d] : INFO! : "msg"\n", basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                snprintf(tempStr, sizeof(tempStr)-10, "%s:%s:%d "msg"\n", __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                if(strlen(tempStr) < MAX_LEN_REMOTE_DIAG_MSG-100) {\
                    rDiagAddMessage(rawtime, GW_ERR, tempStr);\
                } \
            }\
            if(CAB_GW_DEBUG == lvl) {\
                printf("\r\n[%s]:%s:%s: [%d] : DEBUG! : "msg"", strtok(asctime(timeinfo), "\n"), __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                fflush(stdout); \
                syslog(LOG_DEBUG, "%s: [%d] : DEBUG! : "msg"\n", basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                snprintf(tempStr, sizeof(tempStr)-10, "%s:%s:%d "msg"\n", __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                if(strlen(tempStr) < MAX_LEN_REMOTE_DIAG_MSG-100) {\
                    rDiagAddMessage(rawtime, GW_ERR, tempStr);\
                } \
            }\
            if(CAB_GW_TRACE == lvl) {\
                printf("\r\n[%s]:%s:%s: [%d] : TRACE! : "msg"", strtok(asctime(timeinfo), "\n"), __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                fflush(stdout); \
            }\
        }\
    } while (0)
#elif BUILD == DEBUG
#ifndef ACCEL_TEMP_APP
#define CAB_DBG(lvl, msg, ...) \
    do {\
        if (lvl <= ENABLE_DBG_LVL) {\
            time_t rawtime; \
            struct tm * timeinfo; \
            extern char *__progname; \
            time(&rawtime); \
            timeinfo = localtime ( &rawtime ); \
            if (CAB_GW_ERR == lvl) {\
                printf("\r\n[%s]:%s:%s: [%d] : \033[1mERROR! :\033[22m "msg"", strtok(asctime(timeinfo), "\n"), __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                fflush(stdout); \
            }\
            if (CAB_GW_WARNING == lvl) {\
                printf("\r\n[%s]:%s:%s: [%d] : \033[1mWARNING! :\033[22m "msg"", strtok(asctime(timeinfo), "\n"), __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                fflush(stdout); \
            }\
            if (CAB_GW_INFO == lvl) {\
                printf("\r\n[%s]:%s:%s: [%d] : INFO! : "msg"", strtok(asctime(timeinfo), "\n"), __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                fflush(stdout); \
            }\
            if (CAB_GW_DEBUG == lvl) {\
                printf("\r\n[%s]:%s:%s: [%d] : DEBUG! : "msg"", strtok(asctime(timeinfo), "\n"), __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                fflush(stdout); \
            }\
            if (CAB_GW_TRACE == lvl) {\
                printf("\r\n[%s]:%s:%s: [%d] : TRACE! : "msg"", strtok(asctime(timeinfo), "\n"), __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
                fflush(stdout); \
            }\
        }\
    } while (0)
#else
#define CAB_DBG(lvl, format, args...) do{}while(0)
#endif
#elif BUILD == RELEASE
#define CAB_DBG(lvl, msg, ...) \
do {\
    if (lvl <= ENABLE_DBG_LVL) {\
        time_t rawtime; \
        struct tm * timeinfo; \
        extern char *__progname; \
        time(&rawtime); \
        timeinfo = localtime ( &rawtime ); \
        if (CAB_GW_ERR == lvl) {\
            printf("\r\n[%s]:%s:%s: [%d] : \033[1mERROR! :\033[22m "msg"", strtok(asctime(timeinfo), "\n"), __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
            fflush(stdout); \
            syslog(LOG_ERR, "%s: [%d] : ERROR! : "msg"\n", basename(__FILE__), __LINE__, ##__VA_ARGS__);\
        }\
        if(CAB_GW_WARNING == lvl) {\
            printf("\r\n[%s]:%s:%s: [%d] : \033[1mWARNING! :\033[22m "msg"", strtok(asctime(timeinfo), "\n"), __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
            fflush(stdout); \
            syslog(LOG_WARNING, "%s: [%d] : WARNING! : "msg"\n", basename(__FILE__), __LINE__, ##__VA_ARGS__);\
        }\
        if(CAB_GW_INFO == lvl) {\
            printf("\r\n[%s]:%s:%s: [%d] : INFO! : "msg"", strtok(asctime(timeinfo), "\n"), __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
            fflush(stdout); \
            syslog(LOG_INFO, "%s: [%d] : INFO! : "msg"\n", basename(__FILE__), __LINE__, ##__VA_ARGS__);\
        }\
        if(CAB_GW_DEBUG == lvl) {\
            printf("\r\n[%s]:%s:%s: [%d] : DEBUG! : "msg"", strtok(asctime(timeinfo), "\n"), __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
            fflush(stdout); \
            syslog(LOG_DEBUG, "%s: [%d] : DEBUG! : "msg"\n", basename(__FILE__), __LINE__, ##__VA_ARGS__);\
        }\
        if(CAB_GW_TRACE == lvl) {\
            printf("\r\n[%s]:%s:%s: [%d] : TRACE! : "msg"", strtok(asctime(timeinfo), "\n"), __progname, basename(__FILE__), __LINE__, ##__VA_ARGS__);\
            fflush(stdout); \
        }\
    }\
} while (0)
#else

#define CAB_DBG(lvl, format, args...) do{}while(0)

#endif // BUILD

#endif // ENABLE_DBG_LVL>0

/*Function entry-exit prints*/

#if BUILD == DEBUG
#define FUNC_ENTRY  CAB_DBG(CAB_GW_TRACE, "FUNC_ENTRY! : %s", __FUNCTION__);
#define FUNC_EXIT   CAB_DBG(CAB_GW_TRACE, "FUNC_EXIT! : %s", __FUNCTION__);
#elif BUILD == RELEASE
#define FUNC_ENTRY  CAB_DBG(CAB_GW_TRACE, "FUNC_ENTRY!");
#define FUNC_EXIT  CAB_DBG(CAB_GW_TRACE, "FUNC_EXIT!");
#endif // BUILD

#endif      //ifndef _DEBUG_H_
