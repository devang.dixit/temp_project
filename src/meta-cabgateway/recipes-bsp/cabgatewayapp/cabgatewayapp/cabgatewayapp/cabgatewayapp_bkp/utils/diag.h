/********************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file diag.h
 * @brief (Headder file for diag.c.)
 *
 * @Author     - VT
 **********************************************************************************************
 * History
 *
 * Mar/06/2019, VT , First Draft
 **********************************************************************************************/

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include <sys/time.h>


/****************************************
 ********      MACROS	   **************
 ****************************************/
#define FILE_PATH_DIAG              "/media/userdata/dataStorage/diag.csv"
#define DIR_PATH_DIAG		    "/media/userdata/dataStorage/"


/****************************************
 ******** STATIC VARIABLES **************
 ****************************************/

/****************************************
 ******** FUNCTION DEFINITIONS **********
 ****************************************/
/******************************************************************
 *@brief  (This API is used to write string to diag file)
 *
 *@param[IN] string	Debug string to be printed in file
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e diagDump(char * diagString);


#ifdef DIAG_SUPPORT
#define DIAG_DUMP(msg, ...) \
    do {\
	    char diagStr[1024]=""; \
	    time_t rawtime;\
	    struct tm * timeinfo;\
            time ( &rawtime );\
            timeinfo = localtime ( &rawtime );\
	    if (strlen(msg) < 800 ) { \
                snprintf(diagStr, sizeof(diagStr)-10, "[%s],%s,%d,"msg"\n", strtok(asctime(timeinfo),"\n"), basename(__FILE__), __LINE__, ##__VA_ARGS__);\
	        diagDump(diagStr);\
	    }\
        }\
    while (0)
#else
#define DIAG_DUMP(format, args...)  do{}while(0)
#endif
