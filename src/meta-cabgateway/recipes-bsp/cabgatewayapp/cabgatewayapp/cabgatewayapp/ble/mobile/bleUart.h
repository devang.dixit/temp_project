/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : bleUart.h
 * @brief : Header file for bleUart.c
 *
 * @Author     - VT
 ***************************************************************
 * History
 *
 * Aug/27/2018, VT , First Draft
 ***************************************************************/
#ifndef __BLEUART_H__
#define __BLEUART_H__

#include <stdint.h>
#include <time.h>
#include "commonData.h"
#include "configuration.h"
#include "tpmsConfig.h"
#include "error.h"
#include "watchdog.h"
#include "syscall.h"

#define UNUSED_VARIABLE(name) (void)name
#define UART_SOF		        0x55AA                          /**< start of frame */
#define NRF_MTU_HEADER          3                               /**< Len of MTU header */
#define UART_MTU_SIZE           (247 - NRF_MTU_HEADER)          /**< Max len of MTU usable */
#define MAX_PAYLOAD_LEN         (240 - NRF_MTU_HEADER)          /**< Max len of payload */
#define MAX_PAYLOAD_LEN_ANDROID MAX_PAYLOAD_LEN                 /**< Max len of MTU usable */
#define MAX_PAYLOAD_LEN_IOS     (178 - NRF_MTU_HEADER)          /**< Max len of MTU usable for iOS */
#define ZERO_BYTE_CMD_LEN       7                               /**< Len of Zero byte command */
#define CRC_LEN                 1                               /**< CRC len */
#define MAX_CMD_LEN		        (ZERO_BYTE_CMD_LEN + MAX_PAYLOAD_LEN)   /**< MAX single command len */
#define MAX_DATA_LEN            (MAX_PAYLOAD_LEN + CRC_LEN)     /**< MAX payload + crc len */
#define MAX_AXELS_NUMBER        15                              /**< MAX axel number supported */
#define ZERO_BYTE_FILE_CMD_LEN  11                              /**< MAX file command header len */
#define ZERO_BYTE_FILE_INIT_PAYLOAD_LEN 4                       /**< MAX file init command payload */
#define MAX_SENSOR_ID		40                                  /**< */
#define MAX_NW_OPERATOR_LEN	32                                  /**< */
#define MAX_RESPONSE_LEN	32                                  /**< */
#define SOF_LEN			2                                       /**< */
#define MSG_PRIO		0                                       /**< */
#define MAX_BLE_PASSKEY_LEN 	6                                   /**< */
#define MAX_CLOUD_URL_LEN 2048                                  /**< */
#define MAX_FILE_LEN_ALLOWED 	(1024)

#define ANDROID_APP             0x0D0D                          /**< Stands for ANDROID*/
#define IOS_APP                 0x0105                          /**< Stands for IOS*/
#define FILE_XFER_COMMAND       0x0F7E                          /**< Single xfer command identfier*/
#define SINGLE_XFER_COMMAND     0x0516                          /**< File xfer command identfier*/

#define QUEUE_NAME "/commandQueue" /**< UART command queue */
#define UART_PORT   "/dev/ttymxc4" /**< UART interface name */
#define UART_BAUDRATE B115200   /**< UART baudrate */

/* Uart command type typedef */
typedef uint32_t commandCode_e;

/**
 * @struct uartFrame
 * @brief uart command related information
 *
 * This structure contain uart header + payload + crc data
 **/
typedef struct uartFrame {
    uint16_t sof;
    uint16_t cmd;
    uint16_t payloadLen;
    union {
        struct {
            uint16_t subCommand;
            uint16_t fileLen;
        };
        uint8_t data[MAX_PAYLOAD_LEN + CRC_LEN];
    };
    uint8_t crc8;
    uint8_t *payload;
    uint8_t *filePayload;
	uint16_t frameCount;
} uartFrame_t;

/**
 * @struct msgQueue
 * @brief message information
 *
 * This structure contain queue message
 **/
typedef struct msgQueue {
    uartFrame_t *ptr;
} msgQueue_t;

/**
 * @struct commandStatus
 * @brief send command response to mobile
 *
 * This structure contain response of command
 **/
typedef struct commandStatus {
    uint16_t commandId;
    uint16_t status;
    char msg[MAX_RESPONSE_LEN];
} commandStatus_t;


/**
 * @struct lookupTable
 * @brief BLE command type and info
 *
 * This structure frame type and function pointer
 **/
typedef void (*funcPtr)(void * value);
typedef struct lookupTable
{
    int commandID;
    int androidCmdFrameType;
    int androidResponseFrameType;
    int iosCmdFrameType;
    int iosResponseFrameType;
    funcPtr commandHandler;
} lookupTable_t;
/** @brief Init BLE interface
 *
 *      Start ble uart interface and listing ble command
 *
 * @return status code.
 */
returnCode_e bleInit(void);

/** @brief Add and enable device interface.
 *
 *      Release and stop ble interface
 *
 * @return status code.
 *
 */
returnCode_e bleDeInit(void);

/** @brief Get BLE module MAC address.
 *
 *      Send query to get BLE MAC address if BLE module is initialized
 *
 * @return status code.
 *
 */
returnCode_e getBleMacAdd(void);

/** @brief Get BLE advertisement started or not.
 *
 *      Send query to get status whether BLE advertisement is started or not.
 *
 * @return status code.
 *
 */
returnCode_e getBleMacAdvStarted(void);

/** @brief Set ble power mode
 *
 *      Change state of ble hardware module
 *
 * @param[in] power mode
 * @return status code.
 *
 */
returnCode_e bleSetPowerMode(commandCode_e powerType);

/** @brief Send response of command to mobile
 *
 *     API used for sending response of received command
 *
 * @param[in] command id
 * @param[in] command status
 *
 * @return none.
 */
void sendCommandStatus(commandCode_e, returnCode_e);

/** @brief JSON to system structure
 *
 *      API used for converting json payload to system strucutre
 *
 * @param[in] command type
 * @param[in] json object
 * @param[out] system data as per command type
 *
 * @return status code
 */
returnCode_e convertJsonToSystemPayload(commandCode_e, const char *, void *);


returnCode_e setBLEKeepAliveStatus (bool isBLEAlive);
 
bool getBLEKeepAliveStatus ();

/** @brief command handler function
 *
 *      handler function for the BLE commands
 *
 * @param[in] UART frame pointer
 * @return None
 *
 */
void cmdHandler_CFG_GW_TPMS_UNPAIR_CMD(void *);
void cmdHandler_CFG_GW_TPMS_VENDOR_CMD(void *);
void cmdHandler_CFG_GW_TARGET_PRESSURE_CMD(void *);
void cmdHandler_CFG_GW_BATTERY_CMD(void *);
void cmdHandler_CFG_GW_TPMS_PAIR_CMD(void *);
void cmdHandler_CFG_GW_ATTACH_HALO_SN_CMD(void *);
void cmdHandler_CFG_GW_DETACH_HALO_SN_CMD(void *);
void cmdHandler_CFG_GW_FLUSH_TYRE_DETAILS_CMD(void *);
void cmdHandler_CFG_GW_RECORD_TREAD_DETAILS_CMD(void *);
void cmdHandler_CFG_GW_SYSTEM_CMD(void *);
void cmdHandler_CFG_MOBILE_UTC_TIME_CMD(void *);
void cmdHandler_CFG_BLE_PASSKEY_CMD(void *);
void cmdHandler_CFG_GW_OPT_CMD(void *);
void cmdHandler_CFG_GW_CLOUD_CMD(void *);
void cmdHandler_CFG_GW_UPDATE_TYRE_DETAILS_CMD(void *);
void cmdHandler_GET_CFG_GW_ALL_HALO_SN_CMD(void *);
void cmdHandler_GET_CFG_GW_TYRE_DETAILS_CMD(void *);
void cmdHandler_GET_CFG_GW_TREAD_HISTORY_CMD(void *);
void cmdHandler_GET_CFG_GW_SYSTEM_CMD(void *);
void cmdHandler_GET_CFG_GW_OPT_CMD(void *);
void cmdHandler_GET_CFG_GW_TPMS_VENDOR_CMD(void *);
void cmdHandler_GET_CFG_GW_CLOUD_CMD(void *);
void cmdHandler_GET_CFG_GW_TARGET_PRESSURE_CMD(void *);
void cmdHandler_GET_CFG_GW_BATTERY_CMD(void *);
void cmdHandler_DIAG_GW_INFO_CMD(void *);
void cmdHandler_DIAG_GW_PAIRED_SENSOR_CMD(void *);
void cmdHandler_GET_CFG_GW_TPMS_PAIR_CMD(void *);
void cmdHandler_DIAG_GW_GPS_INFO_CMD(void *);
void cmdHandler_DIAG_GW_CELLULAR_INFO_CMD(void *);
void cmdHandler_DIAG_GW_TPMS_DATA_CMD(void *);
void cmdHandler_DIAG_GW_TPMS_BY_ID_CMD(void *);
void cmdHandler_DIAG_GW_BATTERY_EV_LOG_CMD(void *);
void cmdHandler_SELF_DIAG_GW_CMD(void *);
void cmdHandler_INSTALLATION_STATUS_CMD(void *);
void cmdHandler_SYS_GW_RESET_CMD(void *);
	

#endif /* __BLEUART_H__ */
