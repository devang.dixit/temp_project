/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : error.h
 * @brief : This file is used to define enums and global variables to print error messages.
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Aug/14/2018, VT , First Draft
 **************************************************************/

#ifndef _ERROR_H_
#define _ERROR_H_

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include <stdint.h>

/****************************************
 ************* DEFINES ******************
 ****************************************/
#define FAILURE -1

/****************************************
 ************* ENUMS ********************
 ****************************************/
/*This enum defines different type of general return code*/
typedef enum {
    GEN_SUCCESS = 0,
    GEN_NULL_POINTING_ERROR,
    GEN_INVALID_TYPE,
    GEN_MALLOC_ERROR,
    GEN_NOT_INIT,
    GEN_INVALID_ARG,
    GEN_SYS_TIME_NOT_AVAIL,
    GEN_API_NOT_AVAIL,
    GEN_API_FAIL,
    GEN_SYSCALL_FAIL,
    GEN_LOCK_FAIL,
    AT_CMD_FAIL,
    CHECKSUM_FAIL,
    GEN_CMD_MAX
} generalReturnCode_e;

/*This enum defines different type of file return code*/
typedef enum {
    FILE_NOT_FOUND = GEN_CMD_MAX,	//13
    FILE_OPEN_FAIL,
    FILE_READ_ERROR,
    FILE_WRITE_ERROR,
    FILE_HANDLE_INVALID,
    FILE_NONBLOCK_FAIL,
    FILE_CONTROL_SET_FAIL,
    FILE_CONTROL_GET_FAIL,
    FILE_DELETE_ERROR,
    FILE_CREATE_ERROR,
    FILE_CMD_MAX
} fileReturnCode_e;

/*This enum defines different type of data return code*/
typedef enum {
    DATA_CRC_FAIL = FILE_CMD_MAX,	//23
    DATA_NOT_SUFFICIENT,
    DATA_SEND_FAIL,
    DATA_PTR_INVALID,
    DATA_INVALID,
    DATA_CMD_MAX
} dataReturnCode_e;

/*This enum defines different type of function return code*/
typedef enum {
    FUNC_INIT_FAIL = DATA_CMD_MAX,	//28
    FUNC_CMD_MAX
} funcReturnCode_e;

/*This enum defines different type of module return code*/
typedef enum {
    MODULE_INIT_FAIL = FUNC_CMD_MAX,	//29
    MODULE_DEINIT_FAIL,
    MODULE_ALREADY_INIT,
    MODULE_CMD_MAX
} moduleReturnCode_e;

/*This enum defines different type of miscellaneous return code*/
typedef enum {
    MISC_NO_PENDING_EVENT = MODULE_CMD_MAX,	//32
    MISC_USER_INDICATION_ERROR,
    MISC_CMD_MAX
} miscReturnCode_e;

/*This enum defines different type of directory return code*/
typedef enum {
    DIR_CREATE_FAIL = MISC_CMD_MAX,	//34
    DIR_OPEN_FAIL,
    DIR_CMD_MAX
} directoryReturnCode_e;

/*This enum defines different type of configuration return code*/
typedef enum {
    CONFIG_WHEEL_PAIRED = DIR_CMD_MAX,	//36
    CONFIG_SENSOR_ID_PAIRED,
    CONFIG_TPMS_FAIL,
    CONFIG_NO_TPMS_PAIRED,
    CONFIG_NO_MANDATORY_INFO,
    CONFIG_CMD_MAX
} configurationReturnCode_e;

/*This enum defines different type of message queue return code*/
typedef enum {
    MQ_OPEN_ERROR = CONFIG_CMD_MAX,	//41
    MQ_SEND_ERROR,
    MESSAGE_QUEUE_ERROR_MAX
} messageQueue_e;

/*This enum defines different type of configuration return code*/
typedef enum {
    RTC_IOCTL_FAILED = MESSAGE_QUEUE_ERROR_MAX,	//43
    RTC_SYSSET_FAILED,
    RTC_SYSGET_FAILED,
    RTC_RTCSET_FAILED,
    RTC_RTCGET_FAILED,
    RTC_CMD_MAX
} rtcErrorCode_e;

/*This enum defines different type of DM return code*/
typedef enum {
    DM_DEV_ADD_ERROR = RTC_CMD_MAX,	//48
    DM_DEV_REMOVE_ERR,
    DM_INTF_INIT_FAIL,
    DM_HASH_ERROR,
    DM_CMD_MAX
} dmErrorCode_e;

/*This enum defines different type of GPS return code*/
typedef enum {
    GPS_NO_DATA = DM_CMD_MAX,	//52
    GPS_CMD_MAX
} gpsErrorCode_e;

/****************************************
 ************ STRUCTURES ****************
 ****************************************/
/*This enum defines different type of Application return code*/
typedef enum {
    APP_MAX_RETURN_CODE = GPS_CMD_MAX,	//53
    APP_RET_CMD_MAX
} appReturnCode_e;

/*This enum defines different type of diagnostic return code*/
typedef enum {
    DIAG_NO_TPMS_PAIRED = APP_RET_CMD_MAX,	//54
    DIAG_NO_TPMS_DATA,
    DIAG_CMD_MAX
} diagReturnCode_e;

/*This enum defines different type of cloud return code*/
typedef enum {
    CLOUD_CONN_TIMEOUT = DIAG_CMD_MAX,	//56
    CLOUD_TOKEN_EXPIRE,
    CLOUD_CURL_FAIL,
    CLOUD_KEY_GENERATE_FAIL,
    NOTIFY_TO_CLOUD_FAIL,
    CLOUD_NOT_INIT,
    CLOUD_CMD_MAX
} cloudReturnCode_e;

/*This enum defines different type of SDC return code*/
typedef enum {
   SDC_INIT_ERR = CLOUD_CMD_MAX,	//62
   SDC_ALREADY_DEINIT_ERR,
   SDC_ALREADY_INIT_ERR,
   SDC_DEINIT_ERR,
   SDC_CMD_MAX
} sdcErrorCode_e;

/*This enum defines different type of OTA return code*/
typedef enum {
	OTA_CLEANUP_FAIL = SDC_CMD_MAX,	//66
	OTA_RESTART_APP,
	CHECK_OTA_FAIL,
	OTA_UPGRADE_FAIL,
	OTA_UNAVAILABLE,
	OTA_CMD_MAX			//71
} otaErrorCode_e;

/*This enum defines different type of configuration return code*/
typedef enum {
    CONFIG_NO_HALO_SN_ATTACHED = OTA_CMD_MAX,  //71
    CONFIG_HALO_SN_ATTACHED,
    CONFIG_TIRE_POS_ATTACHED,
    CONFIG_HALO_SN_FAIL,
    CONFIG_TYRE_DETAIL_FAIL,
    CONFIG_NO_TYRE_DETAIL_AVAILABLE,
    CONFIG_NO_TREAD_DEPTH_HISTORY_AVAILABLE,
    CONFIG_INVALID_TYRE_POSITION,
    CONFIG_INVALID_TYRE_DETAILS,
    CONFIG_CMD_MAX_NEW
} newConfigurationReturnCode_e;

/****************************************
 ******** GLOBAL VARIABLES **************
 ****************************************/
typedef uint32_t returnCode_e;

/****************************************
 ******** EXTERN VARIABLES **************
 ****************************************/

/****************************************
 ************* CALLBACKS ****************
 ****************************************/

/****************************************
 ********* FUNCTION PROTOTYPES **********
 ****************************************/
/******************************************************************
 *@brief (This API is used to print error description)
 *
 *@param[IN] returnCode_e (Indicates type of error)
 *@param[IN] void* (Indicates pointer for other argument)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error)
 *********************************************************************/
returnCode_e printErr(returnCode_e , void *);

/******************************************************************
 *@brief (This API is used to convert the return code into string)
 *
 *@param[IN] returnCode_e (Indicates type of error)
 *@param[OUT] None
 *
 *@return const char * (It returns string of error code)
 *********************************************************************/
const char * convertReturnCodetoStr(returnCode_e type);

void printLocalTime();
#endif      //ifndef _ERROR_H_
