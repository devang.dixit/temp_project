/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : error.c
 * @brief : This file is used for printing error messages
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Aug/14/2018, VT , First Draft
***************************************************************/

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include "error.h"
#include "debug.h"
#include <sys/time.h>


/****************************************
 ******** STATIC VARIABLES **************
 ****************************************/

/****************************************
 ********* FUNCTION DEFINATION **********
 ****************************************/
/*This array of string is used to print error description*/
static const char * returnCode[ ] = {
    [GEN_SUCCESS] = "GEN_SUCCESS",
    [GEN_NULL_POINTING_ERROR] = "GEN_NULL_POINTING_ERROR",
    [GEN_INVALID_TYPE] = "GEN_INVALID_TYPE",
    [GEN_MALLOC_ERROR] = "GEN_MALLOC_ERROR",
    [GEN_NOT_INIT] = "GEN_NOT_INIT",
    [GEN_INVALID_ARG] = "GEN_INVALID_ARG",
    [GEN_SYS_TIME_NOT_AVAIL] = "GEN_SYS_TIME_NOT_AVAIL",
    [GEN_API_NOT_AVAIL] = "GEN_API_NOT_AVAIL",
    [FILE_NOT_FOUND] = "FILE_NOT_FOUND",
    [FILE_OPEN_FAIL] = "FILE_OPEN_FAIL",
    [FILE_READ_ERROR] = "FILE_READ_ERROR",
    [FILE_WRITE_ERROR] = "FILE_WRITE_ERROR",
    [FILE_HANDLE_INVALID] = "FILE_HANDLE_INVALID",
    [FILE_NONBLOCK_FAIL] = "FILE_NONBLOCK_FAIL",
    [FILE_CONTROL_SET_FAIL] = "FILE_CONTROL_SET_FAIL",
    [FILE_CONTROL_GET_FAIL] = "FILE_CONTROL_GET_FAIL",
    [FILE_DELETE_ERROR] = "FILE_DELETE_ERROR",
    [FILE_CREATE_ERROR] = "FILE_CREATE_ERROR",
    [DATA_CRC_FAIL] = "DATA_CRC_FAIL",
    [DATA_NOT_SUFFICIENT] = "DATA_NOT_SUFFICIENT",
    [DATA_SEND_FAIL] = "DATA_SEND_FAIL",
    [DATA_PTR_INVALID] = "DATA_PTR_INVALID",
    [DATA_INVALID] = "DATA_INVALID",
    [FUNC_INIT_FAIL] = "FUNC_INIT_FAIL",
    [MODULE_INIT_FAIL] = "MODULE_INIT_FAIL",
    [MODULE_DEINIT_FAIL] = "MODULE_DEINIT_FAIL",
    [MISC_NO_PENDING_EVENT] = "MISC_NO_PENDING_EVENT",
    [MISC_USER_INDICATION_ERROR] = "MISC_USER_INDICATION_ERROR",
    [DIR_CREATE_FAIL] = "DIR_CREATE_FAIL",
    [DIR_OPEN_FAIL] = "DIR_OPEN_FAIL",
    [CONFIG_WHEEL_PAIRED] = "CONFIG_WHEEL_PAIRED",
    [CONFIG_SENSOR_ID_PAIRED] = "CONFIG_SENSOR_ID_PAIRED",
    [CONFIG_TPMS_FAIL] = "CONFIG_TPMS_FAIL",
    [CONFIG_NO_TPMS_PAIRED] = "CONFIG_NO_TPMS_PAIRED",
    [CONFIG_NO_MANDATORY_INFO] = "CONFIG_NO_MANDATORY_INFO",
    [RTC_IOCTL_FAILED] = "RTC_IOCTL_FAILED",
    [RTC_SYSSET_FAILED] = "RTC_SYSSET_FAILED",
    [RTC_SYSGET_FAILED] = "RTC_SYSGET_FAILED",
    [DM_DEV_ADD_ERROR] = "DM_DEV_ADD_ERROR",
    [DM_DEV_REMOVE_ERR] = "DM_DEV_REMOVE_ERR",
    [DM_INTF_INIT_FAIL] = "DM_INTF_INIT_FAIL",
    [APP_MAX_RETURN_CODE] = "APP_MAX_RETURN_CODE",
    [DIAG_NO_TPMS_PAIRED] = "DIAG_NO_TPMS_PAIRED",
    [DIAG_NO_TPMS_DATA] = "DIAG_NO_TPMS_DATA",
    [CLOUD_CONN_TIMEOUT] = "CLOUD_CONN_TIMEOUT",
    [CLOUD_TOKEN_EXPIRE] = "CLOUD_TOKEN_EXPIRE",
    [CLOUD_CURL_FAIL] = "CLOUD_CURL_FAIL",
    [CONFIG_NO_HALO_SN_ATTACHED] = "CONFIG_NO_HALO_SN_ATTACHED",
    [CONFIG_HALO_SN_ATTACHED] = "CONFIG_HALO_SN_ATTACHED",
    [CONFIG_TIRE_POS_ATTACHED] = "CONFIG_TIRE_POS_ATTACHED",
    [CONFIG_HALO_SN_FAIL] = "CONFIG_HALO_SN_FAIL",
    [CONFIG_TYRE_DETAIL_FAIL] = "CONFIG_TYRE_DETAIL_FAIL",
    [CONFIG_NO_TYRE_DETAIL_AVAILABLE] = "CONFIG_NO_TYRE_DETAIL_AVAILABLE",
    [CONFIG_NO_TREAD_DEPTH_HISTORY_AVAILABLE] = "CONFIG_NO_TREAD_DEPTH_HISTORY_AVAILABLE",
    [CONFIG_INVALID_TYRE_POSITION] = "CONFIG_INVALID_TYRE_POSITION",
    [CONFIG_INVALID_TYRE_DETAILS] = "CONFIG_INVALID_TYRE_DETAILS",
};

returnCode_e printErr(returnCode_e type, void * arg)
{
    if(arg != NULL) {
        CAB_DBG(CAB_GW_ERR, "%s : %s", returnCode[type], (char*)arg);
    } else {
        CAB_DBG(CAB_GW_ERR, "%s", returnCode[type]);
    }
    return GEN_SUCCESS;
}
void printLocalTime()
{
	time_t rawtime;
	struct tm * timeinfo;
	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	CAB_DBG(CAB_GW_DEBUG, "\r\n[%s]:", strtok(asctime(timeinfo), "\n") );
	fflush(stdout);
}
const char * convertReturnCodetoStr(returnCode_e type)
{
    if(type < APP_MAX_RETURN_CODE) {
        return returnCode[type];
    } else {
        return NULL;
    }
}
