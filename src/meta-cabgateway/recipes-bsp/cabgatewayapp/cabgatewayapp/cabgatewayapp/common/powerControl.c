/***************************************************************
 * Copyright(c) <2020>, Volansys Technologies
 *
 * Description:
 * @file : powerControl.c
 * @brief : This file is used for power related functions.
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Jan/18/2020, VT , First Draft
***************************************************************/

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include "powerControl.h"

/****************************************
 ********* FUNCTION DEFINITION **********
 ****************************************/
void rebootSystem( void )
{
	FILE *fp;
        time_t tm;
	sleep(3);
	system("/cabApp/ble/resetble.sh");
	CAB_DBG(CAB_GW_INFO, "Merging the SYSLOG from RAM");
	if (getAndMergeLogsFromRAM() == GEN_SUCCESS) {
		CAB_DBG(CAB_GW_INFO, "Merged the SYSLOG successfully");
	}
	else {
		CAB_DBG(CAB_GW_ERR, "Merging the SYSLOG failed");
	}
#ifdef SOFT_POWER_OFF_EN
	fp = fopen(SOFT_PWR_OFF_FILE, "w");
	if(fp == NULL) {
		CAB_DBG(CAB_GW_WARNING,"Unable To Open soft power off file");
	}
        time(&tm);
	fprintf(fp, "%d", tm);
	fclose(fp);
#endif
	indicateSysStatus(ERROR);
	sleep(120);
	sync();
	powerOffEG91();
	system("reboot");
	sleep(60);
}
