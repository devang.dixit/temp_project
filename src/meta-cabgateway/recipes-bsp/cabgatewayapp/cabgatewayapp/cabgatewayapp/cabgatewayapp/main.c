/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file config_mod_test.c
 * @brief (This file contains the main application
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * July/19/2018, VT , First Draft
***************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "debug.h"
#include "monitorAndTelemetry.h"
#include "otaEnv.h"
#include "otaModule.h"
#include "userInterface.h"
#include "diag.h"
#include "sysLogStorage.h"


int main()
{
    FUNC_ENTRY
    returnCode_e retCode = GEN_SUCCESS;
    /*reset boot count for full update - Update in U-boot env*/
    //system("fw_setenv bootcount 0");
    //system("fw_setenv upgrade_available 0");

    system("chmod -R 777 /cabApp/can/canfw; chmod -R 777 /cabApp/cc1310.out; bash /cabApp/can/canfw /cabApp/cc1310.out");


    retCode = rDiagInit();
    if (retCode != GEN_SUCCESS) {
    	/*Simple printf as diag init failed*/
    	printf("[%s]:[%d] Remote diag init failed\r\n", basename(__FILE__), __LINE__);
    	return retCode;
    }

    retCode = rDiagModInit((rDiagModuleResHandlerCB)rDiagModuleResHandlerCallback);
    if (retCode != GEN_SUCCESS) {
	/*Simple printf as diag init failed*/
	printf("[%s]:[%d] Remote diag module init failed\r\n", basename(__FILE__), __LINE__);
	return retCode;
    }

    retCode = initSysLogModule(MAX_SYSLOG_PACKAGE);
    if (retCode != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "SYS log storage init failed with code %d", retCode);
    } 

    retCode = diagInit();
    if(retCode != GEN_SUCCESS) {
	CAB_DBG(CAB_GW_ERR, "Error in diag init");
	return retCode;
    }

    /*reset boot count for incremental update - Update in otaEnv.txt*/
    retCode = resetBootCount();
    if(retCode != GEN_SUCCESS){
	//print error message and continue to boot application
	CAB_DBG(CAB_GW_ERR, "Error resetting boot count in otaEnv file");
    }

    /* if file does not exist, extract the package */
    if( access("/cabApp/ble/bleAvailable.txt", F_OK) != 0 ) {
        CAB_DBG(CAB_GW_INFO,"Extracting nrfutil packages for BLE");
        system("tar xvzf /cabApp/ble/blePackage.tar.gz -C /usr/");
        system("sync");
        system("touch /cabApp/ble/bleAvailable.txt");
        system("sync");
	CAB_DBG(CAB_GW_INFO, "NRF Utils installed successfully");
    }
    
    if ( access("/media/userdata/bleFlashStatus", F_OK) != 0 ) {
	retCode = flash_ble_fw();
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "BLE Initial FW flash failed");
		indicateSysStatus(ERROR);
		return 0;
	}
        system("touch /media/userdata/bleFlashStatus");
        system("sync");
        CAB_DBG(CAB_GW_INFO, "BLE fw flashed first time after manufacturing flash.");
    }

    if ( access("/media/userdata/ebyteFlashStatus", F_OK) != 0) {
        retCode = flash_ebyte_fw();
        if (retCode != GEN_SUCCESS) {
            CAB_DBG(CAB_GW_ERR, "EBYTE Initial FW flash failed");
            indicateSysStatus(ERROR);
            return 0;
        }
        system("touch /media/userdata/ebyteFlashStatus");
        system("sync");
        CAB_DBG(CAB_GW_INFO, "EBYTE fw flashed first time after manufacturing flash.");
    }

    if( (access("/cabApp/ble/bleAvailable.txt", F_OK) == 0)  && 
        (access("/media/userdata/bleFlashStatus", F_OK) == 0) &&
        (access("/media/userdata/ebyteFlashStatus", F_OK) == 0) ) {
        CAB_DBG(CAB_GW_INFO, "=== CAB App Started ===");
        retCode = monTelemetryInit();
        if (retCode != GEN_SUCCESS) {
    	    monTelemetryDeinit();
    	    rDiagModDeinit();
    	    rdiagDeinit();
        }
        CAB_DBG(CAB_GW_INFO, "=== CAB App Stopped ===");
        FUNC_EXIT
    }
    return 0;
}
