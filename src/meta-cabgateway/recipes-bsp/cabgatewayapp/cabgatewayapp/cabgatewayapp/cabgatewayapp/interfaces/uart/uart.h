/*******************************************************************************
 * Copyright(c) <2016>, Volansys Technologies
 *
 * Description: 
 * @file uart.h
 * @brief  Header file for uart communication between M4 and A7
 * 
 * @Author   - Volansys
 *
 *******************************************************************************
 *
 * @History
 *
 * Mar/21/2017 VT Created 
 *
 ******************************************************************************/
#ifndef _UART_H
#define _UART_H

/**************************
 * 		INCLUDES
 *************************/
#include <stdbool.h>
#include <stdint.h>
#include <sys/signal.h>

/**************************
 * 		MACROS
 *************************/
#define	MAX_SENSORS	40
#define	SENSOR_ID_SIZE	4

#define START_OF_FRAME		0x55AA

#define	UART_SOF_BYTES		2
#define UART_COMMAND_BYTES	2
#define UART_PAYLOAD_LENGTH_BYTES	2
#define	UART_HEADER_BYTES	(UART_COMMAND_BYTES + UART_PAYLOAD_LENGTH_BYTES)
#define	UART_CRC_BYTES		2

#define MINIMUM_FRAME_SIZE	( UART_SOF_BYTES + UART_HEADER_BYTES )
#define FRAME_SIZE			(MINIMUM_FRAME_SIZE) + (MAX_SENSORS * SENSOR_ID_SIZE) + (UART_CRC_BYTES)

#define MAX_RETRY	3

/********************************************************
|	StartOfFrame:	2bytes,	[0x55, 0xaa]
|	Header:			3 bytes,[command, lengthOfPayload(2byte)]
|	Payload:		DATA + CRC[2 bytes]
*********************************************************/

/**************************
 *		Enumerations
 *************************/
typedef enum {
	NORMAL_POWER_MODE = 0 ,
	REMOVE_SENSOR ,
    NOTIFY_RF ,
	STOP_RF ,
    START_RF ,
    ADD_SENSOR ,
	SEND_SENSOR_DATA ,
	LOW_POWER_MODE ,
	MAX_COMMANDS
} UART_COMMAND_e;

typedef enum {
    UART_ACK = 0,
    UART_NACK
}UART_ERROR_TYPE_e;

typedef enum {
	UART_SOF_STATE = 0,
	UART_COMMAND_STATE = 2,
	UART_PAYLOAD_LENGTH_STATE = 4,
	UART_PAYLOAD_STATE = 6,
	UART_CRC_STATE
} UART_STATE_e;

/*************************
 *		STRUCTURES
 ************************/
typedef struct {
	uint16_t startofFrame;
	uint16_t command;
	uint16_t payloadLength;
	uint8_t payload[FRAME_SIZE];
	uint16_t crc;
} UART_FRAME_t;

int8_t uartWrite(const int uartfd, const uint16_t command, const uint8_t *const payload, 
						const uint16_t payloadLength);

int8_t uartRead(const int uartfd, UART_FRAME_t *readFrame);
#if 0
/****************************
 *		FUNCTIONS
 ***************************/
/** Initialize Uart
 *
 * This function will initialize UART port. 
 *
 * @param[in]   void
 * @return      GEN_SUCCESS         successfully initialized UART
 *              FAILURE         failed to initialize UART
 */
int UARTInit(void);

/** DeInitialize UART
 *
 * This function will deinitialze UART port
 *
 * @param[in]   void
 * @return      void
 */
void UARTDeInit(void);

/** Read data from UART
 *
 * This function is a wrapper function that
 * will read data from UART port.
 *
 * @param[out]   frame      pointer to UART_FRAME_t
 * @return      GEN_SUCCESS     read success    
 *              FAILURE     CRC failed/Invalid Header/Timeout
 */

int8_t read_wrapper(UART_FRAME_t *frame);

/** Write data to UART
 *
 * This function is a wrapper function that
 * will write data to UART port.
 *
 * @param[in]   command         1st byte of header
 * @param[in]   data_buffer     data used to send in frame
 * @param[in]   length          length of data
 * @return      ret             number of bytes written
 *              FAILURE         failed to write all bytes.
 */
int8_t write_wrapper(uint8_t command, uint8_t *data_buffer, uint16_t length);

/** Set configuration of M4
 *
 * This function will set configuration parameters that are used 
 * at M4 side.
 *
 * @param[in]   paramType           parameter type
 * @param[in]   paramValue          parameter value
 * @param[in]   lengthOfparamValue  length of parameter value
 * @return      GEN_SUCCESS             configuration parameter set successfully
 *              FAILURE             failed to set configuration parameter
 */
int8_t setM4Configuration(uint16_t paramType, uint8_t *paramValue, uint16_t lengthOfparamValue);

/** Get configuration of M4
 *
 * This function will get configuration parameters that are used 
 * at M4 side.
 *
 * @param[in]   paramType           parameter type
 * @param[out]  paramValue          parameter value
 * @param[out]  lengthOfparamValue  length of parameter value
 * @return      GEN_SUCCESS             configuration parameter get successfully
 *              FAILURE             failed to get configuration parameter
 */
int8_t getM4Configuration(uint16_t paramType, uint8_t *paramValue, uint16_t *lengthOfparamValue);

void UARTInterruptHandler(int sigNum, siginfo_t * sigInfo, void * unused);
#endif
#endif	/* end _UART_H */
