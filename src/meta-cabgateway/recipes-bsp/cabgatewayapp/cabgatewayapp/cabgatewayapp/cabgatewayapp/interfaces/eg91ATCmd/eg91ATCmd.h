#ifndef __EG91ATCMD_H__
#define __EG91ATCMD_H__

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>   /* File Control Definitions */
#include <termios.h> /* POSIX Terminal Control Definitions */
#include <unistd.h>  /* UNIX Standard Definitions */
#include <errno.h>   /* ERROR Number Definitions */
#include <signal.h>
#include <sys/types.h>
#include "error.h"
#include <unistd.h>
#include <pthread.h>
#include <netdb.h>

/****************************************
 ************ DEFINES ******************
 ****************************************/
#define EG91_PWR_KEY_GPIO	(49)
#define EG91_RESET_SWITCH	(48)
#define EG91_LDO_ENABLE		(35)
#define EG91_STATUS_PIN			(51)
#define RETRY_COUNT 		(20)
#define MAX_EG91_RESET_RETRY_COUNT       (3)
#define EG91_USB_PORT 		"/dev/ttyUSB2"
#define MAX_IMEI_LEN 		(15)
#define EG91_PWRON_RETRY_CNT 		(5)
#define EG91_PWRON_RETRY_SLEEP 		(20)
#define EG91_PWROFF_RETRY_CNT 		(3)
#define EG91_PWROFF_RETRY_SLEEP 	(5)

/****************************************
 ************* FUNCTIONS ****************
 ****************************************/

/*********************************************************************
 *@brief          : Init EG91 module
 *
 *@param[IN]      : None
 *@param[OUT]     : None
 *
 *@return         : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e initEG91();

/*********************************************************************
 *@brief          : deInit EG91 module
 *
 *@param[IN]      : None
 *@param[OUT]     : None
 *
 *@return         : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e deInitEG91();

/*********************************************************************
 *@brief          : Send AT commands and receive response
 *
 *@param[IN]      : writeBuff - AT command to write
 *					len - length of AT command to write
 *					pattern - Expected pattern to receive in response
 *@param[OUT]     : readBuff - Buffer to receive the response
 *					readBytes - number of received bytes
 *
 *@return         : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e ATTransactEG91(char *writeBuff, size_t len,  char *pattern, char *readBuff, int *readBytes);

/*********************************************************************
 *@brief          : Send AT commands and receive response
 *
 *@param[IN]      : timeout - time to wait for response
 *@param[IN]      : writeBuff - AT command to write
 *					len - length of AT command to write
 *					pattern - Expected pattern to receive in response
 *@param[OUT]     : readBuff - Buffer to receive the response
 *					readBytes - number of received bytes
 *
 *@return         : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e ATTransactEG91_CustomSleep(char *writeBuff, size_t len,  char *pattern, char *readBuff, int *readBytes, uint8_t timeout);

/******************************************************************** 
* @brief API to get reset required state of EG91 
*
*	This function provides API to access reset required 
*	state of EG91 module.
*
* @return 	: reset required status of EG91
		  1: reset required
		  0: No reset required
********************************************************************/
uint8_t getEG91ResetReqdState();

/********************************************************************
* @brief API to power on EG91 module and relevant GPIO initialization
*
* @param[IN]      : None
* @param[OUT]     : None
*
* @return         : GEN_SUCCESS on success
					Error on failure
********************************************************************/
returnCode_e powerOnEG91();

/********************************************************************
* @brief API to power off EG91 module and relevant GPIO
*	de-initialization
*
* @param[IN]      : None
* @param[OUT]     : None
*
* @return         : GEN_SUCCESS on success
					Error on failure
********************************************************************/
returnCode_e powerOffEG91();

#endif // __EG91ATCMD_H__
