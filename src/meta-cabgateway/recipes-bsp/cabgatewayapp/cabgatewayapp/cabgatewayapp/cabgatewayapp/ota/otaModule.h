#ifndef __OTA_MODULE_H__
#define __OTA_MODULE_H__

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>   /* File Control Definitions */
#include <termios.h> /* POSIX Terminal Control Definitions */
#include <unistd.h>  /* UNIX Standard Definitions */
#include <errno.h>   /* ERROR Number Definitions */
#include <signal.h>
#include <sys/types.h>
#include "error.h"
#include <unistd.h>
#include <pthread.h>
#include <netdb.h>
#include "cloud.h"
#include "otaEnv.h"

/****************************************
 ************ DEFINES ******************
 ****************************************/
#define OTA_DIR_PATH "/cabApp/ota"
#define OTA_FILE_PATH "/cabApp/ota/otaInfo.txt"
/*#define OTA_STATUS_FILE_PATH	"/cabApp/ota/otaStatus.txt"*/
#define OTA_FILE_NAME "otaInfo.txt"
#define BLE_RETRY_COUNT	3

/****************************************
 ************ structures ******************
 ****************************************/
typedef enum otaState {
	DOWNLOAD_COMPLETE,
	UPGRADE_PROGRESS,
	UPGRADE_COMPLETE,
	MAX_OTA_STATE,
} otaState_e;

typedef struct otaDetails {
	otaInfo_t otaInfo;
	otaState_e state;
} otaDetails_t;
#if 0
typedef struct otaUpdateStatus {
	uint8_t zb_updated;
	uint8_t ble_updated;
	uint8_t can_updated;
	uint8_t rf_updated;
	uint8_t full_update;
} otaUpdateStatus_t;
#endif
/****************************************
 ************* FUNCTIONS ****************
 ****************************************/

/*********************************************************************
 *@brief          : check if OTA is available
 *
 *@param[IN]      : None
 *@param[OUT]     : None
 *
 *@return         : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e checkOTA(char *swVersion, otaDetails_t *otaDetails, uint8_t *isOtaAvailable);

/*********************************************************************
 *@brief          : download OTA from cloud
 *
 *@param[IN]      : None
 *@param[OUT]     : None
 *
 *@return         : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e downloadOTA(otaDetails_t *latestOTADetails, uint8_t *isDownloadCompleted);

/*********************************************************************
 *@brief          : Perform OTA for host CPU, BLE and Zigbee
 *
 *@param[IN]      : None
 *@param[OUT]     : None
 *
 *@return         : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e processOTA();

/*********************************************************************
 *@brief          : Roll back OTA components if OTA perform failed
 *
 *@param[IN]      : None
 *@param[OUT]     : otaEnv_t
 *
 *@return         : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e  rollBackFirmwareComponents(otaEnv_t *otaEnv);
/*********************************************************************
 *@brief          : Extract OTA status information - Which componets updated
 *		    in last OTA process
 *
 *@param[IN]      : None
 *@param[OUT]     : otaEnv_t
 *
 *@return         : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e  extractOtaStatus(otaEnv_t *otaEnv);

/*********************************************************************
 *@brief          : Flash BLE firmware
 *
 *@param[IN]      : None
 *
 *@return         : GEN_SUCCESS on success
					Error on failure
 *********************************************************************/
returnCode_e flash_ble_fw(void);

#endif // __OTA_MODULE_H__
