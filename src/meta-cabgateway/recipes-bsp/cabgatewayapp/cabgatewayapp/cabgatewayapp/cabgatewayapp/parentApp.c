/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file config_mod_test.c
 * @brief (This file contains the main application
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * July/19/2018, VT , First Draft
***************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include <stdbool.h>
#include "error.h"
#include "debug.h"
#include "gpio.h"
#include "batModule.h"
#include "commonData.h"
#include "configuration.h"
#include "sysLogStorage.h"
#include "powerControl.h"
#include "memory.h"

#define APP_SELECTION_PIN (66)
#define MAX_MEM_LEAK_LAUNCHER   32000   /* Max 32MB memory usage is allowed */

#define HYST_PERCENTAGE (5) /* Add 5 percentage of hyst band for allowing parent application */

uint8_t bleResetDone = 1;
static pthread_t memMonitorThreadID_t;
static bool memoryOutRun = false;
static bool deinitIndicated = false;

//*****************************************************************************
//  memMonitor()
//  Param:
//      IN :    void *
//      OUT:    NONE
//  Returns:
//              NONE
//  Description:
//      This function keeps watch on memory usage of the cabAppLauncher
//      application. Restarts the system if it exceeds 32 MB.
//
//  [Pre-condition:]
//      None
//  [Constraints:]
//      None
//
//*****************************************************************************
static void *memMonitor(void *arg)
{
    uint32_t temp=0;

    while(!deinitIndicated) {
        checkSelfProcMem(&temp);
        if (temp > MAX_MEM_LEAK_LAUNCHER) {
            memoryOutRun = true;
            CAB_DBG(CAB_GW_ERR, "Memory leak in cabAppLauncher, rebooting system");
            rebootSystem();
        }
        /* Sleep for 1 minutes */
        sleep(60);
    }

    pthread_detach(pthread_self());
    pthread_exit(0);
    return NULL;
}

void notificationHandler(void * data)
{
    FUNC_ENTRY

    CAB_DBG(CAB_GW_TRACE, "In notification handler... data : %d", *(powerSwitchEvent_e*)data);

    FUNC_EXIT
}

void cfgChangeCB(void *data)
{
    FUNC_ENTRY
    CAB_DBG(CAB_GW_TRACE, "In CFG change CB");
    FUNC_EXIT
}
 
static void indicateIdleStatusOnLed(void)
{
    FUNC_ENTRY
    initUIModule(NULL);
    sleep(5);
    indicateSysStatus(ALERT);
    sleep(5);
    deInitUIModule(); 
    FUNC_EXIT
}

static bool shouldLaunchChildApplication(void)
{
    FUNC_ENTRY

    returnCode_e ret;
    powerDevice_e gatewayPowerStatus;
    powerSwitchEvent_e batteryStatus;
    uint8_t lowBatThreshold = 0;

    ret = initConfigModule( (updatedConfigNotifCB)cfgChangeCB );
    if(ret != GEN_SUCCESS) {
           CAB_DBG(CAB_GW_ERR, "initConfigModule failed with %d\r\n", ret);
           goto exitCheck;
    }

    ret = getConfigModuleParam(LOW_BATTERY_THRESHOLD, &lowBatThreshold);
    if (ret != GEN_SUCCESS) {
            CAB_DBG(CAB_GW_ERR, "Get low battery threashold failed with code %d\r\n", ret);
            goto exitCheck;
    }
    CAB_DBG(CAB_GW_INFO,"Bat threashold %d\r\n", lowBatThreshold);

    ret = initBatModule((notifyPowerSwitchEvent)notificationHandler, lowBatThreshold + HYST_PERCENTAGE);
    if(ret != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "Battery module initialization failed in parent application");
        goto exitCheck;
    }
    ret = getPowerSource(&gatewayPowerStatus);
    CAB_DBG(CAB_GW_DEBUG, "Power status : %d\n", gatewayPowerStatus);
    if(ret != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "error in getting power source");
        goto exitCheck;
    }
    if(gatewayPowerStatus == BACKUP_BATTERY) {
        getBatteryHealth(&batteryStatus);
    	CAB_DBG(CAB_GW_DEBUG, "Battery status : %d\n", batteryStatus);
    }
    ret = deInitBatModule();
    if(ret != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "Battery module Deinitialization failed in parent application");
        goto exitCheck;
    }

    ret = deInitConfigModule();
    if (ret != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "Deinit config module failed");
        goto exitCheck;
    }

    if((gatewayPowerStatus == VEHICLE_POWER) || (batteryStatus == NORMAL_BAT)) {
        FUNC_EXIT
        return true;
    }

exitCheck:
    FUNC_EXIT
    return false;
}

void resetBleModule()
{
    system("/cabApp/ble/resetble.sh");
    sleep(25);
    indicateIdleStatusOnLed();
    CAB_DBG(CAB_GW_INFO, "Launching Cab Gateway Application (Take approx. 90 Seconds)..\n");
    sleep(85);
    bleResetDone = 1;
}

returnCode_e getTestMode(int *val)
{
    returnCode_e retCode = GEN_SUCCESS;
	if (val == NULL) {
		return GEN_NULL_POINTING_ERROR;
	}
	gpioInit(APP_SELECTION_PIN, IN);
	retCode = gpioRead(APP_SELECTION_PIN, val);
	if(retCode != GEN_SUCCESS)
		return retCode;

	gpioDeInit(APP_SELECTION_PIN);
	return retCode;
}
int main()
{
    FUNC_ENTRY

    pid_t pid;
    int status = 0;
    bool ret;
    uint8_t limitExceed;
    uint8_t val = 0;
    returnCode_e retCode = GEN_SUCCESS;

    char *cmd1 = "/cabApp/testjigApp";
    char *argv1[2];
    argv1[0] = "/cabApp/testjigApp";
    argv1[1] = NULL;

    char *cmd = "/cabApp/cabAppA7";
    char *argv[2];
    argv[0] = "/cabApp/cabAppA7";
    argv[1] = NULL;


    retCode = getTestMode(&val);
    if(retCode != GEN_SUCCESS)
	    return retCode;

    retCode = initSysLogModule(MAX_SYSLOG_PACKAGE);
    if (retCode != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "SYS log storage init failed with code %d", retCode);
    }

    deinitIndicated = false;
    pthread_create(&memMonitorThreadID_t, NULL, &memMonitor, NULL);

    CAB_DBG(CAB_GW_INFO,"\nStarting Application Launcher Program...\n");
    while(1) {
	    if (val == 1) {
		    if(!bleResetDone){
			    resetBleModule();
		    }
	    } else {
		    CAB_DBG(CAB_GW_INFO, "Launching Test JIG Application ..");
	    }
        if (memoryOutRun)
        {
            /* Wait here till the reboot is not performed */
            sleep(300);
        }
	    if((ret = shouldLaunchChildApplication()) == true) {
		    if((pid = fork()) < 0) {
			    CAB_DBG(CAB_GW_ERR, "Cab Gateway application launching fail fail with error: %s", strerror(errno));
			    return -1;
		    } else if(pid == 0) {
			    if (val == 1) {
				    retCode = incrementBootCount(&limitExceed);
				    if(retCode != GEN_SUCCESS){
					 CAB_DBG(CAB_GW_ERR,"Error incrementing boot count");
					 limitExceed = 0;
				    }
				    if(limitExceed){
					    CAB_DBG(CAB_GW_ERR, "Boot count limit reached to maxium. Rebooting Cab Gateway to boot-up with previous firmware..");
					    CAB_DBG(CAB_GW_INFO, "Resetting BLE module, please wait for 120 seconds...");
					    rebootSystem();
				    } 
				    if(execvp(cmd, argv) < 0) {
					    CAB_DBG(CAB_GW_ERR, "Cab Gateway application launching fail fail with error: %s", strerror(errno));
					    return -1;
				    }
			    } else {
				    if(execvp(cmd1, argv1) < 0) {
					    CAB_DBG(CAB_GW_ERR, "Launching of testjig fail with error %s", \
							    strerror(errno));
					    return -1;
				    }
			    }
		    } else {
			    if(pid == wait(&status)) {
				    CAB_DBG(CAB_GW_ERR, "Cab Gateway Application Terminated.\n");
				    bleResetDone = 0;
			    }
		    }
	    }
	    else{
		    CAB_DBG(CAB_GW_ERR, "Couldn't launch Cab Gateway Application due to low battery !");
	    }
	    sleep(1);
    }

    /* Join the thread before exiting the process. */
    deinitIndicated = true;
    pthread_join(memMonitorThreadID_t, NULL);

    FUNC_EXIT
    return 0;
}
