#include <sys/syscall.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>
#include <stdlib.h>
#include <semaphore.h>
#include <sys/ioctl.h>
#include <time.h>
#include "LteModule.h"
#include "eg91ATCmd.h"
#include "debug.h"
#include "error.h"
#include "watchdog.h"
#include "syscall.h"
#include "utils/sysTimer.h"
#include "monitorAndTelemetry.h"
#include "sysTimer.h"
#include "memory.h"
#include "userInterface.h"
#include "sysLogStorage.h"
#include "powerControl.h"

#define LIVE
#define PERIODIC
#define LTE_CMD_MSGQ_MAX_SIZE 50
#define LTE_STATE_MSGQ_MAX_SIZE 50


#define LTE_CMD_QUEUE_NAME "/lte_cmd_queue"
#define LTE_STATE_QUEUE_NAME "/lte_state_queue"
#define LTE_STATE_QUEUE_NAME_FOR_REMOTE_DIAG_APP "/lte_state_queue_for_remote_diag_app"
#define LTE_CMD_QUEUE_NAME_FOR_REMOTE_DIAG_APP "/lte_cmd_queue_for_remote_diag_app"

#define ONE_MINUTE                      (60)    /* One minute seconds macro */

#define MQ_SEND_LTE_STATUS_TIMEOUT      (5 * ONE_MINUTE)
#define TIMER_RESOLUTION_SEC            (60)
#define WATCHDOG_TIMER_INTERVAL         (46 * ONE_MINUTE)
#define WATCHDOG_TIMER_COUNT            (WATCHDOG_TIMER_INTERVAL/TIMER_RESOLUTION_SEC)

/*Added for the EG91_USB_PORT*/
#define MAX_READ_BUFF_SIZE 512
#define SMS_MSGQ_MAX_SIZE 10
#define LTE_SMS_QUEUE_NAME "/lte_sms_queue"
#define MAX_SIZE_OF_CMD 512
extern int lteFd;
extern pthread_mutex_t lock;
static mqd_t mqLteSMSQueue = -1; 
fd_set sFd;
struct timeval tv;
extern char *semName;
static sem_t* g_lteSmsSem;

static mqd_t mqLteStateQueue = -1;
static mqd_t mqLteCmdQueue = -1;
static mqd_t mqLteStateQueueForRemoteDiagApp = -1;
static mqd_t mqLteCmdQueueForRemoteDiagApp = -1;
static pthread_t g_keepAliveLTE_tid;
static pthread_t g_lteCmdThreadID_t;
static pthread_t g_lteCmdFromRemoteDiagAppThreadID_t;
static pthread_t g_lteSMSThreadID_t;
pid_t g_childPID;
pid_t pppdPID;
static bool g_isCabAppRuninng = false;
static pthread_mutex_t pidLock;
static pthread_mutex_t stateLock;
static pthread_mutex_t cabAppStateLock;


static lteStatus_e g_lteState = MAX_LTE_STATE;
sem_t g_lteSem;
static uint8_t lteInited = 0;
static TIMER_HANDLE watchDogTimerHandle  = INVALID_TIMER_HANDLE;

static pthread_mutex_t keepAliveLteThreadAliveLock = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t lteCmdHandlerThreadAliveLock = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t lteCmdFromRemoteDiagAppThreadAliveLock = PTHREAD_MUTEX_INITIALIZER;

static bool g_keepAliveLteThreadStatus = true;
static bool g_lteCmdHandlerThreadAliveStatus = true;
static bool g_lteCmdFromRemoteDiagAppThreadAliveStatus = true;


static void getPPPD_PID(pid_t *pid)
{
	pthread_mutex_lock(&pidLock);
	*pid = g_childPID;
	pthread_mutex_unlock(&pidLock);
}

static void setPPPD_PID(pid_t pid)
{
	pthread_mutex_lock(&pidLock);
	g_childPID = pid;
	pthread_mutex_unlock(&pidLock);
}

static void getCabAppState(bool *state)
{
	pthread_mutex_lock(&cabAppStateLock);
	*state = g_isCabAppRuninng;
	pthread_mutex_unlock(&cabAppStateLock);
}

static void setCabAppState(bool state)
{
	pthread_mutex_lock(&cabAppStateLock);
	g_isCabAppRuninng = state;
	pthread_mutex_unlock(&cabAppStateLock);
}

static void getLTE_State(lteStatus_e *outState)
{
	pthread_mutex_lock(&stateLock);
	*outState = g_lteState;
	pthread_mutex_unlock(&stateLock);
}

static void setLTE_State(lteStatus_e targetState)
{
	pthread_mutex_lock(&stateLock);
	g_lteState = targetState;
	pthread_mutex_unlock(&stateLock);
}


static returnCode_e connectionToLTE()
{

	returnCode_e retCode = GEN_SUCCESS;
	pid_t pid;
	char *cmd = "pppd";
	char *argv[6];
	argv[0] = "pppd";
	argv[1] = "call";
	argv[2] = "quectel-ppp";
	argv[3] = "-detach";
	argv[4] = "modem";
	argv[5] = NULL;

	FUNC_ENTRY


		if ((pid = fork()) < 0)
		{
			CAB_DBG(CAB_GW_ERR, "Connect() fail with error %s", strerror(errno));
			FUNC_EXIT
				return GEN_SYSCALL_FAIL;
		}
		else if (pid == 0)
		{
			if (execvp(cmd, argv) < 0)
			{
				CAB_DBG(CAB_GW_ERR, "Connect() fail with error %s", strerror(errno));
				FUNC_EXIT
					return GEN_SYSCALL_FAIL;
			}
		}
		else
		{
			signal(SIGCHLD,SIG_IGN); 
			CAB_DBG(CAB_GW_DEBUG, "PPP connected");
			CAB_DBG(CAB_GW_TRACE, "new pid %d", pid);
			setPPPD_PID(pid);
			sem_post(&g_lteSem);
		}

	FUNC_EXIT
		return retCode;
}

static returnCode_e disconnectionFromLTE()
{
	returnCode_e retCode = GEN_SUCCESS;
	FUNC_ENTRY

		retCode = system("killall -9 pppd");
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "disconnect() failed with %d", retCode);
		FUNC_EXIT
			return retCode;
	}

	FUNC_EXIT
		return retCode;
}



static returnCode_e lteCmdQueueSetup()
{
	returnCode_e retCode = GEN_SUCCESS;
	struct mq_attr attr;
	FUNC_ENTRY

		/* initialize the queue attributes */
		attr.mq_flags = 0;
	attr.mq_maxmsg = LTE_CMD_MSGQ_MAX_SIZE;
	attr.mq_msgsize = sizeof(lteCmd_t);
	attr.mq_curmsgs = 0;

	/* Cleanup the msg queue if exist */
	mq_unlink(LTE_CMD_QUEUE_NAME);

	/* create the message queue */
	mqLteCmdQueue = mq_open(LTE_CMD_QUEUE_NAME, O_CREAT | O_RDWR, 0666, &attr);
	if(mqLteCmdQueue < 0)
	{
		CAB_DBG(CAB_GW_ERR, "mq_open for LTE command queue fail with error %s", strerror(errno));
		FUNC_EXIT
			return MQ_OPEN_ERROR;
	}

	FUNC_EXIT
		return retCode;
}

static returnCode_e lteStateQueueSetup()
{
	returnCode_e retCode = GEN_SUCCESS;
	struct mq_attr attr;
	FUNC_ENTRY

		/* initialize the queue attributes */
		attr.mq_flags = 0;
	attr.mq_maxmsg = LTE_STATE_MSGQ_MAX_SIZE;
	attr.mq_msgsize = sizeof(lteState_t);
	attr.mq_curmsgs = 0;

	/* Cleanup the msg queue if exist */
	mq_unlink(LTE_STATE_QUEUE_NAME);

	/* create the message queue */
	mqLteStateQueue = mq_open(LTE_STATE_QUEUE_NAME, O_CREAT | O_WRONLY, 0444, &attr);
	if(mqLteStateQueue < 0)
	{
		CAB_DBG(CAB_GW_ERR, "mq_open for LTE state queue fail with error %s", strerror(errno));
		FUNC_EXIT
			return MQ_OPEN_ERROR;
	}

	FUNC_EXIT
		return retCode;
}

static returnCode_e lteCmdQueueSetupForRemoteDiagApp()
{
	returnCode_e retCode = GEN_SUCCESS;
	FUNC_ENTRY

		mqLteCmdQueueForRemoteDiagApp = mq_open(LTE_CMD_QUEUE_NAME_FOR_REMOTE_DIAG_APP, O_RDWR);
	if(mqLteCmdQueueForRemoteDiagApp < 0)
	{
		CAB_DBG(CAB_GW_ERR, "mq_open for LTE command queue for remote diag app fail with error %s", strerror(errno));
		FUNC_EXIT
			return MQ_OPEN_ERROR;
	}

	FUNC_EXIT
		return retCode;
}

static returnCode_e lteStateQueueSetupForRemoteDiagApp()
{
	returnCode_e retCode = GEN_SUCCESS;
	FUNC_ENTRY

		mqLteStateQueueForRemoteDiagApp = mq_open(LTE_STATE_QUEUE_NAME_FOR_REMOTE_DIAG_APP, O_WRONLY);
	if(mqLteStateQueueForRemoteDiagApp < 0)
	{
		CAB_DBG(CAB_GW_ERR, "mq_open for LTE state queue for remote diag app fail with error %s", strerror(errno));
		FUNC_EXIT
			return MQ_OPEN_ERROR;
	}

	FUNC_EXIT
		return retCode;
}

static returnCode_e lteSMSQueueSetup()
{
	returnCode_e retCode = GEN_SUCCESS;
	struct mq_attr attr;
	FUNC_ENTRY

		/* initialize the queue attributes */
		attr.mq_flags = 0;
	attr.mq_maxmsg = SMS_MSGQ_MAX_SIZE;
	attr.mq_msgsize = sizeof(lteSms_t);
	attr.mq_curmsgs = 0;

	/* create the message queue */
	mqLteSMSQueue = mq_open(LTE_SMS_QUEUE_NAME, O_CREAT | O_WRONLY, 0666, &attr);
	if(mqLteSMSQueue < 0)
	{
		CAB_DBG(CAB_GW_ERR, "mq_open for LTE state queue fail with error %s", strerror(errno));
		FUNC_EXIT
			return MQ_OPEN_ERROR;
	}

	FUNC_EXIT
		return retCode;

}

static returnCode_e setupMsgQueue()
{
	returnCode_e retCode = GEN_SUCCESS;
	FUNC_ENTRY

		retCode = lteCmdQueueSetup();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "lteCmdQueueSetup() fail with error %d", retCode);
		FUNC_EXIT
			return retCode;
	}

	retCode = lteStateQueueSetup();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "lteStateQueueSetup() fail with error %d", retCode);
		FUNC_EXIT
			return retCode;
	}

	retCode = lteCmdQueueSetupForRemoteDiagApp();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "lteCmdQueueSetupForRemoteDiagApp() fail with error %d", retCode);
		FUNC_EXIT
			return retCode;
	}

	retCode = lteStateQueueSetupForRemoteDiagApp();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "lteStateQueueSetupForRemoteDiagApp() fail with error %d", retCode);
		FUNC_EXIT
			return retCode;
	}

	retCode= lteSMSQueueSetup();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "lteStateQueueSetupForRemoteDiagApp() fail with error %d", retCode);
		FUNC_EXIT
			return retCode;

	}

	FUNC_EXIT
		return retCode;
}

static returnCode_e sendLteCmdFromLteApp(lteCmd_e cmd, mqd_t queue)
{
	returnCode_e retCode = GEN_SUCCESS;
	lteCmd_t lteCmdData;
	struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0}; 
	clock_gettime(CLOCK_REALTIME, &abs_timeout);
	abs_timeout.tv_sec += MQ_SEND_LTE_CMD_TIMEOUT;
	FUNC_ENTRY

		memset(&lteCmdData, 0, sizeof(lteCmdData));
	lteCmdData.cmd = cmd;

	CAB_DBG(CAB_GW_DEBUG, "Sending LTE %d command", lteCmdData.cmd);

	if(mq_timedsend(queue, (const char *)&lteCmdData, sizeof(lteCmd_t), 0, &abs_timeout) < 0)
	{   
		CAB_DBG(CAB_GW_ERR, "mq timed send fail for LTE command message queue");
		FUNC_EXIT
			retCode = MQ_SEND_ERROR;
	}   

	FUNC_EXIT
		return retCode;
}

static bool getLteCmdHandlerThreadAliveStatus(void)
{
	bool status;
	pthread_mutex_lock(&lteCmdHandlerThreadAliveLock);
	status = g_lteCmdHandlerThreadAliveStatus;
	pthread_mutex_unlock(&lteCmdHandlerThreadAliveLock);
	return status;
}

static void setLteCmdHandlerThreadAliveStatus(bool value)
{
	pthread_mutex_lock(&lteCmdHandlerThreadAliveLock);
	g_lteCmdHandlerThreadAliveStatus = value;
	pthread_mutex_unlock(&lteCmdHandlerThreadAliveLock);
}

bool lteCmdHandlerCallback(void)
{
	bool status = false;
	returnCode_e retCode = GEN_SUCCESS;
	CAB_DBG(CAB_GW_TRACE, "In func : %s\n", __func__);
	if(getLteCmdHandlerThreadAliveStatus() == true)
	{
		status = true;
	}

	setLteCmdHandlerThreadAliveStatus(false);

	retCode = sendLteCmdFromLteApp(WATCHDOG_LTE_CMD,mqLteCmdQueue);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "Send LTE command failed with error %d", retCode);
		status = false;
	}
	return status;
}

static void *lteCmdHandler(void *arg)
{
	returnCode_e retCode = GEN_SUCCESS;
	ssize_t bytes_read;
	lteCmd_t lteCmdData;
	uint8_t lteState = 0;
	watchDogTimer_t newWatchdogNode;

	FUNC_ENTRY

		newWatchdogNode.threadId = syscall(SYS_gettid);
	newWatchdogNode.CallbackFn = lteCmdHandlerCallback;

	CAB_DBG(CAB_GW_INFO, "LTE command handler thread id : %ld", syscall(SYS_gettid));
	retCode = threadRegister(&newWatchdogNode);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "Lte Command handler thread registration failed with error %d", retCode);
		rDiagSendMessage(WATCHDOG_REGISTRATION_FAIL, GW_ERR, "LTE Command handler thread registration failed");
	}

	while(1)
	{
		memset(&lteCmdData, 0, sizeof(lteCmdData));
		bytes_read = mq_receive(mqLteCmdQueue, (char *)&(lteCmdData.cmd), sizeof(lteCmd_e), NULL);
		if(bytes_read >= 0)
		{
			switch(lteCmdData.cmd)
			{
				case CONNECT:
					getLTEConnStat(&lteState);
					if (lteState == 0)
					{
						disconnectionFromLTE(); // ADDED FOR SSH
						retCode = connectionToLTE();
						if(retCode != GEN_SUCCESS)
						{
							CAB_DBG(CAB_GW_ERR, "Connection to LTE failed with error %d", retCode);
						}
						setCabAppState(true);
					}
					break;
				case DISCONNECT:
					retCode = disconnectionFromLTE();
					if(retCode != GEN_SUCCESS)
					{
						CAB_DBG(CAB_GW_ERR, "Disconnecting LTE failed with error %d", retCode);
					}
					break;
				case DEINIT:
					setCabAppState(false);
					break;
				case WATCHDOG_LTE_CMD:
					{
						setLteCmdHandlerThreadAliveStatus(true);
					}
					break;
				default:
					break;
			}
		}
	}
	retCode = threadDeregister(newWatchdogNode.threadId);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "Lte Command handler thread de-registration failed with error %d", retCode);
		rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "LTE Command handler thread de-registration failed");
	}

	FUNC_EXIT
}

static bool getLteCmdFromRDiagAppThreadAliveStatus(void)
{
	bool status;
	pthread_mutex_lock(&lteCmdFromRemoteDiagAppThreadAliveLock);
	status = g_lteCmdFromRemoteDiagAppThreadAliveStatus;
	pthread_mutex_unlock(&lteCmdFromRemoteDiagAppThreadAliveLock);
	return status;
}

static void setLteCmdFromRDiagAppThreadAliveStatus(bool value)
{
	pthread_mutex_lock(&lteCmdFromRemoteDiagAppThreadAliveLock);
	g_lteCmdFromRemoteDiagAppThreadAliveStatus = value;
	pthread_mutex_unlock(&lteCmdFromRemoteDiagAppThreadAliveLock);
}

bool lteCmdFromRemoteDiagAppCallback(void)
{
	bool status = false;
	returnCode_e retCode = GEN_SUCCESS;
	CAB_DBG(CAB_GW_TRACE, "In func : %s", __func__);
	if(getLteCmdFromRDiagAppThreadAliveStatus() == true)
	{
		status = true;
	}

	setLteCmdFromRDiagAppThreadAliveStatus(false);

	retCode = sendLteCmdFromLteApp(WATCHDOG_LTE_CMD, mqLteCmdQueueForRemoteDiagApp);;
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "Send LTE command failed with error %d", retCode);
		status = false;
	}
	return status;
}

static void *lteCmdFromRemoteDiagAppHandler(void *arg)
{
	returnCode_e retCode = GEN_SUCCESS;
	ssize_t bytes_read;
	lteCmd_t lteCmdData;
	uint8_t lteState = 0;
	watchDogTimer_t newWatchdogNode;

	FUNC_ENTRY

		newWatchdogNode.threadId = syscall(SYS_gettid);
	newWatchdogNode.CallbackFn = lteCmdFromRemoteDiagAppCallback;

	CAB_DBG(CAB_GW_INFO, "LTE command from Remote Diag App handler thread id : %ld", syscall(SYS_gettid));
	retCode = threadRegister(&newWatchdogNode);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "Lte Command From Remote Diag App handler thread registration failed with error %d", retCode);
		rDiagSendMessage(WATCHDOG_REGISTRATION_FAIL, GW_ERR, "LTE Command From Remote Diag App handler thread registration failed");
	}

	while(1)
	{
		memset(&lteCmdData, 0, sizeof(lteCmdData));
		bytes_read = mq_receive(mqLteCmdQueueForRemoteDiagApp, (char *)&(lteCmdData.cmd), sizeof(lteCmd_e), NULL);
		if(bytes_read >= 0)
		{
			switch(lteCmdData.cmd)
			{
				case CONNECT:
					getLTEConnStat(&lteState);
					if (lteState == 0)
					{
						disconnectionFromLTE(); // ADDED FOR SSH
						retCode = connectionToLTE();
						if(retCode != GEN_SUCCESS)
						{
							CAB_DBG(CAB_GW_ERR, "Connection to LTE failed with error %d", retCode);
						}
						setCabAppState(true);
					}
					break;
				case DISCONNECT:
					retCode = disconnectionFromLTE();
					if(retCode != GEN_SUCCESS)
					{
						CAB_DBG(CAB_GW_ERR, "Disconnecting LTE failed with error %d", retCode);
					}
					break;
				case WATCHDOG_LTE_CMD:
					{
						setLteCmdFromRDiagAppThreadAliveStatus(true);
					}
					break;
				default:
					break;
			}
		}
	}
	retCode = threadDeregister(newWatchdogNode.threadId);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "Lte Command From Remote Diag App handler thread de-registration failed with error %d", retCode);
		rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "LTE Command From Remote Diag App handler thread de-registration failed");
	}
	FUNC_EXIT
}

static returnCode_e sendLteStateToCabApp(lteStatus_e status)
{
	returnCode_e retCode = GEN_SUCCESS;
	lteState_t lteStateData;
	struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0};
	bool cabAppState;
	FUNC_ENTRY

		clock_gettime(CLOCK_REALTIME, &abs_timeout);
	abs_timeout.tv_sec += MQ_SEND_LTE_STATUS_TIMEOUT;

	getCabAppState(&cabAppState);
	if(cabAppState == true)
	{
		memset(&lteStateData, 0, sizeof(lteStateData));
		lteStateData.state = status;

		CAB_DBG(CAB_GW_DEBUG, "Sending LTE status : %s to Cab App", (lteStateData.state == CONNECTED_LTE_STATE) ? "CONNECTED" : "DISCONNECTED" );

		if(mq_timedsend(mqLteStateQueue, (const char *)&lteStateData, sizeof(lteState_t), 0, &abs_timeout) < 0)
		{
			CAB_DBG(CAB_GW_ERR, "mq timed send fail for LTE state message queue");
			retCode = MQ_SEND_ERROR;
		}
	}

	FUNC_EXIT
		return retCode;
}

static returnCode_e sendLteStateToRemoteDiagApp(lteStatus_e status)
{
	returnCode_e retCode = GEN_SUCCESS;
	lteState_t lteStateData;
	struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0};
	FUNC_ENTRY

		clock_gettime(CLOCK_REALTIME, &abs_timeout);
	abs_timeout.tv_sec += MQ_SEND_LTE_STATUS_TIMEOUT;

	memset(&lteStateData, 0, sizeof(lteStateData));
	lteStateData.state = status;

	CAB_DBG(CAB_GW_DEBUG, "Sending LTE status : %s to remote diag app\r\n", (lteStateData.state == CONNECTED_LTE_STATE) ? "CONNECTED" : "DISCONNECTED" );

	if(mq_timedsend(mqLteStateQueueForRemoteDiagApp, (const char *)&lteStateData, sizeof(lteState_t), 0, &abs_timeout) < 0)
	{
		CAB_DBG(CAB_GW_ERR, "mq timed send fail for LTE state message queue for remote diag app");
		retCode = MQ_SEND_ERROR;
	}

	FUNC_EXIT
		return retCode;
}

static bool getKeepAliveLteThreadAliveStatus(void)
{
	bool status;
	pthread_mutex_lock(&keepAliveLteThreadAliveLock);
	status = g_keepAliveLteThreadStatus;
	pthread_mutex_unlock(&keepAliveLteThreadAliveLock);
	return status;
}

static void setKeepAliveLteThreadAliveStatus(bool value)
{
	pthread_mutex_lock(&keepAliveLteThreadAliveLock);
	g_keepAliveLteThreadStatus = value;
	pthread_mutex_unlock(&keepAliveLteThreadAliveLock);
}

bool keepAliveLTECallback(void)
{
	bool status = false;
	lteStatus_e tempState = MAX_LTE_STATE;
	uint8_t connectivity = 0;
	static uint8_t count = 0;

	CAB_DBG(CAB_GW_TRACE, "In func : %s", __func__);
	if(getKeepAliveLteThreadAliveStatus() == true)
	{
		status = true;
	}

	getLTE_State(&tempState);
	getLTEConnStat(&connectivity);
	// If state is connected and getting LTE connectivity than return true else return false
	if ( (tempState == CONNECTED_LTE_STATE) && (connectivity == 1))
	{
		setKeepAliveLteThreadAliveStatus(true);
		count = 0;
	} else if(count == 2) {
		setKeepAliveLteThreadAliveStatus(false);
	} else {
		count++;
	}

	return status;
}

static void * keepAliveLTE(void *param)
{
	int status = 0;
	int killRet = 0;
	uint8_t connStatus = 0;
	pid_t returnPID = 0;
	pid_t globalPID = 0;
	uint16_t loopCount = 0;
	returnCode_e retCode = GEN_SUCCESS;
	lteStatus_e tempState = MAX_LTE_STATE;
	watchDogTimer_t newWatchdogNode;

	FUNC_ENTRY

		newWatchdogNode.threadId = syscall(SYS_gettid);
	newWatchdogNode.CallbackFn = keepAliveLTECallback;

	CAB_DBG(CAB_GW_INFO, "Keep Alive LTE thread id : %ld", syscall(SYS_gettid));
	retCode = threadRegister(&newWatchdogNode);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "Keep Alive LTE thread registration failed with error %d", retCode);
		rDiagSendMessage(WATCHDOG_REGISTRATION_FAIL, GW_ERR, "Keep Alive LTE thread registration failed");
	}

	setLTE_State(DISCONNECTED_LTE_STATE);

	pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
	while (1) {
		getLTE_State(&tempState);
		switch (tempState)
		{
			case DISCONNECTED_LTE_STATE:
				CAB_DBG(CAB_GW_DEBUG, "Waiting for connection req");
				pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
				sem_wait(&g_lteSem);
				pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
				loopCount = 1;
				while (1) {
					CAB_DBG(CAB_GW_DEBUG, "Checking LTE connection...");
					pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
					sleep(CONN_CHECK_MAX_TRY);
					pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
					getLTEConnStat(&connStatus);
					if (connStatus == 1) {
						CAB_DBG(CAB_GW_DEBUG, "connection establish");
						setLTE_State(CONNECTED_LTE_STATE);
						retCode = sendLteStateToCabApp(CONNECTED_LTE_STATE);
						if(retCode != GEN_SUCCESS)
						{
							CAB_DBG(CAB_GW_ERR, "sendLteStateToCabApp fail with error: %d", retCode);
						}
						retCode = sendLteStateToRemoteDiagApp(CONNECTED_LTE_STATE);
						if(retCode != GEN_SUCCESS)
						{
							CAB_DBG(CAB_GW_ERR, "sendLteStateToRemoteDiagApp fail with error: %d", retCode);
						}
						break;
					} else {
						getPPPD_PID(&globalPID);
						CAB_DBG(CAB_GW_DEBUG, "Checking pid existance %d", globalPID);
						killRet = kill(globalPID, 0);
						if (killRet != 0) {
							setLTE_State(DISCONNECTED_LTE_STATE);
							retCode = sendLteStateToCabApp(DISCONNECTED_LTE_STATE);
							if(retCode != GEN_SUCCESS)
							{
								CAB_DBG(CAB_GW_ERR, "sendLteStateToCabApp fail with error: %d", retCode);
							}
							retCode = sendLteStateToRemoteDiagApp(DISCONNECTED_LTE_STATE);
							if(retCode != GEN_SUCCESS)
							{
								CAB_DBG(CAB_GW_ERR, "sendLteStateToRemoteDiagApp fail with error: %d", retCode);
							}
						}
					}
					if (loopCount == CONN_CHECK_MAX_TRY) {
						CAB_DBG(CAB_GW_INFO, "Connection check timeout");
						setLTE_State(DISCONNECTED_LTE_STATE);
						retCode = sendLteStateToCabApp(DISCONNECTED_LTE_STATE);
						if(retCode != GEN_SUCCESS)
						{
							CAB_DBG(CAB_GW_ERR, "sendLteStateToCabApp fail with error: %d", retCode);
						}
						retCode = sendLteStateToRemoteDiagApp(DISCONNECTED_LTE_STATE);
						if(retCode != GEN_SUCCESS)
						{
							CAB_DBG(CAB_GW_ERR, "sendLteStateToRemoteDiagApp fail with error: %d", retCode);
						}
						break;
					}
					loopCount++;
				}
				break;
			case CONNECTED_LTE_STATE:
				getPPPD_PID(&globalPID);
				CAB_DBG(CAB_GW_DEBUG, "Waiting for PID %d", globalPID);
				pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
				returnPID = waitpid(globalPID, &status, 0);
				pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
				CAB_DBG(CAB_GW_INFO, "Disconnected LTE");
				setLTE_State(DISCONNECTED_LTE_STATE);
				retCode = sendLteStateToCabApp(DISCONNECTED_LTE_STATE);
				if(retCode != GEN_SUCCESS)
				{
					CAB_DBG(CAB_GW_ERR, "sendLteStateToCabApp fail with error: %d", retCode);
				}
				retCode = sendLteStateToRemoteDiagApp(DISCONNECTED_LTE_STATE);
				if(retCode != GEN_SUCCESS)
				{
					CAB_DBG(CAB_GW_ERR, "sendLteStateToRemoteDiagApp fail with error: %d", retCode);
				}
				break;
			default:
				break;
		}
	}
	retCode = threadDeregister(newWatchdogNode.threadId);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "Keep Alive LTE thread de-registration failed with error %d", retCode);
		rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "Keep Alive LTE thread de-registration failed");
	}
	FUNC_EXIT
		return NULL;
}
returnCode_e ATTransactEG91LTE(char *writeBuff, size_t len,  char *pattern, char *readbuff, int *readBytes)
{
	int count = 0;
	int  writtenBytes = 0;
	uint8_t matched = 0;
	returnCode_e retCode = GEN_SUCCESS;
	uint16_t idx = 0;
	struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0};
	clock_gettime(CLOCK_REALTIME, &abs_timeout);
	abs_timeout.tv_sec += MQ_SEND_LTE_STATUS_TIMEOUT;
	uint8_t tempRdbuff[512];

	if(!writeBuff || !pattern || !readbuff || !readBytes)
	{
		CAB_DBG(CAB_GW_ERR, "Invalid argument");
		return GEN_NULL_POINTING_ERROR;
	}

//	writtenBytes = write(lteFd, "AT+CMGF=1", strlen("AT+CMGF=1"));
//	writtenBytes = write(lteFd, "\n", 1);
	tcflush(lteFd, TCIOFLUSH);   /* Discards old data in the rx buffer */
	writtenBytes = write(lteFd, writeBuff, len);
	writtenBytes = write(lteFd, "\n", 1);
//	CAB_DBG(CAB_GW_INFO,"Command to LTE : %s\n",writeBuff);

	FD_ZERO(&sFd);
	FD_SET(lteFd, &sFd);

	/* Wait up to 1 seconds. */
	tv.tv_sec = 1;
	tv.tv_usec = 0;

	memset(readbuff, 0, 512);
	while(count++ < RETRY_COUNT)
	{		       
		if(select(lteFd+1, &sFd, NULL, NULL, &tv))
		{
			if(FD_ISSET(lteFd, &sFd))
			{
				*readBytes = read(lteFd, tempRdbuff, 512);
				if(*readBytes < 0)
				{
					CAB_DBG(CAB_GW_ERR, "read fails ... %s", strerror(errno));
					return FILE_READ_ERROR;
				}

				if(strstr(tempRdbuff, "+CMGR:")||strstr(tempRdbuff, "+CPMS:")) {
					if (*readBytes)
						memcpy(readbuff+idx, tempRdbuff, *readBytes);
					idx += *readBytes;
			//		CAB_DBG(CAB_GW_INFO,"readbuff : %s\n", readbuff);
				}
				if(idx) {
					if (*readBytes)
						memcpy(readbuff+idx, tempRdbuff, *readBytes);
					idx += *readBytes;
				}

				if(strstr(tempRdbuff, pattern))
				{
					matched = 1;
					break;	
				}
				memset(tempRdbuff, 0, sizeof(tempRdbuff));
			}
		}
	}	
	if (matched == 0)
	{
		CAB_DBG(CAB_GW_INFO, "AT command failed, checking USB interface");
		*readBytes = 0;
	} else {
		*readBytes = idx;
	}
	return retCode;
}
returnCode_e readSMS(char *readbuff, int *readBytes)
{
	struct termios tty;
	struct tm tm1 = {0};
	struct tm temp1 = {0}; // intermediate datastructes
	time_t t1;
	time_t temp; // used in difftime
	int count = 0;
	int  writtenBytes = 0;
	uint8_t matched = 0;
	returnCode_e retCode = GEN_SUCCESS;
	int count1=1,int_val;
	int index=0,size;
	char timeString[100] = {0};
	char delim[2] = ",";
	char *data;
	lteSms_t lteSmsCmd;

	if(!readbuff || !readBytes)
	{
		CAB_DBG(CAB_GW_ERR, "Invalid argument");
		return GEN_NULL_POINTING_ERROR;
	}

#if 1
	//sleep(1);
#endif

	/*Call the ATTransactEG91 function for getting the command response in to the readbuff*/
	retCode = ATTransactEG91LTE("AT+CPMS?", strlen("AT+CPMS?"), "OK", readbuff, readBytes);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_INFO, "ATTransactEG91()-%s failed with %d", "AT+CPMS?", retCode);
		return retCode;
	}

	/*Parse the number of SMS from the response*/
	char idx;
	char no_of_sms[4] = {0};
	uint8_t firstTimeEntry = 0;
	data = strtok(readbuff, ",");
//	CAB_DBG(CAB_GW_INFO, "data1 %s", data);
	idx = strlen(data);
	idx++;
	uint8_t l_cnt = 0;
	for(l_cnt = 0; readbuff[idx] != ','; l_cnt++,idx++) {
		no_of_sms[l_cnt] = readbuff[idx];
	}
	no_of_sms[l_cnt] = '\0';

	int_val=atoi(no_of_sms);
	CAB_DBG(CAB_GW_INFO,"number of messages - %d\n",int_val);
        
	if(int_val == 0)
		return GEN_SUCCESS;
	sleep(1);

	/*Loop iterates the number of SMS for getting the oldest message*/	
	for(int i=0;i<int_val;i++)
	{
		char writebuff1[12];
		sprintf(writebuff1,"AT+CMGR=%d",i);
		memset(readbuff, 0, sizeof(readbuff));
#if 1
		retCode = ATTransactEG91LTE("AT+CMGF=1", strlen("AT+CMGF=1"), "OK", readbuff, readBytes);
		if(retCode != GEN_SUCCESS)
		{
			CAB_DBG(CAB_GW_INFO, "ATTransactEG91()-%s failed with %d", "AT+CMGF=1" , retCode);
		}
#endif
		//		sleep(1);
		memset(readbuff, 0, sizeof(readbuff));

		retCode = ATTransactEG91LTE(writebuff1, strlen(writebuff1), "OK", readbuff, readBytes);
		if(retCode != GEN_SUCCESS)
		{
			CAB_DBG(CAB_GW_INFO, "ATTransactEG91()-%s failed with %d",writebuff1 , retCode);
		}


//		CAB_DBG(CAB_GW_INFO, "----%s-----\n", readbuff);

//		CAB_DBG(CAB_GW_INFO, "----------------parsing readbuff--------------\n");
		if(strstr(readbuff, "+CMGR:")) {
			l_cnt = 0, idx = 0;
			while(1) {
				if(readbuff[idx] == ',')
					l_cnt++;
				if(l_cnt == 3)
					break;
				idx++;
			}
			idx++;
			idx++;
			for(l_cnt = 0; readbuff[idx] != '"'; l_cnt++,idx++) {
				timeString[l_cnt] = readbuff[idx];
			}
			timeString[l_cnt] = '\0';

			CAB_DBG(CAB_GW_INFO,"TimeString:%s\n", timeString);
			/*Converting the time string in to the time for the comaprision*/
			if(strptime(timeString, "%Y/%m/%d,%H:%M:%S", &tm1) == NULL)
			{
				CAB_DBG(CAB_GW_INFO,"strptime failed\n");
			}

			tm1.tm_year += 2000;
			t1 = mktime(&tm1);
			if(t1 == -1) {
				CAB_DBG(CAB_GW_ERR, "mktime failed with errno: %d error string : %s t1 : %d\n", errno, strerror(errno), t1);
			}
			if(!firstTimeEntry)
			{
				index = i;
				temp1 = tm1;
				temp = t1;
				firstTimeEntry = 1;
			}
			if(difftime(t1,temp) < 0) 
			{
				index = i;
				temp1 = tm1;
				temp = t1;
			}
		} else {
			int_val++;
		}
	}
	/*Storing the oldest messages in to the readbuff*/
	char storebuff[12];
	sprintf(storebuff,"AT+CMGR=%d",index);
	memset(readbuff, 0, sizeof(readbuff));
	CAB_DBG(CAB_GW_INFO,"------Reading index %d message\n", index);
	retCode = ATTransactEG91LTE(storebuff, strlen(storebuff), "OK", readbuff, readBytes);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_INFO, "ATTransactEG91()-%s failed with %d",storebuff, retCode);
	}
	CAB_DBG(CAB_GW_INFO,"ReadBuffer:***%s***",readbuff);

	/*After reading the message delete it from memory*/
	char delbuff[12];
	sprintf(delbuff,"AT+CMGD=%d",index);
	CAB_DBG(CAB_GW_INFO,"------Deleting index %d message\n", index);
	writtenBytes=write(lteFd,delbuff,strlen(delbuff));
	writtenBytes=write(lteFd,"\n",1);

	return retCode;
}
static void* lteSMSHandler(void* arg)
{
#if 0
	lteStatus_e tempState = MAX_LTE_STATE;
	while(1)
	{
		getLTE_State(&tempState);
		if (tempState == CONNECTED_LTE_STATE)
		{
			break;
		}

	}
#endif
	while(1)
	{

		/*Lock until the another thread is using it*/
		int semval;
		sem_getvalue(g_lteSmsSem, &semval);
	//	CAB_DBG(CAB_GW_INFO, "In %s before sem_wait sem_value:%d-%d\n", __func__, semval, *g_lteSmsSem);
		semval = 0;
		if (sem_wait(g_lteSmsSem) < 0)
		{
			CAB_DBG(CAB_GW_INFO,"lteSMS  : [sem_wait] Failed");
		}

		semval = 0;
		sem_getvalue(g_lteSmsSem, &semval);
	//	CAB_DBG(CAB_GW_INFO, "In %s after sem_wait sem_value:%d-%d\n", __func__, semval, *g_lteSmsSem);

		CAB_DBG(CAB_GW_INFO,">>>>>>>>>>>>>>>>>>>>>In LTEAPP SMS Handler thread while\n");

		char readbuff[MAX_READ_BUFF_SIZE];
		int readBytes,int_val;
		char* data;
		char cmd[MAX_SIZE_OF_CMD]; 
		int count=1;

		struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0};
		clock_gettime(CLOCK_REALTIME, &abs_timeout);
		abs_timeout.tv_sec += MQ_SEND_LTE_STATUS_TIMEOUT;
		lteSms_t lteSmsCmd;
		struct termios tty;
		returnCode_e retCode;

		memset(readbuff, 0, sizeof(readbuff));
		memset(cmd,0,sizeof(cmd));
		memset(&lteSmsCmd, 0, sizeof(lteSmsCmd));


		/*Open the EG91 USB port*/
		retCode=openEG91();
		if(retCode != GEN_SUCCESS)
		{
			CAB_DBG(CAB_GW_INFO, "openEG91() open port failed with error %d", retCode);
			sem_post(g_lteSmsSem);
			return retCode;
		}
	//	CAB_DBG(CAB_GW_INFO,"--------------OPEN EG91 SUCCESS-----------------\n");

		/*readSMS available in to the memory*/
		retCode=readSMS(readbuff, &readBytes);
		if(retCode != GEN_SUCCESS)
		{
			CAB_DBG(CAB_GW_INFO, "read SMS failed with error %d", retCode);
			sem_post(g_lteSmsSem);
			return retCode;
		}
		/*Reboot handling*/	
		data=strstr(readbuff,"#REBOOT");
		if(data)
		{
			CAB_DBG(CAB_GW_INFO,"reboot is detected");
			rDiagSendMessage(CABAPP_GEN_WARN, GW_WARNING, "reboot requested from SMS - rebooting");
			/*Reboot the system*/
			rebootSystem();
		}

		/*factory-reset handling*/
		data=strstr(readbuff,"#FACTRESET");
		if(data)
		{
			CAB_DBG(CAB_GW_INFO,"factoryreset is detected");
		//	system("touch /cabApp/FACTRESET_SMS");
			memset(&lteSmsCmd, 0, sizeof(lteSmsCmd));
			strcpy(lteSmsCmd.msg_buff,data);
			lteSmsCmd.msg_type=2;
			if(mq_timedsend(mqLteSMSQueue,(const char*)&lteSmsCmd,sizeof(lteSms_t),0,&abs_timeout)<0)
			{
				CAB_DBG(CAB_GW_ERR, "mq timed send fail for LTE SMS message queue");
			}

		}

		/*Configcloud handling*/
		data=strstr(readbuff,"#CFGCLOUD");
		if(data)
		{
			CAB_DBG(CAB_GW_INFO,"=%s=",data);
			CAB_DBG(CAB_GW_INFO,"configcloud is detected");
			memset(&lteSmsCmd, 0, sizeof(lteSmsCmd));
			strcpy(lteSmsCmd.msg_buff,data);
			lteSmsCmd.msg_type=3;
			if(mq_timedsend(mqLteSMSQueue,(const char*)&lteSmsCmd,sizeof(lteSms_t),0,&abs_timeout)<0)
			{
				CAB_DBG(CAB_GW_ERR, "mq timed send fail for LTE SMS message queue");

			}

		}

		/*Close the ttyUSB2 port*/
		close(lteFd);	

		/*Unlock the semaphore varible after read the data*/
		semval = 0;
		sem_getvalue(g_lteSmsSem, &semval);
	//	CAB_DBG(CAB_GW_INFO, "In %s before sem_post sem_value:%d-%d\n", __func__, semval, *g_lteSmsSem);
		if (sem_post(g_lteSmsSem) < 0)
		{
			CAB_DBG(CAB_GW_INFO,"lteSMS  : [sem_post] Failed");
		}
		semval = 0;
		sem_getvalue(g_lteSmsSem, &semval);
	//	CAB_DBG(CAB_GW_INFO, "In %s after sem_post sem_value:%d-%d\n", __func__, semval, *g_lteSmsSem);

		sleep(1);
	}
}

void watchDogTimerCB(void)
{
	FUNC_ENTRY
		watchDogTimer_t *localWatchDogTimerNode;
	bool status = true;

	CAB_DBG(CAB_GW_DEBUG, "WatchDog timer callback from LTE App...");


	localWatchDogTimerNode = getWatchDogTimerHead();
	while(localWatchDogTimerNode != NULL)
	{
		CAB_DBG(CAB_GW_DEBUG, "thread id from lteApp is %ld...", localWatchDogTimerNode->threadId);
		status = localWatchDogTimerNode->CallbackFn();
		if(status == false)
		{
			CAB_DBG(CAB_GW_ERR, "Reboot the system due to issue with thread id:%ld from LTE App...", localWatchDogTimerNode->threadId);
			rebootSystem();
		}
		localWatchDogTimerNode = localWatchDogTimerNode->next;

	}

	uint32_t temp=0;
	checkSelfProcMem(&temp);
	if (temp > MAX_MEM_LEAK) {
		CAB_DBG(CAB_GW_ERR, "Memory leak in lte, rebooting system");
		printf("\033[1m###### Rebooting system ############\033[22m\r\n");
		rebootSystem();
	}

	FUNC_EXIT
}


static returnCode_e startWatchDogTimer()
{
	TIMER_STATUS_e timerRetSTat = TIMER_SUCCESS;

	TIMER_INFO_t watchDogTimerInfo = { \
		.count = WATCHDOG_TIMER_COUNT, \
			.funcPtr = watchDogTimerCB, \
			.data = 0xB1};

	timerRetSTat = initSysTimer(TIMER_RESOLUTION_SEC);
	if (timerRetSTat != TIMER_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Init watchDog timer failed with code : %d", timerRetSTat);
		return MODULE_INIT_FAIL;
	}

	timerRetSTat = startSystemTimer(watchDogTimerInfo, &watchDogTimerHandle);
	if (timerRetSTat != TIMER_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Start watchDog timer failed with code : %d", timerRetSTat);
		return MODULE_INIT_FAIL;
	}

}

returnCode_e initLTEApp(void)
{
	returnCode_e retCode = GEN_SUCCESS;
	FUNC_ENTRY

		if (pthread_mutex_init(&pidLock, NULL) != 0)
		{
			CAB_DBG(CAB_GW_ERR, "LTE PID mutex init failed");
			FUNC_EXIT
				return MODULE_INIT_FAIL;
		}

	if (pthread_mutex_init(&stateLock, NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "LTE state mutex init failed");
		pthread_mutex_destroy(&pidLock);
		FUNC_EXIT
			return MODULE_INIT_FAIL;
	}

	if (pthread_mutex_init(&cabAppStateLock, NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "LTE cabapp state mutex init failed");
		pthread_mutex_destroy(&pidLock);
		pthread_mutex_destroy(&stateLock);
		FUNC_EXIT
			return MODULE_INIT_FAIL;
	}

	if (sem_init(&g_lteSem, 0, 0) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "LTE semaphore init failed");
		pthread_mutex_destroy(&pidLock);
		pthread_mutex_destroy(&stateLock);
		pthread_mutex_destroy(&cabAppStateLock);
		FUNC_EXIT
			return MODULE_INIT_FAIL;
	}

	/*Semaphore initialization for the SMS synchronization*/
	g_lteSmsSem = sem_open(semName, O_CREAT, 0600, 1);
	if (g_lteSmsSem == SEM_FAILED)
	{
		CAB_DBG(CAB_GW_INFO,"lteSMS : [sem_open] Failed");
	}

	retCode = setupMsgQueue();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "setupMsgQueue() fail with error %d", retCode);
		pthread_mutex_destroy(&pidLock);
		pthread_mutex_destroy(&stateLock);
		pthread_mutex_destroy(&cabAppStateLock);
		sem_destroy(&g_lteSem);
		FUNC_EXIT
			return retCode;
	}

	retCode = startWatchDogTimer();
	if (retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "start watchDog timer failed with code %d", retCode);
		pthread_mutex_destroy(&pidLock);
		pthread_mutex_destroy(&stateLock);
		pthread_mutex_destroy(&cabAppStateLock);
		sem_destroy(&g_lteSem);
		FUNC_EXIT
			return retCode;
	}
	//LTE Keep Alive thread
	if (pthread_create(&g_keepAliveLTE_tid, NULL, keepAliveLTE, NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "Keep alive thread creation fails");
		pthread_mutex_destroy(&pidLock);
		pthread_mutex_destroy(&stateLock);
		pthread_mutex_destroy(&cabAppStateLock);
		sem_destroy(&g_lteSem);
		FUNC_EXIT
			return MODULE_INIT_FAIL;
	}

	setLTE_State(DISCONNECTED_LTE_STATE);

	// create thread to receive the LTE command from cabAppA7
	if (pthread_create(&g_lteCmdThreadID_t, NULL, &lteCmdHandler, NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "LTE Command Handler thread creation fails");
		pthread_mutex_destroy(&pidLock);
		pthread_mutex_destroy(&stateLock);
		pthread_mutex_destroy(&cabAppStateLock);
		sem_destroy(&g_lteSem);
		pthread_cancel(g_keepAliveLTE_tid);
		FUNC_EXIT
			return MODULE_INIT_FAIL;
	}

	// create thread to receive the LTE command from remote diag app
	if (pthread_create(&g_lteCmdFromRemoteDiagAppThreadID_t, NULL, &lteCmdFromRemoteDiagAppHandler, NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "LTE Command From Remote Diag App Handler thread creation fails");
		pthread_mutex_destroy(&pidLock);
		pthread_mutex_destroy(&stateLock);
		pthread_mutex_destroy(&cabAppStateLock);
		sem_destroy(&g_lteSem);
		pthread_cancel(g_keepAliveLTE_tid);
		pthread_cancel(g_lteCmdThreadID_t);
		FUNC_EXIT
			return MODULE_INIT_FAIL;
	}

	//create a new thread for the SMS handling here
	if(pthread_create(&g_lteSMSThreadID_t,NULL,&lteSMSHandler,NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR,"LTE SMS hanlder thread creation fails");
		pthread_mutex_destroy(&pidLock);
		pthread_mutex_destroy(&stateLock);
		pthread_mutex_destroy(&cabAppStateLock);
		sem_destroy(&g_lteSem);
		pthread_cancel(g_keepAliveLTE_tid);
		pthread_cancel(g_lteCmdThreadID_t);
		pthread_cancel(g_lteCmdFromRemoteDiagAppThreadID_t);
		FUNC_EXIT
			return MODULE_INIT_FAIL;
	}

	sleep(3);

	disconnectionFromLTE();
	retCode = connectionToLTE();
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_WARNING, "connect LTE failed with code %d",retCode);
		rDiagSendMessage(LTE_CONNECT_FAIL, GW_WARNING, "LTE connect failed");
	}


	FUNC_EXIT

		lteInited = 1;
	return retCode;
}


returnCode_e deInitLTEApp()
{
	returnCode_e retCode = GEN_SUCCESS;
	lteStatus_e tempState = MAX_LTE_STATE;
	FUNC_ENTRY

		if (lteInited == 1)
		{
			getLTE_State(&tempState);
			if (tempState == CONNECTED_LTE_STATE)
			{
				disconnectionFromLTE();
			}

			pthread_cancel(g_lteCmdThreadID_t);
			pthread_cancel(g_keepAliveLTE_tid);
			pthread_mutex_destroy(&pidLock);
			pthread_mutex_destroy(&stateLock);
			pthread_mutex_destroy(&cabAppStateLock);
			sem_destroy(&g_lteSem);

			/* Queue cleanup */
			mq_close(mqLteStateQueue);
			mq_unlink(LTE_STATE_QUEUE_NAME);
			mq_close(mqLteCmdQueue);
			mq_unlink(LTE_CMD_QUEUE_NAME);

			lteInited = 0;
		}

	FUNC_EXIT
		return retCode;
}

void signalHandler(int sig)
{
	returnCode_e retCode = GEN_SUCCESS;
	FUNC_ENTRY

		retCode = deInitLTEApp();
	if(retCode == GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_INFO, "deInitLTEApp() successful");
	} else	{
		CAB_DBG(CAB_GW_ERR, "deInitLTEApp() fail with error: %d", retCode);
		FUNC_EXIT
	}

	FUNC_EXIT
}
int main()
{
	returnCode_e retCode = GEN_SUCCESS;
	FUNC_ENTRY

		signal(SIGINT,signalHandler);

	retCode = initSysLogModule(MAX_SYSLOG_PACKAGE);
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "SYS log storage init failed with code %d", retCode);
	}

	retCode = signalRegister();
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Signal reg failed in LTE App with code %d", retCode);
		FUNC_EXIT
			return retCode;
	}	
	retCode = initLTEApp();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "initLTEApp() fail with error: %d", retCode);
		FUNC_EXIT
			return retCode;
	}

	CAB_DBG(CAB_GW_DEBUG, "initLTEApp() executed successfully");

	pthread_join(g_keepAliveLTE_tid, NULL);
	pthread_join(g_lteCmdThreadID_t, NULL);
	pthread_join(g_lteCmdFromRemoteDiagAppThreadID_t,NULL);

	FUNC_EXIT
		return 0;
}
