/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : curl.h
 * @brief : Header file for curl.c
 *
 * @Author     - VT
 ***************************************************************
 * History
 *
 * Sep/14/2018, VT , First Draft
 ***************************************************************/

#include <stdio.h>
#include <string.h>
#include <curl/curl.h>
#include <pthread.h>

#include "curl.h"
#include "error.h"
#include "debug.h"

pthread_mutex_t performLock;

/** @brief Convert return code to application return code
 *
 *  This API used for converting rest return code to application return code
 *
 * @param[in] http return code.
 *
 * @return application return code.
 */
static returnCode_e convertHttpToAppReturnCode(httpReturnCode_e httpReturnCode)
{
    FUNC_ENTRY

        switch (httpReturnCode) {
            case HTTP_EXPIRE:
                return CLOUD_TOKEN_EXPIRE;
                break;
            case HTTP_DEFAULT:
                return CLOUD_CURL_FAIL;
                break;
            case HTTP_SUCCESS:
                return GEN_SUCCESS;
                break;
            case NO_CONTENT:
                return OTA_UNAVAILABLE;
                break;
        }

    FUNC_EXIT

    return CLOUD_CURL_FAIL;
}

returnCode_e curlInit(void)
{
    FUNC_ENTRY

    pthread_mutex_init(&performLock, NULL);

    if(curl_global_init(CURL_GLOBAL_ALL))
        return FUNC_INIT_FAIL;

    FUNC_EXIT

    return GEN_SUCCESS;
}

void curlDeinit(void)
{
    FUNC_ENTRY

    curl_global_cleanup();
    pthread_mutex_destroy(&performLock);

    FUNC_EXIT

}

/** @brief callback to get api token data
 * @return status code.
 */
static size_t cbGetTokenData(char *ptr, size_t size, size_t nmemb, void *data)
{
    FUNC_ENTRY
    char *json = (char *)data;
    if (MAX_LEN_CLOUD_RESP < (size *nmemb))
        return 0;
    memcpy(json, ptr, size*nmemb);
    json[size*nmemb] = '\0';
    return size * nmemb;
}

int base64_encode(const void *in_buf, uint16_t inlen,
            void *out, uint16_t *outlen)
{
    unsigned long i, len2, leven;
    const unsigned char *in = in_buf;
    unsigned char *p;
    static const char *codes =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    /* valid output size ? */
    len2 = 4 * ((inlen + 2) / 3);
    if (*outlen < len2 + 1) {
        return -1;
    }
    p = out;
    leven = 3 * (inlen / 3);
    for (i = 0; i < leven; i += 3) {
        *p++ = codes[(in[0] >> 2) & 0x3F];
        *p++ = codes[(((in[0] & 3) << 4) + (in[1] >> 4)) & 0x3F];
        *p++ = codes[(((in[1] & 0xf) << 2) + (in[2] >> 6)) & 0x3F];
        *p++ = codes[in[2] & 0x3F];
        in += 3;
    }
    /* Pad it if necessary...  */
    if (i < inlen) {
        unsigned a = in[0];
        unsigned b = (i + 1 < inlen) ? in[1] : 0;

        *p++ = codes[(a >> 2) & 0x3F];
        *p++ = codes[(((a & 3) << 4) + (b >> 4)) & 0x3F];
        *p++ = (i + 1 < inlen) ? codes[(((b & 0xf) << 2)) & 0x3F] : '=';
        *p++ = '=';
    }

    /* append a NULL byte */
    *p = '\0';

    /* return ok */
    *outlen = p - (unsigned char *)out;
    return 0;
}


returnCode_e uploadLoginJsonToCloud(cloudInfo_t *cloudInfo, char *response, uint8_t *sha256Data)
{
	FUNC_ENTRY
	CURL *curl;
	CURLcode res;
	struct curl_slist *headers = NULL;
	char tempBuff[MAX_LEN_CLOUD_URI + MAX_LEN_APIKEY];
	char temp[10] = "key: ";
	char pemStringKey[13] = "authParam: ";
	char keyAsHeader[MAX_SHA_KEY_SIZE + strlen(temp) + 1];
	returnCode_e retCode = CLOUD_CURL_FAIL;

	curl_global_init(CURL_GLOBAL_DEFAULT); 
	curl = curl_easy_init();
	if(!curl) {
		curl_easy_cleanup(curl);
		curl_global_cleanup(); 
		return retCode;
	}

	#if BUILD == DEBUG
	curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
	#endif
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);

	snprintf(tempBuff, sizeof tempBuff, "libcurl/%s", curl_version_info(CURLVERSION_NOW)->version);
	tempBuff[sizeof tempBuff - 1] = 0;
	curl_easy_setopt(curl, CURLOPT_USERAGENT, tempBuff);

	/* read pem file */
        FILE *fp = fopen(CLIENT_CERTIFICATE, "rb");
        fseek(fp, 0, SEEK_END);
        uint16_t fileSize = ftell(fp);
        fseek(fp, 0, SEEK_SET);  //same as rewind(f);

        char *pemString = malloc(fileSize + 1);
        fread(pemString, fileSize, 1, fp);
        fclose(fp);

        pemString[fileSize] = 0;

	/* convert pem file to base64 format */
	char pemStringBase64[2048] = "";
	uint16_t base64Length = 2048;
	base64_encode(pemString, fileSize, pemStringBase64, &base64Length);

	char *pemAsHeader = calloc(1, strlen(pemStringKey) + base64Length + 1);
	snprintf(pemAsHeader, strlen(pemStringKey) + base64Length + 1, "%s%s", pemStringKey, pemStringBase64);

	headers = curl_slist_append(headers, "Expect:");
	headers = curl_slist_append(headers, "Content-Type: application/json");

	headers = curl_slist_append(headers, pemAsHeader);

	snprintf(keyAsHeader, MAX_SHA_KEY_SIZE + strlen(temp) + 1 ,"%s%s", temp, sha256Data);
	headers = curl_slist_append(headers, keyAsHeader);

	if (!response) {
		snprintf(tempBuff, sizeof tempBuff, "apikey: %s", cloudInfo->apiToken);
		headers = curl_slist_append(headers, tempBuff);
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
		snprintf(tempBuff, sizeof tempBuff, "%s/%s", cloudInfo->url, API_JSON_POST);
	} else {
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
		snprintf(tempBuff, sizeof tempBuff, "%s/%s", cloudInfo->url, API_REGISER_POST);
	}
	curl_easy_setopt(curl, CURLOPT_URL, tempBuff);

        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "Authenticated");
        curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, strlen("Authenticated"));

	if (response) {
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, cbGetTokenData);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, response);
	}

    	/* complete within 30 seconds */
    	curl_easy_setopt(curl, CURLOPT_TIMEOUT, 30L);

    	pthread_mutex_lock(&performLock);
	CAB_DBG(CAB_GW_DEBUG, "%s : Lock", __func__);
	res = curl_easy_perform(curl);
    	pthread_mutex_unlock(&performLock);
	CAB_DBG(CAB_GW_DEBUG, "%s : Unlock", __func__);

	if(res != CURLE_OK) {
		CAB_DBG(CAB_GW_ERR, "POST:Error code %u, %s", res, curl_easy_strerror(res));
	} else {
		curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &retCode);
		CAB_DBG(CAB_GW_INFO, "HTTP return code %d", retCode);
		retCode = convertHttpToAppReturnCode(retCode);
	}
	free(pemAsHeader);
	free(pemString);
	curl_slist_free_all(headers);
	curl_easy_cleanup(curl);
	curl_global_cleanup(); 

	FUNC_EXIT

	return retCode;
}

/** @brief callback to get api data
 * @return status code.
 */
static size_t cbUploadFileData(char *ptr, size_t size, size_t nmemb, void *data)
{
	char *json = (char *)data;
	if ((MAX_LEN_CLOUD_RESP * 2) < (size *nmemb))
		return 0;
	memcpy(json, ptr, size*nmemb);
	json[size*nmemb] = '\0';
	return size * nmemb;
}

returnCode_e uploadFileToCloud(cloudInfo_t *cloudInfo, const char *filePath, const char *filename, const char *version, bool fileTypeFlag, char *data)
{
	FUNC_ENTRY

	CURL *curl;
	CURLcode res;
	struct curl_slist *headers = NULL;
	returnCode_e retCode = CLOUD_CURL_FAIL;
	char tempBuff[MAX_LEN_CLOUD_URI + MAX_LEN_APIKEY];
	struct stat fileInfo;
	FILE *fd;

	snprintf(tempBuff, sizeof tempBuff, "%s/%s", filePath, filename);
	fd = fopen(tempBuff, "rb");
	if(!fd)
		return FILE_OPEN_FAIL;

	if(fstat(fileno(fd), &fileInfo) != 0)
		return FILE_CONTROL_GET_FAIL;

	/* if zip file is zero byte length */
	if(fileInfo.st_size <= 0) {
		CAB_DBG(CAB_GW_INFO,"Zero Length Zip File Found, No need to send to cloud");
		fclose(fd);
		return GEN_SUCCESS;
	}
	
	curl_global_init(CURL_GLOBAL_DEFAULT); 
	curl = curl_easy_init();
	if(!curl) {
		curl_easy_cleanup(curl);
		curl_global_cleanup(); 
		return retCode;
	}

	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);

	snprintf(tempBuff, sizeof tempBuff, "libcurl/%s", curl_version_info(CURLVERSION_NOW)->version);
	tempBuff[sizeof tempBuff - 1] = 0;
	curl_easy_setopt(curl, CURLOPT_USERAGENT, tempBuff);

	headers = curl_slist_append(headers, "Expect:");
	headers = curl_slist_append(headers, "Content-Type: multipart/form-data");

	snprintf(tempBuff, sizeof tempBuff, "authToken: %s", cloudInfo->apiToken);
	headers = curl_slist_append(headers, tempBuff);

	snprintf(tempBuff, sizeof tempBuff, "fileName: %s", filename);
	headers = curl_slist_append(headers, tempBuff);

	snprintf(tempBuff, sizeof tempBuff, "version: %s", version);
	headers = curl_slist_append(headers, tempBuff);

	snprintf(tempBuff, sizeof tempBuff, "fileType: %d", fileTypeFlag);
	headers = curl_slist_append(headers, tempBuff);

	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

	snprintf(tempBuff, sizeof tempBuff, "%s/%s", cloudInfo->url, API_FILE_UPLOAD);
	curl_easy_setopt(curl, CURLOPT_URL, tempBuff);

	curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
	curl_easy_setopt(curl, CURLOPT_READDATA, fd);
	curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE, (curl_off_t)fileInfo.st_size);

	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, cbUploadFileData);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, data);
	#if BUILD == DEBUG
	curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
	#endif
	/* 2 minute connection timeout */
	curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 120L);
	/* 2 minute timeout for speed limit 100 bytes / second */
	curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, 120L);
	curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, 100L);

    	pthread_mutex_lock(&performLock);
	CAB_DBG(CAB_GW_DEBUG, "%s : Lock", __func__);
	res = curl_easy_perform(curl);
    	pthread_mutex_unlock(&performLock);
	CAB_DBG(CAB_GW_DEBUG, "%s : Unlock", __func__);

	if(res != CURLE_OK) {
		CAB_DBG(CAB_GW_ERR, "POST:Error code %u, %s", res, curl_easy_strerror(res));
	} else {
		curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &retCode);
		CAB_DBG(CAB_GW_INFO, "HTTP return code %d", retCode);
		retCode = convertHttpToAppReturnCode(retCode);
	}
	curl_slist_free_all(headers);
	curl_easy_cleanup(curl);
	curl_global_cleanup(); 

	fclose(fd);

	FUNC_EXIT

	return retCode;
}

/** @brief callback to get api data
 * @return status code.
 */
static size_t cbWriteOtaData(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
	FUNC_ENTRY
	return fwrite(ptr, size, nmemb, stream);
}

returnCode_e downlaodFileFromCloud(cloudInfo_t *cloudInfo, const char *downloadUrl, const char *filePath, const char *filename)
{
	FUNC_ENTRY

	CURL *curl;
	CURLcode res;
	struct curl_slist *headers = NULL;
	returnCode_e retCode = CLOUD_CURL_FAIL;
	char tempBuff[MAX_LEN_CLOUD_URI + MAX_LEN_APIKEY];
	FILE *fd;

	snprintf(tempBuff, sizeof tempBuff, "%s/%s", filePath, filename);
	fd = fopen(tempBuff, "wb");
	if (!fd)
		return FILE_OPEN_FAIL;

	curl_global_init(CURL_GLOBAL_DEFAULT); 
	curl = curl_easy_init();
	if(!curl) {
		fclose(fd);
		return retCode;
	}

	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);

	snprintf(tempBuff, sizeof tempBuff, "libcurl/%s", curl_version_info(CURLVERSION_NOW)->version);
	tempBuff[sizeof tempBuff - 1] = 0;
	curl_easy_setopt(curl, CURLOPT_USERAGENT, tempBuff);

	headers = curl_slist_append(headers, "Expect:");
	snprintf(tempBuff, sizeof tempBuff, "authToken: %s", cloudInfo->apiToken);
	headers = curl_slist_append(headers, tempBuff);
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

	curl_easy_setopt(curl, CURLOPT_URL, downloadUrl);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, cbWriteOtaData);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, fd);

	/* 2 minute connection timeout */
	curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 120L);
	/* 2 minute timeout for speed limit 100 bytes / second */
	curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, 120L);
	curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, 100L);

    	pthread_mutex_lock(&performLock);
	CAB_DBG(CAB_GW_DEBUG, "%s : Lock", __func__);
	res = curl_easy_perform(curl);
    	pthread_mutex_unlock(&performLock);
	CAB_DBG(CAB_GW_DEBUG, "%s : Unlock", __func__);

	if(res != CURLE_OK) {
		CAB_DBG(CAB_GW_ERR, "POST:Error code %u, %s", res, curl_easy_strerror(res));
	} else {
		curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &retCode);
		CAB_DBG(CAB_GW_INFO, "HTTP return code %d", retCode);
		retCode = convertHttpToAppReturnCode(retCode);
	}
	curl_slist_free_all(headers);
	curl_easy_cleanup(curl);
	curl_global_cleanup(); 
	fclose(fd);

	FUNC_EXIT

	return retCode;
}


/** @brief callback to get api data
 * @return status code.
 */
static size_t cbSaveGetRequestData(char *ptr, size_t size, size_t nmemb, void *data)
{
	char *json = (char *)data;
	if (MAX_LEN_CLOUD_RESP < (size *nmemb))
		return 0;
	memcpy(json, ptr, size*nmemb);
	json[size*nmemb] = '\0';
	return size * nmemb;
}

returnCode_e getJsonfromCloud(cloudInfo_t *cloudInfo, const char *json, char *data)
{
	FUNC_ENTRY

	CURL *curl;
	CURLcode res;
	struct curl_slist *headers = NULL;
	returnCode_e retCode = CLOUD_CURL_FAIL;
	char tempBuff[MAX_LEN_CLOUD_URI + MAX_LEN_APIKEY];

	curl_global_init(CURL_GLOBAL_DEFAULT); 
	curl = curl_easy_init();
	if(!curl) {
		return retCode;
	}
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);

	snprintf(tempBuff, sizeof tempBuff, "libcurl/%s", curl_version_info(CURLVERSION_NOW)->version);
	tempBuff[sizeof tempBuff - 1] = 0;
	curl_easy_setopt(curl, CURLOPT_USERAGENT, tempBuff);

	headers = curl_slist_append(headers, "Expect:");
	headers = curl_slist_append(headers, "Content-Type: application/json");

	snprintf(tempBuff, sizeof tempBuff, "ota : %s", json);
	headers = curl_slist_append(headers, tempBuff);

	snprintf(tempBuff, sizeof tempBuff, "authToken: %s", cloudInfo->apiToken);
	headers = curl_slist_append(headers, tempBuff);

	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
	snprintf(tempBuff, sizeof tempBuff, "%s/%s", cloudInfo->url, API_FILE_DOWNLOAD);

	curl_easy_setopt(curl, CURLOPT_URL, tempBuff);

	curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);
	curl_easy_setopt(curl,  CURLOPT_WRITEFUNCTION, cbSaveGetRequestData);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, data);

	#if BUILD == DEBUG
	curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
	#endif

	/* complete within 30 seconds */
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, 30L);

    	pthread_mutex_lock(&performLock);
	CAB_DBG(CAB_GW_DEBUG, "%s : Lock", __func__);
	res = curl_easy_perform(curl);
    	pthread_mutex_unlock(&performLock);
	CAB_DBG(CAB_GW_DEBUG, "%s : Unlock", __func__);

	if(res != CURLE_OK) {
		CAB_DBG(CAB_GW_ERR, "POST:Error code %u, %s", res, curl_easy_strerror(res));
	} else {
		curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &retCode);
		CAB_DBG(CAB_GW_INFO, "HTTP return code %d", retCode);
		retCode = convertHttpToAppReturnCode(retCode);
	}
	curl_slist_free_all(headers);
	curl_easy_cleanup(curl);
	curl_global_cleanup(); 

	FUNC_EXIT

	return retCode;
}

static size_t notifyResponseCallback(char *ptr, size_t size, size_t nmemb, void *data)
{
	char *json = (char *)data;
	if (MAX_LEN_CLOUD_RESP < (size *nmemb))
		return 0;
	memcpy(json, ptr, size*nmemb);
	json[size*nmemb] = '\0';
	return size * nmemb;
}

returnCode_e notifyGatewayVersionToCloud(cloudInfo_t *cloudInfo, const char *json, char *data)
{
	FUNC_ENTRY

	CURL *curl;
	CURLcode res;
	struct curl_slist *headers = NULL;
	returnCode_e retCode = CLOUD_CURL_FAIL;
	char tempBuff[MAX_LEN_CLOUD_URI + MAX_LEN_APIKEY];

	curl_global_init(CURL_GLOBAL_DEFAULT); 
	curl = curl_easy_init();
	if(!curl) {
		return retCode;
	}
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);

	snprintf(tempBuff, sizeof tempBuff, "libcurl/%s", curl_version_info(CURLVERSION_NOW)->version);
	tempBuff[sizeof tempBuff - 1] = 0;
	curl_easy_setopt(curl, CURLOPT_USERAGENT, tempBuff);

	headers = curl_slist_append(headers, "Expect:");
	headers = curl_slist_append(headers, "Content-Type: application/json");

	snprintf(tempBuff, sizeof tempBuff, "ota : %s", json);
	headers = curl_slist_append(headers, tempBuff);

	snprintf(tempBuff, sizeof tempBuff, "authToken: %s", cloudInfo->apiToken);
	headers = curl_slist_append(headers, tempBuff);

	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
	snprintf(tempBuff, sizeof tempBuff, "%s/%s", cloudInfo->url, API_NOTIFY_ENDPOINT);

	curl_easy_setopt(curl, CURLOPT_URL, tempBuff);

	curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, notifyResponseCallback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, data);
	#if BUILD == DEBUG
	curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
	#endif

	/* complete within 30 seconds */
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, 30L);

    	pthread_mutex_lock(&performLock);
	CAB_DBG(CAB_GW_DEBUG, "%s : Lock", __func__);
	res = curl_easy_perform(curl);
    	pthread_mutex_unlock(&performLock);
	CAB_DBG(CAB_GW_DEBUG, "%s : Unlock", __func__);

	if(res != CURLE_OK) {
		CAB_DBG(CAB_GW_ERR, "POST:Error code %u, %s", res, curl_easy_strerror(res));
	} else {
		curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &retCode);
		CAB_DBG(CAB_GW_INFO, "HTTP return code %d", retCode);
		retCode = convertHttpToAppReturnCode(retCode);
	}
	curl_slist_free_all(headers);
	curl_easy_cleanup(curl);
	curl_global_cleanup(); 

	FUNC_EXIT

	return retCode;
}

returnCode_e uploadJsonToCloud(cloudInfo_t *cloudInfo, const char *json)
{
	FUNC_ENTRY

	CURL *curl;
	CURLcode res;
	struct curl_slist *headers = NULL;
	char tempBuff[MAX_LEN_CLOUD_URI + MAX_LEN_APIKEY];
	returnCode_e retCode = CLOUD_CURL_FAIL;

	curl_global_init(CURL_GLOBAL_DEFAULT); 
	curl = curl_easy_init();
	if(!curl) {
		curl_easy_cleanup(curl);
		curl_global_cleanup(); 
		return retCode;
	}

	#if BUILD == DEBUG
	curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
	#endif
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);

	snprintf(tempBuff, sizeof tempBuff, "libcurl/%s", curl_version_info(CURLVERSION_NOW)->version);
	tempBuff[sizeof tempBuff - 1] = 0;
	curl_easy_setopt(curl, CURLOPT_USERAGENT, tempBuff);

	headers = curl_slist_append(headers, "Expect:");
	headers = curl_slist_append(headers, "Content-Type: application/json");

	snprintf(tempBuff, sizeof tempBuff, "authToken: %s", cloudInfo->apiToken);
	headers = curl_slist_append(headers, tempBuff);
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

	snprintf(tempBuff, sizeof tempBuff, "%s/%s", cloudInfo->url, API_ALERT_POST);
	curl_easy_setopt(curl, CURLOPT_URL, tempBuff);

        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, json);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, strlen(json));

	/* complete within 30 seconds */
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, 30L);

    	pthread_mutex_lock(&performLock);
	CAB_DBG(CAB_GW_DEBUG, "%s : Lock", __func__);
	res = curl_easy_perform(curl);
    	pthread_mutex_unlock(&performLock);
	CAB_DBG(CAB_GW_DEBUG, "%s : Unlock", __func__);

	if(res != CURLE_OK) {
		CAB_DBG(CAB_GW_ERR, "POST:Error code %u, %s", res, curl_easy_strerror(res));
	} else {
		curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &retCode);
		CAB_DBG(CAB_GW_INFO, "HTTP return code %d", retCode);
		retCode = convertHttpToAppReturnCode(retCode);
	}
	curl_slist_free_all(headers);
	curl_easy_cleanup(curl);
	curl_global_cleanup(); 

	FUNC_EXIT
	return retCode;
}
