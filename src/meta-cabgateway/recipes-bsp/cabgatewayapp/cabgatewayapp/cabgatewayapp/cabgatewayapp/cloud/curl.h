/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : curl.h
 * @brief : Header file for curl.c
 *
 * @Author     - VT
 ***************************************************************
 * History
 *
 * Sep/14/2018, VT , First Draft
 ***************************************************************/
#ifndef __CURL_H__
#define __CURL_H__
#include <pthread.h>
#include "configuration.h"


#define API_JSON_POST       "api/post"            /**< JSON post api endpoint	*/
#define API_REGISER_POST    "gateway/login"        /**< Register api endpoint */
#define API_JSON_GET        "api/get"             /**< JSON get api endpoint */
#define API_FILE_UPLOAD     "gateway/sensordata"      /**< File uplaod api endpoint */
#define API_FILE_DOWNLOAD   "gateway/ota"        /**< File download api endpoint */
#define API_ALERT_POST	    "gateway/alert"        /**< endpoint for alert */
#define API_NOTIFY_ENDPOINT "gateway/notify"        /**< endpoint for alert */

#define CLIENT_CERTIFICATE	"/etc/ssl/client.pem"
#define MAX_LEN_APIKEY      2048                   /**< Register token size */
#define MAX_LEN_CLOUD_RESP  2048                  /**< Cloud response max size */
#define MAX_SHA_KEY_SIZE    64
#define MAX_GW_ID_LEN       64

/**
 * @enum httpReturnCode
 *
 *  Enumeration for HTTPS protocol.
 */
typedef enum httpReturnCode {
    HTTP_DEFAULT = 1000,    /**< http default code */
    HTTP_SUCCESS = 200,     /**< REST api success */
    HTTP_EXPIRE = 403,       /**< Registration expire */
    NO_CONTENT = 202       /**< OTA Not available */
} httpReturnCode_e;

/**
 * @struct cloudInfo
 * @brief cloud connection peer information
 *
 * This structure have information related cloud endpoint
 **/
typedef struct cloudInfo {
    pthread_mutex_t cloudInfoLock;
    bool isInitDone;
    char url[MAX_LEN_CLOUD_URI + 1];
    char apiToken[MAX_LEN_APIKEY + 1];
    char gatewayID[MAX_GW_ID_LEN];
} cloudInfo_t;

/** @brief Upload Login JSON object to cloud
 *
 *  This API used for uploading JSON object to cloud
 *
 * @param[in] cloud peer information.
 * @param[in/out] if NULL not out else register token in response
 * @param[in] sha key
 *
 * @return status code.
 */
returnCode_e uploadLoginJsonToCloud(cloudInfo_t *cloudInfo, char *response, uint8_t *sha256Data);

/** @brief Upload JSON object to cloud
 *
 *  This API used for uploading JSON object to cloud
 *
 * @param[in] cloud peer information.
 * @param[in] json object
 * @param[in/out] if NULL not out else register token in response
 *
 * @return status code.
 */
returnCode_e uploadJsonToCloud(cloudInfo_t *cloudInfo, const char *json);

/** @brief Upload file to cloud
 *
 *  This API used for uploading file to cloud
 *
 * @param[in] cloud peer information.
 * @param[in] filepath
 * @param[in] filename
 * @param[in] current software version
 * @param[in] File type flag for file to be sent
 * @param[out] response data (json object)
 *
 * @return status code.
 */
returnCode_e uploadFileToCloud(cloudInfo_t *cloudInfo, const char *filepath, const char *filename, const char *version, bool fileTypeFlag, char *data);

/** @brief download file fron cloud
 *
 *  This API used for downloading file from cloud
 *
 * @param[in] cloud peer information.
 * @param[in] file download url.
 * @param[in] filepath
 * @param[in] filename
 *
 * @return status code.
 */
returnCode_e downlaodFileFromCloud(cloudInfo_t *cloudInfo, const char *downloadUrl, const char *filePath, const char *filename);

/** @brief Get JSON object from cloud
 *
 *  This API used for get JSON object from cloud
 *
 * @param[in] cloud peer information.
 * @param[in] json object
 * @param[out] json object
 *
 * @return status code.
 */
returnCode_e getJsonfromCloud(cloudInfo_t *cloudInfo, const char *json, char *data);

/** @brief Init curl resource
 *
 * This API used initalize curl resource
 *
 * @return status code.
 */
returnCode_e curlInit(void);

/** @brief cleanup curl resource
 *
 *  This API used for curl cleanup
 */
void curlDeinit(void);

#endif /* __CURL_H__ */
