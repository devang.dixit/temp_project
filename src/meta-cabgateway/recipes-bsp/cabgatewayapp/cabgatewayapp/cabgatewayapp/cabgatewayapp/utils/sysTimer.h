/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file sysTimer.h
 * @brief Header file for system timer.
 *
 * @Author     - Volansys
 *****************************************************************
 * History
 *
 * Sept/18/2018, VT , First Draft
***************************************************************/


#if !defined SYSTMR_H
#define SYSTMR_H

#include "error.h"
#include "debug.h"
#include <stdbool.h>
#include <stdint.h>
#include <time.h>

//********Place all include files ****

//******** Defines and Data Types ****

#define INVALID_TIMER_HANDLE                NULL        // invalid timer handle
//#define TIMER_RESOLUTION_SEC                0
//#define TIMER_RESOLUTION_MINIMUM_MSEC       500         //Minimum resolution in msec
#define USEC_TO_SEC                         (1000000)
#define USEC_TO_MSEC                        (USEC_TO_SEC/1000)
#define TIMER_RESOLUTION_USEC               (TIMER_RESOLUTION_MINIMUM_MSEC * USEC_TO_MSEC)      // timer resolution in uSec
#define CONVERT_SEC_TO_TIMER_COUNT(sec)     ((USEC_TO_SEC/TIMER_RESOLUTION_USEC) * sec) //Calculation for 1 second
#define CONVERT_MSEC_TO_TIMER_COUNT(msec)   ((((USEC_TO_SEC/TIMER_RESOLUTION_USEC) * msec)/USEC_TO_MSEC) <=0 )?1:(((USEC_TO_SEC/TIMER_RESOLUTION_USEC) * msec)/USEC_TO_MSEC)
#define NANOSEC_IN_1_SEC		    (1000000000)
#define TIMER_SIG 			    SIGRTMIN
#define CLOCKID 			    CLOCK_REALTIME


typedef enum {
    TIMER_SUCCESS = 0,
    TIMER_SYS_API_FAIL,
    TIMER_INVLAID_ARG,
    TIMER_MALLOC_FAIL,
    TIMER_RESOURCE_LIMIT_ERR,
    TIMER_LIST_EMPTY,
    MAX_TIMER_ERRORS
} TIMER_STATUS_e;

//  TIMER_INFO_t
//  This structure capsulates parameters required to start a new timer.
typedef struct {
    uint32_t      count;                      // timer count (100ms resolution)
    void (*funcPtr)(uint32_t data);           // pointer to call-back function
    uint32_t      data;                       // input data to pass back with callback function
} TIMER_INFO_t;

typedef struct  TIMER_LIST_NODE     SYS_TIMER_LIST_t;

typedef SYS_TIMER_LIST_t *          TIMER_HANDLE;       // timer handle

typedef struct {
    bool (*funcPtr)(uint32_t userData);
    uint32_t      userData;
} ONE_MIN_NOTIFY_t;

//******** Function Prototypes *******

//*****************************************************************************
//  initSysTimer()
//  Param:
//      None
//  Returns:
//      None
//  Description:
//      Initializes the system timer module.
//      This function is usually called during the startup.
//      No timer API should be called before this function.
//
//*****************************************************************************
TIMER_STATUS_e initSysTimer();

//*****************************************************************************
//  runSysTimer()
//  Param:
//      None
//  Returns:
//      None
//  Description:
//      Executes system timer task. Shoould be called periodically in super-loop
//      of main function.It services all active timers and on expiration it calls
//      the specified user function.
//  Pre-condition:
//      initSysTimer() should have been called before system timer task can run.
//  Note:
//      Granularity of timer provided by this module is 100ms.
//      However, accuracy of the timer depends on how often this task is run
//      in super-loop.
//*****************************************************************************
void runSysTimer();

//*****************************************************************************
//  StartTimer()
//  Param:
//      IN:     TIMER_INFO_t    - information of timer to be started.
//      OUT:    TIMER_HANDLE *  - address at which allocated timer handle will
//                                be stored.
//  Returns:
//      SUCCESS - if successfully started new timer. Else FAIL
//  Description:
//      This function starts a new cyclic timer with specified timer count and
//      register callback function.On expiry of the timer, user supplied callback
//      function will be invoked.Application using this API can implement
//      callback function to take appropriate action on expiry of the timer,
//      including deleting it if no longer required.
//*****************************************************************************
TIMER_STATUS_e startSystemTimer(TIMER_INFO_t timerInfo, TIMER_HANDLE * handle);

//*****************************************************************************
//  deleteTimer()
//  Param:
//      IN:     TIMER_HANDLE *  - pointer to timer handle that need to be
//                                deleted.
//  Returns:
//      NONE
//  Description:
//      This function deletes specified timer if running.
//      After deletion, handle is set to INVALID_TIMER_HANDLE.
//*****************************************************************************
void deleteTimer(TIMER_HANDLE * handle);

//*****************************************************************************
//  reloadTimer()
//  Param:
//      IN:     TIMER_HANDLE    - timer handle to be reloaded
//              uint32_t          - new timer count value
//  Returns:
//      SUCCESS - if successfully reloaded timer. Else FAIL
//  Description:
//      This function reloads timer count with the new value of the specified timer.
//*****************************************************************************
TIMER_STATUS_e reloadTimer(TIMER_HANDLE handle, uint32_t count);

//*****************************************************************************
//  getElapsedTime()
//  Param:
//      IN :    TIMER_HANDLE        // Timer Handle
//      OUT:    uint32_t              // timer count
//  Returns:
//              SUCCESS             // on successful reload of count
//              FAIL                // on failure to load the count
//  Description:
//      Get Elapsed timer since started timer of that particluar handle.
//
//  [Pre-condition:]
//      NONE
//  [Constraints:]
//      NONE
//
//*****************************************************************************
TIMER_STATUS_e getElapsedTime(TIMER_HANDLE handle, uint32_t * timeCnt);

//*****************************************************************************
//  getRemainingTime()
//  Param:
//      IN :    TIMER_HANDLE        // Timer Handle
//      OUT:    uint32_t              // timer count
//  Returns:
//              SUCCESS             // on successful reload of count
//              FAIL                // on failure to load the count
//  Description:
//      Get Remaining timer since started timer of that particluar handle.
//
//  [Pre-condition:]
//      NONE
//  [Constraints:]
//      NONE
//
//*****************************************************************************
TIMER_STATUS_e getRemainingTime(TIMER_HANDLE handle, uint32_t * timeCnt);

//*****************************************************************************
//  getSysTick()
//  Param:
//      IN:     NONE
//  Returns:
//              uint32_t - current system tick.
//  Description:
//      This function returns the current count value of system ticks.
//      Granularity of system tick is 1ms.
//*****************************************************************************
uint32_t getSysTick();

//*****************************************************************************
//  elapsedTick()
//  Param:
//      IN:     uint32_t  - mark value from which to calculate elapsed time.
//  Returns:
//              uint32_t - current system tick.
//  Description:
//      This function returns the count of elapsed tick since the mark tick.
//      Note: Accuracy of is this function depends on how often system timer
//      task is run.
//  Note:
//      System Timer task must be run atleast once since last mark for
//      this function to provide proper elasped tick.
//      Accuracy of is this function depends on how often system timer task is run.
//*****************************************************************************
uint32_t elapsedTick(uint32_t markTick);

//*****************************************************************************
//  registerOnMinFun()
//  Param:
//      IN :    ONE_MIN_NOTIFY_t *
//      OUT:    NONE
//  Returns:
//              bool
//  Description:
//      This function registers one min function which is call @ every actual
//      minute modified.
//
//  [Pre-condition:]
//      runSysTimer() : System timer must be in running state.
//  [Constraints:]
//      NONE
//
//*****************************************************************************
TIMER_STATUS_e registerOnMinFun(ONE_MIN_NOTIFY_t * oneMinFun);


void timerHandler(int sig);

//*****************************************************************************
//
//
//
//
//
//
//*****************************************************************************
void getLocalTimeInBrokenTm(struct tm *localTime);
#endif  // SYSTIMER_H
