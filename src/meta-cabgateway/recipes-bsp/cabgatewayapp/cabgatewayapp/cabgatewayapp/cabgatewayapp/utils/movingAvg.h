/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : movingAvg.h
 * @brief : This file contains API header for moving average
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Nov/16/2018, VR, Added moving avg for leak detection logic
 **************************************************************/

/****************************************
 ************ INCLUDES ******************
 ****************************************/

#include "stdint.h"
#include <pthread.h>

/****************************************
 ************ ENUMS ******************
 ****************************************/

typedef enum {
        MAVG_SUCCESS = 0,
        MAVG_INVALID_ARG,
        MAVG_MALLOC_ERR,
	MAVG_ALREADY_INIT,
	MAVG_NOT_INIT,
        MAVG_CODE
} MAVG_STATUS_e;

typedef struct movingAvg {
        uint8_t isInit;
        uint16_t windowSize;
        uint16_t currIndex;
        float sum;
        float *avgArray;
	pthread_mutex_t mavgMutex;	
} movingAvg_t;

/****************************************
 ************ FUNCTIONS ******************
 ****************************************/

/*********************************************************************
 *@brief          : Init Monitoring and Telemetry module
 *
 *@param mavgHandle[IN]   	: Moving avg handle stucture
 *@param initWindowSize[IN]     : Window size of moving average
 *
 *@return         : Errocode Refer MAVG_STATUS_e
 *********************************************************************/
MAVG_STATUS_e initMovingAvg (movingAvg_t *mavgHandle, uint16_t initWindowSize);

/*********************************************************************
 *@brief          : Init Monitoring and Telemetry module
 *
 *@param mavgHandle[IN]      : Moving avg handle stucture
 *
 *@return         : Errocode Refer MAVG_STATUS_e
 *********************************************************************/
MAVG_STATUS_e deInitMovingAvg (movingAvg_t *mavgHandle);

/*********************************************************************
 *@brief          : Init Monitoring and Telemetry module
 *
 *@param mavgHandle [IN]   : Moving avg handle stucture
 *@param new_val [IN]      : Moving avg handle
 *@param outAvg [OUT]      : Output average of PSI
 *
 *@return         : Errocode Refer MAVG_STATUS_e
 *********************************************************************/
MAVG_STATUS_e calcMovingAvg(movingAvg_t *mavgHandle, float new_val, float *outAvg);
