/********************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file diag.c
 * @brief (This file contains APIs defination of diag module.)
 *
 * @Author     - VT
 **********************************************************************************************
 * History
 *
 * Mar/06/2019, VT , First Draft
 **********************************************************************************************/

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include "debug.h"
#include "diag.h"
#include "error.h"
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <pthread.h>



/****************************************
 ********      MACROS	   **************
 ****************************************/

/****************************************
 ******** STATIC VARIABLES **************
 ****************************************/
static pthread_mutex_t g_diagFileLock = PTHREAD_MUTEX_INITIALIZER;


/****************************************
 ******** FUNCTION DEFINITIONS **********
 ****************************************/

/******************************************************************
 *@brief  (This API is used to init diag module)
 *
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e diagInit()
{
    FUNC_ENTRY

#ifdef DIAG_SUPPORT
    struct stat st = {0};
    
    /*To check for the diag directory and if it not exist then create it*/
    if(stat(DIR_PATH_DIAG, &st) == -1) {
        if((mkdir(DIR_PATH_DIAG, 0755)) == -1) {
            CAB_DBG(CAB_GW_ERR, "dataStorage directory not created");
            return DIR_CREATE_FAIL;
        }
    }
#endif

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (This API is used to write string to diag file)
 *
 *@param[IN] string	Debug string to be printed in file
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e diagDump(char * diagString)
{
    FUNC_ENTRY

#ifdef DIAG_SUPPORT
    int diagFile;                                //file descriptor
    uint32_t ret;

    /*Input argument validation*/
    if(diagString == NULL) {
        CAB_DBG(CAB_GW_ERR, "Invalid argument");
        return GEN_NULL_POINTING_ERROR;
    }

    pthread_mutex_lock(&g_diagFileLock);
    //opening event summary file for append
    diagFile = open(FILE_PATH_DIAG, O_WRONLY | O_APPEND | O_CREAT, 0644);
    if(diagFile == -1) {
        CAB_DBG(CAB_GW_ERR, "Diag file open failed");
    	pthread_mutex_unlock(&g_diagFileLock);
        return FILE_OPEN_FAIL;
    }

    //writing event summery structure data to file
    ret = write(diagFile, diagString, strlen(diagString));
    if(ret == -1) {
        CAB_DBG(CAB_GW_ERR, "File write error");
        close(diagFile);
        pthread_mutex_unlock(&g_diagFileLock);
        return FILE_WRITE_ERROR;
    }
    //close event summary file
    close(diagFile);
    pthread_mutex_unlock(&g_diagFileLock);

#endif
    FUNC_EXIT
    return GEN_SUCCESS;
}
