/*******************************************************************************
 * Copyright(c) <2016>, Volansys Technologies
 *
 * Description:
 * @file cc1310.c
 * @brief  
 *
 * @Author   - Volansys
 *
 *******************************************************************************
 *
 * @History
 *
 * Aug/08/2018 VT, Created
 ******************************************************************************/

#include <stdlib.h>
#include <unistd.h>
#include <mqueue.h>
#include <pthread.h>

#include "cc1310.h"
#include "uart.h"
#include "error.h"
#include "commonData.h"
#include "debug.h"

/*	Uart File Descriptor	*/
static int g_uartfd;

/*	Lock for Uart Communication	*/
static pthread_mutex_t g_DeviceLock;

/*	Flag for preventing creation of 
	multiple thread	for fetching Tpms data	*/
static bool g_fetchingTPMSData = false;

/*	Thread handle for fetchingTpmsdata	*/
static pthread_t g_fetchTPMSThreadID;

/*	Thread attributes for fetching Tpms data	*/
static pthread_attr_t g_fetchTPMSThreadAttr;

/*	Lock for fetching Tpms data */
static pthread_mutex_t g_fetchTPMSThreadLock;

static pthread_mutex_t fetchTPMSDataThreadAliveLock = PTHREAD_MUTEX_INITIALIZER;

static bool g_fetchTPMSDataThreadAliveStatus = true;

typedef struct {
    uint8_t sigNum;
    struct sigaction sig;
} signalInfo_t;

//void TpmsDataFetchCB(int sigNum, siginfo_t * sigInfo, void * unused);

void NULL_sa_SigAction(int sigNum, siginfo_t * sigInfo, void * unused);

static int8_t sendCommandToSub1GHz(const uint16_t command, const uint8_t *const payload, 
									const uint16_t payloadLength, UART_FRAME_t *readFrame)
{
	//FUNC_ENTRY
	int8_t readStatus = 0;
	uint8_t retryCount = 0;

	if(readFrame == NULL) {
		return FAILURE;
	}
	pthread_mutex_lock(&g_DeviceLock);
	do {
		uartWrite(g_uartfd, command, payload, payloadLength);
		readStatus = uartRead(g_uartfd, readFrame);
		if((readStatus == GEN_SUCCESS) && (readFrame->command == command)) {
			break;
		}
		retryCount++;
		readStatus = FAILURE;
	} while(retryCount < MAX_RETRY);
	pthread_mutex_unlock(&g_DeviceLock);

	return readStatus;
	//FUNC_EXIT
}

static int8_t sendCommand(const uint16_t command, const uint8_t *const payload,
                                    const uint16_t payloadLength, UART_FRAME_t *readFrame)
{
	int8_t commandStatus = 0;
	tpmsData_t tpms = {0};

//	commandStatus = sendCommandToSub1GHz(notifyCommand, &notifyPayload, 
//											notifyPayloadLength, readFrame);
//	if(commandStatus == GEN_SUCCESS) {
		switch(command) {
			case NORMAL_POWER_MODE :
			case LOW_POWER_MODE :
			case START_RF :
			case STOP_RF  :
			case ADD_SENSOR :
			case REMOVE_SENSOR :
				commandStatus = sendCommandToSub1GHz(command, payload, payloadLength, readFrame);
				if (commandStatus == GEN_SUCCESS) {
					CAB_DBG(CAB_GW_DEBUG, "Sub1 cmd :%d, payload[0]: %d", command, readFrame->payload[0]);
				}
//				if(commandStatus == GEN_SUCCESS) {
//					commandStatus = payload[0];
//				}
			break;
			case SEND_SENSOR_DATA :
				do {
					commandStatus = sendCommandToSub1GHz(command, payload, payloadLength, readFrame);
					if( (commandStatus != GEN_SUCCESS) || (readFrame->payloadLength <= 0) ) {
						break;
					}

					memcpy(&tpms.sensorID, readFrame->payload + SENSORID_OFFSET, sizeof(uint32_t));

					memcpy(&tpms.pressure, readFrame->payload + PRESSURE_OFFSET, sizeof(uint32_t));

					memcpy(&tpms.temperature, readFrame->payload + TEMPERATURE_OFFSET, sizeof(uint32_t));

					memcpy(&tpms.battery, readFrame->payload + BATTERY_OFFSET, sizeof(uint32_t));

					memcpy(&tpms.rssi, readFrame->payload + RSSI_OFFSET, sizeof(uint8_t));

					memcpy(&tpms.status, readFrame->payload + STATUS_OFFSET, sizeof(uint8_t));

					if(sendDataToDM != NULL) {
						sendDataToDM(&tpms);
					}
					/*	continue fetching data till data ends up and receive 
						payloadLength zero for no data	*/
				} while(1);
			break;
		}
//	}
	return commandStatus;
	//FUNC_EXIT
}


static int8_t pairTpmsSensor(uint32_t *sensorID, uint8_t numberofSensors, bool addSensor)
{
	//FUNC_ENTRY
	int8_t retVal = 0;
	uint16_t command = ADD_SENSOR;
	UART_FRAME_t readFrame[FRAME_SIZE] = {0};
	//uint8_t *payload = calloc(numberofSensors, sizeof(uint32_t));
	uint16_t payloadLength = 1 + (numberofSensors * sizeof(uint32_t) );
	uint8_t *payload = malloc(payloadLength);

	memcpy(payload, (void *)&numberofSensors, 1);
	memcpy(payload + 1, sensorID, payloadLength - 1);

	if(addSensor == false) {
		command = REMOVE_SENSOR;
	}
	retVal = sendCommand(command, payload, payloadLength, readFrame);

	free(payload);
	return retVal;
	//FUNC_EXIT
}

/** Start CC1310 RF module.
 *
 * This api will start RF of CC1310 module.
 *
 * @param[in]	vendorType	vendor type to initialize specific 
 *							vendor based configuration.
 *
 * @return  error code.
 */
static int8_t startCC1310(vendorType)
{
	//FUNC_ENTRY
	int8_t retVal = 0;
	uint8_t payload = vendorType;
	uint16_t payloadLength = sizeof(payload);
	UART_FRAME_t readFrame[FRAME_SIZE] = {0};

	retVal = sendCommand(START_RF, &payload, payloadLength, readFrame);
	return retVal;
}

/** Stop CC1310 RF module.
 *
 * This api will stop RF of CC1310 module. CC1310 will automatically 
 * deinitialze the initialized vendor module.
 *
 * @return  error code.
 */
static int8_t stopCC1310(void)
{
	//FUNC_ENTRY
	int8_t retVal = 0;
	uint16_t payloadLength = 0;
	UART_FRAME_t readFrame[FRAME_SIZE] = {0};

	retVal = sendCommand(STOP_RF, NULL, payloadLength, readFrame);

	return retVal;
}

#if 0
static int8_t registerSignal(void)
{
	int configfd;
	char buf[10];
	signalInfo_t signalInfo;
	signalInfo.sig.sa_flags = SA_SIGINFO;
	signalInfo.sigNum = SIG_UART;
	signalInfo.sig.sa_sigaction = TpmsDataFetchCB;

	sigaction(signalInfo.sigNum, &signalInfo.sig, NULL);

	/* kernel needs to know our pid to be able to send us a signal ->
     * we use debugfs for this -> do not forget to mount the debugfs!
     */
    configfd = open(DEBUG_FS_FILE, O_WRONLY);
    if(configfd < 0) {
        return FAILURE;
    }

    sprintf(buf, "%i", getpid());

    if (write(configfd, buf, strlen(buf) + 1) < 0) {
        return FAILURE;
    }

    fsync(configfd);
    close(configfd);
}

static int8_t signalDeRegister(void)
{
//FUNC_ENTRY
		signalInfo_t signalInfo;

		signalInfo.sig.sa_flags = SA_SIGINFO;
		signalInfo.sigNum = SIG_UART;
		signalInfo.sig.sa_sigaction = NULL_sa_SigAction;
		sigaction(signalInfo.sigNum, &signalInfo.sig, NULL);
}
#endif

static bool getFetchTPMSDataAliveStatus(void)
{
        bool status;
        pthread_mutex_lock(&fetchTPMSDataThreadAliveLock);
        status = g_fetchTPMSDataThreadAliveStatus;
        pthread_mutex_unlock(&fetchTPMSDataThreadAliveLock);
        return status;
}

static void setFetchTPMSDataAliveStatus(bool value)
{
        pthread_mutex_lock(&fetchTPMSDataThreadAliveLock);
        g_fetchTPMSDataThreadAliveStatus = value;
        pthread_mutex_unlock(&fetchTPMSDataThreadAliveLock);
}

bool fetchTPMSDataCallback(void)
{
        bool status = false;
        CAB_DBG(CAB_GW_TRACE, "In func : %s\n", __func__);
        if(getFetchTPMSDataAliveStatus() == true)
        {
                status = true;
        }

        setFetchTPMSDataAliveStatus(false);

        return status;
}

void* fetchTPMSData(void *arg)
{
	//FUNC_ENTRY

	int8_t retVal = 0;
	uint8_t *payload = NULL;
	uint16_t payloadLength = 0;
	returnCode_e retCode = GEN_SUCCESS;
	UART_FRAME_t readFrame[FRAME_SIZE] = {0};

	watchDogTimer_t newWatchdogNode;

	newWatchdogNode.threadId = syscall(SYS_gettid);
	newWatchdogNode.CallbackFn = fetchTPMSDataCallback;

	CAB_DBG(CAB_GW_INFO, "Fetch TPMS data thread id : %ld", syscall(SYS_gettid));
	retCode = threadRegister(&newWatchdogNode);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "Fetch TPMS data thread registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_REGISTRATION_FAIL, GW_ERR, "Fetch TPMS data thread registration failed");
        }

	retVal = sendCommand(SEND_SENSOR_DATA, payload, payloadLength, readFrame);

	pthread_mutex_lock(&g_fetchTPMSThreadLock);
	g_fetchingTPMSData = false;
	pthread_mutex_unlock(&g_fetchTPMSThreadLock);

	setFetchTPMSDataAliveStatus(true);

	retCode = threadDeregister(newWatchdogNode.threadId);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "Fetch TPMS data thread de-registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "Fetch TPMS data thread de-registration failed");
        }
	return NULL;
	//FUNC_EXIT
}

void NULL_sa_SigAction(int sigNum, siginfo_t * sigInfo, void * unused)
{
    //FUNC_ENTRY
    /*Nothing to do.*/
    //FUNC_EXIT
}

/** UART Interrupt handler
 *
 * This function will handle the uart interrupt by creating
 * a thread and that thread will fetch events from M4.
 *
 * @param[in]   sigNum			signal number
 * @param[in]   sigInfo			signal information
 * @param[in]   unused			pointer unused
 * @return		void
 */
void CC1310DataFetchCB(int sigNum, siginfo_t * sigInfo, void * unused)
{
	//FUNC_ENTRY
	int8_t retVal;

	pthread_mutex_lock(&g_fetchTPMSThreadLock);
	if(g_fetchingTPMSData == false) {
		CAB_DBG(CAB_GW_DEBUG,"$\r\n");
			/* create thread */
		retVal = pthread_create(&g_fetchTPMSThreadID, &g_fetchTPMSThreadAttr, fetchTPMSData, NULL);
		if (retVal != 0) {
			CAB_DBG(CAB_GW_ERR,"Error creating Thread");
	    }
		else {
			g_fetchingTPMSData = true;
		}
	}
	else {
		CAB_DBG(CAB_GW_DEBUG,"#\r\n");
	}
	pthread_mutex_unlock(&g_fetchTPMSThreadLock);
	//FUNC_EXIT
}

int8_t initCC1310(const rfInfo_t sub1Info)
{
	int8_t retVal;
	//FUNC_ENTRY
	if((sub1Info.vendorType < 0) || (sub1Info.vendorType >= MAX_VENDORS)) {
		/*	@TODO return error code	for invald vendor type	*/
		return FAILURE;
	}

	/*	UART Lock	*/
    retVal = pthread_mutex_init(&g_DeviceLock, NULL);
    if (retVal != 0) {
        return FAILURE;
    }

	pthread_mutex_lock(&g_DeviceLock);
	retVal = uartInit(&g_uartfd, UART_BAUDRATE_AVETECH, UART_PORT_AVETECH);
	if(retVal != GEN_SUCCESS) {
		pthread_mutex_unlock(&g_DeviceLock);
		return FAILURE;
	}
	pthread_mutex_unlock(&g_DeviceLock);


	/*	lock for fetch tpms data thread	*/
    retVal = pthread_mutex_init(&g_fetchTPMSThreadLock, NULL);
    if (retVal != 0) {
        return FAILURE;
    }

    /* Initialize thread attributes */
    pthread_attr_init(&g_fetchTPMSThreadAttr);

    /* set thread attributes as detached */
    pthread_attr_setdetachstate(&g_fetchTPMSThreadAttr, PTHREAD_CREATE_DETACHED);

	/*	callback to send tpms data to DM	*/
	sendDataToDM = sub1Info.sendTpmsDataToDM;

	g_fetchingTPMSData = false;

	/*	register signal to receive tpms data from CC1310 on interrupt.	*/	
	//registerSignal();

	/*	start CC1310 RF module	*/
	retVal = startCC1310(sub1Info.vendorType);

	return retVal;
	//FUNC_EXIT
}

int8_t deInitCC1310(void)
{
	//FUNC_ENTRY
	int8_t retVal;
	retVal = stopCC1310();

	//signalDeRegister();

	g_fetchingTPMSData = false;

	pthread_mutex_destroy(&g_fetchTPMSThreadLock);

	pthread_mutex_lock(&g_DeviceLock);
	uartDeInit(g_uartfd);
	pthread_mutex_unlock(&g_DeviceLock);

	pthread_mutex_destroy(&g_DeviceLock);
	return retVal;
	//FUNC_EXIT
}

int8_t addTpmsSensor(void *sensorID)
{
	/*	@TODO: for multiple sensors sensorId will have data for numberOfSensors and sensorID	*/
	int8_t retValue;
	uint8_t	numberOfSensors = 1;
	retValue = pairTpmsSensor((uint32_t *)sensorID, numberOfSensors, true);
	return retValue;
}

int8_t removeTpmsSensor(void *sensorID)
{
	/*	@TODO: for multiple sensors sensorId will have data for numberOfSensors and sensorID	*/
	int8_t retValue;
	uint8_t numberOfSensors = 1;
	retValue = pairTpmsSensor(sensorID, numberOfSensors, false);
	return retValue;
}
