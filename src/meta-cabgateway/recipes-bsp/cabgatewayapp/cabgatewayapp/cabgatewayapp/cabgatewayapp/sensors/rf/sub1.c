/*****************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *                                                       
 * Description:
 * @file : sub1.c
 * @brief : This file is an interface between Device manager and 
 * various vendor specific tpms sensors.
 *
 * @Author     - Volansys
 *****************************************************************
 * History
 *
 * Aug/27/2018, VT , Created
 ****************************************************************/
#include <string.h>
#include "sub1.h"
#include "error.h"
#include "cc1310.h"
#include "xbr.h"
#include "gpio.h"

/*	flag for init status	*/
static bool g_sub1InitDone = false;

/*	local copy of vendor specific information	*/
static rfInfo_t g_tpmsInfo = {0};

int8_t initializeSub1(rfInfo_t *tpmsInfo)
{
		int8_t retValue = GEN_SUCCESS;

		/* check if sub1 is already initialized	*/
		if(g_sub1InitDone == true) {
			return FAILURE;
		}

		/*	- Take local copy of vendor type and callback,
			- Pass add/remove api's to DM,
			- Initialize the vendor specific sub1 module,
			- Change the sub1 init status.	*/
		switch(tpmsInfo->vendorType) {
				case VENDOR_AVE_TECH :
						{
								/*	Enable Sub1 Power	*/
								gpioInit(SUB1_POWER_GPIO, OUT);
								gpioWrite(SUB1_POWER_GPIO, true);

								/*	Reset Sub1 Power	*/
								gpioInit(SUB1_RESET_GPIO, OUT);
								gpioWrite(SUB1_RESET_GPIO, false);
								usleep(1000);
								gpioWrite(SUB1_RESET_GPIO, true);

								g_tpmsInfo.addSensor = tpmsInfo->addSensor = addTpmsSensor;
								g_tpmsInfo.removeSensor = tpmsInfo->removeSensor = removeTpmsSensor;
								g_tpmsInfo.sendTpmsDataToDM = tpmsInfo->sendTpmsDataToDM;
								g_tpmsInfo.vendorType = tpmsInfo->vendorType;

								retValue = initCC1310(g_tpmsInfo);
								if(retValue == GEN_SUCCESS) {
									g_sub1InitDone = true;
								}
						}
						break;
				case VENDOR_PRESSURE_PRO :
						{
								/*	Enable Sub1 Power	*/
								gpioInit(SUB1_POWER_GPIO, OUT);
								gpioWrite(SUB1_POWER_GPIO, true);

								g_tpmsInfo.addSensor = tpmsInfo->addSensor = addPressureProSensor;
								g_tpmsInfo.removeSensor = 
											tpmsInfo->removeSensor = removePressureProSensor;
								g_tpmsInfo.sendTpmsDataToDM = tpmsInfo->sendTpmsDataToDM;
								g_tpmsInfo.vendorType = tpmsInfo->vendorType;

								retValue = initXBR(g_tpmsInfo);
								if(retValue == GEN_SUCCESS) {
									g_sub1InitDone = true;
								}
						}
						break;
				default :
						{
								/*	Invalid Vendor	*/
								retValue = FAILURE;
						}
						break;
		}
		return retValue;
}

int8_t deInitializeSub1(void)
{
		int8_t retVal = GEN_SUCCESS;

		/*	check if sub1 is not initialized	*/
		if(g_sub1InitDone == false) {
				return FAILURE;
		}

		/*	- Deinitialize vendor specific sub1 module,
			- Clear the local copy of sub1 information,
			- Change the init status of sub1.	*/
		switch(g_tpmsInfo.vendorType) {
				case VENDOR_AVE_TECH :
						{
								/*	Initialize the vendor specific sub1 module	*/
								deInitCC1310();
								memset(&g_tpmsInfo, 0, sizeof(g_tpmsInfo));
								g_sub1InitDone = false;

								/*	Reset Sub1 Power	*/
								gpioWrite(SUB1_RESET_GPIO, false);
								usleep(1000);
								gpioWrite(SUB1_RESET_GPIO, true);

								gpioDeInit(SUB1_RESET_GPIO);
								gpioWrite(SUB1_POWER_GPIO, false);
								gpioDeInit(SUB1_POWER_GPIO);
						}
						break;
				case VENDOR_PRESSURE_PRO :
						{
								deInitXBR();
								memset(&g_tpmsInfo, 0, sizeof(g_tpmsInfo));
								g_sub1InitDone = false;
								gpioWrite(SUB1_POWER_GPIO, false);
								gpioDeInit(SUB1_POWER_GPIO);
						}
						break;
				default :
						{
								/*	Invalid Vendor	*/
								retVal = FAILURE;
						}
						break;
		}
		return retVal;
}

returnCode_e selfTestSub1(void) {
	int8_t retValue;
    uint32_t sensorID = 0x819CAAA7;
	retValue = g_tpmsInfo.addSensor(&sensorID);
	return retValue;
}

void Sub1DataFetchCB(int sigNum, siginfo_t * sigInfo, void * unused)
{
	switch(g_tpmsInfo.vendorType) {
		case VENDOR_AVE_TECH :
			return CC1310DataFetchCB(sigNum,sigInfo,unused);
		case VENDOR_PRESSURE_PRO :
			return XbrDataFetchCB(sigNum,sigInfo,unused);
		default:
			break;

	}
}
