/*************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : accel.h
 * @brief (This file contains the accelerometer information like includes, macros, enums,
 *         structures, global/extern variables, callbacks, and API prototypes.)
 *
 * @Author     - VT
 ************************************************************************************************
 * History
 *
 * Mar/04/2019, VT , First Draft for sample application
 ************************************************************************************************/

#ifndef __ACCEL_H
#define __ACCEL_H

#ifdef ACCEL_SUPPORT
void accelInterrupt1Handle(int sig);

void accelInterrupt2Handle(int sig);
#endif

returnCode_e accelInit();
returnCode_e accelReadTemp(float *accelTemperature);

#endif
