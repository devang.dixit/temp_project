/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : gpsDeviceMgr.c
 * @brief : This file provides GPS device management APIs
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Aug/28/2018, VT , Stub APIs
 * Aug/17/2018, VT , First Draft
 **************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "configuration.h"
#include "commonData.h"
#include "deviceMgr.h"
#include "gpsDeviceMgr.h"
#include <errno.h>
#include "debug.h"

/**
 * @struct gpsCtrlBlock
 * @brief Core Handler structure for GPS DM control
 *
 *        This strucure to be used for device controling and will be
 *  defined for each interfaces.
 **/
typedef struct gpsCtrlBlock {
    uint8_t isInit;
    pthread_mutex_t gpsDevLock;  /**< GPS DM API mutex */
} gpsCtrlBlock_t;

gpsCtrlBlock_t gpsCtrl = {.isInit = 0};


/**
 * GPS Add device.
 * */
returnCode_e gpsAddDevice(void * data)
{
    FUNC_ENTRY
    /* NOTE: Kept empty for now to avoid any seg fault in upper application. */
    FUNC_EXIT
    return GEN_API_NOT_AVAIL;
}

/**
 * GPS Remove device.
 * */
returnCode_e gpsRemoveDevice(void * data)
{
    FUNC_ENTRY
    /* NOTE: Kept empty for now to avoid any seg fault in upper application. */
    FUNC_EXIT
    return GEN_API_NOT_AVAIL;
}


/**
 *  Get GPS device data.
 * */
returnCode_e gpsGetDiagData(void * data)
{
    gpsDiagInfo_t *tempGPSDiagInfo;
    returnCode_e retCode = GEN_SUCCESS;
    FUNC_ENTRY
    if (data == NULL) {
        retCode = GEN_INVALID_ARG;
        return retCode;
    }

    tempGPSDiagInfo = (gpsDiagInfo_t *)data;
    pthread_mutex_lock(&(gpsCtrl.gpsDevLock));
    if (gpsCtrl.isInit != 1) {
        retCode = GEN_NOT_INIT;
        pthread_mutex_unlock(&(gpsCtrl.gpsDevLock));
        return retCode;
    }

    
    retCode = getGPSTime(&(tempGPSDiagInfo->timestamp));                   //GPS data timestamp
    if (retCode != GEN_SUCCESS) {
	CAB_DBG(CAB_GW_ERR, "Get GPS time failed with code %d", retCode);
        pthread_mutex_unlock(&(gpsCtrl.gpsDevLock));
        return retCode;
    }

    retCode = getGPSData((gpsData_t *)&(tempGPSDiagInfo->gpsData));
    if (retCode != GEN_SUCCESS) {
	CAB_DBG(CAB_GW_ERR, "Get GPS data failed with code %d", retCode);
        pthread_mutex_unlock(&(gpsCtrl.gpsDevLock));
        return retCode;
    }

    retCode = getGPSNoOfSatelite(&(tempGPSDiagInfo->noOfSatInView));              //No of satellite views
    if (retCode != GEN_SUCCESS) {
	CAB_DBG(CAB_GW_ERR, "Get GPS No Sat failed with code %d", retCode);
        pthread_mutex_unlock(&(gpsCtrl.gpsDevLock));
        return retCode;
    }

    retCode = getGPSQuality(&(tempGPSDiagInfo->fixQuality));                //Fix quality
    if (retCode != GEN_SUCCESS) {
	CAB_DBG(CAB_GW_ERR, "Get GPS Fix qual failed with code %d", retCode);
        pthread_mutex_unlock(&(gpsCtrl.gpsDevLock));
        return retCode;
    }

    pthread_mutex_unlock(&(gpsCtrl.gpsDevLock));
    FUNC_EXIT
    return retCode;
}

/**
 *  Get GPS device data.
 * */
returnCode_e gpsGetDeviceData(void * data)
{
    returnCode_e retCode = GEN_SUCCESS;
    FUNC_ENTRY
    if (data == NULL) {
        retCode = GEN_INVALID_ARG;
        return retCode;
    }

    pthread_mutex_lock(&(gpsCtrl.gpsDevLock));
    if (gpsCtrl.isInit != 1) {
        retCode = GEN_NOT_INIT;
        pthread_mutex_unlock(&(gpsCtrl.gpsDevLock));
        return retCode;
    }


    retCode = getGPSData((gpsData_t *)data);
    pthread_mutex_unlock(&(gpsCtrl.gpsDevLock));
    FUNC_EXIT
    return retCode;
}

/**
 *  Set GPS device data.
 * */
returnCode_e gpsSetDeviceData(void * data)
{
    FUNC_ENTRY
    /* NOTE: Kept empty for now to avoid any seg fault in upper application. */
    FUNC_EXIT
    return GEN_API_NOT_AVAIL;
}

/**
 * Init GPS interface API
 **/
returnCode_e gpsInitInterface(interfaceManagerHandler_t *gpsHandler, void * initConfig, interruptDataCBType callback)
{
    returnCode_e retCode = GEN_SUCCESS;

    FUNC_ENTRY
    if(gpsHandler == NULL) {
        return GEN_INVALID_ARG;
    }

    CAB_DBG(CAB_GW_INFO, "Initializing GPS interface");

    gpsCtrl.gpsDevLock = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;

    pthread_mutex_lock(&(gpsCtrl.gpsDevLock));
    gpsHandler->addDevice = gpsAddDevice;
    gpsHandler->removeDevice = gpsRemoveDevice;
    gpsHandler->getData = gpsGetDeviceData;
    gpsHandler->setData = gpsSetDeviceData;
    gpsHandler->getDiagData = gpsGetDiagData;
    retCode = initGPS();
    if(retCode != GEN_SUCCESS && retCode != MODULE_ALREADY_INIT)  {
    	gpsHandler->addDevice = NULL;
    	gpsHandler->removeDevice = NULL;
    	gpsHandler->getData = NULL;
    	gpsHandler->setData = NULL;
    	gpsHandler->getDiagData = NULL;
    	pthread_mutex_unlock(&(gpsCtrl.gpsDevLock));
	retCode = DM_INTF_INIT_FAIL;
        return retCode;
    }
    gpsCtrl.isInit = 1;
    pthread_mutex_unlock(&(gpsCtrl.gpsDevLock));
    if (retCode == MODULE_ALREADY_INIT) {
	retCode = GEN_SUCCESS;
    }
    FUNC_EXIT
    return retCode;
}


/**
 * DeInit GPS interface API
 **/
returnCode_e gpsDeInitInterface(interfaceManagerHandler_t *gpsHandler)
{
    returnCode_e retCode = GEN_SUCCESS;
    FUNC_ENTRY
    if (gpsHandler == NULL) {
	return GEN_INVALID_ARG;
    }

    CAB_DBG(CAB_GW_INFO, "Deinitializing GPS interface");

    pthread_mutex_lock(&(gpsCtrl.gpsDevLock));
    if(gpsCtrl.isInit != 1) {
        retCode = GEN_NOT_INIT;
    	pthread_mutex_unlock(&(gpsCtrl.gpsDevLock));
        return retCode;
    }

    deinitGPS();
    gpsHandler->addDevice = NULL;
    gpsHandler->removeDevice = NULL;
    gpsHandler->getData = NULL;
    gpsHandler->setData = NULL;
    gpsHandler->getDiagData = NULL;
    gpsCtrl.isInit = 0;
    pthread_mutex_unlock(&(gpsCtrl.gpsDevLock));
    FUNC_EXIT

    return retCode;
}

