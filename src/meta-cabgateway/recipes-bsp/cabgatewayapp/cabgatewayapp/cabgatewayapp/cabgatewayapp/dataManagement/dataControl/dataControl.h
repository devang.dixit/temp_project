/*******************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 * 
 * Description:
 * @file dataControl.h
 * @brief
 *
 * Header file for dataControl.c
 * 
 * @Author       - Volansys
 *
 *******************************************************************************
 *
 * History
 *                      
 * @date Sep/10/2018, VT, Created
 *
 ******************************************************************************/

#ifndef DATA_CONTROL_H
#define DATA_CONTROL_H

/*******************
 * Includes
 *******************/
#include "commonData.h"
#include "dataStorage.h"
#include "watchdog.h"
#include "syscall.h"

/*************************
 * Defines
 *************************/
#define PERMS 0644

/*	Key for generating Message Queue	*/
#define	KEY	1234

/*	Maximum message size that can be queued	*/
#define	MAX_MSG_SIZE	(sizeof(tpmsEvent_t))	

#define	MAX_CLOUD_RETRY	4

/***********************
 * Typedefs
 ***********************/

/***********************
 * Structures
 ***********************/

/*      Message Queue information       */
typedef struct {
        long mtype;
        zipType_e zipType;
        char mtext[MAX_MSG_SIZE];
} sdcMsg_t;
/***********************
 *	ENUMS
 ***********************/
/*	@TODO: Later remove this enum and use enum defined by M&T	*/
typedef enum {
    ALERT_EV = 1,
    TRANSMIT_EV,
    GROUP_EV,
    DEINIT_EV,
    WATCHDOG_EV,
    EVENT_MAX_EV
} eventType_e;

typedef enum {
    DATA_RDIAG_ZIP = 1,
    DATA_STORAGE_ZIP,
    DATA_CONFIG_ZIP,
    DATA_ZIP_TYPE_MAX
} DataFilezipType_e;

/****************************************
 ************* CALLBACKS ****************
 ****************************************/
/*It is used to register callback to notify when the zip file is uploaded*/
typedef void (*uploadzipNotifCB)(DataFilezipType_e, void *);

/***********************
 * Global Variables
 ***********************/

/************************
 * Function Prototypes
 ************************/
/** Initialize system data control module.
 *
 * This api will initialize system data control 
 * and storage module.
 *
 * @param[in]	totalZipPackage	total zip package system will allow.
 * @param[in]	notifyCB	notify callback to M&T module.
 *
 * @return int8_t	error code.
 */ 
returnCode_e initSDC(uint16_t totalZipPackage, uploadzipNotifCB notifyCB, char *swVersion, key_t key, char * dataDirName);

/** DeInitialize system data control module.
 *
 * This api is used to deinitialize system data control 
 * and storage module.
 *
 * @return 	error code.
 */ 
returnCode_e deInitSDC(void);

/** Post an Event to System Data Control module.
 *
 * This api is used to enqueue an event to System Data Control
 * module by M&T. It will process the event accordingly.
 *
 * @param[in]	event	event type to handle.
 * @param[in]	data	data for event being handled.
 *
 * @return 	error code.
 */ 
/*	@TODO: change the type of 1st argument to enum provided by M&T	*/
returnCode_e reportEventToSDC(eventType_e event, const void *const data, zipType_e zipType);

/** Store all the system events.
 *
 * This api is used to store all system events to permanent storage.
 *
 * @param[in]	data		data to store.
 * @param[in]	eventType	type of data.
 *
 * @return 	error code.
 */ 
returnCode_e storeDataToSDC(void *data, sysDataType_e eventType);

/** Delete all files located in folder mentioned by path
 *
 * This api is used to delete config files and data storage holding logs
 *
 * @param[in]	data		path to the directory to be deleted
 *
 * @return 	error code.
 */ 
returnCode_e deleteConfigAndDataStorage(char *path);

#endif  /* '#endif' of DATA_CONTROL_H */
