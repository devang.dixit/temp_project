/********************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file dataStorage.c
 * @brief (This file contains APIs defination of data storage module.)
 *
 * @Author     - VT
 **********************************************************************************************
 * History
 *
 * July/24/2018, VT , First Draft
 **********************************************************************************************/

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include "sysLogStorage.h"
#include "debug.h"
#include "cloud.h"
#include "commonData.h"
#include <semaphore.h>

/****************************************
 ******** STATIC VARIABLES **************
 ****************************************/
static pthread_mutex_t g_lock_syslog = PTHREAD_MUTEX_INITIALIZER;  //Initialize mutex variable
static uint16_t g_zipLimit_syslog;
static char sys_outDirName[MAX_DIRNAME_LEN_SYS] = {'\0'};
static char eventSumFileName_sys[MAX_FILENAME_LEN_SYS] = {'\0'};
static sem_t *syslog_semlock;


/****************************************
 ******** FUNCTION DEFINITIONS **********
 ****************************************/
returnCode_e Sem_DeInit()
{
	int return_val;
	return_val = sem_close(syslog_semlock);
	if (return_val == -1) {
		CAB_DBG(CAB_GW_ERR, "Sem Close Failed");
		return -1; 
	}   

	return_val = sem_unlink("/syslog_sem");
	if (return_val == -1) {
		CAB_DBG(CAB_GW_ERR, "Sem Unlink Failed");
		return -1; 
	}   
	return GEN_SUCCESS;
}


returnCode_e semLockWithRetry() 
{
    struct timespec ts;
    int count = 1;
    int ret = 0;

    do {
	    if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {
		    return GEN_API_FAIL;
	    }
	    ts.tv_sec += 3;
	    ret = sem_timedwait(syslog_semlock, &ts);
	    count++;
    } while(ret != 0 && count < 11);

    if (count == 10) {
	    ret  = Sem_DeInit();
	    if (ret != 0) {
		    CAB_DBG(CAB_GW_ERR, "sem deinit failed");
		    return GEN_API_FAIL;
	    }

	    if ((syslog_semlock = sem_open ("/syslog_sem", O_CREAT, 0660, 1)) == SEM_FAILED) {
		    CAB_DBG(CAB_GW_ERR, "sem_open failed");
		    return GEN_API_FAIL;
	    }

	    if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {
		    return GEN_API_FAIL;
	    }
	    ts.tv_sec += 3;

	    ret = sem_timedwait(syslog_semlock, &ts);
	    if (ret != 0) {
		    CAB_DBG(CAB_GW_ERR, "sem timed wait failed");
		    return GEN_API_FAIL;
	    }
    }
    return GEN_SUCCESS;
}

/******************************************************************
 * @brief  (This API is used to read the content of status file.
 *
 * @param[IN] fileName* (Pointer to File Name)
 * @param[OUT] status*  (Pointer to status of command)
 *
 * @return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e readStatusFile(char *fileName, uint8_t *status)
{
    FUNC_ENTRY

    FILE *statusFile;
    uint8_t dataRead;
    int ret;
    
    /*Input argument validation*/
    if(fileName == NULL) {
       CAB_DBG(CAB_GW_ERR, "Invalid Filename");
       return GEN_NULL_POINTING_ERROR;
   }

    //opening status file for read
   statusFile = fopen(fileName, "r");
   if(statusFile == NULL) {
       CAB_DBG(CAB_GW_ERR, "Failed opening file %s", fileName);
       return FILE_OPEN_FAIL;
   }

    //reading value of status from file
   ret = fscanf(statusFile, "%d", &dataRead);
   if(ret == 0 || ret == EOF) {
       CAB_DBG(CAB_GW_ERR, "Failed reading from file %s", fileName);
       fclose(statusFile);
       return FILE_READ_ERROR;
   }

   *status = dataRead;

   CAB_DBG(CAB_GW_DEBUG, "Zip file status :%u", *status);
    //close status file
   fclose(statusFile);

   FUNC_EXIT
   return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (This API is used to read event summary file which contains all sensors summary)
 *
 *@param[IN] logSummary_t* (Pointer to structure of event summary data)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e readEventSummary_sys(logSummary_t * eventSumm)
{
    FUNC_ENTRY

    int summaryFile;                                //file descriptor
    uint32_t ret;

    /*Input argument validation*/
    if(eventSumm == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error");
        return GEN_NULL_POINTING_ERROR;
    }

    //opening event summary file for read
    summaryFile = open(eventSumFileName_sys, O_RDONLY, 0644);
    if(summaryFile == -1) {
        CAB_DBG(CAB_GW_ERR, "Failed to open %s file", eventSumFileName_sys);
        return FILE_OPEN_FAIL;
    }

    //reading event summary structure data from file
    ret = read(summaryFile, eventSumm, sizeof(logSummary_t));
    if(ret == -1) {
        CAB_DBG(CAB_GW_ERR, "Failed to read %s file", eventSumFileName_sys);
        close(summaryFile);
        return FILE_READ_ERROR;
    }
    //close event summary file
    close(summaryFile);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (This API is used to write event summary file which contains all sensors summary)
 *
 *@param[IN] logSummary_t* (Pointer to structure of event summary data)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e writeEventSummary_sys(logSummary_t * eventSumm)
{
    FUNC_ENTRY

    int outFile;                               //file descriptor
    uint32_t ret;

    /*Input argument validation*/
    if(eventSumm == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error");
        return GEN_NULL_POINTING_ERROR;
    }

    //opening event summary file for write
    outFile = open(eventSumFileName_sys, O_WRONLY | O_CREAT, 0644);
    if(outFile == -1) {
        CAB_DBG(CAB_GW_ERR, "Failed to open %s file", eventSumFileName_sys);
        return FILE_OPEN_FAIL;
    }

    CAB_DBG(CAB_GW_DEBUG, "Zip file cnt :%d", g_logeventSumm.zipFileCnt);

    //writing event summery structure data to file
    ret = write(outFile, eventSumm, sizeof(logSummary_t));
    if(ret == -1) {
        CAB_DBG(CAB_GW_ERR, "Failed to write %s file", eventSumFileName_sys);
        close(outFile);
        return FILE_WRITE_ERROR;
    }

    fsync(outFile);
    //close event summary file
    close(outFile);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (This API is used to find the oldest zip file name)
 *
 *@param[IN]  None
 *@param[OUT] void * (Indicates pointer to string)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e countNoOfFile_sys(char * dirName, char *checkExt, uint32_t *fileCnt)
{
    FUNC_ENTRY

    struct dirent *dp;                  // Pointer for directory entry
    char ext[5];                        //to get extention of file
    uint8_t len, loopCnt;

    if (dirName == NULL || checkExt == NULL || fileCnt == NULL) {
	    CAB_DBG(CAB_GW_ERR, "Invalid argument in count No of files");
	    return GEN_API_FAIL;
    }

    // opendir() returns a pointer of DIR type.
    DIR *dr = opendir(dirName);

    if(dr == NULL) {  // opendir returns NULL if couldn't open directory
        CAB_DBG(CAB_GW_ERR, "Failed to open directory");
        return DIR_OPEN_FAIL;
    }

    *fileCnt = 0;
    // for readdir()
    while((dp = readdir(dr)) != NULL) {
        if(strncmp(dp->d_name, ".", 1) == 0 || strncmp(dp->d_name, "..", 2) == 0 || strncmp(dp->d_name, "log", 3) == 0)
            continue;
        len = strlen(dp->d_name);
        len -= 4;
        for(loopCnt = 0; dp->d_name[len] != '\0'; loopCnt++) {
            ext[loopCnt] = dp->d_name[len];
            len++;
        }
        if(strncmp(ext, checkExt, 4) == 0) {
	    *fileCnt = *fileCnt + 1;
        CAB_DBG(CAB_GW_DEBUG, "File name: %s", dp->d_name);
        }
    }
    closedir(dr);
    return GEN_SUCCESS;
}
/******************************************************************
 *@brief  (This API is used to find the oldest zip file name)
 *
 *@param[IN]  None
 *@param[OUT] void * (Indicates pointer to string)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e findOldestFile_sys(void * fileName)
{
    FUNC_ENTRY

    struct dirent *dp;                  // Pointer for directory entry
    time_t oldesttime = 0;              //timestamp for oldest file
    struct stat statbuf;                //to get stat of file
    char buffer[MAX_FILENAME_LEN_SYS];      //to temporary store file name
    char oldestFile[MAX_FILENAME_LEN_SYS];	//to store oldest file name
    char ext[5];                        //to get extention of file
    uint8_t len, loopCnt;
    fileStorageInfo_t tempFileStorageInfo = *(fileStorageInfo_t*)fileName;

    if (sys_outDirName[0] == '\0') {
	    CAB_DBG(CAB_GW_ERR, "Output direcotry is not set");
	    return GEN_API_FAIL;
    }

    // opendir() returns a pointer of DIR type.
    DIR *dr = opendir(sys_outDirName);

    if(dr == NULL) {  // opendir returns NULL if couldn't open directory
        CAB_DBG(CAB_GW_ERR, "Failed to open directory");
        return DIR_OPEN_FAIL;
    }

    // for readdir()
    while((dp = readdir(dr)) != NULL) {
        if(strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0)
            continue;
        len = strlen(dp->d_name);
        len -= 4;
        for(loopCnt = 0; dp->d_name[len] != '\0'; loopCnt++) {
            ext[loopCnt] = dp->d_name[len];
            len++;
        }
        if(strncmp(ext, ".zip", 4) == 0) {
            snprintf(buffer, sizeof(buffer), "%s/%s", sys_outDirName, dp->d_name);
            lstat(buffer, &statbuf);
            if((statbuf.st_mtime < oldesttime) || (oldesttime == 0)) {
                snprintf(oldestFile, sizeof(oldestFile), "%s", dp->d_name);
                oldesttime = statbuf.st_mtime;
            }
        }
    }

    snprintf(tempFileStorageInfo.filename, sizeof(tempFileStorageInfo.filename), "%s", oldestFile);
    snprintf(tempFileStorageInfo.storagePath, sizeof(tempFileStorageInfo.storagePath), "%s", sys_outDirName);
    memcpy(fileName, &tempFileStorageInfo, sizeof(fileStorageInfo_t));
    CAB_DBG(CAB_GW_DEBUG, "Temp file name : %s", (char *)fileName);

    closedir(dr);
    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e initSysLogModule(uint16_t noOfPackage)
{
    FUNC_ENTRY

    uint8_t status = GEN_SUCCESS;
    uint32_t zipIndex = 0;
    struct stat st = {0};
    uint32_t zipFileCnt = 0;
    char checkExt[] = ".zip";
    returnCode_e retCode = GEN_SUCCESS;
    fileStorageInfo_t tempFileStorageInfo;

  
    strcpy(sys_outDirName, DIR_PATH_SYSLOG);

    if (snprintf(eventSumFileName_sys, sizeof(eventSumFileName_sys), FILE_PATH_EVENT_SUMM, DIR_PATH_SYSLOG) <= 0) {
	    CAB_DBG(CAB_GW_ERR, "eve summary file set failed");
	    return MODULE_INIT_FAIL;
    }

    CAB_DBG(CAB_GW_INFO, "Initializing syslog storage");


    if ((syslog_semlock = sem_open ("/syslog_sem", O_CREAT, 0660, 1)) == SEM_FAILED) {
	    CAB_DBG(CAB_GW_ERR, "sem_open failed");
	    return SEM_FAILED;
    }

    retCode = semLockWithRetry();
    if (retCode != GEN_SUCCESS) {
	    CAB_DBG(CAB_GW_ERR, "Sem wait failed with code %d", retCode);
	    return MODULE_INIT_FAIL;
    }

    //Copy no of package file into global variable
    if(noOfPackage == 0) {
        //unlock to protect event summary data
    	sem_post(syslog_semlock);
	CAB_DBG(CAB_GW_ERR, "Invalid argument");
        return GEN_NULL_POINTING_ERROR;
    }

    g_zipLimit_syslog = noOfPackage;

    //To check for the dataStorage directory and if it not exist then create it
    if(stat(sys_outDirName, &st) == -1) {
        if((mkdir(sys_outDirName, 0755)) == -1) {
            CAB_DBG(CAB_GW_ERR, "dataStorage directory not created");
            //unlock to protect event summary data
    	    sem_post(syslog_semlock);
            return DIR_CREATE_FAIL;
        }
    }

    //If event summary file does not exist
    if(access(eventSumFileName_sys, F_OK) == -1) {
        g_logeventSumm = (logSummary_t) {
            .zipFileCnt = 0
        };
        status = writeEventSummary_sys(&g_logeventSumm);       //update event summary file
        if(GEN_SUCCESS != status) {
            CAB_DBG(CAB_GW_ERR, "Failed to write initial data to %s file", eventSumFileName_sys);
        }
    } else {                                           //If event summary file exist
        status = readEventSummary_sys(&g_logeventSumm);       //read event summary file
        if(GEN_SUCCESS != status) {
            CAB_DBG(CAB_GW_ERR, "Failed to read existing data from %s file", eventSumFileName_sys);
        }
    }

    /* correct corrupted evebt summary and clean up data dir if zip files are more than limit*/
    status = countNoOfFile_sys(sys_outDirName, checkExt, &zipFileCnt);
    if (GEN_SUCCESS != status) {
	    CAB_DBG(CAB_GW_ERR, "failed to get zip file count with code :%d", status);
    } else {
	if (zipFileCnt > g_zipLimit_syslog) {
		CAB_DBG(CAB_GW_ERR, "more zip than limit, cleaning extra old zip files");
		for (zipIndex = 0; zipIndex <  (zipFileCnt - g_zipLimit_syslog); zipIndex++) {
			status = findOldestFile_sys((void*)&tempFileStorageInfo);
			if(status != GEN_SUCCESS) {
				CAB_DBG(CAB_GW_ERR, "Not getting oldest file name");
				/* unlock to protect event summary data */
    				sem_post(syslog_semlock);
				return FILE_NOT_FOUND;
			}    
			/* unlock to protect event summary data */
    			sem_post(syslog_semlock);
			status = deleteSyslogPackage((void*)&tempFileStorageInfo);
    			retCode = semLockWithRetry();
    			if (retCode != GEN_SUCCESS) {
    			        CAB_DBG(CAB_GW_ERR, "Sem wait failed with code %d", retCode);
    			        return GEN_API_FAIL;
    			}
			if(status != GEN_SUCCESS) {
				CAB_DBG(CAB_GW_ERR, "Delete package error");
				/* unlock to protect event summary data */
    				sem_post(syslog_semlock);
				return FILE_DELETE_ERROR;
			}    
		}
		g_logeventSumm.zipFileCnt = g_zipLimit_syslog;
	} else if (zipFileCnt != g_logeventSumm.zipFileCnt) {
    	        g_logeventSumm.zipFileCnt = zipFileCnt;
    	}
    	status = writeEventSummary_sys(&g_logeventSumm);
    	if (GEN_SUCCESS != status) {
    	    CAB_DBG(CAB_GW_ERR, "write event sum failed due to error code : %d", status);
    	}
    }

    //unlock to protect event summary data
    sem_post(syslog_semlock);
    
    FUNC_EXIT
    return status;
}

returnCode_e deInitSyslogModule(void)
{
    returnCode_e retCode = GEN_SUCCESS;
    FUNC_ENTRY

    CAB_DBG(CAB_GW_INFO, "Deinitializing Data storage module");
    /*Any ongoing file operation will not allow to take this lock untill
      it completes. Once completed, no other thread will spawn to use event
     module as all other modules must have been deinit before */
    retCode = semLockWithRetry();
    if (retCode != GEN_SUCCESS) {
	    CAB_DBG(CAB_GW_ERR, "Sem wait failed with code %d", retCode);
	    return GEN_API_FAIL;
    }
    sem_post(syslog_semlock);

    //destroying lock
    pthread_mutex_destroy(&g_lock_syslog);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (This API is used to get syslogs from RAM and put into data partition)
 *
 *@param[IN] None
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e getAndMergeLogsFromRAM()
{
    FUNC_ENTRY

    int32_t status=-1;
    char command[MAX_LEN_COMMAND_SYS]={0};
    uint8_t cmdStatus=-1;
    returnCode_e retCode = GEN_SUCCESS;

    //lock to protect event summary data
    retCode = semLockWithRetry();
    if (retCode != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "Sem wait failed with code %d", retCode);
        return GEN_API_FAIL;
    }

    snprintf(command, sizeof(command), "ls -tr "FILE_PATH_SYSLOG_RAM"* | xargs -r -I %% -t sh -c \
        'cat %% >> "SYSLOG_FILE_NAME"; rm %%'; echo $? > "SYSLOG_ZIP_STATUS_FILE"", DIR_PATH_SYSLOG);

    status = WEXITSTATUS(system(command));
    CAB_DBG(CAB_GW_DEBUG, "WEXITSTATUS status=%d\r\n", status);
    status = readStatusFile(SYSLOG_ZIP_STATUS_FILE, &cmdStatus);
    
    if(status != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "Error in readig status file %s with code %d", SYSLOG_ZIP_STATUS_FILE, status);
        //unlock to protect event summary data
            sem_post(syslog_semlock);
        return status;
    }

    if( cmdStatus != GEN_SUCCESS ) {
        CAB_DBG(CAB_GW_ERR, "Merging the Syslog files Failed");
        //unlock to protect event summary data
            sem_post(syslog_semlock);
        return FILE_CREATE_ERROR;
    }

    //unlock to protect event summary data
    sem_post(syslog_semlock);

    FUNC_EXIT
    return GEN_SUCCESS;
}


/******************************************************************
 *@brief  (This API is used to create a package for syslog)
 *
 *@param[IN] packagingEvent_e (Event type packaging like initialization 
 *                             time or day change)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e createFilePackage_Syslog(packagingEvent_e packageTime)
{
    FUNC_ENTRY

    int32_t status;
    fileStorageInfo_t tempFileStorageInfo;
    char command[MAX_LEN_COMMAND_SYS];
    returnCode_e retCode = GEN_SUCCESS;
    uint8_t cmdStatus;

    //lock to protect event summary data
    retCode = semLockWithRetry();
    if (retCode != GEN_SUCCESS) {
        CAB_DBG(CAB_GW_ERR, "Sem wait failed with code %d", retCode);
        return GEN_API_FAIL;
    }
    status = readEventSummary_sys(&g_logeventSumm);       //read event summary file
    if(GEN_SUCCESS != status) {
        CAB_DBG(CAB_GW_ERR, "Failed to read %s file", eventSumFileName_sys);
    }

    if(g_logeventSumm.zipFileCnt >= g_zipLimit_syslog) {
        status = findOldestFile_sys((void*)&tempFileStorageInfo);
        if(status != GEN_SUCCESS) {
            CAB_DBG(CAB_GW_ERR, "Not getting oldest file name");
            //unlock to protect event summary data
            sem_post(syslog_semlock);
            return FILE_NOT_FOUND;
        }

        /* Delete the file in advance only if the package command is for DAY_CHANGE
        as for INIT_TIME there might be no zip creation. */
        if (packageTime == DAY_CHANGE) {
            //unlock to protect event summary data
            sem_post(syslog_semlock);
            CAB_DBG(CAB_GW_ERR, "more zip than limit, cleaning extra old zip files");
            status = deleteSyslogPackage((void*)&tempFileStorageInfo);
            retCode = semLockWithRetry();
            if (retCode != GEN_SUCCESS) {
                CAB_DBG(CAB_GW_ERR, "Sem wait failed with code %d", retCode);
                return GEN_API_FAIL;
            }
            if(status != GEN_SUCCESS) {
                CAB_DBG(CAB_GW_ERR, "Delete package error");
                //unlock to protect event summary data
                sem_post(syslog_semlock);
                return FILE_DELETE_ERROR;
            }
        }
    }

    if (packageTime == INIT_TIME) {
        snprintf(command, sizeof(command), "/cabApp/packSyslog.sh %d", packageTime);
        status = WEXITSTATUS(system(command));
        CAB_DBG(CAB_GW_DEBUG, "WEXITSTATUS status=%d\r\n", status);
        status = readStatusFile(SYSLOG_ZIP_STATUS_FILE, &cmdStatus);
        if(status != GEN_SUCCESS) {
            CAB_DBG(CAB_GW_ERR, "Error in readig status file %s with code %d", SYSLOG_ZIP_STATUS_FILE, status);
            //unlock to protect event summary data
            sem_post(syslog_semlock);
            return status;
        }

        if (cmdStatus != GEN_SUCCESS) {
            if( !(cmdStatus > GEN_SUCCESS) ) {
                CAB_DBG(CAB_GW_ERR, "Zip File Creation for Syslog Failed");
                sem_post(syslog_semlock);
                return FILE_CREATE_ERROR;
            }
            /* Delete the old file if the zip is created at INIT_TIME */
            else if(g_logeventSumm.zipFileCnt >= g_zipLimit_syslog) {
                //unlock to protect event summary data
                sem_post(syslog_semlock);
                CAB_DBG(CAB_GW_ERR, "more zip than limit, cleaning extra old zip files");
                status = deleteSyslogPackage((void*)&tempFileStorageInfo);
                retCode = semLockWithRetry();
                if (retCode != GEN_SUCCESS) {
                    CAB_DBG(CAB_GW_ERR, "Sem wait failed with code %d", retCode);
                    return GEN_API_FAIL;
                }
                if(status != GEN_SUCCESS) {
                    CAB_DBG(CAB_GW_ERR, "Delete package error");
                    //unlock to protect event summary data
                    sem_post(syslog_semlock);
                    return FILE_DELETE_ERROR;
                }
            }
            //Increase zip file count
            g_logeventSumm.zipFileCnt++;
        }
        memset(command, 0, sizeof(command));
    }
    else if (packageTime == DAY_CHANGE) {
        snprintf(command, sizeof(command), "/cabApp/packSyslog.sh %d", packageTime);
        status = WEXITSTATUS(system(command));
        CAB_DBG(CAB_GW_DEBUG, "WEXITSTATUS status=%d\r\n", status);
        status = readStatusFile(SYSLOG_ZIP_STATUS_FILE, &cmdStatus);
        
        if(status != GEN_SUCCESS) {
            CAB_DBG(CAB_GW_ERR, "Error in readig status file %s with code %d", SYSLOG_ZIP_STATUS_FILE, status);
            //unlock to protect event summary data
            sem_post(syslog_semlock);
            return status;
        }
        if( cmdStatus != GEN_SUCCESS ) {
            CAB_DBG(CAB_GW_ERR, "Zip File Creation for Syslog Failed");
            sem_post(syslog_semlock);
            return FILE_CREATE_ERROR;
        }
        memset(command, 0, sizeof(command));
        //Increase zip file count
        g_logeventSumm.zipFileCnt++;
    }
    else {
        CAB_DBG(CAB_GW_ERR, "Invalid argument");
        return GEN_INVALID_ARG;
    }

    /*  Updating event summary file   */
    //Write updated value in summary file
    status = writeEventSummary_sys(&g_logeventSumm);
    if(GEN_SUCCESS != status) {
        CAB_DBG(CAB_GW_ERR, "Failed to update %s file", eventSumFileName_sys);
        //Unlock to protect event data
        sem_post(syslog_semlock);
        return FILE_WRITE_ERROR;
    }

    //unlock to protect event summary data
    sem_post(syslog_semlock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/*Will be enabled in future versions*/
#if 0
returnCode_e getPackageFile(void * data)
{
    FUNC_ENTRY

    struct dirent *dp;                              // Pointer for directory entry
    time_t recenttime = 0;                          //timestamp for recent file
    struct stat statbuf;                            //to get stat of file
    char buffer[MAX_FILENAME_LEN_SYS];                  //to temporary store file name
    char recentFile[MAX_FILENAME_LEN_SYS] = {'\0'};     //to store recent file name
    char ext[5];                                    //to get extention of file
    uint8_t len, loopCnt;
    char tempOldFileName[512] = {'\0'};
    char tempNewFileName[512] = {'\0'};
    char command[1024] = {'\0'};
    fileStorageInfo_t tempFileStorageInfo = *(fileStorageInfo_t*)data;

    //lock to protect event summary data
    pthread_mutex_lock(&g_lock_syslog);

    // opendir() returns a pointer of DIR type.
    DIR *dr = opendir(sys_outDirName);

    if(dr == NULL) {  // opendir returns NULL if couldn't open directory
        printErr(DIR_OPEN_FAIL, NULL);
        //unlock to protect event summary data
        pthread_mutex_unlock(&g_lock_syslog);
        return DIR_OPEN_FAIL;
    }

    // for readdir()
    while((dp = readdir(dr)) != NULL) {
        if(strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0)
            continue;
        len = strlen(dp->d_name);
        len -= 4;
        for(loopCnt = 0; dp->d_name[len] != '\0'; loopCnt++) {
            ext[loopCnt] = dp->d_name[len];
            len++;
        }


	//Rename file name for ziprDiag to zip
	if(strncmp(ext, "Diag", 4) == 0) {
		memcpy(tempOldFileName, dp->d_name, strlen(dp->d_name));
		memcpy(tempNewFileName, dp->d_name, strlen(dp->d_name)-5);
		tempNewFileName[strlen(tempNewFileName)] = '\0';
		memset(dp->d_name, '\0',strlen(dp->d_name));
		memcpy(dp->d_name, tempNewFileName, strlen(tempNewFileName));
                CAB_DBG(CAB_GW_DEBUG, "tempOldFileName:%s\n", tempOldFileName);
                CAB_DBG(CAB_GW_DEBUG, "tempNewFileName:%s\n", tempNewFileName);

		snprintf(command, sizeof(command), "cd %s ;",sys_outDirName);
		strcat(command, "mv ");
		strcat(command, tempOldFileName);
		strcat(command, " ");
		strcat(command, tempNewFileName);
		strcat(command, " ;");
		strcat(command, "sync");
                CAB_DBG(CAB_GW_DEBUG, "command :%s\n", command);

		system(command);
	}

	len = strlen(dp->d_name);
        len -= 4;
        for(loopCnt = 0; dp->d_name[len] != '\0'; loopCnt++) {
            ext[loopCnt] = dp->d_name[len];
            len++;
        }

        //Get latest zip file to send it on cloud
        if(strncmp(ext, ".zip", 4) == 0) {
            snprintf(buffer, sizeof(buffer), "%s/%s", sys_outDirName, dp->d_name);
            lstat(buffer, &statbuf);
            if(statbuf.st_mtime > recenttime) {
                snprintf(recentFile, sizeof(recentFile), "%s", dp->d_name);
                recenttime = statbuf.st_mtime;
            }
        }
    }

    if(recentFile[0] == '\0') {
        printErr(MISC_NO_PENDING_EVENT, NULL);
        closedir(dr);
        //unlock to protect event summary data
    	sem_post(syslog_semlock);
        return MISC_NO_PENDING_EVENT;
    }

    //To send latest file path to get access of file
    snprintf(tempFileStorageInfo.filename, sizeof(tempFileStorageInfo.filename), "%s", recentFile);
    snprintf(tempFileStorageInfo.storagePath, sizeof(tempFileStorageInfo.storagePath), "%s", sys_outDirName);
    memcpy(data, &tempFileStorageInfo, sizeof(fileStorageInfo_t));

    closedir(dr);

    //unlock to protect event summary data
    sem_post(syslog_semlock);

    FUNC_EXIT
    return GEN_SUCCESS;
}
#endif

returnCode_e deleteSyslogPackage(void * data)
{
    FUNC_ENTRY

    int32_t status;
    char buffer[MAX_FILENAME_LEN_SYS];                  //to temporary store file name
    fileStorageInfo_t* tempFileStorageInfo = (fileStorageInfo_t*)data;
    returnCode_e retCode = GEN_SUCCESS;
    
    if(tempFileStorageInfo == NULL) {
    	CAB_DBG(CAB_GW_ERR, "NULL pointing error");
    	return GEN_NULL_POINTING_ERROR;
    }

    retCode = semLockWithRetry();
    if (retCode != GEN_SUCCESS) {
	    CAB_DBG(CAB_GW_ERR, "Sem wait failed with code %d", retCode);
	    return GEN_API_FAIL;
    }

    status = readEventSummary_sys(&g_logeventSumm);       //read event summary file
    if(GEN_SUCCESS != status) {
        CAB_DBG(CAB_GW_ERR, "Failed to read %s file", eventSumFileName_sys);
    }

    //To delete package files from dataStorage directory which name is provided in argument
	
    snprintf(buffer, sizeof(buffer), "%s/%s", tempFileStorageInfo->storagePath, tempFileStorageInfo->filename);
    status = remove(buffer);
    if(status != 0) {
        CAB_DBG(CAB_GW_ERR, "Deleting package file");
        //unlock to protect event summary data
    	sem_post(syslog_semlock);
        return FILE_DELETE_ERROR;
    }

    CAB_DBG(CAB_GW_INFO, "Syslog file name to be deleted : %s", buffer);

    /*  Updating event summary file   */

    //Decrease zip file count
    if(g_logeventSumm.zipFileCnt > 0) {
        g_logeventSumm.zipFileCnt--;

        //Write updated value in summary file
        status = writeEventSummary_sys(&g_logeventSumm);
        if(GEN_SUCCESS != status) {
            CAB_DBG(CAB_GW_ERR, "Failed to update %s file", eventSumFileName_sys);
            //Unlock to protect event data
    	    sem_post(syslog_semlock);
            return FILE_WRITE_ERROR;
        }
    }

    //unlock to protect event summary data
    sem_post(syslog_semlock);

    FUNC_EXIT
    return GEN_SUCCESS;
}
