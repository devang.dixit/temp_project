/*******************************************************************************
 * Copyright(c) <2016>, Volansys Technologies
 *
 * Description: 
 * @file circularBuffer.c
 * @brief Header file for circular buffer module.
 * 
 * @Author   - Volansys
 *
 *******************************************************************************
 *
 * @History
 *
 * Nov/22/2016, Vivek Rajpara	Updated code for overlapping logic
 * Nov/21/2016, Vivek Rajpara	Added code for circular buffer
 *
 ******************************************************************************/
#ifndef __CIR_BUF_H
#define __CIR_BUF_H

#include "pthread.h"


typedef struct cabCircularBuffer
{
    void *bufferStart;    // data buffer
    void *bufferEnd;      // end of data buffer
    uint32_t noOfBufs;      // maximum number of items in the buffer
    size_t sizeOfBuf;     // size of each item in the buffer   
    uint32_t currBufCounts; // number of items in the buffer 
    void *head;           // pointer to head
    void *tail;           // pointer to tail
    pthread_mutex_t readCBMutex;  // Read Circular buffer mutex
    pthread_mutex_t writeCBMutex; // Write Circular buffer mutex
    pthread_mutex_t cirBufCountMutex; // Circular buffer count mutex
} CAB_CIRCULAR_BUFFER_t;

/******************************************************************************
 * @brief Circular buffer initialization
 *              This function is used to initialize circular buffer.
 * @param[in] cirBuffHandle Pointer to handle to be initialized.
 * @param[in] noOfBufs    Number of buffers to be used in circular buffer.
 * @param[in] sizeOfBuf   Sizo of one buffer.
 * @return      Error Code
 ******************************************************************************/

int cabCBInit(CAB_CIRCULAR_BUFFER_t **cirBuffHandle, uint32_t noOfBufs, size_t sizeOfBuf);

/******************************************************************************
 * @brief Circular buffer initialization
 *              This function is used to deinitialize circular buffer.
 * @param[in] cirBuffHandle Pointer to handle to be deinitialized.
 * @return    Error Code
 ******************************************************************************/
int cabCBDeinit(CAB_CIRCULAR_BUFFER_t ** cirBuffHandle);

/******************************************************************************
 * @brief Get buffers from circular buffer
 *              This function is used to get buffers from circular buffer to 
           Push data into memory of the circular buffer.
 * @param[in] cirBuffHandle Pointer to handle of the circular buffer.
 * @param[in] item  Array of buffer items
 * @param[in] numberOfItems Number of items to be pushed.
 * @return Error Code
 ******************************************************************************/
int cabCBGetBufs(CAB_CIRCULAR_BUFFER_t * cirBuffHandle, void *item[], int numberOfItems);

/******************************************************************************
 * @brief Fetch Data from circular buffer
 *              This function is used to pop data from circular buffer.
 * @param[in] cirBuffHandle Pointer to handle of the circular buffer.
 * @param[out] item  Array of buffer items
 * @param[in] numberOfItems Number of items to be pushed.
 * @param[in] overlapFlag This flag takes care of overlapping and gives updates 
                           tail by only one position regardless of the number 
                           of items to be fetched.
 * @return Error Code
 ******************************************************************************/
int cabCBFetchData(CAB_CIRCULAR_BUFFER_t *cirBuffHandle, void *item, int numberOfItems, uint8_t overlapValue);



/******************************************************************************
 * @brief Get buffer counter
 *              This function is used to get current buffer counter
 * @param[in] cirBuffHandle Pointer to handle of the circular buffer.
 * @param[out] counter      Pointer to fill up the current circular buffer counter
 * @return Error Code
 ******************************************************************************/
int cabCBGetCount ( CAB_CIRCULAR_BUFFER_t *cirBuffHandle, uint32_t * counter );

#endif
