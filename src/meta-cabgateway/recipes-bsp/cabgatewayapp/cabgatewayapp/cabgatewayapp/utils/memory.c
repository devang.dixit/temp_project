#include<stdio.h>
#include "error.h"
#include "debug.h"
#include "memory.h"
#include "sys/types.h"
#include "sys/sysinfo.h"
#include "string.h"

static pthread_mutex_t memLock;
static size_t globalMemCount=0;


int parseLine(char* line){
	int i = strlen(line);
	const char* p = line;
	while (*p <'0' || *p > '9') p++;
	line[i-3] = '\0';
	i = atoi(p);
	return i;
}
//Note: this value is in KB!
int getValue()
{ 
	FILE* file = fopen("/proc/self/status", "r");
	int result = -1;
	char line[128];

	while (fgets(line, 128, file) != NULL){
		if (strncmp(line, "VmData:", 7) == 0){
			result = parseLine(line);
			break;
		}
	}
	fclose(file);
	return result;
}
returnCode_e checkSelfProcMem(uint32_t *selfMemUsageKB) 
{
	FUNC_ENTRY
	if(selfMemUsageKB == NULL) {
		CAB_DBG(CAB_GW_ERR, "Check self mem failed");
		return GEN_INVALID_ARG;
	}

	*selfMemUsageKB = getValue();
	CAB_DBG(CAB_GW_INFO, "Self Memory Usage %d",*selfMemUsageKB);
	FUNC_EXIT
	return GEN_SUCCESS;
}


void meminit() 
{
	pthread_mutex_init(&memLock, NULL);
}

returnCode_e incrMemCount(size_t incrementMem)
{
	FUNC_ENTRY
	pthread_mutex_lock(&memLock);
	globalMemCount += incrementMem;
	printf("\r\n#$#Currnet Mem in bytes: %ld,%x#$#\r\n", globalMemCount, globalMemCount);
	pthread_mutex_unlock(&memLock);
	FUNC_EXIT
	return GEN_SUCCESS;
}

returnCode_e decrMemCount(size_t decrementMem)
{
	FUNC_ENTRY
	pthread_mutex_lock(&memLock);
	globalMemCount -= decrementMem;
	printf("\r\n#$#Currnet Mem in bytes: %ld,%x#$#\r\n", globalMemCount, globalMemCount);
	pthread_mutex_unlock(&memLock);
	FUNC_EXIT
	return GEN_SUCCESS;
}
