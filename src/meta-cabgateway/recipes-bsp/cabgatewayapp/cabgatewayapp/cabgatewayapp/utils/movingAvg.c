/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : movingAvg.c
 * @brief : This file contains API for moving average
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Nov/16/2018, VR, Added moving avg for leak detection logic
 **************************************************************/

#include "movingAvg.h"
#include "debug.h"
#include "stdlib.h"

/* initalizing moving avg with circular window size given in argument */
MAVG_STATUS_e initMovingAvg (movingAvg_t *mavgHandle, uint16_t initWindowSize)
{
        if (mavgHandle == NULL || initWindowSize == 0) {
                CAB_DBG(CAB_GW_ERR, "Moving avg invalid args in %s", __FUNCTION__);
                return MAVG_INVALID_ARG;
        }

	pthread_mutex_init(&(mavgHandle->mavgMutex), NULL);
	pthread_mutex_lock(&(mavgHandle->mavgMutex));
	if (mavgHandle->isInit == 1) {
		pthread_mutex_unlock(&(mavgHandle->mavgMutex));
                CAB_DBG(CAB_GW_ERR, "Moving avg is already init in %s", __FUNCTION__);
		return MAVG_ALREADY_INIT;
	}
	
        mavgHandle->sum = 0;
        mavgHandle->windowSize = initWindowSize;
        mavgHandle->currIndex = 0;
	mavgHandle->isInit = 1;
        mavgHandle->avgArray = (float *) malloc(initWindowSize * sizeof(float));
	if (mavgHandle->avgArray == NULL) {
		pthread_mutex_unlock(&(mavgHandle->mavgMutex));
		CAB_DBG(CAB_GW_ERR, "Malloc error in %s", __FUNCTION__);
		return MAVG_MALLOC_ERR;
	}
        memset(mavgHandle->avgArray, 0, (initWindowSize * sizeof(float)) );
	pthread_mutex_unlock(&(mavgHandle->mavgMutex));
        return MAVG_SUCCESS;
}

/* Deinitalizing moving avg */
MAVG_STATUS_e deInitMovingAvg (movingAvg_t *mavgHandle)
{
        if (mavgHandle == NULL) {
                CAB_DBG(CAB_GW_ERR, "Moving avg invalid args in %s", __FUNCTION__);
                return MAVG_INVALID_ARG;
        }

	pthread_mutex_lock(&(mavgHandle->mavgMutex));
	if (mavgHandle->isInit == 0) {
		pthread_mutex_unlock(&(mavgHandle->mavgMutex));
                CAB_DBG(CAB_GW_ERR, "Moving avg not init in %s", __FUNCTION__);
		return MAVG_NOT_INIT;
	}

        free(mavgHandle->avgArray);
        mavgHandle->avgArray = NULL;
        mavgHandle->sum = 0;
        mavgHandle->windowSize = 0;
        mavgHandle->currIndex = 0;
	mavgHandle->isInit = 0;
	pthread_mutex_unlock(&(mavgHandle->mavgMutex));
	pthread_mutex_destroy(&(mavgHandle->mavgMutex));
        return MAVG_SUCCESS;
}

/* Calculate moving avg with current sample and sum of last circular window data */
MAVG_STATUS_e calcMovingAvg(movingAvg_t *mavgHandle, float new_val, float *outAvg)
{
        if (mavgHandle == NULL || outAvg == NULL) {
                CAB_DBG(CAB_GW_ERR, "Moving avg invalid args in %s", __FUNCTION__);
                return MAVG_INVALID_ARG;
        }
	pthread_mutex_lock(&(mavgHandle->mavgMutex));
        if (mavgHandle->windowSize == 0) {
		pthread_mutex_unlock(&(mavgHandle->mavgMutex));
                CAB_DBG(CAB_GW_ERR, "Moving avg invalid args in %s", __FUNCTION__);
                return MAVG_INVALID_ARG;
        }

	if (mavgHandle->isInit == 0) {
		pthread_mutex_unlock(&(mavgHandle->mavgMutex));
                CAB_DBG(CAB_GW_ERR, "Moving avg not init in %s", __FUNCTION__);
		return MAVG_NOT_INIT;
	}

        mavgHandle->sum = mavgHandle->sum - (mavgHandle->avgArray[mavgHandle->currIndex]) + new_val;
        mavgHandle->avgArray[mavgHandle->currIndex] = new_val;
        mavgHandle->currIndex++;
	
	/* Assigning Sum as avg as it doesn't affect in moving avg after first iteration */
	/* Current application also need sum during first iteration */
	*outAvg = mavgHandle->sum;

        if (mavgHandle->currIndex == mavgHandle->windowSize) {
                mavgHandle->currIndex = 0;
        }
	pthread_mutex_unlock(&(mavgHandle->mavgMutex));
        return MAVG_SUCCESS;
}

