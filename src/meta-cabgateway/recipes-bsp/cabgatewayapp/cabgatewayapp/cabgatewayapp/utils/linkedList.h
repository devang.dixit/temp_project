#ifndef __LINKLIST_H__
#define __LINKLIST_H__

#include <stdint.h>

//#include "debug.h"
//#include "error.h"


typedef struct linkedList {
	uint32_t nodeId;
	uint8_t opState;
	char longAlphaNum[30];
	char shortAlphaNum[20];
	uint32_t numericCode; /* max 8 BCD digits taking in 32 bit for size and simple approach */
	uint8_t accessType;
    struct linkedList *next;
} linkedList_t;

int addNode(linkedList_t *tempNode);
int removeNode(uint32_t nodeId);

#endif  /* __LINKLIST_H__ */
