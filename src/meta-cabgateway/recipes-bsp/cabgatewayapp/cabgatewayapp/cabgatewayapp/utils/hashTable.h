/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : hashTable.h
 * @brief : Hash Table header file.
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Aug/23/2018, VT , Doxygen commenting
 * Aug/22/2018, VT , Added initial version of the Hash
 **************************************************************/

#ifndef __HASH_TABLE_H
#define __HASH_TABLE_H

#include<stdint.h>

#define DUMMY_DATA (0xFFFFFFFF)

/** @struct hashItem  Item Data Block
 *
 *  This structure is allocated for each hash table entry on
 * insert and freed on removal.
 **/
typedef struct hashItem {
    void *data;
    uint32_t key;
} hashItem_t;

/** @enum HASH_STATUS
 *
 *  Internal status code for Hash Table library.
 */
typedef enum hashStatusCodes {
    HASH_SUCCESS = 0,   /**< Hash operation success. */
    HASH_INVLID_ARG,    /**< Hash invalid func argument. */
    HASH_NOT_INIT,      /**< Hash not inited. */
    HASH_KEY_NOT_FOUND, /**< Hash key not found. */
    HASH_MALLOC_ERROR,  /**< Hash malloc error. */
    HASH_TABLE_FULL,    /**< Hash Table full. */
    MAX_HASH_STATUS_CODES   /**< Max no of hash error code.*/
} HASH_STATUS;

/** @struct hashHandle
 *  Hash Table Control Handle to be used by each APIs and
 * maintained for each instance of the Hash Table.
 */
typedef struct hashHandle {
    hashItem_t** hashArray;     /**< Hash Table element array. */
    uint16_t totalHashSize;     /**< Total Hash size. */
    uint16_t currHashSize;      /**< No of elements currently filled up.*/
    size_t hashDataSize;        /**< Individual element size.*/
    uint8_t isInit;         /**< Is Hash initialzed or not. */
    pthread_mutex_t hashMutex;  /**< Hash Mutex. */
} hashHandle_t;

/** @brief Initialize Hash Table.
 *
 * @parma[out] hashHandle Hashhandle to be used for all APIs
 * @param[in] hashSize  Hash array table size.
 * @param[in] sizeofDataStruct Size of individual hash element.
 * @return Hash Error code.
 */
HASH_STATUS initHashTable(hashHandle_t **hashHandle, uint16_t hashSize, size_t sizeofDataStruct);

/** @brief De-Initialize Hash Table.
 *
 * @param[in] hashHandle Hash control structure.
 * @return Hash Error code.
 */
HASH_STATUS deInitHashTable(hashHandle_t *hashHandle);

/** @brief Serach element in hash table.
 *
 * @param[in] hashhandle hash control structure.
 * @param[in] key key to be searched in hash table.
 * @param[out] outData  Pointer to be output data.
                  NOTE: Memory of size defined in init will be copied
                        to this given pointer from internal memory.
 * @return Hash Error code.
 */
HASH_STATUS hashSearch(hashHandle_t *hashHandle, uint32_t key, void *outData);

/** @brief Insert element in hash table.
 *
 * @param[in] hashhandle hash control structure.
 * @param[in] key key to be inserted in hash table.
 * @param[out] data  Pointer to be output data.
                  NOTE: Memory of size defined in init will be copied
                        from this given pointer to internal memory.
 * @return Hash Error code.
 */
HASH_STATUS hashInsert(hashHandle_t * hashHandle, uint32_t key, void *data);

/** @brief Delete element from hash table.
 *
 * @param[in] hashhandle hash control structure.
 * @param[in] key key to be deleted in hash table.
 * @return Hash Error code.
 */
HASH_STATUS hashDelete(hashHandle_t *hashHandle, uint32_t key);

/** @brief Flush all elements from hash table.
 *
 * @param[in] hashhandle hash control structure.
 * @return Hash Error code.
 */
HASH_STATUS hashFlush(hashHandle_t *hashHandle);

/** @brief Delete element from hash table.
 *
 * @param[in] hashhandle hash control structure.
 * @param[in] key key for which data needs to be updated.
 * @param[in] data ponter of input data
 * @return Hash Error code.
 */
HASH_STATUS hashUpdateData(hashHandle_t *hashHandle, uint32_t key, void * data);

/** @brief Get current hash size
 *
 * @param[in] hashhandle hash control structure.
 * @param[out] currSize Currently filled hashed
 * @return Hash Error code.
 */
HASH_STATUS getSizeOfHashTable(hashHandle_t *hashHandle, uint16_t * currSize);

/** @brief Fetches current data from the hash table
 *
 * @param[in] hashhandle hash control structure
 * @param[out] data  Output buffer to be filled up with current data.
 * @return Hash Error code.
 */
HASH_STATUS fetchCurrentDataHashTable(hashHandle_t *hashHandle, void *data);
#endif
