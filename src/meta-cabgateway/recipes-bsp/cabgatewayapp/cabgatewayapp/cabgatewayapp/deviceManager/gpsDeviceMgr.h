/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : gpsDeviceMgr.h
 * @brief : Header file for gpsDeviceMgr.c
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Aug/17/2018, VT , First Draft
 **************************************************************/
#ifndef __GPS_DEVMGR_H
#define __GPS_DEVMGR_H
#include "deviceMgr.h"

/** @brief Init GPS interface
 *
 *      This function initializes GPS device with all required APIs.
 *
 * @param[in] gpsHandler Interface handle structure
 * @param[in] initConfig Configuration to initialize device interface
 * @param[in] callback   Callback function if async data needs to be sent.
 * @return error status of the API
 * */
returnCode_e gpsInitInterface(interfaceManagerHandler_t *gpsHandler, void * initConfig, interruptDataCBType callback);

/** @brief DeInit GPS interface
 *
 *      This function de-initializes GPS device APIs.
 *
 * @param[in] gpsHandler Interface handle structure
 * @return error status of the API
 * */
returnCode_e gpsDeInitInterface(interfaceManagerHandler_t *gpsHandler);

#endif

