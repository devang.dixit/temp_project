/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : batDeviceMgr.h
 * @brief : Header file for batDeviceMgr.c
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Aug/17/2018, VT , First Draft
 **************************************************************/
#ifndef __BAT_DEVMGR_H
#define __BAT_DEVMGR_H
#include "deviceMgr.h"

typedef struct batDMData {
	powerSwitchEvent_e batHealth;
	powerDevice_e powerDev;
} batDM_Data_t;

typedef struct batInitConfig {
	uint32_t lowBatThreshold;
} batInitCfg_t;

/** @brief Init Battery interface
 *
 *      This function initializes Battery device with all required APIs.
 *
 * @param[in] batHandler Interface handle structure
 * @param[in] initConfig Configuration to initialize device interface
 * @param[in] callback   Callback function if async data needs to be sent.
 * @return error status of the API
 * */
returnCode_e batInitInterface(interfaceManagerHandler_t *batHandler, void * initConfig, interruptDataCBType callback);

/** @brief DeInit Battery interface
 *
 *      This function de-initializes Battery device APIs.
 *
 * @param[in] batHandler Interface handle structure
 * @return error status of the API
 * */
returnCode_e batDeInitInterface(interfaceManagerHandler_t *batHandler);

#endif

