/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : batDeviceMgr.c
 * @brief : This file provides Battery device management APIs
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Aug/28/2018, VT , Stub APIs
 * Aug/17/2018, VT , First Draft
 **************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "configuration.h"
#include "commonData.h"
#include "deviceMgr.h"
#include "batDeviceMgr.h"
#include "batModule.h"
#include <errno.h>
#include "debug.h"

/**
 * @struct batCtrlBlock
 * @brief Core Handler structure for Battery DM control
 *
 *        This strucure to be used for device controling and will be
 *  defined for each interfaces.
 **/
typedef struct batCtrlBlock {
    uint8_t isInit;
    interruptDataCBType appCB;
    pthread_mutex_t batDevLock;  /**< Battery DM API mutex */
    batInitCfg_t initCfg;
} batCtrlBlock_t;

batCtrlBlock_t batCtrl = {.isInit = 0};


/**
 * Battery Add device.
 * */
returnCode_e batAddDevice(void * data)
{
    FUNC_ENTRY
    /* NOTE: Kept empty for now to avoid any seg fault in upper application. */
    FUNC_EXIT
    return GEN_API_NOT_AVAIL;
}

/**
 * Battery Remove device.
 * */
returnCode_e batRemoveDevice(void * data)
{
    FUNC_ENTRY
    /* NOTE: Kept empty for now to avoid any seg fault in upper application. */
    FUNC_EXIT
    return GEN_API_NOT_AVAIL;
}

/**
 *  Get Battery device data.
 * */
returnCode_e batGetDeviceData(void * data)
{
    returnCode_e retCode = GEN_SUCCESS;
    FUNC_ENTRY
    if (data == NULL) {
        retCode = GEN_INVALID_ARG;
        return retCode;
    }
    batDM_Data_t *tempBAtDMData = (batDM_Data_t *)data;

    pthread_mutex_lock(&(batCtrl.batDevLock));
    if (batCtrl.isInit != 1) {
        retCode = GEN_NOT_INIT;
        pthread_mutex_unlock(&(batCtrl.batDevLock));
        return retCode;
    }

    retCode = getBatteryHealth(&(tempBAtDMData->batHealth)); 
    if (retCode != GEN_SUCCESS) {
        pthread_mutex_unlock(&(batCtrl.batDevLock));
	return retCode;
    }

    retCode = getPowerSource((void *)&(tempBAtDMData->powerDev));
    if (retCode != GEN_SUCCESS) {
        pthread_mutex_unlock(&(batCtrl.batDevLock));
	return retCode;
    }
    pthread_mutex_unlock(&(batCtrl.batDevLock));
    FUNC_EXIT
    return retCode;
}

/**
 *  Set Battery device data.
 * */
returnCode_e batSetDeviceData(void * data)
{
    returnCode_e retCode = GEN_SUCCESS;
    FUNC_ENTRY
    if (data == NULL) {
        retCode = GEN_INVALID_ARG;
        return retCode;
    }

    pthread_mutex_lock(&(batCtrl.batDevLock));
    if (batCtrl.isInit != 1) {
        retCode = GEN_NOT_INIT;
        pthread_mutex_unlock(&(batCtrl.batDevLock));
        return retCode;
    }

    memcpy(&(batCtrl.initCfg), data, sizeof(batInitCfg_t));
    retCode = setBatThreshold(batCtrl.initCfg.lowBatThreshold);
    pthread_mutex_unlock(&(batCtrl.batDevLock));
    FUNC_EXIT
    return retCode;
}

void batCB(void * data)
{
	FUNC_ENTRY
	if (data == NULL) {
	    return;
	}
	batCtrl.appCB((powerSwitchEvent_e *)data);
	FUNC_EXIT
}

/**
 * Init Battery interface API
 **/
returnCode_e batInitInterface(interfaceManagerHandler_t *batHandler, void * initConfig, interruptDataCBType callback)
{
    returnCode_e retCode = GEN_SUCCESS;

    FUNC_ENTRY
    if(batHandler == NULL || initConfig == NULL || callback == NULL) {
        return GEN_INVALID_ARG;
    }

    batCtrl.batDevLock = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;

    pthread_mutex_lock(&(batCtrl.batDevLock));
    batHandler->addDevice = batAddDevice;
    batHandler->removeDevice = batRemoveDevice;
    batHandler->getData = batGetDeviceData;
    batHandler->setData = batSetDeviceData;
    batCtrl.appCB = callback;
    memcpy(&(batCtrl.initCfg), initConfig, sizeof(batInitCfg_t));

    retCode = initBatModule(batCB, batCtrl.initCfg.lowBatThreshold);
    if(retCode != GEN_SUCCESS)  {
    	batHandler->addDevice = NULL;
    	batHandler->removeDevice = NULL;
    	batHandler->getData = NULL;
    	batHandler->setData = NULL;
    	pthread_mutex_unlock(&(batCtrl.batDevLock));
	retCode = DM_INTF_INIT_FAIL;
        return retCode;
    }
    batCtrl.isInit = 1;
    pthread_mutex_unlock(&(batCtrl.batDevLock));
    FUNC_EXIT
    return retCode;
}


/**
 * DeInit Battery interface API
 **/
returnCode_e batDeInitInterface(interfaceManagerHandler_t *batHandler)
{
    returnCode_e retCode = GEN_SUCCESS;
    FUNC_ENTRY
    if (batHandler == NULL) {
	return GEN_INVALID_ARG;
    }

    pthread_mutex_lock(&(batCtrl.batDevLock));
    if(batCtrl.isInit != 1) {
        retCode = GEN_NOT_INIT;
    	pthread_mutex_unlock(&(batCtrl.batDevLock));
        return retCode;
    }

    retCode = deInitBatModule();
    batHandler->addDevice = NULL;
    batHandler->removeDevice = NULL;
    batHandler->getData = NULL;
    batHandler->setData = NULL;
    batCtrl.isInit = 0;
    pthread_mutex_unlock(&(batCtrl.batDevLock));
    FUNC_EXIT

    return retCode;
}

