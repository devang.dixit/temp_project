/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : rtcDeviceMgr.h
 * @brief : Header file for rtcDeviceMgr.c
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Aug/17/2018, VT , First Draft
 **************************************************************/
#ifndef __RTC_DEVMGR_H
#define __RTC_DEVMGR_H

#include "deviceMgr.h"

/** @brief Init RTC interface
 *
 *      This function initializes RTC device with all required APIs.
 *
 * @param[in] rtcHandler Interface handle structure
 * @param[in] initConfig Configuration to initialize device interface
 * @param[in] callback   Callback function if async data needs to be sent.
 * @return error status of the API
 * */
returnCode_e rtcInitInterface(interfaceManagerHandler_t *rtcHandler, void * initConfig, interruptDataCBType callback);

/** @brief DeInit RTC interface
 *
 *      This function de-initializes RTC device APIs.
 *
 * @param[in] rtcHandler Interface handle structure
 * @return error status of the API
 * */
returnCode_e rtcDeInitInterface(interfaceManagerHandler_t *rtcHandler);

#endif
