/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : deviceManager.c
 * @brief : This file contains central control source code for
 *          devices of all interfaces.
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Aug/28/2018, VR , Added RTC and GPS support.
 * Aug/24/2018, VR , Added RF support.
 * Aug/17/2018, VT , First Draft
 **************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "deviceMgr.h"
#include "error.h"
#include "rfDeviceMgr.h"
#include "rtcDeviceMgr.h"
#include "debug.h"

/*
 *  This function should be called before accessing any device manager
 *  functionalities.
 */
returnCode_e initDeviceManager(deviceManagerHandler_t ** dmHandler)
{
    FUNC_ENTRY

    CAB_DBG(CAB_GW_INFO, "Initializing device manager");

    /* Check input params */
    if(dmHandler == NULL) {
        return GEN_NULL_POINTING_ERROR;
    }

    /* Allocate resources for all interfaces and return handler */
    *dmHandler = (deviceManagerHandler_t *) malloc(sizeof(deviceManagerHandler_t));
    if(*dmHandler == NULL) {
        return GEN_MALLOC_ERROR;
    }
    (*dmHandler)->isInit = 0;

    /*Initialize all interfaces of device manager */
    (*dmHandler)->rfIntfHandler = (interfaceManagerHandler_t *) malloc(sizeof(interfaceManagerHandler_t));
    if ((*dmHandler)->rfIntfHandler == NULL) {
	return GEN_MALLOC_ERROR;
    }
    (*dmHandler)->gpsHandler = (interfaceManagerHandler_t *) malloc(sizeof(interfaceManagerHandler_t));
    if ((*dmHandler)->gpsHandler == NULL) {
	return GEN_MALLOC_ERROR;
    }
    (*dmHandler)->rtcHandler = (interfaceManagerHandler_t *) malloc(sizeof(interfaceManagerHandler_t));
    if ((*dmHandler)->rtcHandler == NULL) {
	return GEN_MALLOC_ERROR;
    }
    (*dmHandler)->batHandler = (interfaceManagerHandler_t *) malloc(sizeof(interfaceManagerHandler_t));
    if ((*dmHandler)->batHandler == NULL) {
	return GEN_MALLOC_ERROR;
    }
    (*dmHandler)->isInit = 1;

    FUNC_EXIT
    /* return success */
    return GEN_SUCCESS;
}

/*
 *  This function should be called to clean all resources used by
 * device manager and to stop device data functionality.
 */
returnCode_e deInitDeviceManager(deviceManagerHandler_t * dmHandler)
{
    FUNC_ENTRY
    returnCode_e retCode = GEN_SUCCESS;
    /* Validate input arguments. */
    if(dmHandler == NULL) {
        return GEN_INVALID_ARG;
    }

    CAB_DBG(CAB_GW_INFO, "Deinitializing device manager");

    fflush(stdout);
    if(dmHandler->isInit != 1) {
        return GEN_NOT_INIT;
    }

    /* Free allocated resources */
    free(dmHandler->rfIntfHandler);
    free(dmHandler->gpsHandler);
    free(dmHandler->rtcHandler);
    free(dmHandler->batHandler);
    free(dmHandler);

    FUNC_EXIT
    return retCode;
}


/*
 *  Add new interface in device manager which can handle new devices.
 */
returnCode_e addDeviceInterface(intfID_e intfID, interfaceManagerHandler_t * intfHandler, void *initConfiguration , interruptDataCBType callBack)
{
    returnCode_e retCode = GEN_SUCCESS;
    FUNC_ENTRY
    switch(intfID) {
        case CGW_RF_INTF:
            retCode = rfInitInterface(intfHandler, initConfiguration, callBack);
            break;

        case CGW_GPS_INTF:
            retCode = gpsInitInterface(intfHandler, initConfiguration, callBack);
            break;

        case CGW_RTC_INTF:
            retCode = rtcInitInterface(intfHandler, initConfiguration, callBack);
            break;

        case CGW_BAT_INTF:
            retCode = batInitInterface(intfHandler, initConfiguration, callBack);
            break;

        default:
            retCode = GEN_INVALID_ARG;
            break;
    }
    FUNC_EXIT
    return retCode;
}


/*
 *  Remove interface from device manager which can handle new devices.
 */
returnCode_e removeDeviceInterface(intfID_e  intfID, interfaceManagerHandler_t *intfHandler)
{
    FUNC_ENTRY
    returnCode_e retCode = GEN_SUCCESS;
    if(intfHandler == NULL) {
        return GEN_INVALID_ARG;
    }
    switch(intfID) {
        case CGW_RF_INTF:
            retCode = rfDeInitInterface(intfHandler);
            break;

        case CGW_GPS_INTF:
            retCode = gpsDeInitInterface(intfHandler);
            break;

        case CGW_RTC_INTF:
            retCode = rtcDeInitInterface(intfHandler);
            break;

        case CGW_BAT_INTF:
            retCode = batDeInitInterface(intfHandler);
            break;

        default:
            retCode = GEN_INVALID_ARG;
            break;
    }
    FUNC_EXIT
    return retCode;
}
