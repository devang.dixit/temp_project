/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : rtcDeviceMgr.c
 * @brief : This file provides RTC control APIs
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Aug/17/2018, VT , First Draft
 **************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "configuration.h"
#include "commonData.h"
#include "rtcDeviceMgr.h"
#include <errno.h>
#include "rtc.h"
#include "debug.h"

/**
 * @struct rtcCtrlBlock
 * @brief Core Handler structure for RTC DM control
 *
 *        This strucure to be used for device controling and will be
 *  defined for each interfaces.
 **/
typedef struct rtcCtrlBlock {
    uint8_t isInit;
    pthread_mutex_t rtcDevLock;  /**< RTC DM API mutex */
} rtcCtrlBlock_t;

rtcCtrlBlock_t rtcCtrl = { 0 };

/**
 * RTC Add device.
 * */
returnCode_e rtcAddDevice(void * data)
{
    FUNC_ENTRY
    /* NOTE: Kept empty for now to avoid any seg fault in upper application. */
    FUNC_EXIT
    return GEN_API_NOT_AVAIL;
}

/**
 * RTC Remove device.
 * */
returnCode_e rtcRemoveDevice(void * data)
{
    FUNC_ENTRY
    /* NOTE: Kept empty for now to avoid any seg fault in upper application. */
    FUNC_EXIT
    return GEN_API_NOT_AVAIL;
}

/**
 *  Get RTC device data.
 * */
returnCode_e rtcGetDeviceData(void * data)
{
    returnCode_e retCode = GEN_SUCCESS;
    FUNC_ENTRY
    if (data == NULL) {
	retCode = GEN_INVALID_ARG;
	return retCode;
    }

    pthread_mutex_lock(&(rtcCtrl.rtcDevLock));
    if (rtcCtrl.isInit != 1) {
	retCode = GEN_NOT_INIT;
    	pthread_mutex_unlock(&(rtcCtrl.rtcDevLock));
	return retCode;
    }

    retCode = getTime((time_t *)data);
    pthread_mutex_unlock(&(rtcCtrl.rtcDevLock));
    FUNC_EXIT
    return retCode;
}

/**
 *  Set RTC device data.
 * */
returnCode_e rtcSetDeviceData(void * data)
{
    returnCode_e retCode = GEN_SUCCESS;
    FUNC_ENTRY
    if (data == NULL) {
	retCode = GEN_INVALID_ARG;
	return retCode;
    }

    pthread_mutex_lock(&(rtcCtrl.rtcDevLock));
    if (rtcCtrl.isInit != 1) {
	retCode = GEN_NOT_INIT;
    	pthread_mutex_unlock(&(rtcCtrl.rtcDevLock));
	return retCode;
    }
    retCode = setTime(*(time_t *)data);
    pthread_mutex_unlock(&(rtcCtrl.rtcDevLock));
    FUNC_EXIT
    return retCode;
}

/**
 *  Get RTC device data.
 * */
returnCode_e rtcGetDiagData(void * data)
{
    returnCode_e retCode = GEN_SUCCESS;
    FUNC_ENTRY
    if (data == NULL) {
	retCode = GEN_INVALID_ARG;
	return retCode;
    }

    pthread_mutex_lock(&(rtcCtrl.rtcDevLock));
    if (rtcCtrl.isInit != 1) {
	retCode = GEN_NOT_INIT;
    	pthread_mutex_unlock(&(rtcCtrl.rtcDevLock));
	return retCode;
    }
    retCode = getRtcTime((time_t *)data);
    pthread_mutex_unlock(&(rtcCtrl.rtcDevLock));
    FUNC_EXIT
    return retCode;
}

/**
 * Init RTC interface API
 **/
returnCode_e rtcInitInterface(interfaceManagerHandler_t *rtcHandler, void * initConfig, interruptDataCBType callback)
{
    returnCode_e retCode = GEN_SUCCESS;
    FUNC_ENTRY
    if(rtcHandler == NULL) {
        return GEN_INVALID_ARG;
    }

    pthread_mutex_lock(&(rtcCtrl.rtcDevLock));
    rtcHandler->addDevice = rtcAddDevice;
    rtcHandler->removeDevice = rtcRemoveDevice;
    rtcHandler->getData = rtcGetDeviceData;
    rtcHandler->setData = rtcSetDeviceData;
    rtcHandler->getDiagData = rtcGetDiagData;
    retCode = initRtc();
    if(retCode != GEN_SUCCESS)  {
    	rtcHandler->addDevice = NULL;
    	rtcHandler->removeDevice = NULL;
    	rtcHandler->getData = NULL;
    	rtcHandler->setData = NULL;
    	rtcHandler->getDiagData = NULL;
    	pthread_mutex_unlock(&(rtcCtrl.rtcDevLock));
	retCode = DM_INTF_INIT_FAIL;
        return retCode;
    }
    rtcCtrl.isInit = 1;
    pthread_mutex_unlock(&(rtcCtrl.rtcDevLock));
    FUNC_EXIT
    return retCode;
}


/**
 * DeInit RTC interface API
 **/
returnCode_e rtcDeInitInterface(interfaceManagerHandler_t *rtcHandler)
{
    returnCode_e retCode = GEN_SUCCESS;
    FUNC_ENTRY
    if (rtcHandler == NULL) {
	retCode = GEN_INVALID_ARG;
	return retCode;
    }

    rtcCtrl.rtcDevLock = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;

    pthread_mutex_lock(&(rtcCtrl.rtcDevLock));
    if(rtcCtrl.isInit != 1) {
        retCode = GEN_NOT_INIT;
    	pthread_mutex_unlock(&(rtcCtrl.rtcDevLock));
        return retCode;
    }
    deInitRtc();
    rtcHandler->addDevice = NULL;
    rtcHandler->removeDevice = NULL;
    rtcHandler->getData = NULL;
    rtcHandler->setData = NULL;
    rtcHandler->getDiagData = NULL;
    rtcCtrl.isInit = 0;
    pthread_mutex_unlock(&(rtcCtrl.rtcDevLock));
    FUNC_EXIT
    return retCode;
}
