#include<stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#define EG91_USB_PORT  "/dev/ttyUSB2"
#define MAX_READ_BUFF_SIZE 256
int main(void)
{
	int lteFd=-1;
	int readBytes=0;
	char readbuff[MAX_READ_BUFF_SIZE];
	struct termios tty;
	lteFd = open(EG91_USB_PORT, O_RDWR | O_NOCTTY);
	if(lteFd == -1)
	{
		printf("Opening EG91 USB port fail with error: %s", strerror(errno));
		return -1;
	}

	/*---------- Setting the Attributes of the serial port using termios structure --------- */
	if (tcgetattr(lteFd, &tty) < 0)
	{
		printf("Error from tcgetattr: %s", strerror(errno));
		return -1;
	}

	cfsetospeed(&tty, (speed_t)9600);
	cfsetispeed(&tty, (speed_t)9600);

	tty.c_cflag |= (CLOCAL | CREAD);                  /* ignore modem controls */
	tty.c_cflag |= CS8;                               /* 8-bit characters */
	tty.c_cflag &= ~CSIZE;
	tty.c_cflag &= ~PARENB;                           /* no parity bit */
	tty.c_cflag &= ~CSTOPB;                           /* only need 1 stop bit */
	tty.c_cflag &= ~CRTSCTS;                          /* no hardware flowcontrol */

	/* setup for non-canonical mode */
	tty.c_iflag &= ~(IXON | IXOFF | IXANY);
	tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);

	/* no local echo: allow the other end to do the echoing */
	tty.c_lflag &= ~ECHO;

	/* fetch bytes as they become available */
	tty.c_cc[VMIN] = 1;
	tty.c_cc[VTIME] = 0;

	if (tcsetattr(lteFd, TCSANOW, &tty) != 0) 
	{
		printf("Error from tcsetattr: %s", strerror(errno));
		return -1;
	}

	readBytes=read(lteFd,readbuff,sizeof(readbuff));
	if(readBytes < 0)
	{
		printf("***read fails\n****");
	}

	printf("***%s***",readbuff);

	return 0;
}
