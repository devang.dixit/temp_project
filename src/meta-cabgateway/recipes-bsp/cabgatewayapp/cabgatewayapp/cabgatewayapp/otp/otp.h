/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : otp.h
 * @brief : This file is header file for OTP memory
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Nov/19/2019, VT , First Draft
 **************************************************************/

#ifndef __OTP_H
#define __OTP_H
#include <stdint.h>

#define OTP_GP0_FILE_NAME    "/sys/fsl_otp/HW_OCOTP_SW_GP0"

returnCode_e write_otp(int bitNo);
returnCode_e read_otp(uint32_t *value);
#endif
