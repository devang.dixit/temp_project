/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : deviceManager.h
 * @brief : Header file for deviceManager.c
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Aug/27/2018, VT , Changed API descriptions.
 * Aug/17/2018, VT , First Draft
 **************************************************************/
#ifndef __DEVMGR_H
#define __DEVMGR_H
#include <stdint.h>
#include <stdio.h>
#include "error.h"
/**
 * @enum supportedIntf
 *
 *  Enumeration for supported interfaces for DM.
 */
typedef enum intfID {
    CGW_RF_INTF = 0, /**< RF Interface for DevMgr. */
    CGW_GPS_INTF,   /**< GPS Interface for DevMgr. */
    CGW_RTC_INTF,   /**< RTC Interface for DevMgr. */
    CGW_LTE_INTF,   /**< RTC Interface for DevMgr. */
    CGW_BAT_INTF,   /**< RTC Interface for DevMgr. */
    MAX_CGW_INTF    /**< No of Supported interfaces. */
} intfID_e;

/** Function pointer for registering callback for interrupt based data*/
typedef void (*interruptDataCBType)(void * data);

/**
 * @struct interfaceManagerHandler
 * @brief Core Handler structure for interface manager
 *
 *        This strucure to be used for device controling and will be
 *  defined for each interfaces.
 **/
typedef struct interfaceManagerHandler {
    void * initConfiguration; /**< Initial configuration. */
    returnCode_e(*addDevice)(void *);   /**< Function pointer for adding device */
    returnCode_e(*removeDevice)(void *);   /**< Function pointer for removing device */
    returnCode_e(*getData)(void *);   /**< Function pointer for getting data from device */
    returnCode_e(*setData)(void *);   /**< Function pointer for setting data to device */
    returnCode_e(*getDiagData)(void *);   /**< Function pointer for setting data to device */
    returnCode_e(*flushDevices)(void *);   /**< Function pointer for setting data to device */
    interruptDataCBType callBackFunc; /**< Function pointer for registering
                                          callback for passing interrupt based data */
} interfaceManagerHandler_t;


/**
 * @struct deviceManagerHandler
 * @brief Core Handler structure for device manager
 *
 *        This strucure to be used for managing all interfaces. It gives
 * functionalities to other modules
 **/
typedef struct deviceManagerHandler {
    uint8_t isInit; /**< Initialized flag. */
    interfaceManagerHandler_t *rfIntfHandler; /**< Interface handler for RF */
    interfaceManagerHandler_t *gpsHandler; /**< Interface handler for GPS */
    interfaceManagerHandler_t *rtcHandler; /**< Interface handler for RTC */
    interfaceManagerHandler_t *lteHandler; /**< Interface handler for RTC */
    interfaceManagerHandler_t *batHandler; /**< Interface handler for RTC */
} deviceManagerHandler_t;


/** @brief Initialize Device manager
 *
 *      This function should be called before accessing any device manager
 *  functionalities.
 *
 * @param[out] dmHandler Output DM control handler.
 * @return Error Code.
 */
returnCode_e initDeviceManager(deviceManagerHandler_t ** dmHandler);

/** @brief DeInitialize Device manager
 *
 *      This function should be called to clean all resources used by
 * device manager and to stop device data functionality.
 *
 * @param[in] dmHandler Input DM handler.
 * @return Error Code.
 */
returnCode_e deInitDeviceManager(deviceManagerHandler_t * dmHandler);


/** @brief Add and enable device interface.
 *
 *      This function shoduld be called to initialize and enable any device
 * Interface.
 *
 * @param[in] intfID Interface ID enum.
 * @param[in] intfHandler Interface handler.
 * @param[in] initConfiguration Initial configuration. Should be NULL if not required.
 * @param[in] callback Callback for async devices. Should be NULL if not required.
 * @return Error Code.
 */
returnCode_e addDeviceInterface(intfID_e intfID, interfaceManagerHandler_t * intfHandler, void * initConfiguration , interruptDataCBType callback);

/** @brief DeInitialize Device manager
 *
 *      This function should be called to clean all resources used by
 * device manager and to stop device data functionality.
 *
 * @param[in] intfID Interface ID to be removed.
 * @param[in] intfHandler Interface handler.
 * @return Error Code.
 */
returnCode_e removeDeviceInterface(intfID_e  intfID, interfaceManagerHandler_t *intfHandler);

#endif
