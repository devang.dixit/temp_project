/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : rfDeviceMgr.h
 * @brief : Headerfile for rfDeviceMgr.c
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Aug/24/2018, VR , Added changes as per implemented APIs and
                     doxygen
 * Aug/17/2018, VT , First Draft
 **************************************************************/
#ifndef __RFDEV_H
#define __RFDEV_H
#include <mqueue.h>
#include "error.h"
#include "deviceMgr.h"
#include "commonData.h"
#include "hashTable.h"
#include "watchdog.h"
#include "syscall.h"

/**
 * @struct rfInitConfig
 * @brief Initial Configuration Structure
 *
 *        This strucure should be given in initialization of RF interface.
 **/
typedef struct rfInitConfig {
    uint16_t maxDeviceSupported;    /**< Maximum device to be supported on RF interface. */
    sensorVendorType_e sensorVendorType; /**< Vendor type for TPMS sensor. */
} rfInitConfig_t;

/** @brief Init RF interface
 *
 *      This function initializes RF device with all required APIs.
 *
 * @param[in] rfHandler RF Interface Handler.
 * @param[in] initConfig Initial configuration
 * @param[in] callback   Callback API to be given for async data transfer.
 * @return error status of the API
 * */
returnCode_e rfInitInterface(interfaceManagerHandler_t *rfHandler, void * initConfig, interruptDataCBType callback);

/** @brief Deinit RF Interface
 *
 *      This function de-initializes RF device APIs.
 *
 * @param[in] rfHandler RF Interface handler
 * @return error status of the API
 * */
returnCode_e rfDeInitInterface(interfaceManagerHandler_t *rfHandler);

#endif
