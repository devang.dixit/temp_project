Copyright(c) <2018>, Volansys Technologies                                                       
Readme of device manager.
Author     - VT                                                                                 

History                                                                                          
Aug/17/2018, VT , First Draft                                                                   

Source code for Device manager

deviceMgr   ->   Provide control to other application

rfDeviceMgr  ->  Device management for devices of RF interface

rtcDeviceMgr  ->  Device management for devices of RTC device

gpsDeviceMgr  ->  Device management for devices of GPS device
