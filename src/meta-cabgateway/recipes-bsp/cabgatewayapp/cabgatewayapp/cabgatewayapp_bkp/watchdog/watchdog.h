#ifndef __WATCHDOG_H__
#define __WATCHDOG_H__

#include <pthread.h>
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include "debug.h"
#include "error.h"


typedef struct watchDogTimer {
        volatile pthread_t threadId;
	bool (*CallbackFn)(void);
        struct watchDogTimer *next;
} watchDogTimer_t;

int threadRegister(watchDogTimer_t *tempNode);
int threadDeregister(pthread_t thId);
watchDogTimer_t *getWatchDogTimerHead(void);

#endif //  __WATCHDOG_H__
