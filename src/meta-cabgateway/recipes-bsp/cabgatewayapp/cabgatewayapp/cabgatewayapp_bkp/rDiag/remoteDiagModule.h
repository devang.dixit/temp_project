#ifndef _R_DIAG_H
#define _R_DIAG_H

#include "commonData.h"
#include "error.h"
#include <sys/time.h>
#include <sys/stat.h>
#include <mqueue.h>
#include <fcntl.h>

#define MAX_LEN_BLE_MAC_RD                 (6)                 /**< Max len of ble mac address */
#define MAX_LEN_LTE_IMEI_RD                (16)                /**< Max len of LTE IMEI number */
#define MAX_LEN_GATEWAY_ID_RD              (MAX_LEN_BLE_MAC_RD + MAX_LEN_LTE_IMEI_RD) /**< Max len of gateway id */
#define MAX_LEN_SOFTWARE_VERSION_RD        (10)                 /**< Max len of Software version. */
#define MAX_LEN_CLOUD_URI_RD               (2048)              /**< Max len of cloud URI */
#define MAX_ZIP_COUNT                      (1500)

#define RDIAG_BUF_MAX_SIZE 70
#define MAX_LEN_REMOTE_DIAG_MSG 1024
#define RDIAG_MAX_MSG 10
#define MQ_SEND_TOUT_SEC 0

#define RDIAG_REQ_QUEUE	"/rdiag_req_queue"
#define RDIAG_RES_QUEUE	"/rdiag_res_queue"

//define debug enum
typedef enum debugLevel {
    GW_ERR = 0x1,				// Error
    GW_WARNING,					// Warning
    GW_INFO,					// Message
    GW_DEBUG,					// Debugging Message
    MAX_GW_DBG
} debugLevel_e;

typedef enum rdiagMsgType {
    INIT_CONFIG=0x01,
    RDIAG_MSG,
    WATCHDOG_TIMER_MSG,
    LTE_DIAG,
    GW_ID_REQ,
    GW_ID_RES,
    SEND_BACK_INIT_CONFIG,
    FACTORY_RESET_FROM_CABAPP,
    SWOVER_REQ,
    INTERNET_CONNECTIVITY_TIMESTAMP_UPDATE,
	CLOUD_CONNECTION_STATUS_UPDATE,
    MAX_RDIAG_MSG
} rDiagMsgType_e;

typedef enum remoteDiagError {
    CABAPP_LINUX_SYS_INIT_FAIL = 0x100,		// CabGateway app failed to initialize due to system error
    CABAPP_SIM_NOT_PRESENT,			// CabGateway Init failed due to LTE
    CABAPP_SIM_ICCID_NOT_AVAIL,			// CabGateway sim iccid is not available
    CABAPP_LTE_AT_CMD_FAIL,			// CabGateway LTE AT command failed
    CABAPP_SUB1_INIT_FAIL,			// CabGateway Init failed due to Sub1
    CABAPP_BLE_INIT_FAIL,			// CabGateway Init failed due to Sub1
    CABAPP_LTE_INIT_FAIL,			// CabGateway Init failed due to Sub1
    LOW_BATT_SHUTOFF,
    DEVMGR_INIT_FAIL,
    TPMS_PAIR_FAIL,				// TPMS pair failure
    OTA_PERFORM_ERROR,				// Failed to perform the OTA
    LTE_GPS_KEEP_ALIVE_FAILURE,			// Communication with LTE/GPS module failed
    BLE_KEEP_ALIVE_FAILURE,			// BLE module communication error
    SUB1_KEEP_ALIVE_FAILURE,			// Sub1 receiver communication error
    BLE_DIAG_GET_FAIL,				// Failure in getting diagnostics data through BLE
    TPMS_UART_COMM_ERROR,			// Error in communicating to Sub1 RF module over UART
    LTE_CONNECT_FAIL,				// Failure in connecting to the internet via LTE
    PROCESS_CRASH_ERROR,			// If any process is crashed abruptly this error will be sent
    OTA_MD5SUM_ERROR = 0x200,			// md5sum invalid for downloaded image
    OTA_VALIDATION_ERROR,			// Invalid image content as per software update / cabgateway update features
    OTA_BLE_CONNECTION_ERROR,                   // If BLE connection is there for 60 minutes while trying to process the OTA.
    BLE_CONFIG_SET_FAIL,			// Failure in execution of BLE configuration set API.
    BLE_CONFIG_GET_FAIL,			// Failure in execution of BLE configuration get API.
    GPS_DATA_GET_FAIL,				// Failure in getting GPS data for diagnostics or TPMS sensor
    GPS_XDTA_EXPIRED,                           // Failure due to Invalid GPS XDATA.
    MESSAGE_QUEUE_OVERFLOW,		        // Overflow in TPMS data collection queues
    CABAPP_GEN_WARN,
    WATCHDOG_REGISTRATION_FAIL,
    WATCHDOG_DEREGISTRATION_FAIL,
    REMOTE_DIAG_MAX_ERR
} remoteDiagError_e;

// structure for adding diag data into circular buffer
typedef struct remoteDiagInfoStore {
    debugLevel_e level;				// debug level
    time_t time;				// time stamp
    char msg[MAX_LEN_REMOTE_DIAG_MSG];		// diag message
} remoteDiagInfoStore_t;

// structure to send remote diag data on error
typedef struct remoteDiagInfoSend {
    debugLevel_e level;				// current debug level of the system
    remoteDiagError_e errorType;		// reason of the error
    char msg[MAX_LEN_REMOTE_DIAG_MSG];		// diag message
    time_t time;				// time stamp of the error occurred
    uint16_t noOfRDiagLogs;
    remoteDiagInfoStore_t rDiagLogs[RDIAG_BUF_MAX_SIZE];
} remoteDiagInfoSend_t;

typedef struct initConfig {
	uint16_t maxZipCount;			    //Max zip count
	char cloudURI[MAX_LEN_CLOUD_URI_RD];           //Cloud URI
	uint8_t gatewayId[MAX_LEN_GATEWAY_ID_RD];      //Gateway id 
	char softwareVersion[MAX_LEN_SOFTWARE_VERSION_RD]; //software version
} initConfig_t;

typedef struct remoteDiagWrapper {
	rDiagMsgType_e type;
	union {
		lteDiagInfo_t sendLTEDebug;
		initConfig_t sendInitConfig;
		remoteDiagInfoSend_t sendRDiagMsg;
		time_t timeStamp;
		bool cloudConnectionStatus;
	};
} remoteDiagWrapper_t;

/****************************************
 ************* CALLBACKS ****************
 ****************************************/
/*It is used to register callback to notify the cloud transaction status*/
typedef void (*rDiagModuleResHandlerCB)(void *);

returnCode_e rDiagInit();
returnCode_e rDiagModInit(rDiagModuleResHandlerCB);

void rDiagSendMessage(remoteDiagError_e errorType, debugLevel_e msgLevel, char* errorExtraMessage);
void rDiagSendInitConf(uint16_t maxZipCount, char *cloudURI, uint8_t *gatewayId, char *softwareVersion);
void rDiagSendFactoryResetCmd(void);

returnCode_e rDiagAddMessage(time_t logTime,  debugLevel_e msgLevel, char *message);


#endif
