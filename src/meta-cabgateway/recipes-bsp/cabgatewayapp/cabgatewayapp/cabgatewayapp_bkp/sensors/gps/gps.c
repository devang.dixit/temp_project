/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file : gps.c
 * @brief : This file provides GPS device control APIs
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Aug/28/2018, VR , Added Stub APIs
 * Aug/27/2018, VT , First Draft
 **************************************************************/
#include "commonData.h"
#include "gps.h"
#include "error.h"
#include "debug.h"
#include <time.h>

/* Global current GPS data */
static gpsData_t g_GPS = {0};
static uint8_t g_noOfSateliteinView = 0;
static uint16_t g_fixQuality = 0;
static struct tm g_GPSTimeStamp;

typedef enum {
	VALID_DATA = 0,
	INVALID_DATA = 1,
        IMCOMPLETE_DATA=2,
	MAX_DATA
} validData_e;

/*****
 *Device API to get GPS data
 *****/
static validData_e getGPSLocation(gpsData_t *gpsData)
{
    FUNC_ENTRY


     char gpsGGACmd[] = "AT+QGPSGNMEA=\"GGA\"";
     int  readBytes = 0;
     char readBuff[512];
     int  parseStrCount = 0;
     uint8_t validData = 1;
 
     const char delim2[2] = ",";
     char *token = NULL;

     char latDir = '-', longDir = '-', altiUnit='-';
     float tempAlt = 0;
     double tempLatFloat = 0, tempLongFloat = 0;
     double latFloatMinutePart = 0, longFloatMinutePart = 0;
     int latIntPart = 0, longIntPart = 0;
     int fixQuality = 0, noOfSat = 0;

     returnCode_e retCode = GEN_SUCCESS;


     retCode = ATTransactEG91(gpsGGACmd, strlen(gpsGGACmd), "GPGGA", readBuff, &readBytes);
     if(retCode != GEN_SUCCESS)
     {
             CAB_DBG(CAB_GW_ERR, "ATTransactEG91() failed with %d", retCode);
             return INVALID_DATA;
     }

     if ( readBytes != 0) {
     	 validData = 1;
         token = strtok(readBuff, delim2);
         while (token != NULL) {
         	if(strstr(token, "GPGGA")) {
			parseStrCount = 0;
			if (*(readBuff + strlen(token)+1) == ',') {
				validData = 0;
			}
			break;
		}
             	token = strtok(NULL, delim2);
	 }
	 if (validData) {
             while (token != NULL) {
	            switch(parseStrCount) {
	            	case 0:
	            	case 1:
	            		break;
	            	case 2:
             			sscanf(token, "%lf", &(tempLatFloat));
             			latIntPart = (int)tempLatFloat; 
             			latFloatMinutePart = tempLatFloat - latIntPart;
	            		break;
	            	case 3:
             			sscanf(token, "%c", &(latDir));
             			if (latDir == 'N') {
             			   g_GPS.latitude = ( latIntPart / 100 ) + ( ( (latIntPart % 100) + latFloatMinutePart) / 60);
             			}

             			if (latDir == 'S') {
             			   g_GPS.latitude = - (( latIntPart / 100 ) + ( ( (latIntPart % 100) + latFloatMinutePart) / 60));
             			}
	            		break;
	            	case 4:
             			sscanf(token, "%lf", &(tempLongFloat));
             			longIntPart = (int) tempLongFloat;
             			longFloatMinutePart = tempLongFloat - longIntPart;
	            		break;
	            	case 5:
             			sscanf(token, "%c", &(longDir));
             			if (longDir == 'E') {
             			   g_GPS.longitude = ( longIntPart / 100 ) + ( ( (longIntPart % 100) + longFloatMinutePart) / 60);
             			}

             			if (longDir == 'W') {
             			   g_GPS.longitude = - (( longIntPart / 100 ) + ( ( (longIntPart % 100) + longFloatMinutePart) / 60));
             			}
	            		break;
	            	case 6:
                			sscanf(token, "%d", &(fixQuality));
                			if (fixQuality >= 0 && fixQuality <= 8) {
                			   g_fixQuality = fixQuality;
                			}
	            		break;
	            	case 7:
                			sscanf(token, "%d", &(noOfSat));
                			g_noOfSateliteinView = noOfSat;
	            		break;
	            	case 8:
	            		break;
	            	case 9:
                			sscanf(token, "%f", &(tempAlt));
	            		break;
	            	case 10:
	                		sscanf(token, "%c", &(altiUnit));
	                		if(altiUnit == 'M') {
	                		   g_GPS.altitude = tempAlt;	
	                		}
	            		break;
	            	case 11:
	        		case 12:
	        		case 13:
	        			break;
	            	default:
	            		break;
	            }
             	    token = strtok(NULL, delim2);
	            parseStrCount++;
	     }
	} else {
		return IMCOMPLETE_DATA;
	}
     } else {
	return IMCOMPLETE_DATA;
     }
     CAB_DBG(CAB_GW_TRACE,"Latitude ===> %lf",g_GPS.latitude);
     CAB_DBG(CAB_GW_TRACE,"Longitude ===> %lf",g_GPS.longitude);
     CAB_DBG(CAB_GW_TRACE,"Altitude ===> %lf",g_GPS.altitude);
     CAB_DBG(CAB_GW_TRACE,"fix ===> %d",g_fixQuality);
     CAB_DBG(CAB_GW_TRACE,"No of Sat ===> %d", g_noOfSateliteinView);

     /* Add actual call to GPS data gathering with AT comamnds*/
     gpsData->latitude = g_GPS.latitude;
     gpsData->longitude = g_GPS.longitude;
     gpsData->altitude = g_GPS.altitude;

    FUNC_EXIT
    return VALID_DATA;
}

static validData_e getGPSSpeedData(gpsData_t *gpsData)
{
    FUNC_ENTRY


     char gpsVTGCmd[] = "AT+QGPSGNMEA=\"VTG\"";
     int  readBytes = 0;
     char readBuff[512];
     int  parseStrCount = 0;
     uint8_t validData = 1;
 
     const char delim2[2] = ",";
     char *token = NULL;

     char tempGSpeedUnit = '-';
     float tempGSpeedK = 0;

     returnCode_e retCode = GEN_SUCCESS;

     retCode = ATTransactEG91(gpsVTGCmd, strlen(gpsVTGCmd), "GPVTG", readBuff, &readBytes);
     if(retCode != GEN_SUCCESS)
     {
             CAB_DBG(CAB_GW_ERR, "ATTransactEG91() failed with %d", retCode);
             return INVALID_DATA;
     }

     if ( readBytes != 0) {
     	 validData = 1;
         token = strtok(readBuff, delim2);
         while (token != NULL) {
         	if(strstr(token, "GPVTG")) {
			if (*(readBuff + strlen(token)+1) == ',') {
				validData = 0;
			}
			parseStrCount = 0;
			break;
		}
             	token = strtok(NULL, delim2);
	 }

	 if (validData) {
             while (token != NULL) {
	            switch(parseStrCount) {
	            	case 0:
	            	case 1:
	            	case 2:
	            	case 3:
	            	case 4:
	            	case 5:
	            	case 6:
	        		break;
	            	case 7:
	                		sscanf(token, "%f", &tempGSpeedK);
	            		break;
	            	case 8:
                        		sscanf(token, "%c", &tempGSpeedUnit);
                			if(tempGSpeedUnit == 'K') {
                			   g_GPS.speed = tempGSpeedK;
	                		}
	            		break;
	            	default:
	            		break;
	            }
             	    token = strtok(NULL, delim2);
	            parseStrCount++;
	    }
	} else {
		return IMCOMPLETE_DATA;
	}
     } else {
	return IMCOMPLETE_DATA;
     }

     CAB_DBG(CAB_GW_TRACE, "GSpeed %f kmph", g_GPS.speed);
     /* Add actual call to GPS data gathering with AT comamnds*/
     gpsData->speed = g_GPS.speed;

    FUNC_EXIT
    return VALID_DATA;
}


static validData_e getGPSTimeData()
{
    FUNC_ENTRY

     char gpsRMCCmd[] = "AT+QGPSGNMEA=\"RMC\"";
     int  readBytes = 0;
     char readBuff[512];
     int  parseStrCount = 0;
     uint8_t validData = 1;
 
     const char delim2[2] = ",";
     char *token = NULL;

     int rmcDate = 0, rmcTime = 0;

     returnCode_e retCode = GEN_SUCCESS;

     retCode = ATTransactEG91(gpsRMCCmd, strlen(gpsRMCCmd), "GPRMC", readBuff, &readBytes);
     if(retCode != GEN_SUCCESS)
     {
             CAB_DBG(CAB_GW_ERR, "ATTransactEG91() failed with %d", retCode);
             return INVALID_DATA;
     }

     if ( readBytes != 0) {
     	 validData = 1;
         token = strtok(readBuff, delim2);
         while (token != NULL) {
         	if(strstr(token, "GPRMC")) {
			if (*(readBuff + strlen(token)+1) == ',') {
				validData = 0;
			}
			parseStrCount = 0;
			break;
		}
             	token = strtok(NULL, delim2);
	 }
	 if (validData) {
             while (token != NULL) {
	            switch(parseStrCount) {
	            	case 0:
	        		break;
	            	case 1:
                		sscanf(token, "%d", &rmcTime);
                		g_GPSTimeStamp.tm_sec = rmcTime % 100;
                		g_GPSTimeStamp.tm_min = ((int)(rmcTime/100)) % 100;
                		g_GPSTimeStamp.tm_hour = ((int)(rmcTime/100)) / 100;
	        		break;
	            	case 2:
	            	case 3:
	            	case 4:
	            	case 5:
	            	case 6:
	            	case 7:
	            	case 8:
	            		break;
	            	case 9:
                		sscanf(token, "%d", &rmcDate);
                		g_GPSTimeStamp.tm_year = rmcDate % 100;
                		g_GPSTimeStamp.tm_mon = ((int)(rmcDate/100)) % 100;
                		g_GPSTimeStamp.tm_mday = ((int)(rmcDate/100)) / 100;
	            		break;
	            	default:
	            		break;
	            }
                token = strtok(NULL, delim2);
	        parseStrCount++;
	     }
	} else {
	     return INVALID_DATA;
	}
     } else {
	return INVALID_DATA;
     }

     CAB_DBG(CAB_GW_TRACE, "RMC Timestamp : %d-%d-%d %d:%d:%d", 
		g_GPSTimeStamp.tm_mday, 
		g_GPSTimeStamp.tm_mon, 
		g_GPSTimeStamp.tm_year,
		g_GPSTimeStamp.tm_hour,
		g_GPSTimeStamp.tm_min,
		g_GPSTimeStamp.tm_sec );

    FUNC_EXIT
    return VALID_DATA;
}

returnCode_e getGPSData(gpsData_t * gpsData)
{
	returnCode_e retCode = GEN_SUCCESS;
	validData_e isValid = INVALID_DATA;
	FUNC_ENTRY
	isValid = getGPSLocation(gpsData); 
	if (isValid == INVALID_DATA) {
		CAB_DBG(CAB_GW_WARNING, "No GPS location data available");
	}
	isValid = getGPSSpeedData(gpsData);
	if (isValid == INVALID_DATA) {
		CAB_DBG(CAB_GW_WARNING, "No GPS speed data available");
	}
	FUNC_EXIT
	return retCode;
}

returnCode_e getGPSTime(time_t *gpsTime)
{
	returnCode_e retCode = GEN_SUCCESS;
	if (gpsTime== NULL) {
		return GEN_INVALID_ARG;
	}
	validData_e isValid = INVALID_DATA;
	isValid = getGPSTimeData();
	if (isValid == VALID_DATA) {
        	*gpsTime = mktime(&g_GPSTimeStamp);
		retCode = GEN_SUCCESS;
	} else {
		*gpsTime = 0;
		retCode = GPS_NO_DATA;
	}
	return retCode;
}

returnCode_e getGPSQuality(uint16_t *fixQuality)
{
	returnCode_e retCode = GEN_SUCCESS;
	validData_e isValid = INVALID_DATA;
	if (fixQuality == NULL) {
		return GEN_INVALID_ARG;
	}
	gpsData_t tempGPS;
	isValid = getGPSData(&tempGPS);
	if (isValid == VALID_DATA) {
		*fixQuality = g_fixQuality;
		retCode = GEN_SUCCESS;
	} else {
		*fixQuality = 0;
		retCode = GPS_NO_DATA;
	}
	return retCode;
}

returnCode_e getGPSNoOfSatelite(uint8_t *noOfSatelite)
{
	validData_e isValid = INVALID_DATA;
	returnCode_e retCode = GEN_SUCCESS;
	if (noOfSatelite == NULL) {
		return GEN_INVALID_ARG;
	}
	gpsData_t tempGPS;
	isValid = getGPSData(&tempGPS);
	if (isValid == VALID_DATA) {
		*noOfSatelite = g_noOfSateliteinView;
		retCode = GEN_SUCCESS;
	} else {
		*noOfSatelite = 0;
		retCode = GPS_NO_DATA;
	}
	return GEN_SUCCESS;
}
/*****
 *Device API to get GPS data
 *****/
returnCode_e initGPS(void *config)
{
    returnCode_e retCode = GEN_SUCCESS;
    char gpsONCmd[] = "AT+QGPS=1";
    char gpsNMEACmd[] = "AT+QGPSCFG=\"nmeasrc\",1";
    char gpsAutoCmd[] = "AT+QGPSCFG=\"autogps\",1";
    int  readBytes = 0;
    char readBuff[512];

    FUNC_ENTRY
    retCode = initEG91();
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "initEG91() fail with error: %d", retCode);
            return retCode;
    }

     retCode = ATTransactEG91(gpsONCmd, strlen(gpsONCmd), "OK", readBuff, &readBytes);
     if(retCode != GEN_SUCCESS)
     {
             CAB_DBG(CAB_GW_ERR, "ATTransactEG91() failed with %d", retCode);
             return retCode;
     }

     retCode = ATTransactEG91(gpsNMEACmd, strlen(gpsNMEACmd), "OK", readBuff, &readBytes);
     if(retCode != GEN_SUCCESS)
     {
             CAB_DBG(CAB_GW_ERR, "ATTransactEG91() failed with %d", retCode);
             return retCode;
     }


     retCode = ATTransactEG91(gpsAutoCmd, strlen(gpsAutoCmd), "OK", readBuff, &readBytes);
     if(retCode != GEN_SUCCESS)
     {
             CAB_DBG(CAB_GW_ERR, "ATTransactEG91() failed with %d", retCode);
             return retCode;
     }


    memset((void *)&g_GPSTimeStamp, 0, sizeof(struct tm));
    FUNC_EXIT
    return GEN_SUCCESS;
}

/*****
 *Device API to deinit GPS device
 *****/
returnCode_e deinitGPS()
{
    returnCode_e retCode = GEN_SUCCESS;

     char gpsOffCmd[] = "AT+QGPSEND";
     int  readBytes = 0;
     char readBuff[512];
    FUNC_ENTRY

     retCode = ATTransactEG91(gpsOffCmd, strlen(gpsOffCmd), "OK", readBuff, &readBytes);
     if(retCode != GEN_SUCCESS)
     {
             CAB_DBG(CAB_GW_ERR, "ATTransactEG91() failed with %d", retCode);
             return retCode;
     }


    retCode = deInitEG91();
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "deInitEG91() fail withe error: %d", retCode);
            return retCode;
    }
    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e resetGPS()
{
    returnCode_e retCode = GEN_SUCCESS;

     char gpsOffCmd[] = "AT+QGPSEND";
     char gpsONCmd[] = "AT+QGPS=1";
     char gpsNMEACmd[] = "AT+QGPSCFG=\"nmeasrc\",1";
     char gpsAutoCmd[] = "AT+QGPSCFG=\"autogps\",1";
     int  readBytes = 0;
     char readBuff[512];
    FUNC_ENTRY

     retCode = ATTransactEG91(gpsOffCmd, strlen(gpsOffCmd), "OK", readBuff, &readBytes);
     if(retCode != GEN_SUCCESS)
     {
             CAB_DBG(CAB_GW_ERR, "ATTransactEG91()-%s failed with %d", gpsOffCmd, retCode);
             return retCode;
     }

     retCode = ATTransactEG91(gpsONCmd, strlen(gpsONCmd), "OK", readBuff, &readBytes);
     if(retCode != GEN_SUCCESS)
     {
             CAB_DBG(CAB_GW_ERR, "ATTransactEG91()-%s failed with %d", gpsONCmd, retCode);
             return retCode;
     }

     retCode = ATTransactEG91(gpsNMEACmd, strlen(gpsNMEACmd), "OK", readBuff, &readBytes);
     if(retCode != GEN_SUCCESS)
     {
             CAB_DBG(CAB_GW_ERR, "ATTransactEG91()-%s failed with %d", gpsNMEACmd, retCode);
             return retCode;
     }


     retCode = ATTransactEG91(gpsAutoCmd, strlen(gpsAutoCmd), "OK", readBuff, &readBytes);
     if(retCode != GEN_SUCCESS)
     {
             CAB_DBG(CAB_GW_ERR, "ATTransactEG91()-%s failed with %d", gpsAutoCmd, retCode);
             return retCode;
     }

    CAB_DBG(CAB_GW_INFO, "GPS Reset Success");
    memset((void *)&g_GPSTimeStamp, 0, sizeof(struct tm));
    FUNC_EXIT
    return GEN_SUCCESS;

}

returnCode_e enableGPSOneXtra()
{
    returnCode_e retCode = GEN_SUCCESS;
    char gpsOffCmd[] = "AT+QGPSEND";
    char gpsOneXtraEn[] = "AT+QGPSXTRA=1";
    char HTTPCfgCmd[] = "AT+QHTTPCFG=\"contextid\",2";
    char configPPDCtxt[] = "AT+QICSGP=2,1,\"em\",\"\",\"\",1";
    char activateContext[] = "AT+QIACT=2";
    char dnsConfig[] = "AT+QIDNSCFG=2";
    char setHttpURL[] = "AT+QHTTPURL=40,40";
    char HttpURL[] = "http://xtrapath4.izatcloud.net/xtra2.bin";
    char HttpGet[] = "AT+QHTTPGET=80";
    char HttpReadFile[] = "AT+QHTTPREADFILE=\"RAM:xtra2.bin\",100";
    char getLatestTime[] = "AT+QLTS";
    char injectIntoGNSS[] = "AT+QGPSXTRADATA=\"RAM:xtra2.bin\"";
    char gpsXdataCmd[] = "AT+QGPSXTRADATA?";
    char DeAct[] = "AT+QIDEACT=2";
    char parseGpsTime[20] = {0};
    char injectGPSTime[50] = {0};
    int  readBytes = 0;
    char readBuff[512];
    int16_t idx = 0;
    FUNC_ENTRY

    memset(readBuff, 0, sizeof(readBuff));
    retCode = ATTransactEG91(gpsOffCmd, strlen(gpsOffCmd), "OK", readBuff, &readBytes);
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "ATTransactEG91()-%s failed with %d", gpsOffCmd, retCode);
            return retCode;
    }
    CAB_DBG(CAB_GW_DEBUG, "GPS Off");

    retCode = ATTransactEG91(gpsOneXtraEn, strlen(gpsOneXtraEn), "OK", readBuff, &readBytes);
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "ATTransactEG91()-%s failed with %d", gpsOneXtraEn, retCode);
            return retCode;
    }
    CAB_DBG(CAB_GW_DEBUG, "GPS One XTRA Enable");

    retCode = ATTransactEG91(HTTPCfgCmd, strlen(HTTPCfgCmd), "OK", readBuff, &readBytes);
    if(retCode != GEN_SUCCESS)
    {
	    CAB_DBG(CAB_GW_ERR, "ATTransactEG91()-%s failed with %d", HTTPCfgCmd, retCode);
	    return retCode;
    }
    CAB_DBG(CAB_GW_DEBUG, "GPS Config HTTP contexttid as 2");

    retCode = ATTransactEG91(configPPDCtxt, strlen(configPPDCtxt), "OK", readBuff, &readBytes);
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "ATTransactEG91()-%s failed with %d", configPPDCtxt, retCode);
            return retCode;
    }
    CAB_DBG(CAB_GW_DEBUG, "GPS Configure Context 2");

    retCode = ATTransactEG91(activateContext, strlen(activateContext), "OK", readBuff, &readBytes);
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "ATTransactEG91()-%s failed with %d", activateContext, retCode);
            return retCode;
    }
    CAB_DBG(CAB_GW_DEBUG, "GPS Activate Context 2");

    retCode = ATTransactEG91(dnsConfig, strlen(dnsConfig), "OK", readBuff, &readBytes);
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "ATTransactEG91()-%s failed with %d", gpsOffCmd, retCode);
            return retCode;
    }
    CAB_DBG(CAB_GW_DEBUG, "GPS Configure DNS");

    sleep(2);
    retCode = ATTransactEG91(setHttpURL, strlen(setHttpURL), "CONNECT", readBuff, &readBytes);
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "ATTransactEG91()-%s failed with %d", setHttpURL, retCode);
            return retCode;
    }

    memset(readBuff, 0, sizeof(readBuff));
    retCode = ATTransactEG91(HttpURL, strlen(HttpURL), "OK", readBuff, &readBytes);
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "ATTransactEG91()-%s failed with %d", HttpURL, retCode);
            return retCode;
    } 
    CAB_DBG(CAB_GW_DEBUG, "GPS Set Http URL");

    sleep(2);
    uint8_t retry_count = 3;
    for(idx = 0; idx < retry_count; idx++) {
	    retCode = ATTransactEG91_CustomSleep(HttpGet, strlen(HttpGet), "+QHTTPGET:", readBuff, &readBytes, 80);
	    if(retCode == DATA_NOT_SUFFICIENT)
	    {
            	CAB_DBG(CAB_GW_INFO, "File not downloaded retrying retry_count %d", idx);
                continue;
	    } else if(retCode != GEN_SUCCESS) {
            	CAB_DBG(CAB_GW_ERR, "ATTransactEG91()-%s failed with %d", HttpGet, retCode);
                return retCode;   
            } else if(strstr(readBuff, "714") || strstr(readBuff, "702")) {
            	CAB_DBG(CAB_GW_INFO, "File not downloaded retrying retry_count %d", idx);
                continue;
	    }
            break;
    }
    if(idx == retry_count) {
            return retCode;
    }
    CAB_DBG(CAB_GW_DEBUG, "GPS XTRA Data Downloaded");
    for(idx = 0; idx <= readBytes; idx++)
        CAB_DBG(CAB_GW_DEBUG, "Response : %c", readBuff[idx]);
    
    memset(readBuff, 0, sizeof(readBuff));
    retCode = ATTransactEG91_CustomSleep(HttpReadFile, strlen(HttpReadFile), "+QHTTPREADFILE: 0", readBuff, &readBytes, 100);
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "ATTransactEG91()-%s failed with %d", HttpReadFile, retCode);
            return retCode;
    }
    CAB_DBG(CAB_GW_DEBUG, "GPS XTRA Read File to RAM");

    sleep(2);
    memset(readBuff, 0, sizeof(readBuff));
    retCode = ATTransactEG91(getLatestTime, strlen(getLatestTime), "QLTS:", readBuff, &readBytes);
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "ATTransactEG91()-%s failed with %d", getLatestTime, retCode);
            return retCode;
    }
        
    char *start;
    uint8_t len;
    char timeStamp[20];
    start = strstr(readBuff, "\"");
    start++; //increment one to point to date
    len = 19; 
    if(start == NULL) {
        return GEN_INVALID_ARG;
    }

    sleep(2);
    strncpy(parseGpsTime, start, len); 
    sprintf(injectGPSTime, "AT+QGPSXTRATIME=0,\"%s\",1,1,5", parseGpsTime);
    memset(readBuff, 0, sizeof(readBuff));
    retCode = ATTransactEG91(injectGPSTime, strlen(injectGPSTime), "OK", readBuff, &readBytes);
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "ATTransactEG91()-%s failed with %d", injectGPSTime, retCode);
            return retCode;
    }
    CAB_DBG(CAB_GW_DEBUG, "GPS Config XTRADATA Time");

    sleep(2);
    retCode = ATTransactEG91(injectIntoGNSS, strlen(injectIntoGNSS), "OK", readBuff, &readBytes);
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "ATTransactEG91()-%s failed with %d", injectIntoGNSS, retCode);
            return retCode;
    }
    CAB_DBG(CAB_GW_DEBUG, "GPS Inject XDATA into GNSS");
    
    sleep(2);
    memset(readBuff, 0, sizeof(readBuff));
    retCode = ATTransactEG91(gpsXdataCmd, strlen(gpsXdataCmd), "+QGPSXTRADATA:", readBuff, &readBytes);
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "ATTransactEG91()-%s failed with %d", gpsXdataCmd, retCode);
            return retCode;
    }
    CAB_DBG(CAB_GW_DEBUG, "GPS Injected XDATA of %d bytes is:", readBytes);

    for(idx = 0; idx <= readBytes; idx++)
        CAB_DBG(CAB_GW_DEBUG, "%c", readBuff[idx]);

    retCode = ATTransactEG91(DeAct, strlen(DeAct), "OK", readBuff, &readBytes);
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "ATTransactEG91()-%s failed with %d", DeAct, retCode);
            return retCode;
    }
    CAB_DBG(CAB_GW_DEBUG, "GPS Deactivate Context 2");

    CAB_DBG(CAB_GW_INFO, "GPS One XTRA Config done");
    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e CheckXdataisValid(uint8_t *isValid)
{
    returnCode_e retCode = GEN_SUCCESS;
    char gpsXdataCmd[] = "AT+QGPSXTRADATA?";
    int  readBytes = 0;
    char readBuff[512];

    FUNC_ENTRY
    retCode = ATTransactEG91(gpsXdataCmd, strlen(gpsXdataCmd), "+QGPSXTRADATA:", readBuff, &readBytes);
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "ATTransactEG91()-%s failed with %d", gpsXdataCmd, retCode);
            return retCode;
    }
    if(strstr(readBuff,"+QGPSXTRADATA: 0"))
    {
            *isValid = 0;
            CAB_DBG(CAB_GW_INFO, "GPS XTDATA InValid");
    } else {
            *isValid = 1;
            CAB_DBG(CAB_GW_INFO, "GPS XTDATA Valid");
    }

    FUNC_EXIT
    return retCode;
}
