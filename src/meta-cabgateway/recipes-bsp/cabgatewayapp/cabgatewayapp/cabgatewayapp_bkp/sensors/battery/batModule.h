/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file  batModule.h
 * @brief (Header file for battery module.)
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Sep/24/2018, VT , First Draft
 **************************************************************/

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include <pthread.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <semaphore.h>
#include <time.h>
#include "error.h"
#include "commonData.h"

/****************************************
 ************* DEFINES ******************
 ****************************************/
/**< Path to get battery reading */
#define BAT_READ_PATH       "/sys/bus/iio/devices/iio:device1/in_voltage0_raw"
#define BAT_SWITCH_PATH     "/sys/kernel/debug/cabgateway/powerSwitchStatus"
#define BAT_CHECK_EN        (40)              /**< GPIO02_IO08 */
#define BAT_SWITCH_OVER     (33)               /**< GPIO02_IO01 */
#define CHG_STAT1           (114)             /**< GPIO04_IO018 */
#define CHG_STAT2           (113)             /**< GPIO04_IO017 */

#define ADC_REF_VOLTAGE     (3.3)             /**< ADC reference voltage */
#define ADC_RESOLUTION      (4095)            /**< ADC resolution */
//#define HIGH_BAT_THRESHOLD  2.075091575     /**< High battery threshold */
//#define LOW_BAT_THRESHOLD   1.799487179     /**< Low battery threshold */
#define HIGH_BAT_THRESHOLD  (2.10007326)	    /**< High battery threshold */
#define LOW_BAT_11_PERC_THRESHOLD_VOLT   (1.753)     /**< Low battery threshold */
#define LOW_BAT_23_PERC_THRESHOLD_VOLT   (1.776)     /**< Low battery threshold */
#define LOW_BAT_29_PERC_THRESHOLD_VOLT   (1.799)     /**< Low battery threshold */
#define LOW_BAT_48_PERC_THRESHOLD_VOLT   (1.822)     /**< Low battery threshold */
#define LOW_BAT_57_PERC_THRESHOLD_VOLT   (1.845)     /**< Low battery threshold */
#define LOW_BAT_62_PERC_THRESHOLD_VOLT   (1.868)     /**< Low battery threshold */
#define LOW_BAT_70_PERC_THRESHOLD_VOLT   (1.891)     /**< Low battery threshold */
#define LOW_BAT_76_PERC_THRESHOLD_VOLT   (1.914)     /**< Low battery threshold */
#define LOW_BAT_84_PERC_THRESHOLD_VOLT   (1.937)     /**< Low battery threshold */
#define LOW_BAT_88_PERC_THRESHOLD_VOLT   (1.96)     /**< Low battery threshold */
#define HIGH_BAT_PERCENTAGE (100)             /**< High battery in percentage */
#define BAT_DATA_BUF_SIZE   (10)              /**< Battery data buffer size */
#define BAT_MONTOR_TIME_INTR   (500)	      /**< Battery monitor time interval */
#define BAT_GET_TIME_INTR   (100)	      /**< Battery monitor time interval */
#define BAT_READ_INIT_DELAY    (10)	      /**< Initial delay to read battery */


/****************************************
 ************* ENUMS ********************
 ****************************************/
/**
 * @enum chargingStatus
 *
 *  Enumeration for indicating charging status.
 */
typedef enum chargingStatus {
    PRE_CHARGE = 0,     /**< Pre charge in progress */
    FAST_CHARGE,        /**< Fast charge in progress */
    CHARGE_DONE,        /**< charging done */
    SLEEP_MODE,         /**< sleep mode / charge suspended / timer fault */
    MAX_CHARGE_STATUS   /**< Max number of charging status */
} chargingStatus_e;


/****************************************
 ************ STRUCTURES ****************
 ****************************************/

/****************************************
 ******** GLOBAL VARIABLES **************
 ****************************************/

/****************************************
 ******** EXTERN VARIABLES **************
 ****************************************/

/****************************************
 ************* CALLBACKS ****************
 ****************************************/
/**< To register callback to inform M&T module about power switching event */
typedef void (*notifyPowerSwitchEvent)(void *data);

/****************************************
 ********* FUNCTION PROTOTYPES **********
 ****************************************/
/******************************************************************
 *@brief (It is used to intialize battery module.)
 *
 *@param[IN] notifyPowerSwitchEvent (Indicates callback to notify power switching event.)
 *@param[IN] uint8_t (Indicates low battery threshold percentage.)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e initBatModule(notifyPowerSwitchEvent , uint8_t);

/******************************************************************
 *@brief (It is used to deintialize battery module.)
 *
 *@param[IN] void
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e deInitBatModule(void);

/******************************************************************
 *@brief (It is used to get information about current power source at which cab gateway operated.)
 *
 *@param[IN] None
 *@param[OUT] powerDevice_e* (Information about current power source at which cab gateway operated.)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e getPowerSource(powerDevice_e *);

/******************************************************************
 *@brief (It is used for notify to checking battery health.)
 *
 *@param[IN] void
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e notifyLowBatCond(void);

/******************************************************************
 *@brief (It is used to get information about charging stat.)
 *
 *@param[IN] None
 *@param[OUT] chargingStatus_e * (Information about charging stat.)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e getChargingStat(chargingStatus_e *);

/******************************************************************
 *@brief (It is used to handle the power switching event interrupt.)
 *
 *@param[IN] void
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e switchingSignalHandler(void);

/******************************************************************
 *@brief (It is used for checking latest battery health.)
 *
 *@param[IN] None
 *@param[OUT] powerSwitchEvent_e * (Information about current battery status of gateway battery.)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e getBatteryHealth(powerSwitchEvent_e *);
