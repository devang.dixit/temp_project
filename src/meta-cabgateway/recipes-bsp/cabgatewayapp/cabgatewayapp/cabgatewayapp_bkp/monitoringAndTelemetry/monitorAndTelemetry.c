/***************************************************************
 * Copyright(c) <2018>, Volansys
 * Description
 * @file : monitorAndTelemetry.c
 * @brief : This file provides the core buisness logic for
 *	Cab gateway
 * @Author     - Volansys
 * History
 *
 * Aug/29/2018, VT , First draft
 **************************************************************/

#include "monitorAndTelemetry.h"
#include "deviceMgr.h"
#include "dataControl.h"
#include "configuration.h"
#include "commonData.h"
#include "rfDeviceMgr.h"
#include "rtc.h"
#include "debug.h"
#include "sysTimer.h"
#include "diagnostic.h"
#include "userInterface.h"
#include "LteModule.h"
#include "eg91ATCmd.h"
#include "batDeviceMgr.h"
#include "syscall.h"
#include "cloud.h"
#include "otaModule.h"
#include "movingAvg.h"
#include <semaphore.h>
#include "bleUart.h"
#include "configData.h"
#include "memory.h"
#include "powerControl.h"
#include "gps.h"

#define LOW_BATT_THRESHOLD (30)
#define MPU_TEMPERATURE	"/sys/class/thermal/thermal_zone0/temp"

void xmissionCB(uint32_t data);

static TIMER_HANDLE xMissionHandle  = INVALID_TIMER_HANDLE;
static TIMER_HANDLE batTimerHandle  = INVALID_TIMER_HANDLE;
static TIMER_HANDLE lteTimerHandle  = INVALID_TIMER_HANDLE;
static TIMER_HANDLE diagTimerHandle = INVALID_TIMER_HANDLE;
static TIMER_HANDLE otaTimerHandle  = INVALID_TIMER_HANDLE;
static TIMER_HANDLE watchDogTimerHandle  = INVALID_TIMER_HANDLE;
static TIMER_HANDLE zipPrepTimerHandle = INVALID_TIMER_HANDLE;
static TIMER_HANDLE leakDetTimerHandle = INVALID_TIMER_HANDLE;
static TIMER_HANDLE simDetectTimerHandle = INVALID_TIMER_HANDLE;

static tpmsDiagDataInfo_t tempTPMSDiagData;
static sysOtherData_t tempSysOtherData;
static batteryDiagInfo_t tempBatDiagData;
static uint8_t count = 0;
static bool sendBackInitConfig = false;

typedef struct configMsgStruct {
	configParam_e changedConfig;
	union {
		time_t timeStamp;
		uint32_t tpmsSensId;
		tpmsConfig_t tpmsConfigData;
		vehicleType_e vehicleType;
		haloSNInfo_t haloSNData;
		tyreInfo_t tireDetails;
	};
} configMsgStruct_t;

extern bool isConfigChangefromCloud;

static returnCode_e systemModulesDeinit();
void batDMDataCB(void *batData);
extern bool isMemSame(char *s1, char *s2, uint16_t len);
typedef struct tpmsPsiInfo
{
	time_t time;
	float pressure;
} tpmsPsiInfo_t;

typedef struct tpmsUserConfig_t
{
	uint8_t evType;
	tpmsConfig_t tpmsConfigData;
} tpmsUserConfig_t;

extern bool g_bleKeepAliveThreadStatus;
extern const char *eventStr[];
/* Global variables */
static deviceManagerHandler_t *dmHandler = NULL;
static pthread_t tpmsThreadID_t;
static pthread_t configThreadID_t;
static pthread_t eventThreadID_t;
static pthread_t otaThreadID_t;
static mqd_t mqConfigQueue = -1;
static mqd_t mqTPMSQueue = -1;
static mqd_t mqEvQueue = -1;
static uint32_t alertCount = 0; 
static uint32_t targetPressure[MAX_AXLE_NO] = {0};
static uint8_t alertRecord[MAX_AXLE_NO][MAX_WHEEL_SIDE][MAX_TYRE_POS];
static uint8_t alertRecordPSI[MAX_AXLE_NO][MAX_WHEEL_SIDE][MAX_TYRE_POS];
static sysStates_e currStat = INIT_STATE;
static int8_t g_MaxRSSI = 0;
static bool g_isSub1Rcvd = false;
static uint16_t xMissionInternalTimerCount = XMISSION_INT_MOVING_TIM_COUNT;
static uint16_t localTimerCount = 1;
static uint16_t xMissionAlertTimer = 0;
static otaMgrState_e isOTARunning = OTA_NOT_RUNNING;
static bleState_e isBLEConnected = BLE_DISCONNECTED_MNT_FLAG;
static pthread_mutex_t xMitIntTimerLock;
static pthread_mutex_t otaStateLock;
static pthread_mutex_t bleStateLock;
static char initHardwareVersion[MAX_LEN_HARDWARE_VERSION] = {'\0'}; //Hardware version
static char initSoftwareVersion[MAX_LEN_SOFTWARE_VERSION] = {'\0'}; //software version
static sem_t otaSem;
static selfDiagReq_t g_SelfDiagReq = {.sensorID = {0x90624ABF, 0x9024CC69, 0x90625B0B, 0x819cad5c}};
static bool isSelfDiagCompleted = false;
static bool initRTCCoinCellStatus = false;
static time_t powerStartTime;
static otaDetails_t availableOTADetails;

static float tpmsCurrentPSIDelta[MAX_AXLE_NO][MAX_WHEEL_SIDE][MAX_TYRE_POS];
static float tpmsCurrentPressure[MAX_AXLE_NO][MAX_WHEEL_SIDE][MAX_TYRE_POS];
movingAvg_t tpmsDeltaMovAvg[MAX_AXLE_NO][MAX_WHEEL_SIDE][MAX_TYRE_POS];
pthread_mutex_t leakDetectMutex;
pthread_mutex_t alertRecordMutex;
uint8_t sub1KeepAliveCntr = 0, bleKeepAliveCntr=0, bleKeepAliveFailCount = 0;/*Check connection health with ble/sub1 module*/
sensorVendorType_e sensorMake = VENDOR_AVE_TECH;
bool bleAlive = true; /*Connection with ble alive??*/
uint8_t previousOTA_full_updated = 0;
static bool isGwVersionNtfyToCloud = false;
static bool isInitialOtaCheckPerformed = false;
static uint16_t g_simDetFailCnt = 0;
pthread_mutex_t statLock;
pthread_mutex_t rssiLock;
pthread_mutex_t sub1rxLock;
pthread_mutex_t keepAliveLock;
pthread_mutex_t otaNotifierLock;
pthread_mutex_t simDetCntVarLock;

static pthread_mutex_t recvRFDataFromDMHandlerThreadAliveLock;
static pthread_mutex_t configManagerThreadAliveLock;
static pthread_mutex_t eventManagerThreadAliveLock;
static pthread_mutex_t otaManagerThreadAliveLock;
static pthread_mutex_t otaDownloadLock;
static pthread_mutex_t otaDeatailsAvailableLock;
static pthread_mutex_t gatewayIdValidateLock;
static pthread_mutex_t resetStateLock;

static bool g_recvRFDataFromDMHandlerThreadAliveStatus = true;
static bool g_configManagerThreadAliveStatus = true;
static bool g_eventManagerThreadAliveStatus = true;
static bool g_otaManagerThreadAliveStatus = true;
static bool g_otaDownloadStarted = false;
static bool g_invalidGatewayId = false;
static bool g_resetState = false;
static bool bleAdvertisementStarted = false;

//extern time_t start_time, curr_time;
//extern uint8_t StartTimer = 0;

char vehPowerSource[] = "Vehicle Power";
char batPowerSource[] = "Battery Power";
char coincellPlugStatus[] = "Plugged";
char coincellUnplugStatus[] = "Unplugged";

#define STRST(state) (state == INIT_STATE) ? "INIT_STATE" : \
		     (state == CONFIG_STATE) ? "CONFIG_STATE" : \
		     (state == MOVING_STATE) ? "MOVING_STATE" : \
		     (state == PARKED_STATE) ? "PARKED_STATE" : \
		     (state == OTA_STATE) ? "OTA_STATE" : \
		     (state == DEINIT_STATE) ? "DEINIT_STATE" : \
		     (state == SELF_DIAG_STATE) ? "SELF_DIAG_STATE" : "invalid" 

const char *eventStr[] = {
        "INIT_DONE",    //0
        "CONFIG_SUCCESS", //1
        "TPMS_ALERT",     //2
        "OTA_CHECK",      //3
        "OTA_PROCESS",    //4
        "LTE_CHECK",      //5
        "BATT_CHECK",     //6
        "BATT_TO_VEH",    //7
        "VEH_TO_BATT",    //8
        "DIAG_CHECK",     //9
        "LOW_BATT",       //10
        "XMIT_CHECK",     //11
        "LEAK_DET",       //12
        "SIM_DET",       //13
        "SELF_DIAG",      //14
	"WD_TIMR_EV",
};

const char *statusStr[] = {
        "INIT_STATE",
        "CONFIG_STATE",
        "MOVING_STATE",
        "PARKED_STATE",
        "OTA_STATE",
        "DEINIT_STATE",
        "SELF_DIAG_STATE",
};

void setSimDetFailCnt(uint16_t value)
{
	pthread_mutex_lock(&simDetCntVarLock);
	g_simDetFailCnt = value;
	pthread_mutex_unlock(&simDetCntVarLock);
}

uint16_t getSimDetFailCnt()
{
 	uint16_t currentState;
	pthread_mutex_lock(&simDetCntVarLock);
	currentState = g_simDetFailCnt;
	pthread_mutex_unlock(&simDetCntVarLock);
	return currentState;
}
void setOTANotiFierState(bool value)
{
	pthread_mutex_lock(&otaNotifierLock);
	isGwVersionNtfyToCloud = value;
	pthread_mutex_unlock(&otaNotifierLock);
}

bool getOTANotifierState()
{
	bool currentState;
	pthread_mutex_lock(&otaNotifierLock);
	currentState = isGwVersionNtfyToCloud;
	pthread_mutex_unlock(&otaNotifierLock);
	return currentState;
}
static void otaNotifier()
{
   FUNC_ENTRY
   /* call ota software update version to cloud */
   if(getOTANotifierState() == false) {
           returnCode_e retCode = GEN_SUCCESS;
           otaDetails_t tempOTADetails;
           retCode = notifyToCloud(initSoftwareVersion, &tempOTADetails);
           if (retCode != GEN_SUCCESS) {
                   CAB_DBG(CAB_GW_ERR, "Failed to Notify Gw version to Cloud");
           }
           else {
                   CAB_DBG(CAB_GW_INFO, "Gateway version notified to cloud");
                   setOTANotiFierState(true);
           }
   }
   FUNC_EXIT
}

static returnCode_e updateLTETime()
{
	FUNC_ENTRY
	time_t timeData;
	returnCode_e retCode = GEN_SUCCESS;

	retCode = getLteNetTime(&timeData);
        if(retCode == GEN_SUCCESS)
        {
                retCode = dmHandler->rtcHandler->setData((void *)&timeData);
                if (retCode != GEN_SUCCESS) {
                        CAB_DBG(CAB_GW_ERR, "set RTC time failed with error %d", retCode);
                        return retCode;
                }

                retCode = setConfigModuleParam(SYS_DIAG_TIME_CMD, (void *)&timeData);
                if (retCode != GEN_SUCCESS) {
                        CAB_DBG(CAB_GW_WARNING, "set diag time in config failed with code %d", retCode);
			rDiagSendMessage(BLE_CONFIG_SET_FAIL, GW_WARNING, "BLE set diag failed for SYS_DIAG_TIME_CMD");
                        return retCode;
                }
        }

	FUNC_EXIT
	return retCode;
}

void setSub1DataRcvdState(bool isSub1Recvd)
{
	pthread_mutex_lock(&sub1rxLock);
	g_isSub1Rcvd = isSub1Recvd;
	pthread_mutex_unlock(&sub1rxLock);
}

bool getSub1DataRcvdState()
{
	bool isSub1Recvd;
	pthread_mutex_lock(&sub1rxLock);
	isSub1Recvd = g_isSub1Rcvd;
	pthread_mutex_unlock(&sub1rxLock);
	return isSub1Recvd;
}

void setMaxRSSI(int8_t maxRSSI)
{
	pthread_mutex_lock(&rssiLock);
	g_MaxRSSI = maxRSSI;
	pthread_mutex_unlock(&rssiLock);
}

int8_t getMaxRSSI()
{
	int8_t outRSSI;
	pthread_mutex_lock(&rssiLock);
	outRSSI = g_MaxRSSI;
	pthread_mutex_unlock(&rssiLock);
	return outRSSI;
}

void setState(sysStates_e state)
{
	pthread_mutex_lock(&statLock);
	currStat = state;
	CAB_DBG(CAB_GW_INFO, "State changed to : %s", STRST(state));
	pthread_mutex_unlock(&statLock);
}

sysStates_e getState()
{
	sysStates_e outState;
	pthread_mutex_lock(&statLock);
	outState = currStat;
	pthread_mutex_unlock(&statLock);
	return outState;
}

bool readyToCheckSub1KeepAlive(void)
{
	bool status = false;
	FUNC_ENTRY
	///*No need to check keep alive for XBR PressurePro*/
	//if(VENDOR_PRESSURE_PRO == sensorMake)
	//	return false;
	pthread_mutex_lock(&keepAliveLock);
	sub1KeepAliveCntr ++;
	if(sub1KeepAliveCntr >= SUB1_KEEPALIVE_CHECK_COUNTER)
		status = true;
	pthread_mutex_unlock(&keepAliveLock);
	CAB_DBG(CAB_GW_DEBUG,"readyToCheckSub1KeepAlive Count = %d",sub1KeepAliveCntr);
	return status;
	FUNC_EXIT
}

void resetSub1KeepAliveCntr(void)
{
	FUNC_ENTRY
	pthread_mutex_lock(&keepAliveLock);
	sub1KeepAliveCntr = 0 ;
	pthread_mutex_unlock(&keepAliveLock);
	FUNC_EXIT
}

bool readyToCheckBleKeepAlive(void)
{
	bool status = false;
	FUNC_ENTRY
	pthread_mutex_lock(&keepAliveLock);
	bleKeepAliveCntr ++;
	if(bleKeepAliveCntr >= BLE_KEEPALIVE_CHECK_COUNTER)
		status = true;
	pthread_mutex_unlock(&keepAliveLock);
	CAB_DBG(CAB_GW_DEBUG,"readyToCheckBleKeepAlive Count = %d",bleKeepAliveCntr);
	return status;
	FUNC_EXIT
}

void resetBleKeepAliveCntr(void)
{
	FUNC_ENTRY
	pthread_mutex_lock(&keepAliveLock);
	bleKeepAliveCntr = 0 ;
	pthread_mutex_unlock(&keepAliveLock);
	FUNC_EXIT
}

uint8_t getBleAliveFailCount(void)
{
	uint8_t retCount;
	FUNC_ENTRY
        pthread_mutex_lock(&keepAliveLock);
   	retCount = bleKeepAliveFailCount;
	pthread_mutex_unlock(&keepAliveLock);
	FUNC_EXIT
	return retCount;
}
void setBleAliveFailCount(uint8_t targetCount)
{
	FUNC_ENTRY
        pthread_mutex_lock(&keepAliveLock);
   	bleKeepAliveFailCount = targetCount;
	pthread_mutex_unlock(&keepAliveLock);
	FUNC_EXIT
	return;
}
bool getBleAliveStatus(void)
{
	bool status;
	FUNC_ENTRY
        pthread_mutex_lock(&keepAliveLock);
   	status = bleAlive;
	pthread_mutex_unlock(&keepAliveLock);
	FUNC_EXIT
	CAB_DBG(CAB_GW_DEBUG, "getBleAliveStatus = %d",status);
	return status;
}

void setBleAliveStatus(bool status)
{
	FUNC_ENTRY
	CAB_DBG(CAB_GW_DEBUG, "setBleAliveStatus = %d",status);
        pthread_mutex_lock(&keepAliveLock);
   	bleAlive = status;
	pthread_mutex_unlock(&keepAliveLock);
	FUNC_EXIT
}

void printfLocalDMData (tpmsEvent_t *localEvent)
{
	FUNC_ENTRY
	struct tm *debugTime = localtime(&(localEvent->tpmsDMData.time));
	CAB_DBG(CAB_GW_DEBUG,"\r\n\t===========================================================\r\n");
        CAB_DBG(CAB_GW_DEBUG,"\r\n\t\t\tTime : 20%d,%d,%d  %d:%d:%d	\
		\r\n\t   Lat: %4.2f Long: %4.2f Alt:%4.2f Speed: %4.2f  \
		\r\n\t   Axle:%d Type:%s Side:%s Position:%s Halo:%s \
		\r\n\t   ID:0x%x PRES:%4.1f TMP:%4.1f BAT:%4.1f RSSI:%d Status:%d\n",
                                    debugTime->tm_year - YEAR_OFFSET,   \
                                    debugTime->tm_mon + MONTH_OFFSET,   \
                                    debugTime->tm_mday, \
                                    debugTime->tm_hour, \
                                    debugTime->tm_min,  \
                                    debugTime->tm_sec,
                                    localEvent->tpmsDMData.gpsData.latitude,
                                    localEvent->tpmsDMData.gpsData.longitude,
                                    localEvent->tpmsDMData.gpsData.altitude,
                                    localEvent->tpmsDMData.gpsData.speed,
                                    localEvent->tpmsDMData.wheelInfo.axleNum,
                                    (localEvent->tpmsDMData.wheelInfo.attachedType == HALO) ? "HALO" : "TYRE",
                                    (localEvent->tpmsDMData.wheelInfo.vehicleSide == RIGHT) ? "RIGHT" : "LEFT",
				    (localEvent->tpmsDMData.wheelInfo.tyrePos == OUTER) ? "OUTER" : "INNER" ,
                                    (localEvent->tpmsDMData.wheelInfo.isHaloEnable == DISABLE) ? "DISABLE" : "ENABLE",
                                    localEvent->tpmsDMData.tpmsData.sensorID,
                                    localEvent->tpmsDMData.tpmsData.pressure,
                                    localEvent->tpmsDMData.tpmsData.temperature,
                                    localEvent->tpmsDMData.tpmsData.battery,
                                    localEvent->tpmsDMData.tpmsData.rssi,
                                    localEvent->tpmsDMData.tpmsData.status);
	CAB_DBG(CAB_GW_DEBUG, "\r\n\t===========================================================\r\n");
	fflush(stdout);
	FUNC_EXIT
}

static void printfTPMSUserConfig(tpmsUserConfig_t tpmsUserConfig)
{
	FUNC_ENTRY
	CAB_DBG(CAB_GW_DEBUG, "\r\n\t===========================================================\r\n");
	CAB_DBG(CAB_GW_DEBUG, "\r\n\t\t\tEvent: %s \
		\r\n\t   SensorID: 0x%x wheelId: %d \
		\r\n\t   Axle:%d Type:%s Side:%s Position:%s Halo:%s \
		\r\n\t   \n",
					(tpmsUserConfig.evType == 1)?"Add Tpms Sensor":"Remove Tpms Sensor",
					tpmsUserConfig.tpmsConfigData.sensorID,
					tpmsUserConfig.tpmsConfigData.wheelID,
					tpmsUserConfig.tpmsConfigData.wheelInfo.axleNum,
					(tpmsUserConfig.tpmsConfigData.wheelInfo.attachedType == HALO) ? "HALO": "TYRE",
					(tpmsUserConfig.tpmsConfigData.wheelInfo.vehicleSide == RIGHT) ? "RIGHT": "LEFT",
					(tpmsUserConfig.tpmsConfigData.wheelInfo.tyrePos == OUTER) ? "OUTER" : "INNER",
					(tpmsUserConfig.tpmsConfigData.wheelInfo.isHaloEnable == DISABLE) ? "DISABLE" : "ENABLE");
	CAB_DBG(CAB_GW_DEBUG, "\r\n\t===========================================================\r\n");
	FUNC_EXIT
}

static returnCode_e eventQueueSetup()
{
	FUNC_ENTRY
	returnCode_e retCode = GEN_SUCCESS;
	struct mq_attr attr;

	/* initialize the queue attributes */
	attr.mq_flags = 0;
	attr.mq_maxmsg = EVENT_MSGQ_MAX_SIZE;
	//attr.mq_maxmsg = 5;
	attr.mq_msgsize = sizeof(cgEventData_t);
	attr.mq_curmsgs = 0;

	/* Cleanup the msg queue if exist */
	mq_unlink(EVENT_QUEUE_NAME);

	/* create the message queue */
	mqEvQueue = mq_open(EVENT_QUEUE_NAME, O_CREAT | O_RDWR, 0644, &attr);
	if(mqEvQueue < 0)
	{
		CAB_DBG(CAB_GW_ERR, "mq_open for Event queue fail with error %s", strerror(errno));
		return MQ_OPEN_ERROR;
	}

	FUNC_EXIT
	return retCode;
}

static returnCode_e TPMSMsgQueueSetup()
{
	FUNC_ENTRY
	returnCode_e retCode = GEN_SUCCESS;
	struct mq_attr attr;

	/* initialize the queue attributes */
	attr.mq_flags = 0;
	attr.mq_maxmsg = TPMS_MSGQ_MAX_SIZE;
	attr.mq_msgsize = sizeof(tpmsDMData_t);
	attr.mq_curmsgs = 0;

	/* Cleanup the msg queue if exist */
	mq_unlink(TPMS_QUEUE_NAME);

	/* create the message queue */
	mqTPMSQueue = mq_open(TPMS_QUEUE_NAME, O_CREAT | O_RDWR, 0644, &attr);
	if(mqTPMSQueue < 0)
	{
		CAB_DBG(CAB_GW_ERR, "mq_open for TPMS fail with error %s", strerror(errno));
		return MQ_OPEN_ERROR;
	}

	FUNC_EXIT
	return retCode;
}

static returnCode_e configQueueSetup()
{
	FUNC_ENTRY
	returnCode_e retCode = GEN_SUCCESS;
	struct mq_attr configQueueAttr;

	/* initialize the queue attributes */
	configQueueAttr.mq_flags = 0;
	configQueueAttr.mq_maxmsg = CONFIG_MSGQ_MAX_SIZE;
	configQueueAttr.mq_msgsize = sizeof(configMsgStruct_t);
	configQueueAttr.mq_curmsgs = 0;

	/* Cleanup the msg queue if exist */
	mq_unlink(CONFIG_QUEUE_NAME);

	/* create the message queue */
	mqConfigQueue = mq_open(CONFIG_QUEUE_NAME, O_CREAT | O_RDWR, 0644, &configQueueAttr);
	if(mqConfigQueue < 0)
	{
		CAB_DBG(CAB_GW_ERR, "mq_open for Config queue fail with error %s", strerror(errno));
		return MQ_OPEN_ERROR;
	}

	FUNC_EXIT
	return retCode;
}


static void tireLeakageDetection()
{
        uint8_t axleNo = 0;
        vehicleSide_e vehicleSide;          //Vehicle side (i.e. left or right)
        tyrePos_e tyrePos;                  //Wheel position (i.e. Inner or Outer)
        float tempAvg = 0;
	MAVG_STATUS_e mavgStatus = MAVG_SUCCESS;

        pthread_mutex_lock(&leakDetectMutex);
        pthread_mutex_lock(&alertRecordMutex);
        for (axleNo = 0; axleNo < MAX_AXLE_NO; axleNo++) {
                for (vehicleSide = 0; vehicleSide < MAX_WHEEL_SIDE; vehicleSide++) {
                        for (tyrePos = 0; tyrePos < MAX_TYRE_POS; tyrePos++) {
				/* Calculate 5 PSI leak for each tire*/
                                mavgStatus = calcMovingAvg(&tpmsDeltaMovAvg[axleNo][vehicleSide][tyrePos], tpmsCurrentPSIDelta[axleNo][vehicleSide][tyrePos], &tempAvg);
				if (mavgStatus != MAVG_SUCCESS) {
					CAB_DBG(CAB_GW_ERR, "Error in calculating moving avg");
				}
                                tpmsCurrentPSIDelta[axleNo][vehicleSide][tyrePos] = 0;
                                CAB_DBG(CAB_GW_TRACE, "Tire PSI Delta %d:%d:%d -> %f", axleNo, vehicleSide, tyrePos, tempAvg);

                                if (tempAvg <= (-TIRE_LEAK_THRESHOLD_DIFF)) {
                                        alertRecordPSI[axleNo][vehicleSide][tyrePos] = 0x01;
                                } else {
                                        alertRecordPSI[axleNo][vehicleSide][tyrePos] = 0x00;
                                }
                        }
                }
        }
        pthread_mutex_unlock(&alertRecordMutex);
        pthread_mutex_unlock(&leakDetectMutex);
}

static void updateTirePressureLeakDetection(uint8_t axleNo, vehicleSide_e vehicleSide, tyrePos_e tyrePos, float tirePressure)
{
        pthread_mutex_lock(&leakDetectMutex);
	if (tpmsCurrentPressure[axleNo][vehicleSide][tyrePos] != -1) {
                /* Ignore positive delta */
                if ( (tirePressure - tpmsCurrentPressure[axleNo][vehicleSide][tyrePos]) < 0) {
                        tpmsCurrentPSIDelta[axleNo][vehicleSide][tyrePos] += (tirePressure - tpmsCurrentPressure[axleNo][vehicleSide][tyrePos]);
                }
		CAB_DBG(CAB_GW_DEBUG, "= CP:%f PD: %d:%d:%d:%f =", tpmsCurrentPressure[axleNo][vehicleSide][tyrePos], axleNo, vehicleSide, tyrePos, tpmsCurrentPSIDelta[axleNo][vehicleSide][tyrePos]);
        }
        tpmsCurrentPressure[axleNo][vehicleSide][tyrePos] = tirePressure;
        pthread_mutex_unlock(&leakDetectMutex);
        pthread_mutex_lock(&alertRecordMutex);
        if (alertRecordPSI[axleNo][vehicleSide][tyrePos] == 0x01) {
                alertRecord[axleNo][vehicleSide][tyrePos] |= 0x01;
        } else {
                alertRecord[axleNo][vehicleSide][tyrePos] &= ~0x01;
        }
        pthread_mutex_unlock(&alertRecordMutex);
}

static void tireLowPressureDetection(uint8_t axleNo, vehicleSide_e vehicleSide, tyrePos_e tyrePos, float tirePressure)
{
	// Alert detection
        pthread_mutex_lock(&alertRecordMutex);
	CAB_DBG(CAB_GW_DEBUG,"Filter cond | %d %d %d\n" , (tirePressure < (targetPressure[axleNo] * 0.8)),
			(alertRecord[axleNo][vehicleSide][tyrePos]), ((targetPressure[axleNo] * 0.9) > tirePressure));
	if( ( (tirePressure < (targetPressure[axleNo] * 0.8)) ) || \
		((alertRecord[axleNo][vehicleSide][tyrePos] & 0x02) && ((targetPressure[axleNo] * 0.9) > tirePressure)) )
	{
		alertRecord[axleNo][vehicleSide][tyrePos] |= 0x02;
		CAB_DBG(CAB_GW_DEBUG, "Alert condition detected. measured %f target[%d] %d alertCount %d alertRecord: %d \n" , \
				tirePressure, axleNo, targetPressure[axleNo], alertCount, alertRecord[axleNo][vehicleSide][tyrePos]);
	}
	else
	{
		alertRecord[axleNo][vehicleSide][tyrePos] &= ~0x02;
		CAB_DBG(CAB_GW_DEBUG, "Normal condition detected. measured %f target[%d] %d alertCount %d alertRecord: %d \n" , tirePressure, axleNo, \
				targetPressure[axleNo], alertCount, alertRecord[axleNo][vehicleSide][tyrePos]);
	}
        pthread_mutex_unlock(&alertRecordMutex);
}

static void statusLeakDetection(uint8_t axleNo, vehicleSide_e vehicleSide, tyrePos_e tyrePos, uint8_t status)
{
        pthread_mutex_lock(&alertRecordMutex);
	// Detect if it is a slow leak, fast leak or temperature protection alert

	if((status == 0x03)||(status == 0x04)||(status == 0x06))
	{
		alertRecord[axleNo][vehicleSide][tyrePos] |= 0x04;
		CAB_DBG(CAB_GW_DEBUG, "Alert condition detected. slow leak %d fast leak %d temerature protection %d \n" , \
				(status == 0x03), (status == 0x04), (status == 0x06));
	}
	else
	{
		alertRecord[axleNo][vehicleSide][tyrePos] &= ~0x04;
		CAB_DBG(CAB_GW_DEBUG, "Normal condition detected. Normal %d slow leak %d fast leak %d temerature protection %d \n" , \
				(status == 0x01), (status == 0x03), (status  == 0x04), (status == 0x06));
	}
        pthread_mutex_unlock(&alertRecordMutex);
}

static returnCode_e tpmsInit()
{
	uint8_t axleNo = 0;
	vehicleSide_e vehicleSide = 0;
	MAVG_STATUS_e mavgStatus = MAVG_SUCCESS;
	tyrePos_e tyrePos = 0;

	CAB_DBG(CAB_GW_INFO, "Initializing TPMS information object");

	pthread_mutex_lock(&leakDetectMutex);
	pthread_mutex_lock(&alertRecordMutex);
        for (axleNo = 0; axleNo < MAX_AXLE_NO; axleNo++) {
                for (vehicleSide = 0; vehicleSide < MAX_WHEEL_SIDE; vehicleSide++) {
                        for (tyrePos = 0; tyrePos < MAX_TYRE_POS; tyrePos++) {
                                mavgStatus = initMovingAvg(&tpmsDeltaMovAvg[axleNo][vehicleSide][tyrePos], TIRE_LEAK_MAVG_WINDOW_SIZE);
				if (mavgStatus != MAVG_SUCCESS) {
					CAB_DBG(CAB_GW_ERR, "Error in initializing Moving Avg");
					return MODULE_INIT_FAIL;
				}
                                tpmsCurrentPressure[axleNo][vehicleSide][tyrePos] = -1;
                                tpmsCurrentPSIDelta[axleNo][vehicleSide][tyrePos] = 0;
                                alertRecord[axleNo][vehicleSide][tyrePos] = 0;
                                alertRecordPSI[axleNo][vehicleSide][tyrePos] = 0;
                        }
                }
        }
	pthread_mutex_unlock(&alertRecordMutex);
	pthread_mutex_unlock(&leakDetectMutex);
	return GEN_SUCCESS;
}

static returnCode_e tpmsDeInit()
{
	uint8_t axleNo = 0;
	vehicleSide_e vehicleSide = 0;
	MAVG_STATUS_e mavgStatus = MAVG_SUCCESS;
	tyrePos_e tyrePos = 0;

	CAB_DBG(CAB_GW_INFO, "Deinitializing TPMS information object");

	pthread_mutex_lock(&leakDetectMutex);
	pthread_mutex_lock(&alertRecordMutex);
        for (axleNo = 0; axleNo < MAX_AXLE_NO; axleNo++) {
                for (vehicleSide = 0; vehicleSide < MAX_WHEEL_SIDE; vehicleSide++) {
                        for (tyrePos = 0; tyrePos < MAX_TYRE_POS; tyrePos++) {
                                mavgStatus = deInitMovingAvg(&tpmsDeltaMovAvg[axleNo][vehicleSide][tyrePos]);
				if (mavgStatus != MAVG_SUCCESS) {
					CAB_DBG(CAB_GW_ERR, "Deinit moving avg failed");
					return MODULE_DEINIT_FAIL;
				}
                                tpmsCurrentPressure[axleNo][vehicleSide][tyrePos] = -1;
                                tpmsCurrentPSIDelta[axleNo][vehicleSide][tyrePos] = 0;
                                alertRecord[axleNo][vehicleSide][tyrePos] = 0;
                                alertRecordPSI[axleNo][vehicleSide][tyrePos] = 0;
                        }
                }
        }
	pthread_mutex_unlock(&alertRecordMutex);
	pthread_mutex_unlock(&leakDetectMutex);
	return GEN_SUCCESS;
}

static returnCode_e processTpmsData(tpmsEvent_t *tpmsEventData, uint8_t *isAlert)
{
	returnCode_e retCode = GEN_SUCCESS;
	uint8_t localAlertRecord = 0;

	if(!tpmsEventData || !isAlert)
	{
		CAB_DBG(CAB_GW_ERR, "Invalid argument");
		return GEN_NULL_POINTING_ERROR;
	}

	uint8_t currAxleNum = tpmsEventData->tpmsDMData.wheelInfo.axleNum - 1;
	vehicleSide_e currWS = tpmsEventData->tpmsDMData.wheelInfo.vehicleSide;
	tyrePos_e currTP = tpmsEventData->tpmsDMData.wheelInfo.tyrePos;
	float mesdPressure = tpmsEventData->tpmsDMData.tpmsData.pressure;

        pthread_mutex_lock(&alertRecordMutex);
	localAlertRecord = alertRecord[currAxleNum][currWS][currTP];
        pthread_mutex_unlock(&alertRecordMutex);

	updateTirePressureLeakDetection(currAxleNum, currWS, currTP, mesdPressure);
	tireLowPressureDetection(currAxleNum, currWS, currTP, mesdPressure);
	statusLeakDetection(currAxleNum, currWS, currTP, tpmsEventData->tpmsDMData.tpmsData.status);

        pthread_mutex_lock(&alertRecordMutex);
	if (alertRecord[currAxleNum][currWS][currTP]) {
		CAB_DBG(CAB_GW_DEBUG, "Alert Data");
		tpmsEventData->alert = 1;
		if (localAlertRecord == 0) {
			CAB_DBG(CAB_GW_DEBUG, "First Alert for the tire, transmit to cloud");
			*isAlert = 1;
		}
	}
        pthread_mutex_unlock(&alertRecordMutex);
	return retCode;
}

static bleState_e getBLEConnectState()
{
	bleState_e tempBLEState = MAX_BLE_STATES;
	pthread_mutex_lock(&bleStateLock);
	tempBLEState = isBLEConnected;
	pthread_mutex_unlock(&bleStateLock);
	return tempBLEState;
}

static void setBLEConnectState(bleState_e bleConnect)
{
	pthread_mutex_lock(&bleStateLock);
	isBLEConnected = bleConnect;
	pthread_mutex_unlock(&bleStateLock);
}

static otaMgrState_e getOTAState()
{
	otaMgrState_e tempOTAState = MAX_OTA_STATES;
	pthread_mutex_lock(&otaStateLock);
	tempOTAState = isOTARunning;
	pthread_mutex_unlock(&otaStateLock);
	return tempOTAState;
}

static void setOTAState(otaMgrState_e otastate)
{
	pthread_mutex_lock(&otaStateLock);
	isOTARunning = otastate;
	pthread_mutex_unlock(&otaStateLock);
}

static returnCode_e otaProceeedValidation(uint8_t *canProceed)
{
	returnCode_e retCode = GEN_SUCCESS;
	batDM_Data_t tempBatDMData;

	if (canProceed == NULL) {
		CAB_DBG(CAB_GW_ERR, "OTA proceed invalid args");
		return GEN_INVALID_ARG;
	}

	*canProceed = 0;
	retCode = dmHandler->batHandler->getData((void *)&tempBatDMData);
        if(retCode != GEN_SUCCESS) {
                CAB_DBG(CAB_GW_ERR, "Battery get data failed with code %d", retCode);
                return retCode;
        }

        if(tempBatDMData.powerDev == VEHICLE_POWER) {
		*canProceed = 1;
        }

	return GEN_SUCCESS;
}

static bool getOtaManagerThreadAliveStatus(void)
{
        bool status;
        pthread_mutex_lock(&otaManagerThreadAliveLock);
        status = g_otaManagerThreadAliveStatus;
        pthread_mutex_unlock(&otaManagerThreadAliveLock);
        return status;
}

static void setOtaManagerThreadAliveStatus(bool value)
{
        pthread_mutex_lock(&otaManagerThreadAliveLock);
        g_otaManagerThreadAliveStatus = value;
        pthread_mutex_unlock(&otaManagerThreadAliveLock);
}

bool otaManagerCallback(void)
{
        bool status = false;
        
	CAB_DBG(CAB_GW_TRACE, "In func : %s", __func__);
        if(getOtaManagerThreadAliveStatus() == true)
        {
                status = true;
        }

	pthread_mutex_lock(&otaDownloadLock);
	if(g_otaDownloadStarted == false)
	{
	        setOtaManagerThreadAliveStatus(false);
	}
	pthread_mutex_unlock(&otaDownloadLock);

        return status;
}

static void otaCheckCB(uint32_t data)
{
	otaProgressState_e OTAProgressState;
	/* This call back will only be called if the OTA is downloaded
	 * and still BLE connection is there. No OTA details required
	 * at this stage. */
	OTAProgressState = data;
	sendEvent(OTA_CHECK, (void *)&OTAProgressState, sizeof(otaProgressState_e));
}

static void startOTATimer(otaProgressState_e state)
{
	TIMER_STATUS_e timerRetSTat = TIMER_SUCCESS;
	TIMER_INFO_t otaTimerInfo = { \
			.count = BLE_OTA_TOUT_TIMER_COUNT, \
			.funcPtr = otaCheckCB, \
			.data = state};

	timerRetSTat = startSystemTimer(otaTimerInfo, &otaTimerHandle);
	CAB_DBG(CAB_GW_INFO, "OTA timer started");
	if (timerRetSTat != TIMER_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "OTA sys timer start failed with timer module code : %d", timerRetSTat);
	}
}

static void *otaManager(void *arg)
{
	returnCode_e retCode = GEN_SUCCESS;
	uint8_t canOTAProceed = 0;
	otaDetails_t tempOTADetails;
	uint8_t isOTAAvailable = 0;
	uint8_t isDownloadComplete = 0;
	uint8_t isProcessComplete = 0;
	otaProgressState_e OTAProgressStatus;

	memset(&tempOTADetails, 0, sizeof(otaDetails_t));

	watchDogTimer_t newWatchdogNode;

	newWatchdogNode.threadId = syscall(SYS_gettid);
	newWatchdogNode.CallbackFn = otaManagerCallback;

	CAB_DBG(CAB_GW_INFO, "OTA manager thread id : %ld", syscall(SYS_gettid));
	retCode = threadRegister(&newWatchdogNode);

	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "OTA manager thread registration failed with error %d", retCode);
		rDiagSendMessage(WATCHDOG_REGISTRATION_FAIL, GW_ERR, "OTA manager thread registration failed");
	}

	if (!arg)
	{
		CAB_DBG(CAB_GW_ERR, "Invalid argument");
		goto exitOTA;
	}

	memcpy(&OTAProgressStatus, (otaProgressState_e *)arg, sizeof(otaProgressState_e));

	setOtaManagerThreadAliveStatus(true);
	pthread_mutex_lock(&otaDownloadLock);
	g_otaDownloadStarted = false;
	pthread_mutex_unlock(&otaDownloadLock);

	/* If the OTA check is already done, skip it.  */
	if (OTAProgressStatus < OTA_CHECK_DONE)
	{
		retCode = otaProceeedValidation(&canOTAProceed);
		if (retCode != GEN_SUCCESS || canOTAProceed == 0) {
			CAB_DBG(CAB_GW_WARNING, "condition for OTA failed exiting OTA thread");
			rDiagSendMessage(OTA_VALIDATION_ERROR, GW_WARNING, "OTA proceed validation failed");
			goto exitOTA;
		}

		retCode = checkOTA(initSoftwareVersion, &tempOTADetails, &isOTAAvailable);
		if (retCode != GEN_SUCCESS){
			CAB_DBG(CAB_GW_ERR, "Check OTA API failed, exiting OTA manager");
			goto exitOTA;
		}
		else{
			if(0 == isOTAAvailable){
				CAB_DBG(CAB_GW_ERR, "No OTA to be performed! Exiting OTA manager.");
				goto exitOTA;
			}
		}

		pthread_mutex_lock(&otaDeatailsAvailableLock);
		/* Saving the current OTA details into global structure to use later in case BLE is connected. */
		availableOTADetails = tempOTADetails;
		pthread_mutex_unlock(&otaDeatailsAvailableLock);
	}
	/* In case OTA details are available from XMIT_EVENT, pass the OTA details. */
	else if (OTAProgressStatus == OTA_CHECK_DONE)
	{
		if (memcmp(&availableOTADetails, &tempOTADetails, sizeof(otaDetails_t)) != 0)
		{
			pthread_mutex_lock(&otaDeatailsAvailableLock);
			tempOTADetails = availableOTADetails;
			pthread_mutex_unlock(&otaDeatailsAvailableLock);

			strcpy(tempOTADetails.otaInfo.storagePath, OTA_DIR_PATH);
			memcpy(tempOTADetails.otaInfo.version, initSoftwareVersion, strlen(initSoftwareVersion));

			CAB_DBG(CAB_GW_INFO, "Checking OTA...");
			CAB_DBG(CAB_GW_INFO, "Current SW Version : %s", tempOTADetails.otaInfo.version);
			CAB_DBG(CAB_GW_INFO, "OTA SW Version : %s", tempOTADetails.otaInfo.updateVersion);
			CAB_DBG(CAB_GW_INFO, "OTA File Path : %s", tempOTADetails.otaInfo.storagePath);
			CAB_DBG(CAB_GW_INFO, "OTA File Name : %s", tempOTADetails.otaInfo.filename);
			CAB_DBG(CAB_GW_INFO, "OTA URL : %s", tempOTADetails.otaInfo.otaUrl);
			CAB_DBG(CAB_GW_INFO, "OTA File MD5sum : %s", tempOTADetails.otaInfo.md5sum);

			if(compareVersion(tempOTADetails.otaInfo.updateVersion, initSoftwareVersion) > 0) {
				CAB_DBG(CAB_GW_INFO, "OTA available");
				tempOTADetails.state = MAX_OTA_STATE;
			}
		}
		else
		{
			CAB_DBG(CAB_GW_ERR, "Available OTA details are not valid.");
			goto exitOTA;
		}
	}
	else if (OTAProgressStatus == OTA_DOWNLOAD_DONE)
	{
		if (memcmp(&availableOTADetails, &tempOTADetails, sizeof(otaDetails_t)) != 0)
		{
			pthread_mutex_lock(&otaDeatailsAvailableLock);
			tempOTADetails = availableOTADetails;
			pthread_mutex_unlock(&otaDeatailsAvailableLock);

			strcpy(tempOTADetails.otaInfo.storagePath, OTA_DIR_PATH);
			memcpy(tempOTADetails.otaInfo.version, initSoftwareVersion, strlen(initSoftwareVersion));
		}
		else
		{
			CAB_DBG(CAB_GW_ERR, "Available OTA details are not valid.");
			goto exitOTA;
		}
	}

	retCode = otaProceeedValidation(&canOTAProceed);
	if (retCode != GEN_SUCCESS || canOTAProceed == 0) {
		CAB_DBG(CAB_GW_WARNING, "condition for OTA failed exiting OTA thread");
		rDiagSendMessage(OTA_VALIDATION_ERROR, GW_WARNING, "OTA proceed validation failed");
		goto exitOTA;
	}

	pthread_mutex_lock(&otaDownloadLock);
	g_otaDownloadStarted = true;
	pthread_mutex_unlock(&otaDownloadLock);

	// Reload watch dog timer before starting download OTA to avoid any interrupt in downloading
	reloadTimer(watchDogTimerHandle, WATCHDOG_TIMER_COUNT);

	retCode = downloadOTA(&tempOTADetails, &isDownloadComplete);
	if (retCode != GEN_SUCCESS || isDownloadComplete == 0) {
		if (retCode == CHECKSUM_FAIL) {
			CAB_DBG(CAB_GW_WARNING, "Checksum verification failed for OTA");
			rDiagSendMessage(OTA_MD5SUM_ERROR, GW_WARNING, "OTA MD5sum verification failed");
		}
		CAB_DBG(CAB_GW_ERR, "Download OTA failed, exiting OTA manager");
		goto exitOTA;
	}

	retCode = otaProceeedValidation(&canOTAProceed);
	if (retCode != GEN_SUCCESS || canOTAProceed == 0) {
		CAB_DBG(CAB_GW_WARNING, "condition for OTA failed exiting OTA thread");
		rDiagSendMessage(OTA_VALIDATION_ERROR, GW_WARNING, "OTA proceed validation failed");
		goto exitOTA;
	}
	/*If gateway is already in DEINIT_STATE due to other error condition generated 
	while OTA firmware was downloading, skip BLE connection validation and posting
	OTA_PROCESS event. This is necessary because deinit method may have destroy 
	the symbols which is mandatory to use below methods*/
	if(getState() != DEINIT_STATE){
		if (getBLEConnectState() == BLE_CONNECTED_MNT_FLAG) {
			if (otaTimerHandle != INVALID_TIMER_HANDLE) {
			    retCode = reloadTimer(otaTimerHandle, BLE_OTA_TOUT_TIMER_COUNT);
			    if (retCode !=  TIMER_SUCCESS)
			    {
			        CAB_DBG(CAB_GW_ERR, "OTA failed due to reloading the BLE connection wait timer.");
			        goto exitOTA;
			    }
			}
			else {
				startOTATimer(OTA_DOWNLOAD_DONE);
			}

			count++;

			if (count == 3)
			{
				deleteTimer(&otaTimerHandle);
				CAB_DBG(CAB_GW_ERR, "Reboot the system");
				CAB_DBG(CAB_GW_WARNING, "OTA failed due to constant BLE connection");
				rDiagSendMessage(OTA_BLE_CONNECTION_ERROR, GW_WARNING, "OTA failed due to constant BLE connection");
				rebootSystem();
			}
			goto exitOTA;
		}

		sendEvent(OTA_PROCESS, NULL, 0);
	}

	/* Delete the timer, we do not need that now.
	 * It may be the case BLE is disconnected or the system is in deinit state*/
	if (otaTimerHandle != INVALID_TIMER_HANDLE) {
		deleteTimer(&otaTimerHandle);
		otaTimerHandle = INVALID_TIMER_HANDLE;
	}

	sem_wait(&otaSem);

	indicateSysStatus(OTA_PROCESS_PATTERN);

	retCode = processOTA(&isProcessComplete);
	if (retCode != GEN_SUCCESS || isProcessComplete == 0) {
		CAB_DBG(CAB_GW_WARNING, "Download OTA API failed, exiting OTA manager");
		rDiagSendMessage(OTA_PERFORM_ERROR, GW_WARNING, "OTA perform API failed");
		goto exitOTA;
	}

exitOTA:
	setOTAState(OTA_NOT_RUNNING);
	retCode = threadDeregister(newWatchdogNode.threadId);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "OTA Manager thread de-registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "OTA Manager thread de-registration failed");
        }
	pthread_detach(pthread_self());
	pthread_exit(0);
}

static void startOTAThread(otaProgressState_e * Status)
{
	if (!Status)
	{
		CAB_DBG(CAB_GW_ERR, "Invalid argument");
		return;
	}

	if (getOTAState() != OTA_THREAD_RUNNING) {
		if(pthread_create(&otaThreadID_t, NULL, otaManager, Status) == 0) {
			setOTAState(OTA_THREAD_RUNNING);
		}
	}
}

void notifyDiagCheckEv(void)
{
	sendEvent(DIAG_CHECK, NULL, 0);
}

static returnCode_e logSysEvent(sysDataType_e type, powerDevice_e pwrSrc, alert_e alertStat)
{
	returnCode_e retCode = GEN_SUCCESS;
	sysOtherData_t sysData;

	memset(&sysData, 0, sizeof(sysData));
	sysData.type = type;
	retCode = dmHandler->rtcHandler->getData((void *)&(sysData.time));
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "get time data failed with error %d", retCode);
		return retCode;
	}

	sysData.batteryData.device = pwrSrc;
	sysData.batteryData.alert = alertStat;

	retCode = storeDataToSDC((void *)&sysData, SYS_OTHER_DATA);
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Store SDC data failed with %d", retCode);
		return retCode;
	}
	/*Report power source switching event (not alert event) to diag module*/
	if(!alertStat){
		if(VEHICLE_POWER == pwrSrc)
			memcpy(tempBatDiagData.powerSource,vehPowerSource, sizeof(vehPowerSource));
		else
			memcpy(tempBatDiagData.powerSource,batPowerSource, sizeof(batPowerSource));
		tempBatDiagData.startTime = sysData.time;
		retCode = diagSetBatRtcData((void *)&tempBatDiagData);
        	if (retCode != GEN_SUCCESS) {
                	CAB_DBG(CAB_GW_ERR, "DIAG Set Bat RTC Info failed with code %d", retCode);
                	return retCode;
        	}
	}
	return retCode;
}

static returnCode_e checkBatteryStat(uint8_t *isLowBatt)
{
	returnCode_e retCode = GEN_SUCCESS;
	batDM_Data_t tempBatDMData;

	/* Check battery condition */
	retCode = dmHandler->batHandler->getData((void *)&tempBatDMData);
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "GEt BAT DM data failed with code %d", retCode);
		return retCode;
	}
	if(tempBatDMData.powerDev == BACKUP_BATTERY)
	{
		if(tempBatDMData.batHealth == LOW_BAT)
		{
			CAB_DBG(CAB_GW_INFO, "Low battery detected. Cann't proceed");
			*isLowBatt = 1;
		}
	}
	return retCode;
}

static bool getEventManagerThreadAliveStatus(void)
{
        bool status;
        pthread_mutex_lock(&eventManagerThreadAliveLock);
        status = g_eventManagerThreadAliveStatus;
        pthread_mutex_unlock(&eventManagerThreadAliveLock);
        return status;
}

static void setEventManagerThreadAliveStatus(bool value)
{
        pthread_mutex_lock(&eventManagerThreadAliveLock);
        g_eventManagerThreadAliveStatus = value;
        pthread_mutex_unlock(&eventManagerThreadAliveLock);
}

bool eventManagerCallback(void)
{
        bool status = false;
        CAB_DBG(CAB_GW_TRACE, "In func : %s", __func__);
        if(getEventManagerThreadAliveStatus() == true)
        {
                status = true;
        }

        setEventManagerThreadAliveStatus(false);

	sendEvent(WATCHDOG_TIMER_EVENT, NULL, 0);
        return status;
}

static void *eventManager(void *arg)
{
	FUNC_ENTRY
	cgEventData_t cgEventData;
	ssize_t bytes_read;
	uint8_t selfExit = 0;
        char tempSimIccid[MAX_LEN_SIM_ICCID];           //sim iccid
	uint8_t present = 0;
	char simNotPStr[] = NO_SIM_PRESENT_STR;
	char iccidNA[] = "ICCID Not Avail";
	tpmsConfig_t aliveCheckConfig;
	uint8_t count = 0;
	char errorMsg[100] = "";

	otaProgressState_e OTAProgressState;

	batDM_Data_t tempBatDMData;
	returnCode_e retCode = GEN_SUCCESS;
	watchDogTimer_t newWatchdogNode;

	newWatchdogNode.threadId = syscall(SYS_gettid);
	newWatchdogNode.CallbackFn = eventManagerCallback;

	CAB_DBG(CAB_GW_INFO, "Event manager thread id : %ld", syscall(SYS_gettid));
	retCode = threadRegister(&newWatchdogNode);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "Event manager thread registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_REGISTRATION_FAIL, GW_ERR, "Event manager thread registration failed");
        }

	while(1)
	{
		memset(&cgEventData, 0, sizeof(cgEventData_t));
		bytes_read = mq_receive(mqEvQueue, (char *)&(cgEventData), sizeof(cgEventData_t), NULL);
		if (getState() == DEINIT_STATE) {
			CAB_DBG(CAB_GW_INFO, "Event manager thread deinit");
			pthread_exit(0);
		}

		if(bytes_read >= 0)
		{
			CAB_DBG(CAB_GW_INFO, "--- Event : %s --- ", eventStr[cgEventData.evType]);
			switch(cgEventData.evType)
			{
				case WATCHDOG_TIMER_EVENT:
					{
						setEventManagerThreadAliveStatus(true);
						break;
					}
				case INIT_DONE:
					{
						if(getState() == INIT_STATE || getState() == SELF_DIAG_STATE)
						{
							retCode = bleInit();
							if (retCode != GEN_SUCCESS) {
								CAB_DBG(CAB_GW_ERR, "BLE init failed restarting system");
								indicateSysStatus(ERROR);
								rDiagSendMessage(CABAPP_BLE_INIT_FAIL, GW_ERR, "BLE init failed restarting cabAppA7");
								setState(DEINIT_STATE);
								selfExit = 1;
								break;
							}
							setState(CONFIG_STATE);
							sendEvent(LTE_CHECK, NULL, 0);
						}
						break;
					}
				case CONFIG_SUCCESS:
					{
						if(getState() == CONFIG_STATE)
						{
        						/* Check initial battery condition */
						        retCode = dmHandler->batHandler->getData((void *)&tempBatDMData); 
							if (retCode != GEN_SUCCESS) {
								CAB_DBG(CAB_GW_ERR, "Battery DM get data failed witrh code %d", retCode);
								continue;
							}
						        if(tempBatDMData.powerDev == VEHICLE_POWER) {
								setState(MOVING_STATE);
						        } else {
						                if(tempBatDMData.batHealth == LOW_BAT) {
						                        CAB_DBG(CAB_GW_INFO, "Low battery detected. Cann't proceed");
									sendEvent(LOW_BATT, NULL, 0);
						                } else {
									setState(PARKED_STATE);
								}
							}
						}
						else if (getState() == MOVING_STATE || getState() == PARKED_STATE)
						{
							// Mandatory param changed. So, need
							// to flush all TPMS sensors throgh DM
							retCode = dmHandler->rfIntfHandler->flushDevices(NULL);
	                                                if ( retCode != GEN_SUCCESS ) {
                                                      	     CAB_DBG(CAB_GW_ERR, "Flush sensor(rfFlushDevice) fail with error %d", retCode);
                                                             /*if retCode!=DM_HASH_ERROR meaning connection with sub1 receiver may lost*/
                                                             if (retCode != DM_HASH_ERROR) {
                                                                CAB_DBG(CAB_GW_ERR, "Can't communicate with sub1 receiver. Deinitialise the gateway");
                                                                indicateSysStatus(ERROR);
								rDiagSendMessage(CABAPP_SUB1_INIT_FAIL, GW_ERR, "Restarting cabAppA7 due to SUB1 init failed");
                                                                setState(DEINIT_STATE);
								selfExit = 1;
								break;
                                                             }
                                                             continue;
                                                        }  
						}
						break;
					}
				case TPMS_ALERT:
					{
						if(getState() == MOVING_STATE)
						{
							CAB_DBG(CAB_GW_DEBUG, "## TPMS Alert ##");
                                                        reloadXmissionIntCount(XMISSION_INT_MOVING_TIM_COUNT);
							alertTimerLoad();			
						}
						break;
					}
				case OTA_CHECK:
					{
						memcpy(&OTAProgressState, (otaProgressState_e *)&cgEventData.data, sizeof(otaProgressState_e));
						CAB_DBG(CAB_GW_INFO, "# OTA Check event with state : %d #", OTAProgressState);
						if(getState() == CONFIG_STATE || (getState() == MOVING_STATE)) {
							startOTAThread(&OTAProgressState);
						}
						break;
					}
				case OTA_PROCESS:
					{
						time_t timeData=0;
						if((getState() == CONFIG_STATE) || (getState() == MOVING_STATE)) {
							retCode = dmHandler->rtcHandler->getData((void *)&timeData);
							if (retCode != GEN_SUCCESS) {
								CAB_DBG(CAB_GW_ERR, "DM get RTC failed with code %d", retCode);
							}
							CAB_DBG(CAB_GW_DEBUG, "# Zip data #");
							reportEventToSDC(GROUP_EV, (void *)&timeData, CSV_DATA);
							setState(DEINIT_STATE);	
						}
						break;
					}
				case LTE_CHECK:
					{
						time_t timeData;
						uint8_t setRTCFlag, isRtcAlreadySync;	
						if((getState() == CONFIG_STATE) || (getState() == MOVING_STATE) || (getState() == PARKED_STATE))
						{
							retCode = getConfigModuleParam(RTC_FLAG, (void *)&isRtcAlreadySync);
							if (retCode != GEN_SUCCESS) {
								CAB_DBG(CAB_GW_WARNING, "Get RTC flag failed with error %d", retCode);
								rDiagSendMessage(BLE_CONFIG_GET_FAIL, GW_WARNING, "BLE Get param failed");
								continue;
							}

							setRTCFlag = (1 << RTC_NW_SYNC_BIT);
							memset(&timeData, 0, sizeof(time_t));
							retCode = updateLTETime();
							if(retCode == GEN_SUCCESS)
							{
								if (lteTimerHandle != INVALID_TIMER_HANDLE) {
									deleteTimer(&lteTimerHandle);
									lteTimerHandle = INVALID_TIMER_HANDLE;
								}
		
								retCode = setConfigModuleParam(SYS_RTC_FLAG, &setRTCFlag);
								if (retCode != GEN_SUCCESS) {
									CAB_DBG(CAB_GW_WARNING, "set RTC flag failed with error %d", retCode);
									rDiagSendMessage(BLE_CONFIG_SET_FAIL, GW_WARNING, "BLE set diag failed for SYS_RTC_FLAG");
									continue;
								}
								/*If RTC sync first time, startDeviceConfiguration as device has been waiting*/
								if(!isRtcAlreadySync){
									startDeviceConfiguration();
								}
							} else
							{		
								gpsDiagInfo_t tempGPSDiag;
								retCode = dmHandler->gpsHandler->getDiagData(&tempGPSDiag);
								if(retCode == GEN_SUCCESS)
								{
								        if (lteTimerHandle != INVALID_TIMER_HANDLE) {
								        	deleteTimer(&lteTimerHandle);
										lteTimerHandle = INVALID_TIMER_HANDLE;
								        }
								        retCode = dmHandler->rtcHandler->setData((void *)&(tempGPSDiag.timestamp));
								        if (retCode != GEN_SUCCESS) {
								        	CAB_DBG(CAB_GW_ERR, "set RTC time failed with error %d", retCode);
								        	continue;
								        }
		
								        retCode = setConfigModuleParam(SYS_RTC_FLAG, &setRTCFlag);
								        if (retCode != GEN_SUCCESS) {
								        	CAB_DBG(CAB_GW_WARNING, "set RTC flag failed with error %d", retCode);
										rDiagSendMessage(BLE_CONFIG_SET_FAIL, GW_WARNING, "BLE set diag failed for SYS_RTC_FLAG");
								        	continue;
								        }
	                                                                /*If RTC sync first time, startDeviceConfiguration as device has been waiting*/
        	                                                        if(!isRtcAlreadySync){
                	                                                        startDeviceConfiguration();
                        	                                        }

								}
							}
						}
						break;
					}
				case BATT_CHECK:
					{
						uint8_t isLowBatt = 0;
						if((getState() == CONFIG_STATE) || (getState() == PARKED_STATE))
						{
        						/* Check initial battery condition */
							retCode = checkBatteryStat(&isLowBatt);
							if(retCode != GEN_SUCCESS)
							{
								CAB_DBG(CAB_GW_ERR, "checkBatteryStat() fail with error: %d", retCode);
							}
							if ( isLowBatt == 1 ) {
								sendEvent(LOW_BATT, NULL, 0);
							}
						}
						break;
					}
				case BATT_TO_VEH:
					{
						alert_e alertStat = NO;

						if(getState() == CONFIG_STATE)
						{
							reloadTimer(xMissionHandle, XMISSION_MOVING_TIMER_COUNT);
							reloadXmissionIntCount(XMISSION_INT_MOVING_TIM_COUNT);
						}	
						else if(getState() == PARKED_STATE)
						{
							reloadTimer(xMissionHandle, XMISSION_MOVING_TIMER_COUNT);
							reloadXmissionIntCount(XMISSION_INT_MOVING_TIM_COUNT);
							setState(MOVING_STATE);
						}
						logSysEvent(BATTERY, VEHICLE_POWER, alertStat);
						break;
					}
				case VEH_TO_BATT:
					{
						alert_e alertStat = NO;
						uint8_t isLowBatt = 0;

						retCode = checkBatteryStat(&isLowBatt);
						if(retCode != GEN_SUCCESS)
						{
							CAB_DBG(CAB_GW_ERR, "checkBatteryStat() fail with error: %d", retCode);
						}

						if(isLowBatt)
						{
							alertStat = YES;
						}

						if(getState() == CONFIG_STATE)
						{
							if(!isLowBatt)
							{
								reloadTimer(xMissionHandle, XMISSION_PARKED_TIMER_COUNT);
								reloadXmissionIntCount(XMISSION_INT_PARKED_TIM_COUNT);
							}
						}	
						else if(getState() == MOVING_STATE)
						{
							reloadTimer(zipPrepTimerHandle, ZIP_PREPARE_TIMER_COUNT);
							reloadTimer(xMissionHandle, XMISSION_PARKED_TIMER_COUNT);
							reloadXmissionIntCount(XMISSION_INT_PARKED_TIM_COUNT);
							/* Reset alert timer in parked state as alerts are not taken care off 
							   in parked state */
							alertTimerReset();
							setState(PARKED_STATE);
						}

						logSysEvent(BATTERY, BACKUP_BATTERY, alertStat);

						if(isLowBatt)
						{
							sendEvent(LOW_BATT, NULL, 0);
						}
						break;
					}
				case DIAG_CHECK:
					{
						float accTemp = 0;
						uint8_t setRTCFlag = (1 << RTC_NW_SYNC_BIT), isRtcAlreadySync;
						retCode = getConfigModuleParam(RTC_FLAG, (void *)&isRtcAlreadySync);
                                                if (retCode != GEN_SUCCESS) {
                                                        CAB_DBG(CAB_GW_WARNING, "Get RTC flag failed with error %d", retCode);
							rDiagSendMessage(BLE_CONFIG_GET_FAIL, GW_WARNING, "BLE Get param failed");
                                                        continue;
                                                }

						if((getState() == CONFIG_STATE) || (getState() == MOVING_STATE) || (getState() == PARKED_STATE))
						{
							/* From DM and set them individually 
							 to Diag module */
							retCode = dmHandler->rfIntfHandler->getDiagData((void *)&tempTPMSDiagData);
							if (retCode != GEN_SUCCESS) {
								CAB_DBG(CAB_GW_ERR, "RF get diag data failed with code %d", retCode);
							}
							CAB_DBG(CAB_GW_DEBUG, "No of paired sensors : %d", tempTPMSDiagData.pairedNumber);
							retCode = diagSetTpmsData(&tempTPMSDiagData);
							if (retCode != GEN_SUCCESS) {
								CAB_DBG(CAB_GW_ERR, "Diag set data failed with code %d", retCode);
							}

							memset(&tempSysOtherData, 0, sizeof(tempSysOtherData));
							retCode = getLTEDiagInfo(&(tempSysOtherData.diagData.lteDiagData));
							if(retCode == GEN_SUCCESS)
							{
								retCode = updateLTETime();
								if (retCode != GEN_SUCCESS) {
									CAB_DBG(CAB_GW_ERR, "Update LTE time data failed with error %d", retCode);

								} else {
									/*Allow time configuration from mobile app until gateway is being configured 
									with network time from LTE*/
									retCode = setConfigModuleParam(SYS_RTC_FLAG, &setRTCFlag);
									if (retCode != GEN_SUCCESS) {
										CAB_DBG(CAB_GW_WARNING, "set RTC flag failed with error %d", retCode);
		      								rDiagSendMessage(BLE_CONFIG_SET_FAIL, GW_WARNING, "BLE set diag failed for SS");
									} else {
										/*If RTC sync first time, startDeviceConfiguration as device has been waiting*/
        	                                        			if(!isRtcAlreadySync){
	                                                			        startDeviceConfiguration();
                                                				}
									}
								}

								retCode = getConfigModuleParam( DIAG_TIME_CMD, \
								(void *)(&(tempSysOtherData.diagData.lteDiagData.timestamp)) ); 
								if (retCode != GEN_SUCCESS) {
									CAB_DBG(CAB_GW_WARNING, "Get diag time command failed with code %d", retCode);
									rDiagSendMessage(BLE_CONFIG_GET_FAIL, GW_WARNING, "BLE Get param failed");
								}

								retCode = diagSetLteData(&(tempSysOtherData.diagData.lteDiagData));
								if (retCode != GEN_SUCCESS) {
									CAB_DBG(CAB_GW_ERR, "RF get diag data failed with code %d", retCode);
								}
							}

							memset(tempSimIccid, 0, sizeof(tempSimIccid));

							count = 0;
							do {
								retCode = checkSIMDetection(&present);
								if (retCode != GEN_SUCCESS) {
									CAB_DBG(CAB_GW_ERR, "Check SIM detection failed with code %d", retCode);
									setSimDetFailCnt(getSimDetFailCnt()+1);
								} else {
									if (present > 1) {
										setSimDetFailCnt(0);
										retCode = getLteICCIDNum(tempSimIccid);
										if (retCode != GEN_SUCCESS) {
											memcpy(tempSimIccid, iccidNA, sizeof(iccidNA));
											CAB_DBG(CAB_GW_WARNING, "Failed to get ICCID number with code %d", retCode);
											rDiagSendMessage(CABAPP_SIM_ICCID_NOT_AVAIL, GW_WARNING, "ICCID API failed with code");
										} else {
											retCode = setConfigModuleParam(SYS_SET_SIM_ICCID_CMD, tempSimIccid);
											if (retCode != GEN_SUCCESS) {
												CAB_DBG(CAB_GW_WARNING, "Failed to set ICCID number with code %d", retCode);
												rDiagSendMessage(CABAPP_GEN_WARN,  GW_WARNING, "Set config error");
											}
										}
										break;
									} else {
										memcpy(tempSimIccid, simNotPStr, sizeof(simNotPStr));
										setSimDetFailCnt(getSimDetFailCnt()+1);
									}

									retCode = diagSetGatewayInfo(SYS_SIM_ICCID, (void *)tempSimIccid);
									if (retCode != GEN_SUCCESS) {
										CAB_DBG(CAB_GW_WARNING, "Diag SIM ICCID set info failed with code %d", retCode);
										rDiagSendMessage(CABAPP_SIM_ICCID_NOT_AVAIL,  GW_WARNING, "Diag set API failed for ICCID");
									}
								}
							} while(count++ <= 3);

							retCode = dmHandler->gpsHandler->getDiagData((void *)&(tempSysOtherData.diagData.gpsDiagData));
							if(retCode == GEN_SUCCESS)
							{
								retCode = dmHandler->rtcHandler->getData((void *)&(tempSysOtherData.diagData.gpsDiagData.timestamp));
								if (retCode != GEN_SUCCESS) {
									CAB_DBG(CAB_GW_WARNING, "get time data failed with error %d", retCode);
									rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_WARNING, "Get RTC data failed with code");
								} else {
									retCode = diagSetGpsData(&(tempSysOtherData.diagData.gpsDiagData));
									if (retCode != GEN_SUCCESS) {
													CAB_DBG(CAB_GW_WARNING, "Diag set GPS data failed with code %d", retCode);
													rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_WARNING, "Set Diag data failed");
									}
								}
							}
							double zero = 0;
							if((tempSysOtherData.diagData.gpsDiagData.gpsData.latitude == zero) && (tempSysOtherData.diagData.gpsDiagData.gpsData.longitude == zero)
									&& (tempSysOtherData.diagData.gpsDiagData.gpsData.altitude == zero)) {
								if(StartTimer == 0)
								{
									StartTimer = 1;
									start_time = time(NULL);
								} else {
									curr_time = time(NULL);
									if(curr_time > (start_time + TWENTY_FOUR_HOUR) ) {
										uint8_t isValid = 0;
										retCode = CheckXdataisValid(&isValid);
										if(retCode != GEN_SUCCESS)
										{
											CAB_DBG(CAB_GW_ERR, "CheckXdataisValid() fail with error: %d", retCode);
											return retCode;
										}
										if(!isValid) {
											rDiagSendMessage(GPS_XDTA_EXPIRED, GW_WARNING, "Rebooting as the GPS XDATA is Expired");
                                                                                        rebootSystem();
										}
										CAB_DBG(CAB_GW_INFO, "Reseting GPS");
										resetGPS();
										StartTimer = 0;
									}
								}

							} else {
								StartTimer = 0;
							}

							uint8_t lteConnStat=0;
							tempSysOtherData.diagData.rtcPwrAccDiagData.coinCellStatusBool =  initRTCCoinCellStatus;
							tempSysOtherData.diagData.rtcPwrAccDiagData.powerSWTime = powerStartTime;
							retCode = getLTEConnStat(&lteConnStat);
							if (retCode != GEN_SUCCESS) {
								CAB_DBG(CAB_GW_WARNING, "Get LTE conn status failed with code %d", retCode);
								rDiagSendMessage(CABAPP_GEN_WARN, GW_WARNING, "Get LTE conn status API failed");
							}
							tempSysOtherData.diagData.rtcPwrAccDiagData.isInternetConnected = (lteConnStat == 1) ? true : false;


							/* Get accelerometer diag data */
							retCode = accelReadTemp(&accTemp);
							if (retCode != GEN_SUCCESS){
								CAB_DBG(CAB_GW_WARNING, "Read Accel temp failed with code %d", retCode);
								rDiagSendMessage(CABAPP_GEN_WARN, GW_WARNING, "Accel read temp failed");
							}
							tempSysOtherData.diagData.rtcPwrAccDiagData.accelTemperature = (int16_t)accTemp;

							/* Get Current MPU temperature */

							FILE *fp;
							int mpu_curr_temp = 0;
							fp = fopen(MPU_TEMPERATURE, "r");
							if(fp == NULL) {
								CAB_DBG(CAB_GW_WARNING,"[Temp-ShutOFF] Unable to open Temp file MPU %d", retCode);
								rDiagSendMessage(CABAPP_GEN_WARN, GW_WARNING, "MPU read failed");
								//return fp;
							}
							fscanf(fp, "%d", &mpu_curr_temp);
							fclose(fp);
							CAB_DBG(CAB_GW_DEBUG, "MPU temperature : %d", mpu_curr_temp);
							tempSysOtherData.diagData.rtcPwrAccDiagData.mpuTemperature = mpu_curr_temp;

                                                        /* Get RTC poweroff Flag and timestamp */
                                                        uint8_t pwrOffFlg = 0;
                                                        retCode = getRtcBsfData(&pwrOffFlg, &tempSysOtherData.diagData.rtcPwrAccDiagData.PwrOffTmStamp);
							if (retCode != GEN_SUCCESS) {
								CAB_DBG(CAB_GW_WARNING, "get RTC bsf Data failed %d", retCode);
								rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_WARNING, "Get data RTC data failed");
                                                        }
                                                        
                                                        if(pwrOffFlg == HARD_PWR_OFF) {
							    CAB_DBG(CAB_GW_INFO, "Storing Hard Power Off in SysOtherData.csv\n");
                                                            tempSysOtherData.diagData.rtcPwrAccDiagData.PwrOffFlg = HARD_PWR_OFF;
#ifdef SOFT_POWER_OFF_EN
                                                            remove(SOFT_PWR_OFF_FILE);
#endif
                                                        }
#ifdef SOFT_POWER_OFF_EN 
                                                        else if(access(SOFT_PWR_OFF_FILE, F_OK) == 0){
								tempSysOtherData.diagData.rtcPwrAccDiagData.PwrOffFlg = SOFT_PWR_OFF;
								FILE *fp;
								fp = fopen(SOFT_PWR_OFF_FILE, "r");
								if(fp == NULL) {
									CAB_DBG(CAB_GW_WARNING,"Unable To Open soft power off file");
								}
								fscanf(fp, "%d", &tempSysOtherData.diagData.rtcPwrAccDiagData.PwrOffTmStamp);
								fclose(fp);
                                                                remove(SOFT_PWR_OFF_FILE);
							}
#endif
                                                        CAB_DBG(CAB_GW_INFO, "Power Off Flag : %d timestamp : %ld", 
                                                            tempSysOtherData.diagData.rtcPwrAccDiagData.PwrOffFlg, tempSysOtherData.diagData.rtcPwrAccDiagData.PwrOffTmStamp);

							/* Store diag data to cloud */
							retCode = dmHandler->rtcHandler->getData(&(tempSysOtherData.time));
							if (retCode != GEN_SUCCESS) {
								CAB_DBG(CAB_GW_WARNING, "RF get diag data failed with code %d", retCode);
								rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_WARNING, "Get data RTC data failed");
							}
                                                        
							tempSysOtherData.type = DIAGNOSTIC_DATA;
							retCode = storeDataToSDC((void *)&(tempSysOtherData), SYS_OTHER_DATA);
							if (retCode != GEN_SUCCESS) {
								CAB_DBG(CAB_GW_WARNING, "RF store data failed with code %d", retCode);
								rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_WARNING, "RF store data failed");
							}

						}
						break;
					}
				case LOW_BATT:
					{
						// Log power switching event
						logSysEvent(BATTERY, BACKUP_BATTERY, YES);

						if(getState() == PARKED_STATE)
						{
							time_t timeData;
							retCode = dmHandler->rtcHandler->getData((void *)&timeData);
							if (retCode != GEN_SUCCESS) {
								CAB_DBG(CAB_GW_ERR, "RTC DM get data failed with code %d", retCode);
								continue;
							}
				
							otaNotifier();
							reportEventToSDC(TRANSMIT_EV, (void *)&timeData, CSV_DATA);
						}

						if((getState() == CONFIG_STATE) || (getState() == PARKED_STATE))
						{
							CAB_DBG(CAB_GW_ERR, "Shutting off due to low battery");
							rDiagSendMessage(LOW_BATT_SHUTOFF, GW_ERR, "Shutting off due to low battery");
							setState(DEINIT_STATE);
							selfExit = 1;
							break;
						}
						break;
					}
				case XMIT_CHECK:
					{
						time_t timeData;
						xMitEventDataEnum_e xMitOp = 0;
						retCode = dmHandler->rtcHandler->getData((void *)&timeData);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "DM get RTC failed with code %d", retCode);
							continue;
						}
						memcpy(&xMitOp, (xMitEventDataEnum_e *)&cgEventData.data, sizeof(xMitEventDataEnum_e));

						if((getState() == CONFIG_STATE) || (getState() == MOVING_STATE) || (getState() == SELF_DIAG_STATE && isSelfDiagCompleted == true))
						{
							if (xMitOp == XMIT_DATA) {
								CAB_DBG(CAB_GW_DEBUG, "## TX Norm St ####");
								otaNotifier();
								reportEventToSDC(TRANSMIT_EV, (void *)&timeData, CSV_DATA);
								sendEvent(LTE_CHECK, NULL, 0);	
							}
						}
						else if(getState() == PARKED_STATE)
						{
							if(xMitOp == ZIP_DATA)
							{
								// Just to prepare zip only
								CAB_DBG(CAB_GW_DEBUG, "# Zip data #");
								reportEventToSDC(GROUP_EV, (void *)&timeData, CSV_DATA);
							}
							else // This will come at every 48 hours
							{
								CAB_DBG(CAB_GW_INFO, "#### TX in Park ST ####");
								otaNotifier();
								// Send TPMS and Diag data to cloud
								reportEventToSDC(TRANSMIT_EV, (void *)&timeData, CSV_DATA);
								sendEvent(LTE_CHECK, NULL, 0);
							}
						}
						break;
					}
				case LEAK_DET_CHECK:
					{
						uint32_t temp=0;
						checkSelfProcMem(&temp);
						if (temp > MAX_MEM_LEAK) {
							CAB_DBG(CAB_GW_INFO, "Memory leak exceeded rebooting system");
							setState(DEINIT_STATE);
							//rebootSystem();
						}
						lteDiagInfo_t tempLTEDiagInfo;
						if (getState() == MOVING_STATE || getState() == PARKED_STATE) {
							tireLeakageDetection();
						}
						if (getState() == MOVING_STATE || getState() == PARKED_STATE || getState() == CONFIG_STATE) {
							/*Check keep alive for sub1 receiver*/
							if(true == readyToCheckSub1KeepAlive()){
								CAB_DBG(CAB_GW_DEBUG, "Sub1 Keep Alive Check...");
								memset((void*)(&aliveCheckConfig),0,sizeof(tpmsConfig_t));
								aliveCheckConfig.sensorID = 0x0000;
								retCode = dmHandler->rfIntfHandler->removeDevice((void *)&(aliveCheckConfig));
								if(retCode != GEN_SUCCESS)
								{
									/*if retCode!=DM_HASH_ERROR meaning connection with sub1 receiver may lost*/
									if (retCode != DM_HASH_ERROR) {
										CAB_DBG(CAB_GW_ERR, "Sub1 keep alive check failed. Deinitialise the gateway");
										indicateSysStatus(ERROR);
										rDiagSendMessage(SUB1_KEEP_ALIVE_FAILURE, GW_ERR, "Sub1 keep alive failed");
										setState(DEINIT_STATE);
										selfExit = 1;
										break;
									}
								}
								else{
									CAB_DBG(CAB_GW_INFO, "Connection with Sub1 is OK !");
								}	
								resetSub1KeepAliveCntr();
							}
							/*Check keep alive for ble module*/
							if(true == readyToCheckBleKeepAlive()) {
								CAB_DBG(CAB_GW_DEBUG, "BLE Keep Alive Check...");
								if( (false == getBleAliveStatus()) && (false == getBLEKeepAliveStatus())){
									CAB_DBG(CAB_GW_ERR, "###### BLE keep alive check for %d times ####", getBleAliveFailCount() + 1);
									setBleAliveFailCount(getBleAliveFailCount()+1);
									if (getBleAliveFailCount() >= BLE_KEEPALIVE_FAIL_COUNTER) {
										CAB_DBG(CAB_GW_ERR, "BLE keep alive check failed. Deinitialise the gateway");
                                                                        	indicateSysStatus(ERROR);
										rDiagSendMessage(BLE_KEEP_ALIVE_FAILURE, GW_ERR, "BLE keep alive failed restarting app");
	                                                                        setState(DEINIT_STATE);
        	                                                                selfExit = 1;
                                                                        	break;
									}
								}
								else{
									CAB_DBG(CAB_GW_INFO, "Conection with BLE module is OK !");
									/*send BLE MAC addr query again.*/
									setBleAliveStatus(false);
									setBLEKeepAliveStatus(false);
									setBleAliveFailCount(0);
								}

                                /* Only request for BLE advertisement star confirmation if not received on initialization time. */
                                if (!bleAdvertisementStarted)
                                {
                                    getBleMacAdvStarted();
                                }

								getBleMacAdd();
								resetBleKeepAliveCntr();
								setBLEKeepAliveStatus(false);
							}

							/* Check and reset EG91 module if required */
							if (getEG91ResetReqdState() == 1) {
								CAB_DBG(CAB_GW_ERR, "LTE keep alive failed, restarting cab gateway application");
							    rDiagSendMessage(LTE_GPS_KEEP_ALIVE_FAILURE, GW_ERR, "LTE keep alive failed, restarting cab gateway application");
                                                            indicateSysStatus(ERROR);
	                                                    setState(DEINIT_STATE);
        	                                            selfExit = 1;
                                                            break;
							}

							retCode = checkSIMDetection(&present);
							if (retCode != GEN_SUCCESS) {
								CAB_DBG(CAB_GW_ERR, "Check SIM detection failed with code %d", retCode);
								setSimDetFailCnt(getSimDetFailCnt()+1);
							} else {
								if (present > 1) {
									setSimDetFailCnt(0);
									retCode = getLteICCIDNum(tempSimIccid);
									if (retCode != GEN_SUCCESS) {
										memcpy(tempSimIccid, iccidNA, sizeof(iccidNA));
										CAB_DBG(CAB_GW_WARNING, "Failed to get ICCID number with code %d", retCode);
										rDiagSendMessage(CABAPP_SIM_ICCID_NOT_AVAIL, GW_WARNING, "ICCID API failed with code");
									} else {
										retCode = setConfigModuleParam(SYS_SET_SIM_ICCID_CMD, tempSimIccid);
										if (retCode != GEN_SUCCESS) {
											CAB_DBG(CAB_GW_WARNING, "Failed to set ICCID number with code %d", retCode);
											rDiagSendMessage(CABAPP_GEN_WARN,  GW_WARNING, "Set config error");
										}
									}
								} else {
									memcpy(tempSimIccid, simNotPStr, sizeof(simNotPStr));
									setSimDetFailCnt(getSimDetFailCnt()+1);
								}

								retCode = diagSetGatewayInfo(SYS_SIM_ICCID, (void *)tempSimIccid);
                                                		if (retCode != GEN_SUCCESS) {
                                                		        CAB_DBG(CAB_GW_WARNING, "Dig SIM ICCID set info failed with code %d", retCode);
									rDiagSendMessage(CABAPP_SIM_ICCID_NOT_AVAIL,  GW_WARNING, "Diag set API failed for ICCID");
                                                		}
							}

							memset(&tempLTEDiagInfo, 0, sizeof(lteDiagInfo_t));
							retCode = getLTEDiagInfo(&(tempLTEDiagInfo));
							if(retCode == GEN_SUCCESS) 
							{
								rDiagSendLTEDebug(&tempLTEDiagInfo);
							}
						}

						break;
					}
				case SIM_DET_CHECK:
					{
						if (getSimDetFailCnt() > 0) {
							char localmsg[1024]="";
							CAB_DBG(CAB_GW_WARNING, "SIM card detection failed for %d times", getSimDetFailCnt());
							snprintf(localmsg, sizeof(localmsg),  "SIM card detection failed for %d times", getSimDetFailCnt());
							rDiagSendMessage(CABAPP_SIM_NOT_PRESENT, GW_WARNING, localmsg);

							CAB_DBG(CAB_GW_ERR, "Reboot the system due to SIM card not present");
							rebootSystem();
						}
					
					} break;
				case SELF_DIAG_EVENT:
					{
						/*if (getState() == CONFIG_STATE || getState() == MOVING_STATE || getState() == PARKED_STATE) */
						if (getState() == SELF_DIAG_STATE) {
							selfDiagStatus_t tempSelfDiagStatus;
							lteDiagInfo_t tempLTEDiag;
							gpsDiagInfo_t tempGPSDiag;
							uint8_t present = 0, lteStat = 0, btnGPIO = 1, sensorInd = 0;
							uint8_t retryCount = 0, rtcTestCount = 0, passTestCount = 0;
							time_t tempRTCTime, refRTCTime;
							int fd;
							uint8_t writeReg[3] = {'1', '5', '\n'} ;
							uint8_t readReg[4];
							tpmsConfig_t testSensors[] = { \
							         {0x491F90, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 0x01, {ENABLE, TYRE, INNER, LEFT, 1}},        \
							         {0x49170F, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 0x01, {ENABLE, TYRE, INNER, RIGHT, 1}},       \
							         {0x61C9F2, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 0x01, {ENABLE, TYRE, INNER, LEFT, 2}},        \
							         {0x591957, DEFAULT_SENSOR_PART_NO, DEFAULT_SENSOR_MODEL, 0x01, {ENABLE, TYRE, OUTER, LEFT, 2}},        \
							};
							sensorVendorType_e sensorMake;

							isSelfDiagCompleted = false;

							tempSelfDiagStatus.isTestCompleted = false;
							retCode = setConfigModuleParam(SELF_DIAG_GW_RESP_CMD, (void *)&tempSelfDiagStatus);
							if (GEN_SUCCESS != retCode) {
								CAB_DBG(CAB_GW_WARNING, "set config failed with code %d", retCode);

		      						rDiagSendMessage(BLE_CONFIG_SET_FAIL, GW_WARNING, "BLE set diag failed for SELF_DIAG_GW_RESP_CMD");
							}
							
							getVendorTypeTemp(&sensorMake);
							
							/*Starting self diag mode*/
							memset(&tempSelfDiagStatus, 0, sizeof(tempSelfDiagStatus));
							memset(&tempLTEDiag, 0, sizeof(tempLTEDiag));
							gpioInit(PUSH_BTN_GPIO, IN);

                                                        retCode = dmHandler->rfIntfHandler->flushDevices(NULL);
							if ( retCode != GEN_SUCCESS ) {
                                                	        CAB_DBG(CAB_GW_ERR, "Flush sensor(rfFlushDevice) fail with error %d", retCode);
                                                	        /*if retCode!=DM_HASH_ERROR meaning connection with sub1 receiver may lost*/
                                                	        if (retCode != DM_HASH_ERROR) {
                                                	               CAB_DBG(CAB_GW_ERR, "Can't communicate with sub1 receiver. Deinitialise the gateway");
                                                	        }
								tempSelfDiagStatus.rf_test_status = false;
							}
							for (sensorInd = 0; sensorInd < MAX_SELF_DIAG_TPMS_ID; sensorInd++) {
								testSensors[sensorInd].sensorID = g_SelfDiagReq.sensorID[sensorInd];
								retCode = dmHandler->rfIntfHandler->addDevice((void *)&testSensors[sensorInd]);
								if(retCode != GEN_SUCCESS)
								{
									CAB_DBG(CAB_GW_ERR, "Add sensor(rFAddDevice) fail with error %d", retCode);
									/*if retCode!=DM_HASH_ERROR meaning connection with sub1 receiver may lost*/
									if (retCode != DM_HASH_ERROR) {
										CAB_DBG(CAB_GW_ERR, "Can't communicate with sub1 receiver. Deinitialise the gateway");
                                                		        }
								        tempSelfDiagStatus.rf_test_status = false;
								}
							}

							/* Keep BLE always PASS */
							tempSelfDiagStatus.ble_test_status = true;
							passTestCount++;
							/* RTC init time for comparison*/
							dmHandler->rtcHandler->getDiagData((void *)&refRTCTime);
							indicateSysStatus(SELF_DIAG_PATTERN);
							
							do {
								if (getBLEConnectState() == BLE_DISCONNECTED_MNT_FLAG) {
									tempSelfDiagStatus.isTestCompleted = false;
									CAB_DBG(CAB_GW_INFO, "Exiting from Self Diag test due to BLE disconnected");
									goto compulsary_execution;
								}

							        if (tempSelfDiagStatus.lte_sim_test_status != true) {
								    /* SIM present test */
								    retCode = checkSIMDetection(&present);
        	                                        	    if (retCode != GEN_SUCCESS) {
                	                                	        CAB_DBG(CAB_GW_ERR, "Check SIM detection failed with code %d", retCode);
								    	tempSelfDiagStatus.lte_sim_test_status = false;
	                                                	    } else {
									CAB_DBG(CAB_GW_INFO, "LTE SIM PASS");
									if (present > 1) {
								    	    tempSelfDiagStatus.lte_sim_test_status = true;
									    passTestCount++;
									}
								    }
								}

								if (tempSelfDiagStatus.lte_rssi_test_status != true) {
								    /* SIM RSSI test */
								    retCode = getLTEDiagInfo(&tempLTEDiag);
        	                                        	    if (retCode != GEN_SUCCESS) {
                	                                	        CAB_DBG(CAB_GW_ERR, "Get LTE diag failed with code %d", retCode);
								    	tempSelfDiagStatus.lte_rssi_test_status = false;
	                                                	    } else {
								    	if ( (tempLTEDiag.signalStrength >= 2 && tempLTEDiag.signalStrength <= 31) ||
								    	     (tempLTEDiag.signalStrength >= 102 && tempLTEDiag.signalStrength <= 191) ) {
									    CAB_DBG(CAB_GW_INFO, "LTE RSSI PASS");
								  	    tempSelfDiagStatus.lte_rssi_test_status = true;
									    passTestCount++;
								    	}
								    }
								}

								if (tempSelfDiagStatus.lte_cloud_test_status != true) {
								    /* SIM cloud test */
								    retCode = getLTEConnStat(&lteStat);
								    if (retCode != GEN_SUCCESS) {
									CAB_DBG(CAB_GW_INFO, "LTE Conn check failed");
								    } else {
									if (lteStat == 1) {
								    		retCode = getAPiToken();
								    		if (retCode != GEN_SUCCESS) {
								    		    CAB_DBG(CAB_GW_INFO, "Cloud connectivity failed, check Cloud URL and GW reg. status");
								    		}
								    		if (retCode == GEN_SUCCESS) {
								    		    CAB_DBG(CAB_GW_INFO, "LTE CLOUD PASS");
								    		    tempSelfDiagStatus.lte_cloud_test_status = true;
									    	    passTestCount++;
								    		}
									} else {
								    	    CAB_DBG(CAB_GW_INFO, "No LTE conection");
								    	    disconnectLTE();
								    	    retCode = connectLTE();
									    if (retCode != GEN_SUCCESS) {
										    CAB_DBG(CAB_GW_WARNING, "connect LTE failed with code %d",retCode);
										    rDiagSendMessage(LTE_CONNECT_FAIL, GW_WARNING, "LTE connect failed");
									    }
									}
								    }
								}

								if (tempSelfDiagStatus.gps_test_status != true) {
								    /*GPS test*/
								    retCode = dmHandler->gpsHandler->getDiagData((void *)&(tempGPSDiag));
		                                                    if(retCode != GEN_SUCCESS) {
									CAB_DBG(CAB_GW_INFO, "Get GPS diag failed");
		                                                    } else {
									if ((tempGPSDiag.noOfSatInView > 0) && (tempGPSDiag.fixQuality == 1)) {
									    CAB_DBG(CAB_GW_INFO, "GPS PASS no of Sat:%d,fixQuality:%d", tempGPSDiag.noOfSatInView, tempGPSDiag.fixQuality);
									    tempSelfDiagStatus.gps_test_status = true;
									    passTestCount++;
									}
								    }
								}

								if (tempSelfDiagStatus.accelerometer_test_status != true) {
									fd = open("/sys/kernel/debug/iio/iio\:device0/direct_reg_access", O_WRONLY);
									if (fd != -1) {
									    write(fd, (void *)&writeReg, 3);
									}
									close(fd);
									fd = open("/sys/kernel/debug/iio/iio\:device0/direct_reg_access", O_RDONLY);
									if (fd != -1) {
									    read(fd, (void *)&readReg, 4);
									}
									close(fd);
									CAB_DBG( CAB_GW_INFO, "Accreg 0x%x:0x%x,0x%x, 0x%x",readReg[0], readReg[1], readReg[2], readReg[3]);
									if(readReg[2] == '4' || readReg[3] == '1') {
									    CAB_DBG(CAB_GW_INFO, "ACC PASS");
									    tempSelfDiagStatus.accelerometer_test_status = true;
									    passTestCount++;
						                        }

								}

								if (tempSelfDiagStatus.rtc_test_status != true) {
									rtcTestCount++;
									dmHandler->rtcHandler->getDiagData(&tempRTCTime);
									if (rtcTestCount >= 3) {
										if (tempRTCTime >= (refRTCTime+3)) {
									                CAB_DBG(CAB_GW_INFO, "RTC PASS");
											tempSelfDiagStatus.rtc_test_status = true;
									    		passTestCount++;
										}
									}
								}
	
								if (tempSelfDiagStatus.led_btn_test_status != true) {
									retCode = gpioRead(PUSH_BTN_GPIO, &btnGPIO);
									if(retCode != GEN_SUCCESS)
									{
									        CAB_DBG(CAB_GW_ERR, "gpioInit() fail with error %d", retCode);
									} else {
										if (btnGPIO == 0) {
									                CAB_DBG(CAB_GW_INFO, "LED BTN PASS");
											tempSelfDiagStatus.led_btn_test_status = true;
									    		passTestCount++;
											indicateSysStatus(ALERT);
										}
									}
								}

								if (tempSelfDiagStatus.rf_test_status != true) {
									if (getSub1DataRcvdState() == true) {
										if (sensorMake == VENDOR_AVE_TECH) {
											if (getMaxRSSI() > CC1310_RSSI_THRESHOLD) {
									                	CAB_DBG(CAB_GW_INFO, "RF PASS");
												tempSelfDiagStatus.rf_test_status = true;
												passTestCount++;
											}
										} else if (sensorMake == VENDOR_PRESSURE_PRO) {
											if (getMaxRSSI() > XBR_RSSI_THRESHOLD) {
									                	CAB_DBG(CAB_GW_INFO, "RF PASS");
												tempSelfDiagStatus.rf_test_status = true;
												passTestCount++;
											}
										}
									}
								}

								if (passTestCount >= SELF_DIAG_TEST_COUNT) {
									CAB_DBG(CAB_GW_INFO, "All tests passed");
									break;
								}
								sleep(1);
								CAB_DBG(CAB_GW_INFO,"-#-");
							} while ((retryCount++ < 10));
							tempSelfDiagStatus.isTestCompleted = true;

compulsary_execution:
							indicateSysStatus(NORMAL);
							gpioDeInit(PUSH_BTN_GPIO);	
							setSub1DataRcvdState(false);
							for (sensorInd = 0; sensorInd < MAX_SELF_DIAG_TPMS_ID; sensorInd++) {
								testSensors[sensorInd].sensorID = g_SelfDiagReq.sensorID[sensorInd];
								retCode = dmHandler->rfIntfHandler->removeDevice((void *)&testSensors[sensorInd]);
								if(retCode != GEN_SUCCESS)
								{
									CAB_DBG(CAB_GW_ERR, "remove sensor(rFAddDevice) fail with error %d", retCode);
								}
							}
							setMaxRSSI(0);
							retCode = setConfigModuleParam(SELF_DIAG_GW_RESP_CMD, (void *)&tempSelfDiagStatus);
							if (GEN_SUCCESS != retCode) {
								CAB_DBG(CAB_GW_WARNING, "Set config failed for SELF_DIAG_GW_RESP_CMD with code %d", retCode);
		      						rDiagSendMessage(BLE_CONFIG_SET_FAIL, GW_WARNING, "BLE set diag failed for SELF_DIAG_GW_RESP_CMD");
							}
							CAB_DBG(CAB_GW_INFO,"-*-");
							isSelfDiagCompleted = true;
							startDeviceConfiguration();
							indicateSysStatus(NORMAL);
						}
						break;
					}
				default:
					{
						break;
					}
			}
		} else {
			CAB_DBG(CAB_GW_WARNING, "mq_rec for %s failed with error %s", EVENT_QUEUE_NAME, strerror(errno));
            sprintf(errorMsg, "mq_rec for %s failed with error %s", EVENT_QUEUE_NAME, strerror(errno));
            rDiagSendMessage(MESSAGE_QUEUE_OVERFLOW, GW_WARNING, errorMsg);
        }


		if(selfExit)
		{
			CAB_DBG(CAB_GW_INFO, "Event manager thread deinit");
			break;
		}
	}

	retCode = threadDeregister(newWatchdogNode.threadId);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "Event manager thread de-registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "Event manager thread de-registration failed");
        }
	FUNC_EXIT
	return NULL;
}

static bool getrecvRFDataFromDMHandlerThreadAliveStatus(void)
{
        bool status;
        pthread_mutex_lock(&recvRFDataFromDMHandlerThreadAliveLock);
        status = g_recvRFDataFromDMHandlerThreadAliveStatus;
        pthread_mutex_unlock(&recvRFDataFromDMHandlerThreadAliveLock);
        return status;
}

static void setrecvRFDataFromDMHandlerThreadAliveStatus(bool value)
{
        pthread_mutex_lock(&recvRFDataFromDMHandlerThreadAliveLock);
        g_recvRFDataFromDMHandlerThreadAliveStatus = value;
        pthread_mutex_unlock(&recvRFDataFromDMHandlerThreadAliveLock);
}

bool recvRFDataCallback(void)
{
        bool status = false;
	tpmsDMData_t localDMDummyData;
        CAB_DBG(CAB_GW_TRACE, "In func : %s", __func__);
        if(getrecvRFDataFromDMHandlerThreadAliveStatus() == true)
        {
                status = true;
        }

        setrecvRFDataFromDMHandlerThreadAliveStatus(false);

	memset(&localDMDummyData, 0 , sizeof(localDMDummyData));
	DmRfDataCB(&localDMDummyData);
        return status;
}

static void *recvRFDataFromDMHandler(void *arg)
{
	FUNC_ENTRY
	static uint8_t isFirstTimeInParked = 1;
	uint8_t isAlert = 0;
	ssize_t bytes_read;
	returnCode_e retCode = GEN_SUCCESS;
	tpmsEvent_t tpmsEventData;
	gpsData_t gpsRefData;
	wheelInfo_t wheelRefInfo;
	tpmsData_t tpmsRefData;
	time_t refTime = 0;
	watchDogTimer_t newWatchdogNode;
	char errorMsg[100] = "";

	newWatchdogNode.threadId = syscall(SYS_gettid);
	newWatchdogNode.CallbackFn = recvRFDataCallback;

	CAB_DBG(CAB_GW_INFO, "Recv RF data from DM handler thread id : %ld", syscall(SYS_gettid));
	retCode = threadRegister(&newWatchdogNode);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "Recv RF data From DM handler thread registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_REGISTRATION_FAIL, GW_ERR, "Recv RF data From DM handler thread registration failed");
        }

	memset(&gpsRefData, 0, sizeof(gpsRefData));
	memset(&wheelRefInfo, 0, sizeof(wheelRefInfo));
	memset(&tpmsRefData, 0, sizeof(tpmsRefData));
	while(1)
	{
		memset(&tpmsEventData, 0, sizeof(tpmsEventData));
		bytes_read = mq_receive(mqTPMSQueue, (char *)&(tpmsEventData.tpmsDMData), sizeof(tpmsDMData_t), NULL);
		if(bytes_read >= 0)
		{
			if((memcmp(&tpmsEventData.tpmsDMData.gpsData,&gpsRefData,sizeof(gpsData_t)) == 0) &&
			(memcmp(&(tpmsEventData.tpmsDMData.wheelInfo),&wheelRefInfo,sizeof(wheelInfo_t)) == 0) 
			&& (memcmp(&(tpmsEventData.tpmsDMData.tpmsData),&tpmsRefData,sizeof(tpmsData_t)) == 0)
			&& (memcmp(&(tpmsEventData.tpmsDMData.time),&refTime,sizeof(time_t)) == 0))
			{
				setrecvRFDataFromDMHandlerThreadAliveStatus(true);
				continue;
			}

			if((getState() == INIT_STATE) || (getState() == CONFIG_STATE))
			{
				continue;
			}

			if(getState() == DEINIT_STATE)
			{
				CAB_DBG(CAB_GW_INFO, "RF data receiver thread deinit");
				break;
			}
			else if(getState() == OTA_STATE)
			{
				continue;
			}

			if (getState() != SELF_DIAG_STATE) {
		    		// Display TPMS data
				CAB_DBG(CAB_GW_DEBUG,"\r\n\t===========================================================");
				CAB_DBG(CAB_GW_DEBUG,"\r\n\t                     M&T Receiver Data");
				printfLocalDMData(&tpmsEventData);

				/*TPMS data received so connection with sub1 receiver is healthy. Skip keep
				alive check with sub1*/
				resetSub1KeepAliveCntr();

				isAlert = 0;
				// Process received TPMS data
				if (tpmsEventData.tpmsDMData.wheelInfo.attachedType != HALO) {
					processTpmsData(&tpmsEventData, &isAlert);

					if(!isFirstTimeInParked && (getState() != PARKED_STATE))
						isFirstTimeInParked = 1;

					if(isAlert && isFirstTimeInParked)
					{
						// Send Alert message to SDC module
						CAB_DBG(CAB_GW_DEBUG, "## TPMS Cloud Alert ##");
						otaNotifier();
						reportEventToSDC(ALERT_EV, (void *)&tpmsEventData, CSV_DATA);
						sendEvent(TPMS_ALERT, NULL, 0);
						isAlert = 0;
						if(getState() == PARKED_STATE)
							isFirstTimeInParked = 0;
					}
					else
					{
						// Send Normal data to SDC module
						storeDataToSDC((void *)&tpmsEventData, TPMS);
					}
				} else {
					CAB_DBG(CAB_GW_DEBUG, "Storing HALO data");
					storeDataToSDC((void *)&tpmsEventData, TPMS);
				}
			} else {
				setSub1DataRcvdState(true);
				if (tpmsEventData.tpmsDMData.tpmsData.rssi > getMaxRSSI()) {
					setMaxRSSI(tpmsEventData.tpmsDMData.tpmsData.rssi);
				}
			}
		} else {
			CAB_DBG(CAB_GW_WARNING, "mq_rec for %s failed with error %s", TPMS_QUEUE_NAME, strerror(errno));
            sprintf(errorMsg, "mq_rec for %s failed with error %s", TPMS_QUEUE_NAME, strerror(errno));
            rDiagSendMessage(MESSAGE_QUEUE_OVERFLOW, GW_WARNING, errorMsg);
		}
	}

	retCode = threadDeregister(newWatchdogNode.threadId);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "Recv RF data From DM handler thread de-registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "Recv RF data From DM handler thread de-registration failed");
        }
	FUNC_EXIT
	return NULL;
}

/* This callback can be used to send event to Event Manager Queue */
void sendEvent(sysEvent_e evType, void *data, uint32_t sizeOfData)
{
	FUNC_ENTRY
	cgEventData_t eventData;
	char errorMsg[100] = "";
	struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0};
	clock_gettime(CLOCK_REALTIME, &abs_timeout);
	abs_timeout.tv_sec += MQ_SEND_EVMGR_TOUT_SEC;



	memset(&eventData, 0, sizeof(eventData));
	eventData.evType = evType;
	if (data != NULL && sizeOfData != 0) {
		memcpy(&eventData.data, data, sizeOfData);
	}

	CAB_DBG(CAB_GW_DEBUG, "sending event # %d", eventData.evType);

	if (mq_timedsend(mqEvQueue, (const char *)&eventData, sizeof(cgEventData_t), 0, &abs_timeout) < 0) {
		CAB_DBG(CAB_GW_WARNING, "mq_timedsend for %s failed with error %s", EVENT_QUEUE_NAME, strerror(errno));
		sprintf(errorMsg, "mq_timedsend for %s failed with error %s", EVENT_QUEUE_NAME, strerror(errno));
		rDiagSendMessage(MESSAGE_QUEUE_OVERFLOW, GW_WARNING, errorMsg);
	}

	FUNC_EXIT
}

/* This callback is being used by DM to report TPMS data */
void DmRfDataCB(void *tpmsData)
{
	FUNC_ENTRY
	tpmsDMData_t localDMData;
	char errorMsg[100] = "";

	struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0};
	clock_gettime(CLOCK_REALTIME, &abs_timeout);
	abs_timeout.tv_sec += MQ_SEND_TPMS_TOUT_SEC;

	if(!tpmsData)
	{
		CAB_DBG(CAB_GW_ERR, "Invalid argument");
		return;
	}

	memcpy(&localDMData, tpmsData, sizeof(tpmsDMData_t));

	if ( mq_timedsend(mqTPMSQueue, (const char *)tpmsData, sizeof(tpmsDMData_t), 0, &abs_timeout) < 0) {
		CAB_DBG(CAB_GW_WARNING, "mq_timedsend for %s failed with error %s", TPMS_QUEUE_NAME, strerror(errno));
		sprintf(errorMsg, "mq_timedsend for %s failed with error %s", TPMS_QUEUE_NAME, strerror(errno));
		rDiagSendMessage(MESSAGE_QUEUE_OVERFLOW, GW_WARNING, errorMsg);
	}

	FUNC_EXIT
}

void configChangeCallback(configParam_e changedConfig, void * optionalConfigData) 
{
	FUNC_ENTRY
	struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0};
	char errorMsg[100] = "";
	clock_gettime(CLOCK_REALTIME, &abs_timeout);
	abs_timeout.tv_sec += MQ_SEND_CFGMGR_TOUT_SEC;


	configMsgStruct_t changedConfigData;
	changedConfigData.changedConfig = changedConfig;
	if ( optionalConfigData != NULL ) {
		switch(changedConfigData.changedConfig) {
			case ADD_SENSOR_ID:
		 		memcpy(&(changedConfigData.tpmsConfigData), optionalConfigData, sizeof(tpmsConfig_t)); 
				break;
			case REMOVE_SENSOR_ID:	
		 		memcpy(&(changedConfigData.tpmsConfigData), optionalConfigData, sizeof(tpmsConfig_t)); 
				break;
			case ATTACH_HALO_SN:
			case DETACH_HALO_SN:
				memcpy(&(changedConfigData.haloSNData), optionalConfigData, sizeof(haloSNInfo_t)); 
				break;
			case UPDATE_TYRE_DETAILS:
			case FLUSH_TIRE_DETAILS:
			case UPDATE_TREAD_DEPTH_VALUE:
				memcpy(&(changedConfigData.tireDetails), optionalConfigData, sizeof(tyreInfo_t)); 
				break;
			case TIME_STAMP:
		 		memcpy(&(changedConfigData.timeStamp), optionalConfigData, sizeof(time_t)); 
				break;
			case MANDATORY_FIELDS:
				memcpy(&(changedConfigData.vehicleType), optionalConfigData, sizeof(vehicleType_e));
				break;
			default:
				break;
		}
	}

	if ( mq_timedsend(mqConfigQueue, (const char *)&(changedConfigData), sizeof(configMsgStruct_t), 0, &abs_timeout) < 0) {
		CAB_DBG(CAB_GW_WARNING, "mq_timedsend for %s failed with error %s",  CONFIG_QUEUE_NAME, strerror(errno));
		sprintf(errorMsg, "mq_timedsend for %s failed with error %s",  CONFIG_QUEUE_NAME, strerror(errno));
		rDiagSendMessage(MESSAGE_QUEUE_OVERFLOW, GW_WARNING, errorMsg);
	}

	FUNC_EXIT
}

void cloudNotifycallback(void * data)
{
    FUNC_ENTRY

    bool status;
    returnCode_e retCode;
    callBackNotification_t callBackData;
    otaProgressState_e OTAProgressState;
    configChangeInfo_t configChangeInfo = {0};

    if(!data)
    {
        CAB_DBG(CAB_GW_ERR, "Invalid argument\n");
        return;
    }

    memcpy(&callBackData, (callBackNotification_t *)data, sizeof(callBackNotification_t));

    switch (callBackData.CBNotifType)
    {
        case CLOUD_OTA_DATA:
        {
            /* We only want to send OTA event if it is not already started and BLE timeout
             * timer is not running. */
            if (getOTAState() != OTA_THREAD_RUNNING && otaTimerHandle == INVALID_TIMER_HANDLE)
            {
                OTAProgressState = OTA_CHECK_DONE;
                pthread_mutex_lock(&otaDeatailsAvailableLock);
            /* Getting the current OTA details into global structure to use it later. */
                availableOTADetails.otaInfo = callBackData.otaInfo;
                pthread_mutex_unlock(&otaDeatailsAvailableLock);
                sendEvent(OTA_CHECK, (void *)&OTAProgressState, sizeof(otaProgressState_e));
            }
            else
            {
                CAB_DBG(CAB_GW_INFO, "Either OTA is in progress or waiting for BLE disconnect.");
            }
        } break;
        case CLOUD_CONNECTION_STATUS:
        {
            status = callBackData.cloudConnectionStatus;
            retCode = setConfigModuleParam(MNT_CLOUD_CONNECTION_STATUS_CMD, (void *)&status);
            if (retCode != GEN_SUCCESS) {
                CAB_DBG(CAB_GW_ERR, "Failed to set M&T cloud connection status : %d", retCode);
            }

        } break;
        case CONFIG_CHANGE_REQUEST:
        {
            configChangeInfo = callBackData.configChangeInfo;
            targetPressureInfo_t targetPressureInfo = {};
            char fleetID[MAX_LEN_FLEET_ID] = "";
            
            if(configChangeInfo.unpairTpmsAvail) {
                    for(uint8_t i = 0; i < configChangeInfo.no_of_tpms_unpair; i++) {
        	            retCode = setConfigModuleParam(CFG_GW_TPMS_UNPAIR_CMD, (void *)&configChangeInfo.unpairTpmsSID[i]);
	                    if (retCode != GEN_SUCCESS) {
                       		CAB_DBG(CAB_GW_ERR, "Failed to Unpair TPMS : %d", retCode);
                    	    }
                    }
                    configChangeInfo.unpairTpmsAvail = 0;
            }
            if (configChangeInfo.vehicleInfoChange) {
                   retCode = setConfigModuleParam(CFG_GW_SYSTEM_CMD, &configChangeInfo.vehicleInfo);
	           if (retCode != GEN_SUCCESS) {
                   	CAB_DBG(CAB_GW_ERR, "Failed to set Vehicle Info : %d", retCode);
                   }
	    }
            if(configChangeInfo.tireInfoChange) {
                    for(uint8_t i = 0; i < configChangeInfo.no_of_tire; i++) {
        	            retCode = setConfigModuleParam(CFG_GW_UPDATE_TYRE_DETAILS_CMD, (void *)&configChangeInfo.tireInfo[i]);
	                    if (retCode != GEN_SUCCESS) {
                       		CAB_DBG(CAB_GW_ERR, "Failed to PAIR TPMS : %d", retCode);
                    	}
                    }
            }
            if (configChangeInfo.tpmsInfoChange) {
                    for(uint8_t i = 0; i < configChangeInfo.no_of_tpms; i++) {
        	            retCode = setConfigModuleParam(CFG_GW_TPMS_PAIR_CMD, (void *)&configChangeInfo.tpmsInfo[i]);
	                    if (retCode != GEN_SUCCESS) {
                       		CAB_DBG(CAB_GW_ERR, "Failed to PAIR TPMS : %d", retCode);
                    	}
                    }
                    configChangeInfo.tpmsInfoChange = 0;
            }
            if(configChangeInfo.halo_info_attach) {
                    for(uint8_t i = 0; i < configChangeInfo.no_of_halo_attach; i++) {
        	            retCode = setConfigModuleParam(CFG_GW_ATTACH_HALO_SN_CMD, (void *)&configChangeInfo.halos[i]);
	                    if (retCode != GEN_SUCCESS) {
                       		CAB_DBG(CAB_GW_ERR, "Failed to add halo : %d", retCode);
                            }
                    }
                   configChangeInfo.halo_info_attach = false;
            }
            if(configChangeInfo.halo_info_detach) {
                    for(uint8_t i = 0; i < configChangeInfo.no_of_halo_detach; i++) {
        	            retCode = setConfigModuleParam(CFG_GW_DETACH_HALO_SN_CMD, (void *)&configChangeInfo.halos_detach[i]);
	                    if (retCode != GEN_SUCCESS) {
                       		CAB_DBG(CAB_GW_ERR, "Failed to add halo : %d", retCode);
                            }
                    }
                   configChangeInfo.halo_info_detach = false;
            }
            if (configChangeInfo.setPointChangeAvailable) {
                retCode = getConfigModuleParam(TPMS_THRESHOLD, (void *) &targetPressureInfo);
                if (retCode != GEN_SUCCESS) {
                    CAB_DBG(CAB_GW_WARNING, "get config failed with error %d", retCode);
                }
                else if(targetPressureInfo.tpmsThreshold[0] == configChangeInfo.tpmsThreshold[0] &&
                        targetPressureInfo.tpmsThreshold[1] == configChangeInfo.tpmsThreshold[1]){
                    CAB_DBG(CAB_GW_WARNING, "The same configuration value is provided for change");
                }
                else {
                    targetPressureInfo.tpmsThreshold[0] = configChangeInfo.tpmsThreshold[0];

                    for (uint8_t i=1; i<targetPressureInfo.numberOfAxle; i++) {
                        targetPressureInfo.tpmsThreshold[i] = configChangeInfo.tpmsThreshold[1];
                    }

                    retCode = setConfigModuleParam(CFG_GW_TARGET_PRESSURE_CMD, (void *)&targetPressureInfo);
                    if (retCode != GEN_SUCCESS) {
                        CAB_DBG(CAB_GW_ERR, "Failed to set Target Pressure : %d", retCode);
                    }
                }
                configChangeInfo.setPointChangeAvailable = false;
            }
        
            if (configChangeInfo.fleetIDChangeAvailable) {
                retCode = getConfigModuleParam(FLEET_ID, (void *)&fleetID);
                if (retCode != GEN_SUCCESS) {
                    CAB_DBG(CAB_GW_WARNING, "get config failed with error %d", retCode);
                }
                else if (strncmp(fleetID, configChangeInfo.fleetID, MAX_LEN_FLEET_ID) == 0){
                    CAB_DBG(CAB_GW_WARNING, "The same configuration value is provided for change");
                }
                else {
                    strncpy(fleetID, configChangeInfo.fleetID, MAX_LEN_FLEET_ID);
                    retCode = setConfigModuleParam(CHANGE_FLEET_ID_CMD, (void *)&fleetID);
                    if (retCode != GEN_SUCCESS) {
                        CAB_DBG(CAB_GW_ERR, "Failed to set Fleet ID : %d", retCode);
                    }
                }
                configChangeInfo.fleetIDChangeAvailable = false;
            }
           isConfigChangefromCloud = false; 
        } break;
        default:
            break;
    }

    FUNC_EXIT
}

void zipUploadNotifycallback(DataFilezipType_e fileType, void * uploadStatus)
{
	FUNC_ENTRY

	bool status = false;
	returnCode_e retCode;

	if(!uploadStatus)
	{
		CAB_DBG(CAB_GW_ERR, "Invalid argument");
		return;
	}

	switch(fileType)
	{
	case DATA_RDIAG_ZIP:
		break;
	case DATA_STORAGE_ZIP:
		break;
	case DATA_CONFIG_ZIP:
		memcpy(&status, uploadStatus, sizeof(bool));
		retCode = setConfigModuleParam(CONIF_FILE_UPLOAD_STATUS_CMD, (void *)&status);
		if (retCode != GEN_SUCCESS) {
			CAB_DBG(CAB_GW_ERR, "Failed to set cofing file upload status : %d", retCode);
		}
		break;
	default:
		CAB_DBG(CAB_GW_ERR, "Invalid argument");
		break;
	}

	FUNC_EXIT
}

void rDiagModuleResHandlerCallback(void * data)
{
	FUNC_ENTRY

	returnCode_e retCode = GEN_SUCCESS;
	remoteDiagWrapper_t msg;
	initConfig_t initCloudConfig;
	memset(&initCloudConfig, '\0', sizeof(initCloudConfig));

	if(!data)
	{
		CAB_DBG(CAB_GW_ERR, "Invalid argument");
		return;
	}

	memcpy(&msg, data, sizeof(remoteDiagWrapper_t));

	if(getState() != DEINIT_STATE)
	{
		switch(msg.type)
		{
			case GW_ID_REQ:
			{
				// start the process to get latest GW ID
				getBleMacAdd();
				if (retCode != GEN_SUCCESS) {
				    CAB_DBG(CAB_GW_ERR, "Get BLE MAC address failed with code %d", retCode);
				}
				/*The above call will send a command to the config manager and cloud parameters
				 * will be updated. so sleep it is intended to wait for the cloud parameter update.*/
				sleep(2);
				retCode = getConfigModuleParam(GATEWAY_ID, initCloudConfig.gatewayId);
				if (retCode != GEN_SUCCESS) {
					CAB_DBG(CAB_GW_ERR, "Get config failed with code %d", retCode);
				}
				retCode = getConfigModuleParam(CLOUD_URI, initCloudConfig.cloudURI);
				if (retCode != GEN_SUCCESS) {
					CAB_DBG(CAB_GW_ERR, "Get config failed with code %d", retCode);
				}
				retCode = getConfigModuleParam(SOFTWARE_VERSION, initCloudConfig.softwareVersion);
				if (retCode != GEN_SUCCESS) {
					CAB_DBG(CAB_GW_ERR, "Get config failed with code %d", retCode);
				}
				sendGatewayIdToRdiag(&initCloudConfig);

				if(sendBackInitConfig == true)
				{
					//send INIT_CONFIG
					rDiagSendInitConf(MAX_ZIP_COUNT, initCloudConfig.cloudURI, initCloudConfig.gatewayId, initCloudConfig.softwareVersion);
					sendBackInitConfig = false;
				}
			}
			break;
			case SEND_BACK_INIT_CONFIG:
			{
				sendBackInitConfig = true;
			}
			break;
			case SWOVER_REQ:
			{
				//char avialOps[1024]="";
				//CAB_DBG(CAB_GW_INFO, "Switch over requested");
				//getAvailOpInfo(&avialOps);
                                //swOver(&avialOps);
				// rDiagSendMessage(CABAPP_GEN_WARN, GW_WARNING, "Switch over requested");
			}break;
			case INTERNET_CONNECTIVITY_TIMESTAMP_UPDATE:
			{
				time_t timestamp;
				memcpy(&timestamp, &(msg.timeStamp), sizeof(time_t));
				retCode = setConfigModuleParam(INTERNET_CONNECTIVITY_TIMESTAMP, (void *)&timestamp);
				if (retCode != GEN_SUCCESS) {
					CAB_DBG(CAB_GW_ERR, "Set config failed with code %d", retCode);
				}
			} break;
			case CLOUD_CONNECTION_STATUS_UPDATE:
			{
				bool connectionStatus;
				memcpy(&connectionStatus, &(msg.cloudConnectionStatus), sizeof(bool));

				retCode = setConfigModuleParam(RDIAG_CLOUD_CONNECTION_STATUS_CMD, (void *)&connectionStatus);
				if (retCode != GEN_SUCCESS) {
					CAB_DBG(CAB_GW_ERR, "Failed to set rDiag cloud connection status with code %d", retCode);
				}
			}
			break;
			default:
				break;
		}
	}

	FUNC_EXIT
}


returnCode_e changeConfigAction(configParam_e changedConfig, void * getDataPtr, changeConfigParam_t* changedEventHandler)
{
	returnCode_e retCode = GEN_SUCCESS;
	changedEventHandler->type = changedConfig;
        retCode = dmHandler->rtcHandler->getData((void *)&(changedEventHandler->time));
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "get time data failed with error %d", retCode);
		return retCode;
	}
	retCode = getConfigModuleParam(changedConfig, (void *)getDataPtr);
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_WARNING, "get config %d failed with error %d ", changedConfig, retCode);
		rDiagSendMessage(BLE_CONFIG_GET_FAIL, GW_WARNING, "BLE Get param failed");
		return retCode;
	}
	retCode = storeDataToSDC((void*)changedEventHandler, CONFIG_PARAM);
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Store SDC data failed with %d", retCode);
		return retCode;
	}

}


static bool getConfigManagerThreadAliveStatus(void)
{
        bool status;
        pthread_mutex_lock(&configManagerThreadAliveLock);
        status = g_configManagerThreadAliveStatus;
        pthread_mutex_unlock(&configManagerThreadAliveLock);
        return status;
}

static void setConfigManagerThreadAliveStatus(bool value)
{
        pthread_mutex_lock(&configManagerThreadAliveLock);
        g_configManagerThreadAliveStatus = value;
        pthread_mutex_unlock(&configManagerThreadAliveLock);
}

bool configManagerCallback(void)
{
        bool status = false;
        CAB_DBG(CAB_GW_TRACE, "In func : %s", __func__);
        if(getConfigManagerThreadAliveStatus() == true)
        {
                status = true;
        }

        setConfigManagerThreadAliveStatus(false);

	configChangeCallback(WATCHDOG_TIMER_CONFIG, NULL);
        return status;
}

static void *configManager(void *arg)
{
	FUNC_ENTRY
	ssize_t bytes_read;
	configMsgStruct_t tempConfigMsg;
	changeConfigParam_t configEventSDCData;
	cabGatewayConfig_t gwConfig;
	returnCode_e retCode = GEN_SUCCESS;
	uint8_t tpmsInd = 0;
	sensorVendorType_e sensMakeTemp = 0;
        uint8_t tempGatewayId[MAX_LEN_GATEWAY_ID] = {0};
	cgEventData_t eventData;
	char tempSimIccid[MAX_LEN_SIM_ICCID];           //sim iccid
	uint8_t tempIMEI[MAX_LEN_LTE_IMEI] = {0};       //LTE IMIE number
	char tempCloudURI[MAX_LEN_CLOUD_URI] = {'\0'};	
      	uint8_t present = 0, bleMacUpdated = 0;
        char simNotPStr[] = NO_SIM_PRESENT_STR;
	uint8_t count = 0;
	char errorMsg[100] = "";

	cloudInitInfo_t initCloudConfig;
	watchDogTimer_t newWatchdogNode;

	newWatchdogNode.threadId = syscall(SYS_gettid);
	newWatchdogNode.CallbackFn = configManagerCallback;

	memset(&initCloudConfig, 0, sizeof(cloudInitInfo_t));
        memset(&eventData, 0, sizeof(eventData));

	CAB_DBG(CAB_GW_INFO, "Config manager thread id : %ld", syscall(SYS_gettid));
	retCode = threadRegister(&newWatchdogNode);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "Config manager thread registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_REGISTRATION_FAIL, GW_ERR, "Config manager thread registration failed");
        }

	while(1)
	{
		memset(&tempConfigMsg, 0, sizeof(configMsgStruct_t));
		bytes_read = mq_receive(mqConfigQueue, (char *)&tempConfigMsg, sizeof(configMsgStruct_t), NULL);
		if (getState() == DEINIT_STATE || getState() == OTA_STATE) {
			CAB_DBG(CAB_GW_INFO, "Config Manager thread deinit");
			pthread_exit(0);
		}

		if(bytes_read >= 0)
		{
			if ( (getState() == INIT_STATE || getState() == SELF_DIAG_STATE) && tempConfigMsg.changedConfig != GATEWAY_CONFIGURED && 
				     tempConfigMsg.changedConfig != GATEWAY_NOT_CONFIGURED && tempConfigMsg.changedConfig != BLE_MAC &&
				     tempConfigMsg.changedConfig != BLE_DISCONNECTED && tempConfigMsg.changedConfig != BLE_ADVERTISEMENT_STARTED) {
				continue;
			}
			/*Reset ble Keep alive check if receive any configuration change from mobile application*/
			if(tempConfigMsg.changedConfig != GATEWAY_CONFIGURED && tempConfigMsg.changedConfig != GATEWAY_NOT_CONFIGURED && 
			   tempConfigMsg.changedConfig != BLE_MAC)
				resetBleKeepAliveCntr();

			switch(tempConfigMsg.changedConfig) {
				case WATCHDOG_TIMER_CONFIG:
					{
						setConfigManagerThreadAliveStatus(true);
						break;
					}
				case GATEWAY_CONFIGURED:
					{
						if((getState() != INIT_STATE) && (getState() != CONFIG_STATE) && (getState() != SELF_DIAG_STATE)){
							CAB_DBG(CAB_GW_INFO, "Gateway already configured !");
							continue;
						} 
						CAB_DBG(CAB_GW_DEBUG, "Applying gateway configuration...");
                                                retCode = getConfigModuleParam(ALL_DATA, (void *)&gwConfig);
                                                if (retCode != GEN_SUCCESS) {
                                                        CAB_DBG(CAB_GW_WARNING, "get config failed with error %d", retCode);
							rDiagSendMessage(BLE_CONFIG_GET_FAIL, GW_WARNING, "BLE Get param failed");
                                                        continue;
                                                }
						/* update to global variable for TPMS thread */
                                                memcpy(targetPressure, gwConfig.targetPressInfo.tpmsThreshold, sizeof(gwConfig.targetPressInfo.tpmsThreshold));
						/* Add TPMS sensors */
						for (tpmsInd = 0; tpmsInd < MAX_TPMS; tpmsInd++) {
							if (gwConfig.vehicleDef_s.tpmsConfig[tpmsInd].sensorID != 0) {
								retCode = dmHandler->rfIntfHandler->addDevice((void *)&(gwConfig.vehicleDef_s.tpmsConfig[tpmsInd]));
        	                                		if (retCode != GEN_SUCCESS) {
									if (retCode != DM_HASH_ERROR) {
										CAB_DBG(CAB_GW_ERR, "Initial add device failed");
										rDiagSendMessage(TPMS_PAIR_FAIL, GW_ERR, "Init TPMS pair failed");
										indicateSysStatus(ERROR);
										setState(DEINIT_STATE);
									}
									continue;
                                                		}
							}
						}


						if (getConfigModuleParam(HARDWARE_VERSION, gwConfig.hardwareVersion) != GEN_SUCCESS) {
							getHardwareVersion(gwConfig.hardwareVersion);
						}
						retCode = diagSetGatewayInfo(SYS_HARDWARE_VERSION, (void *)gwConfig.hardwareVersion);
                                                if (retCode != GEN_SUCCESS) {
                                                        CAB_DBG(CAB_GW_ERR, "Diiag HW version set info failed with code %d", retCode);
							rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_ERR, "Diag set GW info failed for HW version");
							indicateSysStatus(ERROR);
							setState(DEINIT_STATE);
                                                        continue;
                                                }

						retCode = diagSetGatewayInfo(SYS_SOFTWARE_VERSION, (void *)gwConfig.softwareVersion);
        	                                if (retCode != GEN_SUCCESS) {
	                                                CAB_DBG(CAB_GW_ERR, "Diag SW version set info failed with code %d", retCode);
							rDiagSendMessage(CABAPP_BLE_INIT_FAIL, GW_ERR, "Diag set GW info failed for SW version");
							indicateSysStatus(ERROR);
							setState(DEINIT_STATE);
                                                	continue;
                                                }

						if (getConfigModuleParam(LTE_IMEI, tempIMEI) != GEN_SUCCESS) {
							retCode = getLteIMEINum(tempIMEI);
							if (retCode != GEN_SUCCESS) {
								CAB_DBG(CAB_GW_ERR, "Failed to get IMEI number with code %d", retCode);
								rDiagSendMessage(CABAPP_LTE_INIT_FAIL, GW_ERR, "Failed to get IMEI number");
								indicateSysStatus(ERROR);
								setState(DEINIT_STATE);
								continue;
							}
							retCode = setConfigModuleParam(SYS_SET_LTE_IMEI_CMD, tempIMEI);
							if (retCode != GEN_SUCCESS) {
								CAB_DBG(CAB_GW_ERR, "Failed to set IMEI number with code %d", retCode);
								rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_ERR, "Failed to set IMEI number");
								indicateSysStatus(ERROR);
								setState(DEINIT_STATE);
								continue;
							}
						}

						retCode = diagSetGatewayInfo(SYS_MODEM_IMEI, (void *)tempIMEI);
						if (retCode != GEN_SUCCESS) {
										CAB_DBG(CAB_GW_ERR, "Dig Modem IMEI set info failed with code %d", retCode);
										rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_ERR, "Diag set IMEI failed");
										indicateSysStatus(ERROR);
										setState(DEINIT_STATE);
										continue;
						}

						count = 0;
						do {
							retCode = checkSIMDetection(&present);
							memset(tempSimIccid, 0, sizeof(tempSimIccid));
							if (retCode != GEN_SUCCESS) {
								CAB_DBG(CAB_GW_ERR, "Check SIM detection failed with code %d", retCode);
								setSimDetFailCnt(getSimDetFailCnt() + 1);
							} else {
								if (present > 1) {
									retCode = getLteICCIDNum(tempSimIccid);
									if (retCode != GEN_SUCCESS) {
										CAB_DBG(CAB_GW_ERR, "Failed to get ICCID number with code %d", retCode);
										rDiagSendMessage(CABAPP_SIM_ICCID_NOT_AVAIL, GW_ERR, "Failed to get ICCID number");
									} else {
										retCode = setConfigModuleParam(SYS_SET_SIM_ICCID_CMD, tempSimIccid);
										if (retCode != GEN_SUCCESS) {
											CAB_DBG(CAB_GW_ERR, "Failed to set ICCID number with code %d", retCode);
											rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_ERR, "Set SIM ICCID failed");
										}
									}
									break;
								} else {
									memcpy(tempSimIccid, simNotPStr, sizeof(simNotPStr));
									setSimDetFailCnt(getSimDetFailCnt() + 1);
								}

								retCode = diagSetGatewayInfo(SYS_SIM_ICCID, (void *)tempSimIccid);
								if (retCode != GEN_SUCCESS) {
									CAB_DBG(CAB_GW_ERR, "Diag SIM ICCID set info failed with code %d", retCode);
									rDiagSendMessage(CABAPP_SIM_ICCID_NOT_AVAIL, GW_ERR, "Diag set SIM ICCID failed");
								}
							}
						} while(count++ <= 3);

						retCode = diagSetGatewayInfo(SYS_GATEWAY_UNIQUE_ID, (void *)gwConfig.gatewayId);
						if (retCode != GEN_SUCCESS) {
										CAB_DBG(CAB_GW_ERR, "Diag GW ID set info failed with code %d", retCode);
										indicateSysStatus(ERROR);
										rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_ERR, "SYS init failed as diag set GW info failed");
										setState(DEINIT_STATE);
										continue;
						}

						retCode = diagSetGatewayInfo(SYS_CLOUD_URI, (void *)gwConfig.cloudURI);
       						if (retCode != GEN_SUCCESS) {
       						        CAB_DBG(CAB_GW_ERR, "Diag Cloud URI set info failed with code %d", retCode);
							rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_ERR, "Set Cloud URI API failed");
							indicateSysStatus(ERROR);
							setState(DEINIT_STATE);
							continue;
       						}
						rDiagSendInitConf(MAX_SDC_ZIP, initCloudConfig.url, initCloudConfig.gatewayID, initSoftwareVersion);

						/* send init done event*/
						sendEvent(INIT_DONE, NULL, 0);
						/* send cofig success event*/
						sendEvent(CONFIG_SUCCESS, NULL, 0);
					}
					break;
				case GATEWAY_NOT_CONFIGURED:
					{
						CAB_DBG(CAB_GW_INFO, "GW is not configured");
						/* send init done event*/
	       					sendEvent(INIT_DONE, NULL, 0);
					}
					break;
				case ADD_SENSOR_ID:
					{
						retCode = dmHandler->rfIntfHandler->addDevice((void *)&(tempConfigMsg.tpmsConfigData));
						if(retCode != GEN_SUCCESS)
						{
							CAB_DBG(CAB_GW_ERR, "Add sensor(rFAddDevice) fail with error %d", retCode);
							/*if retCode!=DM_HASH_ERROR meaning connection with sub1 receiver may lost*/
							if (retCode != DM_HASH_ERROR) {
							       CAB_DBG(CAB_GW_ERR, "Can't communicate with sub1 receiver. Deinitialise the gateway");
							       rDiagSendMessage(SUB1_KEEP_ALIVE_FAILURE, GW_ERR, "Sub1 Keep Alive failed");
                                                               indicateSysStatus(ERROR);
                                                               setState(DEINIT_STATE);
                                                        }
							continue;
						}
						memcpy(&(configEventSDCData.tpmsSensorID), &(tempConfigMsg.tpmsConfigData), sizeof(tpmsConfig_t));
						configEventSDCData.type = tempConfigMsg.changedConfig;
        					retCode = dmHandler->rtcHandler->getData((void *)&(configEventSDCData.time));
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "get time data failed with error %d", retCode);
						}
						retCode = storeDataToSDC((changeConfigParam_t *)&(configEventSDCData), CONFIG_PARAM);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "Store SDC data failed with %d", retCode);
						}
					}
					break;
				case REMOVE_SENSOR_ID:
					{
						retCode = dmHandler->rfIntfHandler->removeDevice((void *)&(tempConfigMsg.tpmsConfigData));
						if(retCode != GEN_SUCCESS)
						{
							CAB_DBG(CAB_GW_ERR, "Remove sensor(rfRemoveDevice) fail with error %d", retCode);
                                                        /*if retCode!=DM_HASH_ERROR meaning connection with sub1 receiver may lost*/
                                                        if (retCode != DM_HASH_ERROR) {
                                                               CAB_DBG(CAB_GW_ERR, "Can't communicate with sub1 receiver. Deinitialise the gateway");
                                                               rDiagSendMessage(SUB1_KEEP_ALIVE_FAILURE, GW_ERR, "Can't communicate with sub1 receiver. Deinitialise the gateway");
                                                               indicateSysStatus(ERROR);
                                                               setState(DEINIT_STATE);
                                                        }
							continue;
						}
						memcpy(&(configEventSDCData.tpmsSensorID), &(tempConfigMsg.tpmsConfigData), sizeof(tpmsConfig_t));
						configEventSDCData.type = tempConfigMsg.changedConfig;
        					retCode = dmHandler->rtcHandler->getData((void *)&(configEventSDCData.time));
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "get time data failed with error %d", retCode);
							continue;
						}
						retCode = storeDataToSDC((changeConfigParam_t *)&(configEventSDCData), CONFIG_PARAM);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "Store SDC data failed with %d", retCode);
							continue;
						}
					}
					break;
				case ATTACH_HALO_SN:
				case DETACH_HALO_SN:
					{
						memcpy(&(configEventSDCData.haloSNData), &(tempConfigMsg.haloSNData), sizeof(haloSNInfo_t));
						configEventSDCData.type = tempConfigMsg.changedConfig;

						retCode = dmHandler->rtcHandler->getData((void *)&(configEventSDCData.time));
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "get time data failed with error %d", retCode);
							continue;
						}

						retCode = storeDataToSDC((changeConfigParam_t *)&(configEventSDCData), CONFIG_PARAM);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "Store SDC data failed with %d", retCode);
							continue;
						}
					}
					break;
				case UPDATE_TYRE_DETAILS:
				case FLUSH_TIRE_DETAILS:
				case UPDATE_TREAD_DEPTH_VALUE:
					{
						memcpy(&(configEventSDCData.tireDetails), &(tempConfigMsg.tireDetails), sizeof(tyreInfo_t));
						configEventSDCData.type = tempConfigMsg.changedConfig;

						retCode = dmHandler->rtcHandler->getData((void *)&(configEventSDCData.time));
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "get time data failed with error %d", retCode);
							continue;
						}

						retCode = storeDataToSDC((changeConfigParam_t *)&(configEventSDCData), CONFIG_PARAM);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "Store SDC data failed with %d", retCode);
							continue;
						}
					}
					break;
				case MANDATORY_FIELDS:
					{
						time_t timeData;
						retCode = changeConfigAction(CUSTOMER_ID, (void *)configEventSDCData.customerID, &configEventSDCData);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "change config action failed %d", retCode);
							continue;
						}
						retCode = changeConfigAction(FLEET_ID, (void *)configEventSDCData.fleetID, &configEventSDCData);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "change config action failed %d", retCode);
							continue;
						}
						retCode = changeConfigAction(VEHICLE_NUMBER, (void *)configEventSDCData.vehicleNum, &configEventSDCData);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "change config action failed %d", retCode);
							continue;
						}
						retCode = changeConfigAction(VEHICLE_TYPE, (void *)&(configEventSDCData.vehicleType), &configEventSDCData);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "change config action failed %d", retCode);
							continue;
						}
						/* When vehicle type is changed */
						if(configEventSDCData.vehicleType != tempConfigMsg.vehicleType)
						{
							/* Flush all TPMS */
							retCode = dmHandler->rfIntfHandler->flushDevices(NULL);
							if ( retCode != GEN_SUCCESS ) {
						                CAB_DBG(CAB_GW_ERR, "Flush sensor(rfFlushDevice) fail with error %d", retCode);
						                /*if retCode!=DM_HASH_ERROR meaning connection with sub1 receiver may lost*/
						                if (retCode != DM_HASH_ERROR) {
						                       CAB_DBG(CAB_GW_ERR, "Can't communicate with sub1 receiver. Deinitialise the gateway");
						                       indicateSysStatus(ERROR);
							       	       rDiagSendMessage(SUB1_KEEP_ALIVE_FAILURE, GW_ERR, "SUB1 communication failed");
						                       setState(DEINIT_STATE);
						                }
								continue;
							}
							/* Get target pressure for new vehicle type*/
							retCode = getConfigModuleParam(TPMS_THRESHOLD, (void *)&(configEventSDCData.targetPressInfo));
							if (retCode != GEN_SUCCESS) {
							        CAB_DBG(CAB_GW_WARNING, "get config failed with error %d", retCode);
								rDiagSendMessage(BLE_CONFIG_GET_FAIL, GW_WARNING, "BLE Get param failed");
							        continue;
							}
							/* update to global variable for TPMS thread */
							memcpy(targetPressure, configEventSDCData.targetPressInfo.tpmsThreshold, \
						                sizeof(configEventSDCData.targetPressInfo.tpmsThreshold));

							/* send cofig success event*/
							sendEvent(CONFIG_SUCCESS, NULL, 0);
						}
						retCode = dmHandler->rtcHandler->getData((void *)&timeData);
						if ( retCode != GEN_SUCCESS ) {
							CAB_DBG(CAB_GW_ERR, "RTC DM get data failed with code %d", retCode);
							continue;
						}
						otaNotifier();
						retCode = reportEventToSDC(TRANSMIT_EV, (void *)&timeData, CSV_DATA);
						if ( retCode != GEN_SUCCESS ) {
							CAB_DBG(CAB_GW_ERR, "Report event to SDC failed with code %d", retCode);
							continue;
						}
					}
					break;
				case CUSTOMER_ID: 
					{
						retCode = changeConfigAction(CUSTOMER_ID, (void *)configEventSDCData.customerID, &configEventSDCData);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "change config action failed %d", retCode);
							continue;
						}
					}
					break;
			        case VEHICLE_NUMBER:
					{
						time_t timeData;
						retCode = changeConfigAction(VEHICLE_NUMBER, (void *)configEventSDCData.vehicleNum, &configEventSDCData);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "change config action failed %d", retCode);
							continue;
						}
						retCode = dmHandler->rtcHandler->getData((void *)&timeData);
						if ( retCode != GEN_SUCCESS ) {
							CAB_DBG(CAB_GW_ERR, "RTC DM get data failed with code %d", retCode);
							continue;
						}
						otaNotifier();
						retCode = reportEventToSDC(TRANSMIT_EV, (void *)&timeData, CSV_DATA);
						if ( retCode != GEN_SUCCESS ) {
							CAB_DBG(CAB_GW_ERR, "Report event to SDC failed with code %d", retCode);
							continue;
						}
					}
					break;
			        case FLEET_ID:
					{
						retCode = changeConfigAction(FLEET_ID, (void *)configEventSDCData.fleetID, &configEventSDCData);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "change config action failed %d", retCode);
							continue;
						}
					}
					break;
			        case VEHICLE_TYPE:
					{
						retCode = changeConfigAction(VEHICLE_TYPE, (void *)&(configEventSDCData.vehicleType), &configEventSDCData);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "change config action failed %d", retCode);
							continue;
						}
						/* Flush all TPMS */
						retCode = dmHandler->rfIntfHandler->flushDevices(NULL);
						if ( retCode != GEN_SUCCESS ) {
                                                        CAB_DBG(CAB_GW_ERR, "Flush sensor(rfFlushDevice) fail with error %d", retCode);
                                                        /*if retCode!=DM_HASH_ERROR meaning connection with sub1 receiver may lost*/
                                                        if (retCode != DM_HASH_ERROR) {
                                                               CAB_DBG(CAB_GW_ERR, "Can't communicate with sub1 receiver. Deinitialise the gateway");
                                                               indicateSysStatus(ERROR);
							       rDiagSendMessage(SUB1_KEEP_ALIVE_FAILURE, GW_ERR, "SUB1 communication failed");
                                                               setState(DEINIT_STATE);
                                                        }
 							continue;
						}
						/* Get target pressure for new vehicle type*/
        					retCode = getConfigModuleParam(TPMS_THRESHOLD, (void *)&(configEventSDCData.targetPressInfo));
        					if (retCode != GEN_SUCCESS) {
        					        CAB_DBG(CAB_GW_WARNING, "get config failed with error %d", retCode);
							rDiagSendMessage(BLE_CONFIG_GET_FAIL, GW_WARNING, "BLE Get param failed");
        					        continue;
        					}
						/* update to global variable for TPMS thread */
                                                memcpy(targetPressure, configEventSDCData.targetPressInfo.tpmsThreshold, \
                                                        sizeof(configEventSDCData.targetPressInfo.tpmsThreshold));

						/* send cofig success event*/
       						sendEvent(CONFIG_SUCCESS, NULL, 0);
					}
					break;
			        case TIME_STAMP:
					{
						uint8_t setRTCFlag = (1 << RTC_MOBILE_SYNC_BIT), isRtcAlreadySync;
						retCode = dmHandler->rtcHandler->setData((void *)&(tempConfigMsg.timeStamp));
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "set RTC time failed with error %d", retCode);
							continue;
						}
						retCode = getConfigModuleParam(RTC_FLAG, (void *)&isRtcAlreadySync);
                                                if (retCode != GEN_SUCCESS) {
                                                        CAB_DBG(CAB_GW_WARNING, "Get RTC flag failed with error %d", retCode);
							rDiagSendMessage(BLE_CONFIG_GET_FAIL, GW_WARNING, "BLE Get param failed");
                                                        continue;
                                                }
						/*Allow time configuration from mobile app until gateway is being configured 
						with network time from LTE*/
						retCode = setConfigModuleParam(SYS_RTC_FLAG, &setRTCFlag);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_WARNING, "set RTC flag failed with error %d", retCode);
		      					rDiagSendMessage(BLE_CONFIG_SET_FAIL, GW_WARNING, "BLE set diag failed for SS");
							continue;
						}
						/*If RTC sync first time, startDeviceConfiguration as device has been waiting*/
                                                if(!isRtcAlreadySync){
                                                        startDeviceConfiguration();
                                                }
					}
					break;
			        case FLEET_NAME:
					{
						retCode = changeConfigAction(FLEET_NAME, (void *)configEventSDCData.fleetName, &configEventSDCData);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "change config action failed %d", retCode);
							continue;
						}
					}
					break;
			        case VEHICLE_MAKE:
					{
						retCode = changeConfigAction(VEHICLE_MAKE, (void *)configEventSDCData.vehicleMake, &configEventSDCData);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "change config action failed %d", retCode);
							continue;
						}
					}
					break;
			        case TYRE_MAKE:
					{
						retCode = changeConfigAction(TYRE_MAKE, (void *)configEventSDCData.tyreMake, &configEventSDCData);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "change config action failed %d", retCode);
							continue;
						}
					}
					break;
			        case TYRE_SIZE:
					{
						retCode = changeConfigAction(TYRE_SIZE, (void *)configEventSDCData.tyreSize, &configEventSDCData);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "change config action failed %d", retCode);
							continue;
						}
					}
					break;
			        case VEHICLE_YEAR:
					{
						retCode = changeConfigAction(VEHICLE_YEAR, (void *)&(configEventSDCData.vehicleYear), &configEventSDCData);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "change config action failed %d", retCode);
							continue;
						}
					}
					break;
			        case TPMS_THRESHOLD:
					{
						retCode = changeConfigAction(TPMS_THRESHOLD, (void *)&(configEventSDCData.targetPressInfo), &configEventSDCData);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "change config action failed %d", retCode);
							continue;
						}
						/* update to global variable for TPMS thread */
						memcpy(targetPressure, configEventSDCData.targetPressInfo.tpmsThreshold, \
						        sizeof(configEventSDCData.targetPressInfo.tpmsThreshold));
					}
					break;
			        case LOW_BATTERY_THRESHOLD:
					{
						batInitCfg_t batConfig;
						retCode = changeConfigAction(LOW_BATTERY_THRESHOLD, (void *)&(configEventSDCData.lowBatteryThreshold), &configEventSDCData);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "change config action failed %d", retCode);
							continue;
						}

						retCode = getConfigModuleParam(LOW_BATTERY_THRESHOLD, &batConfig.lowBatThreshold);
						if (retCode != GEN_SUCCESS)
						{
							CAB_DBG(CAB_GW_WARNING, "Get low battery threashold failed with code %d", retCode);
							rDiagSendMessage(BLE_CONFIG_GET_FAIL, GW_WARNING, "BLE Get param failed");
							return retCode;
						}

						retCode = dmHandler->batHandler->setData((void *)&batConfig);
						if(retCode != GEN_SUCCESS)
						{
							CAB_DBG(CAB_GW_ERR, "Set battery data failed with %d", retCode);
							return retCode;
						}

					}
					break;
			        case CLOUD_URI:
					{
						retCode = changeConfigAction(CLOUD_URI, (void *)configEventSDCData.cloudURI, &configEventSDCData);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "change config action failed %d", retCode);
							continue;
						}
						
						retCode = diagSetGatewayInfo(SYS_CLOUD_URI, (void *)configEventSDCData.cloudURI);
       						if (retCode != GEN_SUCCESS) {
       						        CAB_DBG(CAB_GW_ERR, "Dig Cloud URI set info failed with code %d", retCode);
							continue;
       						}

				                retCode = updateCloudUrl(configEventSDCData.cloudURI);
				                if(retCode != GEN_SUCCESS) {
				                        CAB_DBG(CAB_GW_ERR, "update cloud URI fail with error %d", retCode);
							continue;
				                }
				                memcpy(initCloudConfig.url, configEventSDCData.cloudURI, MAX_LEN_CLOUD_URI);
						rDiagSendInitConf(MAX_SDC_ZIP, initCloudConfig.url, initCloudConfig.gatewayID, initSoftwareVersion);


					}
					break;
			        case SENSOR_TYPE:
					{
						retCode = changeConfigAction(SENSOR_TYPE, (void *)&(configEventSDCData.sensorType), &configEventSDCData);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_ERR, "change config action failed %d", retCode);
							continue;
						}
						/* TODO: change sensor type in DM will be done after MVP when more devices are supported */
					}
					break;
			        case BLE_CONNECTED:
					{
						/*  Should be incorporated with OTA module*/
						CAB_DBG(CAB_GW_INFO, "BLE Connected");
						setBLEConnectState(BLE_CONNECTED_MNT_FLAG);
					}
					break;
			        case BLE_DISCONNECTED:
					{
						/*  Should be incorporated with OTA module*/
						CAB_DBG(CAB_GW_INFO, "BLE DisConnected");
						setBLEConnectState(BLE_DISCONNECTED_MNT_FLAG);
					}
					break;
                    case BLE_ADVERTISEMENT_STARTED:
                    {
                        /* Need to change the LED indication only first time. */
                        if (!bleAdvertisementStarted)
                        {
                            bleAdvertisementStarted = true;
                            CAB_DBG(CAB_GW_DEBUG, "BLE Advertisement started");
                            // Now change the LED indication to normal
                            indicateSysStatus(NORMAL);
                        }
                  } break;
			        case BLE_MAC:
					{
						/*MAC query response received. Connection with BLE module is alive*/
						setBleAliveStatus(true);
						CAB_DBG(CAB_GW_DEBUG, "BLE MAC in M&T");

						retCode = getConfigModuleParam(GATEWAY_ID, tempGatewayId);
						if (retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_WARNING, "get config GW ID failed with error %d", retCode);
							rDiagSendMessage(BLE_CONFIG_GET_FAIL, GW_WARNING, "BLE Get param failed");
							continue;
						}
						
						retCode = getConfigModuleParam(CLOUD_URI, tempCloudURI);
						if(retCode != GEN_SUCCESS) {
							CAB_DBG(CAB_GW_WARNING, "get config cloud url failed with error %d", retCode);
							rDiagSendMessage(BLE_CONFIG_GET_FAIL, GW_WARNING, "BLE Get param failed");
							continue;
						}

						pthread_mutex_lock(&gatewayIdValidateLock);
						/*Prepare GW ID only once or when having invalid GW ID*/
						if(!bleMacUpdated || (g_invalidGatewayId == true) || \
 						    (isMemSame(tempGatewayId, initCloudConfig.gatewayID, MAX_LEN_GATEWAY_ID) != true) || \
						    (strcmp(tempCloudURI, initCloudConfig.url)!=0)
						    ) {
							pthread_mutex_unlock(&gatewayIdValidateLock);
							retCode = getLteIMEINum(tempIMEI);
							if (retCode != GEN_SUCCESS) {
								CAB_DBG(CAB_GW_ERR, "Failed to get IMEI number");
								rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_ERR, "Get IMEI failed ");
							}

							retCode = setConfigModuleParam(SYS_SET_LTE_IMEI_CMD, tempIMEI);
							if (retCode != GEN_SUCCESS) {
								CAB_DBG(CAB_GW_WARNING, "Failed to set IMEI number");
								rDiagSendMessage(BLE_CONFIG_SET_FAIL, GW_WARNING, "BLE set diag failed for SYS_SET_LTE_IMEI_CMD");
							}

  							retCode = diagSetGatewayInfo(SYS_MODEM_IMEI, (void *)tempIMEI);
        						if (retCode != GEN_SUCCESS) {
        						        CAB_DBG(CAB_GW_ERR, "DIAG GW Info for SIM IMEI failed with code %d", retCode);
        						}

							retCode = diagSetGatewayInfo(SYS_GATEWAY_UNIQUE_ID, (void *)tempGatewayId);
							if (retCode != GEN_SUCCESS) {
								CAB_DBG(CAB_GW_ERR, "Dig GW ID set info failed with code %d", retCode);
								continue;
							}

							retCode = getConfigModuleParam(GATEWAY_ID, tempGatewayId);
							if (retCode != GEN_SUCCESS) {
								CAB_DBG(CAB_GW_WARNING, "get config GW ID failed with error %d", retCode);
								rDiagSendMessage(BLE_CONFIG_GET_FAIL, GW_WARNING, "BLE Get param failed");
								continue;
							}

							CAB_DBG(CAB_GW_DEBUG, "cloud param updated");
							memcpy(initCloudConfig.url, tempCloudURI, sizeof(initCloudConfig.url));
							memcpy(initCloudConfig.gatewayID, tempGatewayId, sizeof(initCloudConfig.gatewayID));
							retCode = validateGWID(initCloudConfig.gatewayID);
							if(retCode != GEN_SUCCESS) {
								CAB_DBG(CAB_GW_ERR, "Gateway ID validation fail with error %d", retCode);
								// If validate GW ID become fail, request latest Gateway ID
								pthread_mutex_lock(&gatewayIdValidateLock);
								g_invalidGatewayId = true;
								pthread_mutex_unlock(&gatewayIdValidateLock);
								getBleMacAdd();
								sleep(1);
							}

							rDiagSendInitConf(MAX_SDC_ZIP, initCloudConfig.url, initCloudConfig.gatewayID, initSoftwareVersion);
							retCode = cloudInit(&initCloudConfig, initSoftwareVersion, (cloudConStatusNotifCB)cloudNotifycallback);
							if(retCode != GEN_SUCCESS) {
								CAB_DBG(CAB_GW_ERR, "initCloud fail with error %d", retCode);
							}
							bleMacUpdated = 1;
						}
						pthread_mutex_unlock(&gatewayIdValidateLock);

					}
					break;
			        case BLE_PASSKEY:
					{
						/* TODO: This is for future use */
						CAB_DBG(CAB_GW_INFO, "BLE Passkey updated");
					}
					break;


				case SELF_DIAG_REQ:
					{
						if ((getState() != SELF_DIAG_STATE) && (getState() != INIT_STATE)) {
							retCode = getConfigModuleParam(SELF_DIAG_GW_REQ_CMD, (void *)&g_SelfDiagReq);
							if (retCode != GEN_SUCCESS) {
								CAB_DBG(CAB_GW_WARNING, "get config %d failed with error %d", tempConfigMsg.changedConfig, retCode);
								rDiagSendMessage(BLE_CONFIG_GET_FAIL, GW_WARNING, "BLE Get param failed");
							}
							setState(SELF_DIAG_STATE);
							sendEvent(SELF_DIAG_EVENT, NULL, 0);
						}
					}
					break;
                case FACTORY_RESET:
                {
#ifdef SOFT_POWER_OFF_EN
                    FILE *fp;
                    time_t tm;
                    
                    CAB_DBG(CAB_GW_INFO, "Storing Soft reboot time in a file\n");
		    fp = fopen(SOFT_PWR_OFF_FILE, "w");
		    if(fp == NULL) {
			    CAB_DBG(CAB_GW_WARNING,"Unable To Open soft power off file");
		    }
		    time(&tm);
		    fprintf(fp, "%d", tm);
		    fclose(fp);
#endif
                    rDiagSendFactoryResetCmd();
                    rDiagStopResponseQueue();
                    indicateSysStatus(ERROR);
                    setState(DEINIT_STATE);

		    pthread_mutex_lock(&resetStateLock);
		    g_resetState = true;
		    pthread_mutex_unlock(&resetStateLock);

		    /* Give time to Delete the files. */
		    sleep(10);
		    /* Don't want to create the syslog zip again so not usig the rebootSystem() function */
		    system("/cabApp/ble/resetble.sh");
		    sleep(120);
		    sync();
		    powerOffEG91();
		    system("reboot");
		    sleep(60);
                }
                break;
                default:
                    break;
			}
		} else {
			CAB_DBG(CAB_GW_WARNING, "mq_rec for %s failed with error %s",  CONFIG_QUEUE_NAME, strerror(errno));
			sprintf(errorMsg, "mq_rec for %s failed with error %s",  CONFIG_QUEUE_NAME, strerror(errno));
			rDiagSendMessage(MESSAGE_QUEUE_OVERFLOW, GW_WARNING, errorMsg);
		}
	}

	retCode = threadDeregister(newWatchdogNode.threadId);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "Config manager thread de-registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "Config manager thread de-registration failed");
        }
	FUNC_EXIT
	return NULL;
}

/****************************************************************************/
/*                          Timer functionalities                           */
/****************************************************************************/
void reloadXmissionIntCount (uint16_t count) 
{
	pthread_mutex_lock(&xMitIntTimerLock);
	xMissionInternalTimerCount = count;
	localTimerCount = 1;
	pthread_mutex_unlock(&xMitIntTimerLock);
}

void alertTimerReset () 
{
	pthread_mutex_lock(&xMitIntTimerLock);
	xMissionAlertTimer = 0;
	pthread_mutex_unlock(&xMitIntTimerLock);
}

void alertTimerLoad () 
{
	pthread_mutex_lock(&xMitIntTimerLock);
	xMissionAlertTimer = 1;
	localTimerCount = 1;
	pthread_mutex_unlock(&xMitIntTimerLock);
}

void getandUpdateXmissionStatus (uint8_t *isExpired)
{
	pthread_mutex_lock(&xMitIntTimerLock);
	if (localTimerCount++ == xMissionInternalTimerCount) {
		*isExpired = 1;	
		xMissionAlertTimer = 0;
		localTimerCount = 1;
	} else {
		*isExpired = 0;
	}

	if (xMissionAlertTimer == 1 && getState() == MOVING_STATE) {
		*isExpired = 1;
	}
	pthread_mutex_unlock(&xMitIntTimerLock);
}

void xmissionCB(uint32_t data)
{
	uint8_t expirationCheck = 0;
	xMitEventDataEnum_e xMitOperation = XMIT_DATA;
	getandUpdateXmissionStatus(&expirationCheck);
	if (expirationCheck == 1) {
	        sendEvent(XMIT_CHECK, (void *)&xMitOperation, sizeof(xMitEventDataEnum_e));
	}
}

void batCheckCB(uint32_t data)
{
        sendEvent(BATT_CHECK, NULL, 0);
}

void lteCheckCB(uint32_t data)
{
        sendEvent(LTE_CHECK, NULL, 0);
}

void diagCheckCB(uint32_t data)
{
        sendEvent(DIAG_CHECK, NULL, 0);
}

void leakDetCheckCB(uint32_t data)
{
        sendEvent(LEAK_DET_CHECK, NULL, 0);
}
void simDetCheckCB(uint32_t data)
{
        sendEvent(SIM_DET_CHECK, NULL, 0);
}

static void resetBLEModule(void)
{
	FUNC_ENTRY
	system("/cabApp/ble/resetble.sh");
	FUNC_EXIT
}

void watchDogTimerCB(uint32_t data)
{
	FUNC_ENTRY
	watchDogTimer_t *localWatchDogTimerNode;
	bool status = true;

	CAB_DBG(CAB_GW_DEBUG, "WatchDog timer callback from M&T...");
	
	localWatchDogTimerNode = getWatchDogTimerHead();

	// TODO need to check exact solution for 2 times, not fit in for all threads
	while(localWatchDogTimerNode != NULL)
	{
		CAB_DBG(CAB_GW_INFO, "thread id from M&T is %ld...", localWatchDogTimerNode->threadId);
		status = localWatchDogTimerNode->CallbackFn();
		if( status == false )
		{
                        CAB_DBG(CAB_GW_ERR, "Reboot the system due to issue with thread id:%ld from CabAppA7...", localWatchDogTimerNode->threadId);
			rebootSystem();
		}
		localWatchDogTimerNode = localWatchDogTimerNode->next;

	}
	
	CAB_DBG(CAB_GW_DEBUG, "Exiting from WatchDog timer callback...");
	FUNC_EXIT
}

void zipPrepCB(uint32_t data)
{
	xMitEventDataEnum_e xMitOperation = ZIP_DATA;
        sendEvent(XMIT_CHECK, &xMitOperation, sizeof(xMitEventDataEnum_e));
}

/* Start all timers */
static returnCode_e startSysTimers ()
{
	CAB_DBG(CAB_GW_INFO, "Starting the system timers");
	returnCode_e retCode = GEN_SUCCESS;
	batDM_Data_t tempBatDMData;
	TIMER_STATUS_e timerRetSTat = TIMER_SUCCESS;
	TIMER_INFO_t xMissionTimerInfo = { \
			.count = XMISSION_MOVING_TIMER_COUNT, \
			.funcPtr = xmissionCB, \
			.data = 0xA5};
	TIMER_INFO_t batcheckTimerInfo = { \
			.count = BATT_CHECK_TIMER_COUNT, \
			.funcPtr = batCheckCB, \
			.data = 0xA6};
	TIMER_INFO_t lteCheckTimerInfo = { \
			.count = LTE_CHECK_TIMER_COUNT, \
			.funcPtr = lteCheckCB, \
			.data = 0xA7};
	TIMER_INFO_t diagCheckTimerInfo = { \
			.count = DIAG_CHECK_TIMER_COUNT, \
			.funcPtr = diagCheckCB, \
			.data = 0xA8};
	TIMER_INFO_t zipPrepareTimerInfo = { \
			.count = ZIP_PREPARE_TIMER_COUNT, \
			.funcPtr = zipPrepCB, \
			.data = 0xA0};
	TIMER_INFO_t leakDetTimerInfo = { \
			.count = TIRE_LEAK_MEAS_TIME_COUNT, \
			.funcPtr = leakDetCheckCB, \
			.data = 0xA1};
	TIMER_INFO_t simDetectTimerInfo = { \
			.count = SIM_DET_CHK_COUNT, \
			.funcPtr = simDetCheckCB, \
			.data = 0xA1};
	TIMER_INFO_t watchDogTimerInfo = { \
			.count = WATCHDOG_TIMER_COUNT, \
			.funcPtr = watchDogTimerCB, \
			.data = 0xA4};

        retCode = dmHandler->batHandler->getData((void *)&tempBatDMData); 
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Battery get DM data failed with code %d", retCode);
		return MODULE_INIT_FAIL;
	}
        if(tempBatDMData.powerDev == VEHICLE_POWER) {
		xMissionTimerInfo.count = XMISSION_MOVING_TIMER_COUNT;
	 } else {
		xMissionTimerInfo.count = XMISSION_PARKED_TIMER_COUNT;
	}
	timerRetSTat = initSysTimer(TIMER_RESOLUTION_SEC);
	if (timerRetSTat != TIMER_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Init sys timer failed with timer module code : %d", timerRetSTat);
		return MODULE_INIT_FAIL;
	}
	timerRetSTat = startSystemTimer(xMissionTimerInfo, &xMissionHandle);
	if (timerRetSTat != TIMER_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Init sys timer failed with timer module code : %d", timerRetSTat);
		return MODULE_INIT_FAIL;
	}
	timerRetSTat = startSystemTimer(batcheckTimerInfo, &batTimerHandle);
	if (timerRetSTat != TIMER_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Init sys timer failed with timer module code : %d", timerRetSTat);
		return MODULE_INIT_FAIL;
	}
	timerRetSTat = startSystemTimer(lteCheckTimerInfo, &lteTimerHandle);
	if (timerRetSTat != TIMER_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Init sys timer failed with timer module code : %d", timerRetSTat);
		return MODULE_INIT_FAIL;
	}
	timerRetSTat = startSystemTimer(diagCheckTimerInfo, &diagTimerHandle);
	if (timerRetSTat != TIMER_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Init sys timer failed with timer module code : %d", timerRetSTat);
		return MODULE_INIT_FAIL;
	}
	timerRetSTat = startSystemTimer(watchDogTimerInfo, &watchDogTimerHandle);
	if (timerRetSTat != TIMER_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Init sys timer failed with timer module code : %d", timerRetSTat);
		return MODULE_INIT_FAIL;
	}
	timerRetSTat = startSystemTimer(zipPrepareTimerInfo, &zipPrepTimerHandle);
	if (timerRetSTat != TIMER_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Init sys timer failed with timer module code : %d", timerRetSTat);
		return MODULE_INIT_FAIL;
	}
	timerRetSTat = startSystemTimer(leakDetTimerInfo, &leakDetTimerHandle);
	if (timerRetSTat != TIMER_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Init sys timer failed with timer module code : %d", timerRetSTat);
		return MODULE_INIT_FAIL;
	}
	timerRetSTat = startSystemTimer(simDetectTimerInfo, &simDetectTimerHandle);
	if (timerRetSTat != TIMER_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Init sys timer for sim detect check failed with timer module code : %d", timerRetSTat);
		return MODULE_INIT_FAIL;
	}
}

static returnCode_e messageQueueSetup()
{
	FUNC_ENTRY
	returnCode_e retCode = GEN_SUCCESS;

	CAB_DBG(CAB_GW_INFO, "Setting up mesage queues");

	retCode = eventQueueSetup();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "eventQueueSetup() fail with error %d", retCode);
		return retCode;
	}

	retCode = TPMSMsgQueueSetup();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "TPMSMsgQueueSetup() fail with error %d", retCode);
		return retCode;
	}

	retCode = configQueueSetup();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "configQueueSetup() fail with error %d", retCode);
		return retCode;
	}

	FUNC_EXIT
	return retCode;
}
/****************************************************************************/


static returnCode_e spawnThreads()
{
	FUNC_ENTRY
	returnCode_e retCode = GEN_SUCCESS;

	pthread_create(&tpmsThreadID_t, NULL, &recvRFDataFromDMHandler, NULL);
	pthread_create(&configThreadID_t, NULL, &configManager, NULL);
	pthread_create(&eventThreadID_t, NULL, &eventManager, NULL);

	FUNC_EXIT
	return retCode;
}


static void stopTimers()
{
	FUNC_ENTRY

	CAB_DBG(CAB_GW_INFO, "Deleting the system timers");

	deleteTimer(&xMissionHandle);
	deleteTimer(&batTimerHandle);
	deleteTimer(&lteTimerHandle);
	deleteTimer(&diagTimerHandle);
	deleteTimer(&watchDogTimerHandle);
	deleteTimer(&zipPrepTimerHandle);
	deleteTimer(&leakDetTimerHandle);
	deleteTimer(&simDetectTimerHandle);

	FUNC_EXIT
}

void mnt_lteCallback(lteCBData_t *data)
{
	returnCode_e retCode = GEN_SUCCESS;
	otaProgressState_e OTAProgressStatus_initial = OTA_NO_PROGRESS;
	bool isMandatoryDataAvailable = true;
	FUNC_ENTRY
	if (data == NULL) {
		return;
	}

	switch(data->eventType) {
		case CONNECTED:
			CAB_DBG(CAB_GW_INFO, "LTE Connected");
			otaNotifier();

			/* Only need to perform power ON time OTA check once any cloud transaction is happened. */
			if (!isInitialOtaCheckPerformed)
			{
				getConfigModuleParam(MANDATORY_FLAG, &isMandatoryDataAvailable);
				if(!isMandatoryDataAvailable && getState() != DEINIT_STATE && getState() != SELF_DIAG_STATE)
				{
					CAB_DBG(CAB_GW_INFO, "Performing initialization time OTA.");
					sendEvent(OTA_CHECK, (void *) &OTAProgressStatus_initial, sizeof(otaProgressState_e));
					isInitialOtaCheckPerformed = true;
				}
			}
			break;
		case DISCONNECTED:
			if (getState() != DEINIT_STATE) {
				CAB_DBG(CAB_GW_INFO, "Restarting LTE");
				disconnectLTE();
				retCode = connectLTE();
				if (retCode != GEN_SUCCESS) {
					CAB_DBG(CAB_GW_WARNING, "Connect LTE failed with code %d", retCode);
					rDiagSendMessage(LTE_CONNECT_FAIL, GW_WARNING, "LTE connect failed");
				}
			}
			break;
		default:
			break;
	}
	FUNC_EXIT
	return;
}

void SDCcallback (void * data)
{
	CAB_DBG(CAB_GW_DEBUG, "\r\n----SDC CB-----\r\n");
}

void batDMDataCB(void *batData)
{
	FUNC_ENTRY
	powerSwitchEvent_e pwrEvent;

	if (!batData){	
		CAB_DBG(CAB_GW_ERR, "Invalid argument");
		return;
	}
       	
	pwrEvent = *((powerSwitchEvent_e *) batData);

	CAB_DBG(CAB_GW_DEBUG, "%s() Bat CB   | %d", __FUNCTION__, pwrEvent);
	if(pwrEvent == BAT_TO_VEH)
	{
		sendEvent(BATT_TO_VEH, NULL, 0);
	}
	else if(pwrEvent == VEH_TO_BAT)
	{
		sendEvent(VEH_TO_BATT, NULL, 0);
	}
	else if(pwrEvent == LOW_BAT)
	{
		sendEvent(LOW_BATT, NULL, 0);
	}

	FUNC_EXIT
}

static returnCode_e deInitDMAndIntf()
{
	FUNC_ENTRY
	returnCode_e retCode = GEN_SUCCESS;

	if (dmHandler == NULL) {
		CAB_DBG(CAB_GW_ERR, "DM is not initialized yet");
		return retCode;
	}

	retCode = removeDeviceInterface(CGW_RF_INTF, dmHandler->rfIntfHandler);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "Remove interface for RF fail with error %d", retCode);
	}

	FUNC_EXIT
	return retCode;
}

static returnCode_e initDMAndIntf(sensorVendorType_e sensorMake)
{
	returnCode_e retCode = GEN_SUCCESS;
    	rfInitConfig_t rfConfig;
	batInitCfg_t batConfig;

    	rfConfig.maxDeviceSupported = MAX_TPMS;
	rfConfig.sensorVendorType = sensorMake;

	retCode = initDeviceManager(&dmHandler);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "initDeviceManager fail with error %d", retCode);
		rDiagSendMessage(DEVMGR_INIT_FAIL, GW_ERR, "DevMgr core init failed");
		return retCode;
	}
 
	retCode = addDeviceInterface(CGW_RTC_INTF, dmHandler->rtcHandler, NULL, NULL);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "Add interface for RTC fail with error %d", retCode);
		rDiagSendMessage(DEVMGR_INIT_FAIL, GW_ERR, "DevMgr RTC init failed");
		return retCode;
	}

	retCode = addDeviceInterface(CGW_GPS_INTF, dmHandler->gpsHandler, NULL, NULL);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "Add interface for GPS fail with error %d", retCode);
		rDiagSendMessage(DEVMGR_INIT_FAIL, GW_ERR, "DevMgr GPS init failed");
		return retCode;
	}

	retCode = addDeviceInterface(CGW_RF_INTF, dmHandler->rfIntfHandler, (void *)&rfConfig, DmRfDataCB);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "Add interface for RF fail with error %d", retCode);
		rDiagSendMessage(DEVMGR_INIT_FAIL, GW_ERR, "DevMgr RF init failed");
		return retCode;
	}

	retCode = getConfigModuleParam(LOW_BATTERY_THRESHOLD, &batConfig.lowBatThreshold);
	if (retCode != GEN_SUCCESS) 
	{
		CAB_DBG(CAB_GW_ERR, "Get low battery threashold failed with code %d", retCode);
		rDiagSendMessage(DEVMGR_INIT_FAIL, GW_ERR, "Get battery threshold failed");
		return retCode;
	}

	retCode = addDeviceInterface(CGW_BAT_INTF, dmHandler->batHandler, (void *)&batConfig, batDMDataCB);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "Add interface for bat fail with error %d", retCode);
		rDiagSendMessage(DEVMGR_INIT_FAIL, GW_ERR, "BAT init failed");
		return retCode;
	}

	return retCode;
}


static returnCode_e systemModulesInit()
{
	FUNC_ENTRY
	returnCode_e retCode = GEN_SUCCESS;
        char softwareversion[20+MAX_LEN_SOFTWARE_VERSION] = {0};
        char vfilenamecmd[40+MAX_LEN_SOFTWARE_VERSION] = {0};
        char vfilename[35+MAX_LEN_SOFTWARE_VERSION] = {0};

	retCode = initUIModule(NULL);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "initUI fail with error %d", retCode);
		rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_ERR, "UI module init failed");
		return retCode;
	}


	retCode = initConfigModule( (updatedConfigNotifCB)configChangeCallback );
	if(retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "initConfigModule failed with %d", retCode);
		rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_ERR, "Config module init failed");
		return retCode;
	}

	/* init diag module -  allocate memory to store diag data.*/
	retCode = initDiagModule((notifyDiagGetEvent)notifyDiagCheckEv);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "initDiag fail with error %d", retCode);
		rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_ERR, "Diag module init failed");
		return retCode;
	}

	memset(initHardwareVersion, 0, sizeof(initHardwareVersion));
	retCode = getHardwareVersion(initHardwareVersion);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "getHardwareVersion fail with error %d", retCode);
		rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_ERR, "Get Hardware version failed");
		return retCode;
	}

	retCode = getSensorVendorType(&sensorMake);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "getSensorVendorType fail with error %d", retCode);
		rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_ERR, "Get Sensor Vendor Type failed");
		return retCode;
	}

	retCode = setVendorTypeTemp(sensorMake);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "Set Vendor type fail with error %d", retCode);
		rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_ERR, "Set Vendor Type failed");
		return retCode;
	}

	CAB_DBG(CAB_GW_INFO, "Cab gateway HW version : %s", initHardwareVersion);
	retCode = setConfigModuleParam(SYS_SET_HARDWARE_VERSION, (void *)initHardwareVersion);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "setHwVersion fail with error %d", retCode);
		rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_ERR, "Set HW version config failed");
		return retCode;
	}

	retCode = diagSetGatewayInfo(SYS_HARDWARE_VERSION, (void *)initHardwareVersion);
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Dig HW version set info failed with code %d", retCode);
		rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_ERR, "Set HW version daig failed");
		return retCode;
	}

	retCode = getConfigModuleParam(SOFTWARE_VERSION, initSoftwareVersion);
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Config get software version failed with code %d", retCode);
		rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_ERR, "Get software version failed");
		return retCode;
	}

        sprintf(vfilename, "/cabApp/v%s", initSoftwareVersion);
        /* if file does not exist, then create it */
        if( access((char const*)vfilename, F_OK) != 0 ) {
            CAB_DBG(CAB_GW_INFO,"Creating FW version file v%s\n", initSoftwareVersion);
            sprintf(vfilenamecmd, "touch %s", vfilename);
            system(vfilenamecmd);
            system("sync");
        }

	retCode = diagSetGatewayInfo(SYS_SOFTWARE_VERSION, (void *)initSoftwareVersion);
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Dig SW version set info failed with code %d", retCode);
		rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_ERR, "Diag set software version failed");
		return retCode;
	}
	CAB_DBG(CAB_GW_INFO, "Cab gateway SW version : %s", initSoftwareVersion);

	// vendortype and max TPMS supported as info
	retCode = initDMAndIntf(sensorMake);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "initDMAndIntf fail with error %d", retCode);
		return retCode;
	}

	retCode = initSDC(MAX_SDC_ZIP, (uploadzipNotifCB)zipUploadNotifycallback, initSoftwareVersion, 1234, DIR_PATH_DATA_STORAGE);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "initSDC fail with error %d", retCode);
		rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_ERR, "Init SDC module failed");
                indicateSysStatus(ERROR);
		return retCode;
	}


	retCode = initCfgStorageModule(MAX_SDC_ZIP, initSoftwareVersion, DIR_PATH_CFG_DATA);
	if (retCode != GEN_SUCCESS) 
	{
		CAB_DBG(CAB_GW_ERR, "init cfg storage failed with error code %d", retCode);
		rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_ERR, "init cfg storage failed");
		return retCode;
	}
	retCode = initLTE(mnt_lteCallback);
	if (retCode != GEN_SUCCESS) {
		if (retCode == MODULE_ALREADY_INIT) {
			CAB_DBG(CAB_GW_WARNING, "Init LTE is already done");
			retCode = GEN_SUCCESS;
		} else {
			CAB_DBG(CAB_GW_ERR, "Failed to init LTE");
		        indicateSysStatus(ERROR);
			return retCode;
		}
	}

	swOverInit();
	return retCode;
}

returnCode_e monTelemetryDeinit()
{
	FUNC_ENTRY
	tpmsDMData_t localDMDummyData;
	returnCode_e retCode = GEN_SUCCESS;


	signalDeRegister();
    setState(DEINIT_STATE);
	sendEvent(INIT_DONE, NULL, 0);
	pthread_join(eventThreadID_t, NULL);
	systemModulesDeinit();
	DmRfDataCB(&localDMDummyData);
	configChangeCallback(BLE_DISCONNECTED, NULL);
	pthread_join(tpmsThreadID_t, NULL);
    pthread_mutex_lock(&resetStateLock);
    if( g_resetState ) {
        deleteConfigAndDataStorage(DATE_STORAGE_FOLDER);
        deleteConfigAndDataStorage(DATA_CFG_FOLDER);
        deleteConfigAndDataStorage(CFG_STORAGE_FOLDER);
        deleteConfigAndDataStorage(SYSLOG_FOLDER);
        deleteConfigAndDataStorage(OTA_ENV_DIR_PATH);
    }
    pthread_mutex_unlock(&resetStateLock);
	pthread_join(configThreadID_t, NULL);

	pthread_mutex_destroy(&resetStateLock);
	pthread_mutex_destroy(&statLock);
	pthread_mutex_destroy(&rssiLock);
	pthread_mutex_destroy(&sub1rxLock);
	pthread_mutex_destroy(&keepAliveLock);
	pthread_mutex_destroy(&otaNotifierLock);
	pthread_mutex_destroy(&simDetCntVarLock);
	pthread_mutex_destroy(&xMitIntTimerLock);
	pthread_mutex_destroy(&otaStateLock);
	pthread_mutex_destroy(&bleStateLock);
	pthread_mutex_destroy(&leakDetectMutex);
	pthread_mutex_destroy(&alertRecordMutex);
	pthread_mutex_destroy(&recvRFDataFromDMHandlerThreadAliveLock);
	pthread_mutex_destroy(&configManagerThreadAliveLock);
	pthread_mutex_destroy(&eventManagerThreadAliveLock);
	pthread_mutex_destroy(&otaManagerThreadAliveLock);
	pthread_mutex_destroy(&otaDownloadLock);
	pthread_mutex_destroy(&otaDeatailsAvailableLock);

	/* queue cleanup */
	mq_close(mqTPMSQueue);
	mq_unlink(TPMS_QUEUE_NAME);
	mq_close(mqEvQueue);
	mq_unlink(EVENT_QUEUE_NAME);
	mq_close(mqConfigQueue);
	mq_unlink(CONFIG_QUEUE_NAME);

	if (getOTAState() == OTA_THREAD_RUNNING) {
		sem_post(&otaSem);
		pthread_join(otaThreadID_t, NULL);
	}

	retCode = otaDeInit();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "deinitOTA() fail with error %d", retCode);
	}

	retCode = cloudDeinit();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "deinitCloud() fail with error %d", retCode);
	}

	retCode = removeDeviceInterface(CGW_GPS_INTF, dmHandler->gpsHandler);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "Remove interface for GPS fail with error %d", retCode);
	}

	retCode = deInitLTE();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "deInitLTE() fail with error %d", retCode);
	}
	
	retCode = removeDeviceInterface(CGW_RTC_INTF, dmHandler->rtcHandler);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "Remove interface for RTC fail with error %d", retCode);
	}

	retCode = removeDeviceInterface(CGW_BAT_INTF, dmHandler->batHandler);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "Remove interface for battery fail with error %d", retCode);
	}
	retCode = deInitDeviceManager(dmHandler);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "deInitDeviceManager() fail with error %d", retCode);
	}
	dmHandler = NULL;
	pthread_mutex_destroy(&otaStateLock);
	pthread_mutex_destroy(&bleStateLock);
	sem_destroy(&otaSem);


	CAB_DBG(CAB_GW_INFO, "=== M&T Closed ===");
	FUNC_EXIT
	return retCode;
}

returnCode_e monTelemetryInit()
{
	FUNC_ENTRY
	returnCode_e retCode = GEN_SUCCESS;
	batDM_Data_t tempBatDMData;
	cloudInitInfo_t initCloudConfig;
	bool cloudConnectionStatus = false;

	uint8_t tempIMEI[MAX_LEN_LTE_IMEI] = {'\0'};
        char tempSimIccid[MAX_LEN_SIM_ICCID] = {'\0'};
	uint8_t tempGatewayId[MAX_LEN_GATEWAY_ID] = {0};
	char tempCloudURI[MAX_LEN_CLOUD_URI] = "";
	alertCount = 0;
	uint8_t index = 0;
	uint8_t present = 0;
	char simNotPStr[] = NO_SIM_PRESENT_STR;
	time_t rtcStartTime, rtcRefTime, internetCheckTime;

	memset(&initCloudConfig, 0, sizeof(cloudInitInfo_t));
	CAB_DBG(CAB_GW_INFO, "=== M&T Started ===");
	setState(INIT_STATE);
	memset(alertRecord, 0, sizeof(alertRecord));
	memset(alertRecordPSI, 0, sizeof(alertRecordPSI));

	retCode = otaInit();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "initOTA fail with error %d", retCode);
		rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_ERR, "OTA error");
                indicateSysStatus(ERROR);
		return retCode;
	}

	retCode = initOTAEnv();
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "initOTAEnv fail with error %d", retCode);
                rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_ERR, "OTA error");
                indicateSysStatus(ERROR);
                return retCode;
        }

	/*Check previous OTA type. In case of incremental OTA, start incremental OTA
	check timer*/
	retCode = checkPreviousOTAType(&previousOTA_full_updated);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "Failed to check previous OTA type with error %d", retCode);
                indicateSysStatus(ERROR);
                return retCode;
        }

	retCode = signalRegister();
	if(retCode != GEN_SUCCESS) 
	{
		CAB_DBG(CAB_GW_ERR, "signal register failed with code %d", retCode);
		indicateSysStatus(ERROR);
		return retCode;
	}

	if (pthread_mutex_init(&statLock, NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "Status mutex init failed");
                indicateSysStatus(ERROR);
		return MODULE_INIT_FAIL;
	}

	if (pthread_mutex_init(&rssiLock, NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "RSSI flag mutex init failed");
                indicateSysStatus(ERROR);
		return MODULE_INIT_FAIL;
	}

	if (pthread_mutex_init(&sub1rxLock, NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "sub1 rx mutex init failed");
                indicateSysStatus(ERROR);
		return MODULE_INIT_FAIL;
	}

	if (pthread_mutex_init(&keepAliveLock, NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "keep alive mutex init failed");
                indicateSysStatus(ERROR);
		return MODULE_INIT_FAIL;
	}

	if (pthread_mutex_init(&otaNotifierLock, NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "OTA notify mutex init failed");
                indicateSysStatus(ERROR);
		return MODULE_INIT_FAIL;
	}

	if (pthread_mutex_init(&simDetCntVarLock, NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "SIM det cnt var mutex init failed");
                indicateSysStatus(ERROR);
		return MODULE_INIT_FAIL;
	}

	if (pthread_mutex_init(&xMitIntTimerLock, NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "XMIT mutex init failed");
                indicateSysStatus(ERROR);
		return MODULE_INIT_FAIL;
	}
	
	if (pthread_mutex_init(&otaStateLock, NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "OTA mutex init failed");
                indicateSysStatus(ERROR);
		return MODULE_INIT_FAIL;
	}
	
	if (pthread_mutex_init(&bleStateLock, NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "BLE State init failed");
                indicateSysStatus(ERROR);
		return MODULE_INIT_FAIL;
	}

	if (pthread_mutex_init(&leakDetectMutex, NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "Leak det mutex init failed");
                indicateSysStatus(ERROR);
		return MODULE_INIT_FAIL;
	}
	
	if (pthread_mutex_init(&alertRecordMutex, NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "Alert Det mutex init failed");
                indicateSysStatus(ERROR);
		return MODULE_INIT_FAIL;
	}

	if (pthread_mutex_init(&recvRFDataFromDMHandlerThreadAliveLock, NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "Recv RF Data From DM Thread mutex init failed");
                indicateSysStatus(ERROR);
		return MODULE_INIT_FAIL;
	}

	if (pthread_mutex_init(&configManagerThreadAliveLock, NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "Config Manager Thread mutex init failed");
                indicateSysStatus(ERROR);
		return MODULE_INIT_FAIL;
	}

	if (pthread_mutex_init(&eventManagerThreadAliveLock, NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "Event Manager Thread mutex init failed");
                indicateSysStatus(ERROR);
		return MODULE_INIT_FAIL;
	}

	if (pthread_mutex_init(&otaManagerThreadAliveLock, NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "OTA Manager Thread mutex init failed");
                indicateSysStatus(ERROR);
		return MODULE_INIT_FAIL;
	}

	if (pthread_mutex_init(&otaDownloadLock, NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "OTA Download mutex init failed");
                indicateSysStatus(ERROR);
		return MODULE_INIT_FAIL;
	}

	if (pthread_mutex_init(&otaDeatailsAvailableLock, NULL) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "OTA Details Available mutex init failed");
		indicateSysStatus(ERROR);
		return MODULE_INIT_FAIL;
	}

    if (pthread_mutex_init(&resetStateLock, NULL) != 0)
    {
        CAB_DBG(CAB_GW_ERR, "Reset State mutex init failed");
        indicateSysStatus(ERROR);
        return MODULE_INIT_FAIL;
    }

	if (sem_init(&otaSem, 0, 0) != 0)
	{
		CAB_DBG(CAB_GW_ERR, "OTA semaphore init failed");
                indicateSysStatus(ERROR);
		return MODULE_INIT_FAIL;
	}

	retCode = messageQueueSetup();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "messageQueueSetup() fail with error %d", retCode);
                indicateSysStatus(ERROR);
		return retCode;
	}

	retCode = spawnThreads();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "spawnThreads() fail with error %d", retCode);
                indicateSysStatus(ERROR);
		return retCode;
	}

	retCode = tpmsInit();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "tpmsInit() fail with error %d", retCode);
                indicateSysStatus(ERROR);
		return retCode;
	}
	retCode = systemModulesInit();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "systemModulesInit() fail with error %d", retCode);
                indicateSysStatus(ERROR);
		return retCode;
	}

	rDiagStartResponseQueue();

	/*If RTC time < 1st Jan'18, RTC not configured OR coin cell not plugged */
        retCode = dmHandler->rtcHandler->getData((void *)(&rtcStartTime));
        if (retCode != GEN_SUCCESS) {
                CAB_DBG(CAB_GW_ERR, "get time data failed with error %d", retCode);
                return retCode;
        }
	rtcRefTime = 1514764800; //Monday, January 1, 2018 12:00:00 AM
	if(rtcStartTime < rtcRefTime){
		CAB_DBG(CAB_GW_WARNING, "RTC not configured OR RTC coin cell is missing");
		index = 0;//reusing existing variable
		retCode = setConfigModuleParam(SYS_RTC_FLAG, &index);
                if (retCode != GEN_SUCCESS) {
                      CAB_DBG(CAB_GW_WARNING, "set RTC flag failed with error %d", retCode);
		      rDiagSendMessage(BLE_CONFIG_SET_FAIL, GW_WARNING, "BLE set diag failed for SYS_RTC_FLAG");
                      return retCode;
                }
		initRTCCoinCellStatus = false;
		memcpy(tempBatDiagData.coinCellStatus,coincellUnplugStatus, sizeof(coincellUnplugStatus));
	}	
	else{
		initRTCCoinCellStatus = true;
		memcpy(tempBatDiagData.coinCellStatus,coincellPlugStatus, sizeof(coincellPlugStatus));
	}
	powerStartTime = rtcStartTime;
	tempBatDiagData.startTime = rtcStartTime; 

	/* Check initial battery condition */
	retCode = dmHandler->batHandler->getData((void *)&tempBatDMData); 
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "Battery get data failed with code %d", retCode);
		return retCode;
	}

	if(tempBatDMData.powerDev == BACKUP_BATTERY)
	{
		if(tempBatDMData.batHealth == LOW_BAT)
		{
			CAB_DBG(CAB_GW_ERR, "Low battery detected. Cann't proceed");
                	indicateSysStatus(ERROR);
			return GEN_SUCCESS;
		}
		memcpy(tempBatDiagData.powerSource,batPowerSource, sizeof(batPowerSource));
	}
	else{
		memcpy(tempBatDiagData.powerSource,vehPowerSource, sizeof(vehPowerSource));
	}
	retCode = diagSetBatRtcData((void *)&tempBatDiagData);
	if (retCode != GEN_SUCCESS) {
                CAB_DBG(CAB_GW_ERR, "DIAG Set Bat RTC Info failed with code %d", retCode);
                indicateSysStatus(ERROR);
                return retCode;
        }
		
	retCode = getLteIMEINum(tempIMEI);
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_ERR, "Failed to get IMEI number");
                indicateSysStatus(ERROR);
		return retCode;
	}

	retCode = setConfigModuleParam(SYS_SET_LTE_IMEI_CMD, tempIMEI);
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_WARNING, "Failed to set IMEI number");
		rDiagSendMessage(BLE_CONFIG_SET_FAIL, GW_WARNING, "BLE set diag failed for SYS_SET_LTE_IMEI_CMD");
                indicateSysStatus(ERROR);
		return retCode;
	}

  	retCode = diagSetGatewayInfo(SYS_MODEM_IMEI, (void *)tempIMEI);
        if (retCode != GEN_SUCCESS) {
                CAB_DBG(CAB_GW_ERR, "DIAG GW Info for SIM IMEI failed with code %d", retCode);
                indicateSysStatus(ERROR);
		return retCode;
        }


	memset(tempSimIccid, 0, sizeof(tempSimIccid));
	retCode = checkSIMDetection(&present);
	if (retCode != GEN_SUCCESS){
		CAB_DBG(CAB_GW_ERR, "checkSIMDetection failed with code %d", retCode);
		setSimDetFailCnt(getSimDetFailCnt() + 1);
	} else {
		if (present > 1) {
			setSimDetFailCnt(0);
			retCode = getLteICCIDNum(tempSimIccid);
			if (retCode != GEN_SUCCESS) {
				CAB_DBG(CAB_GW_WARNING, "Failed to get ICCID number with code : %d", retCode);
				rDiagSendMessage(CABAPP_SIM_ICCID_NOT_AVAIL, GW_WARNING, "SIM ICCID get API failed");
			}
	
			retCode = setConfigModuleParam(SYS_SET_SIM_ICCID_CMD, tempSimIccid);
			if (retCode != GEN_SUCCESS) {
				CAB_DBG(CAB_GW_WARNING, "Failed to set IMEI number with code %d", retCode);
				rDiagSendMessage(CABAPP_SIM_ICCID_NOT_AVAIL, GW_WARNING, "SIM ICCID set config failed ");
			}
		} else {
			memcpy(tempSimIccid, simNotPStr, sizeof(simNotPStr));
			setSimDetFailCnt(getSimDetFailCnt() + 1);
		}
	}

	
  	retCode = diagSetGatewayInfo(SYS_SIM_ICCID, (void *)tempSimIccid);
        if (retCode != GEN_SUCCESS) {
                CAB_DBG(CAB_GW_WARNING, "DIAG GW Info for SIM ICCID failed with code %d", retCode);
		rDiagSendMessage(CABAPP_SIM_ICCID_NOT_AVAIL, GW_WARNING, "SIM ICCID get API failed");
        }

	retCode = getConfigModuleParam(GATEWAY_ID, tempGatewayId);
        if (retCode != GEN_SUCCESS) {
                CAB_DBG(CAB_GW_WARNING, "Config get gw ID failed with code %d", retCode);
        } else {
  		retCode = diagSetGatewayInfo(SYS_GATEWAY_UNIQUE_ID, (void *)tempGatewayId);
        	if (retCode != GEN_SUCCESS) {
        	        CAB_DBG(CAB_GW_WARNING, "DIAG GW info set for GW ID failed with code %d", retCode);
			rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_WARNING, "Set Diag GWID failed");
        	        indicateSysStatus(ERROR);
			return retCode;
        	}
	}

	retCode = getConfigModuleParam(CLOUD_URI, tempCloudURI);
        if (retCode != GEN_SUCCESS) {
                CAB_DBG(CAB_GW_WARNING, "Config get cloud URI failed with code %d", retCode);
		rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_WARNING, "Get Cloud URI failed");
                indicateSysStatus(ERROR);
		return retCode;
        }

  	retCode = diagSetGatewayInfo(SYS_CLOUD_URI, (void *)tempCloudURI);
        if (retCode != GEN_SUCCESS) {
                CAB_DBG(CAB_GW_WARNING, "DIAG GW info set for GW ID failed with code %d", retCode);
		rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_WARNING, "Set diag GW ID failed");
                indicateSysStatus(ERROR);
		return retCode;
        }

	retCode = connectLTE();
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_WARNING, "Failed to get connect to LTE");
		rDiagSendMessage(LTE_CONNECT_FAIL, GW_WARNING, "Connect to LTE failed");
	}

	retCode = getConfigModuleParam(GATEWAY_ID, tempGatewayId);
	if (retCode == GEN_SUCCESS) {
		memcpy(initCloudConfig.url, tempCloudURI, sizeof(initCloudConfig.url));
		memcpy(initCloudConfig.gatewayID, tempGatewayId, sizeof(initCloudConfig.gatewayID));
		retCode = validateGWID(initCloudConfig.gatewayID);
		if(retCode != GEN_SUCCESS) {
						CAB_DBG(CAB_GW_WARNING, "Gateway ID validation fail with error %d", retCode);
						// If validate GW ID become fail, request latest Gateway ID
						pthread_mutex_lock(&gatewayIdValidateLock);
						g_invalidGatewayId = true;
						pthread_mutex_unlock(&gatewayIdValidateLock);
						getBleMacAdd();
						sleep(1);
		}

		pthread_mutex_lock(&gatewayIdValidateLock);
		if(g_invalidGatewayId == true)
		{
						pthread_mutex_unlock(&gatewayIdValidateLock);
						retCode = getConfigModuleParam(GATEWAY_ID, tempGatewayId);
						if (retCode != GEN_SUCCESS) {
										CAB_DBG(CAB_GW_ERR, "Get config failed with code %d", retCode);
						}
						memcpy(initCloudConfig.gatewayID, tempGatewayId, sizeof(initCloudConfig.gatewayID));
						pthread_mutex_lock(&gatewayIdValidateLock);
						g_invalidGatewayId = false;

		}
		pthread_mutex_unlock(&gatewayIdValidateLock);

		// Reset the previous cloud connection status
		retCode = setConfigModuleParam(MNT_CLOUD_CONNECTION_STATUS_CMD, &cloudConnectionStatus);
		if (retCode != GEN_SUCCESS) {
			CAB_DBG(CAB_GW_ERR, "Failed to set cloud connection status from M&T");
		}

		retCode = cloudInit(&initCloudConfig, initSoftwareVersion, (cloudConStatusNotifCB)cloudNotifycallback);
		if(retCode != GEN_SUCCESS)
		{
			CAB_DBG(CAB_GW_WARNING, "initCloud fail with error %d", retCode);
			rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_WARNING, "Cloud inti failed");
                	indicateSysStatus(ERROR);
			return retCode;
		}

		/* For the case when the cabapp is terminated, the cabapp starts and does not do any cloud transaction till any event occurs
		 * which performs cloud operation. As a result the cloud connection status is not updated for 15 minutes max. To correct this
		 * get the API token here, instead of requesting later.  */
		retCode = getAPiToken();
		if(retCode == GEN_SUCCESS)
		{
			// if we get the API token that means we have Internet connectivity so update the the config status parameter
			time(&internetCheckTime);
			retCode = setConfigModuleParam(INTERNET_CONNECTIVITY_TIMESTAMP, (void *)&internetCheckTime);
			if (retCode != GEN_SUCCESS) {
				CAB_DBG(CAB_GW_ERR, "Set config failed with code %d", retCode);
			}

			cloudConnectionStatus = true;
			// Set the cloud connection status
			retCode = setConfigModuleParam(MNT_CLOUD_CONNECTION_STATUS_CMD, &cloudConnectionStatus);
			if (retCode != GEN_SUCCESS) {
			CAB_DBG(CAB_GW_ERR, "Failed to set cloud connection status from M&T");
			}
		}
		else
		{
			/* Not considering this as a fail condition as getAPiToken() is added here to get the exact
			 * cloud connection status in case cabapp is terminated. */

			/* In normal condition (i.e. system reboot), the Internet connectivity is not established
			 * this soon, so the call will always fail. Later when Internet connectivity is detected,
			 * the cloud connection is established. */

			cloudConnectionStatus = false;
			// Reset the cloud connection status
			retCode = setConfigModuleParam(MNT_CLOUD_CONNECTION_STATUS_CMD, &cloudConnectionStatus);
			if (retCode != GEN_SUCCESS) {
			CAB_DBG(CAB_GW_ERR, "Failed to set cloud connection status from M&T");
			}
		}
	}

	rDiagSendInitConf(MAX_SDC_ZIP, initCloudConfig.url, initCloudConfig.gatewayID, initSoftwareVersion);

	retCode = startSysTimers();
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_WARNING, "Init sys timer failed with code %d", retCode);
		rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_WARNING, "Start sys timer failed");
                indicateSysStatus(ERROR);
		return retCode;
	}

	retCode = accelInit();
	if (retCode != GEN_SUCCESS) {
		CAB_DBG(CAB_GW_WARNING, "Accel init failed with retCode %d", retCode);
		rDiagSendMessage(CABAPP_LINUX_SYS_INIT_FAIL, GW_WARNING, "Accel init failed");
                indicateSysStatus(ERROR);
		return retCode;
	}

	/* Cause config module to generate notification to config thread*/
	startDeviceConfiguration();
	while(1)
	{
		if(getState() == DEINIT_STATE)
		{
			monTelemetryDeinit();
			rDiagModDeinit();
			rdiagDeinit();
			break;
		}
		sleep(1);
	}

	FUNC_EXIT
	return GEN_SUCCESS;
}

returnCode_e systemModulesDeinit()
{
	FUNC_ENTRY
	returnCode_e retCode = GEN_SUCCESS;

	retCode = deInitDiagModule();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "deinitDiag() fail with error %d", retCode);
		return retCode;
	}

	stopTimers();


	retCode = deInitDMAndIntf();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "deInitDMAndIntf() fail with error %d", retCode);
		return retCode;
	}

	retCode = tpmsDeInit();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "tpmsDeInit() fail with error %d", retCode);
		return retCode;
	}

	retCode = deInitSDC();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "deinitSDC() fail with error %d", retCode);
		return retCode;
	}

	retCode = deInitCfgStorageModule();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "deInitCfgStorageModule() fail with error %d", retCode);
		return retCode;
	}

	retCode = bleDeInit();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "deInitBLE() fail with error %d", retCode);
		return retCode;
	}

	retCode = deInitConfigModule();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "deinitConfigModule() fail with error %d", retCode);
		return retCode;
	}
	/* Dont't deintialize the UI module as in case of OTA processing OTA blinking 
	   will not continue once deinitialized */
#if 0
	retCode = deInitUIModule();
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "deinitUI() fail with error %d", retCode);
		return retCode;
	}
#endif
	return retCode;
}
