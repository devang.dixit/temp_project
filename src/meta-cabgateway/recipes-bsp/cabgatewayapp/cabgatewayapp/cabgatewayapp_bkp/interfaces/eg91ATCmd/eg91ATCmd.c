#include "eg91ATCmd.h"
#include "gpio.h"
#include "debug.h"
#include <semaphore.h>

/* Global variables */
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t resetFlagLock = PTHREAD_MUTEX_INITIALIZER;
fd_set sFd;
static int lteFd = -1;
static uint8_t eg91Inited = 0;
struct timeval tv;
/* reset require flag */
static uint8_t resetRequired = 0;
static sem_t *eg91_semlock; 
static uint8_t eg91_semlock_inited = 0;

static void setResetFlag(uint8_t value)
{
	pthread_mutex_lock(&resetFlagLock);
	resetRequired = value;
	pthread_mutex_unlock(&resetFlagLock);
}

static uint8_t getResetFlag()
{
	uint8_t tempFlag = 0;
	pthread_mutex_lock(&resetFlagLock);
	tempFlag = resetRequired;
	pthread_mutex_unlock(&resetFlagLock);
	return tempFlag;
}

returnCode_e Sem_DeInit_eg91()                                          
{                                                                  
	int return_val;                                            
	return_val = sem_close(eg91_semlock);                    
	if (return_val == -1) {                                    
		CAB_DBG(CAB_GW_ERR, "Sem Close Failed");      
		return -1;                                         
	}                                                          

	return_val = sem_unlink("/eg91_sem");                    
	if (return_val == -1) {                                    
		CAB_DBG(CAB_GW_ERR, "Sem Unlink Failed");     
		return -1;                                         
	}                                                          
	return GEN_SUCCESS;
}

returnCode_e semLockWithRetry_eg91()                                                                        
{                                                                                                      
	struct timespec ts;                                                                                
	int count = 1;                                                                                     
	int ret = 0;                                                                                       

	if(eg91_semlock_inited == 0) {
		if ((eg91_semlock = sem_open ("/eg91_sem", O_CREAT, 0660, 1)) == SEM_FAILED) {
			CAB_DBG(CAB_GW_ERR, "sem_open failed");
			return GEN_API_FAIL;
		}
		eg91_semlock_inited = 1;
	}
	do {                                                                                               
		if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {                                            
			return GEN_API_FAIL;                                                               
		}                                                                                          
		ts.tv_sec += 3;                                                                            
		ret = sem_timedwait(eg91_semlock, &ts);                                                  
		count++;                                                                                   
	} while(ret != 0 && count < 11);                                                                   

	if (count == 10) {                                                                                 
		ret  = Sem_DeInit_eg91();                                                                       
		if (ret != 0) {                                                                            
			CAB_DBG(CAB_GW_ERR, "sem deinit failed");                                          
			return GEN_API_FAIL;                                                               
		}                                                                                          

		if ((eg91_semlock = sem_open ("/eg91_sem", O_CREAT, 0660, 1)) == SEM_FAILED) {         
			CAB_DBG(CAB_GW_ERR, "sem_open failed");                                       
			return GEN_API_FAIL;                                                               
		}                                                                                          

		if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {                                            
			return GEN_API_FAIL;                                                               
		}                                                                                          
		ts.tv_sec += 3;                                                                            

		ret = sem_timedwait(eg91_semlock, &ts);                                                  
		if (ret != 0) {                                                                            
			CAB_DBG(CAB_GW_ERR, "sem timed wait failed");                                      
			return GEN_API_FAIL;                                                               
		}                                                                                          
	}                                                                                                  
	return GEN_SUCCESS;                                                                                
}                                                                                                      

returnCode_e powerOnEG91()
{
	uint8_t retryCount = 0;
	returnCode_e retCode = GEN_SUCCESS;
	int retVal;

	retCode = semLockWithRetry_eg91();
	if (retCode == GEN_SUCCESS) {
		// Power up the LTE/GPS module here
		gpioInit(EG91_LDO_ENABLE, OUT);
		gpioInit(EG91_RESET_SWITCH, OUT);
		gpioInit(EG91_PWR_KEY_GPIO, OUT);
		gpioInit(EG91_STATUS_PIN, IN);

		gpioWrite(EG91_LDO_ENABLE, 1);
		gpioWrite(EG91_PWR_KEY_GPIO, 0);
		usleep(500000);
		gpioWrite(EG91_PWR_KEY_GPIO, 1);
		while(retryCount++ < EG91_PWRON_RETRY_CNT) {
			sleep(EG91_PWRON_RETRY_SLEEP);
			retVal = access( EG91_USB_PORT, F_OK );
			if( retVal == 0 ) {
				CAB_DBG(CAB_GW_INFO, "EG91 USB port came up");
				retCode = GEN_SUCCESS;
				break;
			}
		}
		sem_post(eg91_semlock);               
	}
	return retCode;
}

returnCode_e powerOffEG91()
{
	uint8_t retryCount = 0;
	returnCode_e retCode = GEN_SUCCESS;
	int retVal;

	retCode = semLockWithRetry_eg91();
	if (retCode == GEN_SUCCESS) {
    		gpioWrite(EG91_PWR_KEY_GPIO, 0);
    		usleep(700000);
    		gpioWrite(EG91_PWR_KEY_GPIO, 1);
		gpioWrite(EG91_LDO_ENABLE, 0);
		while(retryCount++ < EG91_PWROFF_RETRY_CNT) {
        	        sleep(EG91_PWROFF_RETRY_SLEEP);
        	        retVal = access( EG91_USB_PORT, F_OK );
        	        if( retVal == -1 ) {
				CAB_DBG(CAB_GW_INFO, "EG91 USB port turned down");
				retCode = GEN_SUCCESS;
				break;
			} 
		}

		gpioDeInit(EG91_LDO_ENABLE);
		gpioDeInit(EG91_RESET_SWITCH);
		gpioDeInit(EG91_PWR_KEY_GPIO);
		gpioDeInit(EG91_STATUS_PIN);
		sem_post(eg91_semlock);               
	}
	return retCode;
}

returnCode_e initEG91()
{
	struct termios tty;	/* Create the terminal structure */
	char writeBuff[] = "AT";
	int  readBytes = 0;
	char readBuff[512];
	returnCode_e retCode = GEN_SUCCESS;

	pthread_mutex_lock(&lock);

	if(eg91Inited)
	{
		CAB_DBG(CAB_GW_DEBUG, "EG91 module already Inited. Can't reInit");
		pthread_mutex_unlock(&lock);
		return GEN_SUCCESS;
	}

	// TODO retry logic in open file just for safety
	lteFd = open(EG91_USB_PORT, O_RDWR | O_NOCTTY);
	if(lteFd == -1)
	{
		CAB_DBG(CAB_GW_ERR, "Opening EG91 USB port fail with error: %s", strerror(errno));
	pthread_mutex_unlock(&lock);
		return GEN_SYSCALL_FAIL;
	}

	/*---------- Setting the Attributes of the serial port using termios structure --------- */
	if (tcgetattr(lteFd, &tty) < 0)
	{
        CAB_DBG(CAB_GW_ERR, "Error from tcgetattr: %s", strerror(errno));
	pthread_mutex_unlock(&lock);
        return GEN_SYSCALL_FAIL;
    	}

	cfsetospeed(&tty, (speed_t)9600);
	cfsetispeed(&tty, (speed_t)9600);

	tty.c_cflag |= (CLOCAL | CREAD);      /* ignore modem controls */
	tty.c_cflag |= CS8;      			  /* 8-bit characters */
	tty.c_cflag &= ~CSIZE;
	tty.c_cflag &= ~PARENB;  			  /* no parity bit */
	tty.c_cflag &= ~CSTOPB;  			  /* only need 1 stop bit */
	tty.c_cflag &= ~CRTSCTS; 			  /* no hardware flowcontrol */

	/* setup for non-canonical mode */
	tty.c_iflag &= ~(IXON | IXOFF | IXANY);
	tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);

	/* no local echo: allow the other end to do the echoing */
	tty.c_lflag &= ~ECHO;

	/* fetch bytes as they become available */
	tty.c_cc[VMIN] = 1;
	tty.c_cc[VTIME] = 0;

	if (tcsetattr(lteFd, TCSANOW, &tty) != 0) {
		CAB_DBG(CAB_GW_ERR, "Error from tcsetattr: %s", strerror(errno));
		pthread_mutex_unlock(&lock);
		return GEN_SYSCALL_FAIL;
	}

	pthread_mutex_unlock(&lock);

	retCode = ATTransactEG91(writeBuff, strlen(writeBuff), "OK", readBuff, &readBytes);
	if(retCode != GEN_SUCCESS)
	{
		CAB_DBG(CAB_GW_ERR, "ATTransactEG91() failed during EG91 Init with error %d", retCode);
		return retCode;
	}

	if(!strstr(readBuff, "OK"))
	{
		CAB_DBG(CAB_GW_ERR, "EG91 init failed" );
		return AT_CMD_FAIL;
	}


	pthread_mutex_lock(&lock);
	eg91Inited = 1;
	pthread_mutex_unlock(&lock);
	return retCode;
}

returnCode_e deInitEG91()
{
	returnCode_e retCode = GEN_SUCCESS;

	pthread_mutex_lock(&lock);

	CAB_DBG(CAB_GW_INFO, "Deinitializing EG91 module");

	if(lteFd > 0)
	{
		close(lteFd); /* Close the Serial port */
	}

	eg91Inited = 0;
	pthread_mutex_unlock(&lock);

	pthread_mutex_destroy(&lock);

	return retCode;
}

static void updateEG91ResetStatus()
{
	pthread_mutex_lock(&lock);
        if( access( EG91_USB_PORT, F_OK ) == -1 ) {
		setResetFlag(1);
	} else {
		setResetFlag(0);
	}
	pthread_mutex_unlock(&lock);
}

returnCode_e ATTransactEG91_LongSleep(char *writeBuff, size_t len,  char *pattern, char *readBuff, int *readBytes)
{
	int count = 0;
	int  writtenBytes = 0;
	uint8_t matched = 0;
	returnCode_e retCode = GEN_SUCCESS;
	int retVal = 0;

	if(!writeBuff || !pattern || !readBuff || !readBytes)
	{
		CAB_DBG(CAB_GW_ERR, "Invalid argument");
		return GEN_NULL_POINTING_ERROR;
	}

	pthread_mutex_lock(&lock);

	tcflush(lteFd, TCIFLUSH);   /* Discards old data in the rx buffer */

	//memcpy(tempWriteBuff, writeBuff, len);
	//*(tempWriteBuff+len) = '\n';
	//*(tempWriteBuff+len+1) = '\0';
	//writtenBytes = write(lteFd, tempWriteBuff, len);
	//FD_ZERO(&sFd);
	//FD_SET(lteFd, &sFd);
	//writtenBytes = write(lteFd, writeBuff, len);
	//writtenBytes = write(lteFd, "\n", 1);

	//CAB_DBG(CAB_GW_INFO,"%s\r\n",writeBuff);

	///* Wait up to 1 seconds. */
	//tv.tv_sec = 30;
	//tv.tv_usec = 0;

	while(count++ < 3)
	{
		FD_ZERO(&sFd);
		FD_SET(lteFd, &sFd);
		writtenBytes = write(lteFd, writeBuff, len);
		writtenBytes = write(lteFd, "\n", 1);

		CAB_DBG(CAB_GW_INFO,"%s\r\n",writeBuff);

		tv.tv_sec = 30;
		tv.tv_usec = 0;

		CAB_DBG(CAB_GW_INFO,"Loop*%d, %ld", __LINE__, tv.tv_sec);
		retVal = select(lteFd+1, &sFd, NULL, NULL, &tv);
		CAB_DBG(CAB_GW_INFO,"RetVal = %d", retVal);
		if (retVal == -1) {
			perror("select:");
		}
		if(retVal == 1)
		{
			if(FD_ISSET(lteFd, &sFd))
			{
				*readBytes = read(lteFd, readBuff, 512);
				if(*readBytes < 0)
				{
					CAB_DBG(CAB_GW_ERR, "read fails ... %s", strerror(errno));
					pthread_mutex_unlock(&lock);
					return FILE_READ_ERROR;
				}
				CAB_DBG(CAB_GW_INFO,"%s",readBuff);
				if(strstr(readBuff, pattern))
				{
					matched = 1;
					break;
				}
				if(strstr(pattern, "imei") && (*readBytes >= MAX_IMEI_LEN))
				{
					matched = 1;
					break;
				}
			}
		}
	}
	pthread_mutex_unlock(&lock);
        if (matched == 0) {
		CAB_DBG(CAB_GW_INFO, "AT command failed, checking USB interface");
		*readBytes = 0;
	} 
	updateEG91ResetStatus();

	return retCode;
}

returnCode_e ATTransactEG91_CustomSleep(char *writeBuff, size_t len,  char *pattern, char *readBuff, int *readBytes, uint8_t timeout)
{
        int count = 0;
        int  writtenBytes = 0;
        uint8_t matched = 0;
        returnCode_e retCode = GEN_SUCCESS;
        int retVal = 0;
        struct tm a;

        if(!writeBuff || !pattern || !readBuff || !readBytes)
        {
                CAB_DBG(CAB_GW_ERR, "Invalid argument");
                return GEN_NULL_POINTING_ERROR;
        }

        pthread_mutex_lock(&lock);

        tcflush(lteFd, TCIFLUSH);   /* Discards old data in the rx buffer */

        writtenBytes = write(lteFd, writeBuff, len);
        writtenBytes = write(lteFd, "\n", 1);
        FD_ZERO(&sFd);
        FD_SET(lteFd, &sFd);

        tv.tv_sec = timeout;
        tv.tv_usec = 0;

        //CAB_DBG(CAB_GW_INFO, "timeout : %d", tv.tv_sec);
        while(count++ < RETRY_COUNT)
        {
                //CAB_DBG(CAB_GW_INFO, "Count : %d", count);
                if(select(lteFd+1, &sFd, NULL, NULL, &tv))
                {
                        if(FD_ISSET(lteFd, &sFd))
                        {
                                *readBytes = read(lteFd, readBuff, 512);
                                if(*readBytes < 0)
                                {
                                        CAB_DBG(CAB_GW_ERR, "read fails ... %s", strerror(errno));
                                        pthread_mutex_unlock(&lock);
                                        return FILE_READ_ERROR;
                                }
                                if(strstr(readBuff, pattern))
                                {
                                        //CAB_DBG(CAB_GW_INFO, "------Pattern Matched-----\n");
                                        matched = 1;
                                        break;
                                }
                                if(strstr(pattern, "imei") && (*readBytes >= MAX_IMEI_LEN))
                                {
                                        matched = 1;
                                        break;
                                }
                        }
                }
        }
        pthread_mutex_unlock(&lock);
        if (matched == 0) {
                CAB_DBG(CAB_GW_INFO, "AT command failed, checking USB interface");
                *readBytes = 0;
                retCode = DATA_NOT_SUFFICIENT;
        }
        updateEG91ResetStatus();

        return retCode;
}

returnCode_e ATTransactEG91(char *writeBuff, size_t len,  char *pattern, char *readBuff, int *readBytes)
{
	int count = 0;
	int  writtenBytes = 0;
	uint8_t matched = 0;
	returnCode_e retCode = GEN_SUCCESS;

	if(!writeBuff || !pattern || !readBuff || !readBytes)
	{
		CAB_DBG(CAB_GW_ERR, "Invalid argument");
		return GEN_NULL_POINTING_ERROR;
	}

	pthread_mutex_lock(&lock);

	tcflush(lteFd, TCIFLUSH);   /* Discards old data in the rx buffer */

	//memcpy(tempWriteBuff, writeBuff, len);
	//*(tempWriteBuff+len) = '\n';
	//*(tempWriteBuff+len+1) = '\0';
	//writtenBytes = write(lteFd, tempWriteBuff, len);
	writtenBytes = write(lteFd, writeBuff, len);
	writtenBytes = write(lteFd, "\n", 1);
	//CAB_DBG(CAB_GW_INFO,"%s\r\n",writeBuff);

	FD_ZERO(&sFd);
	FD_SET(lteFd, &sFd);

	/* Wait up to 1 seconds. */
        tv.tv_sec = 1;
	tv.tv_usec = 0;
        
	while(count++ < RETRY_COUNT)
	{
		if(select(lteFd+1, &sFd, NULL, NULL, &tv))
		{
			if(FD_ISSET(lteFd, &sFd))
			{
				*readBytes = read(lteFd, readBuff, 512);
				if(*readBytes < 0)
				{
					CAB_DBG(CAB_GW_ERR, "read fails ... %s", strerror(errno));
					pthread_mutex_unlock(&lock);
					return FILE_READ_ERROR;
				}
				if(strstr(readBuff, pattern))
				{
					matched = 1;
					break;
				}
				if(strstr(pattern, "imei") && (*readBytes >= MAX_IMEI_LEN))
				{
					matched = 1;
					break;
				}
			}
		}
	}
	pthread_mutex_unlock(&lock);
        if (matched == 0) {
		CAB_DBG(CAB_GW_DEBUG, "AT command failed, checking USB interface");
		*readBytes = 0;
	} 
	updateEG91ResetStatus();

	return retCode;
}

/** @brief API to get reset required state of EG91 

	This function provides API to access reset required 
	state of EG91 module.
*/
uint8_t getEG91ResetReqdState()
{
	int readBytes;
	char readBuff[50];
	ATTransactEG91("AT", 2, "OK", &readBuff, &readBytes);
	return getResetFlag();
}
