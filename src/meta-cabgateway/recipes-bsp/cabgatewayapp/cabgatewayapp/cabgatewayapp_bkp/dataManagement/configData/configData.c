/********************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file dataStorage.c
 * @brief (This file contains APIs defination of data storage module.)
 *
 * @Author     - VT
 **********************************************************************************************
 * History
 *
 * July/24/2018, VT , First Draft
 **********************************************************************************************/

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include "configData.h"
#include "cloud.h"

/****************************************
 ******** STATIC VARIABLES **************
 ****************************************/
static pthread_mutex_t g_lock = PTHREAD_MUTEX_INITIALIZER;  //Initialize mutex variable
static int ver1 = 0, ver2 = 0, ver3 = 0, ver4 = 0;
static char cfgOutDirName[MAX_CFG_DIRNAME_LEN] = {'\0'};
static char vehCSVFileName[MAX_CFG_FILENAME_LEN] = {'\0'};
static char cfgEventSumFileName[MAX_CFG_FILENAME_LEN] = {'\0'};
static uint16_t g_zipLimit;
extern bool isConfigChangefromCloud;
/****************************************
 ******** FUNCTION DEFINITIONS **********
 ****************************************/
/******************************************************************
 *@brief  (This API is used to read event summary file which contains all sensors summary)
 *
 *@param[IN] cfgSummary_t* (Pointer to structure of event summary data)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e readCfgEventSummary(cfgSummary_t * eventSumm)
{
    FUNC_ENTRY

    int summaryFile;                                //file descriptor
    uint32_t ret;

    /*Input argument validation*/
    if(eventSumm == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error");
        return GEN_NULL_POINTING_ERROR;
    }

    //opening event summary file for read
    summaryFile = open(cfgEventSumFileName, O_RDONLY, 0644);
    if(summaryFile == -1) {
        CAB_DBG(CAB_GW_ERR, "%s file opening error", cfgEventSumFileName);
        return FILE_OPEN_FAIL;
    }

    CAB_DBG(CAB_GW_DEBUG, "Zip file cnt :%d", g_cfgEventSumm.zipFileCnt);
    //reading event summary structure data from file
    ret = read(summaryFile, eventSumm, sizeof(cfgSummary_t));
    if(ret == -1) {
        CAB_DBG(CAB_GW_ERR, "%s file reading error", cfgEventSumFileName);
        close(summaryFile);
        return FILE_READ_ERROR;
    }
    //close event summary file
    close(summaryFile);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (This API is used to write event summary file which contains all sensors summary)
 *
 *@param[IN] cfgSummary_t* (Pointer to structure of event summary data)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e writeCfgEventSummary(cfgSummary_t * eventSumm)
{
    FUNC_ENTRY

    int outFile;                               //file descriptor
    uint32_t ret;

    /*Input argument validation*/
    if(eventSumm == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error");
        return GEN_NULL_POINTING_ERROR;
    }

    //opening event summary file for write
    outFile = open(cfgEventSumFileName, O_WRONLY | O_CREAT, 0644);
    if(outFile == -1) {
        CAB_DBG(CAB_GW_ERR, "%s file opening error", cfgEventSumFileName);
        return FILE_OPEN_FAIL;
    }

    CAB_DBG(CAB_GW_DEBUG, "Zip file cnt :%d", g_cfgEventSumm.zipFileCnt);
    //writing event summery structure data to file
    ret = write(outFile, eventSumm, sizeof(cfgSummary_t));
    if(ret == -1) {
        CAB_DBG(CAB_GW_ERR, "%s file writing error", cfgEventSumFileName);
        close(outFile);
        return FILE_WRITE_ERROR;
    }

    fsync(outFile);
    //close event summary file
    close(outFile);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/*
 * Check if a file exist using stat() function
 * return 1 if the file exist otherwise return 0
 */
static uint8_t constCfgfileexists(const char* filename)
{
    FUNC_ENTRY
    struct stat buffer;
    int exist = stat(filename,&buffer);
    if (exist == 0) {
    	FUNC_EXIT
        return 1;
    } else {
	FUNC_EXIT
        return 0;
    }
}

/******************************************************************
 *@brief  (This API is used to check if csv present)
 *
 *@param[IN]  None
 *
 *@return returnCode_e GEN_SUCCESS -> data avail. DATA_INVALID-> no data
 *********************************************************************/
static returnCode_e checkCfgDataPresence(void)
{
    FUNC_ENTRY

    if (constCfgfileexists(vehCSVFileName)) {
	CAB_DBG(CAB_GW_TRACE, "vehicle info Data csv present");
	return GEN_SUCCESS;
    }
    
    CAB_DBG(CAB_GW_TRACE, "Vehicle info csv not present");

    FUNC_EXIT
    return DATA_INVALID;
}

/******************************************************************
 *@brief  (This API is used to find the oldest zip file name)
 *
 *@param[IN]  None
 *@param[OUT] void * (Indicates pointer to string)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e countNoOfCfgFile(char * dirName, char *checkExt, uint32_t *fileCnt)
{
    FUNC_ENTRY

    struct dirent *dp;                  // Pointer for directory entry
    char ext[5];                        //to get extention of file
    uint8_t len, loopCnt;

    if (dirName == NULL || checkExt == NULL || fileCnt == NULL) {
	    CAB_DBG(CAB_GW_ERR, "Invalid argument in count No of files");
	    return GEN_API_FAIL;
    }

    // opendir() returns a pointer of DIR type.
    DIR *dr = opendir(dirName);

    if(dr == NULL) {  // opendir returns NULL if couldn't open directory
        CAB_DBG(CAB_GW_ERR, "Opening directory failed");
        return DIR_OPEN_FAIL;
    }

    *fileCnt = 0;
    // for readdir()
    while((dp = readdir(dr)) != NULL) {
        if(strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0)
            continue;
        len = strlen(dp->d_name);
        len -= 4;
        for(loopCnt = 0; dp->d_name[len] != '\0'; loopCnt++) {
            ext[loopCnt] = dp->d_name[len];
            len++;
        }
        if(strncmp(ext, checkExt, 4) == 0) {
	    *fileCnt = *fileCnt + 1;
        }
    }

    closedir(dr);
    CAB_DBG(CAB_GW_DEBUG, "######Zip file cnt :%d#######", *fileCnt);
    return GEN_SUCCESS;
}
/******************************************************************
 *@brief  (This API is used to find the oldest zip file name)
 *
 *@param[IN]  None
 *@param[OUT] void * (Indicates pointer to string)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e findOldestCfgFile(void * fileName)
{
    FUNC_ENTRY

    struct dirent *dp;                  // Pointer for directory entry
    time_t oldesttime = 0;              //timestamp for oldest file
    struct stat statbuf;                //to get stat of file
    char buffer[MAX_LEN_FILENAME];      //to temporary store file name
    char oldestFile[MAX_LEN_FILENAME];	//to store oldest file name
    char ext[5];                        //to get extention of file
    uint8_t len, loopCnt;
    fileStorageInfo_t tempFileStorageInfo = *(fileStorageInfo_t*)fileName;

    if (cfgOutDirName[0] == NULL) {
	    CAB_DBG(CAB_GW_ERR, "Output direcotry is not set");
	    return GEN_API_FAIL;
    }

    // opendir() returns a pointer of DIR type.
    DIR *dr = opendir(cfgOutDirName);

    if(dr == NULL) {  // opendir returns NULL if couldn't open directory
        CAB_DBG(CAB_GW_ERR, "Opening directory failed");
        return DIR_OPEN_FAIL;
    }

    CAB_DBG(CAB_GW_DEBUG, "Zip file cnt :%d", g_cfgEventSumm.zipFileCnt);
    // for readdir()
    while((dp = readdir(dr)) != NULL) {
        if(strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0)
            continue;
        len = strlen(dp->d_name);
        len -= 4;
        for(loopCnt = 0; dp->d_name[len] != '\0'; loopCnt++) {
            ext[loopCnt] = dp->d_name[len];
            len++;
        }
        if(strncmp(ext, ".zip", 4) == 0) {
            sprintf(buffer, "%s/%s", cfgOutDirName, dp->d_name);
            lstat(buffer, &statbuf);
            if((statbuf.st_mtime < oldesttime) | (oldesttime == 0)) {
                sprintf(oldestFile, "%s", dp->d_name);
                oldesttime = statbuf.st_mtime;
            }
        }
    }

    sprintf(tempFileStorageInfo.filename, "%s", oldestFile);
    sprintf(tempFileStorageInfo.storagePath, "%s", cfgOutDirName);
    memcpy(fileName, &tempFileStorageInfo, sizeof(fileStorageInfo_t));
    CAB_DBG(CAB_GW_DEBUG, "Temp file name : %s", (char *)fileName);

    closedir(dr);
    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (This API is used to set Changed configuration event data in configuration event file)
 *
 *@param[IN] void * (Indicates pointer to structure of changed configuration data)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e configEventSetForCfgFile(void * data)
{
    FUNC_ENTRY

    uint32_t status;
    uint8_t axleCount,axle;
    FILE * outFile;                                               //file descriptor
    changeConfigParam_t tempEvent = *(changeConfigParam_t*)data;  //to get temp instance to add id

    //Validate input argument
    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //Lock to protect event data
    pthread_mutex_lock(&g_lock);
    CAB_DBG(CAB_GW_DEBUG, "Zip file cnt :%d", g_cfgEventSumm.zipFileCnt);

    //Add event id in data comes from M&T module
    tempEvent.eventID = g_cfgEventSumm.vehicleInfoCnt + 1;

    /*  Writing Changed configuration event data to Changed configuration event file  */

    //Open event file. If not exist, create it.
    outFile = fopen(vehCSVFileName, "a");
    if(outFile == NULL) {
        CAB_DBG(CAB_GW_ERR, "%s file opening failed", vehCSVFileName);
        //Unlock to protect event data
        pthread_mutex_unlock(&g_lock);
        return FILE_OPEN_FAIL;
    }

    // write entire event structure to event data file
    switch(tempEvent.type) {
        case VEHICLE_NUMBER :
            status = fprintf(outFile, "%d,%d,%ld,%s,%d\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.vehicleNum, isConfigChangefromCloud);
            break;
        case CUSTOMER_ID :
            status = fprintf(outFile, "%d,%d,%ld,%s,%d\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.customerID, isConfigChangefromCloud);
            break;
        case VEHICLE_TYPE :
            status = fprintf(outFile, "%d,%d,%ld,%d,%d\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.vehicleType, isConfigChangefromCloud);
            break;
        case FLEET_NAME :
            status = fprintf(outFile, "%d,%d,%ld,%s,%d\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.fleetName, isConfigChangefromCloud);
            break;
        case FLEET_ID :
            status = fprintf(outFile, "%d,%d,%ld,%s,%d\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.fleetID, isConfigChangefromCloud);
            break;
        case VEHICLE_MAKE :
            status = fprintf(outFile, "%d,%d,%ld,%s,%d\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.vehicleMake, isConfigChangefromCloud);
            break;
        case VEHICLE_YEAR :
            status = fprintf(outFile, "%d,%d,%ld,%d,%d\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.vehicleYear, isConfigChangefromCloud);
            break;
        case TYRE_MAKE :
            status = fprintf(outFile, "%d,%d,%ld,%s,%d\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.tyreMake, isConfigChangefromCloud);
            break;
        case TYRE_SIZE :
            status = fprintf(outFile, "%d,%d,%ld,%s,%d\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.tyreSize, isConfigChangefromCloud);
            break;
        case SENSOR_TYPE :
            status = fprintf(outFile, "%d,%d,%ld,%d,%d\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.sensorType, isConfigChangefromCloud);
            break;
        case ADD_SENSOR_ID :
            status = fprintf(outFile, "%d,%d,%ld,%X,%d,%d,%d,%d,%d,%s,%s,%d\n", tempEvent.eventID, \
                             tempEvent.type, tempEvent.time, tempEvent.tpmsSensorID.sensorID, \
                             tempEvent.tpmsSensorID.wheelInfo.isHaloEnable, \
                             tempEvent.tpmsSensorID.wheelInfo.attachedType,  \
                             tempEvent.tpmsSensorID.wheelInfo.tyrePos,   \
                             tempEvent.tpmsSensorID.wheelInfo.vehicleSide, \
                             tempEvent.tpmsSensorID.wheelInfo.axleNum,
                             tempEvent.tpmsSensorID.sensorPN, tempEvent.tpmsSensorID.sensorModel, isConfigChangefromCloud);
            break;
        case REMOVE_SENSOR_ID :
            status = fprintf(outFile, "%d,%d,%ld,%X,%d,%d,%d,%d,%d,%d\n", tempEvent.eventID, \
                             tempEvent.type, tempEvent.time, tempEvent.tpmsSensorID.sensorID, \
                             tempEvent.tpmsSensorID.wheelInfo.isHaloEnable, \
                             tempEvent.tpmsSensorID.wheelInfo.attachedType,  \
                             tempEvent.tpmsSensorID.wheelInfo.tyrePos,   \
                             tempEvent.tpmsSensorID.wheelInfo.vehicleSide, \
                             tempEvent.tpmsSensorID.wheelInfo.axleNum, isConfigChangefromCloud);
            break;
        case TPMS_THRESHOLD :
	    axleCount = tempEvent.targetPressInfo.numberOfAxle;
            status = fprintf(outFile, "%d,%d,%ld,%d",  \
                             tempEvent.eventID, tempEvent.type, \
			     tempEvent.time,axleCount);
	    for(axle=0;axle<axleCount;axle++)
		fprintf(outFile,",%d",tempEvent.targetPressInfo.tpmsThreshold[axle]);
	    fprintf(outFile,",%d",isConfigChangefromCloud);
	    fprintf(outFile,"\n");
            break;
        case LOW_BATTERY_THRESHOLD :
            status = fprintf(outFile, "%d,%d,%ld,%d\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.lowBatteryThreshold);
            break;
        case CLOUD_URI :
            status = fprintf(outFile, "%d,%d,%ld,%s\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.cloudURI);
            break;
        case ATTACH_HALO_SN:
        case DETACH_HALO_SN:
            status = fprintf(outFile, "%d,%d,%ld,%s,%d,%d,%d\n", tempEvent.eventID, tempEvent.type, \
                             tempEvent.time, tempEvent.haloSNData.serialNo, tempEvent.haloSNData.vehicleSide, \
                             tempEvent.haloSNData.axleNum, isConfigChangefromCloud);
            break;
        case UPDATE_TYRE_DETAILS:
            status = fprintf(outFile, "%d,%d,%ld,%d,%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%u,%ld,%d\n", tempEvent.eventID, \
                             tempEvent.type, tempEvent.time, tempEvent.tireDetails.axleNum, \
                             tempEvent.tireDetails.vehicleSide, tempEvent.tireDetails.tyrePos, \
                             tempEvent.tireDetails.tyreSerialNo, tempEvent.tireDetails.tyreTIN, \
                             tempEvent.tireDetails.tyreMake, tempEvent.tireDetails.tyreModel, \
                             tempEvent.tireDetails.tyreWidth, tempEvent.tireDetails.ratio, \
                             tempEvent.tireDetails.diameter, tempEvent.tireDetails.loadRating, \
                             tempEvent.tireDetails.treadDepthHistory[0].treadDepthValue, \
                             tempEvent.tireDetails.treadDepthHistory[0].vehicleMileage, \
                             tempEvent.tireDetails.treadDepthHistory[0].recordTime, isConfigChangefromCloud);
            break;
        case FLUSH_TIRE_DETAILS:
            status = fprintf(outFile, "%d,%d,%ld,%d,%d,%d\n", tempEvent.eventID, \
                             tempEvent.type, tempEvent.time, tempEvent.tireDetails.axleNum, \
                             tempEvent.tireDetails.vehicleSide, tempEvent.tireDetails.tyrePos);
            break;
        case UPDATE_TREAD_DEPTH_VALUE:
            status = fprintf(outFile, "%d,%d,%ld,%d,%d,%d,%s,%u,%ld\n", tempEvent.eventID, \
                             tempEvent.type, tempEvent.time, tempEvent.tireDetails.axleNum, \
                             tempEvent.tireDetails.vehicleSide, tempEvent.tireDetails.tyrePos, \
                             tempEvent.tireDetails.treadDepthHistory[0].treadDepthValue, \
                             tempEvent.tireDetails.treadDepthHistory[0].vehicleMileage, \
                             tempEvent.tireDetails.treadDepthHistory[0].recordTime);
            break;
        default :
            CAB_DBG(CAB_GW_ERR, "Invalid option.");
            //Unlock to protect event data
            pthread_mutex_unlock(&g_lock);
            return GEN_INVALID_TYPE;
    }
    if(status < 0) {
        CAB_DBG(CAB_GW_ERR, "%s file writing failed.", vehCSVFileName);
        fclose(outFile);
        //Unlock to protect event data
        pthread_mutex_unlock(&g_lock);
        return FILE_WRITE_ERROR;
    }
    /* Write buffered data to the memory right away */
    fflush(outFile);
    fsync(fileno(outFile));
    //Close event file
    fclose(outFile);
    /*  Updating event summary file   */

    //Increase TPMS total event count
    g_cfgEventSumm.vehicleInfoCnt ++;

    //Write updated value in summary file
    status = writeCfgEventSummary(&g_cfgEventSumm);
    if(GEN_SUCCESS != status) {
        CAB_DBG(CAB_GW_ERR, "Updating event summary file failed");
        //Unlock to protect event data
        pthread_mutex_unlock(&g_lock);
        return FILE_WRITE_ERROR;
    }
    //Unlock to protect event data
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e deleteCfgPackageFile(void * data)
{
    FUNC_ENTRY

    int32_t status;
    char buffer[MAX_LEN_FILENAME];                  //to temporary store file name
    fileStorageInfo_t* tempFileStorageInfo = (fileStorageInfo_t*)data;
    
    if(tempFileStorageInfo == NULL) {
    	CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
    	return GEN_NULL_POINTING_ERROR;
    }

    //lock to protect event summary data
    pthread_mutex_lock(&g_lock);

    //To delete package files from dataStorage directory which name is provided in argument
	
    sprintf(buffer, "%s/%s", tempFileStorageInfo->storagePath, tempFileStorageInfo->filename);
    status = remove(buffer);
    if(status != 0) {
        CAB_DBG(CAB_GW_ERR, "Error in deleting package file.");
        //unlock to protect event summary data
        pthread_mutex_unlock(&g_lock);
        return FILE_DELETE_ERROR;
    }

    /*  Updating event summary file   */

    //Decrease zip file count
    if(g_cfgEventSumm.zipFileCnt > 0) {
        g_cfgEventSumm.zipFileCnt --;

        //Write updated value in summary file
        status = writeCfgEventSummary(&g_cfgEventSumm);
        if(GEN_SUCCESS != status) {
            CAB_DBG(CAB_GW_ERR, "Updating event summary file failed");
            //Unlock to protect event data
            pthread_mutex_unlock(&g_lock);
            return FILE_WRITE_ERROR;
        }
    }

    //unlock to protect event summary data
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e initCfgStorageModule(uint16_t noOfPackage, char *swVersion, char *inputDirName)
{
    FUNC_ENTRY

    uint8_t status = GEN_SUCCESS;
    uint32_t zipIndex = 0;
    struct stat st = {0};
    uint32_t zipFileCnt = 0;
    char checkExt[] = ".zip";
    fileStorageInfo_t tempFileStorageInfo;
  
    if (inputDirName == NULL) {
	    CAB_DBG(CAB_GW_ERR, "Init storage failed due to invalid arg");
	    return MODULE_INIT_FAIL;
    }
    if (strcpy(cfgOutDirName, inputDirName) == NULL) {
	    CAB_DBG(CAB_GW_ERR, "strcpy failed in init data storage module");
	    return MODULE_INIT_FAIL;
    }
    if (sprintf(vehCSVFileName, CFG_VEHICLE_INFO_FILE_PATH) <= 0) {
	    CAB_DBG(CAB_GW_ERR, "veh info filename set failed");
	    return MODULE_INIT_FAIL;
    }
    if (sprintf(cfgEventSumFileName, CFG_FILE_PATH_EVENT_SUMM) <= 0) {
	    CAB_DBG(CAB_GW_ERR, "eve summary file set failed");
	    return MODULE_INIT_FAIL;
    }

    CAB_DBG(CAB_GW_INFO, "Initializing Config Data storage module");


    //Initializind mutext lock
    pthread_mutex_init(&g_lock, NULL);

    //lock to protect event summary data
    pthread_mutex_lock(&g_lock);

    //Copy no of package file into global variable
    if(noOfPackage == 0 || swVersion == NULL) {
        //unlock to protect event summary data
        pthread_mutex_unlock(&g_lock);
	CAB_DBG(CAB_GW_ERR, "Invalid argument");
        return GEN_NULL_POINTING_ERROR;
    }

    sscanf(swVersion, "%d.%d.%d.%d", &ver1, &ver2, &ver3, &ver4);
    

    g_zipLimit = noOfPackage;

    //To check for the dataStorage directory and if it not exist then create it
    if(stat(cfgOutDirName, &st) == -1) {
        if((mkdir(cfgOutDirName, 0755)) == -1) {
            CAB_DBG(CAB_GW_ERR, "dataStorage directory not created");
            //unlock to protect event summary data
            pthread_mutex_unlock(&g_lock);
            return DIR_CREATE_FAIL;
        }
    }

    //If event summary file does not exist
    if(access(cfgEventSumFileName, F_OK) == -1) {
        status = writeCfgEventSummary(&g_cfgEventSumm);       //update event summary file
        if(GEN_SUCCESS != status) {
            CAB_DBG(CAB_GW_ERR, "Updating event summary file failed");
        }
    } else {                                           //If event summary file exist
        status = readCfgEventSummary(&g_cfgEventSumm);       //read event summary file
        if(GEN_SUCCESS != status) {
            CAB_DBG(CAB_GW_ERR, "Reading event summary file failed");
        }
    }

    /* correct corrupted evebt summary and clean up data dir if zip files are more than limit*/
    status = countNoOfCfgFile(cfgOutDirName, checkExt, &zipFileCnt);
    if (GEN_SUCCESS != status) {
	    CAB_DBG(CAB_GW_ERR, "failed to get zip file count with code :%d", status);
    } else {
	if (zipFileCnt > g_zipLimit) {
		CAB_DBG(CAB_GW_ERR, "more zip than limit, cleaning extra old zip files");
		for (zipIndex = 0; zipIndex <  (zipFileCnt - g_zipLimit); zipIndex++) {
			status = findOldestCfgFile((void*)&tempFileStorageInfo);
			if(status != GEN_SUCCESS) {
                CAB_DBG(CAB_GW_ERR, "Not getting oldest file name");
				/* unlock to protect event summary data */
				pthread_mutex_unlock(&g_lock);
				return FILE_NOT_FOUND;
			}    
			/* unlock to protect event summary data */
			pthread_mutex_unlock(&g_lock);
			status = deleteCfgPackageFile((void*)&tempFileStorageInfo);
			pthread_mutex_lock(&g_lock);
			if(status != GEN_SUCCESS) {
				CAB_DBG(CAB_GW_ERR, "Delete package error");
				/* unlock to protect event summary data */
				pthread_mutex_unlock(&g_lock);
				return FILE_DELETE_ERROR;
			}    
		}
		g_cfgEventSumm.zipFileCnt = g_zipLimit;
	} else if (zipFileCnt != g_cfgEventSumm.zipFileCnt) {
    	        g_cfgEventSumm.zipFileCnt = zipFileCnt;
    	}
    	status = writeCfgEventSummary(&g_cfgEventSumm);
    	if (GEN_SUCCESS != status) {
    	    CAB_DBG(CAB_GW_ERR, "write event sum failed due to error code : %d", status);
    	}
    }

    //unlock to protect event summary data
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return status;
}

returnCode_e deInitCfgStorageModule(void)
{
    FUNC_ENTRY

    CAB_DBG(CAB_GW_INFO, "Deinitializing config Data storage module");
    /*Any ongoing file operation will not allow to take this lock untill
      it completes. Once completed, no other thread will spawn to use event
     module as all other modules must have been deinit before */
    pthread_mutex_lock(&g_lock);
    pthread_mutex_unlock(&g_lock);

    //destroying lock
    pthread_mutex_destroy(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e createCfgFilePackage(time_t time_value)
{
    FUNC_ENTRY

    int32_t status;
    fileStorageInfo_t tempFileStorageInfo;
    char command[MAX_LEN_COMMAND_IN_CFG_FILE];
    cabGatewayStatus_t installationStatus;
    char latestMandatoryConfigFileName[MAX_CFG_FILENAME_LEN] = {'\0'};
    returnCode_e retCode;

    if (checkCfgDataPresence() != GEN_SUCCESS) {
	CAB_DBG(CAB_GW_DEBUG, "No data available, skipping packaging");
	return GEN_SUCCESS;
    }

    CAB_DBG(CAB_GW_DEBUG, "Zip file cnt :%d", g_cfgEventSumm.zipFileCnt);

    //lock to protect event summary data
    pthread_mutex_lock(&g_lock);

    if(g_cfgEventSumm.zipFileCnt >= g_zipLimit) {
        status = findOldestCfgFile((void*)&tempFileStorageInfo);
        if(status != GEN_SUCCESS) {
            CAB_DBG(CAB_GW_ERR, "Not getting oldest file name");
            //unlock to protect event summary data
            pthread_mutex_unlock(&g_lock);
            return FILE_NOT_FOUND;
        }
        //unlock to protect event summary data
        pthread_mutex_unlock(&g_lock);
        status = deleteCfgPackageFile((void*)&tempFileStorageInfo);
        pthread_mutex_lock(&g_lock);
        if(status != GEN_SUCCESS) {
            CAB_DBG(CAB_GW_ERR, "Deleting package file failed");
            //unlock to protect event summary data
            pthread_mutex_unlock(&g_lock);
            return FILE_DELETE_ERROR;
        }
    }

    	sprintf(command, "cd %s; zip -m ./"CFG_ZIP_FILE_NAME" ./*.csv 2>/dev/null 1>/dev/null; sync",  cfgOutDirName, ver1, ver2, ver3, ver4, time_value);
    	//To create a zip file to send data on cloud
    	//File name is unique because format is CG_<epoch_time>.zip
    	status = WEXITSTATUS(system(command));
    	if(status != GEN_SUCCESS) {
    	    CAB_DBG(CAB_GW_ERR, "Creating zip file failed");
    	    //unlock to protect event summary data
    	    pthread_mutex_unlock(&g_lock);
    	    return FILE_CREATE_ERROR;
    	}

    	memset((void *)&installationStatus, 0, sizeof(cabGatewayStatus_t));

    	/* Save the configuration zip file name if it is created for mandatory configuration file.
    	 * And flip the mandatoryConfigFileUpdated to detect upcoming mandatory field changes. */
    	retCode = getConfigModuleParam(INSTALLATION_STATUS_CMD, (void *)&installationStatus);

    	if (retCode == GEN_SUCCESS && installationStatus.mandatoryConfigFileUpdated == true)
    	{
    	    snprintf(latestMandatoryConfigFileName, MAX_CFG_FILENAME_LEN, CFG_ZIP_FILE_NAME, ver1, ver2, ver3, ver4, time_value);

    		retCode = setConfigModuleParam(CONIF_FILE_CREATION_STATUS_CMD, (void *)&latestMandatoryConfigFileName);
    		if (retCode != GEN_SUCCESS) {
    			CAB_DBG(CAB_GW_ERR, "Failed to set cofing file creation status with code : %d", retCode);
    		}
    	}

    	memset(command, 0, sizeof(command));

    /*  Updating event summary file   */

    //Increase zip file count
    g_cfgEventSumm.zipFileCnt ++;

    //Write updated value in summary file
    status = writeCfgEventSummary(&g_cfgEventSumm);
    if(GEN_SUCCESS != status) {
        CAB_DBG(CAB_GW_ERR, "Updating event summary file failed");
        //Unlock to protect event data
        pthread_mutex_unlock(&g_lock);
        return FILE_WRITE_ERROR;
    }

    //unlock to protect event summary data
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

returnCode_e getCfgPackageFile(void * data, void * isLatestMandatoryConfigFile)
{
    FUNC_ENTRY

    struct dirent *dp;                              // Pointer for directory entry
    time_t recenttime = 0;                          //timestamp for recent file
    struct stat statbuf;                            //to get stat of file
    char buffer[MAX_LEN_FILENAME];                  //to temporary store file name
    char recentFile[MAX_LEN_FILENAME] = {'\0'};     //to store recent file name
    char ext[5];                                    //to get extention of file
    uint8_t len, loopCnt;
    returnCode_e retCode;
    bool LatestMandatoryConfigFile = false;
    cabGatewayStatus_t installationStatus;
    fileStorageInfo_t tempFileStorageInfo = *(fileStorageInfo_t*)data;

    //lock to protect event summary data
    pthread_mutex_lock(&g_lock);

    // opendir() returns a pointer of DIR type.
    DIR *dr = opendir(cfgOutDirName);

    if(dr == NULL) {  // opendir returns NULL if couldn't open directory
        CAB_DBG(CAB_GW_ERR, "Opening the directory failed");
        //unlock to protect event summary data
        pthread_mutex_unlock(&g_lock);
        return DIR_OPEN_FAIL;
    }

    if(isLatestMandatoryConfigFile == NULL) {
    	CAB_DBG(CAB_GW_ERR, "NULL pointing error");
    	//unlock to protect event summary data
    	pthread_mutex_unlock(&g_lock);
    	return GEN_NULL_POINTING_ERROR;
    }

    // for readdir()
    while((dp = readdir(dr)) != NULL) {
    	if(strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0)
    		continue;
    	len = strlen(dp->d_name);
    	len -= 4;
    	for(loopCnt = 0; dp->d_name[len] != '\0'; loopCnt++) {
    		ext[loopCnt] = dp->d_name[len];
    		len++;
    	}

    	len = strlen(dp->d_name);
    	len -= 4;
    	for(loopCnt = 0; dp->d_name[len] != '\0'; loopCnt++) {
    		ext[loopCnt] = dp->d_name[len];
    		len++;
    	}

    	//Get latest zip file to send it on cloud
    	if(strncmp(ext, ".zip", 4) == 0) {
    		sprintf(buffer, "%s/%s", cfgOutDirName, dp->d_name);
    		lstat(buffer, &statbuf);
    		if(statbuf.st_mtime > recenttime) {
    			sprintf(recentFile, "%s", dp->d_name);
    			recenttime = statbuf.st_mtime;
    		}
    	}
    }

    if(recentFile[0] == '\0') {
        CAB_DBG(CAB_GW_DEBUG, "No pending event");
        closedir(dr);
        //unlock to protect event summary data
        pthread_mutex_unlock(&g_lock);
        return MISC_NO_PENDING_EVENT;
    }

    //To send latest file path to get access of file
    sprintf(tempFileStorageInfo.filename, "%s", recentFile);
    sprintf(tempFileStorageInfo.storagePath, "%s", cfgOutDirName);
    memcpy(data, &tempFileStorageInfo, sizeof(fileStorageInfo_t));

    retCode = getConfigModuleParam(INSTALLATION_STATUS_CMD, (void *)&installationStatus);
    // If the current file is the latest mandatory config zip file then indicate the same
    if (retCode == GEN_SUCCESS && strncmp(installationStatus.latestMandatoryConfigFileName, tempFileStorageInfo.filename, MAX_LEN_FILENAME) == 0)
    {
    	LatestMandatoryConfigFile = true;
    }

    memcpy(isLatestMandatoryConfigFile, &LatestMandatoryConfigFile, sizeof(bool));

    closedir(dr);

    //unlock to protect event summary data
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

