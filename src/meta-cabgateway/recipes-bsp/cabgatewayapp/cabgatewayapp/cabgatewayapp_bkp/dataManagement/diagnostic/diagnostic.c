/***************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file    diagnostic.c
 * @brief   (Source file for diagnostic module.)
 *
 * @Author     - VT
 *****************************************************************
 * History
 *
 * Sep/13/2018, VT , First Draft
***************************************************************/

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include "diagnostic.h"
#include "debug.h"

/****************************************
 ******** STATIC VARIABLES **************
 ****************************************/
static notifyDiagGetEvent g_notifyDiagGetEvent;             /**< To store callback function. */
static gpsDiagInfo_t g_gpsDiagInfo;                         /**< GPS diagnostic information. */
static lteDiagInfo_t g_lteDiagInfo;                         /**< LTE diagnostic information. */
static tpmsDiagDataInfo_t g_tpmsDiagDataInfo;               /**< TPMS diagnostic information. */
static sysDiagInfo_t g_sysDiagInfo;                         /**< Gateway diagnostic information. */
static batteryDiagInfo_t g_batDiagInfo;			    /**< Battery & RTC coin cell status*/
static pthread_mutex_t g_lock = PTHREAD_MUTEX_INITIALIZER;  /**< Initialize mutex variable. */

/****************************************
 ********* FUNCTION DEFINITION **********
 ****************************************/
/******************************************************************
 *@brief (This API is used to get GPS diagnostic data)
 *
 *@param[IN] None
 *@param[OUT] gpsDiagInfo_t * (Pointer to structure of gps diagnostic information)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e diagGetGpsData(gpsDiagInfo_t * data)
{
    FUNC_ENTRY

    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //lock mutex
    pthread_mutex_lock(&g_lock);

    memcpy(data, &g_gpsDiagInfo, sizeof(g_gpsDiagInfo));

    //unlock mutex
    pthread_mutex_unlock(&g_lock);

    (*g_notifyDiagGetEvent)();

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief (This API is used to get LTE diagnostic data)
 *
 *@param[IN] None
 *@param[OUT] lteDiagInfo_t * (Pointer to structure of lte diagnostic information)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e diagGetLteData(lteDiagInfo_t * data)
{
    FUNC_ENTRY

    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //lock mutex
    pthread_mutex_lock(&g_lock);

    memcpy(data, &g_lteDiagInfo, sizeof(g_lteDiagInfo));

    //unlock mutex
    pthread_mutex_unlock(&g_lock);

    (*g_notifyDiagGetEvent)();

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief (This API is used to get Battery & RTC coin cell diag data)
 *
 *@param[IN] None
 *@param[OUT] batteryDiagInfo_t * (Pointer to structure of bat diag info)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e diagGetBatRtcData(batteryDiagInfo_t * data)
{
    FUNC_ENTRY
    uint8_t isRtcAlreadySync;
    returnCode_e retCode;
    char timeSetStatus[] = "Time set";
    char timeNotSetStatus[] = "Time not set";

    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    /*Look for RTC status from configuration module*/
   retCode =  getConfigModuleParam(RTC_FLAG, (void *)&isRtcAlreadySync);
    if (GEN_SUCCESS != retCode) {
	    CAB_DBG(CAB_GW_ERR, "Get RTC flag failed with error %d\r\n", retCode);
	    return retCode;
    }

    //lock mutex
    pthread_mutex_lock(&g_lock);
    if(isRtcAlreadySync)
	    memcpy(g_batDiagInfo.RTCStatus, timeSetStatus, sizeof(timeSetStatus));
    else
	    memcpy(g_batDiagInfo.RTCStatus, timeNotSetStatus, sizeof(timeNotSetStatus));

    memcpy(data, &g_batDiagInfo, sizeof(g_batDiagInfo));

    //unlock mutex
    pthread_mutex_unlock(&g_lock);

    (*g_notifyDiagGetEvent)();

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief (This API is used to get TPMS diagnostic data)
 *
 *@param[IN] None
 *@param[OUT] tpmsDiagDataInfo_t * (Pointer to structure of TPMS diagnostic information)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e diagGetTpmsData(tpmsDiagDataInfo_t * data)
{
    FUNC_ENTRY

    returnCode_e retCode = GEN_SUCCESS;

    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //lock mutex
    pthread_mutex_lock(&g_lock);

    if(g_tpmsDiagDataInfo.pairedNumber == 0) {
        CAB_DBG(CAB_GW_ERR, "No TPMS data available");
        retCode = DIAG_NO_TPMS_DATA;
    }
    else{
    	memcpy(data, &g_tpmsDiagDataInfo, sizeof(g_tpmsDiagDataInfo));
	retCode = GEN_SUCCESS;
    }
    //unlock mutex
    pthread_mutex_unlock(&g_lock);

    (*g_notifyDiagGetEvent)();

    FUNC_EXIT
    return retCode;
}

/******************************************************************
 *@brief (This API is used to get single TPMS diagnostic data by sensor ID)
 *
 *@param[IN] None
 *@param[OUT] tpmsDMData_t * (Pointer to structure of single TPMS diagnostic information)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e diagGetTpmsDataById(tpmsDMData_t * data)
{
    FUNC_ENTRY

    tpmsDMData_t tempTpmsDMData;
    uint8_t loopCnt;
    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //lock mutex
    pthread_mutex_lock(&g_lock);

    tempTpmsDMData = *(tpmsDMData_t*)data;
    for(loopCnt = 0; loopCnt < g_tpmsDiagDataInfo.pairedNumber; loopCnt++) {
        if(g_tpmsDiagDataInfo.data[loopCnt].tpmsData.sensorID == tempTpmsDMData.tpmsData.sensorID) {
            memcpy(data, &g_tpmsDiagDataInfo.data[loopCnt], \
                   sizeof(g_tpmsDiagDataInfo.data[loopCnt]));
            CAB_DBG(CAB_GW_TRACE, "Sensor ID matched and data send to BLE module");
            //unlock mutex
            pthread_mutex_unlock(&g_lock);

            (*g_notifyDiagGetEvent)();

            return GEN_SUCCESS;
        }
    }
    CAB_DBG(CAB_GW_ERR, "Sensor ID not matched and no data send to BLE module");
    //unlock mutex
    pthread_mutex_unlock(&g_lock);

    (*g_notifyDiagGetEvent)();

    FUNC_EXIT
    return DATA_SEND_FAIL;
}

/******************************************************************
 *@brief (This API is used to get gateway diagnostic information)
 *
 *@param[IN] None
 *@param[OUT] sysDiagInfo_t * (Pointer to structure of gateway diagnostic data)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e diagGetGatewayInfo(sysDiagInfo_t * data)
{
    FUNC_ENTRY

    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //lock mutex
    pthread_mutex_lock(&g_lock);

    memcpy(data, &g_sysDiagInfo, sizeof(g_sysDiagInfo));

    //unlock mutex
    pthread_mutex_unlock(&g_lock);
    (*g_notifyDiagGetEvent)();       

    FUNC_EXIT
    return GEN_SUCCESS;
}

/******************************************************************
 *@brief (This API is used to convert imei uint no to string)
 *
 *@param[IN] inStr	Input IMEI string
 *@param[IN] outStr	Output converted string
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e imeiStringConvert(uint8_t *inArray, char * outStr)
{
	int loop;
	char *tempPtr = outStr;

	FUNC_ENTRY
	if (inArray == NULL || outStr == NULL) {
		return GEN_INVALID_ARG;
	}

	for(loop = 0; loop < MAX_MODEM_IMEI_LEN; loop ++) {
		sprintf(tempPtr, "%d", inArray[loop]);
		tempPtr++;
	}
	FUNC_EXIT
	return GEN_SUCCESS;
}

/******************************************************************
 *@brief (This API is used to convert Gateway ID uint no to string)
 *
 *@param[IN] inStr	Input IMEI string
 *@param[IN] outStr	Output converted string
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e gatewayIDStringConvert(char *inArray, char * outStr)
{
	int loop;
	char *tempPtr = outStr;

	FUNC_ENTRY
	if (inArray == NULL || outStr == NULL) {
		return GEN_INVALID_ARG;
	}
	for(loop = 0; loop < MAX_LEN_BLE_MAC; loop++) {
		sprintf(tempPtr, "%02X", inArray[loop]);
		tempPtr += 2;
	}
	for(loop = MAX_LEN_BLE_MAC; loop <  (MAX_LEN_BLE_MAC+MAX_MODEM_IMEI_LEN); loop++) {
		sprintf(tempPtr, "%d", inArray[loop]);
		tempPtr++;
	}
	FUNC_EXIT
	return GEN_SUCCESS;
}

/******************************************************************
 *@brief (This API is used to get paired TPMS information)
 *
 *@param[IN] None
 *@param[OUT] tpmsPairedDiagInfo_t * (Pointer to structure of paired TPMS information)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e diagGetPairedTpmsList(tpmsPairedDiagInfo_t * data)
{
    FUNC_ENTRY

    uint8_t status = GEN_SUCCESS;

    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    status = getPairedTpmsList(data);
    if(GEN_SUCCESS != status) {
        CAB_DBG(CAB_GW_ERR, "No TPMS paired.");
        return DIAG_NO_TPMS_PAIRED;
    }
    return status;
}

/**
 * Initialize diagnostic module
 * */
returnCode_e initDiagModule(notifyDiagGetEvent funPtr)
{
    FUNC_ENTRY

    CAB_DBG(CAB_GW_INFO, "Initializing diagnostic module");

    //Initializind mutext lock
    pthread_mutex_init(&g_lock, NULL);

    //lock mutex
    pthread_mutex_lock(&g_lock);

    if(funPtr == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        //unlock mutex
        pthread_mutex_unlock(&g_lock);
        return GEN_NULL_POINTING_ERROR;
    }
    g_notifyDiagGetEvent = funPtr;

    //unlock mutex
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/**
 * Deinitialize diagnostic module
 * */
returnCode_e deInitDiagModule(void)
{
    FUNC_ENTRY

    CAB_DBG(CAB_GW_INFO, "Deinitializing diagnostic module");

    /*Any ongoing file operation will not allow to take this lock untill
      it completes. Once completed, no other thread will spawn to use event
     module as all other modules must have been deinit before */
    pthread_mutex_lock(&g_lock);
    pthread_mutex_unlock(&g_lock);

    //destroying lock
    pthread_mutex_destroy(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/**
 * Set GPS Diagnostic information
 * */
returnCode_e diagSetGpsData(gpsDiagInfo_t * data)
{
    FUNC_ENTRY

    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //lock mutex
    pthread_mutex_lock(&g_lock);

    memcpy(&g_gpsDiagInfo, data, sizeof(g_gpsDiagInfo));

    //unlock mutex
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/**
 * Set LTE Diagnostic information
 * */
returnCode_e diagSetLteData(lteDiagInfo_t * data)
{
    FUNC_ENTRY

    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //lock mutex
    pthread_mutex_lock(&g_lock);

    memcpy(&g_lteDiagInfo, data, sizeof(g_lteDiagInfo));

    //unlock mutex
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/**
 * Set Battery & RTC Diagnostic information
 * */
returnCode_e diagSetBatRtcData(batteryDiagInfo_t * data)
{
    FUNC_ENTRY

    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //lock mutex
    pthread_mutex_lock(&g_lock);

    memcpy(&g_batDiagInfo, data, sizeof(g_batDiagInfo));

    //unlock mutex
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/**
 * Set TPMS Diagnostic information
 * */
returnCode_e diagSetTpmsData(tpmsDiagDataInfo_t * data)
{
    FUNC_ENTRY

    if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //lock mutex
    pthread_mutex_lock(&g_lock);

    memcpy(&g_tpmsDiagDataInfo, data, sizeof(g_tpmsDiagDataInfo));

    //unlock mutex
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return GEN_SUCCESS;
}

/**
 * Set Gateway Diagnostic information
 * */
returnCode_e diagSetGatewayInfo(sysDiagInfoParam_e type, void * data)
{
    FUNC_ENTRY

    returnCode_e status = GEN_SUCCESS;

    if((char *)data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL pointing error.");
        return GEN_NULL_POINTING_ERROR;
    }

    //lock mutex
    pthread_mutex_lock(&g_lock);

    switch(type) {
        case SYS_GATEWAY_UNIQUE_ID :                        /** To set gateway id */
	    gatewayIDStringConvert((char *)data, g_sysDiagInfo.gatewayId); 
            break;
        case SYS_HARDWARE_VERSION :                         /** To set hardware version */
            strcpy(g_sysDiagInfo.hwVersion, (char*)data);
            break;
        case SYS_SOFTWARE_VERSION :                         /** To set software version */
            strcpy(g_sysDiagInfo.swVersion, (char*)data);
            break;
        case SYS_MODEM_IMEI :                               /** To set modem IMEI */
	    imeiStringConvert((char *)data, g_sysDiagInfo.modemIMEI);
            break;
        case SYS_SIM_ICCID :                                /** To set SIM ICCCID */
            strcpy(g_sysDiagInfo.simICCID, (char*)data);
            break;
        case SYS_CLOUD_URI :                                /** To set cloud URL */
            strcpy(g_sysDiagInfo.cloudUrl, (char*)data);
            break;
        default :                                           /** Invalid data */
            CAB_DBG(CAB_GW_ERR, "Invalid option");
            status = GEN_INVALID_TYPE;
    }

    //unlock mutex
    pthread_mutex_unlock(&g_lock);

    FUNC_EXIT
    return status;
}

/**
 * Get Diagnostic information
 * */
returnCode_e getDiagInfo(commandCode_e type, void * data)
{
    FUNC_ENTRY

    uint8_t status = GEN_SUCCESS;

    CAB_DBG(CAB_GW_DEBUG,"Get Diag req CMD from BLE %d", type);
    switch(type) {
        case DIAG_GW_INFO_CMD :
            status = diagGetGatewayInfo((sysDiagInfo_t*)data);
            break;
        case DIAG_GW_PAIRED_SENSOR_CMD :
            status = diagGetPairedTpmsList((tpmsPairedDiagInfo_t*)data);
            break;
        case DIAG_GW_TPMS_DATA_CMD :
            status = diagGetTpmsData((tpmsDiagDataInfo_t*)data);
            break;
        case DIAG_GW_TPMS_BY_ID_CMD :
            status = diagGetTpmsDataById((tpmsDMData_t*)data);
            break;
        case DIAG_GW_GPS_INFO_CMD :
            status = diagGetGpsData((gpsDiagInfo_t*)data);
            break;
        case DIAG_GW_CELLULAR_INFO_CMD :
            status = diagGetLteData((lteDiagInfo_t*)data);
            break;
	case DIAG_GW_BATTERY_EV_LOG_CMD:
	    status = diagGetBatRtcData((batteryDiagInfo_t*)data);
	    break;
        default :
            CAB_DBG(CAB_GW_ERR, "Invalid option");
            return GEN_INVALID_TYPE;
    }

    FUNC_EXIT
    return status;
}
