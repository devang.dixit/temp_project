/*******************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *                                                       
 * Description:
 * @file : dataControl.c
 * @brief : 
 *
 * This module is used to handle all the system data. It will provide an interface 
 * between M&T and data Storage module for permanent storage of system data.
 * It will also enable the data transmission to cloud.
 *
 * @Author     - Volansys
 *******************************************************************************
 * History
 *
 * Sep/10/2018, VT , Created
 ******************************************************************************/

/**********************
 *		INCLUDES
 *********************/
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#include "dataControl.h"
#include "dataStorage.h"
#include "error.h"
#include "debug.h"
#include "cloud.h"

/**********************
 *	GLOBAL VARIABLE
 *********************/

/*	callback to intimate M&T	*/
uploadzipNotifCB g_notifyMTCB;

/*	Thread for processing event from SDC	*/
pthread_t g_processEventThreadID;

/*	lock for msgsnd	*/
static pthread_mutex_t msgQueMutex = PTHREAD_MUTEX_INITIALIZER;

/*	lock for deinit the sdc module gracefully	*/
static pthread_mutex_t moduleMutex = PTHREAD_MUTEX_INITIALIZER;


static bool g_initFlag = false;
static bool g_deinitReq = false;
static bool g_cloudThreadExist = false;
static uint8_t altertThreadCount = 0;
static zipType_e g_zipType = MAX_ZIP_TYPE;

pthread_mutex_t alertDataToCloudThreadAliveLock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t sendZipToCloudThreadAliveLock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t processEventFromMTThreadAliveLock = PTHREAD_MUTEX_INITIALIZER;

bool g_alertDataToCloudThreadAliveStatus = true;
bool g_sendZipToCloudThreadAliveStatus = true;
bool g_processEventFromMTThreadAliveStatus = true;

/*	Message Queue Identifier	*/
static int msqid;


static void setModuleState(bool state)
{
	pthread_mutex_lock(&moduleMutex);
	g_initFlag = state;
	pthread_mutex_unlock(&moduleMutex);
}

static bool getModuleState(void)
{
	bool state;
	pthread_mutex_lock(&moduleMutex);
	state = g_initFlag;	
	pthread_mutex_unlock(&moduleMutex);
	return state;
}

static void setDeInitStatus(bool state)
{
	pthread_mutex_lock(&moduleMutex);
	g_deinitReq = state;
	pthread_mutex_unlock(&moduleMutex);
}

static bool getDeInitStatus(void)
{
	bool state;
	pthread_mutex_lock(&moduleMutex);
	state = g_deinitReq;
	pthread_mutex_unlock(&moduleMutex);
	return state;
}

static void incrementAlertThreadCount(void)
{
	pthread_mutex_lock(&moduleMutex);
	altertThreadCount++;
	pthread_mutex_unlock(&moduleMutex);
}

static void decrementAlertThreadCount(void)
{
	pthread_mutex_lock(&moduleMutex);
	altertThreadCount--;
	pthread_mutex_unlock(&moduleMutex);
}

static uint8_t getAlertThreadCount(void)
{
	uint8_t threadCount = 0;
	pthread_mutex_lock(&moduleMutex);
	threadCount = altertThreadCount;
	pthread_mutex_unlock(&moduleMutex);
	return threadCount;
}

static void setSendZipToCloudStatus(bool state)
{
	pthread_mutex_lock(&moduleMutex);
	g_cloudThreadExist = state;
	pthread_mutex_unlock(&moduleMutex);
}

static bool getSendZipToCloudStatus(void)
{
	bool state;
	pthread_mutex_lock(&moduleMutex);
	state = g_cloudThreadExist;
	pthread_mutex_unlock(&moduleMutex);
	return state;
}

static bool getAlertDataToCloudThreadAliveStatus(void)
{
        bool status;
        pthread_mutex_lock(&alertDataToCloudThreadAliveLock);
        status = g_alertDataToCloudThreadAliveStatus;
        pthread_mutex_unlock(&alertDataToCloudThreadAliveLock);
        return status;
}

static void setAlertDataToCloudThreadAliveStatus(bool value)
{
        pthread_mutex_lock(&alertDataToCloudThreadAliveLock);
        g_alertDataToCloudThreadAliveStatus = value;
        pthread_mutex_unlock(&alertDataToCloudThreadAliveLock);
}

static bool getSendZipToCloudThreadAliveStatus(void)
{
        bool status;
        pthread_mutex_lock(&sendZipToCloudThreadAliveLock);
        status = g_sendZipToCloudThreadAliveStatus;
        pthread_mutex_unlock(&sendZipToCloudThreadAliveLock);
        return status;
}

static void setSendZipToCloudThreadAliveStatus(bool value)
{
        pthread_mutex_lock(&sendZipToCloudThreadAliveLock);
        g_sendZipToCloudThreadAliveStatus = value;
        pthread_mutex_unlock(&sendZipToCloudThreadAliveLock);
}

bool getProcessEventFromMTThreadAliveStatus(void)
{
        bool status;
        pthread_mutex_lock(&processEventFromMTThreadAliveLock);
        status = g_processEventFromMTThreadAliveStatus;
        pthread_mutex_unlock(&processEventFromMTThreadAliveLock);
        return status;
}

void setProcessEventFromMTThreadAliveStatus(bool value)
{
        pthread_mutex_lock(&processEventFromMTThreadAliveLock);
        g_processEventFromMTThreadAliveStatus = value;
        pthread_mutex_unlock(&processEventFromMTThreadAliveLock);
}

bool sendZipToCloudCallback(void)
{
        bool status = false;
	CAB_DBG(CAB_GW_TRACE, "In func : %s", __func__);
	if( getSendZipToCloudThreadAliveStatus() == true)
	{
		status = true;
	}
	
	setSendZipToCloudThreadAliveStatus(false);

	return status;
}

static void *sendZipToCloud(void *arg)
{
	CAB_DBG(CAB_GW_TRACE,"==== %d %s ====", __LINE__, __func__);
	char data[sizeof(fileStorageInfo_t)];
	pthread_detach(pthread_self());
	returnCode_e retValue;
	uint8_t retryCount = 0;
	returnCode_e retCode = GEN_SUCCESS;
	bool uploadStatus = false;
	bool isLatestMandatoryConfigFile = false;

	watchDogTimer_t newWatchdogNode;
	
	newWatchdogNode.threadId = syscall(SYS_gettid);
	newWatchdogNode.CallbackFn = sendZipToCloudCallback;
	
	CAB_DBG(CAB_GW_DEBUG, "Send zip to cloud thread id : %ld", syscall(SYS_gettid));
	retCode = threadRegister(&newWatchdogNode);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "Send zip to cloud thread registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_REGISTRATION_FAIL, GW_ERR, "Send zip to cloud thread registration failed");
        }

	while( (getPackageFile(data) != MISC_NO_PENDING_EVENT) && (retryCount < MAX_CLOUD_RETRY) ) {
		retValue = connectToCloud(ZIP_POST_DATA, data);
		/*	if successfully sent then delete zip	*/
		if(retValue == GEN_SUCCESS) {
			deletePackageFile(data);
		}
		else {
			retryCount++;
		}
		setSendZipToCloudThreadAliveStatus(true);	// for watchDog timer
	}

	if ( g_zipType == CSV_DATA ) {
		retryCount = 0;
		while( (getCfgPackageFile(data, &isLatestMandatoryConfigFile) != MISC_NO_PENDING_EVENT) && (retryCount < MAX_CLOUD_RETRY) ) {
			retValue = connectToCloud(ZIP_POST_DATA, data);
			/*	if successfully sent then delete zip	*/
			if(retValue == GEN_SUCCESS) {
				deleteCfgPackageFile(data);

				if (isLatestMandatoryConfigFile == true)
				{
					/* Set the cofnig file upload status */
					uploadStatus = true;
					(*g_notifyMTCB)(DATA_CONFIG_ZIP, (void *)&uploadStatus);
				}
			}
			else {
				retryCount++;
			}
			setSendZipToCloudThreadAliveStatus(true);	 // for watchDog timer
		}
	}

	setSendZipToCloudStatus(false);

	retCode = threadDeregister(newWatchdogNode.threadId);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "Send zip to cloud thread de-registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "Send zip to cloud thread de-registration failed");
        }
	return NULL;
}

bool alertDataToCloudCallback(void)
{
        bool status = false;
	CAB_DBG(CAB_GW_TRACE, "In func : %s", __func__);
	if( getAlertDataToCloudThreadAliveStatus() == true)
	{
		status = true;
	}
	
	setAlertDataToCloudThreadAliveStatus(false);

	return status;
}

static void *alertDataToCloud(void *arg)
{
	returnCode_e retCode = GEN_SUCCESS;
	pthread_detach(pthread_self());
	returnCode_e retValue = DATA_SEND_FAIL;
	watchDogTimer_t newWatchdogNode;
	
	newWatchdogNode.threadId = syscall(SYS_gettid);
	newWatchdogNode.CallbackFn = alertDataToCloudCallback;
	
	CAB_DBG(CAB_GW_INFO, "Alert data to cloud thread id : %ld", syscall(SYS_gettid));
	retCode = threadRegister(&newWatchdogNode);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "Alert data to cloud thread registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_REGISTRATION_FAIL, GW_ERR, "Alert data to cloud thread registration failed");
        }

	uint8_t retryCount = 0;
	do {
		retValue = connectToCloud(JSON_POST_DATA, arg);
		retryCount++;
	} while( (retValue != GEN_SUCCESS) && (retryCount < MAX_CLOUD_RETRY) );

	setAlertDataToCloudThreadAliveStatus(true);

	if (getAlertThreadCount() != 0) {
		decrementAlertThreadCount();
	}
	else {
		CAB_DBG(CAB_GW_ERR, "Tried to decrement the 0 Alert thread count");
	}

	retCode = threadDeregister(newWatchdogNode.threadId);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "Alert data to cloud thread de-registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "Alert data to cloud thread de-registration failed");
        }
	return NULL;
}

bool processEventFromMTCallback(void)
{
        bool status = false;
	sdcMsg_t systemData;

	CAB_DBG(CAB_GW_TRACE, "In func : %s", __func__);
	if( getProcessEventFromMTThreadAliveStatus() == true)
	{
		status = true;
	}
	
	setProcessEventFromMTThreadAliveStatus(false);

	memset(&systemData, '\0', sizeof(sdcMsg_t));
	systemData.mtype = WATCHDOG_EV;

	pthread_mutex_lock(&msgQueMutex);
	if(msgsnd(msqid, &systemData, sizeof(systemData.mtype), IPC_NOWAIT) < 0) {
		pthread_mutex_unlock(&msgQueMutex);
		status = false;
	}
	pthread_mutex_unlock(&msgQueMutex);
	return status;
}

void *processEventFromMT(void *arg)
{
	sdcMsg_t systemData;
	ssize_t retValue;
	bool deinitRequest = false;		/*	Deinit api will send exit command, to exit thread	*/
	pthread_t sendZipThreadID;		/*	Thread for sending zip to cloud		*/
	pthread_t alertDataThreadID;	/*	Thread for sending alert to cloud	*/
	returnCode_e retCode = GEN_SUCCESS;

	watchDogTimer_t newWatchdogNode;
	
        CAB_DBG(CAB_GW_TRACE, "In func : %s", __func__);

	newWatchdogNode.threadId = syscall(SYS_gettid);
	newWatchdogNode.CallbackFn = processEventFromMTCallback;
	
	retCode = threadRegister(&newWatchdogNode);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "Process event from MT thread registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_REGISTRATION_FAIL, GW_ERR, "Process event from MT thread registration failed");
        }
	do {
		retValue = msgrcv(msqid, &systemData, (sizeof(systemData.mtext)+sizeof(zipType_e)), 0, 0);
		if(retValue >= 0) {
			switch(systemData.mtype) {
				case ALERT_EV:
				{
					eventSet(systemData.mtext, TPMS);
                    CAB_DBG(CAB_GW_INFO,"## Send alert data to cloud ##");
					if(pthread_create(&alertDataThreadID, NULL, alertDataToCloud, systemData.mtext) == 0) {
						incrementAlertThreadCount();
					}
					break;
				}
				case TRANSMIT_EV:
				{
					time_t time_value;
					memcpy(&time_value, systemData.mtext, sizeof(time_t));
					createFilePackage(time_value, systemData.zipType);
					if (systemData.zipType == CSV_DATA) {
						createCfgFilePackage(time_value);
					}
					CAB_DBG(CAB_GW_INFO,"## Send zip data to cloud ##");
					if(getSendZipToCloudStatus() == false) {
						if(pthread_create(&sendZipThreadID, NULL, sendZipToCloud, NULL) == 0) {
							setSendZipToCloudStatus(true);
						}
					}
					break;
				}
				case GROUP_EV:
				{
					time_t time_value;
					memcpy(&time_value, systemData.mtext, sizeof(time_t));
					createFilePackage(time_value, systemData.zipType);
					if (systemData.zipType == CSV_DATA) {
						createCfgFilePackage(time_value);
					}
					break;
				}
				case DEINIT_EV:
				{
					deinitRequest = true;
					break;
				}
				case WATCHDOG_EV:
				{
					setProcessEventFromMTThreadAliveStatus(true);
					break;
				}
				default :
				{
					CAB_DBG(CAB_GW_DEBUG, "SDC RCV DFL");
					break;
				}
			}
		}
	} while(deinitRequest == false);

	retCode = threadDeregister(newWatchdogNode.threadId);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "Process event from MT thread de-registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "Process event from MT thread de-registration failed");
        }

	pthread_exit(NULL);
}

returnCode_e initSDC(uint16_t totalZipPackage, uploadzipNotifCB notifyCB, char *swVersion, key_t key, char *dataDir)
{
	returnCode_e retValue;

	if (dataDir == NULL || notifyCB == NULL) {
		CAB_DBG(CAB_GW_ERR, "Invalid arg");
		return MODULE_INIT_FAIL;
	}

	if(getModuleState() == true) {
		return SDC_ALREADY_INIT_ERR;
	}

	CAB_DBG(CAB_GW_INFO, "Initializing SDC module");

	retValue = initDataStorageModule(totalZipPackage, swVersion, dataDir);
	if(retValue != GEN_SUCCESS) {
		goto error;
	}

	pthread_mutex_lock(&msgQueMutex);
	msqid = msgget(key, PERMS | IPC_CREAT);
	if(msqid == -1) {
		pthread_mutex_unlock(&msgQueMutex);
		goto error;
	}

	if (notifyCB != NULL)
	{
		g_notifyMTCB = notifyCB;
	}
	pthread_mutex_unlock(&msgQueMutex);

	if(pthread_create(&g_processEventThreadID, NULL, processEventFromMT, NULL) != 0) {
		goto error;
	}
	setModuleState(true);

	return GEN_SUCCESS;

error:
	return SDC_INIT_ERR;
}

returnCode_e deInitSDC()
{
	returnCode_e retValue;

	if(getModuleState() == false) {
		return SDC_ALREADY_DEINIT_ERR;
	}

	CAB_DBG(CAB_GW_INFO, "Deinitializing SDC module");

	setDeInitStatus(true);

	reportEventToSDC(DEINIT_EV, NULL, CSV_DATA);

	pthread_join(g_processEventThreadID, NULL);

	pthread_mutex_lock(&msgQueMutex);
	if (msgctl(msqid, IPC_RMID, NULL) == -1) {
		pthread_mutex_unlock(&msgQueMutex);
		goto error;
	}
	pthread_mutex_unlock(&msgQueMutex);

	while(getSendZipToCloudStatus() == true) {
		usleep(1000);	
	}

	while(getAlertThreadCount() != 0) {
		usleep(1000);
	}

	/* The callback function is used by sendZiptoCloud(), so deinitialize afterwards */
	g_notifyMTCB = NULL;

	retValue = deInitDataStorageModule();
	if(retValue != GEN_SUCCESS) {
		goto error;
	}

	setModuleState(false);

	return GEN_SUCCESS;

error:
	return SDC_DEINIT_ERR;
}

returnCode_e storeDataToSDC(void *data, sysDataType_e eventType)
{
	if(getModuleState() == false) {
		return SDC_ALREADY_DEINIT_ERR;
	}

	if(data == NULL) {
        CAB_DBG(CAB_GW_ERR, "NULL poiting error.");
        return GEN_NULL_POINTING_ERROR;
	}

	CAB_DBG(CAB_GW_DEBUG, "SDC setting event");
	return eventSet(data, eventType);
}

returnCode_e reportEventToSDC(eventType_e event, const void *const data, zipType_e zipType)
{
	sdcMsg_t systemData;
	size_t systemDataSize;

	if(getModuleState() == false) {
		return SDC_ALREADY_DEINIT_ERR;
	}

	g_zipType = zipType;

	switch(event) {
		case ALERT_EV:
		{
			/* For ALERT we only use TPMS event data, no zip type needed */
			CAB_DBG(CAB_GW_INFO, "## In reportEventToSDC ##");
			systemDataSize = sizeof(tpmsEvent_t)+sizeof(zipType_e);
			memcpy(systemData.mtext, data, systemDataSize);
			break;
		}
		case TRANSMIT_EV:
		case GROUP_EV:
		{
			systemData.zipType = zipType;
			systemDataSize = sizeof(time_t) + sizeof(zipType_e);
			memcpy(systemData.mtext, data, sizeof(time_t));
			break;
		}
		case DEINIT_EV:
		{
			systemDataSize = 0;
			break;
		}
		default :
		{
			CAB_DBG(CAB_GW_DEBUG, "SDC event : %d", event);
			return GEN_INVALID_TYPE;
			break;
		}
	}

	systemData.mtype = event;

	pthread_mutex_lock(&msgQueMutex);
	if(msgsnd(msqid, &systemData, systemDataSize, IPC_NOWAIT) < 0) {
		pthread_mutex_unlock(&msgQueMutex);
		return DATA_SEND_FAIL;
	}
	pthread_mutex_unlock(&msgQueMutex);

	return GEN_SUCCESS;
}

returnCode_e deleteConfigAndDataStorage(char *path)
{
    DIR *directory;
    struct dirent *dir;
    returnCode_e retVal = GEN_SUCCESS;
    char *buf;
    size_t len;
    int status = -1;


    directory = opendir(path);

    if(directory)
    {
	status = 0;
        while ((!status) && ((dir = readdir(directory)) != NULL))
	{
		if (!strcmp(dir->d_name, ".") || !strcmp(dir->d_name, ".."))
			continue;

		len = strlen(path) + strlen(dir->d_name) + 2; 
		buf = malloc(len);

		if (buf)
		{
			struct stat statbuf;
			snprintf(buf, len, "%s/%s", path, dir->d_name);
			if (!stat(buf, &statbuf))
			{
				if (S_ISDIR(statbuf.st_mode))
				{
					retVal = deleteConfigAndDataStorage(buf);
				}
				else
				{
					CAB_DBG(CAB_GW_INFO,"Deleting File : %s", buf);
					retVal = unlink(buf);
					usleep(100);
					sync();
				}
			}

			free(buf);
		}

		status = retVal;
	}

        closedir(directory);

    }
    if(retVal != 0)
	retVal = GEN_SYSCALL_FAIL;
    return retVal;
}

