/************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file dataStorage.h
 * @brief (Header file for Data storage module.)
 *
 * @Author     - VT
 *************************************************************************************************
 * History
 *
 * July/24/2018, VT , First Draft
 *************************************************************************************************/

#ifndef _SYSLOG_STORAGE_H_
#define _SYSLOG_STORAGE_H_

/****************************************
 ************ INCLUDES ******************
 ****************************************/
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <fcntl.h>
#include <time.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include "error.h"

/****************************************
 ************* DEFINES ******************
 ****************************************/
/* File paths for each data file */
#define FILE_PATH_SYSLOG_RAM        "/var/syslog/messages"
/* File path for event summary data */
#define FILE_PATH_EVENT_SUMM        "%s/eventSummary.txt"
/* Directory path to locate all data file */
#define DIR_PATH_SYSLOG             "/media/userdata/sysLog"
/* Zip status file for syslog */
#define SYSLOG_ZIP_STATUS_FILE      "/home/root/syslog-zip-status-file.txt"
/* Log file name to be zipped and merged into */
#define SYSLOG_FILE_NAME            "%s/log"

/*Max directory name length*/
#define MAX_DIRNAME_LEN_SYS		    (200)
#define MAX_FILENAME_LEN_SYS 		    (300)

#define MAX_SYSLOG_PACKAGE				(7)
/* Zip file name format */
#define LOG_ZIP_FILE_NAME               "CG_$(date +%%s)_log.zip"

/* Max length of filename and command */
#define MAX_LEN_COMMAND_SYS             (300)



/****************************************
 ************* ENUMS ********************
 ****************************************/
typedef enum packagingEvent {
    INIT_TIME = 0,
    DAY_CHANGE,
    MAX_EVENT
} packagingEvent_e;

/****************************************
 ************ STRUCTURES ****************
 ****************************************/
/* This stucture is used to define summary data for different sensors */
typedef struct logSummary {
    uint16_t zipFileCnt;
} logSummary_t;

/****************************************
 ******** GLOBAL VARIABLES **************
 ****************************************/
logSummary_t g_logeventSumm;      //event summary variable which contains information for all sensors

/****************************************
 ******** EXTERN VARIABLES **************
 ****************************************/

/****************************************
 ************* CALLBACKS ****************
 ****************************************/

/****************************************
 ********* FUNCTION PROTOTYPES **********
 ****************************************/

/******************************************************************
 * @brief  (This API is used to read the content of status file.
 *
 * @param[IN] fileName* (Pointer to File Name)
 * @param[OUT] status*  (Pointer to status of command)
 *
 * @return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e readStatusFile(char *, uint8_t *);


/******************************************************************
 *@brief  (This API is used to initialize data storage module)
 *
 *@param[IN] uint16_t (Indicates maximum number of storage package file.)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e initSysLogModule(uint16_t noOfPackage);


/******************************************************************
 *@brief  (This API is used to Deinitialize data storage module)
 *
 *@param[IN] void  (No arguments passed)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e deInitSyslogModule(void);

/******************************************************************
 *@brief  (This API is used to create a file package to send data on cloud)
 *
 *@param[IN] packagingEvent_e (Indicates packaging event occurance time.)
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e createFilePackage_Syslog(packagingEvent_e);

/******************************************************************
 *@brief  (This API is used to create a file package to send data on cloud)
 *
 *@param[IN] None
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e getAndMergeLogsFromRAM();

/******************************************************************
 *@brief  (This API is used to get the path of package file to access that file)
 *
 *@param[IN] None
 *@param[OUT] void * (Indicates pointer to get path of package file)
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
#if 0
returnCode_e getPackageFile(void *);
#endif

/******************************************************************
 *@brief  (This API is used to delete all package files from dataStorage directory)
 *
 *@param[IN] None
 *@param[OUT] None
 *
 *@return returnCode_e (It returns the type of error (i.e. SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
returnCode_e deleteSyslogPackage(void * data);

#endif      /*_SYSLOG_STORAGE_H_*/
