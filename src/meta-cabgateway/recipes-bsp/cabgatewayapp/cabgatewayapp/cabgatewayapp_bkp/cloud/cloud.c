#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/evp.h>

#include "cJSON.h"
#include "curl.h"
#include "cloud.h"
#include "commonData.h"
#include "dataStorage.h"
#include "dataControl.h"

#define MAX_LEN_SHA256_KEY 32       /**< Max len of shared secret key */

static cloudConStatusNotifCB g_notifFunPtr;

static int ver1 = 0, ver2 = 0, ver3 = 0, ver4 = 0;

bool isConfigChangefromCloud;

static cloudInfo_t g_cloudInfo = {
	.cloudInfoLock = PTHREAD_MUTEX_INITIALIZER,
	.isInitDone = false,
	.url = "",
	.apiToken = "",
	.gatewayID = "",
};

static void setOTAInitState(bool state)
{
	pthread_mutex_lock(&g_cloudInfo.cloudInfoLock);
	g_cloudInfo.isInitDone = state;
	pthread_mutex_unlock(&g_cloudInfo.cloudInfoLock);
}

static bool getOTAInitState(void)
{
	bool state;
	pthread_mutex_lock(&g_cloudInfo.cloudInfoLock);
	state = g_cloudInfo.isInitDone;
	pthread_mutex_unlock(&g_cloudInfo.cloudInfoLock);
	return state;
}

/** @brief convert json data to system data
 *
 * @param[in] json data
 * @param[out] system data
 *
 * @return status code.
 */
static returnCode_e jsonToSystemPayload(restApiType_e restApiType,  const char *json, void *data)
{
	FUNC_ENTRY

	cJSON *root;
	cJSON *item;
       
	returnCode_e retCode;
	if (!json)
		return GEN_NULL_POINTING_ERROR;

	switch (restApiType) {
		case ZIP_POST_DATA:
		{
			callBackNotification_t *callBackData = (callBackNotification_t *) data;
			otaInfo_t *otaInfo = (otaInfo_t *) &(callBackData->otaInfo);
			char statusCode[5];

			if (!otaInfo)
				return GEN_NULL_POINTING_ERROR;

			root = cJSON_Parse(json);
			if (!root)
				return DATA_INVALID;

			char *payloadAddr = cJSON_PrintUnformatted(root);
		        CAB_DBG(CAB_GW_INFO, "----------------> %s <---------------\n", payloadAddr);

			cJSON *configChange = cJSON_GetObjectItem(root,"config_change");
			if (configChange)
			{
				configChangeInfo_t *configChangeInfo = (configChangeInfo_t *) &(callBackData->configChangeInfo);
				char *configString = cJSON_PrintUnformatted(configChange);
				uint8_t total_params = 0;
				cJSON *value;
                                
				CAB_DBG(CAB_GW_INFO, "Config change request from cloud : %s", configString);
				free(configString);
                                configChangeInfo->configChangeAvail = 1 ;

				item = cJSON_GetObjectItem(configChange, "total_parameters");
				if (cJSON_IsNumber(item)) {
                                        isConfigChangefromCloud = true;
					total_params = item->valueint;
				        CAB_DBG(CAB_GW_INFO, "total_params : %d\n", total_params);

					/* Get the change parameter array */
					cJSON *params = cJSON_GetObjectItem(configChange, "parameters_array");
					for (uint8_t i=0; i < total_params; i++) {
                                                CAB_DBG(CAB_GW_INFO, "INSIDE PARAM ARRAY\n");
						cJSON *configParam = cJSON_GetArrayItem(params, i);
                                                cJSON *mainParam;
                                                cJSON *subParam;
                                                cJSON *tpmsArray;
                                                cJSON *haloArray;
						if ((value = cJSON_GetObjectItem(configParam, "reboot"))) {
                                                        rDiagSendMessage(CABAPP_GEN_WARN, GW_WARNING, "reboot requested from cloud - rebooting");
							rebootSystem();
						}
						if ((value = cJSON_GetObjectItem(configParam, "retrive_config"))) {
							time_t timeData;

							time(&timeData);

				                        CAB_DBG(CAB_GW_INFO, "Config Retrive received\n");
							retCode = reportEventToSDC(TRANSMIT_EV, (void *)&timeData, CFG_DATA);
							if ( retCode != GEN_SUCCESS ) {
								CAB_DBG(CAB_GW_ERR, "Report event to SDC failed with code %d", retCode);
								continue;
							}

							/*
							   createFilePackage(timeData, CFG_DATA);
							   if(getSendZipToCloudStatus() == false) {
							   if(pthread_create(&sendZipThreadID, NULL, sendZipToCloud, NULL) == 0) {
							   setSendZipToCloudStatus(true);
							   }
							   }
							 */
						}
						if ((value = cJSON_GetObjectItem(configParam, "set_point"))) {
							cJSON *setPoint = cJSON_GetObjectItem(value, "steer");

							if (cJSON_IsNumber(setPoint)) {
								configChangeInfo->tpmsThreshold[0] = setPoint->valueint;
								configChangeInfo->setPointChangeAvailable = true;
							}

							setPoint = cJSON_GetObjectItem(value, "drive");
							if (cJSON_IsNumber(setPoint)) {
								configChangeInfo->tpmsThreshold[1] = setPoint->valueint;
								configChangeInfo->setPointChangeAvailable = true;
							}
						}
						if ((value = cJSON_GetObjectItem(configParam, "fleet_id"))){
							if (value && value->valuestring && strlen(value->valuestring) <= MAX_LEN_FLEET_ID) {
								snprintf(configChangeInfo->fleetID, sizeof(configChangeInfo->fleetID), "%s", value->valuestring);
								configChangeInfo->fleetIDChangeAvailable = true;
							}
						}
                                                if((mainParam = cJSON_GetObjectItem(configParam,"unpair_tpms"))) {
                                                        configChangeInfo->unpairTpmsAvail = 1;
				                        CAB_DBG(CAB_GW_INFO, "Request to UNPAIR TPMS\n");
                                                        subParam = cJSON_GetObjectItem(mainParam, "no_of_tpms");
                                                        configChangeInfo->no_of_tpms_unpair = subParam->valueint;
				                        CAB_DBG(CAB_GW_INFO, "No OF TPMS to UNPAIR : %d\n", configChangeInfo->no_of_tpms_unpair);
                                                        tpmsArray = cJSON_GetObjectItem(mainParam,"tpms_array");
                                                        for(uint8_t i = 0; i < configChangeInfo->no_of_tpms_unpair; i++)
							{
                                                                cJSON *tpmsElement = cJSON_GetArrayItem(tpmsArray, i); 
				                                CAB_DBG(CAB_GW_INFO, "Extracting sensor ID\n");
								value = cJSON_GetObjectItem(tpmsElement, "sensor_id");
								if (cJSON_IsNumber(value)) {
                                                                	configChangeInfo->unpairTpmsSID[i] = value->valueint;
                                                                }
				                                CAB_DBG(CAB_GW_INFO, "SensorID to unpair : %d\n", configChangeInfo->unpairTpmsSID[i]);
							}
                                                }
						if ((mainParam = cJSON_GetObjectItem(configParam, "vehicle_info"))) {
                                                        configChangeInfo->vehicleInfoChange = 1;
				                        CAB_DBG(CAB_GW_INFO, "Extracting type\n");
                                                        if ((value = cJSON_GetObjectItem(mainParam, "type"))) {
								if (value && value->valuestring && strlen(value->valuestring) <= MAX_LEN_VEH_TYPE) {
                                                                	snprintf(configChangeInfo->vehicleInfo.vehicleType , sizeof(configChangeInfo->vehicleInfo.vehicleType), "%s", value->valuestring);
                                                                }
							}
                                                        CAB_DBG(CAB_GW_INFO, "Type : %s\n", configChangeInfo->vehicleInfo.vehicleType);
							if ((value = cJSON_GetObjectItem(mainParam, "vin"))) {
								if (value && value->valuestring && strlen(value->valuestring) <= MAX_LEN_VEH_NO) {
                                                                	snprintf(configChangeInfo->vehicleInfo.vehicleVin, sizeof(configChangeInfo->vehicleInfo.vehicleVin), "%s", value->valuestring);
								}
                                                        }
                                                        CAB_DBG(CAB_GW_INFO, "vin : %s\n", configChangeInfo->vehicleInfo.vehicleVin);
							if ((value = cJSON_GetObjectItem(mainParam, "fleet_id"))) {
								if (value && value->valuestring && strlen(value->valuestring) <= MAX_LEN_FLEET_ID) {
                                                                	snprintf(configChangeInfo->vehicleInfo.fleetID, sizeof(configChangeInfo->vehicleInfo.fleetID), "%s", value->valuestring);
								}
                                                        }
                                                        CAB_DBG(CAB_GW_INFO, "fleet_id : %s\n", configChangeInfo->vehicleInfo.fleetID);
							if ((value = cJSON_GetObjectItem(mainParam, "customer_id"))) {
								if (value && value->valuestring && strlen(value->valuestring) <= MAX_LEN_FLEET_NAME) {
                                                                	snprintf(configChangeInfo->vehicleInfo.customerID, sizeof(configChangeInfo->vehicleInfo.customerID), "%s", value->valuestring);
								}
                                                        }
                                                        CAB_DBG(CAB_GW_INFO, "customer_id : %s\n", configChangeInfo->vehicleInfo.customerID);
						}
                                                if((mainParam = cJSON_GetObjectItem(configParam,"tpms_info"))) {
                                                        configChangeInfo->tpmsInfoChange = 1;
				                        CAB_DBG(CAB_GW_INFO, "Request to ADD TPMS\n");

                                                        subParam = cJSON_GetObjectItem(mainParam, "no_of_tpms");
                                                        configChangeInfo->no_of_tpms = subParam->valueint;
				                        CAB_DBG(CAB_GW_INFO, "no of TPMS to add %d\n", configChangeInfo->no_of_tpms);
                                                        tpmsArray = cJSON_GetObjectItem(mainParam,"tpms_array");

							for(uint8_t i = 0; i < configChangeInfo->no_of_tpms; i++) {
                                                                cJSON *tpmsElement = cJSON_GetArrayItem(tpmsArray, i); 
				                                CAB_DBG(CAB_GW_INFO, "Extracting sensor ID\n");
								value = cJSON_GetObjectItem(tpmsElement, "id");
								configChangeInfo->tpmsInfo[i].sensorID = value->valueint;
				                                CAB_DBG(CAB_GW_INFO, "SensorID to Add : %d\n", configChangeInfo->tpmsInfo[i].sensorID);

								if ((value = cJSON_GetObjectItem(tpmsElement, "pn"))) {
									if (value && value->valuestring && strlen(value->valuestring) <= MAX_LEN_SENSOR_PART_NO) {
										snprintf(configChangeInfo->tpmsInfo[i].sensorPN, sizeof(configChangeInfo->tpmsInfo[i].sensorPN), "%s", value->valuestring);
									}
								}
				                                CAB_DBG(CAB_GW_INFO, "part number : %s\n", configChangeInfo->tpmsInfo[i].sensorPN);
								if ((value = cJSON_GetObjectItem(tpmsElement, "mn"))) {
									if (value && value->valuestring && strlen(value->valuestring) <= MAX_LEN_SENSOR_MODEL) {
										snprintf(configChangeInfo->tpmsInfo[i].sensorModel, sizeof(configChangeInfo->tpmsInfo[i].sensorPN), "%s", value->valuestring);
									}
								}
				                                CAB_DBG(CAB_GW_INFO, "sensor model : %s\n", configChangeInfo->tpmsInfo[i].sensorModel);
								value = cJSON_GetObjectItem(tpmsElement, "side");
								if (cJSON_IsNumber(value)) {
									configChangeInfo->tpmsInfo[i].vehicleSide = (vehicleSide_e)value->valueint;
								}
				                                CAB_DBG(CAB_GW_INFO, "Vehicle Side : %d\n", configChangeInfo->tpmsInfo[i].vehicleSide);
								value = cJSON_GetObjectItem(tpmsElement, "axle");
								if (cJSON_IsNumber(value)) {
									configChangeInfo->tpmsInfo[i].axleNum = (uint8_t)value->valueint;
								}
				                                CAB_DBG(CAB_GW_INFO, "Axle Num : %d\n", configChangeInfo->tpmsInfo[i].axleNum);
								value = cJSON_GetObjectItem(tpmsElement, "type");
								if (cJSON_IsNumber(value)) {
									configChangeInfo->tpmsInfo[i].attachedType = (attachedType_e)value->valueint;
								}
				                                CAB_DBG(CAB_GW_INFO, "Type : %d\n", configChangeInfo->tpmsInfo[i].attachedType);
								value = cJSON_GetObjectItem(tpmsElement, "position");
								if (cJSON_IsNumber(value)) {
									configChangeInfo->tpmsInfo[i].tyrePos = value->valueint;
								}
				                                CAB_DBG(CAB_GW_INFO, "Pos : %d\n", configChangeInfo->tpmsInfo[i].tyrePos);
							}
						}
                                                if((mainParam = cJSON_GetObjectItem(configParam,"tire_info"))) {
                                                        configChangeInfo->tireInfoChange = 1;
				                        CAB_DBG(CAB_GW_INFO, "Request to ADD Tire Info\n");
                                                        subParam = cJSON_GetObjectItem(mainParam, "no_of_tires");
							if (cJSON_IsNumber(subParam)) {
                                                        	configChangeInfo->no_of_tire = subParam->valueint;
							}
				                        CAB_DBG(CAB_GW_INFO, "no of tires info to add %d\n", configChangeInfo->no_of_tire);
                                                        tpmsArray = cJSON_GetObjectItem(mainParam,"info_array");
							for(uint8_t i = 0; i < configChangeInfo->no_of_tire; i++) {
                                                                cJSON *tireElement = cJSON_GetArrayItem(tpmsArray, i); 
                                                                configChangeInfo->tireInfo[i].isTyreDetailConfigured = false;
				                                CAB_DBG(CAB_GW_INFO, "\n");
								value = cJSON_GetObjectItem(tireElement, "pos");
								if (cJSON_IsNumber(value)) {
									configChangeInfo->tireInfo[i].tyrePos = value->valueint;
								}
								value = cJSON_GetObjectItem(tireElement, "axle");
								if (cJSON_IsNumber(value)) {
									configChangeInfo->tireInfo[i].axleNum = (uint8_t)value->valueint;
								}
								value = cJSON_GetObjectItem(tireElement, "side");
								configChangeInfo->tpmsInfo[i].vehicleSide = (vehicleSide_e)value->valueint;
								if(value = cJSON_GetObjectItem(tireElement, "make")) {
									if (value && value->valuestring && strlen(value->valuestring) <= MAX_LEN_TYRE_MAKE_STR) {
										snprintf(configChangeInfo->tireInfo[i].tyreMake, sizeof(configChangeInfo->tireInfo[i].tyreMake),"%s", value->valuestring);
									}
								}
								if(value = cJSON_GetObjectItem(tireElement, "model")) {
									if (value && value->valuestring && strlen(value->valuestring) <= MAX_LEN_TYRE_MODEL) {
										snprintf(configChangeInfo->tireInfo[i].tyreModel, sizeof(configChangeInfo->tireInfo[i].tyreModel),"%s", value->valuestring);
                                                                        }
                                                                }
								if(value = cJSON_GetObjectItem(tireElement, "size")) {
                                                                       if (value && value->valuestring && strlen(value->valuestring) <= MAX_LEN_TYRE_MODEL) {
                                                                       }
                                                                }
								if(value = cJSON_GetObjectItem(tireElement, "tyre_width")) {
                                                                       if (value && value->valuestring && strlen(value->valuestring) <= MAX_LEN_TYRE_WIDTH) {
										snprintf(configChangeInfo->tireInfo[i].tyreWidth, sizeof(configChangeInfo->tireInfo[i].tyreWidth),"%s", value->valuestring);
                                                                       }
                                                                }
								if(value = cJSON_GetObjectItem(tireElement, "aspect_ratio")) {
                                                                       if (value && value->valuestring && strlen(value->valuestring) <= MAX_LEN_RATIO) {
										snprintf(configChangeInfo->tireInfo[i].ratio, sizeof(configChangeInfo->tireInfo[i].ratio),"%s", value->valuestring);
                                                                       }
                                                                }
                                                                if(value = cJSON_GetObjectItem(tireElement, "loadRating")) {
                                                                       if (value && value->valuestring && strlen(value->valuestring) <= MAX_LEN_LOAD_RATING) {
										snprintf(configChangeInfo->tireInfo[i].loadRating, sizeof(configChangeInfo->tireInfo[i].diameter),"%s", value->valuestring);
                                                                       }
                                                                }
								if(value = cJSON_GetObjectItem(tireElement, "diameter")) {
                                                                       if (value && value->valuestring && strlen(value->valuestring) <= MAX_LEN_DIAMETER) {
										snprintf(configChangeInfo->tireInfo[i].diameter, sizeof(configChangeInfo->tireInfo[i].diameter),"%s", value->valuestring);
								       }
								}
								if(value = cJSON_GetObjectItem(tireElement, "tread_depth")) {
									if (value && value->valuestring && strlen(value->valuestring) <= MAX_LEN_TREAD_DEPTH) {
										snprintf(configChangeInfo->tireInfo[i].treadDepthHistory[0].treadDepthValue, sizeof(configChangeInfo->tireInfo[i].treadDepthHistory[0].treadDepthValue), "%s", value->valuestring);
									}
								}
								value = cJSON_GetObjectItem(tireElement, "veh_mil");
								configChangeInfo->tireInfo[i].treadDepthHistory[0].vehicleMileage = value->valueint;
							}
                                                }
                                                if((mainParam = cJSON_GetObjectItem(configParam,"attach_halo"))) {
                                                        configChangeInfo->halo_info_attach = 1;
				                        CAB_DBG(CAB_GW_INFO, "Request to ADD HALO Sensors\n");
                                                        subParam = cJSON_GetObjectItem(mainParam, "nos_halo_attach");
							if (cJSON_IsNumber(subParam)) {
                                                        	configChangeInfo->no_of_halo_attach = subParam->valueint;
							}
				                        CAB_DBG(CAB_GW_INFO, "no of halos to add %d\n", configChangeInfo->no_of_halo_attach);
                                                        haloArray = cJSON_GetObjectItem(mainParam,"halo_array");
							for(uint8_t i = 0; i < configChangeInfo->no_of_halo_attach; i++) {
				                                CAB_DBG(CAB_GW_INFO, "---------Extracting halo element-------\n");
                                                                cJSON *haloElement = cJSON_GetArrayItem(haloArray, i); 
				                                CAB_DBG(CAB_GW_INFO, "---------Extracting serial no-------\n");
								if(value = cJSON_GetObjectItem(haloElement, "serial_no")) {
                                                                       if (value && value->valuestring && strlen(value->valuestring) <= MAX_LEN_HALO_SN) {
										snprintf(configChangeInfo->halos[i].serialNo, sizeof(configChangeInfo->halos[i].serialNo),"%s", value->valuestring);
                                                                       }
                                                                }
				                                CAB_DBG(CAB_GW_INFO, "---------Extracting side-------\n");
								value = cJSON_GetObjectItem(haloElement, "side");
								if (cJSON_IsNumber(value)) {
                                                                	configChangeInfo->halos[i].vehicleSide = (vehicleSide_e)value->valueint;
					                                CAB_DBG(CAB_GW_INFO, "---------Extracting axle-------\n");
								}
								value = cJSON_GetObjectItem(haloElement, "axle");
								if (cJSON_IsNumber(value)) {
				                                	CAB_DBG(CAB_GW_INFO, "---------Extracting axle passed-------\n");
	                                                                configChangeInfo->halos[i].axleNum = (uint8_t)value->valueint;
								}
				                                CAB_DBG(CAB_GW_INFO, "---------Extracting axle passed 1-------\n");
                                                        }
                                                }
				                CAB_DBG(CAB_GW_INFO, "---------Extracted all halos-------\n");
                                                if((mainParam = cJSON_GetObjectItem(configParam,"detach_halo"))) {
                                                        configChangeInfo->halo_info_detach = 1;
				                        CAB_DBG(CAB_GW_INFO, "Request to Remove HALO Sensors\n");
                                                        subParam = cJSON_GetObjectItem(mainParam, "nos_halo_detach");
							if (cJSON_IsNumber(subParam)) {
                                                        	configChangeInfo->no_of_halo_detach = subParam->valueint;
							}
				                        CAB_DBG(CAB_GW_INFO, "no of halos to remove %d\n", configChangeInfo->no_of_halo_detach);
                                                        haloArray = cJSON_GetObjectItem(mainParam,"halo_array");
							for(uint8_t i = 0; i < configChangeInfo->no_of_halo_detach; i++) {
                                                                cJSON *haloElement = cJSON_GetArrayItem(haloArray, i); 
				                                CAB_DBG(CAB_GW_INFO, "---------Extracting serial no-------\n");
								if(value = cJSON_GetObjectItem(haloElement, "serial_no")) {
                                                                       if (value && value->valuestring && strlen(value->valuestring) <= MAX_LEN_HALO_SN) {
										snprintf(configChangeInfo->halos_detach[i].serialNo, sizeof(configChangeInfo->halos_detach[i].serialNo),"%s", value->valuestring);
                                                                       }
                                                                }
                                                        }
                                                }
				                CAB_DBG(CAB_GW_INFO, "---------detached all halos-------\n");
#if 0
                                                if((mainParam = cJSON_GetObjectItem(configParam,"tpmsInfoChange"))) {
                                                        configChangeInfo->vehicleInfo.tireInfoChange = 1;
                                                }
                                                if((mainParam = cJSON_GetObjectItem(configParam,"tpmsInfoChange"))) {
                                                        configChangeInfo->vehicleInfo.tireInfoChange = 1;
                                                }
                                                if((mainParam = cJSON_GetObjectItem(configParam,"tpmsInfoChange"))) {
                                                        configChangeInfo->vehicleInfo.tireInfoChange = 1;
                                                }
#endif
					}
				}
			}

			cJSON *otaResponse = cJSON_GetObjectItem(root,"otaresponse");

			if (otaResponse)
			{
				char *otaString = cJSON_PrintUnformatted(otaResponse);
				CAB_DBG(CAB_GW_INFO, "OTA response from /sensordata : %s", otaString);
				free(otaString);

				item = cJSON_GetObjectItem(otaResponse, "statusCode");
				if (item && item->valuestring)
					snprintf(&statusCode, sizeof(statusCode), "%s", item->valuestring);

				if (strcmp(statusCode, "200") == 0)
				{
					item = cJSON_GetObjectItem(otaResponse, "version");
					if (item && item->valuestring)
						snprintf(otaInfo->updateVersion, sizeof(otaInfo->updateVersion), "%s", item->valuestring);

					item = cJSON_GetObjectItem(otaResponse, "md5sum");
					if (item && item->valuestring)
						snprintf(otaInfo->md5sum, sizeof(otaInfo->md5sum), "%s", item->valuestring);

					item = cJSON_GetObjectItem(otaResponse, "url");
					if (item && item->valuestring)
						snprintf(otaInfo->otaUrl, sizeof(otaInfo->otaUrl), "%s", item->valuestring);

					item = cJSON_GetObjectItem(otaResponse, "filename");
					if (item && item->valuestring)
						snprintf(otaInfo->filename, sizeof(otaInfo->filename), "%s", item->valuestring);
				}
				else
				{
					return OTA_UNAVAILABLE;
				}
			}
			cJSON_Delete(root);
			free(payloadAddr);
		} break;
		case OTA_GET_DATA:
		{
			otaInfo_t *otaInfo = (otaInfo_t *)data;

			if (!otaInfo)
				return GEN_NULL_POINTING_ERROR;

			root = cJSON_Parse(json);
			if (!root)
				return DATA_INVALID;

			char *payloadAddr = cJSON_PrintUnformatted(root);
			item = cJSON_GetObjectItem(root, "version");
			if (item && item->valuestring)
				snprintf(otaInfo->updateVersion, sizeof(otaInfo->updateVersion), "%s", item->valuestring);

			item = cJSON_GetObjectItem(root, "md5sum");
			if (item && item->valuestring)
				snprintf(otaInfo->md5sum, sizeof(otaInfo->md5sum), "%s", item->valuestring);

			item = cJSON_GetObjectItem(root, "url");
			if (item && item->valuestring)
				snprintf(otaInfo->otaUrl, sizeof(otaInfo->otaUrl), "%s", item->valuestring);

			item = cJSON_GetObjectItem(root, "filename");
			if (item && item->valuestring)
				snprintf(otaInfo->filename, sizeof(otaInfo->filename), "%s", item->valuestring);

			cJSON_Delete(root);
			free(payloadAddr);
		} break;
		case REGISTER_POST_DATA:
		{
			char *apiToken = (char *)data;

			if (!apiToken)
				return GEN_NULL_POINTING_ERROR;

			root = cJSON_Parse(json);
			if (!root)
				return DATA_INVALID;

			item = cJSON_GetObjectItem(root, "authToken");
			if (item && item->valuestring)
				snprintf(apiToken, MAX_LEN_APIKEY, "%s", item->valuestring);
			cJSON_Delete(root);
		} break;
		default:
			return DATA_INVALID;

	}

	FUNC_EXIT

	return GEN_SUCCESS;
}

/** @brief Convert system to json payload
 *
 * @param[in] data type
 * @param[in] system data
 *
 * This API used for conver system structure to json payload
 *
 * @return json payload or NULL
 */
static char* systemToJsonPayload(restApiType_e restApiType, void *data)
{
	FUNC_ENTRY

	char *payloadAddr = NULL;
	cJSON *root;

	switch (restApiType) {
		case JSON_POST_DATA:
		{
			tpmsEvent_t *tpmsEvent = (tpmsEvent_t *)data;
			char swVersionStr[20] = "";
			char sensorID[10] = "";
			cJSON *fmt;

			if (!tpmsEvent)
				return NULL;

			root = cJSON_CreateObject();
			if (!root)
				return NULL;

			cJSON_AddItemToObject(root, "tpms_event", fmt = cJSON_CreateObject());
			if(!fmt)
				return NULL;
			cJSON_AddNumberToObject(fmt, "event_id", tpmsEvent->eventID);
			cJSON_AddNumberToObject(fmt, "alert_type", tpmsEvent->alert);

			snprintf(sensorID, sizeof(sensorID), "%X", tpmsEvent->tpmsDMData.tpmsData.sensorID);
			cJSON_AddStringToObject(fmt, "sensor_id", sensorID);

			cJSON_AddNumberToObject(fmt, "timestamp", tpmsEvent->tpmsDMData.time);
			cJSON_AddNumberToObject(fmt, "side", tpmsEvent->tpmsDMData.wheelInfo.vehicleSide);
			cJSON_AddNumberToObject(fmt, "axle", tpmsEvent->tpmsDMData.wheelInfo.axleNum);
			cJSON_AddNumberToObject(fmt, "type", tpmsEvent->tpmsDMData.wheelInfo.attachedType);
			cJSON_AddNumberToObject(fmt, "position", tpmsEvent->tpmsDMData.wheelInfo.tyrePos);
			cJSON_AddNumberToObject(fmt, "halo_enabled",tpmsEvent->tpmsDMData.wheelInfo.isHaloEnable);
			cJSON_AddNumberToObject(fmt, "pressure", tpmsEvent->tpmsDMData.tpmsData.pressure);
			cJSON_AddNumberToObject(fmt, "temperature", tpmsEvent->tpmsDMData.tpmsData.temperature);
			cJSON_AddNumberToObject(fmt, "rssi", tpmsEvent->tpmsDMData.tpmsData.rssi);
			cJSON_AddNumberToObject(fmt, "battery", tpmsEvent->tpmsDMData.tpmsData.battery);
			cJSON_AddNumberToObject(fmt, "latitude", tpmsEvent->tpmsDMData.gpsData.latitude);
			cJSON_AddNumberToObject(fmt, "longitude", tpmsEvent->tpmsDMData.gpsData.longitude);
			cJSON_AddNumberToObject(fmt, "altitude", tpmsEvent->tpmsDMData.gpsData.altitude);
			cJSON_AddNumberToObject(fmt, "speed", tpmsEvent->tpmsDMData.gpsData.speed);
			cJSON_AddNumberToObject(fmt, "status", tpmsEvent->tpmsDMData.tpmsData.status);

			sprintf(swVersionStr, SW_VERSION_STRING_FORMAT, ver1, ver2, ver3, ver4);
			cJSON_AddStringToObject(fmt, "sw_version", swVersionStr);
			payloadAddr = cJSON_PrintUnformatted(root);
			cJSON_Delete(root);
		} break;
		case REGISTER_POST_DATA:
		{
			root = cJSON_CreateObject();
			if (!root)
				return NULL;
			cJSON_AddStringToObject(root, "gateway_id", g_cloudInfo.gatewayID);
			cJSON_AddStringToObject(root, "key", (char *)data);

			payloadAddr = cJSON_PrintUnformatted(root);
			cJSON_Delete(root);
		} break;
		case OTA_GET_DATA:
		{
			otaInfo_t *otaInfo = (otaInfo_t *)data;
			if (!otaInfo)
				return NULL;

			root = cJSON_CreateObject();
			if (!root)
				return NULL;

			cJSON_AddStringToObject(root, "gateway_id", g_cloudInfo.gatewayID);
			cJSON_AddStringToObject(root, "version", otaInfo->version);

			payloadAddr = cJSON_PrintUnformatted(root);
			cJSON_Delete(root);
		} break;
		case NOTIFY_GW_VERSION:
		{
			otaInfo_t *otaInfo = (otaInfo_t *)data;
			if (!otaInfo)
				return NULL;

			root = cJSON_CreateObject();
			if (!root)
				return NULL;

			cJSON_AddStringToObject(root, "gateway_id", g_cloudInfo.gatewayID);
			cJSON_AddStringToObject(root, "version", otaInfo->version);

			payloadAddr = cJSON_PrintUnformatted(root);
			cJSON_Delete(root);
		} break;
		case ZIP_POST_DATA:
		case OTA_GET_DOWNLOAD_DATA:
		{
		} break;
	}

	FUNC_EXIT

	return payloadAddr;
}

static generateSecretKey(char *data)
{
    char string1[] = {0xF4,0xED,0xE3,0xE4,0xE5,0xE8,0xED,0xE4,0xE3,0x87,0xBF, 0x7F};
    char string2[] = {0xCF,0xB2,0xED,0xCC,0xF1,0xF6,0xE8,0xAF, 0x7F};
    char string3[] = {0xF1,0xE8,0xF8,0xE0,0xA0,0xB6,0xE3,0xEE,0xEE, 0x7F};
    uint8_t index = 0;
    uint8_t minLength;
    uint8_t temp = 0;

    for(index = 0; index < strlen(string1); index++) {
        string1[index] = string1[index] - 127;
    }
    for(index = 0; index < strlen(string2); index++) {
        string2[index] = string2[index] - 127;
    }
    for(index = 0; index < strlen(string3); index++) {
        string3[index] = string3[index] - 127;
    }

    minLength = strlen(string2);
    for(index = 0; index < minLength; index++) {
       data[index] = ( string1[index] > string2[index] ) ? string1[index] > string3[index] ? string1[index] : string3[index] : string2[index] > string3[index] ? string2[index] : string3[index];
    }

    minLength = strlen(data);

    for(index = minLength; index < 20; index++) {
        if(index % 2 == 0) {
            data[index] = '!' + index ;
        }
        else {
            data[index] = '@' + index ;
        }
    }
    minLength = strlen(data);
    temp = 0;
    for(index = minLength; index < minLength + 6; index++) {
            temp++;
            data[index] = data[temp] + data[temp + 1];
            if(data[index] == '0') {
                data[index] = index + 'A';
            }
    }

    minLength = strlen(data);
    temp = 5;
    for(index = minLength; index < minLength + 6; index++) {
            temp++;
            data[index] = data[temp] + data[temp + 1];
            if(data[index] == '0') {
                data[index] = index + 'V';
            }
    }
    minLength = strlen(data);
    for(index = 0; index < minLength; index++) {
        if( (data[index] < 0x21) || (data[index] > 0x7E)) {
                data[index] = ((index * 15 ) % 17) + '1';
        }
    }
}

static returnCode_e generateRegisterKeySHA256(char *sha256Data)
{
	unsigned char* hmacData;
	unsigned int sha256Len = 0;
	int index;
	returnCode_e retCode;

	/* create json for generating sha key */
	cJSON *root;
	root = cJSON_CreateObject();
	if (!root)
		return NULL;

	/* gateway_id is used as payload for generating sha key */
	cJSON_AddStringToObject(root, "gateway_id", g_cloudInfo.gatewayID);

	char *jsonString = cJSON_PrintUnformatted(root);

	cJSON_Delete(root);

	char tempBuf[33] = "";
	generateSecretKey(tempBuf);
	hmacData = HMAC(EVP_sha256(), tempBuf, strlen(tempBuf), jsonString, strlen(jsonString), NULL, &sha256Len);

	if(hmacData == NULL) {
		retCode = CLOUD_KEY_GENERATE_FAIL; 
		goto error;
	}
	else {
		for (index = 0; index < sha256Len; index++) {
			sprintf(&(sha256Data[index * 2]),"%02x", hmacData[index]);
		}
		CAB_DBG(CAB_GW_INFO, "SHA Key : %s", sha256Data);
		retCode = GEN_SUCCESS;
	}
error:
	free(jsonString);
	return retCode;
}

static returnCode_e gatewayIDToString(char *inArray, char * outStr)
{
	int loop;
	char *tempPtr = outStr;

	FUNC_ENTRY
	if (inArray == NULL || outStr == NULL) {
		return GEN_INVALID_ARG;
	}
	for(loop = 0; loop < MAX_LEN_BLE_MAC; loop++) {
		sprintf(tempPtr, "%02X", inArray[loop]);
		tempPtr += 2;
	}
	for(loop = MAX_LEN_BLE_MAC; loop < MAX_LEN_GATEWAY_ID - 1; loop++) {
		sprintf(tempPtr, "%d", inArray[loop]);
		tempPtr++;
	}
	FUNC_EXIT
	return GEN_SUCCESS;
}

/** @brief Get api token for rest api
 *
 * This function request API token for rest API
 *
 * @return status code.
 */
returnCode_e getAPiToken()
{
	FUNC_ENTRY

	if(getOTAInitState() != true)
	        return CLOUD_NOT_INIT;

	uint8_t sha256Data[EVP_MAX_MD_SIZE] = { 0 };
	char response[MAX_LEN_CLOUD_RESP];
	returnCode_e retCode;
	cloudInfo_t cloudInfo;
	char apiToken[MAX_LEN_APIKEY + 1] = "";
	uint8_t count = SHA256_KEYGEN_MAX_TRY;
	bool connectionStatus = false;
	callBackNotification_t connCallBackData;

	/* generate sha key for whole json string */
	do
	{
		retCode = generateRegisterKeySHA256(sha256Data);
		if (retCode == GEN_SUCCESS)
			break;
		count--;
	} while(count > 0);

	if((retCode != GEN_SUCCESS) && (count == 0))
		return retCode;

	pthread_mutex_lock(&g_cloudInfo.cloudInfoLock);
	cloudInfo = g_cloudInfo;
	pthread_mutex_unlock(&g_cloudInfo.cloudInfoLock);

	retCode = uploadLoginJsonToCloud(&cloudInfo, response, sha256Data);

	if (GEN_SUCCESS == retCode) {
		retCode = jsonToSystemPayload(REGISTER_POST_DATA, response, apiToken);

		pthread_mutex_lock(&g_cloudInfo.cloudInfoLock);
		strcpy(g_cloudInfo.apiToken, apiToken);
		CAB_DBG(CAB_GW_INFO, "AuthToken : %s", g_cloudInfo.apiToken);
		pthread_mutex_unlock(&g_cloudInfo.cloudInfoLock);
		connectionStatus = true;
	}

	connCallBackData.CBNotifType = CLOUD_CONNECTION_STATUS;
	connCallBackData.cloudConnectionStatus = connectionStatus;
	(*g_notifFunPtr)((void *)&connCallBackData);

	FUNC_EXIT
	return retCode;
}

returnCode_e connectToCloud(restApiType_e restApiType, void *data)
{
	FUNC_ENTRY
	
	if(getOTAInitState() != true)
		return CLOUD_NOT_INIT; 

	char *payload;
	returnCode_e retCode = CLOUD_CURL_FAIL;
	cloudInfo_t cloudInfo;
	bool connectionStatus = false;
	callBackNotification_t connCallBackData;

	if(strcmp(g_cloudInfo.apiToken, "") == 0) {
		CAB_DBG(CAB_GW_INFO, "Gateway first time login");
		retCode = getAPiToken();
		if(retCode != GEN_SUCCESS)
			return retCode;
		CAB_DBG(CAB_GW_INFO, "Token received successfully");
	}

	switch (restApiType) {
		case ZIP_POST_DATA:
		{
			char responseStr[MAX_LEN_CLOUD_RESP * 2];
			char swVersionStr[MAX_LEN_SOFTWARE_VERSION] = "";
			bool fileTypeFlag = false;
			CAB_DBG(CAB_GW_INFO, "Request for sending zip to cloud");

			if (!data)
			{
			    CAB_DBG(CAB_GW_ERR, "Invalid argument");
			    return GEN_NULL_POINTING_ERROR;
			}

			fileStorageInfo_t *fileStorageInfo = (fileStorageInfo_t *)data;

			pthread_mutex_lock(&g_cloudInfo.cloudInfoLock);
			cloudInfo = g_cloudInfo;
			pthread_mutex_unlock(&g_cloudInfo.cloudInfoLock);

			sprintf(swVersionStr, "%d.%d.%d.%d", ver1, ver2, ver3, ver4);

			#if defined(CABAPPA7)
				/* FileTypeFlag 'true' for config data and sensordata which is sent from cabapp only
					For rDiag file it will be default 'false' */
				fileTypeFlag = true;
			# endif

			retCode = uploadFileToCloud(&cloudInfo, fileStorageInfo->storagePath, fileStorageInfo->filename, swVersionStr, fileTypeFlag, responseStr);
			if (retCode == GEN_SUCCESS)
			{
				/* Only cabapp should perform the OTA, so restricted the callback.  */
#if defined(CABAPPA7)
				callBackNotification_t callBackData={0};
				returnCode_e tempRetCode = CLOUD_CURL_FAIL;
				CAB_DBG(CAB_GW_DEBUG, "/gateway/sensordata response : %s", responseStr);
				tempRetCode = jsonToSystemPayload(restApiType, responseStr, &callBackData);

				/* If the config change request available, hadle it before OTA */
				if (callBackData.configChangeInfo.configChangeAvail) {
					CAB_DBG(CAB_GW_INFO, "Calling the callback\n");
					callBackData.CBNotifType = CONFIG_CHANGE_REQUEST;
					(*g_notifFunPtr)((void *)&callBackData);
				}

				if (tempRetCode == GEN_SUCCESS)
				{
					/* Pass the OTA response data */
					callBackData.CBNotifType = CLOUD_OTA_DATA;
					(*g_notifFunPtr)((void *)&callBackData);
				}
#endif
				CAB_DBG(CAB_GW_INFO, "Uploaded zip file name %s/%s", fileStorageInfo->storagePath, fileStorageInfo->filename);
				connectionStatus = true;
			}
		} break;
		case JSON_POST_DATA:
		{
			CAB_DBG(CAB_GW_INFO, "Request for sending alert to cloud");
			payload = systemToJsonPayload(restApiType, data);
			if (!payload)
				return GEN_MALLOC_ERROR;

			pthread_mutex_lock(&g_cloudInfo.cloudInfoLock);
			cloudInfo = g_cloudInfo;
			pthread_mutex_unlock(&g_cloudInfo.cloudInfoLock);

			retCode = uploadJsonToCloud(&cloudInfo, payload);
			if (retCode == GEN_SUCCESS)
			{
			    CAB_DBG(CAB_GW_INFO, "Uploaded alert event was : %s", payload);
			    connectionStatus = true;
			}
			free(payload);
		} break;
		case OTA_GET_DATA:
		{
			CAB_DBG(CAB_GW_INFO, "Request for checking ota to cloud");
			char otaInfo[MAX_LEN_CLOUD_RESP + 1];
			payload = systemToJsonPayload(restApiType, data);
			if (!payload)
				return GEN_MALLOC_ERROR;

			pthread_mutex_lock(&g_cloudInfo.cloudInfoLock);
			cloudInfo = g_cloudInfo;
			pthread_mutex_unlock(&g_cloudInfo.cloudInfoLock);

			retCode = getJsonfromCloud(&cloudInfo, payload, otaInfo);
			free(payload);
			if(retCode == GEN_SUCCESS) {
				retCode = jsonToSystemPayload(restApiType, otaInfo, data);
			}
			else {
				CAB_DBG(CAB_GW_DEBUG, "Unable to get OTA : %d", retCode);
			}

			/* OTA checking cloud operation can be considered successful if 
			   We have OTA or OTA is not available. */
			if (retCode == GEN_SUCCESS || retCode == OTA_UNAVAILABLE)
			{
				connectionStatus = true;
			}
		} break;
		case NOTIFY_GW_VERSION:
		{
			CAB_DBG(CAB_GW_INFO, "Request for notifying gw version to cloud");
			char otaInfo[MAX_LEN_CLOUD_RESP + 1];
			payload = systemToJsonPayload(restApiType, data);
			if (!payload)
				return GEN_MALLOC_ERROR;

			pthread_mutex_lock(&g_cloudInfo.cloudInfoLock);
			cloudInfo = g_cloudInfo;
			pthread_mutex_unlock(&g_cloudInfo.cloudInfoLock);

			retCode = notifyGatewayVersionToCloud(&cloudInfo, payload, otaInfo);
			free(payload);
			if((retCode != GEN_SUCCESS) && (retCode != OTA_UNAVAILABLE)) {
				CAB_DBG(CAB_GW_ERR, "Unable to notify gw version to cloud : %d", retCode);
				retCode = NOTIFY_TO_CLOUD_FAIL;
			}
			else {
				retCode = GEN_SUCCESS;
				connectionStatus = true;
			}
		} break;
		case OTA_GET_DOWNLOAD_DATA:
		{
			CAB_DBG(CAB_GW_INFO, "Request for downloading ota from cloud");

			otaInfo_t *otaInfo = (otaInfo_t *)data;
			CAB_DBG(CAB_GW_DEBUG, "Download location %s/%s", otaInfo->storagePath, otaInfo->filename);

			pthread_mutex_lock(&g_cloudInfo.cloudInfoLock);
			cloudInfo = g_cloudInfo;
			pthread_mutex_unlock(&g_cloudInfo.cloudInfoLock);

			retCode = downlaodFileFromCloud(&cloudInfo, otaInfo->otaUrl, otaInfo->storagePath, otaInfo->filename);
			if (retCode == GEN_SUCCESS)
			{
				connectionStatus = true;
			}
		} break;
		default:
		{
		} break;
	}

	// Updated the cloud connection / API transaction status
	connCallBackData.CBNotifType = CLOUD_CONNECTION_STATUS;
	connCallBackData.cloudConnectionStatus = connectionStatus;
	(*g_notifFunPtr)((void *)&connCallBackData);

	if (retCode != CLOUD_TOKEN_EXPIRE)
		return retCode;

	CAB_DBG(CAB_GW_DEBUG, "Token Expired");
	getAPiToken();

	FUNC_EXIT
	return retCode;
}

returnCode_e updateCloudUrl(const char *url)
{
	FUNC_ENTRY

	if(getOTAInitState() != true)
		return CLOUD_NOT_INIT; 

	if (!url)
		return GEN_NULL_POINTING_ERROR;

	if (strlen(url) > MAX_LEN_CLOUD_URI)
		return DATA_INVALID;

	pthread_mutex_lock(&g_cloudInfo.cloudInfoLock);
	strncpy(g_cloudInfo.url, url, strlen(url)+1);
	pthread_mutex_unlock(&g_cloudInfo.cloudInfoLock);

	FUNC_EXIT

	return GEN_SUCCESS;
}

returnCode_e updateGWID(const char *id)
{
    FUNC_ENTRY
    returnCode_e retCode = GEN_SUCCESS;

    if(getOTAInitState() != true)
        return CLOUD_NOT_INIT;

    if (!id)
        return GEN_NULL_POINTING_ERROR;

    if (strlen(id) > MAX_LEN_GATEWAY_ID)
        return DATA_INVALID;

    pthread_mutex_lock(&g_cloudInfo.cloudInfoLock);
    retCode = gatewayIDToString(id, g_cloudInfo.gatewayID);
    pthread_mutex_unlock(&g_cloudInfo.cloudInfoLock);

    FUNC_EXIT

    return retCode;
}

returnCode_e cloudInit(cloudInitInfo_t *cloudInitInfo, char *swVersion, cloudConStatusNotifCB funPtr)
{
	FUNC_ENTRY
	returnCode_e retCode = GEN_SUCCESS;

	if(getOTAInitState() == true)
		return GEN_SUCCESS;

	CAB_DBG(CAB_GW_INFO, "Initializing cloud module.");

	if (cloudInitInfo == NULL || swVersion == NULL || funPtr == NULL)
		return GEN_NULL_POINTING_ERROR;

	sscanf(swVersion, "%d.%d.%d.%d", &ver1, &ver2, &ver3, &ver4);


	pthread_mutex_lock(&g_cloudInfo.cloudInfoLock);
	snprintf(g_cloudInfo.url, sizeof(g_cloudInfo.url), "%s", cloudInitInfo->url);
	retCode = gatewayIDToString(cloudInitInfo->gatewayID, g_cloudInfo.gatewayID);
	g_notifFunPtr = funPtr;
	pthread_mutex_unlock(&g_cloudInfo.cloudInfoLock);

	if(retCode == GEN_SUCCESS)
		retCode = curlInit();

	if(retCode == GEN_SUCCESS)
		setOTAInitState(true);

	CAB_DBG(CAB_GW_INFO, "Cloud Init Success with GwID : %s", g_cloudInfo.gatewayID);
	FUNC_EXIT
	return retCode;
}

returnCode_e cloudDeinit(void)
{
	FUNC_ENTRY

	CAB_DBG(CAB_GW_INFO, "Deinitializing cloud module.");

	if(getOTAInitState() == true) {
		curlDeinit();
		setOTAInitState(false);
	}

	g_notifFunPtr = NULL;

	FUNC_EXIT
	return GEN_SUCCESS;
}

returnCode_e validateGWID(char *gatewayID)
{
	FUNC_ENTRY
	char invalidID[] = {'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'};
	if(memcmp(gatewayID, invalidID, sizeof(gatewayID)) != 0)
	{
		return GEN_SUCCESS;
	}
	else
	{
		return GEN_API_FAIL;
	}

	FUNC_EXIT

}
