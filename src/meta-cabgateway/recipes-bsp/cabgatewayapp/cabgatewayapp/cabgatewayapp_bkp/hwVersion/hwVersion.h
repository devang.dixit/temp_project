#include <stdio.h>
#include "debug.h"
#include "gpio.h"
#include "tpmsConfig.h"

#define HW_VER_GPIO_0 (117)
#define HW_VER_GPIO_1 (118)
#define HW_VER_GPIO_2 (108)
#define HW_VER_GPIO_3 (1)


typedef struct LookUpT {
	char hardwareVersion[MAX_LEN_HARDWARE_VERSION];
	bool isThermalShutOffAvail;
	bool isCC_OTA_Avail;
}HWVersionLookUp_t;

returnCode_e getHardwareVersion(char *);
returnCode_e isThermalShutOffAvailable(bool *);
returnCode_e isCC1310OTAAvailable(bool *);
