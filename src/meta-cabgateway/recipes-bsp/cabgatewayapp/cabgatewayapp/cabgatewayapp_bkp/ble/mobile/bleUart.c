/***************************************************************
+------------+
|BLE app init|
+-----+------+
      |
+-----v------+
|Initialize  |
|UART port   |
+-----+------+
      |
+-----v------+   +--------------------+
|Start data  |   |Receive data packets|
|receiver    +--->from UART & send it |    +----------------+
|thread      |   |to command handler  |    |Send data to BLE|   +--------+
+-----+------+   +---------+----------+    |UART    as per  |   |Put data|
      |                    |               |File or single  +--->on UART |
+-----v------+   +---------v----------+    |xfer command    |   +--------+
|Start cmd   |   |Process command     |    +-------^--------+
|handler     +--->, perform operation +------------+
|thread      |   |, send response     |
+-----+------+   +---------^----------+
      |                    |
+-----v------+             |
|Request     |             |
|IMEI, MAC & |             |
|Adv. start  +-------------+
|notification|
+------------+
***************************************************************/

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>
#include <mqueue.h>
#include <openssl/aes.h>
#include <errno.h>

#include "bleUart.h"
#include "cJSON.h"
#include "diagnostic.h"
#include "debug.h"

#define MAX_MSG_SIZE (sizeof(msgQueue_t))   /**< size of message */
#define MAX_QUEUE_SIZE (40)  /**< number of messsag queued of size @MAX_MSG_SIZE */
#define MQ_SEND_BLE_CMD_TOUT	(60) /* timeout to prevent deadlocks of application */
#define MQ_SEND_BLE_KEEPALIVE_TOUT	(20) /* timeout to prevent deadlocks of application */

/**< uart start of frame */
const static uint8_t sof[] = {0xAA, 0x55};

/**< AES key for Encryption and Decryption */
const static unsigned char aesKey[] = {0xEA,  0xF9, 0xE8, 0xDD, 0x16, 0xB8, 0xBD, 0x85, 0x84, 0xB9, 0x35, 0x33, 0x58, 0xDC, 0x80, 0x00};
//const static unsigned char aesIv[] = {0xE8,  0xC7, 0x1C, 0x99, 0x16, 0x17, 0x1F, 0x39, 0xB1, 0x4C, 0x60, 0xB8, 0x9C, 0x63, 0xE3, 0xD2};

/**< convert numerical value to string */
const char * intTostrTireSide[] = {"left", "right"};
const char * intTostrAttachedType[] = {"tire", "halo"};
const char * intTostrTyrePos[] = {"inner", "outer"};

//TODO: add into ble resource manager
/**< uart thread id */
pthread_t readerId;
pthread_t consumerId;
static pthread_mutex_t keepAliveFlagLock;
static pthread_mutex_t uartDataHandlerAliveLock;
static pthread_mutex_t uartCmdHandlerAliveLock;
static bool g_isBLEAlive = true;

static bool g_bleUartDataHandlerAliveStatus = true;
static bool g_bleUartCmdHandlerAliveStatus = true;

static uint8_t bleInited = 0;
static uint16_t maxMTUPerApp = UART_MTU_SIZE;
static uint16_t connectedAppType = ANDROID_APP;
static bool application_with_pn_mn = false; /* An identifier to application version for backward compatibility */

#define APP_WISE_MAX_PAYLOAD(x) ((x == UART_MTU_SIZE)?(MAX_PAYLOAD_LEN_ANDROID):MAX_PAYLOAD_LEN_IOS)
#define CONNECTED_APP(x) ((x == UART_MTU_SIZE)?(ANDROID_APP):IOS_APP)

static const lookupTable_t CmdLookupTable []= {
    // All the command below with last value not NULL, are used for uart data, command handler and send data to BLE
    // All the command below with last value as NULL, are used for only send data to BLE
//  [COMMAND_NAME,                 Req Frame Type Android, Res Frame Type Android, Req Frame Type IOS, Res Frame Type IOS]
    {CFG_GW_SYSTEM_CMD,               SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, cmdHandler_CFG_GW_SYSTEM_CMD},
    {CFG_GW_OPT_CMD,                  FILE_XFER_COMMAND,   SINGLE_XFER_COMMAND, FILE_XFER_COMMAND,   SINGLE_XFER_COMMAND, cmdHandler_CFG_GW_OPT_CMD},
    {CFG_GW_TPMS_VENDOR_CMD,          SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, cmdHandler_CFG_GW_TPMS_VENDOR_CMD},
    {CFG_GW_TPMS_PAIR_CMD,            SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, cmdHandler_CFG_GW_TPMS_PAIR_CMD},
    {CFG_GW_TPMS_UNPAIR_CMD,          SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, cmdHandler_CFG_GW_TPMS_UNPAIR_CMD},
    {CFG_GW_CLOUD_CMD,                FILE_XFER_COMMAND,   SINGLE_XFER_COMMAND, FILE_XFER_COMMAND,   SINGLE_XFER_COMMAND, cmdHandler_CFG_GW_CLOUD_CMD},
    {CFG_MOBILE_UTC_TIME_CMD,         SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, cmdHandler_CFG_MOBILE_UTC_TIME_CMD},
    {CFG_GW_TARGET_PRESSURE_CMD,      SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, cmdHandler_CFG_GW_TARGET_PRESSURE_CMD},
    {CFG_GW_BATTERY_CMD,              SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, cmdHandler_CFG_GW_BATTERY_CMD},
    {CFG_GW_ATTACH_HALO_SN_CMD,       SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, cmdHandler_CFG_GW_ATTACH_HALO_SN_CMD},
    {CFG_GW_DETACH_HALO_SN_CMD,       SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, cmdHandler_CFG_GW_DETACH_HALO_SN_CMD},
    {CFG_GW_UPDATE_TYRE_DETAILS_CMD,  FILE_XFER_COMMAND,   SINGLE_XFER_COMMAND, FILE_XFER_COMMAND,   SINGLE_XFER_COMMAND, cmdHandler_CFG_GW_UPDATE_TYRE_DETAILS_CMD},
    {CFG_GW_FLUSH_TYRE_DETAILS_CMD,   SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, cmdHandler_CFG_GW_FLUSH_TYRE_DETAILS_CMD},
    {CFG_GW_RECORD_TREAD_DETAILS_CMD, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, cmdHandler_CFG_GW_RECORD_TREAD_DETAILS_CMD},
    {CFG_BLE_PASSKEY_CMD,             SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, cmdHandler_CFG_BLE_PASSKEY_CMD},
    {GET_CFG_GW_SYSTEM_CMD,           SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, cmdHandler_GET_CFG_GW_SYSTEM_CMD},
    {GET_CFG_GW_OPT_CMD,              SINGLE_XFER_COMMAND, FILE_XFER_COMMAND,   SINGLE_XFER_COMMAND, FILE_XFER_COMMAND  , cmdHandler_GET_CFG_GW_OPT_CMD},
    {GET_CFG_GW_TPMS_VENDOR_CMD,      SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, cmdHandler_GET_CFG_GW_TPMS_VENDOR_CMD},
    {GET_CFG_GW_TPMS_PAIR_CMD,        SINGLE_XFER_COMMAND, FILE_XFER_COMMAND,   SINGLE_XFER_COMMAND, FILE_XFER_COMMAND  , cmdHandler_GET_CFG_GW_TPMS_PAIR_CMD},
    {GET_CFG_GW_CLOUD_CMD,            SINGLE_XFER_COMMAND, FILE_XFER_COMMAND,   SINGLE_XFER_COMMAND, FILE_XFER_COMMAND  , cmdHandler_GET_CFG_GW_CLOUD_CMD},
    {GET_CFG_GW_TARGET_PRESSURE_CMD,  SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, cmdHandler_GET_CFG_GW_TARGET_PRESSURE_CMD},
    {GET_CFG_GW_BATTERY_CMD,          SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, cmdHandler_GET_CFG_GW_BATTERY_CMD},
    {GET_CFG_GW_ALL_HALO_SN_CMD,      SINGLE_XFER_COMMAND, FILE_XFER_COMMAND,   SINGLE_XFER_COMMAND, FILE_XFER_COMMAND  , cmdHandler_GET_CFG_GW_ALL_HALO_SN_CMD},
    {GET_CFG_GW_TYRE_DETAILS_CMD,     SINGLE_XFER_COMMAND, FILE_XFER_COMMAND,   SINGLE_XFER_COMMAND, FILE_XFER_COMMAND  , cmdHandler_GET_CFG_GW_TYRE_DETAILS_CMD},
    {GET_CFG_GW_TREAD_HISTORY_CMD,    SINGLE_XFER_COMMAND, FILE_XFER_COMMAND,   SINGLE_XFER_COMMAND, FILE_XFER_COMMAND  , cmdHandler_GET_CFG_GW_TREAD_HISTORY_CMD},
    {DIAG_GW_INFO_CMD,                SINGLE_XFER_COMMAND, FILE_XFER_COMMAND,   SINGLE_XFER_COMMAND, FILE_XFER_COMMAND  , cmdHandler_DIAG_GW_INFO_CMD},
    {DIAG_GW_PAIRED_SENSOR_CMD,       SINGLE_XFER_COMMAND, FILE_XFER_COMMAND,   SINGLE_XFER_COMMAND, FILE_XFER_COMMAND  , cmdHandler_DIAG_GW_PAIRED_SENSOR_CMD},
    {DIAG_GW_TPMS_BY_ID_CMD,          SINGLE_XFER_COMMAND, FILE_XFER_COMMAND,   SINGLE_XFER_COMMAND, FILE_XFER_COMMAND  , cmdHandler_DIAG_GW_TPMS_BY_ID_CMD},
    {DIAG_GW_GPS_INFO_CMD,            SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, FILE_XFER_COMMAND  , cmdHandler_DIAG_GW_GPS_INFO_CMD},
    {DIAG_GW_CELLULAR_INFO_CMD,       SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, cmdHandler_DIAG_GW_CELLULAR_INFO_CMD},
    {DIAG_GW_TPMS_DATA_CMD,           SINGLE_XFER_COMMAND, FILE_XFER_COMMAND,   SINGLE_XFER_COMMAND, FILE_XFER_COMMAND  , cmdHandler_DIAG_GW_TPMS_DATA_CMD},
    {DIAG_GW_BATTERY_EV_LOG_CMD,      SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, cmdHandler_DIAG_GW_BATTERY_EV_LOG_CMD},
    {INSTALLATION_STATUS_CMD,         SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, cmdHandler_INSTALLATION_STATUS_CMD},
    {SYS_GW_RESET_CMD,                SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, cmdHandler_SYS_GW_RESET_CMD},
    {SELF_DIAG_GW_CMD,                SINGLE_XFER_COMMAND, FILE_XFER_COMMAND,   SINGLE_XFER_COMMAND, FILE_XFER_COMMAND  , cmdHandler_SELF_DIAG_GW_CMD},
    {SYS_MOBILE_RESP_CMD,             SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, NULL},
    // All the system commands below, for them the Frame type doesn't matter, they are here just for searching pool for uart data handler
    {CFG_FILE_INIT_CMD,               SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, NULL},
    {CFG_FILE_DATA_CMD,               SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, SINGLE_XFER_COMMAND, NULL},
};

/** @brief dump command
 *
 *      API used for debug command data
 *
 * @param[in] command frame
 * @param[in] function name
 *
 * @return none
 */
static void dumpCommand(const uartFrame_t *const uartFrame, const char *func)
{
    CAB_DBG(CAB_GW_TRACE, "========= Command Info : %s =======\n", func);
    CAB_DBG(CAB_GW_TRACE, "sof %04x\n", uartFrame->sof);
    CAB_DBG(CAB_GW_TRACE, "cmd %04x\n", uartFrame->cmd);
    CAB_DBG(CAB_GW_TRACE, "Len %04x, %d\n", uartFrame->payloadLen, uartFrame->payloadLen);
    if (uartFrame->cmd == CFG_FILE_INIT_CMD) {
        CAB_DBG(CAB_GW_TRACE, "sub command %04x\n", uartFrame->subCommand);
        CAB_DBG(CAB_GW_TRACE, "len of file %04x, %u\n", uartFrame->fileLen, uartFrame->fileLen);
        CAB_DBG(CAB_GW_TRACE, "crc %02x\n", uartFrame->crc8);
    } else {
        if (uartFrame->payloadLen % 16)
        CAB_DBG(CAB_GW_TRACE, "data %s\n", uartFrame->data);
    }
    CAB_DBG(CAB_GW_TRACE, "crc %02x\n", uartFrame->data[uartFrame->payloadLen]);
    CAB_DBG(CAB_GW_TRACE, "===============================\n");

}

/** @brief Generate 8-bit crc
 *
 *      API used for generate 8-bit crc
 *
 * @param[in] data
 * @param[in] data length
 *
 * @return 8-bit crc
 */
static uint8_t generateCRC8(const uint8_t *data, const uint8_t len)
{
    FUNC_ENTRY

    uint16_t i;
    uint8_t crc8 = 0;
    for (i = 0; i < len; i++)
        crc8 ^= data[i];
    CAB_DBG(CAB_GW_TRACE, "data crc %02x\n", crc8);

    FUNC_EXIT

    return crc8;
}

/** @brief validate 8-bit crc
 *
 *      API used for validate 8-bit crc
 *
 * @param[in] data
 * @param[in] data length
 * @param[in] data crc
 *
 * @return status code
 */
static returnCode_e validateCRC8(const uint8_t *data, const uint8_t len, uint8_t dataCrc8)
{
    FUNC_ENTRY

    uint8_t crc8;

    crc8 = generateCRC8(data, len);
    if (crc8 != dataCrc8) {
        CAB_DBG(CAB_GW_WARNING, "Generated crc %02x, payload crc %02x\n", crc8, dataCrc8);
        return DATA_CRC_FAIL;
    }
    CAB_DBG(CAB_GW_TRACE, "Data crc matched\n");

    FUNC_EXIT

    return GEN_SUCCESS;
}

/** @brief AES128 encryption of data
 *
 *      API used for encryption of data
 *
 * @param[in] plain text
 * @param[in] encrypted data len
 * @param[out] cipher text
 *
 * @return none
 */
static void aes128Encrypt(uint8_t *data, const uint16_t len, uint8_t *cipherText)
{
    FUNC_ENTRY

    unsigned char iv[AES_BLOCK_SIZE];
    AES_KEY encKey;

    memset(iv, 0x00, AES_BLOCK_SIZE);

    CAB_DBG(CAB_GW_TRACE, "Running cipher on %s\n", data);
    AES_set_encrypt_key(aesKey, sizeof(aesKey)*8, &encKey);
    AES_cbc_encrypt(data, cipherText, len, &encKey, iv, AES_ENCRYPT);

    FUNC_EXIT
}

/** @brief AES128 decryption of data
 *
 *      API used for decryption of data
 *
 * @param[in/out] encrypted data, return plain text
 * @param[in] encrypted data len
 *
 * @return none
 */
static void aes128Decrypt(uint8_t *data, const uint16_t len)
{
    FUNC_ENTRY

    unsigned char iv[AES_BLOCK_SIZE];
    uint8_t plainText[len + 16];
    AES_KEY decKey;

    if (!len)
        return;

    CAB_DBG(CAB_GW_TRACE, "Running Decipher\n");
    memset(iv, 0x00, AES_BLOCK_SIZE);
    AES_set_decrypt_key(aesKey, sizeof(aesKey)*8, &decKey);
    AES_cbc_encrypt(data, plainText, len, &decKey, iv, AES_DECRYPT);
    CAB_DBG(CAB_GW_TRACE, "Decryption Data %s\n", plainText);
    memcpy(data, plainText, len);
    data[len] = '\0';

    FUNC_EXIT
}


/** @brief Prepare file init packet
 *
 *      API used for prepare file init packet
 *
 * @param[in] sub command type
 * @param[out] command
 * @param[in] length of sub command
 *
 * @return none
 */
static void prepareFileInitPacket(commandCode_e subCommand, uint8_t *buf,  int16_t subCommandLen)
{
    FUNC_ENTRY

    uartFrame_t uartFrame;

    uartFrame.sof = UART_SOF;
    uartFrame.cmd = (uint16_t)CFG_FILE_INIT_CMD;
    uartFrame.payloadLen = (uint16_t)0x0004;
    uartFrame.subCommand = (uint16_t)subCommand;
    uartFrame.fileLen = (uint16_t)subCommandLen;
    uartFrame.data[uartFrame.payloadLen] = (uint8_t)generateCRC8((uint8_t *)uartFrame.data, uartFrame.payloadLen);
    memcpy(buf, &uartFrame, ZERO_BYTE_FILE_CMD_LEN);

    dumpCommand((uartFrame_t *)buf, __func__);

    FUNC_EXIT
}

/** @brief Prepare data packet
 *
 *      API used for prepare data packet
 *
 * @param[in] command type
 * @param[out] data packet
 * @param[in] payload data
 * @param[in] payload length
 *
 * @return none
 */
static void prepareDataPacket(commandCode_e command, uint8_t *buf, char *payload, int16_t payloadLen)
{
    FUNC_ENTRY

    uartFrame_t uartFrame;

    uartFrame.sof = UART_SOF;
    uartFrame.cmd = (uint16_t)command;
    uartFrame.payloadLen = (uint16_t)payloadLen;
    if (payloadLen) {
        memcpy(uartFrame.data, payload, payloadLen);
        uartFrame.data[payloadLen] = (uint8_t)generateCRC8((uint8_t *)payload, payloadLen);
    } else {
        uartFrame.data[payloadLen] = '\0';
    }
    memcpy(buf, &uartFrame, ZERO_BYTE_CMD_LEN + payloadLen);

    dumpCommand((uartFrame_t *)buf, __func__);

    FUNC_EXIT
}

/******************************************************************
 *@brief  (It is used to get details for all the attached HALO SNs)
 *
 *@param[IN] void * (pointer to the structure having details)
 *@param[OUT] char ** (JSON string consisting all the details)
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e getAllHaloSNData(void *value, char **payloadAddr)

{
    unsigned int i;
    cJSON *root = NULL;
    cJSON *array = NULL;
    cJSON *fmt = NULL;

    if (!value)
    {
        return DATA_PTR_INVALID;
    }

    attachedHaloSNInfo_t * allHaloSNInfo = (attachedHaloSNInfo_t *) value;

    root = cJSON_CreateObject();
    if (!root)
        return DATA_PTR_INVALID;

    array = cJSON_AddArrayToObject(root, "attached_halo_info");
    for (i = 0; i < allHaloSNInfo->numberOfAttachedHaloSN; i++) {
        fmt = cJSON_CreateObject();

        cJSON_AddStringToObject(fmt, "halo_serial_number", allHaloSNInfo->haloSNInfo[i].serialNo);
        cJSON_AddStringToObject(fmt, "side", intTostrTireSide[allHaloSNInfo->haloSNInfo[i].vehicleSide]);
        cJSON_AddNumberToObject(fmt, "axle", allHaloSNInfo->haloSNInfo[i].axleNum);
        cJSON_AddItemToArray(array, fmt);
    }
    *payloadAddr = cJSON_PrintUnformatted(root);
    cJSON_Delete(root);

    return GEN_SUCCESS;
}


/******************************************************************
 *@brief  (It is used to get details of the tire)
 *
 *@param[IN] void * (pointer to the structure having details)
 *@param[OUT] char ** (JSON string consisting all the details)
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e getTireDetails(void *value, char **payloadAddr)
{
    cJSON *root = NULL;
    cJSON *fmt = NULL;

    if (!value)
    {
        return DATA_PTR_INVALID;
    }

    tyreInfo_t *tyreInfo = (tyreInfo_t *) value;

    root = cJSON_CreateObject();
    if (!root)
        return DATA_PTR_INVALID;

    cJSON_AddItemToObject(root, "tire_details", fmt = cJSON_CreateObject());
    cJSON_AddStringToObject(fmt, "Serial_No", tyreInfo->tyreSerialNo);
    cJSON_AddStringToObject(fmt, "Tire_TIN", tyreInfo->tyreTIN);
    cJSON_AddStringToObject(fmt, "Tire_Make", tyreInfo->tyreMake);
    cJSON_AddStringToObject(fmt, "Tire_Model", tyreInfo->tyreModel);
    cJSON_AddStringToObject(fmt, "Tire_width", tyreInfo->tyreWidth);
    cJSON_AddStringToObject(fmt, "Ratio", tyreInfo->ratio);
    cJSON_AddStringToObject(fmt, "Diameter", tyreInfo->diameter);
    cJSON_AddStringToObject(fmt, "Load_Rating", tyreInfo->loadRating);

    *payloadAddr = cJSON_PrintUnformatted(root);
    cJSON_Delete(root);

    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (It is used to get details of the tire tread depth records)
 *
 *@param[IN] void * (pointer to the structure having details)
 *@param[OUT] char ** (JSON string consisting all the details)
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e getTreadRecordHistrory(void *value, char **payloadAddr)
{
    unsigned int i;
    cJSON *root = NULL;
    cJSON *array = NULL;
    cJSON *fmt = NULL;

    if (!value)
    {
        return DATA_PTR_INVALID;
    }

    configuredTreadRecordInfo_t *configuredTreadRecordInfo = (configuredTreadRecordInfo_t *) value;

    root = cJSON_CreateObject();
    if (!root)
        return DATA_PTR_INVALID;

    array = cJSON_AddArrayToObject(root, "tread_history");
    for (i = 0; i < configuredTreadRecordInfo->numberOfRecords; i++) {
        fmt = cJSON_CreateObject();

        cJSON_AddStringToObject(fmt, "Tread_Depth", configuredTreadRecordInfo->treadDepthInfo[i].treadDepthValue);
        cJSON_AddNumberToObject(fmt, "Mileage", configuredTreadRecordInfo->treadDepthInfo[i].vehicleMileage);
        cJSON_AddNumberToObject(fmt, "last_updated", configuredTreadRecordInfo->treadDepthInfo[i].recordTime);
        cJSON_AddItemToArray(array, fmt);
    }
    *payloadAddr = cJSON_PrintUnformatted(root);
    cJSON_Delete(root);

    return GEN_SUCCESS;
}

/** @brief System data to JSON
 *
 *      API used for convert system data to json format
 *
 * @param[in] command type
 * @param[in] system value as per command type
 *
 * @return json payload or NULL
 */
static char * prepareJsonObject(commandCode_e cmdType, void *value)
{
    FUNC_ENTRY

    cJSON *root = NULL;
    cJSON *fmt = NULL;
    char *payloadAddr = NULL;

    CAB_DBG(CAB_GW_TRACE, "System data to JSON for command  %04x\n", cmdType);
    switch (cmdType) {
        case DIAG_GW_INFO_CMD:
        {
            sysDiagInfo_t *sysDiagInfo = (sysDiagInfo_t*)value;

            root = cJSON_CreateObject();
            if (!root)
                return NULL;

            cJSON_AddItemToObject(root, "gateway_info", fmt = cJSON_CreateObject());
            cJSON_AddStringToObject(fmt, "gateway_id", sysDiagInfo->gatewayId);
            cJSON_AddStringToObject(fmt, "hw_version", sysDiagInfo->hwVersion);
            cJSON_AddStringToObject(fmt, "sw_version", sysDiagInfo->swVersion);
            cJSON_AddStringToObject(fmt, "modem_IMEI", sysDiagInfo->modemIMEI);
            cJSON_AddStringToObject(fmt, "sim_ICCID", sysDiagInfo->simICCID);
            cJSON_AddStringToObject(fmt, "cloud_uri", sysDiagInfo->cloudUrl);
            payloadAddr = cJSON_PrintUnformatted(root);
            cJSON_Delete(root);
            return payloadAddr;
        } break;
        case DIAG_GW_PAIRED_SENSOR_CMD:
        case GET_CFG_GW_TPMS_PAIR_CMD:
        {
            int i;
            cJSON *array;
            tpmsPairedDiagInfo_t *tpmsPairedDiagInfo = (tpmsPairedDiagInfo_t *)value;

            root = cJSON_CreateObject();
            if (!root)
                return NULL;

            CAB_DBG(CAB_GW_TRACE, "Number of paired sensor %d\n", tpmsPairedDiagInfo->pairedNumber);
            array = cJSON_AddArrayToObject(root, "paired_tpms_info");
            for (i = 0; i < tpmsPairedDiagInfo->pairedNumber; i++) {
                fmt = cJSON_CreateObject();
                cJSON_AddNumberToObject(fmt, "id", tpmsPairedDiagInfo->tpmsInfo[i].sensorID);
                if (application_with_pn_mn) {
                    cJSON_AddStringToObject(fmt, "pn", tpmsPairedDiagInfo->tpmsInfo[i].sensorPN);
                    cJSON_AddStringToObject(fmt, "mn", tpmsPairedDiagInfo->tpmsInfo[i].sensorModel);
                }
                cJSON_AddStringToObject(fmt, "side", intTostrTireSide[tpmsPairedDiagInfo->tpmsInfo[i].vehicleSide]);
                cJSON_AddNumberToObject(fmt, "axle", tpmsPairedDiagInfo->tpmsInfo[i].axleNum);
                cJSON_AddStringToObject(fmt, "type", intTostrAttachedType[tpmsPairedDiagInfo->tpmsInfo[i].attachedType]);
                cJSON_AddStringToObject(fmt, "position", intTostrTyrePos[tpmsPairedDiagInfo->tpmsInfo[i].tyrePos]);
                cJSON_AddStringToObject(fmt, "halo_enabled", (tpmsPairedDiagInfo->tpmsInfo[i].isHaloEnable == DISABLE)?"no":"yes");
                cJSON_AddItemToArray(array, fmt);
            }
            payloadAddr = cJSON_PrintUnformatted(root);
            cJSON_Delete(root);
            return payloadAddr;
        } break;
        case DIAG_GW_TPMS_DATA_CMD:
        {
            int i;
            cJSON *array;
            tpmsDiagDataInfo_t *tpmsDiagDataInfo = (tpmsDiagDataInfo_t *)value;

            root = cJSON_CreateObject();
            if (!root)
                return NULL;

            array = cJSON_AddArrayToObject(root, "paired_tpms_data");
            for (i = 0; i < tpmsDiagDataInfo->pairedNumber; i++) {
                fmt = cJSON_CreateObject();
                cJSON_AddNumberToObject(fmt, "id", tpmsDiagDataInfo->data[i].tpmsData.sensorID);
		if( tpmsDiagDataInfo->data[i].time){
			cJSON_AddNumberToObject(fmt, "timestamp", tpmsDiagDataInfo->data[i].time);
			cJSON_AddNumberToObject(fmt, "pressure", tpmsDiagDataInfo->data[i].tpmsData.pressure);
			cJSON_AddNumberToObject(fmt, "temperature", tpmsDiagDataInfo->data[i].tpmsData.temperature);
			cJSON_AddNumberToObject(fmt, "tpms_rssi", tpmsDiagDataInfo->data[i].tpmsData.rssi);
			cJSON_AddNumberToObject(fmt, "battery_status", tpmsDiagDataInfo->data[i].tpmsData.battery);
			cJSON_AddNumberToObject(fmt, "tpms_status", tpmsDiagDataInfo->data[i].tpmsData.status);
		}
		else{
            		cJSON_AddStringToObject(fmt, "data: ","Not Available" );
		}
                cJSON_AddItemToArray(array, fmt);
            }
            payloadAddr = cJSON_PrintUnformatted(root);
            cJSON_Delete(root);
            return payloadAddr;
        } break;
        case DIAG_GW_TPMS_BY_ID_CMD:
        {
            tpmsDMData_t *tpmsDiagDataInfo = (tpmsDMData_t *)value;

            root = cJSON_CreateObject();
            if (!root)
                return NULL;

            cJSON_AddItemToObject(root, "tpms_data", fmt = cJSON_CreateObject());
            cJSON_AddNumberToObject(fmt, "id", tpmsDiagDataInfo->tpmsData.sensorID);
            cJSON_AddStringToObject(fmt, "side", intTostrTireSide[tpmsDiagDataInfo->wheelInfo.vehicleSide]);
            cJSON_AddNumberToObject(fmt, "axle", tpmsDiagDataInfo->wheelInfo.axleNum);
            cJSON_AddStringToObject(fmt, "type", intTostrAttachedType[tpmsDiagDataInfo->wheelInfo.attachedType]);
            cJSON_AddStringToObject(fmt, "position", intTostrTyrePos[tpmsDiagDataInfo->wheelInfo.tyrePos]);
            cJSON_AddStringToObject(fmt, "halo_enabled", (tpmsDiagDataInfo->wheelInfo.isHaloEnable == DISABLE)?"no":"yes");
	    if(tpmsDiagDataInfo->time){
		    cJSON_AddNumberToObject(fmt, "timestamp", tpmsDiagDataInfo->time);
		    cJSON_AddNumberToObject(fmt, "pressure", tpmsDiagDataInfo->tpmsData.pressure);
		    cJSON_AddNumberToObject(fmt, "temperature", tpmsDiagDataInfo->tpmsData.temperature);
		    cJSON_AddNumberToObject(fmt, "rssi", tpmsDiagDataInfo->tpmsData.rssi);
		    cJSON_AddNumberToObject(fmt, "battery", tpmsDiagDataInfo->tpmsData.battery);
		    cJSON_AddNumberToObject(fmt, "latitude", tpmsDiagDataInfo->gpsData.latitude);
		    cJSON_AddNumberToObject(fmt, "longitude", tpmsDiagDataInfo->gpsData.longitude);
		    cJSON_AddNumberToObject(fmt, "altitude", tpmsDiagDataInfo->gpsData.altitude);
		    cJSON_AddNumberToObject(fmt, "speed", tpmsDiagDataInfo->gpsData.speed);
		    cJSON_AddNumberToObject(fmt, "status", tpmsDiagDataInfo->tpmsData.status);
	    }
	    else{
		    cJSON_AddStringToObject(fmt, "data: ","Not Available" );
	    }
            payloadAddr = cJSON_PrintUnformatted(root);
            cJSON_Delete(root);
            return payloadAddr;
        } break;
        case GET_CFG_GW_ALL_HALO_SN_CMD:
        {
            if(getAllHaloSNData(value, &payloadAddr) == GEN_SUCCESS) {
                return payloadAddr;
            }
        } break;
        case GET_CFG_GW_TYRE_DETAILS_CMD:
        {
            if(getTireDetails(value, &payloadAddr) == GEN_SUCCESS) {
                return payloadAddr;
            }

        } break;
        case GET_CFG_GW_TREAD_HISTORY_CMD:
        {
            if(getTreadRecordHistrory(value, &payloadAddr) == GEN_SUCCESS) {
                return payloadAddr;
            }

        } break;
        case DIAG_GW_GPS_INFO_CMD:
        {
            gpsDiagInfo_t * gpsDiagInfo = (gpsDiagInfo_t *)value;

            root = cJSON_CreateObject();
            if (!root)
                return NULL;

            cJSON_AddItemToObject(root, "gps_info", fmt = cJSON_CreateObject());
	    if(gpsDiagInfo->timestamp)
	            cJSON_AddNumberToObject(fmt, "timestamp", gpsDiagInfo->timestamp);
            cJSON_AddNumberToObject(fmt, "latitude", gpsDiagInfo->gpsData.latitude);
            cJSON_AddNumberToObject(fmt, "longitude", gpsDiagInfo->gpsData.longitude);
            cJSON_AddNumberToObject(fmt, "altitude", gpsDiagInfo->gpsData.altitude);
            cJSON_AddNumberToObject(fmt, "speed", gpsDiagInfo->gpsData.speed);
            cJSON_AddNumberToObject(fmt, "satellites_in_view", gpsDiagInfo->noOfSatInView);
            cJSON_AddNumberToObject(fmt, "fix_quality", gpsDiagInfo->fixQuality);
            payloadAddr = cJSON_PrintUnformatted(root);
            cJSON_Delete(root);
            return payloadAddr;
        } break;
        case DIAG_GW_CELLULAR_INFO_CMD:
        {
            lteDiagInfo_t *lteDiagInfo = (lteDiagInfo_t *)value;

            root = cJSON_CreateObject();
            if (!root)
                return NULL;

            cJSON_AddItemToObject(root, "celluar_info", fmt = cJSON_CreateObject());
	    if(lteDiagInfo->timestamp)
            	    cJSON_AddNumberToObject(fmt, "Last conn. timestamp", lteDiagInfo->timestamp);
            cJSON_AddStringToObject(fmt, "network_operator", lteDiagInfo->operatorName);
            cJSON_AddNumberToObject(fmt, "signal_strength", lteDiagInfo->signalStrength);
            cJSON_AddNumberToObject(fmt, "signal_quality", lteDiagInfo->signalQuality);
            payloadAddr = cJSON_PrintUnformatted(root);
            cJSON_Delete(root);
            return payloadAddr;
        } break;
        case DIAG_GW_BATTERY_EV_LOG_CMD:
        {
            batteryDiagInfo_t* batteryDiagInfo = (batteryDiagInfo_t *)value;

            root = cJSON_CreateObject();
            if (!root)
                return NULL;

            cJSON_AddItemToObject(root, "backup_battery_logging", fmt = cJSON_CreateObject());
	    cJSON_AddStringToObject(fmt,"power source", batteryDiagInfo->powerSource);
	    if(batteryDiagInfo->startTime)
	            cJSON_AddNumberToObject(fmt, "timestamp", batteryDiagInfo->startTime);
            cJSON_AddStringToObject(fmt, "RTC coin cell", batteryDiagInfo->coinCellStatus);
            cJSON_AddStringToObject(fmt, "RTC status", batteryDiagInfo->RTCStatus);
            payloadAddr = cJSON_PrintUnformatted(root);
            cJSON_Delete(root);
            return payloadAddr;
        } break;
        case SYS_MOBILE_RESP_CMD:
        {
            commandStatus_t* commandStatus = (commandStatus_t *)value;

            root = cJSON_CreateObject();
            if (!root)
                return NULL;

            cJSON_AddItemToObject(root, "command_status", fmt = cJSON_CreateObject());
            cJSON_AddNumberToObject(fmt, "command_id", commandStatus->commandId);
            cJSON_AddNumberToObject(fmt, "status", commandStatus->status);
            cJSON_AddStringToObject(fmt, "response", commandStatus->msg);
            payloadAddr = cJSON_PrintUnformatted(root);
            cJSON_Delete(root);
            return payloadAddr;
        } break;
        case GET_CFG_GW_SYSTEM_CMD:
        {
            gatewayInfo_t *gatewayInfo = (gatewayInfo_t *)value;
            root = cJSON_CreateObject();
            if (!root)
                return NULL;

            cJSON_AddItemToObject(root, "vehicle_info", fmt = cJSON_CreateObject());
            cJSON_AddStringToObject(fmt, "customer_id", gatewayInfo->customerID);
            cJSON_AddStringToObject(fmt, "fleet_id", gatewayInfo->fleetID);
            cJSON_AddStringToObject(fmt, "type", gatewayInfo->vehicleType);
            cJSON_AddStringToObject(fmt, "vehicle_vin", gatewayInfo->vehicleVin);
            payloadAddr = cJSON_PrintUnformatted(root);
            cJSON_Delete(root);
            return payloadAddr;
        } break;
        case GET_CFG_GW_OPT_CMD:
        {
            vehicleOptInfo_t *vehicleOptionalInfo = (vehicleOptInfo_t *)value;

            root = cJSON_CreateObject();
            if (!root)
                return NULL;

            cJSON_AddItemToObject(root, "optional_vehicle_info", fmt = cJSON_CreateObject());
            cJSON_AddStringToObject(fmt, "fleet_name", vehicleOptionalInfo->fleetName);
            cJSON_AddStringToObject(fmt, "vehicle_make", vehicleOptionalInfo->vehicleMake);
            cJSON_AddNumberToObject(fmt, "vehicle_year", vehicleOptionalInfo->vehicleYear);
            cJSON_AddStringToObject(fmt, "tire_make", vehicleOptionalInfo->tyreMake);
            cJSON_AddStringToObject(fmt, "tire_size", vehicleOptionalInfo->tyreSize);
            payloadAddr = cJSON_PrintUnformatted(root);
            cJSON_Delete(root);
            return payloadAddr;
        } break;
        case GET_CFG_GW_TPMS_VENDOR_CMD:
        {
            root = cJSON_CreateObject();
            if (!root)
                return NULL;

            cJSON_AddStringToObject(root, "tpms_vendor_type", (char *)value);
            payloadAddr = cJSON_PrintUnformatted(root);
            cJSON_Delete(root);
            return payloadAddr;
        } break;
        case GET_CFG_GW_CLOUD_CMD:
        {
            root = cJSON_CreateObject();
            if (!root)
                return NULL;

            cJSON_AddStringToObject(root, "cloud_url", (char *)value);
            payloadAddr = cJSON_PrintUnformatted(root);
            cJSON_Delete(root);
            return payloadAddr;
        } break;
        case GET_CFG_GW_TARGET_PRESSURE_CMD:
        {
            targetPressureInfo_t *targetPressureInfo = (targetPressureInfo_t *)value;

            root = cJSON_CreateObject();
            if (!root)
                return NULL;

            fmt = cJSON_CreateIntArray(targetPressureInfo->tpmsThreshold, targetPressureInfo->numberOfAxle);
            cJSON_AddItemToObject(root, "target_pressures", fmt);
            payloadAddr = cJSON_PrintUnformatted(root);
            cJSON_Delete(root);
            return payloadAddr;
        } break;
        case GET_CFG_GW_BATTERY_CMD:
        {
            root = cJSON_CreateObject();
            if (!root)
                return NULL;
            cJSON_AddNumberToObject(root, "low_battery_threshold", *(int *)value);
            payloadAddr = cJSON_PrintUnformatted(root);
            cJSON_Delete(root);
            return payloadAddr;
        } break;
        case SELF_DIAG_GW_CMD:
        {
            selfDiagStatus_t *diagStatus = (selfDiagStatus_t *)value;
            selfDiagStatus_t tempSelfDiagStatus;

	    memset(&tempSelfDiagStatus, 0, sizeof(tempSelfDiagStatus));
            root = cJSON_CreateObject();
            if (!root)
                return NULL;

            cJSON_AddItemToObject(root, "self_diag_result", fmt = cJSON_CreateObject());

            /* if isDiagCompleted == 1 then send structure */
            if (diagStatus->isTestCompleted) {
                cJSON_AddStringToObject(fmt, "BLE", (diagStatus->ble_test_status == true) ? "PASS" : "FAIL");
                cJSON_AddStringToObject(fmt, "LTE SIM", (diagStatus->lte_sim_test_status == true) ? "PASS" : "FAIL");
                cJSON_AddStringToObject(fmt, "LTE Cloud", (diagStatus->lte_cloud_test_status == true) ? "PASS" : "FAIL");
                cJSON_AddStringToObject(fmt, "LTE RSSI", (diagStatus->lte_rssi_test_status == true) ? "PASS" : "FAIL");
                cJSON_AddStringToObject(fmt, "GPS", (diagStatus->gps_test_status == true) ? "PASS" : "FAIL");
                cJSON_AddStringToObject(fmt, "RTC", (diagStatus->rtc_test_status == true) ? "PASS" : "FAIL");
                cJSON_AddStringToObject(fmt, "Accelerometer", (diagStatus->accelerometer_test_status == true) ? "PASS" : "FAIL");
                cJSON_AddStringToObject(fmt, "Button & LED", (diagStatus->led_btn_test_status == true) ? "PASS" : "FAIL");
                cJSON_AddStringToObject(fmt, "TPMS", (diagStatus->rf_test_status == true) ? "PASS" : "FAIL");
		tempSelfDiagStatus.isTestCompleted = false;
		setConfigModuleParam(SELF_DIAG_GW_RESP_CMD, (void *)&tempSelfDiagStatus);
            }
            else {
            /* if isDiagCompleted == 0 then send in progress status */
                cJSON_AddStringToObject(fmt, "Test Status", "In Progress");
            }
            payloadAddr = cJSON_PrintUnformatted(root);
            cJSON_Delete(root);
            return payloadAddr;
        } break;
        case INSTALLATION_STATUS_CMD:
        {
            if (!value)
            {
                return DATA_PTR_INVALID;
            }

            cabGatewayStatus_t *installationStatus= (cabGatewayStatus_t *) value;
            root = cJSON_CreateObject();
            if (!root)
                return NULL;
            cJSON_AddItemToObject(root, "installation_status", fmt = cJSON_CreateObject());

            cJSON_AddNumberToObject(fmt, "cloud_connection_status", installationStatus->mntCloudConnectionStatus);
            cJSON_AddNumberToObject(fmt, "config_file_upload_status", installationStatus->configFileUploadStatus);
            cJSON_AddNumberToObject(fmt, "internet_connection", installationStatus->internetConnectivityTimestamp);
            payloadAddr = cJSON_PrintUnformatted(root);
            cJSON_Delete(root);
            return payloadAddr;
        } break;
        default:
	    {
		//No Implementation needed
	    }
    }
    return NULL;
}

/** @brief Send data to ble over uart
 *
 *      API used for send data to ble over UART
 *
 * @param[in] data
 * @param[in] data length
 *
 * @return status code
 */
static returnCode_e sendDataToUart(uint8_t *data, uint16_t len)
{
    FUNC_ENTRY

    int fd;
    uint8_t padCount,i=0;

    fd = open(UART_PORT, O_WRONLY | O_NOCTTY | O_SYNC);
    if (fd < 0) {
        CAB_DBG(CAB_GW_WARNING, "%s: %s\n", UART_PORT, strerror(errno));
        return FILE_OPEN_FAIL;
    }

    /* Add padding data for nrf module uart when data is less then MTU size */
    padCount = (ZERO_BYTE_CMD_LEN + APP_WISE_MAX_PAYLOAD(maxMTUPerApp)) - len;
    padCount = (padCount > 6) ? 6 : padCount;
    for(i=0; i < padCount; i++){
        data[len+i]= '\n';
    }
    len = len + padCount;

    if (len != write(fd, data, len)) {
        CAB_DBG(CAB_GW_WARNING, "write %s\n", strerror(errno));
        close(fd);
        return FILE_WRITE_ERROR;
    }
    CAB_DBG(CAB_GW_TRACE, "send byte %d\n", len);

    tcdrain(fd);
    close(fd);

    FUNC_EXIT

    return GEN_SUCCESS;
}

/** @brief Send data to ble
 *
 *      API used for sending data to mobile over BLE interface
 *
 * @param[in] command type
 * @param[in] value according to type
 *
 * @return status code
 */
returnCode_e sendDataToBle(commandCode_e cmdType, void *value)
{
    FUNC_ENTRY

    uartFrame_t uartFrame;
    uint8_t buf[UART_MTU_SIZE + 1];
    char *payload = NULL;
    int16_t payloadLen;
    returnCode_e retCode = DATA_SEND_FAIL;

    CAB_DBG(CAB_GW_TRACE, "sending command %04x\n", cmdType);
    memset(&uartFrame, '\0', sizeof(uartFrame));

    switch (cmdType) {
        case CFG_BLE_PASSKEY_CMD:
        {
            uartFrame.sof = UART_SOF;
            uartFrame.cmd = (uint16_t)SYS_BLE_PASSKEY_CMD;
            uartFrame.payloadLen = (uint16_t)MAX_BLE_PASSKEY_LEN;
            CAB_DBG(CAB_GW_TRACE, "ble passkey %s, payload len %d, %04x\n", (char *)value, uartFrame.payloadLen, (uint16_t)uartFrame.payloadLen);
            memcpy(uartFrame.data, (char *)value, MAX_BLE_PASSKEY_LEN);
            uartFrame.data[MAX_BLE_PASSKEY_LEN] = '\0';
            CAB_DBG(CAB_GW_TRACE, "ble passkey %s\n", uartFrame.data);
            uartFrame.data[uartFrame.payloadLen] = generateCRC8((uint8_t *)uartFrame.data, MAX_BLE_PASSKEY_LEN);
            dumpCommand(&uartFrame, __func__);
            retCode = sendDataToUart((uint8_t *)&uartFrame, ZERO_BYTE_CMD_LEN + MAX_BLE_PASSKEY_LEN);
        } break;
        case SYS_NOTIFY_GW_APP_START:
        {
            uartFrame.sof = UART_SOF;
            uartFrame.cmd = (uint16_t)cmdType;
            uartFrame.payloadLen = (uint16_t)MAX_LEN_LTE_IMEI;
            memcpy(uartFrame.data, value, MAX_LEN_LTE_IMEI);
            uartFrame.data[uartFrame.payloadLen] = generateCRC8((uint8_t *)uartFrame.data, uartFrame.payloadLen);
            retCode = sendDataToUart((uint8_t *)&uartFrame, ZERO_BYTE_CMD_LEN + uartFrame.payloadLen);
        } break;
        case SYS_BLE_MAC_CMD:
        {
            prepareDataPacket(cmdType, buf, NULL, 0);
            retCode = sendDataToUart((uint8_t *)&buf, ZERO_BYTE_CMD_LEN);
        } break;
        case SYS_ADVERTISEMENT_STARTED:
        {
            prepareDataPacket(cmdType, buf, NULL, 0);
            retCode = sendDataToUart((uint8_t *)&buf, ZERO_BYTE_CMD_LEN);
        } break;
        default:
        {
            for (int i=0; i < (sizeof(CmdLookupTable)/sizeof(CmdLookupTable[0])); i++) {
                int frameType = -1;

                 if (cmdType == CmdLookupTable[i].commandID) {
                    frameType = (connectedAppType == ANDROID_APP) ? CmdLookupTable[i].androidResponseFrameType : CmdLookupTable[i].iosResponseFrameType;
                 }
                 else {
                    continue;
                 }

                if (frameType == SINGLE_XFER_COMMAND) {
                    if (!value)
                    {
                        return DATA_PTR_INVALID;
                    }

                    uint8_t cipherText[240];
                    payload = prepareJsonObject(cmdType, value);
                    if (!payload)
                        break;
                    payloadLen = strlen(payload);

                    if (payloadLen > APP_WISE_MAX_PAYLOAD(maxMTUPerApp)) {
                        CAB_DBG(CAB_GW_WARNING, "Payload length not supported for single command\n");
                        free(payload);
                        break;
                    }
                    memset(&cipherText, 0x00, sizeof(cipherText));
                    memset(&buf, '\0', sizeof(buf));

                    aes128Encrypt((uint8_t *)payload, payloadLen, cipherText);
                    payloadLen += ((payloadLen % 16)?(16 - (payloadLen %16)):0);
                    prepareDataPacket(cmdType, buf, (char *)cipherText, payloadLen);
                    retCode = sendDataToUart(buf, ZERO_BYTE_CMD_LEN + payloadLen);
                    free(payload);
                }
                else if (frameType == FILE_XFER_COMMAND) {
                    /* Changing sleep as per testing done for CG-657 with 40 TPMS diag & cfg data*/
                    const struct timespec timeSpec = {.tv_sec = 0, .tv_nsec = 600000000};
                    uint8_t paddingLen;

                    payload = prepareJsonObject(cmdType, value);
                    if (!payload)
                        break;

                    payloadLen = (uint16_t)strlen(payload);
                    paddingLen = (payloadLen % 16)?(16 - (payloadLen % 16)):0;
                    uint8_t cipherText[payloadLen + paddingLen];
                    memset(&cipherText, '\0', sizeof(cipherText));
                    aes128Encrypt((uint8_t *)payload, payloadLen, cipherText);
                    free(payload);
                    payload = (char *)&cipherText[0];
                    payloadLen += paddingLen;

                    prepareFileInitPacket(cmdType, buf, payloadLen);
                    retCode = sendDataToUart(buf, ZERO_BYTE_FILE_CMD_LEN);
                    if (retCode != GEN_SUCCESS) {
                        break;
                    }
                    while (payloadLen/APP_WISE_MAX_PAYLOAD(maxMTUPerApp)) {
                        prepareDataPacket(CFG_FILE_DATA_CMD, buf, payload + (uartFrame.frameCount * APP_WISE_MAX_PAYLOAD(maxMTUPerApp)), APP_WISE_MAX_PAYLOAD(maxMTUPerApp));
                        retCode = sendDataToUart(buf,  ZERO_BYTE_CMD_LEN + APP_WISE_MAX_PAYLOAD(maxMTUPerApp));
                        if (retCode != GEN_SUCCESS) {
                            break;
                        }
                        payloadLen -= APP_WISE_MAX_PAYLOAD(maxMTUPerApp);
                        ++uartFrame.frameCount;
                        nanosleep(&timeSpec, NULL);
                    }
                    CAB_DBG(CAB_GW_TRACE, "End of file packet len %04x, %d\n", (uint16_t)payloadLen, payloadLen);
                    if (GEN_SUCCESS == retCode && payloadLen) {
                        prepareDataPacket(CFG_FILE_DATA_CMD, buf, payload + (uartFrame.frameCount * APP_WISE_MAX_PAYLOAD(maxMTUPerApp)), payloadLen);
                        retCode = sendDataToUart(buf, ZERO_BYTE_CMD_LEN + payloadLen);
                    }
                    CAB_DBG(CAB_GW_TRACE, "system diag info success send to mobile\n");
                }
                else {
                    // Nothing to be implemented
                }
                break;
            }
        } break;
    }

    FUNC_EXIT
    return retCode;
}

/** @brief Send response of command to mobile
 *
 *     API used for sending response of received command
 *
 * @param[in] command id
 * @param[in] command status
 *
 * @return none.
 */
void sendCommandStatus(commandCode_e cmd, returnCode_e retCode)
{
    FUNC_ENTRY

    commandStatus_t commandStatus;
    commandStatus.commandId = (uint16_t)cmd;
    memset(&commandStatus.msg, '\0', sizeof(commandStatus.msg));
    snprintf(commandStatus.msg, MAX_RESPONSE_LEN, "%s", convertReturnCodetoStr(retCode));
    commandStatus.status = (retCode != GEN_SUCCESS)?0:1;
    CAB_DBG(CAB_GW_TRACE, "Sending response command\n");
    CAB_DBG(CAB_GW_TRACE, "status %d, message %s\n", commandStatus.status, commandStatus.msg);
    sendDataToBle(SYS_MOBILE_RESP_CMD, &commandStatus);

    FUNC_EXIT
}

/** @brief setup uart port
 *
 *      API for setting up uart port related changes
 *
 * @param[in] uart port name
 * @param[in] uart baudrate
 *
 * @return status code.
 */
static returnCode_e setUartPort(const char *portName, uint32_t baudrate)
{
    FUNC_ENTRY

    struct termios tty;
    int fd;

    /*baudrate 115200, 8 bits, no parity, 1 stop bit */
    fd = open(portName, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0) {
        CAB_DBG(CAB_GW_ERR, "open %s: %s\n", portName, strerror(errno));
        return FILE_OPEN_FAIL;
    }

    if (tcgetattr(fd, &tty) < 0) {
        CAB_DBG(CAB_GW_ERR ,"tcgetattr %s\n", strerror(errno));
        close(fd);
        return FILE_CONTROL_GET_FAIL;
    }

    cfsetospeed(&tty, (speed_t)baudrate);
    cfsetispeed(&tty, (speed_t)baudrate);

    tty.c_cflag |= (CLOCAL | CREAD);
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;         /* 8-bit characters */
    tty.c_cflag &= ~PARENB;     /* no parity bit */
    tty.c_cflag &= ~CSTOPB;     /* only need 1 stop bit */
    tty.c_cflag &= ~CRTSCTS;    /* no hardware flowcontrol */

    /* setup for non-canonical mode */
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    tty.c_oflag &= ~OPOST;

    tty.c_cc[VMIN] = 7;
    tty.c_cc[VTIME] = 5;

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        CAB_DBG(CAB_GW_ERR, "tcsetattr: %s\n", strerror(errno));
        close(fd);
        return FILE_CONTROL_SET_FAIL;
    }
    close(fd);

    FUNC_EXIT

    return GEN_SUCCESS;
}

static bool getUartDataHandlerAliveStatus(void)
{
	bool status;
	pthread_mutex_lock(&uartDataHandlerAliveLock);
	status = g_bleUartDataHandlerAliveStatus;
	pthread_mutex_unlock(&uartDataHandlerAliveLock);
	return status;
}

static void setUartDataHandlerAliveStatus(bool value)
{
	pthread_mutex_lock(&uartDataHandlerAliveLock);
	g_bleUartDataHandlerAliveStatus = value;
	pthread_mutex_unlock(&uartDataHandlerAliveLock);
}


bool uartDataHandlerCallback(void)
{
	bool status = false;
	CAB_DBG(CAB_GW_TRACE, "In func : %s\n", __func__);
	if(getUartDataHandlerAliveStatus() == true)
	{
		status = true;
	}
	
	setUartDataHandlerAliveStatus(false);
	return status;
}

/** @brief BLE uart receiver
 *
 *      This api will receive ble data and if command is valid
 *  then pass it for further processing
 *
 * @param[in] none
 *
 * @return none.
 */
static void *uartDataHandler(void *argv)
{
    FUNC_ENTRY

    int readLen;
    int fd;
    uint8_t buf[UART_MTU_SIZE];
    uint16_t skip_offset = 0;
    uartFrame_t *uartFrame = NULL;
    mqd_t msgQueueId;
    msgQueue_t msgQueue;
    returnCode_e retCode = GEN_SUCCESS;
    char errorMsg[100] = "";

    struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0};
    watchDogTimer_t newWatchdogNode;
    
    newWatchdogNode.threadId = syscall(SYS_gettid);
    newWatchdogNode.CallbackFn = uartDataHandlerCallback;
   
    CAB_DBG(CAB_GW_INFO, "BLE UART data handler thread id : %ld", syscall(SYS_gettid));
    retCode = threadRegister(&newWatchdogNode);
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "UART data handler thread registration failed with error %d", retCode);
            rDiagSendMessage(WATCHDOG_REGISTRATION_FAIL, GW_ERR, "UART data handler thread registration failed");
    }        

    fd = open(UART_PORT, O_RDONLY | O_NOCTTY | O_SYNC);
    if (fd < 0) {
        CAB_DBG(CAB_GW_ERR, "%s: %s\n", UART_PORT, strerror(errno));
        retCode = threadDeregister(newWatchdogNode.threadId);
        if(retCode != GEN_SUCCESS)     
        {
           CAB_DBG(CAB_GW_ERR, "UART data handler thread de-registration failed with error %d", retCode);
           rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "UART data handler thread de-registration failed");
       }        
       return NULL;
   }

    CAB_DBG(CAB_GW_TRACE, "Uart data\n");
    msgQueueId = mq_open(QUEUE_NAME, O_WRONLY);
    if (msgQueueId == (mqd_t)-1) {
        CAB_DBG(CAB_GW_ERR, "%s: %s\n", QUEUE_NAME, strerror(errno));
        close(fd);
        retCode = threadDeregister(newWatchdogNode.threadId);
        if(retCode != GEN_SUCCESS)     
        {
           CAB_DBG(CAB_GW_ERR, "UART data handler thread de-registration failed with error %d", retCode);
           rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "UART data handler thread de-registration failed");
       }        
       return NULL;
   }

    CAB_DBG(CAB_GW_TRACE, "Start receiving uart data");

    while (1) {
        memset(&buf, '\0', sizeof(buf));
        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
        readLen = read(fd, buf, UART_MTU_SIZE);
        if (readLen >= SOF_LEN) {
            pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);

            skip_offset = 0;
            while(readLen>0 && memcmp(buf+skip_offset, sof, SOF_LEN)) {
                CAB_DBG(CAB_GW_WARNING, "skip %02x %02x", buf[skip_offset], buf[skip_offset+1]);
                readLen -= SOF_LEN;
                skip_offset += SOF_LEN;
            }

            uartFrame = (uartFrame_t*)buf;
            if (readLen < (ZERO_BYTE_CMD_LEN + uartFrame->payloadLen)) {
                CAB_DBG(CAB_GW_WARNING, "BLE command incomplete");
                sendCommandStatus(CFG_GW_SYSTEM_CMD, DATA_NOT_SUFFICIENT);
                continue;
            }

            setUartDataHandlerAliveStatus(true);

            switch (uartFrame->cmd) {
                case SYS_NOTIFY_BLE_CONNECTED:
                case SYS_NOTIFY_BLE_DISCONNECTD:
                {
                    /* Setting BLE keep alive true as valid command received from BLE chip. */
                    setBLEKeepAliveStatus(true);
                    CAB_DBG(CAB_GW_TRACE, "mobile-cabgateway %s",uartFrame->cmd == SYS_NOTIFY_BLE_CONNECTED?"Connected":"Disconnected");
                    setConfigModuleParam(uartFrame->cmd, uartFrame->data);
                } break;
                case SYS_MTU_UPDATED:
                {
                    uint16_t mtu_size = 0;
                    memcpy(&mtu_size, uartFrame->data, sizeof(uint16_t));
                    maxMTUPerApp = mtu_size;
                    connectedAppType = CONNECTED_APP(maxMTUPerApp);
                    CAB_DBG(CAB_GW_INFO, "Changed MTU size is: %d %d", maxMTUPerApp, APP_WISE_MAX_PAYLOAD(maxMTUPerApp));
                    application_with_pn_mn = false; /* We will be considering it as old app for backward compatibility */
                } break;
                case SYS_NOTIFY_MOBILE_DATA_SUCCESS:
                case SYS_NOTIFY_MOBILE_DATA_FAIL:
                {
                    /* Setting BLE keep alive true as valid command received from BLE chip. */
                    setBLEKeepAliveStatus(true);
                    CAB_DBG(CAB_GW_TRACE, "cabgateway-mobile data send %s", uartFrame->cmd == SYS_NOTIFY_MOBILE_DATA_FAIL?"fail":"success");
                } break;
                case SYS_BLE_MAC_CMD:
                {
                    /* Setting BLE keep alive true as valid command received from BLE chip. */
                    setBLEKeepAliveStatus(true);
                    setConfigModuleParam(uartFrame->cmd, uartFrame->data);
                    CAB_DBG(CAB_GW_TRACE, "ble mac address");
                } break;
                case SYS_ADVERTISEMENT_STARTED:
                {
                    setConfigModuleParam(uartFrame->cmd, uartFrame->data);
                } break;
                case SYS_NOTIFY_BLE_RESET_CMD:
                {
                    /* Setting BLE keep alive true as valid command received from BLE chip. */
                    setBLEKeepAliveStatus(true);
                    UNUSED_VARIABLE(bleSetPowerMode(uartFrame->cmd));
                    CAB_DBG(CAB_GW_TRACE, "Reset ble after password successful updated");
                } break;
                default:
                {
                    bool commandFound = false;
                    for (int i=0; i < (sizeof(CmdLookupTable)/sizeof(CmdLookupTable[0])); i++) {
                        if (uartFrame->cmd == CmdLookupTable[i].commandID) {
                            commandFound = true;
                            /* Setting BLE keep alive true as valid command received from BLE chip. */
                            setBLEKeepAliveStatus(true);
                            uartFrame = malloc(sizeof(uartFrame_t));
                            if (!uartFrame) {
                                CAB_DBG(CAB_GW_ERR, "Ble command : out of memory");
                                break;
                            }
                            memset(&uartFrame->data, '\0', UART_MTU_SIZE);
                            memcpy(uartFrame, buf + skip_offset, readLen-skip_offset);
                            msgQueue.ptr = uartFrame;
                            clock_gettime(CLOCK_REALTIME, &abs_timeout);
                            abs_timeout.tv_sec += MQ_SEND_BLE_CMD_TOUT;
                            if (mq_timedsend(msgQueueId, (const char *)&msgQueue, MAX_MSG_SIZE, 0, &abs_timeout)) {
                                CAB_DBG(CAB_GW_WARNING, "mq_timedsend for %s failed with error %s", QUEUE_NAME, strerror(errno));
                                sprintf(errorMsg, "mq_timedsend for %s failed with error %s", QUEUE_NAME, strerror(errno));
                                rDiagSendMessage(MESSAGE_QUEUE_OVERFLOW, GW_WARNING, errorMsg);
                                free(uartFrame);
                            }
                            CAB_DBG(CAB_GW_TRACE, "Queued up message");
                            break;
                        }
                    }

                    if (!commandFound) {
                        CAB_DBG(CAB_GW_WARNING, "Invalid command ID %4x\n", uartFrame->cmd);
                    }
                } break;
            }
            CAB_DBG(CAB_GW_TRACE, "Ready to receive next command\n");
        } 
        else {
            CAB_DBG(CAB_GW_WARNING, "Receiving garbage data");
        }
    }
    close(fd);
    mq_close(msgQueueId);

    retCode = threadDeregister(newWatchdogNode.threadId);
    if(retCode != GEN_SUCCESS)     
    {
        CAB_DBG(CAB_GW_ERR, "UART data handler thread de-registration failed with error %d", retCode);
        rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "UART data handler thread de-registration failed");
    }        
    FUNC_EXIT

    pthread_exit(NULL);
}

/******************************************************************
 *@brief  (It is used to get details to attach HALO SN for axle 
 *          number and vehicle side)
 *
 *@param[IN] const char * (JSON payload string)
 *@param[OUT] void * (pointer to output structure)
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
 static returnCode_e attachHaloSN(const char *cjson, void *output)
 {
    haloSNInfo_t *haloSNInfo = (haloSNInfo_t *)output;
    cJSON *root = NULL;
    cJSON *item = NULL;
    cJSON *value = NULL;

    if (!haloSNInfo)
        return DATA_PTR_INVALID;

    root = cJSON_Parse(cjson);
    if (!root)
        return DATA_INVALID;

    item = cJSON_GetObjectItem(root, "halo_info");
    if (!item) {
        cJSON_Delete(root);
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "side");
    if (value && value->valuestring) {
        if (memcmp(value->valuestring, intTostrTireSide[0], strlen(intTostrTireSide[0])) == 0) {
            haloSNInfo->vehicleSide =  LEFT;
        } else if (memcmp(value->valuestring, intTostrTireSide[1], strlen(intTostrTireSide[1])) == 0) {
            haloSNInfo->vehicleSide =  RIGHT;
        } else {
            CAB_DBG(CAB_GW_ERR, "Invalid vehicle side");
            return DATA_INVALID;
        }
    } else {
        CAB_DBG(CAB_GW_ERR, "No value of vehicle side");
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "axle");
    if (value && value->valueint) {
        if (value->valueint != 0 )  {
            haloSNInfo->axleNum = value->valueint;
        } else {
            CAB_DBG(CAB_GW_ERR, "Invalid axle");
            return DATA_INVALID;
        }
    } else {
        CAB_DBG(CAB_GW_ERR, "No value of axle");
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "halo_serial_number");
    if (value && value->valuestring && (strlen(value->valuestring) < MAX_LEN_HALO_SN)) {
        snprintf(haloSNInfo->serialNo, MAX_LEN_HALO_SN, "%s", value->valuestring);
    } else {
        CAB_DBG(CAB_GW_ERR, "Invalid value of HALO SN");
        return DATA_INVALID;
    }

    CAB_DBG(CAB_GW_TRACE, "==== HALO SN info ====\n");
    CAB_DBG(CAB_GW_TRACE, "side %s\n", (haloSNInfo->vehicleSide == RIGHT)?"right":"left");
    CAB_DBG(CAB_GW_TRACE, "Axel %d\n", haloSNInfo->axleNum);
    CAB_DBG(CAB_GW_TRACE, "HALO SN %s\n", haloSNInfo->serialNo);
    CAB_DBG(CAB_GW_TRACE, "==========================\n");
    cJSON_Delete(root);

    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (It is used to get details to detach HALO SN)
 *
 *@param[IN] const char * (JSON payload string)
 *@param[OUT] void * (pointer to output structure)
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
 static returnCode_e detachHaloSN(const char *cjson, void *output)
 {
    haloSNInfo_t *haloSNInfo = (haloSNInfo_t *)output;
    cJSON *root = NULL;
    cJSON *value = NULL;

    if (!haloSNInfo)
        return DATA_PTR_INVALID;

    root = cJSON_Parse(cjson);
    if (!root)
        return DATA_INVALID;

    value = cJSON_GetObjectItem(root, "halo_serial_number");
    if (value && value->valuestring && (strlen(value->valuestring) < MAX_LEN_HALO_SN)) {
        snprintf(haloSNInfo->serialNo, MAX_LEN_HALO_SN, "%s", value->valuestring);
    } else {
        CAB_DBG(CAB_GW_ERR, "Invalid value of HALO SN");
        return DATA_INVALID;
    }

    cJSON_Delete(root);

    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (It is used to get details to update tyre details for
 *          given tyre position)
 *
 *@param[IN] const char * (JSON payload string)
 *@param[OUT] void * (pointer to output structure)
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e configureTyreDetails(const char *cjson, void *output)
{
    tyreInfo_t *tyreInfo = (tyreInfo_t *)output;
    cJSON *root = NULL;
    cJSON *item = NULL;
    cJSON *value = NULL;

    root = cJSON_Parse(cjson);
    if (!root)
        return DATA_INVALID;

    item = cJSON_GetObjectItem(root, "tire_details");
    if (!item) {
        cJSON_Delete(root);
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "axle");
    if (value && value->valueint) {
        if (value->valueint != 0 )  {
            tyreInfo->axleNum = value->valueint;
        } else {
            CAB_DBG(CAB_GW_ERR, "Invalid axle");
            return DATA_INVALID;
        }
    } else {
        CAB_DBG(CAB_GW_ERR, "No value of axle");
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "side");
    if (value && value->valuestring) {
        if (memcmp(value->valuestring, intTostrTireSide[0], strlen(intTostrTireSide[0])) == 0) {
            tyreInfo->vehicleSide =  LEFT;
        } else if (memcmp(value->valuestring, intTostrTireSide[1], strlen(intTostrTireSide[1])) == 0) {
            tyreInfo->vehicleSide =  RIGHT;
        } else {
            CAB_DBG(CAB_GW_ERR, "Invalid vehicle side");
            return DATA_INVALID;
        }
    } else {
        CAB_DBG(CAB_GW_ERR, "No value of vehicle side");
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "position");
    if (value && value->valuestring) {
        if (memcmp(value->valuestring, intTostrTyrePos[0], strlen(intTostrTyrePos[0])) == 0) {
            tyreInfo->tyrePos =  INNER;
        } else if (memcmp(value->valuestring, intTostrTyrePos[1], strlen(intTostrTyrePos[1])) == 0) {
            tyreInfo->tyrePos =  OUTER;
        } else {
            CAB_DBG(CAB_GW_ERR, "Invalid tyre position");
            return DATA_INVALID;
        }
    } else {
        CAB_DBG(CAB_GW_ERR, "No tyre position ");
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "Serial_No");
    if (value && value->valuestring && (strlen(value->valuestring) < MAX_LEN_TYRE_SERIAL_NO)) {
        snprintf(tyreInfo->tyreSerialNo, MAX_LEN_TYRE_SERIAL_NO, "%s", value->valuestring);
    } else {
        CAB_DBG(CAB_GW_ERR, "Invalid value of Tyre Serial Number");
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "Tire_TIN");
    if (value && value->valuestring && (strlen(value->valuestring) < MAX_LEN_TYRE_TIN)) {
        snprintf(tyreInfo->tyreTIN, MAX_LEN_TYRE_TIN, "%s", value->valuestring);
    } else {
        CAB_DBG(CAB_GW_ERR, "Invalid value of Tyre TIN");
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "Tire_Make");
    if (value && value->valuestring && (strlen(value->valuestring) < MAX_LEN_TYRE_MAKE_STR)) {
        snprintf(tyreInfo->tyreMake, MAX_LEN_TYRE_MAKE_STR, "%s", value->valuestring);
    } else {
        CAB_DBG(CAB_GW_ERR, "Invalid value of Tyre make");
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "Tire_Model");
    if (value && value->valuestring && (strlen(value->valuestring) < MAX_LEN_TYRE_MODEL)) {
        snprintf(tyreInfo->tyreModel, MAX_LEN_TYRE_MODEL, "%s", value->valuestring);
    } else {
        CAB_DBG(CAB_GW_ERR, "Invalid value of Tyre model");
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "Tire_width");
    if (value && value->valuestring && (strlen(value->valuestring) < MAX_LEN_TYRE_WIDTH)) {
        snprintf(tyreInfo->tyreWidth, MAX_LEN_TYRE_WIDTH, "%s", value->valuestring);
    } else {
        CAB_DBG(CAB_GW_ERR, "Invalid value of Tyre width");
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "Ratio");
    if (value && value->valuestring && (strlen(value->valuestring) < MAX_LEN_RATIO)) {
        snprintf(tyreInfo->ratio, MAX_LEN_RATIO, "%s", value->valuestring);
    } else {
        CAB_DBG(CAB_GW_ERR, "Invalid value of Ratio");
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "Diameter");
    if (value && value->valuestring && (strlen(value->valuestring) < MAX_LEN_DIAMETER)) {
        snprintf(tyreInfo->diameter, MAX_LEN_DIAMETER, "%s", value->valuestring);
    } else {
        CAB_DBG(CAB_GW_ERR, "Invalid value of Diameter");
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "Load_Rating");
    if (value && value->valuestring && (strlen(value->valuestring) < MAX_LEN_LOAD_RATING)) {
        snprintf(tyreInfo->loadRating, MAX_LEN_LOAD_RATING, "%s", value->valuestring);
    } else {
        CAB_DBG(CAB_GW_ERR, "Invalid value of Load rating");
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "Tread_Depth");
    if (value && value->valuestring && (strlen(value->valuestring) < MAX_LEN_TREAD_DEPTH)) {
        snprintf(tyreInfo->treadDepthHistory[0].treadDepthValue, MAX_LEN_TREAD_DEPTH, "%s", value->valuestring);
    } else {
        CAB_DBG(CAB_GW_ERR, "Invalid value of Tread depth");
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "Mileage");
    if (value && value->valueint) {
        if (value->valueint != 0 )  {
            tyreInfo->treadDepthHistory[0].vehicleMileage = value->valueint;
        } else {
            CAB_DBG(CAB_GW_ERR, "Invalid Mileage value");
            return DATA_INVALID;
        }
    } else {
        CAB_DBG(CAB_GW_ERR, "No value of Mileage");
        return DATA_INVALID;
    }

    CAB_DBG(CAB_GW_TRACE, "==== Tyre info ====\n");
    CAB_DBG(CAB_GW_TRACE, "side %s\n", (tyreInfo->vehicleSide == RIGHT)?"right":"left");
    CAB_DBG(CAB_GW_TRACE, "Position %s\n", (tyreInfo->tyrePos == OUTER)?"outer":"inner");
    CAB_DBG(CAB_GW_TRACE, "Axel %d\n", tyreInfo->axleNum);
    CAB_DBG(CAB_GW_TRACE, "Tyre SN %s\n", tyreInfo->tyreSerialNo);
    CAB_DBG(CAB_GW_TRACE, "Tyre TIN %s\n", tyreInfo->tyreTIN);
    CAB_DBG(CAB_GW_TRACE, "Tyre Make %s\n", tyreInfo->tyreMake);
    CAB_DBG(CAB_GW_TRACE, "Tyre Model %s\n", tyreInfo->tyreModel);
    CAB_DBG(CAB_GW_TRACE, "Tyre Width %s\n", tyreInfo->tyreWidth);
    CAB_DBG(CAB_GW_TRACE, "Tyre Ratio %s\n", tyreInfo->ratio);
    CAB_DBG(CAB_GW_TRACE, "Tyre Diameter %s\n", tyreInfo->diameter);
    CAB_DBG(CAB_GW_TRACE, "Tyre Load Rating %s\n", tyreInfo->loadRating);
    CAB_DBG(CAB_GW_TRACE, "Tyre Tread depth %s\n", tyreInfo->treadDepthHistory[0].treadDepthValue);
    CAB_DBG(CAB_GW_TRACE, "Vehicle Mileage %u\n", tyreInfo->treadDepthHistory[0].vehicleMileage);
    CAB_DBG(CAB_GW_TRACE, "==========================\n");
    cJSON_Delete(root);

    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (It is used to get details to flush tyre details for
 *          given tyre position)
 *
 *@param[IN] const char * (JSON payload string)
 *@param[OUT] void * (pointer to output structure)
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e getTyrePositionDetails(const char *cjson, void *output)
{
    tyrePosInfo_t *tyrePosInfo = (tyrePosInfo_t *)output;
    cJSON *root = NULL;
    cJSON *item = NULL;
    cJSON *value = NULL;

    root = cJSON_Parse(cjson);
    if (!root)
        return DATA_INVALID;

    item = cJSON_GetObjectItem(root, "tire_position");
    if (!item) {
        cJSON_Delete(root);
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "axle");
    if (value && value->valueint) {
        if (value->valueint != 0 )  {
            tyrePosInfo->axleNum = value->valueint;
        } else {
            CAB_DBG(CAB_GW_ERR, "Invalid axle");
            return DATA_INVALID;
        }
    } else {
        CAB_DBG(CAB_GW_ERR, "No value of axle");
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "side");
    if (value && value->valuestring) {
        if (memcmp(value->valuestring, intTostrTireSide[0], strlen(intTostrTireSide[0])) == 0) {
            tyrePosInfo->vehicleSide =  LEFT;
        } else if (memcmp(value->valuestring, intTostrTireSide[1], strlen(intTostrTireSide[1])) == 0) {
            tyrePosInfo->vehicleSide =  RIGHT;
        } else {
            CAB_DBG(CAB_GW_ERR, "Invalid vehicle side");
            return DATA_INVALID;
        }
    } else {
        CAB_DBG(CAB_GW_ERR, "No value of vehicle side");
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "position");
    if (value && value->valuestring) {
        if (memcmp(value->valuestring, intTostrTyrePos[0], strlen(intTostrTyrePos[0])) == 0) {
            tyrePosInfo->tyrePos =  INNER;
        } else if (memcmp(value->valuestring, intTostrTyrePos[1], strlen(intTostrTyrePos[1])) == 0) {
            tyrePosInfo->tyrePos =  OUTER;
        } else {
            CAB_DBG(CAB_GW_ERR, "Invalid tyre position");
            return DATA_INVALID;
        }
    } else {
        CAB_DBG(CAB_GW_ERR, "No tire position ");
        return DATA_INVALID;
    }

    CAB_DBG(CAB_GW_TRACE, "==== Tyre position info ====\n");
    CAB_DBG(CAB_GW_TRACE, "side %s\n", (tyrePosInfo->vehicleSide == RIGHT)?"right":"left");
    CAB_DBG(CAB_GW_TRACE, "Position %s\n", (tyrePosInfo->tyrePos == OUTER)?"outer":"inner");
    CAB_DBG(CAB_GW_TRACE, "Axel %d\n", tyrePosInfo->axleNum);
    CAB_DBG(CAB_GW_TRACE, "==========================\n");
    cJSON_Delete(root);

    return GEN_SUCCESS;
}

/******************************************************************
 *@brief  (It is used to get details to flush tyre details for
 *          given tyre position)
 *
 *@param[IN] const char * (JSON payload string)
 *@param[OUT] void * (pointer to output structure)
 *
 *@return returnCode_e (It returns the type of error (i.e. GEN_SUCCESS, FAILURE, ERROR, etc...))
 *********************************************************************/
static returnCode_e treadRecordDetails(const char *cjson, void *output)
{
    treadRecordInfo_t *treadRecordInfo = (treadRecordInfo_t *)output;
    cJSON *root = NULL;
    cJSON *item = NULL;
    cJSON *value = NULL;

    root = cJSON_Parse(cjson);
    if (!root)
        return DATA_INVALID;

    item = cJSON_GetObjectItem(root, "tire_position");
    if (!item) {
        cJSON_Delete(root);
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "axle");
    if (value && value->valueint) {
        if (value->valueint != 0 )  {
            treadRecordInfo->tyrePosInfo.axleNum = value->valueint;
        } else {
            CAB_DBG(CAB_GW_ERR, "Invalid axle");
            return DATA_INVALID;
        }
    } else {
        CAB_DBG(CAB_GW_ERR, "No value of axle");
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "side");
    if (value && value->valuestring) {
        if (memcmp(value->valuestring, intTostrTireSide[0], strlen(intTostrTireSide[0])) == 0) {
            treadRecordInfo->tyrePosInfo.vehicleSide =  LEFT;
        } else if (memcmp(value->valuestring, intTostrTireSide[1], strlen(intTostrTireSide[1])) == 0) {
            treadRecordInfo->tyrePosInfo.vehicleSide =  RIGHT;
        } else {
            CAB_DBG(CAB_GW_ERR, "Invalid vehicle side");
            return DATA_INVALID;
        }
    } else {
        CAB_DBG(CAB_GW_ERR, "No value of vehicle side");
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "position");
    if (value && value->valuestring) {
        if (memcmp(value->valuestring, intTostrTyrePos[0], strlen(intTostrTyrePos[0])) == 0) {
            treadRecordInfo->tyrePosInfo.tyrePos =  INNER;
        } else if (memcmp(value->valuestring, intTostrTyrePos[1], strlen(intTostrTyrePos[1])) == 0) {
            treadRecordInfo->tyrePosInfo.tyrePos =  OUTER;
        } else {
            CAB_DBG(CAB_GW_ERR, "Invalid tyre position");
            return DATA_INVALID;
        }
    } else {
        CAB_DBG(CAB_GW_ERR, "No tire position ");
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "Tread_Depth");
    if (value && value->valuestring && (strlen(value->valuestring) < MAX_LEN_TREAD_DEPTH)) {
        snprintf(treadRecordInfo->treadDepthInfo.treadDepthValue, MAX_LEN_TREAD_DEPTH, "%s", value->valuestring);
    } else {
        CAB_DBG(CAB_GW_ERR, "Invalid value of Tread depth");
        return DATA_INVALID;
    }

    value = cJSON_GetObjectItem(item, "Mileage");
    if (value && value->valueint) {
        if (value->valueint != 0 )  {
            treadRecordInfo->treadDepthInfo.vehicleMileage = value->valueint;
        } else {
            CAB_DBG(CAB_GW_ERR, "Invalid Mileage value");
            return DATA_INVALID;
        }
    } else {
        CAB_DBG(CAB_GW_ERR, "No value of Mileage");
        return DATA_INVALID;
    }

    CAB_DBG(CAB_GW_TRACE, "==== Tread Record info ====\n");
    CAB_DBG(CAB_GW_TRACE, "side %s\n", (treadRecordInfo->tyrePosInfo.vehicleSide == RIGHT)?"right":"left");
    CAB_DBG(CAB_GW_TRACE, "Position %s\n", (treadRecordInfo->tyrePosInfo.tyrePos == OUTER)?"outer":"inner");
    CAB_DBG(CAB_GW_TRACE, "Axel %d\n", treadRecordInfo->tyrePosInfo.axleNum);
    CAB_DBG(CAB_GW_TRACE, "Tyre Tread depth %s\n", treadRecordInfo->treadDepthInfo.treadDepthValue);
    CAB_DBG(CAB_GW_TRACE, "Vehicle Mileage %u\n", treadRecordInfo->treadDepthInfo.vehicleMileage);
    CAB_DBG(CAB_GW_TRACE, "==========================\n");
    cJSON_Delete(root);

    return GEN_SUCCESS;

}

/** @brief JSON to system structure
 *
 *      API used for converting json payload to system strucutre
 *
 * @param[in] command type
 * @param[in] json object
 * @param[out] system data as per command type
 *
 * @return status code
 */
returnCode_e convertJsonToSystemPayload(commandCode_e cmdType, const char *cjson, void *output)
{
    FUNC_ENTRY

    cJSON *root = NULL;
    returnCode_e retCode;

    if (!cjson)
        return DATA_PTR_INVALID;

    switch (cmdType) {
        case CFG_BLE_PASSKEY_CMD:
        {
            char *data = (char *)output;
	    uint8_t keyInd = 0;
            cJSON *value;

            if (!data)
                return DATA_PTR_INVALID;

            root = cJSON_Parse(cjson);
            if (!root)
                return DATA_INVALID;

            value = cJSON_GetObjectItem(root, "ble_password");
            if (value && value->valuestring && (strlen(value->valuestring) <= MAX_BLE_PASSKEY_LEN) ) {
                snprintf(data, MAX_BLE_PASSKEY_LEN+1, "%s", value->valuestring);
	    } else {
		return DATA_INVALID;
	    }
            cJSON_Delete(root);
	    for (keyInd = 0; keyInd < MAX_BLE_PASSKEY_LEN; keyInd++) {
	    	if ( (data[keyInd] < 0x30 || data[keyInd] > 0x39) && 
	        	(data[keyInd] != 0) )  {
	        	return DATA_INVALID;
	        }
	    }
        } break;
        case CFG_GW_SYSTEM_CMD:
        {
            gatewayInfo_t *gatewayInfo = (gatewayInfo_t *)output;
            cJSON *item;
            cJSON *value;

            if (!gatewayInfo)
                return DATA_PTR_INVALID;

            root = cJSON_Parse(cjson);
            if (!root)
                return DATA_INVALID;

            item = cJSON_GetObjectItem(root, "vehicle_info");
            if (!item) {
                cJSON_Delete(root);
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(item, "customer_id");
            if (value && value->valuestring && (strlen(value->valuestring) <= sizeof(gatewayInfo->customerID))) {
                snprintf(gatewayInfo->customerID, sizeof(gatewayInfo->customerID), "%s", value->valuestring);
	    } else {
		return DATA_INVALID;
	    }

            value = cJSON_GetObjectItem(item, "fleet_id");
            if (value && value->valuestring && (strlen(value->valuestring) <= sizeof(gatewayInfo->fleetID))) {
                snprintf(gatewayInfo->fleetID, sizeof(gatewayInfo->fleetID), "%s", value->valuestring);
	    } else {
		return DATA_INVALID;
	    }

            value = cJSON_GetObjectItem(item, "type");
            if (value && value->valuestring && (strlen(value->valuestring) <= sizeof(gatewayInfo->vehicleType))) {
                snprintf(gatewayInfo->vehicleType, sizeof(gatewayInfo->vehicleType), "%s", value->valuestring);
	    } else {
		return DATA_INVALID;
	    }

            value = cJSON_GetObjectItem(item, "vehicle_vin");
            if (value && value->valuestring && (strlen(value->valuestring) <= sizeof(gatewayInfo->vehicleVin))) {
                snprintf(gatewayInfo->vehicleVin, sizeof(gatewayInfo->vehicleVin), "%s", value->valuestring);
	    } else {
		return DATA_INVALID;
	    }

            CAB_DBG(CAB_GW_TRACE, "==== Gateway system information ====\n");
            CAB_DBG(CAB_GW_TRACE, "customer_id %s\n", gatewayInfo->customerID);
            CAB_DBG(CAB_GW_TRACE, "fleet_id %s\n", gatewayInfo->fleetID);
            CAB_DBG(CAB_GW_TRACE, "type %s\n", gatewayInfo->vehicleType);
            CAB_DBG(CAB_GW_TRACE, "vehicle_vin %s\n", gatewayInfo->vehicleVin);
            CAB_DBG(CAB_GW_TRACE, "=====================================\n");

            cJSON_Delete(root);
        } break;
        case CFG_GW_OPT_CMD:
        {
            vehicleOptInfo_t *vehicleOptionalInfo = (vehicleOptInfo_t *)output;
            cJSON *item;
            cJSON *value;

            if (!vehicleOptionalInfo)
                return DATA_PTR_INVALID;

            root = cJSON_Parse(cjson);
            if (!root)
                return DATA_INVALID;

            item = cJSON_GetObjectItem(root, "optional_vehicle_info");
            if (!item) {
                cJSON_Delete(root);
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(item, "fleet_name");
            if (value && value->valuestring && (strlen(value->valuestring) <= sizeof(vehicleOptionalInfo->fleetName))) {
                snprintf(vehicleOptionalInfo->fleetName, sizeof(vehicleOptionalInfo->fleetName), "%s", value->valuestring);
	    } else {
		return DATA_INVALID;
	    }

            value = cJSON_GetObjectItem(item, "vehicle_make");
            if (value && value->valuestring && (strlen(value->valuestring) <= sizeof(vehicleOptionalInfo->vehicleMake))) {
                snprintf(vehicleOptionalInfo->vehicleMake, sizeof(vehicleOptionalInfo->vehicleMake), "%s", value->valuestring);
	    } else {
		return DATA_INVALID;
	    }

            value = cJSON_GetObjectItem(item, "vehicle_year");
            if (value && value->valueint) {
                vehicleOptionalInfo->vehicleYear = value->valueint;
	    } else {
		return DATA_INVALID;
	    }

            value = cJSON_GetObjectItem(item, "tire_make");
            if (value && value->valuestring && (strlen(value->valuestring) <= sizeof(vehicleOptionalInfo->tyreMake))) {
                snprintf(vehicleOptionalInfo->tyreMake, sizeof(vehicleOptionalInfo->tyreMake), "%s", value->valuestring);
	    } else {
		return DATA_INVALID;
	    }

            value = cJSON_GetObjectItem(item, "tire_size");
            if (value && value->valuestring && (strlen(value->valuestring) <= sizeof(vehicleOptionalInfo->tyreSize))) {
                snprintf(vehicleOptionalInfo->tyreSize, sizeof(vehicleOptionalInfo->tyreSize), "%s", value->valuestring);
	    } else {
		return DATA_INVALID;
	    }

            CAB_DBG(CAB_GW_TRACE, "===== Vehical optional info =====\n");
            CAB_DBG(CAB_GW_TRACE, "fleet_name %s\n", vehicleOptionalInfo->fleetName);
            CAB_DBG(CAB_GW_TRACE, "vehicle_make %s\n", vehicleOptionalInfo->vehicleMake);
            CAB_DBG(CAB_GW_TRACE, "vehicle_year %d\n", vehicleOptionalInfo->vehicleYear);
            CAB_DBG(CAB_GW_TRACE, "tyre_make %s\n", vehicleOptionalInfo->tyreMake);
            CAB_DBG(CAB_GW_TRACE, "tyre_size %s\n", vehicleOptionalInfo->tyreSize);
            CAB_DBG(CAB_GW_TRACE, "==================================\n");
            cJSON_Delete(root);
        } break;
        case CFG_GW_TPMS_VENDOR_CMD:
        {
            cJSON *value;
            char *data = (char *)output;

            if (!data)
                return DATA_PTR_INVALID;

            root = cJSON_Parse(cjson);
            if (!root)
                return DATA_INVALID;

            value = cJSON_GetObjectItem(root, "tpms_vendor_type");
            if (value && value->valuestring) {
                snprintf(data, 32, "%s", value->valuestring);
	    } else {
                return DATA_INVALID;
	    }

            CAB_DBG(CAB_GW_TRACE, "TPMS vendor type = %s\n", data);
            cJSON_Delete(root);
        } break;
        case CFG_GW_TPMS_PAIR_CMD:
        {
            tpmsInfo_t *tpmsInfo = (tpmsInfo_t *)output;
            cJSON *item;
            cJSON *value;

            if (!tpmsInfo)
                return DATA_PTR_INVALID;

            root = cJSON_Parse(cjson);
            if (!root)
                return DATA_INVALID;

            item = cJSON_GetObjectItem(root, "tpms_info");
            if (!item) {
                cJSON_Delete(root);
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(item, "id");
            if (value && value->valuedouble) {
                tpmsInfo->sensorID = (uint32_t)value->valuedouble;
            } else {
                CAB_DBG(CAB_GW_ERR, "Invalid ID");
                return DATA_INVALID;
            }

            cJSON *pnResponse = cJSON_GetObjectItem(item,"pn");
            cJSON *modelResponse = cJSON_GetObjectItem(item,"mn");

            if (pnResponse != NULL && modelResponse != NULL) {
                value = pnResponse;
                if (value && value->valuestring && (strlen(value->valuestring) < MAX_LEN_SENSOR_PART_NO)) {
                    snprintf(tpmsInfo->sensorPN, MAX_LEN_SENSOR_PART_NO, "%s", value->valuestring);
                } else {
                    CAB_DBG(CAB_GW_ERR, "Invalid Part no");
                    return DATA_INVALID;
                }

                value = modelResponse;
                if (value && value->valuestring && (strlen(value->valuestring) < MAX_LEN_SENSOR_MODEL)) {
                    snprintf(tpmsInfo->sensorModel, MAX_LEN_SENSOR_MODEL, "%s", value->valuestring);
                } else {
                    CAB_DBG(CAB_GW_ERR, "Invalid Model no");
                    return DATA_INVALID;
                }
            }
            else {
                /* Support old App, which won't have pn and mn parameters in payload, so defaulting the values */
                snprintf(tpmsInfo->sensorPN, MAX_LEN_SENSOR_PART_NO, "%s", DEFAULT_SENSOR_PART_NO);
                snprintf(tpmsInfo->sensorModel, MAX_LEN_SENSOR_MODEL, "%s", DEFAULT_SENSOR_MODEL);
            }

            value = cJSON_GetObjectItem(item, "side");
            if (value && value->valuestring) {
                if (memcmp(value->valuestring, intTostrTireSide[0], strlen(intTostrTireSide[0])) == 0) {
                    tpmsInfo->vehicleSide =  LEFT;
                } else if (memcmp(value->valuestring, intTostrTireSide[1], strlen(intTostrTireSide[1])) == 0) {
                    tpmsInfo->vehicleSide =  RIGHT;
                } else {
                    CAB_DBG(CAB_GW_ERR, "Invalid vehicle side");
                    return DATA_INVALID;
                }
            } else {
                CAB_DBG(CAB_GW_ERR, "No value of vehicle side");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(item, "axle");
            if (value && value->valueint) {
                if (value->valueint != 0 )  {
                    tpmsInfo->axleNum = value->valueint;
                } else {	
                    CAB_DBG(CAB_GW_ERR, "Invalid axle");
                    return DATA_INVALID;
                }
            } else {
                CAB_DBG(CAB_GW_ERR, "No value of axle");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(item, "type");
            if (value && value->valuestring) {
                if (memcmp(value->valuestring, intTostrAttachedType[0], strlen(intTostrAttachedType[0])) == 0) {
                    tpmsInfo->attachedType =  TYRE;
                } else if (memcmp(value->valuestring, intTostrAttachedType[1], strlen(intTostrAttachedType[1])) == 0) {
                    tpmsInfo->attachedType =  HALO;
                } else {
                    CAB_DBG(CAB_GW_ERR, "Invalid tire type ");
                    return DATA_INVALID;
                }
            } else {
                CAB_DBG(CAB_GW_ERR, "No tire type ");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(item, "position");
            if (value && value->valuestring) {
                if (memcmp(value->valuestring, intTostrTyrePos[0], strlen(intTostrTyrePos[0])) == 0) {
                    tpmsInfo->tyrePos =  INNER;
                } else if (memcmp(value->valuestring, intTostrTyrePos[1], strlen(intTostrTyrePos[1])) == 0) {
                    tpmsInfo->tyrePos =  OUTER;
                } else {
                    CAB_DBG(CAB_GW_ERR, "Invalid tire position ");
                    return DATA_INVALID;
                }
            } else {
                CAB_DBG(CAB_GW_ERR, "No tire position ");
                return DATA_INVALID;
            }

            value = cJSON_GetObjectItem(item, "halo_enabled");
            if (value && value->valuestring) {
                if (memcmp(value->valuestring, "no", 2) == 0) {
                    tpmsInfo->isHaloEnable = DISABLE;
                } else if (memcmp(value->valuestring, "yes", 3) == 0) {
                    tpmsInfo->isHaloEnable = ENABLE;
                } else {
                    CAB_DBG(CAB_GW_ERR, "Invalid value of halo enabled");
                    return DATA_INVALID;
                }
            } else {
                CAB_DBG(CAB_GW_ERR, "No value of halo enabled");
                return DATA_INVALID;
            }

            CAB_DBG(CAB_GW_TRACE, "==== TPMS info ====\n");
            CAB_DBG(CAB_GW_TRACE, "id %04x\n", (uint32_t)tpmsInfo->sensorID);
            CAB_DBG(CAB_GW_TRACE, "Part no %s\n", tpmsInfo->sensorPN);
            CAB_DBG(CAB_GW_TRACE, "Model %s\n", tpmsInfo->sensorModel);
            CAB_DBG(CAB_GW_TRACE, "side %s\n", (tpmsInfo->vehicleSide == RIGHT)?"right":"left");
            CAB_DBG(CAB_GW_TRACE, "Axel %d\n", tpmsInfo->axleNum);
            CAB_DBG(CAB_GW_TRACE, "type %s\n", (tpmsInfo->attachedType == TYRE)?"tyre":"halo");
            CAB_DBG(CAB_GW_TRACE, "tyre pos %s\n", (tpmsInfo->tyrePos == OUTER)?"outer":"inner");
            CAB_DBG(CAB_GW_TRACE, "halo enable  %s\n", (tpmsInfo->isHaloEnable == DISABLE)?"no":"yes");
            CAB_DBG(CAB_GW_TRACE, "===================\n");
            cJSON_Delete(root);
        } break;
        case CFG_GW_TPMS_UNPAIR_CMD:
        {
            cJSON *value;
            uint32_t *ptr = (uint32_t *)output;

            if (!ptr)
                return DATA_PTR_INVALID;

            root = cJSON_Parse(cjson);
            if (!root)
                return DATA_INVALID;

            value = cJSON_GetObjectItem(root, "tpms_unpair");
            if (value && value->valuedouble) {
                *ptr = (uint32_t)value->valuedouble;
	    } else {
                return DATA_INVALID;
	    }

            CAB_DBG(CAB_GW_TRACE, "TPMS unpair %x\n", (uint32_t)value->valuedouble);
            cJSON_Delete(root);
        } break;
        case CFG_GW_CLOUD_CMD:
        {
            cJSON *value;
            char *data = (char *)output;

            if (!data)
                return DATA_PTR_INVALID;

            root = cJSON_Parse(cjson);
            if (!root)
                return DATA_INVALID;

            value = cJSON_GetObjectItem(root, "cloud_url");
            if ( value && value->valuestring && (strlen(value->valuestring) <= MAX_CLOUD_URL_LEN) ) {
                snprintf(data, MAX_CLOUD_URL_LEN+1, "%s", value->valuestring);
	    } else {
		return DATA_INVALID;
	    }
            CAB_DBG(CAB_GW_TRACE, "cloud url = %s\n", data);
            cJSON_Delete(root);
        } break;
        case CFG_GW_TARGET_PRESSURE_CMD:
        {
            cJSON *array;
            int i;
            targetPressureInfo_t *targetPressureInfo = (targetPressureInfo_t *)output;

            if (!targetPressureInfo)
                return DATA_PTR_INVALID;

            root = cJSON_Parse(cjson);
            if (!root)
                return DATA_INVALID;

            array = cJSON_GetObjectItem(root, "target_pressures");
	    if (!array) {
                return DATA_INVALID;
	    }

            targetPressureInfo->numberOfAxle = cJSON_GetArraySize(array);
            for (i = 0; i < targetPressureInfo->numberOfAxle; i++) {
                targetPressureInfo->tpmsThreshold[i] = cJSON_GetArrayItem(array, i)->valueint;
                CAB_DBG(CAB_GW_TRACE, "Axel [%d] pressure %d\n", i, targetPressureInfo->tpmsThreshold[i]);
            }
            cJSON_Delete(root);
        } break;
        case CFG_MOBILE_UTC_TIME_CMD:
        {
            cJSON *value;
            time_t *ptr = (time_t *)output;

            if (!ptr)
                return DATA_PTR_INVALID;

            root = cJSON_Parse(cjson);
            if (!root)
                return DATA_INVALID;

            value = cJSON_GetObjectItem(root, "timestamp");
            if (value && value->valuedouble) {
                *ptr = (time_t)value->valuedouble;
	    } else {
                return DATA_INVALID;
	    }

            CAB_DBG(CAB_GW_TRACE, "mobile time stamp %lu\n", *ptr);
            cJSON_Delete(root);
        } break;
        case CFG_GW_BATTERY_CMD:
        {
            cJSON *value;
            int *ptr = (int *)output;

            if (!ptr)
                return DATA_PTR_INVALID;

            root = cJSON_Parse(cjson);
            if (!root)
                return DATA_INVALID;

            value = cJSON_GetObjectItem(root, "low_battery_threshold");
            if (value && value->valueint) {
                *ptr = value->valueint;
	    } else {
		return DATA_INVALID;
	    }
            CAB_DBG(CAB_GW_TRACE, "low_battery_threshold %d\n", *ptr);
            cJSON_Delete(root);
        } break;
        case CFG_GW_ATTACH_HALO_SN_CMD:
        {
            retCode = attachHaloSN(cjson, output);
            if (retCode != GEN_SUCCESS)
                return retCode;
            
        } break;
        case CFG_GW_DETACH_HALO_SN_CMD:
        {
            retCode = detachHaloSN(cjson, output);
            if (retCode != GEN_SUCCESS)
                return retCode;

        } break;
        case CFG_GW_UPDATE_TYRE_DETAILS_CMD:
        {
            retCode = configureTyreDetails(cjson, output);
            if (retCode != GEN_SUCCESS)
                return retCode;

        } break;
        case CFG_GW_FLUSH_TYRE_DETAILS_CMD:
        case GET_CFG_GW_TYRE_DETAILS_CMD:
        case GET_CFG_GW_TREAD_HISTORY_CMD:
        {
            retCode = getTyrePositionDetails(cjson, output);
            if (retCode != GEN_SUCCESS)
                return retCode;
        } break;
        case CFG_GW_RECORD_TREAD_DETAILS_CMD:
        {
            retCode = treadRecordDetails(cjson, output);
            if (retCode != GEN_SUCCESS)
                return retCode;
        } break;
        case DIAG_GW_TPMS_BY_ID_CMD:
        {
            cJSON *value;
            uint32_t *ptr = (uint32_t *)output;

            if (!ptr)
                return DATA_PTR_INVALID;

            root = cJSON_Parse(cjson);
            if (!root)
                return DATA_INVALID;

            value = cJSON_GetObjectItem(root, "tpms_id");
            if (value && value->valuedouble) {
                *ptr = (uint32_t)value->valuedouble;
	    } else {
		return DATA_INVALID;
	    }
            CAB_DBG(CAB_GW_TRACE, "Requested tpms data for id %u %x\n", (uint32_t)value->valuedouble, (uint32_t)*ptr);
            cJSON_Delete(root);
        } break;
        case SELF_DIAG_GW_CMD:
        {
            cJSON *array;
            int i;
            selfDiagReq_t *selfDiagReq = (selfDiagReq_t *)output;

            if (!selfDiagReq)
                return DATA_PTR_INVALID;

            root = cJSON_Parse(cjson);
            if (!root)
                return DATA_INVALID;

            array = cJSON_GetObjectItem(root, "get_self_diag");
	    if (!array) {
                return DATA_INVALID;
	    }

            for (i = 0; i < MAX_SELF_DIAG_TPMS_ID; i++) {
                selfDiagReq->sensorID[i] = cJSON_GetArrayItem(array, i)->valuedouble;
                CAB_DBG(CAB_GW_TRACE, "sensorID[%d] : %X\n", i, selfDiagReq->sensorID[i]);
            }
            cJSON_Delete(root);
        } break;
    	case SYS_GW_RESET_CMD:
    	{ // Need to convert recv data into proper format from JSON
                char *data = (char *)output;
                cJSON *value;

                if (!data)
                    return DATA_PTR_INVALID;

                root = cJSON_Parse(cjson);
                if (!root)
                    return DATA_INVALID;

                value = cJSON_GetObjectItem(root, "reset_config");
                if (value && value->valuestring && (strlen(value->valuestring) <= MAX_RESET_CMD_LEN) ) {
                    snprintf(data, MAX_RESET_CMD_LEN+1, "%s", value->valuestring);
    	    } else {
    		return DATA_INVALID;
    	    }
                cJSON_Delete(root);
    	} break;
        case GET_CFG_GW_TPMS_PAIR_CMD:
        {
            bool *data = (bool *)output;
            cJSON *value = NULL;

            if (!data)
                return DATA_PTR_INVALID;

            root = cJSON_Parse(cjson);
            if (!root) {
                /* If there is no json object then it is old app. */
                application_with_pn_mn = false;
            }
            else {
                value = cJSON_GetObjectItem(root, "with_pn_mn");
                if (cJSON_IsBool(value)) {
                    application_with_pn_mn = value->valueint;
                }
                else {
                    /* If this is empty payload then it is an old app. */
                    application_with_pn_mn = false;
                }
            }
            cJSON_Delete(root);
        } break;
        default:
        {
                return DATA_INVALID;
        }
    }

    FUNC_EXIT
    return GEN_SUCCESS;
}

static bool getUartCmdHandlerAliveStatus(void)
{
	bool status;
	pthread_mutex_lock(&uartCmdHandlerAliveLock);
	status = g_bleUartCmdHandlerAliveStatus;
	pthread_mutex_unlock(&uartCmdHandlerAliveLock);
	return status;
}

static void setUartCmdHandlerAliveStatus(bool value)
{
	pthread_mutex_lock(&uartCmdHandlerAliveLock);
	g_bleUartCmdHandlerAliveStatus = value;
	pthread_mutex_unlock(&uartCmdHandlerAliveLock);
}

bool uartCmdHandlerCallback(void)
{
	bool status = false;
    	mqd_t msgQueueId;
    	uartFrame_t *uartFrame = NULL;
    	msgQueue_t msgQueue;
	struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0};
	clock_gettime(CLOCK_REALTIME, &abs_timeout);
	abs_timeout.tv_sec += MQ_SEND_BLE_KEEPALIVE_TOUT;

	if(getUartCmdHandlerAliveStatus() == true)
	{
		status = true;
	}
	
	uartFrame = malloc(sizeof(uartFrame_t));
	if (!uartFrame) {
	        CAB_DBG(CAB_GW_ERR, "UART Data Handler Callback : out of memory");
		status = false;
	}

	msgQueueId = mq_open(QUEUE_NAME, O_WRONLY);
    	if (msgQueueId == (mqd_t)-1)
	{
        	CAB_DBG(CAB_GW_ERR, "%s: %s", QUEUE_NAME, strerror(errno));
	        free(uartFrame);
		status = false;
	}

	setUartCmdHandlerAliveStatus(false);

	uartFrame->cmd = SYS_BLE_WATCHDOG_DATA_CMD;
	msgQueue.ptr = uartFrame;     
	if (mq_timedsend(msgQueueId, (const char *)&msgQueue, MAX_MSG_SIZE, 0, &abs_timeout)) {
	        CAB_DBG(CAB_GW_ERR, "mq_timedsend %s", strerror(errno));
	        free(uartFrame);
		mq_close(msgQueueId);
		status = false;
	}

	free(uartFrame);
	mq_close(msgQueueId);
	return status;
}

/** @brief ble command parse, process, action
 *
 *     API should parse ble command, process
 *
 * @param[in] none
 *
 * @return none
 */
static void *uartCommandHandler(void *argv)
{
    FUNC_ENTRY

    mqd_t msgQueueId;
    struct mq_attr msgQueueAttr;
    returnCode_e retCode;
    uartFrame_t *uartFrame = (uartFrame_t *)0;
    uartFrame_t *uartFileFrame = (uartFrame_t *)0;
    msgQueue_t msgQueue;
    watchDogTimer_t newWatchdogNode;
    char errorMsg[100] = "";
    
    newWatchdogNode.threadId = syscall(SYS_gettid);
    newWatchdogNode.CallbackFn = uartCmdHandlerCallback;
    
    CAB_DBG(CAB_GW_INFO, "BLE UART command handler thread id : %ld", syscall(SYS_gettid));
    retCode = threadRegister(&newWatchdogNode);
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "UART command handler thread registration failed with error %d", retCode);
            rDiagSendMessage(WATCHDOG_REGISTRATION_FAIL, GW_ERR, "UART command handler thread registration failed");
    }        

    msgQueueId = mq_open(QUEUE_NAME, O_RDONLY);
    if (msgQueueId == (mqd_t)-1) {
        CAB_DBG(CAB_GW_ERR,"mq_open %s, %s\n", QUEUE_NAME, strerror(errno));
        pthread_exit(NULL);
        retCode = threadDeregister(newWatchdogNode.threadId);
        if(retCode != GEN_SUCCESS)
        {
                CAB_DBG(CAB_GW_ERR, "UART command handler thread de-registration failed with error %d", retCode);
                rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "UART command handler thread de-registration failed");
        }        
        return NULL;
    }

    CAB_DBG(CAB_GW_TRACE ,"Command handler thread started\n");

    while (1) {
        mq_getattr(msgQueueId, &msgQueueAttr);
        if (!msgQueueAttr.mq_curmsgs)
            pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);

        if (mq_receive(msgQueueId, (char *)&msgQueue, MAX_MSG_SIZE, NULL) != -1) {
            pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
            uartFrame = msgQueue.ptr;

            if(uartFrame->cmd == SYS_BLE_WATCHDOG_DATA_CMD) {
                CAB_DBG(CAB_GW_TRACE, "In Watchdog Timer Dummy Command");
                setUartCmdHandlerAliveStatus(true);
                continue;
            }

            /* Internal command for NUS to get BLE MAC*/
            if(uartFrame->cmd == SYS_BLE_MAC_CMD){
                sendDataToBle(SYS_BLE_MAC_CMD, NULL);
                if (uartFrame) {
                    CAB_DBG(CAB_GW_TRACE,"mq_receive frame free up %p", uartFrame);
                    free(uartFrame);
                    uartFrame = (uartFrame_t*)0;
                }
                continue;
            }

            /* Internal command for NUS to get notification for BLE ADV. start*/
            if(uartFrame->cmd == SYS_ADVERTISEMENT_STARTED){
                sendDataToBle(SYS_ADVERTISEMENT_STARTED, NULL);
                if (uartFrame) {
                    CAB_DBG(CAB_GW_TRACE,"mq_receive frame free up %p", uartFrame);
                    free(uartFrame);
                    uartFrame = (uartFrame_t*)0;
                }
                continue;
            }

            /* Receive payload for single transfer command and decrypt it */
            uartFrame->crc8 = (uint8_t)uartFrame->data[uartFrame->payloadLen];
            uartFrame->data[uartFrame->payloadLen] = '\0';
            if (uartFrame->cmd != CFG_FILE_INIT_CMD) {
                retCode = validateCRC8(uartFrame->data, uartFrame->payloadLen, uartFrame->crc8);
                if (retCode != GEN_SUCCESS) {
                    sendCommandStatus(uartFrame->cmd, retCode);
                    CAB_DBG(CAB_GW_WARNING, "ble command invalid crc %d", uartFrame->cmd);
                    free(uartFrame);
                    continue;
                }
                if (uartFrame->cmd != CFG_FILE_DATA_CMD)
                    aes128Decrypt(uartFrame->data, uartFrame->payloadLen);
            }

            if (uartFileFrame && uartFrame->cmd != CFG_FILE_DATA_CMD) {
                CAB_DBG(CAB_GW_WARNING, "File packet sequance missed");
                if (uartFileFrame->filePayload) {
                    CAB_DBG(CAB_GW_TRACE,"Free up old payload,%p", uartFileFrame->filePayload);
                    free(uartFileFrame->filePayload);
                }
                CAB_DBG(CAB_GW_TRACE,"Free up frame %p\n", uartFileFrame);
                free(uartFileFrame);
                uartFileFrame = (uartFrame_t *)0;
            }
            CAB_DBG(CAB_GW_INFO, "BLE command recvd %d", uartFrame->cmd);
            switch(uartFrame->cmd) {
                case CFG_FILE_INIT_CMD:
                {
                    dumpCommand(uartFrame, __func__);
                    uartFileFrame = uartFrame;
                    uartFileFrame->frameCount = 0;
                    /*Checking file length to aviod any malloc specific errors in the code*/
                    if (uartFileFrame->fileLen > MAX_FILE_LEN_ALLOWED) {
                        CAB_DBG(CAB_GW_ERR, "Too big file ! not allowed in GW");
                        sendCommandStatus(uartFrame->subCommand, DATA_INVALID);
                        if (uartFrame) {
                            free(uartFrame);
                        }
                        uartFrame = (uartFrame_t *)0;
                        uartFileFrame = (uartFrame_t *)0;
                        retCode = DATA_INVALID;
                        break;
                    }

                    uartFileFrame->filePayload = malloc(sizeof(uartFileFrame->fileLen) + 1024);
                    memset(uartFileFrame->filePayload, 0x00 , sizeof(uartFileFrame->fileLen) + 1024);
                    if (!uartFileFrame->filePayload) {
                        uartFileFrame = (uartFrame_t *)0;
                    } else {
                        uartFileFrame->filePayload[uartFileFrame->fileLen] = '\0';
                        uartFrame = (uartFrame_t*)0;
                    }
                    continue;
                } break;
                case CFG_FILE_DATA_CMD:
                {
                    if (!uartFileFrame || !(uartFileFrame->filePayload))
                        break;
                    /*Checking file length to aviod any malloc specific errors in the code*/
                    if (uartFileFrame->fileLen > MAX_FILE_LEN_ALLOWED) {
                        CAB_DBG(CAB_GW_ERR, "Too big file ! not allowed in GW");
                        sendCommandStatus(uartFrame->subCommand, DATA_INVALID);
                        retCode = DATA_INVALID;
                        if (uartFileFrame->filePayload)
                            free(uartFileFrame->filePayload);
                        if (uartFileFrame)
                            free(uartFileFrame);

                        uartFileFrame = (uartFrame_t *)0;
                        break;
                    }
                    CAB_DBG(CAB_GW_TRACE, "remaining file len %d, frame count %d\n", uartFileFrame->fileLen, uartFileFrame->frameCount);
                    memcpy(uartFileFrame->filePayload + (uartFileFrame->frameCount * APP_WISE_MAX_PAYLOAD(maxMTUPerApp)),
                            uartFrame->data, uartFrame->payloadLen);
                    uartFileFrame->fileLen -= uartFrame->payloadLen;
                    ++(uartFileFrame->frameCount);

                    if (uartFileFrame->fileLen > 0)
                        continue;

                    CAB_DBG(CAB_GW_TRACE, "sub command %04x\n",uartFileFrame->subCommand);
                    CAB_DBG(CAB_GW_TRACE, "Encrypted file data %d\n", uartFrame->payloadLen + ((uartFileFrame->frameCount - 1) * APP_WISE_MAX_PAYLOAD(maxMTUPerApp)));
                    aes128Decrypt(uartFileFrame->filePayload, (uartFrame->payloadLen + ((uartFileFrame->frameCount - 1) * APP_WISE_MAX_PAYLOAD(maxMTUPerApp))));
                }
                default:
                {
                    bool commandFound = false;
                    uint16_t cmdType = (uartFrame->cmd == CFG_FILE_DATA_CMD) ? uartFileFrame->subCommand : uartFrame->cmd;
                    for (int i=0; i < (sizeof(CmdLookupTable)/sizeof(CmdLookupTable[0])); i++) {
                        int frameType = -1;

                        if (cmdType == CmdLookupTable[i].commandID) {
                            commandFound = true;
                            frameType = (connectedAppType == ANDROID_APP) ? CmdLookupTable[i].androidCmdFrameType : CmdLookupTable[i].iosCmdFrameType;
                        }
                        else {
                            continue;
                        }

                        if (frameType == SINGLE_XFER_COMMAND) {
                            (*CmdLookupTable[i].commandHandler)((void *)uartFrame);
                        }
                        else if (frameType == FILE_XFER_COMMAND) {
                            (*CmdLookupTable[i].commandHandler)((void *)uartFileFrame);
                        }
                        else {
                            // Nothing to implement here
                        }

                        break;
                    }

                    if (!commandFound) {
                        CAB_DBG(CAB_GW_WARNING, "Invalid command received %04x", uartFrame->cmd);
                    }
                } break;
            }
            if (uartFrame) {
                CAB_DBG(CAB_GW_TRACE,"mq_receive frame free up %p\n", uartFrame);
                free(uartFrame);
                uartFrame = (uartFrame_t*)0;
            }
            if (uartFileFrame) {
                CAB_DBG(CAB_GW_TRACE,"file payload buffer free %p\n", uartFileFrame->filePayload);
                free(uartFileFrame->filePayload);
                CAB_DBG(CAB_GW_TRACE,"file frame buffer free %p\n", uartFileFrame);
                free(uartFileFrame);
                uartFileFrame = (uartFrame_t *)0;
            }
        } else {
        	CAB_DBG(CAB_GW_WARNING, "mq_receive for %s failed with error %s", QUEUE_NAME, strerror(errno));
			sprintf(errorMsg, "mq_receive for %s failed with error %s", QUEUE_NAME, strerror(errno));
			rDiagSendMessage(MESSAGE_QUEUE_OVERFLOW, GW_WARNING, errorMsg);
		}
    }
    mq_close(msgQueueId);

    retCode = threadDeregister(newWatchdogNode.threadId);
    if(retCode != GEN_SUCCESS)
    {
            CAB_DBG(CAB_GW_ERR, "UART command handler thread de-registration failed with error %d", retCode);
            rDiagSendMessage(WATCHDOG_DEREGISTRATION_FAIL, GW_ERR, "UART command handler thread de-registration failed");
    }        
    FUNC_EXIT

    pthread_exit(NULL);
}

returnCode_e bleSetPowerMode(commandCode_e powerType)
{
    switch (powerType) {
        case SYS_BLE_LOW_POWER_CMD:
        {
            sendDataToBle(SYS_BLE_LOW_POWER_CMD, NULL);
        } break;
        case SYS_NOTIFY_BLE_RESET_CMD:
        {
/*CG-685 -> No need for reset now as restarting advertising from the FW*/
#if 0
            //TODO: add proper gpio userspace api, seperate deep sleep gpio
            system("echo 23 > export");
            system("echo \"out\" > gpio32/direction");
            system("echo 0 > /sys/class/gpio/gpio32/value");
            system("echo 1 > /sys/class/gpio/gpio32/value");
#endif
        } break;
        default:
        {
            CAB_DBG(CAB_GW_TRACE, "Command not supported\n");
        }
    }
}

returnCode_e setBLEKeepAliveStatus (bool isBLEAlive)
{
	FUNC_ENTRY
	pthread_mutex_lock(&keepAliveFlagLock);
	g_isBLEAlive = isBLEAlive;
	pthread_mutex_unlock(&keepAliveFlagLock);
	FUNC_EXIT
	return GEN_SUCCESS;
}

bool getBLEKeepAliveStatus ()
{
	FUNC_ENTRY
	bool isBLEAlive;
	pthread_mutex_lock(&keepAliveFlagLock);
	isBLEAlive = g_isBLEAlive;
	pthread_mutex_unlock(&keepAliveFlagLock);
	FUNC_EXIT
	return isBLEAlive;
}

//*****************************************************************************
//  getBleMacAdvStarted()
//  Param:
//      IN :    NONE
//      OUT:    NONE
//  Returns:
//              returnCode_e    //return code
//  Description:
//      This function sends event to BLE in order to get status of BLE
//      advertisement.
//
//  [Pre-condition:]
//      bleInited should be 1
//  [Constraints:]
//      None
//
//*****************************************************************************
returnCode_e getBleMacAdvStarted(void)
{
    returnCode_e retCode;
    mqd_t msgQueueId;
    uartFrame_t *uartFrame = NULL;
    msgQueue_t msgQueue;
    struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0};
    char errorMsg[100] = "";
    clock_gettime(CLOCK_REALTIME, &abs_timeout);
    abs_timeout.tv_sec += MQ_SEND_BLE_KEEPALIVE_TOUT;


    FUNC_ENTRY
    if (bleInited == 1){
        msgQueueId = mq_open(QUEUE_NAME, O_WRONLY);
        if (msgQueueId == (mqd_t)-1) {
            CAB_DBG(CAB_GW_ERR, "%s: %s", QUEUE_NAME, strerror(errno));
            return MODULE_INIT_FAIL;
        }

        uartFrame = malloc(sizeof(uartFrame_t));
        if (!uartFrame) {
            CAB_DBG(CAB_GW_ERR, "Ble command : out of memory");
            mq_close(msgQueueId);
            return MODULE_INIT_FAIL;
        }
        uartFrame->cmd = SYS_ADVERTISEMENT_STARTED;
        msgQueue.ptr = uartFrame;     
        if (mq_timedsend(msgQueueId, (const char *)&msgQueue, MAX_MSG_SIZE, 0, &abs_timeout)) {
            CAB_DBG(CAB_GW_WARNING, "mq_timedsend for %s failed with error %s", QUEUE_NAME, strerror(errno));
            sprintf(errorMsg, "mq_timedsend for %s failed with error %s", QUEUE_NAME, strerror(errno));
            rDiagSendMessage(MESSAGE_QUEUE_OVERFLOW, GW_WARNING, errorMsg);
            free(uartFrame);
        }
        CAB_DBG(CAB_GW_TRACE, "Queued up message\n");
        mq_close(msgQueueId);   //retCode = sendDataToBle(SYS_BLE_MAC_CMD, NULL);
        retCode = GEN_SUCCESS;
    } 
    else
    retCode = MODULE_INIT_FAIL;
    FUNC_EXIT
    return retCode;
}

returnCode_e getBleMacAdd(void)
{
    returnCode_e retCode;
    mqd_t msgQueueId;
    uartFrame_t *uartFrame = NULL;
    msgQueue_t msgQueue;
    struct timespec abs_timeout = {.tv_sec = 0, .tv_nsec = 0};
    char errorMsg[100] = "";
    clock_gettime(CLOCK_REALTIME, &abs_timeout);
    abs_timeout.tv_sec += MQ_SEND_BLE_KEEPALIVE_TOUT;


    FUNC_ENTRY
    if (bleInited == 1) {
        msgQueueId = mq_open(QUEUE_NAME, O_WRONLY);
        if (msgQueueId == (mqd_t)-1) {
            CAB_DBG(CAB_GW_ERR, "%s: %s", QUEUE_NAME, strerror(errno));
            return MODULE_INIT_FAIL;
        }

        uartFrame = malloc(sizeof(uartFrame_t));
        if (!uartFrame) {
            CAB_DBG(CAB_GW_ERR, "Ble command : out of memory");
            mq_close(msgQueueId);
            return MODULE_INIT_FAIL;
        }
        uartFrame->cmd = SYS_BLE_MAC_CMD;
        msgQueue.ptr = uartFrame;     
        if (mq_timedsend(msgQueueId, (const char *)&msgQueue, MAX_MSG_SIZE, 0, &abs_timeout)) {
            CAB_DBG(CAB_GW_WARNING, "mq_timedsend for %s failed with error %s", QUEUE_NAME, strerror(errno));
            sprintf(errorMsg, "mq_timedsend for %s failed with error %s", QUEUE_NAME, strerror(errno));
            rDiagSendMessage(MESSAGE_QUEUE_OVERFLOW, GW_WARNING, errorMsg);
            free(uartFrame);
        }
        CAB_DBG(CAB_GW_TRACE, "Queued up message\n");
        mq_close(msgQueueId);	//retCode = sendDataToBle(SYS_BLE_MAC_CMD, NULL);
        retCode = GEN_SUCCESS;
    }
    else
       retCode = MODULE_INIT_FAIL;
   FUNC_EXIT
   return retCode;
}

returnCode_e bleInit(void)
{
    FUNC_ENTRY

    mqd_t msgQueueId;
    struct mq_attr msgQueueAttr;
    returnCode_e retCode;

    CAB_DBG(CAB_GW_INFO, "Initializing BLE interface");

    if (bleInited == 1)
    {
        return GEN_SUCCESS;
    }
    else {
        retCode = setUartPort(UART_PORT, UART_BAUDRATE);
        if (GEN_SUCCESS != retCode) {
            CAB_DBG(CAB_GW_ERR, "uart port %s, baudrate %d, %s", UART_PORT, UART_BAUDRATE,  strerror(errno));
            FUNC_EXIT
            return MODULE_INIT_FAIL;
        }

        memset(&msgQueueAttr, 0, sizeof(msgQueueAttr));

        msgQueueAttr.mq_maxmsg = MAX_QUEUE_SIZE;
        msgQueueAttr.mq_msgsize = MAX_MSG_SIZE;

        CAB_DBG(CAB_GW_TRACE, "max message size %ld\n", msgQueueAttr.mq_msgsize);
        CAB_DBG(CAB_GW_TRACE, "max queue size %ld\n", msgQueueAttr.mq_maxmsg);

        UNUSED_VARIABLE(mq_unlink(QUEUE_NAME));
        msgQueueId = mq_open(QUEUE_NAME, O_CREAT|O_RDWR, S_IRWXU, &msgQueueAttr);
        if (msgQueueId == (mqd_t)-1) {
            CAB_DBG(CAB_GW_ERR, "mq_open %s:%s", QUEUE_NAME, strerror(errno));
            FUNC_EXIT
            return MODULE_INIT_FAIL;
        }
        UNUSED_VARIABLE(mq_close(msgQueueId));

        retCode = pthread_create(&readerId, NULL, uartDataHandler, NULL);
        if (retCode != 0) {
            CAB_DBG(CAB_GW_ERR, "pthread_create %s", strerror(errno));
            FUNC_EXIT
            return MODULE_INIT_FAIL;
        }

        retCode = pthread_create(&consumerId, NULL, uartCommandHandler, NULL);
        if (retCode != 0) {
            CAB_DBG(CAB_GW_ERR, "pthread_create %s", strerror(errno));
            FUNC_EXIT
            return MODULE_INIT_FAIL;
        }

        if (pthread_mutex_init(&keepAliveFlagLock, NULL) != 0) {
            CAB_DBG(CAB_GW_ERR, "Keep alive flag lock init failed");
            FUNC_EXIT
            return MODULE_INIT_FAIL;
        }

        if (pthread_mutex_init(&uartDataHandlerAliveLock, NULL) != 0) {
            CAB_DBG(CAB_GW_ERR, "UART Data Handler lock init failed");
            pthread_mutex_destroy(&keepAliveFlagLock);
            FUNC_EXIT
            return MODULE_INIT_FAIL;
        }

        if (pthread_mutex_init(&uartCmdHandlerAliveLock, NULL) != 0) {
            CAB_DBG(CAB_GW_ERR, "UART Command Handler lock init failed");
            pthread_mutex_destroy(&keepAliveFlagLock);
            pthread_mutex_destroy(&uartDataHandlerAliveLock);
            FUNC_EXIT
            return MODULE_INIT_FAIL;
        }

        pthread_detach(consumerId);
        pthread_detach(readerId);

        uint8_t lteIMEI[MAX_LEN_LTE_IMEI] = {0x00};
        getConfigForBle(SYS_GET_LTE_IMEI_CMD, lteIMEI);
        sendDataToBle(SYS_NOTIFY_GW_APP_START, lteIMEI);

        sendDataToBle(SYS_BLE_MAC_CMD, NULL);
        sendDataToBle(SYS_ADVERTISEMENT_STARTED, NULL);
        bleInited = 1;
    }
    FUNC_EXIT

    return GEN_SUCCESS;
}

returnCode_e bleDeInit(void)
{
    FUNC_ENTRY

    if (bleInited == 1) {
       pthread_cancel(readerId);
       pthread_cancel(consumerId);
       bleInited = 0;
   }

    CAB_DBG(CAB_GW_INFO, "Deinitializing BLE interface");

    pthread_mutex_destroy(&keepAliveFlagLock);
    pthread_mutex_destroy(&uartDataHandlerAliveLock);
    pthread_mutex_destroy(&uartCmdHandlerAliveLock);

    FUNC_EXIT
    return GEN_SUCCESS;
}
