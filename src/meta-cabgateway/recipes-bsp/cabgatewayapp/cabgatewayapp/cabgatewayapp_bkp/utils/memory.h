#ifndef __MEM_H
#define __MEM_H
#include<stdio.h>
#include "error.h"
#include "debug.h"

#define MAX_MEM_LEAK (190000)

void meminit(); 

returnCode_e incrMemCount(size_t incrementMem);

returnCode_e decrMemCount(size_t decrementMem);

returnCode_e checkSelfProcMem(uint32_t *selfMemUsageKB);
#endif
