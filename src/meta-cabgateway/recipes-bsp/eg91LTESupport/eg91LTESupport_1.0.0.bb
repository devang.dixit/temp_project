SUMMARY = " EG91 LTE Module Support "
DESCRIPTION = " Support ppp scripts for EG91 LTE module dialing for internet. "
SECTION = "base"
LICENSE = "Proprietary"

LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}-${PV}:"

SRC_URI = " file://scripts \
                "

S = "${WORKDIR}"

do_install() {
        install -d ${D}/etc/ppp/peers/
        install -m 0755 ${S}/scripts/quectel-ppp ${D}/etc/ppp/peers/
        install -m 0755 ${S}/scripts/quectel-chat-connect ${D}/etc/ppp/peers/
        install -m 0755 ${S}/scripts/quectel-chat-disconnect ${D}/etc/ppp/peers/
        install -m 0755 ${S}/scripts/quectel-ppp_jio ${D}/etc/ppp/peers/
        install -m 0755 ${S}/scripts/quectel-chat-connect_jio ${D}/etc/ppp/peers/
        install -m 0755 ${S}/scripts/quectel-chat-disconnect_jio ${D}/etc/ppp/peers/
}

FILES_${PN} += "/etc/ppp/peers/*"

COMPATIBLE_MACHINE = "(mx7|mx6|mx6ul|mx6ull)"

