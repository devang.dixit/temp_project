FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += " \
	file://0001-U-Boot-Patch-for-Pmic-in-cabgw.patch \
	file://0002-Added-Cabgateway-Board-Support.patch \
	file://0003-Added-board-support-for-device-tree.patch \
	file://0004-Modified-config-file-for-board-support.patch \
	file://0005-Added-board-supported-device-tree-in-Makefile.patch \
	file://0006-Modified-config-file.patch \
	file://0007-Modified-Device-tree-for-MMC.patch \
	file://0008-Added-support-for-ota-in-u-boot.patch \
	file://0009-Added-support-for-OTA-and-changed-the-environment-va.patch \
	file://0001-Debug-port-moved-to-UART2-RS232-port-of-cab-gateway.patch \
	file://0010-Modified-bootlimit-for-upgrade_available-as-0-to-res.patch \
	"
