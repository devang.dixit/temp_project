FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += " \
	 file://0001-Added-driver-changes-for-supporting-EG91-LTE-module.patch \
	 file://0002-Kernel-RTC-driver-support.patch \
	 file://0003-Kernel-Pmic-support.patch \
	 file://0004-Kernel-gpio-driver-changes-for-interrupt.patch \
	 file://0005-Device-Tree-for-Cabgateway-board-support.patch \
	 file://0006-Modified-the-GPIO-placement-in-device-tree.patch \
	 file://0007-Removed-battery-switch-over-interrupt-node-in-device.patch \
	 file://0008-Added-battery-switch-over-interrupt-in-Device-tree.patch \
	 file://0009-Added-support-for-power-source-interrupt.patch \
	 file://0010-Added-support-for-reading-gpio-value-for-power-sourc.patch \
	 file://0011-Added-uart-node-for-testjig-test-mode-pin-as-gpio-an.patch \
	 file://0012-Changed-the-IRQ-number-in-kernel-as-per-new-device-t.patch \
	 file://0013-Changed-Power-Source-interrupt.patch \
	 file://0014-lis2hh12-driver-support-added-with-extra-dummy-event.patch \
	 file://0015-Minor-syntax-error.patch \
	 file://0016-DTS-changes-for-GPIO-settings-and-ACC-interrupts.patch \
	 file://0017-Added-support-for-threshold-and-duration-in-inactivi.patch \
	 file://0018-Added-debugfs-reg-access-support.patch \
	 file://0019-Direct-reg-access-for-lis2hh12-driver.patch \
	 file://0020-Added-event-and-reading-support-temp.patch \
	 file://0021-Added-acceleromter-interrupt-signal-support.patch \
	 file://0022-Updated-DTS-for-GPIO-interrupt-and-wakeup-configs.patch \
	 file://0023-Poweroff-wakeup-bit-enabled-in-dtsi-for-snvs-rtc-fea.patch \
	 file://0024-Kernel-MPU-Temp-Threshold-to-115C.patch \
         file://0025-Modified-DTS-for-HW-version-GPIO.patch \
         file://0026-Added-BSF-Hard-Pwr-Off-changes.patch \
         file://0027-Added-Support-For-lis2dw12.patch \
         file://0028-Added_Changes_In_ACC_Driver_to_support_LIS2DW12_XYZ_axis.patch \
	 file://lte.cfg \
	 file://rtc.cfg \
	 file://mqueue.cfg \
	 file://can.cfg \
	 file://panic.cfg \
	 file://multimedia_remove.cfg \
	 file://staging_remove.cfg \
	 file://iio.cfg \
	 file://triggeriio.cfg \
"

do_configure_append() {
    cat ${WORKDIR}/*.cfg >> ${B}/.config
}
