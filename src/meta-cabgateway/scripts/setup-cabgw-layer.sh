#
# Cab gateway layer setup script
#
# Copyright (C) 2016 Volansys Technologies Pvt. LTD.

CURR_DIR=${PWD}
SOURCE_DIR="../../../../../../../../"

cd $SOURCE_DIR"../"
DISTRO=cabgateway-dist MACHINE=cabgateway source fsl-setup-release.sh -b build-x11-cabGateway

touch conf/sanity.conf

cd $CURR_DIR
META_SWUPDATE_URL="https://github.com/sbabic/meta-swupdate.git"
META_SWUPDATE_COMMIT_HASH="dae0d3e906e764e02a8b6357e7698bfde278fa6e"


echo "Downloading meta-swupdate package"
echo "URL:${META_SWUPDATE_URL}"
cd $SOURCE_DIR
git clone ${META_SWUPDATE_URL}

if [ ! -d "meta-swupdate" ]; then
        # Control will enter here if $DIRECTORY doesn't exist.
        echo " Failed to download the package"
        exit 1
fi

cd meta-swupdate
git checkout master
git reset --hard ${META_SWUPDATE_COMMIT_HASH}
cd ../..

# Copy necessary files in build directory
cp -r sources/halo-connect-firmware/CAB-Gateway/src/gw/imx6ull/meta/meta-cabgw-ota/recipes-volansys/ota-tools/files/keys build-x11-cabGateway/
cp -r sources/halo-connect-firmware/CAB-Gateway/src/gw/imx6ull/meta/meta-cabgw-ota/recipes-volansys/ota-tools/files/update.sh build-x11-cabGateway/
cp -r sources/halo-connect-firmware/CAB-Gateway/src/gw/imx6ull/meta/meta-cabgw-ota/recipes-volansys/ota-tools/files/create-bundle.sh  build-x11-cabGateway/

if grep -Fq "${BSPDIR}/sources/halo-connect-firmware/CAB-Gateway/src/gw/imx6ull/meta/meta-cabgw-ota \"" build-x11-cabGateway/conf/bblayers.conf
then
        echo "Already meta-cabgw-ota meta layer set"
else
        echo "BBLAYERS += \" \${BSPDIR}/sources/halo-connect-firmware/CAB-Gateway/src/gw/imx6ull/meta/meta-cabgw-ota \"" >> build-x11-cabGateway/conf/bblayers.conf
fi

if grep -Fq "${BSPDIR}/sources/meta-swupdate \"" build-x11-cabGateway/conf/bblayers.conf
then
        echo "Already meta-swupdate meta layer set"
else
        echo "BBLAYERS += \" \${BSPDIR}/sources/meta-swupdate \"" >> build-x11-cabGateway/conf/bblayers.conf
fi

if grep -Fq "${BSPDIR}/sources/meta-openembedded/meta-oe \"" build-x11-cabGateway/conf/bblayers.conf
then
        echo "Already meta-oe meta layer set"
else
        echo "BBLAYERS += \" \${BSPDIR}/sources/meta-openembedded/meta-oe \"" >> build-x11-cabGateway/conf/bblayers.conf
fi

if grep -Fq "${BSPDIR}/sources/halo-connect-firmware/CAB-Gateway/src/gw/imx6ull/meta/meta-cabgateway \"" build-x11-cabGateway/conf/bblayers.conf
then
        echo "Already meta-cabgateway meta layer set"
else
        echo "BBLAYERS += \" \${BSPDIR}/sources/halo-connect-firmware/CAB-Gateway/src/gw/imx6ull/meta/meta-cabgateway \"" >>  build-x11-cabGateway/conf/bblayers.conf
fi

cd build-x11-cabGateway/

echo ""
echo "Cab Gateway setup complete. Create an image with:"
echo "    $ bitbake cabgateway-image"
echo ""
